<?php
/**
 * Copyright (c) 2018 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *   Christopher Guindon (Eclipse Foundation) - Improve breadcrumbs
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");
// Set the theme for your project's web pages.
$theme = NULL;

if (!isset($Breadcrumb) || !is_a($Breadcrumb, 'Breadcrumb')) {
  $Breadcrumb = new Breadcrumb();
  $items = explode("/", $_SERVER['REQUEST_URI']);

  // Custom breadcrumbs for the Eclispe Newsletter
  if ($items[2] == 'eclipse_newsletter') {

    // Replace community with eclipse newsletter
    if (strtolower($items[1]) === "community") {
      $Breadcrumb->insertCrumbAt(1, "Eclipse Newsletter", "/community/eclipse_newsletter/", "_self");
      $Breadcrumb->removeCrumb(2);
    }

    // For article specific breadcrumb
    if (!empty($items[3])) {
      if (strpos($items[3], '20') !== FALSE) {
        $Breadcrumb->insertCrumbAt(2, $items[3], "/community/eclipse_newsletter/archives.php", "_self");
      }
    }

    if (empty($items[5])) {
      $Breadcrumb->removeCrumb(4);
    }

    if (isset($breadcrumbParentTitle) && !empty($items[5])) {
      $Breadcrumb->insertCrumbAt(3, $breadcrumbParentTitle, "/community/eclipse_newsletter/" . $items[3] . "/" . $items[4], "_self");
    }
  }
}