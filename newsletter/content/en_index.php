<?php
/**
 * Copyright (c) 2018, 2022, 2023, 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation) - initial API and implementation
 *    Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>

<div class="col-xs-24">
  <h1><?php print $pageTitle; ?></h1>
  <p>The Eclipse Foundation publishes a monthly Eclipse Member Newsletter. If you are interested in receiving this newsletter, please join the <a href="https://accounts.eclipse.org/mailing-list/eclipse.org-member-marketing">eclipse.org-member-marketing</a> or <a href="https://accounts.eclipse.org/mailing-list/eclipse.org-members-committers">eclipse.org-members-committers mailing lists</a>.</p>
  <div class="col-md-6">
    <h3>2024</h3>
    <ul>
      <li><a href="2024/october/" target="_blank">October 2024</a></li>
      <li><a href="2024/september/" target="_blank">September 2024</a></li>
      <li><a href="2024/august/" target="_blank">August 2024</a></li>
      <li><a href="2024/july/" target="_blank">July 2024</a></li>
      <li><a href="2024/june/" target="_blank">June 2024</a></li>
      <li><a href="2024/may/" target="_blank">May 2024</a></li>
      <li><a href="2024/march-april/" target="_blank">March/April 2024</a></li>
      <li><a href="2024/february/" target="_blank">February 2024</a></li>
      <li><a href="2024/january/" target="_blank">January 2024</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2023</h3>
    <ul>
      <li><a href="2023/december/" target="_blank">December 2023</a></li>
      <li><a href="2023/november/" target="_blank">November 2023</a></li>
      <li><a href="2023/october/" target="_blank">October 2023</a></li>
      <li><a href="2023/september/" target="_blank">September 2023</a></li>
      <li><a href="2023/august/" target="_blank">August 2023</a></li>
      <li><a href="2023/june-july/" target="_blank">June/July 2023</a></li>
      <li><a href="2023/may/" target="_blank">May 2023</a></li>
      <li><a href="2023/april/" target="_blank">April 2023</a></li>
      <li><a href="2023/march/" target="_blank">March 2023</a></li>
      <li><a href="2023/february/" target="_blank">February 2023</a></li>
      <li><a href="2023/january/" target="_blank">January 2023</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2022</h3>
    <ul>
      <li><a href="2022/november-december/" target="_blank">November/December 2022</a></li>
      <li><a href="2022/october/" target="_blank">October 2022</a></li>
      <li><a href="2022/september/" target="_blank">September 2022</a></li>
      <li><a href="2022/july/" target="_blank">July 2022</a></li>
      <li><a href="2022/june/" target="_blank">June 2022</a></li>
      <li><a href="2022/may/" target="_blank">May 2022</a></li>
    	<li><a href="2022/april/" target="_blank">April 2022</a></li>
    	<li><a href="2022/march/" target="_blank">March 2022</a></li>
    	<li><a href="2022/february/" target="_blank">February 2022</a></li>
    	<li><a href="2022/january/" target="_blank">January 2022</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2021</h3>
    <ul>
      <li><a href="2021/december/" target="_blank">December 2021</a></li>
      <li><a href="2021/november/" target="_blank">November 2021</a></li>
      <li><a href="2021/october/" target="_blank">October 2021</a></li>
      <li><a href="2021/september/" target="_blank">September 2021</a></li>
      <li><a href="2021/august/" target="_blank">August 2021</a></li>
      <li><a href="2021/june/" target="_blank">June 2021</a></li>
      <li><a href="2021/may/" target="_blank">May 2021</a></li>
      <li><a href="2021/april/" target="_blank">April 2021</a></li>
      <li><a href="2021/march/" target="_blank">March 2021</a></li>
      <li><a href="2021/february/" target="_blank">February 2021</a></li>
    	<li><a href="2021/january/" target="_blank">January 2021</a></li>
    </ul>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6">
    <h3>2020</h3>
    <ul>
      <li><a href="2020/december/" target="_blank">December 2020</a></li>
      <li><a href="2020/september/" target="_blank">September 2020</a></li>
      <li><a href="2020/august/" target="_blank">August 2020</a></li>
      <li><a href="2020/july/" target="_blank">July 2020</a></li>
      <li><a href="2020/june/" target="_blank">June 2020</a></li>
      <li><a href="2020/may/" target="_blank">May 2020</a></li>
      <li><a href="2020/april/" target="_blank">April 2020</a></li>
      <li><a href="2020/march/" target="_blank">March 2020</a></li>
      <li><a href="2020/february/" target="_blank">February 2020</a></li>
      <li><a href="2020/january/" target="_blank">January 2020</a></li>
    </ul>
  </div>
   <div class="col-md-6">
    <h3>2019</h3>
    <ul>
    	<li><a href="2019/2019December.html" target="_blank">December 2019</a></li>
      <li><a href="2019/2019November.html" target="_blank">November 2019</a></li>
      <li><a href="2019/2019June.html" target="_blank">June 2019</a></li>
    	<li><a href="2019/2019May.html" target="_blank">May 2019</a></li>
  		<li><a href="2019/2019April.html" target="_blank">April 2019</a></li>
    	<li><a href="2019/2019March.html" target="_blank">March 2019</a></li>
    	<li><a href="2019/2019February.html" target="_blank">February 2019</a></li>
    	<li><a href="2019/2019January.html" target="_blank">January 2019</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2018</h3>
    <ul>
    	<li><a href="2018/2018December.html" target="_blank">December 2018</a></li>
    	<li><a href="2018/2018November.html" target="_blank">November 2018</a></li>
     	<li><a href="2018/2018October.html" target="_blank">October 2018</a></li>
      	<li><a href="2018/2018September.html" target="_blank">September 2018</a></li>
      	<li><a href="2018/2018August.html" target="_blank">August 2018</a></li>
   	  	<li><a href="2018/2018July.html" target="_blank">July 2018</a></li>
  	  	<li><a href="2018/2018June.html" target="_blank">June 2018</a></li>
      	<li><a href="2018/2018May.html" target="_blank">May 2018</a></li>
     	<li><a href="2018/2018April.html" target="_blank">April 2018</a></li>
     	<li><a href="2018/2018March.html" target="_blank">March 2018</a></li>
     	<li><a href="2018/2018February.html" target="_blank">February 2018</a></li>
   		<li><a href="2018/2018January.html" target="_blank">January 2018</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2017</h3>
    <ul>
      <li><a href="2017/2017December.html" target="_blank">December 2017</a></li>
      <li><a href="2017/2017November.html" target="_blank">November 2017</a></li>
      <li><a href="2017/2017October.html" target="_blank">October 2017</a></li>
      <li><a href="2017/2017September.html" target="_blank">September 2017</a></li>
      <li><a href="2017/2017August.html" target="_blank">August 2017</a></li>
      <li><a href="2017/2017July.html" target="_blank">July 2017</a></li>
      <li><a href="2017/2017June.html" target="_blank">June 2017</a></li>
      <li><a href="2017/2017May.html" target="_blank">May 2017</a></li>
      <li><a href="2017/2017April.html" target="_blank">April 2017</a></li>
      <li><a href="2017/2017March.html" target="_blank">March 2017</a></li>
      <li><a href="2017/2017February.html" target="_blank">February 2017</a></li>
      <li><a href="2017/2017January.html" target="_blank">January 2017</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2016</h3>
    <ul>
      <li><a href="2016/2016December.html" target="_blank">December 2016</a></li>
      <li><a href="2016/2016November.html" target="_blank">November 2016</a></li>
      <li><a href="2016/2016October.html" target="_blank">October 2016</a></li>
      <li><a href="2016/2016September.html" target="_blank">September 2016</a></li>
      <li><a href="2016/2016August.html" target="_blank">August 2016</a></li>
      <li><a href="2016/2016July.html" target="_blank">July 2016</a></li>
      <li><a href="2016/2016June.html" target="_blank">June 2016</a></li>
      <li><a href="2016/2016May.html" target="_blank">May 2016</a></li>
      <li><a href="2016/2016April.html" target="_blank">April 2016</a></li>
      <li><a href="2016/2016March.html" target="_blank">March 2016</a></li>
      <li><a href="2016/2016February.html" target="_blank">February 2016</a></li>
      <li><a href="2016/2016January.html" target="_blank">January 2016</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2015</h3>
    <ul>
      <li><a href="2015/2015December.html">December 2015</a></li>
      <li><a href="2015/2015November.html">November 2015</a></li>
      <li><a href="2015/2015October.html">October 2015</a></li>
      <li><a href="2015/2015September.html">September 2015</a></li>
      <li><a href="2015/2015August.html">August 2015</a></li>
      <li><a href="2015/2015July.html">July 2015</a></li>
      <li><a href="2015/2015June.html">June 2015</a></li>
      <li><a href="2015/2015May.html">May 2015</a></li>
      <li><a href="2015/2015April.html">April 2015</a></li>
      <li><a href="2015/2015March.html">March 2015</a></li>
      <li><a href="2015/2015February.html">February 2015</a></li>
      <li><a href="2015/2015January.html">January 2015</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2014</h3>
    <ul>
      <li><a href="2014/2014December.html">December 2014</a></li>
      <li><a href="2014/2014November.html">November 2014</a></li>
      <li><a href="2014/2014October.html">October 2014</a> </li>
      <li><a href="2014/2014September.html">September 2014</a> </li>
      <li><a href="2014/2014August.html">August 2014</a> </li>
      <li><a href="2014/2014July.html">July 2014</a> </li>
      <li><a href="2014/2014June.html">June 2014</a> </li>
      <li><a href="2014/2014May.html">May 2014</a> </li>
      <li><a href="2014/2014April.html">April 2014</a> </li>
      <li><a href="2014/2014March.html">March 2014</a> </li>
      <li><a href="2014/2014February.html">February 2014</a></li>
      <li><a href="2014/2014January.html">January 2014</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2013 </h3>
    <ul>
      <li><a href="2013/2013December.html">December 2013</a></li>
      <li><a href="2013/2013November.html">November 2013</a></li>
      <li><a href="2013/2013October.html">October 2013</a></li>
      <li><a href="2013/2013September.html">September 2013</a></li>
      <li><a href="2013/2013August.html">August 2013</a></li>
      <li><a href="2013/2013July.html">July 2013</a></li>
      <li><a href="2013/2013June.html">June 2013</a></li>
      <li><a href="2013/2013May.html">May 2013</a></li>
      <li><a href="2013/2013April.html">April 2013</a></li>
      <li><a href="2013/2013March.html">March 2013</a></li>
      <li><a href="2013/2013February.html">February 2013</a></li>
      <li><a href="2013/2013January.html">January 2013</a></li>
    </ul>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6">
    <h3>2012 </h3>
    <ul>
      <li><a href="2012/2012November.html">November 2012</a></li>
      <li><a href="2012/2012October.html">October 2012</a></li>
      <li><a href="2012/2012September.html">September 2012</a></li>
      <li><a href="2012/2012August.html">August 2012</a></li>
      <li><a href="2012/2012July.html">July 2012</a></li>
      <li><a href="2012/2012June.html">June 2012</a></li>
      <li><a href="2012/2012May.html">May 2012</a></li>
      <li><a href="2012/2012April.html">April 2012</a></li>
      <li><a href="2012/2012March.html">March 2012</a></li>
      <li><a href="2012/2012February.html">February 2012</a></li>
      <li><a href="2012/2012January.html">January 2012</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2011</h3>
    <ul>
      <li><a href="2011/2011December.html">December 2011</a></li>
      <li><a href="2011/2011November.html">November 2011</a></li>
      <li><a href="2011/2011October.html">October 2011</a></li>
      <li><a href="2011/2011September.html">September 2011</a></li>
      <li><a href="2011/2011August.html">August 2011</a></li>
      <li><a href="2011/2011July.html">July 2011</a></li>
      <li><a href="2011/2011June.html">June 2011</a></li>
      <li><a href="2011/2011May.html">May 2011</a></li>
      <li><a href="2011/2011April.html">April 2011</a></li>
      <li><a href="2011/2011March.html">March 2011</a></li>
      <li><a href="2011/2011February.html">February 2011</a></li>
      <li><a href="2011/2011January.html">January 2011</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2010</h3>
    <ul>
      <li><a href="2010/2010December.html">December 2010</a></li>
      <li><a href="2010/2010November.html">November 2010</a></li>
      <li><a href="2010/2010October.html">October 2010</a></li>
      <li><a href="2010/2010September.html">September 2010</a></li>
      <li><a href="2010/2010August.html">August 2010</a></li>
      <li><a href="2010/2010July.html">July 2010</a></li>
      <li><a href="2010/2010June.html">June 2010</a></li>
      <li><a href="2010/2010May.html">May 2010</a></li>
      <li><a href="2010/2010April.html">April 2010</a></li>
      <li><a href="2010/2010March.html">March 2010</a></li>
      <li><a href="2010/2010February.html">February 2010</a></li>
      <li><a href="2010/2010January.html">January 2010</a></li>
    </ul>
  </div>
  <div class="col-md-6">
    <h3>2009</h3>
    <ul>
      <li><a href="2009/2009December.html">December 2009</a></li>
      <li><a href="2009/2009November.html">November 2009</a></li>
      <li><a href="2009/2009October.html">October 2009</a></li>
      <li><a href="2009/2009September.html">September 2009</a></li>
      <li><a href="2009/2009August.html">August 2009</a></li>
      <li><a href="2009/2009July.html">July 2009</a></li>
      <li><a href="2009/2009June.html">June 2009</a></li>
      <li><a href="2009/2009May.html">May 2009</a></li>
      <li><a href="2009/2009March.html">March 2009</a></li>
      <li><a href="2009/2009February.html">February 2009</a></li>
      <li><a href="2009/2009January.html">January 2009</a></li>
    </ul>
  </div>
  <div class="clearfix"></div>
  <div class="col-md-6">
    <h3>2008</h3>
    <ul>
      <li><a href="2008/2008December.html">December 2008</a></li>
      <li><a href="2008/2008November.html">November 2008</a></li>
      <li><a href="2008/2008October.html">October 2008</a></li>
      <li><a href="2008/2008September.html">September 2008</a></li>
      <li><a href="2008/2008August.html">August 2008</a></li>
      <li><a href="2008/2008July.html">July 2008</a></li>
      <li><a href="2008/2008June.html">June 2008</a></li>
      <li><a href="2008/2008May.html">May 2008</a></li>
      <li><a href="2008/2008April.html">April 2008</a></li>
      <li><a href="2008/2008March.html">March 2008</a></li>
      <li><a href="2008/2008February.html">February 2008</a></li>
      <li><a href="2008/2008January.html">January 2008</a></li>
    </ul>
  </div>
</div>
