<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?>
</h1>

<h2>Discover the Open VSX Registry</h2>

<p>Read our new<a
    href="https://outreach.eclipse.foundation/openvsx?utm_campaign=Open%2520VSX%2520White%2520Paper&amp;utm_source=documents%2520page">
    white paper</a> to learn more about the<a href="https://open-vsx.org/"> Open VSX Registry</a>, a vendor neutral,
  open source marketplace for VS Code extensions.</p>

<p>The Open VSX Registry is hosted at the Eclipse Foundation and is being managed under the<a
    href="https://ecdtools.eclipse.org/"> Eclipse Cloud Development (ECD) Tools Working Group</a>. It&rsquo;s based on
  the<a href="https://projects.eclipse.org/projects/ecd.openvsx"> Eclipse Open VSX</a> project, and it delivers on the
  industry&rsquo;s need for a more flexible and open approach to VS Code extensions and marketplace technologies.</p>

<p>The white paper explores the growing momentum around open source developer tools and technologies, the need for an
  open source marketplace, and the unique benefits the Open VSX Registry provides to extension users, extension
  publishers, tool developers, and enterprises.</p>

<p><a
    href="https://outreach.eclipse.foundation/openvsx?utm_campaign=Open%2520VSX%2520White%2520Paper&amp;utm_source=documents%2520page">Download
    the Open VSX White Paper</a>.</p>

<hr />
<h2>Add Your Voice to the Jakarta EE Developer Survey</h2>

<p>The survey results could influence your next Java development decisions. The<a
    href="https://www.surveymonkey.com/r/77HYFNW"> survey is open</a> until May 31 and takes less than 8 minutes to
  complete.</p>

<p>By sharing details about the tools, technologies, and architectures you&rsquo;re using, everyone in the Java
  ecosystem will gain better visibility into:</p>

<ul>
  <li>Which technologies and tools are gaining ground, and which are losing favor. Understanding these trends could lead
    you to explore the benefits of frameworks, runtimes, and tools that deliver capabilities you don&rsquo;t have today
    and can help accelerate your cloud evolution.</li>
  <li>Ongoing strategies to move legacy applications to the cloud and create new cloud native applications from the
    ground up. This visibility could affect your cloud evolution strategy and approach.</li>
</ul>

<p>The survey is also a great opportunity to let the Jakarta EE community know your top priorities for Jakarta EE
  evolution. The community needs this insight to make informed decisions that align as closely as possible with the
  requirements of developers around the world.</p>

<p><a href="https://www.surveymonkey.com/r/77HYFNW">Start the survey now</a>.</p>

<hr />
<h2>Why Jakarta EE Is the Right Choice for Today&rsquo;s Java Applications</h2>

<p>We spoke to leading Java experts globally to explore why they rely on<a href="https://jakarta.ee/"> Jakarta EE</a>
  and captured their insights in this<a
    href="https://outreach.jakartaee.org/white-paper-java-applications?utm_campaign=Jakarta%2520EE%2520White%2520Paper&amp;utm_source=banner">
    white paper</a>.</p>

<p>Jakarta EE is much more than the next iteration of Java EE. It is the doorway to strategic and technical advantages
  that are only possible through the Jakarta EE community.</p>

<p>The experts we spoke to described a combination of benefits that aren&rsquo;t available in any other platform,
  including:</p>

<ul>
  <li>An application framework that can be set up in just a few dozen lines of code.</li>
  <li>A very stable foundation for innovation.</li>
  <li>The freedom and flexibility to choose the optimal development and deployment models.</li>
  <li>The ability to easily meet requirements to support new servers and other underlying technologies.</li>
  <li>An extensive library of carefully thought-out specifications that enable a declarative programming paradigm that
    is efficient, reusable, and scalable.</li>
  <li>An active community of leading organizations and individuals who are advancing Jakarta EE.</li>
</ul>

<p>For more insight,<a
    href="https://outreach.jakartaee.org/white-paper-java-applications?utm_campaign=Jakarta%2520EE%2520White%2520Paper&amp;utm_source=banner">
    download the white paper</a>.</p>

<hr />
<h2>Submit Your EclipseCon 2021 Talk Proposal</h2>

<p>The Call for Proposals for<a href="https://www.eclipsecon.org/2021"> EclipseCon 2021</a> is open until June 15.
  You&rsquo;ll find the information you need to get started, including a detailed submission FAQ, on the<a
    href="https://www.eclipsecon.org/2021/cfp"> CFP webpage</a>.</p>

<p>This year&rsquo;s EclipseCon conference is October 25-28, and will be held online. Sessions will be selected by an<a
    href="https://www.eclipsecon.org/2021/about-program-committee"> independent program committee</a> of community
  volunteers.</p>

<p>If you&rsquo;ve never attended EclipseCon, it&rsquo;s the main annual gathering of the Eclipse community, whether
  it&rsquo;s held online or in person. It&rsquo;s a unique opportunity for all of our communities to get together to
  share ideas, learn from one another, and celebrate accomplishments.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>