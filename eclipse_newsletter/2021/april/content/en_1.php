<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?>
</h1>
<p>IDE and tool developers can now quickly and easily evaluate the functionality<a href="https://theia-ide.org/">
    Eclipse Theia</a> provides and accelerate their own IDE and tool development with<a
    href="https://theia-ide.org/docs/blueprint_download"> Eclipse Theia Blueprint</a>.</p>

<p>Eclipse Theia is a web-based platform for building IDEs and tools for the cloud, browser, or desktop. It is similar
  to VS Code from a feature perspective, but provides<a
    href="https://blogs.eclipse.org/post/mike-milinkovich/eclipse-theia-and-vs-code-differences-explained"> unique
    advantages</a> when building custom IDEs and tools.</p>

<p>There have been a number of requests for a functional example of the Theia platform to allow for capabilities to be
  easily explored without the need to build a full Theia-based product. With Theia Blueprint, this is now possible.</p>

<p>Theia Blueprint is a<a href="https://github.com/eclipse-theia/theia-blueprint"> subproject of the Eclipse Theia
    project</a> that gives developers a first-hand look at the power of the Theia platform and the types of IDEs and
  tools that can be easily built upon it. As the name suggests, Theia Blueprint provides templates to jumpstart
  development of Theia-based IDEs and tools by providing ready-to-try, pre-packaged<a
    href="https://theia-ide.org/docs/blueprint_download/"> blueprints</a> with<a
    href="https://theia-ide.org/docs/blueprint_documentation/"> documentation</a> that describes how to use and
  customize them.</p>

<p><strong>Collaboration in the Eclipse Cloud DevTools Working Group</strong></p>

<p>Theia Blueprint was developed through a collaboration between Arm, STMicroelectronics, and EclipseSource (Figure 1),
  and has recently been contributed to the project.</p>

<p>Figure 1: Theia Blueprint Collaborators</p>

<p><img src="images/1_1.png"></p>

<p>The three companies are members of the<a href="https://ecdtools.eclipse.org/"> Eclipse Cloud DevTools Working
    Group</a>, and are very enthusiastic about Theia, so working together came naturally.</p>

<p>Arm is the leading technology provider of processor intellectual property, offering the widest range of processors to
  address the performance, power, and cost requirements of every device. Arm CPUs and NPUs include Cortex-A, Cortex-M,
  Cortex-R, Neoverse, Ethos, and SecurCore. Arm is using the Theia platform to build tools such as Mbed Studio.</p>

<p>STMicroelectronics manufactures integrated devices that are found wherever microelectronics are used, from autonomous
  vehicles, industrial automation, and smart home and building solutions to embedded computers and personal electronics.
  The company has a strong interest in web-based tooling.</p>

<p>EclipseSource is a founding member of the Eclipse Foundation with a focus on helping customers adopt open source
  technologies. One of EclipseSource&rsquo;s specialties is providing services to companies that are building IDEs,
  tools, and modeling tools <a href="https://eclipsesource.com/technology/eclipse-theia/">based on Theia and related
    technologies</a>.</p>

<p><strong>Theia Blueprint Features</strong></p>

<p>Theia Blueprint provides a Theia-based IDE with a number of features, including:</p>

<ul>
  <li>An application frame, windowing, workspace support, and console</li>
  <li>Code editing and debugging for JavaScript, TypeScript, and Java</li>
  <li>Editing support, including syntax highlighting for common file types, such as HTML, JSON, yml, md, and xml</li>
  <li>Git support</li>
</ul>

<p>Figure 2: Auto-Completion in Theia Blueprint</p>

<p><img class="img-responsive" src="images/1_2.gif"></p>

<p><strong>Try Eclipse Theia on the Desktop, in a Browser, or in the Cloud</strong></p>

<p>The<a href="https://github.com/eclipse-theia/theia-blueprint"> Theia Blueprint source code</a> is published in GitHub
  under the Eclipse Theia project, and is licensed under the<a href="/legal/epl-2.0/"> Eclipse
    Public License</a>. You can download Theia Blueprint from the<a href="https://theia-ide.org/"> Theia webpage</a>.
  Please note that it is currently an alpha release.</p>

<p><strong>Please also note that Theia Blueprint is not a fully polished and tested IDE that is meant to replace VS
    Code, the Eclipse IDE, or any other production IDE.</strong></p>

<p>Depending on your operating system, you&rsquo;ll get a Windows Installer, MacOS .pkg, or Linux AppImage to install
  the Theia binaries. Once the installation is complete, you&rsquo;ll be able to launch a Theia instance as a desktop
  app based on the Electron software framework.</p>

<p>There are also<a href="https://github.com/eclipse-theia/theia-blueprint/issues/74"> plans</a> to provide a Theia
  Blueprint ready-to-try Docker image to showcase Theia Blueprint in a browser.</p>

<p>In addition, several resources are available for testing Theia-based commercial products and demos in the cloud,
  including:</p>

<ul>
  <li><a href="https://developers.redhat.com/products/codeready-workspaces/overview">Red Hat Codeready Workspaces</a>
    based on<a href="/che/getting-started/cloud/"> Eclipse Che</a> and Eclipse Theia</li>
  <li><a href="https://eclipsesource.com/coffee-editor">Coffee editor demo</a> based on<a
      href="/emfcloud/"> Eclipse EMF.cloud</a> and Eclipse Theia</li>
  <li><a href="https://cloud.google.com/blog/products/application-development/introducing-cloud-shell-editor">Google
      Cloud Shell Editor</a> based on Eclipse Theia</li>
</ul>

<p><strong>Customize Theia Blueprint to Meet Your Needs</strong></p>

<p>While Theia Blueprint includes the major Theia features, it&rsquo;s important to remember that it&rsquo;s not a
  production-ready product, but a template you will need to enhance and adapt to your users&#39; needs.</p>

<p>For example, Theia Blueprint includes update functionality, but developers who use that functionality will need to
  configure their own update service. You&rsquo;ll also need to add packages to support development in different
  programming languages. Developers creating IDEs for embedded software development will likely want to add packages for
  C or C++ code editing. This is a quite common use case for Theia, and is found in Arm&rsquo;s Mbed Studio IDE.</p>

<p>Finally, you&rsquo;ll also likely want to add your own branding.</p>

<p>Theia Blueprint provides<a href="https://theia-ide.org/docs/blueprint_documentation/"> documentation</a> that
  explains the different ways you can customize the software to meet your specific requirements.</p>

<p><strong>Get Started and Share Your Feedback</strong></p>

<p>We invite you to<a href="https://theia-ide.org/docs/blueprint_download"> download the Theia Blueprint alpha software
    now</a>. We&rsquo;re very interested in your feedback, so feel free to:</p>

<ul>
  <li><a href="https://github.com/eclipse-theia/theia/issues">Open issues</a> for feature requests or bugs in Theia</li>
  <li><a href="https://github.com/eclipse-theia/theia-blueprint/issues">Open issues</a> for missing features in Theia
    Blueprint</li>
  <li><a href="mailto:theia@eclipsesource.com">Contact EclipseSource</a> with questions about how to build products
    based on Theia</li>
</ul>

<p>If you would like to receive updates about Theia Blueprint, read<a
    href="https://eclipsesource.com/blogs/author/helming_koegel/"> the EclipseSource blog</a>, or follow us on<a
    href="https://twitter.com/eclipsesource"> Twitter</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive"
            alt="<?php print $pageAuthor; ?>"
            src="images/maximilian.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?>
          </p>
          <p class="author-bio">
          Maximilian Koegel is deeply involved in the Eclipse community. He is project lead and committer on several open
  source projects and serves on the<a href="/org/foundation/council.php%23architecture"> Eclipse
    Foundation Architecture Council</a>. Maximilian is general manager at<a href="http://eclipsesource.com/">
    EclipseSource</a>, where he focuses on building pragmatic web- and desktop-based modeling tools.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>