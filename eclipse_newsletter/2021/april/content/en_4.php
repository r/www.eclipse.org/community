<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?>
</h1>
<h2>At a Glance: Philip Langer</h2>

<div style="display:inline;">
  <img width="160" class="float-left margin-right-40 img img-responsive" src="images/philip.jpg" alt="Jason Mehrens">
</div>
<div style="display:inline;">
  <ul>
    <li>Involved in open source since: About 2005</li>
    <li>Works for: EclipseSource</li>
    <li>Eclipse Foundation contributor since: 2014</li>
    <li>Involved in:<a href="https://projects.eclipse.org/projects/ecd.emfcloud"> Eclipse EMF.cloud</a>,<a
        href="/glsp/"> Eclipse GLSP</a>,<a href="https://projects.eclipse.org/projects/ecd.sprotty"> Eclipse
        Sprotty</a>,<a href="/emf/compare/"> Eclipse EMF Compare</a>,<a href="emfstore/"> Eclipse EMFStore</a>,<a
        href="/papyrus-rt/">
        Eclipse Papyrus for Real Time (Papyrus-RT)</a>,<a href="/papyrus/"> Eclipse Papyrus</a>
    </li>
    <li>Committer to: All of the above</li>
    <li>Committer since: 2016</li>
    <li>Fun Fact: Philip&rsquo;s 20-year-old turtle was not impressed when Philip started working from home when the
      pandemic started, but the two now share the home office in harmony, and Koopa is a frequent background guest on
      Philip&rsquo;s video calls.</li>
  </ul>

</div>

<hr />
<h2>Why did you first get involved in open source software communities?</h2>

<p>I started using Linux in about 2005 when I was at the Vienna University of Technology. By 2007, I was working with
  the<a href="/modeling/emf/"> Eclipse Modeling Framework</a>, and later, did my master&rsquo;s
  thesis and PhD based on tools such<a href="/emf/compare/"> Eclipse EMF Compare</a> and other
  projects related to the Eclipse Modeling Framework. At that time, I was primarily a user of the frameworks.</p>

<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>

<p>The frameworks are pretty complex, so I needed to look at the source code. I became more and more interested in open
  source because I realized that the ability to look at the source code and adapt it to your needs helps you use those
  frameworks more efficiently.</p>

<p>Over time, I developed a better understanding of how the frameworks function and was able to provide feedback on bug
  reports, and even contribute small fixes.</p>

<p>When I joined EclipseSource in 2014, I started contributing to the Eclipse modeling projects on a regular basis.
  These contributions gave me the experience needed to become a committer.</p>

<h2>How would you summarize your experiences as a committer?</h2>

<p>The most rewarding thing is having the opportunity to evolve and shape software that&rsquo;s used by so many other
  people. You can really have an impact.</p>

<p>It&rsquo;s also very rewarding to receive constructive feedback from highly talented and experienced peers in the
  open source community. You&rsquo;re collaborating and getting new ideas from people all over the world, and
  that&rsquo;s pretty cool.</p>

<p>With frameworks, you always have to balance how generic the framework remains while considering complexity for
  developers and extensibility.</p>

<p>We collaborate and have discussions in a very open manner to find the right compromises between specific
  functionality and the complexity and maintainability in the long run. It&rsquo;s challenging, but it&rsquo;s also what
  makes open source great. It&rsquo;s very rewarding when you collaborate with people who have the same ideals and goals
  as you do, and find the right balance.</p>

<p>It&rsquo;s often surprising to learn how people actually use the software you&rsquo;re working on. But it&rsquo;s a
  very positive thing because you learn about use cases you never would have thought would be a good fit for that
  framework.&nbsp;</p>

<h2>What are your next steps and goals as a committer and Eclipse Foundation community member?</h2>

<p>My main focus is on the Eclipse cloud development projects, such as<a href="/glsp/"> Eclipse
    GLSP</a>,<a href="https://projects.eclipse.org/projects/ecd.sprotty"> Eclipse Sprotty</a>,<a
    href="https://projects.eclipse.org/projects/ecd.emfcloud"> Eclipse EMF.cloud</a>, and<a
    href="https://projects.eclipse.org/projects/ecd.theia"> Eclipse Theia</a>. I think there&rsquo;s great potential to
  make existing EMF technologies available and accessible in modern technology stacks and user interfaces so they can be
  used in cloud applications.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the
  Eclipse Foundation?</h2>

<p>In my early days as a contributor, I was excited, but also a bit anxious to put myself out there in public. But
  I&rsquo;ve always received very encouraging and helpful feedback that made my contributions, and the overall project,
  better in the end.</p>

<p>I really recommend that people start engaging with the projects they use. Your efforts will be very much appreciated,
  and you&rsquo;ll learn a lot as a developer because you&rsquo;ll receive feedback from very talented and experienced
  people. You&rsquo;ll also develop many friendships.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>