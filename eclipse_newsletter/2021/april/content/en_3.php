<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?>
</h1>
<p>The<a href="https://github.com/eclipse-theia/theia-cpp-extensions/pull/119"> Eclipse Theia Memory Inspector</a>
  replaces the current<a href="https://github.com/eclipse-theia/theia-cpp-extensions"> Theia cpp-debug-extension</a>,
  and it may well deliver one of the best memory inspection experiences available in any IDE.</p>

<p>While our perspective may come with some bias, our goal was to create a memory viewer that&rsquo;s best-in-class in
  as many areas as possible. With this goal in mind, we took the best memory inspection features from various IDEs and
  combined them with other enhancements to provide a memory widget that&rsquo;s quite unique.</p>

<p>Here are six reasons you should give the Theia Memory Inspector a try.</p>

<p><strong>1. Dynamic Memory Views</strong></p>

<p>Dynamic views allow for a superior developer experience. Most memory inspector tools implement static views, which
  require a manual refresh each time you want to update the view as you step through code.&nbsp;</p>

<p>In contrast, the Theia Memory Inspector allows you to see changes as they occur. Memory locations that have changed
  from one frame to the next are highlighted (Figure 1). You can freeze a view of a particular frame by clicking the
  lock icon.</p>

<p>&nbsp;Figure 1: Dynamic Updates of Memory Views</p>

<p><img class="img-responsive" src="images/3_1.gif"></p>

<p>You can also generate multiple memory views to compare memory at different times or memory from different regions.
  This is particularly helpful when you&rsquo;re copying or moving data from one region to another and need to verify
  the operation is occurring as it should.</p>

<p>To make it more convenient to visually compare memory, the Memory Inspector provides a memory diff widget. When you
  create two memory views and choose the compare option, the widget aligns the views side by side and highlights the
  differences, similar to a diff view in Git (Figure 2).</p>

<p>Figure 2: Diffing Memory</p>

<p><img class="img-responsive" src="images/3_2.png"></p>

<p><strong>2. Viewing Options and Accessibility</strong></p>

<p>The Memory Inspector displays bytes in hex format, but also provides the option to view memory in ASCII format.
  Hovering over a specific value reveals even more representations including binary, decimal, and even UTF8 (if
  available). Variables in scope are colored so it&rsquo;s easier to distinguish what you&rsquo;re looking at. These
  visual markers greatly improve readability (Figure 3).</p>

<p>Figure 3: Different Representations of Memory</p>

<p><img class="img-responsive" src="images/3_3.png"></p>

<p>To support developers who are color-blind, we added a high contrast theme. If you&rsquo;re using a high contrast
  theme in Theia, you&rsquo;ll see contrasting colors when looking at the variables in the Memory Inspector (Figure 4).
</p>

<p>Figure 4: High Contrast Theme</p>

<p><img class="img-responsive" src="images/3_4.png"></p>

<p><strong>3. Fast and Easy Setup With a Simplified Configuration</strong></p>

<p>Our primary goal is to help developers get into the memory as quickly and easily as possible. As a result, the Memory
  Inspector provides multiple entry points to directly access a memory view, including right-clicking a variable in the
  debugger pane and explicitly entering an address or expression in the Memory Inspector&rsquo;s input field.</p>

<p>To further simplify usability, the Memory Inspector provides a minimalist interface for configuring basic viewing
  parameters, as well as a more advanced menu for specific configuration, such as byte size and endianness (Figure 5).
</p>

<p>Figure 5: Memory View Configuration</p>

<p><img class="img-responsive" src="images/3_5.png"></p>

<p><strong>4. Dockable Panels and Flexible Views</strong></p>

<p>Modern IDEs, such as those built using<a href="https://theia-ide.org/"> Theia</a>, provide a minimalist user
  interface (UI) rather than heavy toolbars and cluttered UI elements. We&rsquo;ve followed this paradigm in the Memory
  Inspector UI (Figure 6).</p>

<p>Figure 6: Moving Memory Views Around</p>

<p><img class="img-responsive" src="images/3_6.gif"></p>

<p>To take maximum advantage of horizontal screen real estate, the Memory Inspector consists of a dockable tab bar that
  is displayed in the right-most column of the IDE by default. If you have multiple memory views open, you can
  &ldquo;tear&rdquo; each one from the panel, resize it, and place it where it&rsquo;s most convenient in your screen
  layout. If you&rsquo;re reviewing a large memory chunk, you likely need a large memory inspection panel. At other
  times, you may just want to tuck the panel into a corner of your screen.</p>

<p>The flexibility to tailor the number and size of individual Memory Inspector views comes from Theia. We simply
  applied it to our widget. This point is worth emphasizing because many IDEs have rigid layouts with predefined areas
  to host certain types of views. There are often limits to where you can place a view. Theia frees you to place a
  memory view anywhere on the screen.<strong>&nbsp;</strong></p>

<p><strong>5. Easy Functionality Overriding</strong></p>

<p>Another important capability the Memory Inspector inherits from Theia is the ability to override almost anything.</p>

<p>For example, it&rsquo;s quite easy to extend the Memory Inspector to work with a debug adapter other than the CDT GDB
  tooling it&rsquo;s paired with now. It&rsquo;s simply a matter of extending and modifying one class in the Memory
  Inspector code.</p>

<p><strong>6. A Drop-In Extension for Eclipse Theia-Based IDEs</strong></p>

<p>Anyone building a Theia-based IDE and using a debugger that reads memory can add the Memory Inspector as a dependency
  and it will be available in the IDE.</p>

<p>Bringing the Memory Inspector, with all of its features for viewing memory, into Theia-based IDEs for embedded
  software development is a particularly powerful combination.</p>

<p><strong>Try the Memory Inspector and Provide Feedback</strong></p>

<p>We believe the Eclipse Theia Memory Inspector offers a lot of potential to developers, and we&rsquo;re eager to get
  feedback about concrete use cases.</p>

<p>To see the pull request and documentation for the Memory Inspector,<a
    href="https://github.com/eclipse-theia/theia-cpp-extensions/pull/119"> go to GitHub.</a> To report bugs and add
  feature requests, use the<a href="https://github.com/eclipse-theia/theia-cpp-extensions/issues"> Issues page</a>.</p>

<p>We also welcome developers to join us in enhancing the Memory Inspector, especially those with a particular feature
  they would like to add.</p>

<p>For additional insight into the Memory Inspector and its features,<a
    href="https://blogs.eclipse.org/post/brian-king/memory-inspector-debugger-theia"> read our blog post</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive"
            alt="Colin Grant"
            src="images/colin.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Colin Grant
          </p>
          <p class="author-bio">Colin Grant is a web technologies developer at Ericsson and a committer to the Eclipse Theia project.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
  <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
            <img class="img img-responsive"
            alt="Kenneth Marut"
            src="images/kenneth.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Kenneth Marut
          </p>
          <p class="author-bio">Kenneth Marut is a web technologies developer at Ericsson and a committer to the Eclipse Theia project.</p>
        </div>
      </div>
    </div>
  </div>
</div>