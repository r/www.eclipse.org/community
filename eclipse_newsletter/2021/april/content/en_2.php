<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
    exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?>
</h1>
<p>Brian King, the Cloud DevTools community manager at the Eclipse Foundation, explains why the<a
    href="https://ecdtools.eclipse.org/"> Eclipse Cloud DevTools Working Group</a> has created the Foundation&rsquo;s
  first special interest group (SIG), who is involved, and what they hope to achieve.</p>

<p><strong>Q. What drove the need for a special interest group (SIG) within the Eclipse Cloud DevTools Working
    Group?</strong></p>

<p><strong>A.</strong> Over the last year or two, we received numerous inquiries about cloud development tools from
  embedded tool vendors. Most of these vendors are still invested in traditional desktop tools, such as the<a
    href="/eclipseide/"> Eclipse IDE</a>, but they&rsquo;re exploring their strategy and their
  entry point to cloud development for embedded software.</p>

<p>We saw a fantastic opportunity to bring these organizations together as a special interest group (SIG) to collaborate
  on technical initiatives within the Cloud DevTools working group for embedded software development. Because group
  members have a shared area of focus, the SIG concept lends itself to much closer collaboration than it does at the
  broader working group level.</p>

<p><strong>Q. What is the relationship between the SIG and the Eclipse Cloud DevTools Working Group?</strong></p>

<p><strong>A.</strong> For starters, the work done in the SIG must be consistent with the working group charter.
  We&rsquo;ve established the embedded SIG with a &ldquo;trial run&rdquo; model. Organizations can get involved and talk
  with all of the other interested parties during a three-month incubation period. At the end of the three-month period,
  they can either officially join the Eclipse Cloud DevTools Working Group and continue in the SIG, or drop out
  gracefully.</p>

<p>It&rsquo;s been a successful model. So far, three companies involved in the SIG &mdash; <a
    href="https://www.arm.com/">Arm</a>, <a href="https://www.st.com/content/st_com/en.html">STMicroelectronics</a>, and
  <a href="https://www.renesas.com/us/en">Renesas</a> &mdash; have become members of the working group. The other two
  participants &mdash; <a href="https://eclipsesource.com/">EclipseSource</a> and <a
    href="https://www.ericsson.com/en">Ericsson</a> &mdash; were already members of the working group.
</p>

<p><strong>Q. What stage is the SIG at in its development?</strong></p>

<p><strong>A. </strong>We had an open kickoff call in late February with representatives from eight or nine
  organizations, so the group is just getting started. It&rsquo;s a great time for additional organizations to get
  involved because they can really help shape the direction and activities of the group. The group just nominated its
  first chairperson &mdash; Rob Moran from Arm &mdash; who will set up meetings, create the agenda, and decide on the
  work items.</p>

<p>During the kickoff meeting, we had a couple of demos, and those were real &ldquo;aha&rdquo; moments for everyone on
  the call because there was a perception among some that cloud software in this domain was vaporware. These are
  real-world applications that are already being used.</p>

<p>Arm demonstrated its <a href="https://os.mbed.com/studio/">Mbed Studio</a> product, which is built on Eclipse Theia.
  It&rsquo;s currently available as a desktop application, but is being rebranded and will also run in-browser. And
  Ericsson demonstrated a <a href="https://github.com/eclipse-theia/theia-cpp-extensions/pull/119">memory inspection
    tool</a>, which is a very important capability in that environment. It really showed that things are happening in
  the embedded tools industry.</p>

<p><strong>Q. What are the group&rsquo;s goals?</strong></p>

<p><strong>A. </strong>The primary goal is to accelerate the move from desktop tools for embedded development to
  cloud-hosted tools. As part of this effort, the group will develop and promote technical standards to help drive
  innovation in the embedded tool ecosystem. This will help to create a foundational layer for cloud development tools
  upon which embedded tool vendors can innovate.</p>

<p>By collaborating, the group can bring these foundational pieces to market faster to accelerate their momentum and
  adoption. They can also help to identify and fill gaps in the Eclipse Cloud DevTools ecosystem that are specific to
  embedded environments.</p>

<p><strong>Q. Has a tactical approach to achieve these goals been developed at this point?</strong></p>

<p><strong>A.</strong> The starting point is Eclipse Theia. It&rsquo;s a mature technology, it&rsquo;s widely used in
  the marketplace, and it runs in desktop and cloud environments. So, it provides a great option for embedded tool
  vendors. The first steps are to ensure Theia offers basic embedded developer functionality and can be extended for
  device-specific features.</p>

<p>To accomplish this, a new code stream called <a href="https://theia-ide.org/docs/blueprint_download">Theia
    Blueprint</a> has been created from Theia. Vendors can use the Theia Blueprint code as a template application and
  foundation to build their own software for their own use case. They can then feed that experience back into the group
  as the basis for discussion around additional features and APIs that are needed and protocols that should be
  standardized.</p>

<p>These efforts will give embedded tool vendors a more mature project to use as the basis for commercial tools.</p>

<p><strong>Q. Are there other aspects of embedded cloud development tools that are likely to be addressed in the
    SIG?</strong></p>

<p><strong>A. </strong>Naturally, the conversation and focus areas will evolve as the group wants them to. But another
  likely focus area is determining the most efficient way for organizations to invest in cloud- and web-based embedded
  development tools while they&rsquo;re still investing in desktop solutions. What does the migration path to the cloud
  look like? There&rsquo;s going to be a long tail for that migration because these organizations will still have many
  customers using their current products as they evolve to the cloud.</p>

<p><strong>Q. How can organizations learn more about the SIG and get involved?</strong></p>

<p><strong>A. </strong>Those who are interested can read the<a
    href="https://docs.google.com/document/d/1Rk4t8kcUmIOx9jiGqQ4YbuU0Vh1DiaaGUOAW-mH_oi8/edit"> proposal document</a>
  that describes how the group functions. To sign up or learn more about the SIG, fill out<a
    href="https://forms.gle/PuSsR1XHakjTbn756"> this form</a>.</p>

<p>We encourage everyone to join the conversation, even if they&rsquo;re not currently an Eclipse Foundation member.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive"
            alt="<?php print $pageAuthor; ?>"
            src="images/brian.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?>
          </p>
          <p class="author-bio">
            The Community Manager for Cloud Development Tools at the Eclipse Foundation, Brian King, is responsible for
            driving community growth, adoption and evolution around open source cloud-based development tools. Working
            closely with members and developers from Red Hat, IBM, Intel, SAP and other industry giants, Brian leads
            strategic planning, organizational decision making, and new project execution for one of the industry’s
            fastest-growing working groups.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>