<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Kevin Herron</h2>

<div style="display:inline;">
<img width="220" class="float-left margin-right-40 img img-responsive" src="images/kevin_herron.jpg" alt="Kevin Herron">
</div>
<div style="display:inline;">
<ul>
  <li>Involved in open source since: Approximately 2005</li>
  <li>Works for: Inductive Automation, which develops web-based industrial automation software</li>
  <li>Eclipse Foundation contributor since: 2016</li>
  <li>Involved in: <a href="https://projects.eclipse.org/projects/iot.milo">Eclipse Milo</a>, <a href="https://sparkplug.eclipse.org/">Eclipse Sparkplug Working Group</a>, <a href="https://projects.eclipse.org/projects/iot.sparkplug">Sparkplug project</a></li>
  <li>Committer to: Eclipse Milo, Sparkplug</li>
  <li>Committer since: 2016</li>
  <li>Fun fact: Kevin is a former cross-country mountain bike racer, now enjoys overnighting and camping, and is learning to play piano. He works on open source software as a hobby as well as at work.</li>
</ul>
</div>

<hr />
<h2>Why did you first get involved in open source software communities?</h2>
<p>When I was in college, I started using Linux. As I got more into programming, I started looking for code other people had written so I could learn from it, and I ended up using different kinds of open source software.</p>
<p>When I started working professionally, I realized we were actually using a lot of open source software as well, and I thought it would be kind of neat to give back. So, I started filing bug reports and submitting fixes to the projects we were using.</p>

<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>
<p>In 2014, I started writing open source code that implements the OPC Unified Architecture (UA) Specification for industrial automation. Previously, we had developed a closed source, partial implementation of the specification, but I had convinced my employer we should develop the code as open source.</p>
<p>In 2016, the Eclipse Foundation contacted me because they thought the technology would be a good fit with the <a href="https://iot.eclipse.org/">Eclipse IoT community</a>. Because I built the software from the ground up, I was immediately a committer when it became the <a href="https://projects.eclipse.org/projects/iot.milo">Eclipse Milo</a> project.</p>

<h2>How would you summarize your experiences as a committer?</h2>
<p>The most rewarding thing is discovering that someone found software I created to be useful. I really like hearing from people who are using Eclipse Milo. I’ve been very surprised to see how many people are using Milo and the size of the companies. Just from emails I’ve received and looking at GitHub analytics, I’ve learned that Amazon, SAP, and Samsung are all using Eclipse Milo.</p>
<p>The most challenging thing about being a committer is managing the bug reports. The OPC UA specification is huge and very complicated. If people don’t already have experience with it, they sometimes want to use the issue reporting section of the Eclipse Milo project as a tech support forum. I try to help them along, but I often can’t provide as much support as they’re looking for.</p>

<h2>
What are your next steps and goals as a committer and Eclipse Foundation community member?
</h2>
<p>My biggest upcoming goal is to release Eclipse Milo 1.0. I would also be very happy to grow the number of contributors to the project. The ideal contributor is likely from the industrial automation industry and has some experience working with the OPC UA Specification.</p>
<p>Also, I’m just starting to work with the <a href="https://sparkplug.eclipse.org/">Eclipse Sparkplug Working Group</a>. I think they’re expecting me to be a contributor and committer to the Java implementation of <a href="https://projects.eclipse.org/projects/iot.sparkplug">Sparkplug</a> at some point.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>
<p>I would say getting involved is totally worth it. I think it’s really important to give back to the open source ecosystem because that’s the way the ecosystem works. It survives when everyone contributes back to it. Just talk to the people involved with the project and find out what they need. I think they’ll be happy to hear from you.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
