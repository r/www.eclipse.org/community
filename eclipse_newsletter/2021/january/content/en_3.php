<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2>We&rsquo;re Now the Eclipse Foundation AISBL</h2>

<p>Our transition to Europe is complete, and we&rsquo;re now formally established as the Eclipse Foundation AISBL, an international non-profit association based in Brussels, Belgium.</p>

<p>With this transition, we see a huge opportunity to build on our strength in Europe to advance open source innovation globally. Europe has embraced open source software in a way no other region of the world has. The Eclipse Foundation has more than 170 members and more than 900 committers based in Europe, and we&rsquo;re already home to a number of publicly funded European <a href="https://www.eclipse.org/org/research/">research projects</a>.</p>

<p>To help everyone understand what our move to Europe means to them, we&rsquo;ve created a number of resources:</p>

<ul>
	<li><a href="https://www.eclipse.org/europe/faq.php">Frequently asked questions</a></li>
	<li>A free white paper called <a href="https://www.eclipse.org/europe/documents/eu-opensource-whitepaper-jan2021.pdf">Enabling Digital Transformation in Europe Through Global Open Source Collaboration</a></li>
	<li>An <a href="https://www.eclipse.org/europe/eclipse-slide-deck.pdf">overview presentation</a> about the Eclipse Foundation AISBL</li>
</ul>

<p> Eclipse Foundation Executive Director Mike Milinkovich has also published blogs on the topic. His most recent blogs:</p>

<ul>
	<li><a href="https://blogs.eclipse.org/post/mike-milinkovich/welcome-eclipse-foundation-aisbl">Announce the formal transition and provide more insight into the rationale behind the move.</a></li>
	<li><a href="https://blogs.eclipse.org/post/mike-milinkovich/eclipse-foundation%E2%80%99s-move-europe-membership-impacts">Summarize the impact of the move on Eclipse Foundation members.</a></li>
</ul>

<h2>The IoT and Edge Commercial Adoption Survey Needs Your Input</h2>

<p>If you&rsquo;re deploying or using commercial IoT and edge computing solutions, please complete our <a href="https://www.surveymonkey.com/r/2021CommunityNewsletter">2021 IoT and Edge Commercial Adoption Survey</a> before February 28.</p>

<p>The IoT and Edge Commercial Adoption Survey goes beyond our IoT Developer Survey to provide insight into the overall IoT industry landscape. With your input, all IoT and edge ecosystem stakeholders &mdash; software vendors, platform vendors, solution providers, and manufacturing organizations &mdash; will have deeper insight into:</p>

<ul>
	<li>Forecasts for IoT and edge market growth</li>
	<li>Challenges and potential barriers that impact market development and size</li>
	<li>The latest IoT and edge computing market trends</li>
	<li>The mix of proprietary and open source solutions being used in IoT solutions and where open source software provides key benefits</li>
	<li>How edge computing is being incorporated into IoT solutions</li>
	<li>Strategies other companies are using to increase their IoT footprint</li>
</ul>

<p>The survey results will also influence <a href="https://iot.eclipse.org/">Eclipse IoT</a> and <a href="https://edgenative.eclipse.org/">Edge Native Working Group</a> roadmaps, helping to ensure these ecosystems remain focused on the top requirements and priorities for commercial IoT solutions.</p>

<p><a href="https://www.surveymonkey.com/r/2021CommunityNewsletter">Start the survey now</a></p>

<h2>The Obeo Member Case Study Is Out Now</h2>

<p><a href="https://outreach.eclipse.foundation/obeo-open-source-case-study">Read our latest member case study</a> to learn how Obeo has accelerated growth and increased its profile as a Strategic Member of the Eclipse Foundation.</p>

<p>In 2009, just two years after Obeo joined the Eclipse Foundation, the company took a bold step and became a Strategic Member with a representative on the Eclipse Foundation Board of Directors.</p>

<p>Obeo CEO, C&eacute;dric Brun, says the reputation the company built by participating in the Eclipse Foundation has been key to the company&rsquo;s growth, particularly its growth outside of France. &ldquo;Our involvement in the Eclipse Foundation helped us to be recognized in our technical niche in just a few years,&rdquo; Brun says.</p>

<p><a href="https://outreach.eclipse.foundation/obeo-open-source-case-study">Read the Obeo case study.</a></p>

<p>If you missed our earlier member case studies, use the links below to download them:</p>

<ul>
	<li><a href="https://outreach.eclipse.foundation/payara-services-open-source-case-study">Payara Services Gains an Equal Footing With Industry Leaders</a></li>
	<li><a href="https://outreach.eclipse.foundation/cedalo-iot-open-source-case-study">Cedalo Finds New Opportunities in the Eclipse IoT Ecosystem</a></li>
</ul>

<p>To share your open source success story, email <a href="mailto:marketing@eclipse.org">marketing@eclipse.org</a>.</p>

<h2>Top Webinars of 2020</h2>

<p>With more than 50 Eclipse webinars in the past year, it&rsquo;s possible you missed some valuable content. Don&rsquo;t worry, the recordings are available on our <a href="https://www.youtube.com/user/EclipseFdn/playlists?view=50&amp;sort=dd&amp;shelf_id=1">YouTube channel</a>.</p>

<p>To help you choose among the many webinars community members presented last year, our events specialist, Hudson Kelly, has published a series of blogs highlighting the top talks of 2020.</p>

<p>Follow the links below to read the blogs and watch the webinars:</p>

<ul>
	<li><a href="https://blogs.eclipse.org/post/hudson-kelly/top-3-jakarta-tech-talks-2020">The Top 3 Jakarta Tech Talks of 2020</a></li>
	<li><a href="https://blogs.eclipse.org/post/hudson-kelly/virtual-eclipse-community-meetups-2020-year-review">Virtual Eclipse Community Meetups: 2020 Year in Review</a></li>
	<li><a href="https://blogs.eclipse.org/post/hudson-kelly/cloud-tool-time-webinar-series-cloud-development-tools">Cloud Tool Time: A Webinar Series for Cloud Development Tools</a></li>
	<li><a href="https://blogs.eclipse.org/post/hudson-kelly/virtual-iot-meetups-year-review">Virtual IoT Meetups: 2020 Year in Review</a></li>
</ul>

<p>If you have a project or topic you&rsquo;d like to present to the broader community, reach out to us at <a href="mailto:events@eclipse-foundation.org">events@eclipse-foundation.org</a>. We&rsquo;d be happy to help you set up and promote your webinar.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>