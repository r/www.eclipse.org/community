<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The <a href="https://open-vsx.org/">Open VSX Registry</a> is a vendor-neutral, open source alternative to the Microsoft Visual Studio Marketplace for VS Code extensions. It&rsquo;s built on the <a href="https://projects.eclipse.org/projects/ecd.openvsx">Eclipse Open VSX</a> project, and delivers on the industry&rsquo;s need for a more flexible and open approach to VS Code extensions and marketplace technologies.</p>

<p>As of December 2020, the Open VSX Registry is hosted at the Eclipse Foundation and is being managed under the <a href="https://ecdtools.eclipse.org/">Eclipse Cloud Development (ECD) Tools Working Group</a>.</p>

<p>Tim DeBoer, Chair of the ECD Tools Working Group Steering Committee, says hosting the Open VSX Registry at the Eclipse Foundation is a logical move.</p>

<p>&ldquo;The Eclipse Open VSX project is already fostered within the ECD Tools ecosystem. And, as the number of extensions in the Open VSX Registry grows, we expect the marketplace to play an increasingly important role for developers who rely on open source cloud development tools,&rdquo; DeBoer says.</p>

<h2>Open Source Developer Tools Need an Open Source Marketplace</h2>

<p>In contrast to the Visual Studio Marketplace that licenses free extensions only for use with Microsoft VS products, with the Open VSX Registry, developers have free and open access to VS Code extensions that can be used with any technology or tool that supports them. In addition, because the Eclipse Open VSX source code is open to all, anyone can adopt, adapt, and enhance the marketplace technology to meet their specific needs.</p>

<p>The Open VSX Registry is already the default extension registry for <a href="https://theia-ide.org/">Eclipse Theia</a>, Gitpod, and VSCodium. And it can be easily added as the default extension registry in forks of VS Code.</p>

<p>According to Sven Efftinge, project co-lead for Eclipse Theia and Eclipse Open VSX, platforms such as Eclipse Theia simply don&rsquo;t make sense without the ability to add extensions.</p>

<p>&ldquo;It makes no difference that the software is open source if it can&rsquo;t be used by downstream adopters and inventors simply because they can&rsquo;t install extensions,&rdquo; Efftinge says. &ldquo;Besides, it would be a very boring landscape if no one tries to innovate on top of what Microsoft does.&rdquo;</p>

<h2>Extension Users Have a Familiar and Intuitive User Interface to Find Extensions</h2>

<p>If you&rsquo;re already using the Open VSX Registry, the technology&rsquo;s transition to the Eclipse Foundation has no impact on the way you access or use extensions.</p>

<p>If you&rsquo;re accessing extensions through the Open VSX Registry for the first time, you&rsquo;ll notice it&rsquo;s very similar to the Visual Studio Marketplace from a usability perspective (Figure 1). Icons identify extensions and publishers, and star ratings are provided for each extension. Extensions can be found by name, tag, or description, filtered by category, and sorted by various criteria.</p>

<p><span style="font-size:12px">Figure 1: Open VSX Registry</span></p>
<img class="img img-responsive" src="images/a1_p1.png" alt="Figure 1: Open VSX Registry">

<h2>Extension Publishers Must Sign a Publisher Agreement and Choose a License</h2>

<p>With the transition to the Eclipse Foundation, all Open VSX Registry extension publishers &mdash; even those who published extensions before the transition &mdash; must create an Eclipse Foundation account and sign a publisher agreement.</p>

<p>In addition, all extensions in the Open VSX Registry must be licensed under the license of your choice. To identify the license under which you&rsquo;re publishing the extension, simply add the license identifier to the extension manifest (package.json) file. If you don&rsquo;t identify a license, you&rsquo;ll have the opportunity to choose the default <a href="https://opensource.org/licenses/MIT">MIT license</a> during the publishing process.</p>

<p>We&rsquo;ve made the publishing process as straightforward as possible, and provided the steps in the <a href="https://github.com/eclipse/openvsx/wiki/Publishing-Extensions#how-to-publish-an-extension">Open VSX wiki</a>.</p>

<p>Previous publishers will also find that the extension ownership model has changed since the transition to the Eclipse Foundation. The <a href="https://github.com/eclipse/openvsx/wiki/Namespace-Access">Namespace Access wiki page</a> provides the latest policy details and describes the new process to claim extension ownership.</p>

<h2>Organizations Can Create Their Own Internal Extension Registry</h2>

<p>The source code for the Open VSX Registry is available in <a href="https://github.com/EclipseFdn/open-vsx.org">GitHub</a> so organizations can create an in-house registry that&rsquo;s customized for the extensions their developers use and their internal processes. Organizations can also connect their in-house extension registry to the upstream public Open VSX Registry so their developers can easily access private and public extensions (Figure 2).</p>

<p><span style="font-size:12px">Figure 2: An Internal Extension Registry Connected to Public Registry</span></p>
<img class="img img-responsive" src="images/a1_p2.png" alt="Figure 2: An Internal Extension Registry Connected to Public Registry">

<h2>Tool Developers Can Add Open VSX Registry Access to Their Software</h2>

<p>To make it easy for tool developers to integrate access to the Open VSX Registry into any IDE, desktop tool, or online tool, a REST API provides endpoints for creating namespaces, publishing, querying metadata, and searching.</p>

<p>Miro Spönemann, co-lead of the Eclipse Open VSX project, points out this is the first time tool developers outside of Microsoft can integrate access to VS Code extensions into their solutions.</p>

<p>&ldquo;With this public API, any developer can provide their users with access to VS Code extensions,&rdquo; Spönemann says. &ldquo;This is a significant new capability that will help developers add value to their solutions.&rdquo;</p>

<h2>Learn More</h2>

<p>Take a few minutes to explore the more than 900 extensions in the <a href="https://open-vsx.org/">Open VSX Registry</a>. Once you&rsquo;re on the site, you&rsquo;ll find easy access to detailed documentation, an FAQ, the community conversation on Gitter, and more.</p>

<p>For details about the Eclipse Open VSX project and access to the source code for the Open VSX Registry, <a href="https://projects.eclipse.org/projects/ecd.openvsx">visit the project page</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/jan_kohnlein.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Dr. Jan Köhnlein is a co-founder of TypeFox GmbH. TypeFox is a major contributor to the Eclipse Open VSX project. The company also developed and hosted the Eclipse Open VSX Registry before its transition to the Eclipse Foundation.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>