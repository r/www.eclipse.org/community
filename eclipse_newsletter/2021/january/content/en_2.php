<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Just before Christmas 2020, Open Robotics announced the <a href="https://projects.eclipse.org/projects/iot.cyclonedds">Eclipse Cyclone DDS</a> project had been <a href="https://discourse.ros.org/t/ros-2-galactic-default-middleware-announced/18064">selected by the Robot Operating System (ROS) 2 Technical Steering Committee</a> to be the default middleware in the ROS 2 Galactic Geochelone release. The announcement was an important milestone on the long road from the origins of the Data Distribution Service (DDS) concept and technology to today and the promising future of the Eclipse Cyclone DDS implementation.</p>

<p>I&rsquo;ll start with some history.</p>

<p>I&rsquo;ve been working with DDS technology for about 25 years, and in many ways, I&rsquo;m working in the family business. The first versions of the technology that later became the Object Management Group (OMG) DDS standard were developed by my father, Maarten Boasson, in the early 1980s when he worked in the defense industry. So, we&rsquo;re actually working with a method of building systems that&rsquo;s about 40 years old.</p>

<p>Development on the Eclipse Cyclone DDS implementation started about 10 years ago as part of the ADLINK OpenSplice DDS product, and in 2018, we made the initial commit to the Eclipse Cyclone DDS project.</p>

<p>To help you understand more about Eclipse Cyclone DDS, how we got to the Open Robotics announcement, and our goals for the future, here are answers to the questions I&rsquo;m most often asked.</p>

<h2>What Role Does DDS Play in ROS 2?</h2>

<p>Robots are built with many components and nodes that need to exchange messages, data, and parameters. The DDS implementation is the glue that interconnects all of these nodes and components so they can discover one another and communicate.</p>

<p>With a DDS implementation as the middleware layer, robot developers, who are typically not software engineers, don&rsquo;t have to work directly with DDS, which is a complex standard. They can simply select the middleware they want to use with ROS 2 and the rest is taken care of.</p>

<p>Figure 1 illustrates where Eclipse Cycle DDS fits in the ROS 2 architecture.</p>

<p><span style="font-size:12px">Figure 1: Eclipse Cyclone DDS in the ROS 2.0 Architecture</span></p>
<img class="img img-responsive" src="images/a2_p1.png" alt="Figure 1: Eclipse Cyclone DDS in the ROS 2.0 Architecture">

<h2>How did Eclipse Cyclone DDS Become the Default ROS 2 Middleware?</h2>

<p>Open Robotics was rewriting the original ROS software and had decided to base it on DDS. So, I developed a first, very minimalist implementation of support for ROS 2 using Eclipse Cyclone DDS. I then announced at a conference that this minimalist implementation performed much better than the implementations most people were using at the time. That got the ball rolling. By the time the next conference came along, our implementation included a lot more functionality.</p>

<p>My colleague, Joe Speed, did a tremendous job evangelizing the merits of Eclipse Cyclone DDS and getting robotics companies to switch out the original ROS 2 default middleware and try it. Most were very happy, and many wrote blog posts and tweets about their experience:</p>

<ul>
	<li>Rover Robotics, which makes small, rugged autonomous vehicles, is a great example. They tried five DDS implementations using Wi-Fi and made a <a href="https://blog.roverrobotics.com/navigation2-now-were-getting-somewhere/">video series about their experience</a>. In the first video, the Eclipse Cyclone DDS implementation had the system up and running in 11 seconds. Two implementations weren&rsquo;t able to start at all. And another needed a minute to start the system. One of the implementations that could not start the system was the default ROS 2 middleware at the time. To be fair, a few bug fixes solved their problem.</li>
	<li>Ghost Robotics was also an early adopter of Eclipse Cyclone DDS, using it in ROS 2 quadruped robots adopted by Verizon and the U.S. Air Force.</li>
	<li>Trajekt Sports uses Eclipse Cyclone DDS in robotic pitching machines for Major League Baseball players.</li>
	<li>Mission Robotics <a href="https://discourse.ros.org/t/ros2-embarks-on-a-mission-to-the-bottom-of-lake-tahoe/17139">took Eclipse Cyclone DDS 457 m (1,500 ft) underwater to the bottom of Lake Tahoe</a>.</li>
	<li>Box Robotics (now Seegrid) used Eclipse Cyclone DDS to accelerate 3D LiDAR point clouds.</li>
</ul>

<p>These companies, and many others, listened to Joe, tried Eclipse Cyclone DDS, and never looked back. Many became contributors to the project. You can see the growing list of Eclipse Cyclone DDS adopters, <a href="https://iot.eclipse.org/adopters/?#iot.cyclonedds">here</a>.</p>

<p>When Open Robotics recently held its first-ever selection process for the default middleware for a ROS 2 release, Eclipse Cyclone DDS was selected based on <a href="https://osrf.github.io/TSC-RMW-Reports/">Open Robotics&rsquo; extensive testing</a> and their survey of ROS 2 users, which found that <a href="https://osrf.github.io/TSC-RMW-Reports/#Survey">Eclipse Cyclone DDS users are happier</a> than users of other middleware.</p>

<p>Further validating the decision, the Technical University of Dresden published research entitled &ldquo;<a href="https://arxiv.org/pdf/2101.02074.pdf">Latency Overhead of ROS2 for Modular Time-Critical Systems</a>,&rdquo; which concluded that &ldquo;CycloneDDS yields the best latency.&rdquo; Figure 2 illustrates some of the research findings published in the paper.</p>

<p><span style="font-size:12px">Figure 2: Technical University of Dresden ROS 2 Latency Overhead Analysis</span></p>
<img class="img img-responsive" src="images/a2_p2.png" alt="Figure 2: Technical University of Dresden ROS 2 Latency Overhead Analysis">
<h2>Are Other DDS Implementations Also Open Source?</h2>

<p>The previous ROS 2 default middleware is open source, but is developed by a company that simply publishes in GitHub, so it doesn&rsquo;t have the same assurances you get with Eclipse Foundation licensing and governance. Another DDS implementation that is often a point of comparison is a proprietary, closed-source implementation which, according to the Technical University of Dresden research, doesn&rsquo;t perform nearly as well as Eclipse Cyclone DDS in ROS 2 robots, despite its cost.</p>

<p>Naturally, our plan is to ensure that everyone who uses ROS 2 is so happy with Eclipse Cyclone DDS as the default middleware, the next selection process will only be a formality.</p>

<h2>How Will You Enhance the Software to Remain the Default ROS 2 Middleware?</h2>

<p>We&rsquo;ll work closely with the Open Robotics ROS community to understand their rapidly evolving needs.</p>

<p>One thing we&rsquo;re doing based on the community&rsquo;s interest is integrating the <a href="https://projects.eclipse.org/projects/technology.iceoryx">Eclipse iceoryx</a> software to add zero-copy memory transfer and significantly optimize data copy performance. Performance in this area is an issue for many robotics companies because robots with HD cameras, LiDAR, and radar output huge amounts of data. If you have to copy that data multiple times, you end up using all of your resources to copy data instead of other, more useful, tasks.</p>

<p>Robot developers will be able to build Cyclone DDS implementations with Eclipse iceoryx integrated to offload some of that traffic and data. The Eclipse iceoryx integration will be completely transparent to applications. Robot developers will get all of the technology&rsquo;s benefits with no additional effort as we will take on the pain of making it all work behind the scenes.</p>

<p>We&rsquo;re also working on scaling to much larger systems so it&rsquo;s easy and efficient to manage systems transparently through the cloud. DDS is very good at communicating across LANs, but not so great at communicating over the internet. We&rsquo;ve developed a bridge so we can use <a href="http://zenoh.io/">Eclipse zenoh</a> to connect different DDS networks across large areas.</p>

<p>Eclipse Cyclone DDS and Eclipse zenoh are an important combination for robot swarming and autonomous vehicle &ldquo;V2X&rdquo; applications. The combination has been adopted by the <a href="https://discourse.ros.org/t/carma-migrating-to-ros-2-with-cyclonedds-and-zenoh/17541">U.S. Department of Transportation CARMA program</a>. It&rsquo;s early days, but we&rsquo;ve already had a lot of interest from companies and universities with these types of needs, including <a href="https://www.prweb.com/releases/adlinks_edge_ai_powers_autonomous_race_cars_as_part_of_the_indy_autonomous_challenge/prweb17512331.htm">Indy Autonomous Challenge</a> university teams. Figure 3 shows a race car being used in the challenge.</p>

<p><span style="font-size:12px">Figure 3: Indy Autonomous Challenge Race Car</span></p>
<img class="img img-responsive" src="images/a2_p3.png" alt="Figure 3: Indy Autonomous Challenge Race Car">
<h2>How Do You See Eclipse Cyclone DDS Being Used Beyond ROS 2?</h2>

<p>DDS is used in many industries for IoT and other applications, and we want to expand our implementation to reflect that breadth and increase adoption. Here are just a few examples of where DDS is used:</p>

<ul>
	<li>Smart Cities, such as Singapore&#39;s smart lamppost project, which uses DDS, to connect lamppost sensors to the data center</li>
	<li>Smart greenhouses use DDS to control sprinklers and sun shades</li>
	<li>Airports use DDS in control systems for runway lighting and in air traffic control systems</li>
	<li>Agriculture equipment companies use DDS to control self-driving harvesters</li>
</ul>

<p>And, of course it&rsquo;s used in many defense systems, which is where the technology originated. Flight simulators for pilot training are just one example.</p>

<p>Basically, DDS can be used anywhere devices, sensors, controllers, actuators, and computers need to be integrated and share data with low latency, and at relatively high data rates. Even NASA uses DDS for launch and control systems at the Kennedy Space Center.</p>

<h2>Try the Eclipse Cyclone DDS</h2>

<p>I encourage everyone who needs middleware for distributed communications to try the Eclipse Cyclone DDS implementation. If you run into any issues or have questions, just let us know. We&rsquo;re happy to provide suggestions and assistance. Don&rsquo;t be afraid to tell us what you think. And if you want to contribute to the software, so much the better. You can find our developer mailing list details, <a href="https://accounts.eclipse.org/mailing-list/cyclonedds-dev">here</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/erik_boasson.png" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Erik Boasson is a member of the Technology Office at ADLINK Technology. His focus is on DDS, its applications, and future developments, and his current efforts are primarily toward developing Eclipse Cyclone DDS. Erik has 25 years of experience in the field, beginning at the source of the data-centric programming model, Hollandse Signaalapparaten B.V., now Thales Nederland B.V., in the 1990s. His implementation of the SPLICE architecture there played a crucial role in convincing the U.S. Department of Defense to mandate use of DDS.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>