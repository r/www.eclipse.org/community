<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><a href="https://www.eclipse.org/paho/">Eclipse Paho</a> is quite different from other Eclipse Foundation projects in that it&rsquo;s a collection of libraries and utilities designed specifically for use with the MQTT protocol, rather than a single deliverable entity. There are separate <a href="https://github.com/eclipse?q=paho&amp;type=&amp;language">GitHub</a> repositories for each sub-project, including MQTT client libraries in various programming languages: C, C++, Java, Python, Go, and Rust, among others. Each repository is typically the responsibility of a separate committer, so the opportunities for collaboration arise out of the project&rsquo;s intent: to help foster a community around MQTT.</p>

<p>Before I get into the challenges we&rsquo;re having with project maintenance and how you can help, here&rsquo;s some additional background and insight into MQTT and Eclipse Paho.</p>

<h2>Eclipse Paho Has Evolved in Alignment With MQTT</h2>

<p>MQTT was created in the late 1990s to allow large networks of IoT devices to be built. Eclipse Paho was created in 2011 by IBM and started with Java and C client libraries. I&rsquo;ve been involved from the beginning, contributing the C client, and then as project leader since 2014.</p>

<p>Since Eclipse Paho was created, MQTT has evolved from a specification published by IBM to two OASIS standards: <a href="http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/mqtt-v3.1.1.html">MQTT Version 3.1.1</a>, published in 2014, and <a href="http://docs.oasis-open.org/mqtt/mqtt/v5.0/mqtt-v5.0.html">MQTT Version 5.0</a>, published in 2019. These advances have brought MQTT to new audiences and increased its popularity (Figure 1).</p>

<p><span style="font-size:12px">Figure 1: Google Trends Chart Showing Increase in MQTT Popularity</span></p>
<img class="img img-responsive" src="images/a3_p1.png" alt="Figure 1: Google Trends Chart Showing Increase in MQTT Popularity">
<p>Apart from new libraries for additional programming languages, most of the enhancements to Eclipse Paho over the last decade have been implementations of standardized MQTT versions. The latest revision, MQTT Version 5.0, contained significant improvements, and the implementation effort associated with these improvements is significant. Eclipse Paho currently supports multiple languages and MQTT versions (Figure 2).</p>

<p><span style="font-size:12px">Figure 2: Eclipse Paho Support for Languages and MQTT Versions</span></p>
<img class="img img-responsive" src="images/a3_p2.png" alt="Figure 2: Eclipse Paho Support for Languages and MQTT Versions">
<h2>MQTT Standardization Is Ongoing</h2>

<p>There are two additional ongoing standardization efforts related to MQTT: <a href="https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=mqtt-sn">MQTT for Sensor Networks (MQTT-SN)</a> and the <a href="https://sparkplug.eclipse.org/">Sparkplug</a> specification at the Eclipse Foundation.</p>

<p>MQTT-SN is a variant of MQTT that was designed for non-TCP networks, originally ZigBee and UDP. It can extend the reach of MQTT-style messaging to WANs and mesh networks, and is being standardized at OASIS. Eclipse Paho already has MQTT-SN implementations and I expect they will be enhanced to conform to the new standard.</p>

<p>Sparkplug is a specification for MQTT topic names and message contents to enable easier interoperation of industrial applications. <a href="https://projects.eclipse.org/projects/iot.tahu">Eclipse Tahu</a> is the project for SparkPlug implementations.</p>

<h2>The Challenges of Life as an Open Source Maintainer</h2>

<p>Even without adding any major new functionality to Eclipse Paho, staying ahead of issues and pull requests (PRs) on a popular project is a constant workload. Happily, this is a side effect of being successful; if you have no issues, it&rsquo;s likely you have no users. I do find that the faster and more comprehensively you respond, the more issues tend to arrive. These can be simple requests for help or requests to add new functions and fix bugs. Overall, my experience maintaining the C client is rewarding, but it&rsquo;s also challenging, and sometimes exhausting.</p>

<p>Through issues that are created, hearsay, and my experience working at IBM, I know some of the Eclipse Paho client libraries are used by major companies, either in software development kits (SDKs), or in solutions. Every one of these projects, with very minor exceptions, seems to ignore the question of support for their essential open source components. I&rsquo;m sure this aspect wouldn&rsquo;t be ignored if the components were purchased. Is this due to their demonstrated reliability? Or is it because there is no apparent way of paying?</p>

<p>I feel there are many projects in danger of ending up in the position shown in this image from xkcd.com.</p>

<h2>Most Eclipse Paho Team Members Volunteer Their Time</h2>

<p>When Eclipse Paho was first created, the majority of committers came from IBM. As time went on, due to the project&rsquo;s visibility, other libraries and utilities were contributed and their maintainers joined the project. Most of these maintainers were working in their free time. This is even more true today. The current roster of Eclipse Paho committers can be found <a href="https://projects.eclipse.org/projects/iot.paho/who">here</a>. You&rsquo;ll see that most of the work is being done by individuals on their own time. I retired from IBM in 2019.</p>

<p>I recently held an Eclipse Paho team meeting at 19:00 on a Friday evening because I had to find a suitable time slot to accommodate team members from the UK, U.S., and New Zealand. But it was fun. It&rsquo;s been a great pleasure to collaborate with, and learn from, Eclipse Paho contributors from all over the world. The enthusiasm for MQTT and its ability to enable creative solutions gives me a warm feeling. This collaboration is one of the main reasons I&rsquo;ve continued to work on Eclipse Paho.</p>

<h2>Eclipse Paho Needs More Help</h2>

<p>There&rsquo;s always more work outstanding than can be absorbed by the committers we have. Some components haven&rsquo;t had a maintainer for years: the .Net, JavaScript, and Android client libraries, for example. Occasionally I&rsquo;ve stepped in to help, but I can&rsquo;t spread myself too thin. Even accepting PRs, apart from the very simplest, can take significant effort. Without a dedicated committer to manage the component, it&rsquo;s difficult to keep on top of PRs and issues.</p>

<p>Fortunately, I&rsquo;ve been able to attract sponsors to the project since I left IBM. HiveMQ sponsors general project work, and KeySight sponsors specific improvements to the C client through the GitHub Sponsors program. These sponsorships don&rsquo;t cover the full amount of time put into Eclipse Paho, and represent just a small fraction of the benefits consumers of the client libraries have accrued.</p>

<p>Over the last two years, I&rsquo;ve had discussions with IBM, Amazon, and Microsoft, among others, about supporting Eclipse Paho as they use one or more components. Unfortunately, most of these discussions have not resulted in significant contributions.</p>

<p>I&rsquo;ve also had discussions with a number of individuals who find the process of becoming a committer too onerous. Fortunately, most are willing to sign the <a href="https://www.eclipse.org/legal/ECA.php">Eclipse Contributor Agreement (ECA)</a> so they can open a PR, but some are not. Some people would like to help by managing PRs and issues for a short time, say three to six months, then move on. The &ldquo;overhead&rdquo; of becoming a committer is too much for them.</p>

<h2>Get Involved in Eclipse Paho Today</h2>

<p>If anyone has any thoughts on how to encourage more people to become committers, or contribute in other ways to help the project continue, I would very much like to hear them.</p>

<p>Contributions can take several forms:</p>

<ul>
	<li>Become a committer</li>
	<li>Create a PR</li>
	<li>Open an issue</li>
	<li>Provide financial support</li>
</ul>

<p>We welcome everyone with an interest in getting involved in Eclipse Paho. You can find more details about the options listed above and provide comments <a href="https://github.com/eclipse/paho.mqtt.c/issues/1010">here</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/ian_craggs.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Ian Craggs lives in the UK, near Stonehenge. He has worked with IoT and MQTT solutions since 2001, working for IBM until 2019. He continues to work on Eclipse Paho as project lead, MQTT-SN standardization as co-chair, and Sparkplug standardization. He likes running, cycling, VR games and VR art.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>