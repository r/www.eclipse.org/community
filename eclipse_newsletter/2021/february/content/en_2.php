<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>While most developers are familiar with the benefits of cloud-based DevOps, the requirements and environments for edge computing are very different. For example:</p>

<ul>
	<li>Edge computing environments are heterogenous rather than homogenous.</li>
	<li>The edge is not as elastic as the cloud.</li>
	<li>At the edge, compute and power resources are limited, and network connections are slower and less predictable.</li>
</ul>

<p>The bottom line is that traditional DevOps technologies and tools are not well-suited to edge computing. This has led to the emergence of what we refer to as &ldquo;EdgeOps.&rdquo;</p>

<p>EdgeOps technologies and tools are built from the ground up to support emerging edge computing applications. While still an emerging market, some EdgeOps-friendly tools and technologies are available today. A large percentage of them are open source, and a significant number are hosted at the Eclipse Foundation.</p>

<p>To help developers understand more about EdgeOps, its role in edge computing, and the technologies available today, I asked Fr&eacute;d&eacute;ric Desbiens, the IoT and edge computing program manager at the Eclipse Foundation, for additional insight.</p>

<h2>Q. How do you describe the need for EdgeOps to developers?</h2>

<p>A. When you take a closer look at the unique challenges, characteristics, and deployment considerations in edge computing, it&rsquo;s clear that purpose-built technologies are needed.</p>

<p>I like to refer to this illustration to summarize the differences between EdgeOps and DevOps principles (Figure 1).</p>

<p><span style="font-size:12px">Figure 1: EdgeOps Challenges and Requirements Compared to DevOps</span></p>
<img class="img img-responsive" src="images/a2_p1.png" alt="Figure 1: EdgeOps Challenges and Requirements Compared to DevOps">
<p>The challenges are important because they&rsquo;re very specific to the edge, and they can have quite serious consequences if not properly addressed.</p>

<p>You choose edge computing to minimize latency and data processing. If you&rsquo;re operating an oil and gas refinery, you don&rsquo;t want to wait for remote field data to be sent to the cloud, processed, and sent back before making a decision. But, bandwidth from remote nodes is often an issue and cellular communications are very expensive. Resiliency is also hugely important. Think about an autonomous vehicle that suddenly can&rsquo;t communicate with the connected objects it uses for guidance. And issues with data sovereignty can result in healthcare or personal data being shared outside of a hospital, province, state, or country.</p>

<p>In terms of characteristics, edge computing solutions have longer lifespans than most IT projects. If you add edge computing on every floor of a smart building, you want it to be viable for many years, if not decades. Edge solutions must support a wide variety of devices with different processors, capacities, and power requirements. And those devices must be rugged enough to resist dust, water, and other environmental contaminants.</p>

<p>From a deployment perspective, edge computing applications also have unique requirements. For example, updates must be made in a very careful way and take into account the physical and connectivity limitations of the edge devices. In some cases, you may have to send a technician to the field, though eliminating this need is key to efficient EdgeOps. This is quite different from the convenience of the cloud.</p>

<p>Even in cases where cloud approaches such as microservices are used, technologies must be fully adapted for the edge. Microservices in edge computing environments must be more robust than those in cloud environments because edge environments are less predictable and resilient than cloud environments.</p>

<p>Full-featured EdgeOps platforms deliver on all of these requirements, whereas DevOps platforms were designed to meet different requirements.</p>

<h2>Q. Why should developers take an open source approach to EdgeOps?</h2>

<p>A. There&rsquo;s so much diversity and richness available in open source EdgeOps technologies today that you really don&rsquo;t need a commercial platform. With open source, all the technologies you need are available, and you can choose the deployment model that&rsquo;s best for your use case, instead of trying to understand complex licensing schemes for commercial services or locking yourself into a specific cloud or software provider. You have more control and more flexibility than with proprietary EdgeOps software. And of course, it&rsquo;s nice that open source software is free.</p>

<h2>Q. Which Eclipse Foundation projects and communities are relevant to EdgeOps?</h2>

<p>A. The Eclipse Foundation hosts a large number of projects and communities that give developers the features and flexibility needed for a comprehensive edge strategy. Projects are not proofs of concept or blueprints. Tested and proven code is available and ready for use. Some projects, such as Eclipse fog05 and Eclipse ioFog, are already used in edge production environments.</p>

<p>Core edge projects include:</p>

<ul>
	<li><a href="https://fog05.io/">Eclipse fog05</a>, a decentralized edge platform that uses a unified compute fabric to tie together constrained devices, edge nodes, and cloud resources.</li>
	<li><a href="https://iofog.org/">Eclipse ioFog</a>, a centralized edge platform that provides resilience, fault tolerance, security, and low-latency connections between edge devices in an edge compute network.</li>
	<li><a href="http://zenoh.io/">Eclipse zenoh</a>, a publish/subscribe protocol designed to unify data in motion, data in use, data at rest, and computations.</li>
</ul>

<p> </p>

<p>Other complementary projects include:</p>

<ul>
	<li><a href="https://projects.eclipse.org/projects/iot.cyclonedds">Eclipse Cyclone DDS</a>, an implementation of the Data Distribution Service (DDS) protocol frequently used in robots, autonomous vehicles, and other demanding applications.</li>
	<li><a href="https://www.eclipse.org/ditto/">Eclipse Ditto</a>, a highly flexible digital twin platform.</li>
	<li><a href="https://projects.eclipse.org/projects/iot.hawkbit.hara">Eclipse Hara</a>, a client implementation of the Eclipse hawkBit API.</li>
	<li><a href="https://www.eclipse.org/hawkbit/">Eclipse hawkBit</a>&trade;, a platform to deploy software updates to constrained devices and edge nodes.</li>
	<li><a href="https://www.eclipse.org/hono/">Eclipse Hono</a>&trade;, a scalable message routing platform that supports protocols such as MQTT, CoAP, and AMQP.</li>
	<li><a href="https://jakarta.ee/">Jakarta&reg; EE</a>, a set of specifications that define a platform for Cloud Native Java applications.</li>
	<li><a href="https://www.eclipse.org/jkube/">Eclipse JKube</a>&trade;, a collection of plugins and libraries for building container images of Java applications.</li>
	<li><a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a>&trade;, a modular IoT cloud platform to manage and integrate devices and their data.</li>
	<li><a href="https://www.eclipse.org/kura/">Eclipse Kura</a>&trade;, an extensible IoT edge framework that&rsquo;s based on Java/OSGi and offers API access to the hardware interfaces of IoT gateways.</li>
	<li><a href="https://microprofile.io/">MicroProfile</a>&trade;, a baseline platform definition that optimizes enterprise Java for a microservices architecture.</li>
	<li><a href="https://mosquitto.org/">Eclipse Mosquitto</a>&trade;, a message broker that implements the MQTT protocol.</li>
	<li><a href="https://www.eclipse.org/paho/">Eclipse Paho</a>, client implementations of the MQTT and MQTT-SN protocols.</li>
	<li><a href="https://skupper.io/">Skupper</a>, a Layer 7 service network interconnect that enables secure communications across edge nodes with no virtual private networks (VPNs) or special firewall rules. Skupper is not an Eclipse Foundation project, but has been integrated into Eclipse ioFog v2.0.</li>
</ul>

<p>Projects span multiple communities, including the <a href="https://edgenative.eclipse.org/">Eclipse Edge Native Working Group</a>, <a href="https://iot.eclipse.org/">Eclipse IoT</a>,<a href="https://www.osgi.org/"> </a><a href="https://www.osgi.org/">OSGi Working Group</a>, and <a href="https://sparkplug.eclipse.org/">Sparkplug Working Group</a>, so there are many people contributing from different perspectives. This is important for a well-rounded edge strategy.</p>

<h2>Q. How do Eclipse Foundation projects fit within the broader EdgeOps ecosystem?</h2>

<p>A. When you look at technologies for development and operations across the edge-to-cloud continuum, you can see where Eclipse Foundation projects fit in relation to other technologies. We like to use this matrix to illustrate the role the Eclipse Foundation plays in developing this emerging market (Figure 2). The color logos are Eclipse Foundation projects.</p>

<p><span style="font-size:12px">Figure 2: Key Open Source Projects in the EdgeOps Matrix</span></p>
<img class="img img-responsive" src="images/a2_p2.png" alt="Figure 2: Key Open Source Projects in the EdgeOps Matrix">
<p>The Eclipse Foundation ecosystem is strong. We also work closely with other open source communities, including the Cloud Native Computing Foundation. We gladly work with other open ecosystems because the debates, ideas, and collaboration ultimately result in better technologies for EdgeOps.</p>

<h2>Learn More</h2>

<p>To learn more about EdgeOps and the edge technologies being developed at the Eclipse Foundation:</p>

<ul>
	<li>Visit the <a href="https://edgenative.eclipse.org/">Edge Native Working Group website</a>.</li>
	<li>Check out the many projects listed above.</li>
	<li>Watch for our soon to be released white paper EdgeOps: A Vision for Edge Computing.</li>
</ul>



<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/clark_roundy.jpeg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Clark Roundy is responsible for IoT, edge, and cloud development tools marketing at the Eclipse Foundation.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>