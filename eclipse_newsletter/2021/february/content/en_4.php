<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Simon Bernard</h2>

<div style="display:inline;">
<img width="160" class="float-left margin-right-40 img img-responsive" src="images/committer_profile_generic.png" alt="Simon Bernard">
</div>
<div style="display:inline;">
<ul>
	<li>Involved in open source since: Approximately 2007</li>
	<li>Works for: Sierra Wireless</li>
	<li>Eclipse Foundation contributor since: Approximately 2007</li>
	<li>Involved in: <a href="https://www.eclipse.org/leshan/">Eclipse Leshan</a>,<a href="https://www.eclipse.org/wakaama/"> Eclipse Wakaama</a>, <a href="https://www.eclipse.org/californium/">Eclipse Californium (Cf) CoAP Framework</a>, <a href="https://www.eclipse.org/ldt/">Eclipse Lua Development Tools</a></li>
	<li>Committer to: All five projects he is involved in, although his primary focus is on Eclipse Leshan and Eclipse Wakaama</li>
	<li>Committer since: 2011</li>
</ul>
</div>

<hr />
<h2>Why did you first get involved in open source software communities?</h2>

<p>I feel that cooperation makes us better. Modern science is one of the greatest achievements of humankind and this knowledge is built through cooperation and sharing. With this idea in mind, it&rsquo;s not hard to conclude that software should also be written in a cooperative and transparent way.</p>

<p>When I first started working as a developer, my employer asked me to work on a project based on the <a href="https://wiki.eclipse.org/Rich_Client_Platform">Eclipse Rich Client Platform (RCP)</a>. I first started contributing by reporting issues, discussing new features, and sometimes providing small bug fixes.</p>

<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>

<p>I first became a committer in 2011 on an Eclipse Foundation project called Koneki that is now archived. I also worked on the <a href="https://www.eclipse.org/ldt/">Eclipse Lua Development Tools</a> project, and when that ended, I told my manager how important open source software is to me and that I wanted to continue working on it. Two of my coworkers&mdash;Manuel Sangoi and Julien Vermillard&mdash;were creating Leshan as an open source project to test the LWM2M protocol, so I joined the team.</p>

<p>When the LWM2M protocol was chosen as the preferred protocol for device management at Sierra Wireless, Leshan became an important part of our IoT platform. Because Leshan was already open source on GitHub, and the Eclipse Foundation hosts <a href="https://iot.eclipse.org/projects/">many IoT projects</a>, we decided to host Leshan at the Eclipse Foundation. Now, I&rsquo;m the main contributor, and a committer, to <a href="https://www.eclipse.org/leshan/">Eclipse Leshan</a>. Because Leshan is based on <a href="https://www.eclipse.org/californium/">Eclipse Californium</a>, I became a committer to that project as well. And in 2020, I became a committer to <a href="https://www.eclipse.org/wakaama/">Eclipse Wakaama</a>.</p>

<h2>How would you summarize your experiences as a committer?</h2>

<p>The most rewarding thing is getting positive feedback from users. Knowing that our work is used and appreciated is great. But it&rsquo;s also sometimes challenging to get more people involved in project development.</p>

<p>I have to carefully consider all contributions and try to do the right thing for the project without demotivating contributors. This can be a lot of work. There&rsquo;s a lot of code to review, and the contributor is not always an expert on the project, so you need to have discussions to get to a solution that&rsquo;s accepted by the committer and contributor.</p>

<h2>What are your next steps and goals as a committer and Eclipse Foundation community member?</h2>

<p>My next goals are to deliver an Eclipse Leshan v2.0.0 release that supports LWM2M 1.1, and to keep Eclipse Wakaama on track.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>

<p>They should definitely try it, especially because we need their help. As open source is used more and more, we need more people to help create sustainable and good quality open source code, and to maintain it.</p>

<p>Joining an open source project can help developers bring more meaning to their work, and it&rsquo;s a good way to improve their technical and cooperative skills.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
