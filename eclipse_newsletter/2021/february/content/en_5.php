<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2>Share Your Commercial IoT and Edge Experiences</h2>

<p>Help the industry and our working groups focus on your top priorities for commercial IoT and edge solutions. Complete the <a href="https://www.surveymonkey.com/r/2021CommunityNewsletter">2021 IoT and Edge Commercial Adoption Survey</a> before February 28.</p>

<p>With your input, all IoT and edge ecosystem stakeholders &mdash; software vendors, platform vendors, solution providers, and manufacturing organizations &mdash; will have deeper insight into:</p>

<ul>
	<li>Forecasts for IoT and edge market growth</li>
	<li>Challenges and potential barriers that impact market development and size</li>
	<li>The latest IoT and edge computing market trends</li>
	<li>The mix of proprietary and open source solutions being used in IoT solutions and where open source software provides key benefits</li>
	<li>How edge computing is being incorporated into IoT solutions</li>
	<li>Strategies other companies are using to increase their IoT footprint</li>
</ul>

<p>Your survey responses will also influence <a href="https://iot.eclipse.org/">Eclipse IoT</a> and <a href="https://edgenative.eclipse.org/">Edge Native Working Group</a> roadmaps, helping to ensure they continue to deliver the open source software you need most.</p>

<p><a href="https://www.surveymonkey.com/r/2021CommunityNewsletter">Participate in the survey here</a></p>

<h2>Eclipse Keyple&trade; v1.0 Java Now Available</h2>

<p><a href="https://keyple.org/">Eclipse Keyple</a> is the first open source API for contactless ticketing and access control. The v1.0 Java release allows the technology to be deployed on terminals with a distributed architecture.</p>

<p>With Eclipse Keyple v1.0 Java, developers can drive remote smart card readers independently of network communications protocols. For example, they can enable a ticketing terminal that doesn&rsquo;t have a secure access module (SAM) to send its SAM requests to a central server. Conversely, a ticketing server can manage a remote light terminal&rsquo;s card readers.</p>

<p>The release of Eclipse Keyple v1.0 Java means the API is now frozen. This API will be the reference API for terminal certification, which is currently being developed.</p>

<p>The Keyple technology was initiated by the Calypso Networks Association (CNA), and is now hosted at the Eclipse Foundation.</p>

<p>For more information about Eclipse Keyple, visit the <a href="https://keyple.org/">website</a>.</p>

<p>To access the Eclipse Keyple v1.0 Java source files, go to <a href="https://github.com/eclipse/keyple-java/releases/tag/1.0.0">GitHub</a>.</p>

<h2>Read the itemis Member Case Study</h2>

<p>Learn how itemis is <a href="https://outreach.eclipse.foundation/itemis-open-source-case-study">building its automotive industry business</a> through Eclipse Foundation membership.</p>

<p>itemis specializes in model-driven technologies. About half of the company&rsquo;s software architects and engineers use, or are involved in, projects at the Eclipse Foundation.</p>

<p>Although the automotive industry has historically preferred to use proprietary software, Andreas Graf, the business development manager for automotive at itemis, says that perspective is changing. itemis uses the technologies hosted at the Eclipse Foundation to successfully support two different business models &mdash; selling services and selling licenses for its YAKINDU software tools.</p>

<p>For more insight into itemis&rsquo; strategy, growth, and success, <a href="https://outreach.eclipse.foundation/itemis-open-source-case-study">read the case study</a>.</p>

<p>If you missed our earlier member case studies, use the links below to download them:</p>

<ul>
	<li><a href="https://outreach.eclipse.foundation/payara-services-open-source-case-study">Payara Services Gains an Equal Footing With Industry Leaders</a></li>
	<li><a href="https://outreach.eclipse.foundation/cedalo-iot-open-source-case-study">Cedalo Finds New Opportunities in the Eclipse IoT Ecosystem</a></li>
	<li><a href="https://outreach.eclipse.foundation/obeo-open-source-case-study">Obeo Accelerates Growth, Increases Exposure as a Strategic Member of the Eclipse Foundation</a></li>
</ul>

<p>To share your open source success story, email marketing@eclipse.org.</p>

<h2>Eclipse VOLTTRON&trade; in Real-World IoT Deployments</h2>

<p>After starting life as an internal project at the Pacific Northwest National Laboratory (PNNL) in the U.S., Eclipse VOLTTRON is <a href="https://outreach.eclipse.foundation/volttron-iot-case-study">proving its value and versatility in a variety of IoT applications</a>.</p>

<p><a href="https://volttron.org/">Eclipse VOLTTRON</a> is a software platform for distributed sensing and control. It provides a cost-effective, scalable, and secure foundation for any application that needs to collect, manage, and analyze data streams from almost any system or subsystem.</p>

<p>With support from companies that develop and manage applications based on the technology, Eclipse VOLTTRON, is now a key component in IoT deployments at a variety of organizations, including:</p>

<ul>
	<li>Washington D.C.&rsquo;s Department of General Services (DGS)</li>
	<li>Penn State University</li>
	<li>University of Toledo</li>
	<li>Energy provider, Southern Company</li>
	<li>The City of Monroe, Louisiana</li>
</ul>

<p>To learn more about these Eclipse VOLTTRON deployments and the growing ecosystem around the technology, <a href="https://outreach.eclipse.foundation/volttron-iot-case-study">read the case study</a>.</p>

<h2>New Webinar Series: The Edge of Things</h2>

<p>The first session in our new webinar series is on March 24 at 11:00 a.m. ET, 17:00 CET, and will focus on the 2020 IoT Developer Survey results. Register <a href="https://www.crowdcast.io/e/edgeofthings_mar24_21">here</a>.</p>

<p>The Edge of Things webinar series will feature panel discussions on a variety of topics surrounding edge computing and IoT, including:</p>

<ul>
	<li>IoT and Edge Commercial Adoption Survey results</li>
	<li>The latest edge and IoT white papers, case studies, and other content resources</li>
	<li><a href="https://docs.google.com/forms/d/e/1FAIpQLSesOtjsgamZK6K47pV2ojRZMzNs5Q9p7w_T4116dwKKWlM1vg/viewform?usp=sf_link">Topics suggested by members</a></li>
</ul>

<p>Watch for announcements about upcoming webinars.</p>

<h2>Save the Date: EclipseCon 2021</h2>

<p>Reserve October 25-28 to attend EclipseCon 2021. This year&rsquo;s event will once again be virtual, and will build on the success of EclipseCon 2020.</p>

<p>EclipseCon 2020 featured dozens of sessions across 10 different tracks and garnered thousands of views. For EclipseCon 2020 highlights, check out <a href="https://blogs.eclipse.org/blogs/hudson-kelly">Hudson Kelly&rsquo;s blog posts</a>.</p>



<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>