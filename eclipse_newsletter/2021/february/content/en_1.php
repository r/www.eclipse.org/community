<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The Message Queuing Telemetry Transport Protocol (MQTT) has become extremely popular for IoT and Industrial IoT (IIoT) applications, and for good reason. This simple, lightweight, and open transport protocol enables devices and clients to publish information to a central MQTT broker. Other devices, clients, and additional brokers can subscribe to the messages from the MQTT broker and publish their own messages back to the broker.</p>

<p>Before we take a closer look at the benefits MQTT provides, here&rsquo;s a brief look at the protocol&rsquo;s origins and relationship to the Sparkplug specification.</p>

<h2>An Implementation Standard Is Needed for Interoperability in IIoT</h2>

<p>Arlen Nipper and Andy Stanford-Clark (Figure 1) co-invented MQTT while trying to solve a specific problem for Phillips 66 in the late 1990s. The problem they were trying to solve is still relevant today: How do we improve data access in the field for an entire enterprise without using massive amounts of bandwidth?</p>

<p><span style="font-size:12px">Figure 1: Arlen Nipper and Andy Stanford-Clark</span></p>
<img class="img img-responsive" src="images/a1_p1.png" alt="Figure 1: Arlen Nipper and Andy Stanford-Clark">

<p>Instead of following the model that had been used for decades, the team decided to think outside of the typical poll-response industrial automation box. MQTT was the result of that effort.</p>

<p>The majority of implementation information was intentionally left out of the original MQTT specification. This decision enabled maximum flexibility and is the reason Facebook, Amazon Web Services, and Microsoft Azure were early MQTT adopters and have found the protocol so appealing.</p>

<p>However, to meet the interoperability needs of the industrial automation community, MQTT implementation standards had to be defined. This has been achieved with the creation of the Sparkplug specification for MQTT. As Arlen Nipper described in an interview with Inductive Automation in 2019, the Sparkplug specification provides several important benefits for IIoT.</p>

<p>According to Nipper, &quot;Sparkplug is a specification that defines how to use MQTT in a mission-critical, real-time OT environment. It defines a standard MQTT Namespace that&#39;s optimized for industrial application use cases. It defines an efficient MQTT Payload definition that&#39;s extensible but that&#39;s been optimized for SCADA, tag, and metric representations. It defines how best to use MQTT state management in real-time SCADA implementations. And lastly, it defines some high-availability MQTT architectures addressing both redundancy and scale.&quot;</p>

<p>But do we really need a specification for MQTT? Won&rsquo;t a specification reduce the flexibility MQTT offers? I discussed these questions with Walker Reynolds of Intellic Integration. Reynolds is a friend and a superb systems integrator.</p>

<h2>The Sparkplug Specification Brings Structure to MQTT</h2>

<p>Walker brought some great insights to the discussion. &quot;Yes, MQTT is flexible &mdash; you can basically publish any payload to any topic,&rdquo; he said, &ldquo;but this can create a mess in the topic namespace. Additionally, building IoT applications using many MQTT clients publishing into the same broker, without any uniformity in the payload structure, takes additional time. Sparkplug B is the specification written by Arlen Nipper and team for publishing industrial data and provides the structure, compression, and uniformity needed to normalize disparate data into a unified structure at the broker.&quot;</p>

<p>Walker is right. Sparkplug &mdash; B is the latest version of the specification &mdash; provides the structure for anyone who wants to create an IoT or IIoT application that uses MQTT without worrying about interoperability between their project and other applications.</p>

<p>Arlen Nipper and his team were wise enough to realize the Sparkplug specification had to be a collaborative effort within the industrial automation community. That&rsquo;s why they chose to move the specification to the Eclipse Foundation and form the <a href="https://sparkplug.eclipse.org/">Sparkplug Working Group</a>.</p>

<p>Here are a few of the main reasons to consider an MQTT architecture based on the Sparkplug specification.</p>

<h2>Dramatically Simplify Network Architectures</h2>

<p>Consider a traditional architecture and the number of connections that must be balanced between devices, servers, and client tools (Figure 2).</p>

<p><span style="font-size:12px">Figure 2: Traditional Architecture With Spaghetti-Like Connections</span></p>
<img class="img img-responsive" src="images/a1_p2.png" alt="Figure 2: Traditional Architecture With Spaghetti-Like Connections">

<p>The MQTT architecture is dramatically simpler (Figure 3). You just need to point publishing devices to a single broker and define a single topic, or multiple topics, the device should publish under. Subscribing clients can then use those same topics to filter the data they want to receive from the broker.</p>

<p><span style="font-size:12px">Figure 3: Simplified Architecture With MQTT</span></p>
<img class="img img-responsive" src="images/a1_p3.png" alt="Figure 3: Simplified Architecture With MQTT">

<h2>Use Considerably Less Bandwidth</h2>

<p>Because MQTT does not use poll-response communications, it only communicates when change occurs. This massively reduces bandwidth requirements. Additionally, the Sparkplug specification enables MQTT to achieve three times higher compression on the packets it transports than it could previously.</p>

<p>Johnathan Hottell is an application engineer (and genius) with Kelvin Inc, a company that specializes in upstream oil and gas artificial intelligence. Hottell compared bandwidth usage with a Sparkplug MQTT implementation to other industry protocols such as OPC Unified Architecture (UA) and Modbus. Hottell demonstrated that MQTT can provide between 75 percent and 99.5 percent more bandwidth efficiency than Modbus in real-world applications. The savings over other publish/subscribe protocols, such as OPC UA, are also significant (Figure 4).</p>

<p><span style="font-size:12px">Figure 4:  MQTT Bandwidth Costs Compared to Other Protocols</span></p>
<img class="img img-responsive" src="images/a1_p4.png" alt="Figure 4:  MQTT Bandwidth Costs Compared to Other Protocols">
<p><span style="font-size:12px">Source: Johnathan Hottell, Kelvin Inc.</span></p>

<h2>Increase Data Granularity</h2>

<p>Because MQTT only communicates changes, you can theoretically increase the scan class without increasing bandwidth usage.</p>

<p>For example, if a discrete tag typically changes six times a day and you&rsquo;re using poll response, you need to balance how often you scan the tag with the amount of bandwidth you&rsquo;re prepared to sacrifice. For example, if the tag is polled every 15 minutes, data is communicated 96 times per day to capture six value changes. And there is potential for a delay of up to 14:59 minutes from the time the value changes to the time you are aware of the change. Using MQTT, you are immediately aware of the tag change, and you only send the six value changes per day.</p>

<h2>Increase State Awareness</h2>

<p>MQTT brokers are aware of client states and can communicate that state to other clients. This awareness is enabled with birth and death certificates and the MQTT Last Will and Testament (LWT) feature.</p>

<p>When a publishing client connects to a broker, it provides a birth certificate and its LWT payload to the broker. The broker then informs MQTT clients that subscribe to the same topic as the publishing client when the publishing client is online or offline. If the publishing client is offline, the broker also provides the subscribing client with the LWT payload. This ensures stale data is not delivered to subscribing clients, and is accomplished using a pre-configured keep alive timer or heart beat check between broker and publishing clients.</p>

<h2>Ensure Security</h2>

<p>Network security is inherited because MQTT is based on TCP/IP. This means the MQTT specification evolves at the same pace as TCP/IP, and helps ensure the specification remains simple.</p>

<h2>Learn More About Sparkplug and Get Involved</h2>

<p>If you&rsquo;re interested in learning more about MQTT and the Sparkplug specification, visit the <a href="https://sparkplug.eclipse.org/">Sparkplug Working Group website</a>. You&rsquo;ll find helpful <a href="https://sparkplug.eclipse.org/resources/">resources</a> and an <a href="https://sparkplug.eclipse.org/about/faq/">FAQ page</a> that explains more about the relationship between MQTT, the Sparkplug specification, and Eclipse Foundation projects.</p>

<p>To get involved in developer discussions about Sparkplug, subscribe to the <a href="https://accounts.eclipse.org/mailing-list/sparkplug-dev">mailing list</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/jeff_knepper.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Jeff Knepper is the Executive Director of Canary Labs. Jeff works directly with large enterprises globally to assist with the collection, storage, and contextualization of time series data using the Canary software solution.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>