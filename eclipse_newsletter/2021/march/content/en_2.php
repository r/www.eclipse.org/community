<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  As most Jakarta EE developers are likely already aware, the concept of a single reference
  implementation (RI) to prove a specification can be implemented doesn&rsquo;t exist in Jakarta EE.
  The<a href="https://jakarta.ee/about/jesp/"> </a><a href="https://jakarta.ee/about/jesp/">Jakarta
    EE Specification Process (JESP)</a> mandates there must be at least one compatible
  implementation (CI), but preferably as many as possible.
</p>
<p>However, some developers may not realize how easy it is to have a Jakarta EE-compatible
  implementation listed on a specification page. Here are the high-level steps:</p>
<ol>
  <li>Pass the technology compatibility kit (TCK) tests and publish the results.</li>
  <li>File a<a
    href="https://raw.githubusercontent.com/jakartaee/specification-committee/master/compatibility-certification-request.md"
  > </a><a
    href="https://raw.githubusercontent.com/jakartaee/specification-committee/master/compatibility-certification-request.md"
  >certification request</a>.
  </li>
  <li>Submit a<a href="https://github.com/jakartaee/specifications/pulls"> </a><a
    href="https://github.com/jakartaee/specifications/pulls"
  >pull request (PR)</a> to have your implementation listed.
  </li>
</ol>
<p>And here&rsquo;s a closer look at each step. We&rsquo;re investigating ways to further simplify
  the process by automating some steps, but this is the process today.</p>
<h2>1. Pass the TCK Tests and Publish the Results</h2>
<p>
  Your software must pass the TCK tests for the specification you implement. The TCK can be
  downloaded from the specification page and is unique for each version of the specification (Figure
  1). You can access the Jakarta EE specification pages<a href="https://jakarta.ee/specifications/">
  </a><a href="https://jakarta.ee/specifications/">here</a>.
</p>
<p>Figure 1: TCK Location Example</p>
<p><img src="images/2-1.png" alt="Figure 1: TCK Location Example"></p>
<p>The next step is to publish the TCK test results. This is the heart of self-certification.
  Because you run the TCK tests and publish the results yourself, you don&rsquo;t have to rely on
  others to run the tests for you. When you publish the results, you&rsquo;re taking responsibility
  for their correctness.</p>
<p>For examples of how to publish TCK test results, check the results for the following:</p>
<ul>
  <li><a href="https://eclipse-ee4j.github.io/yasson/tck/TCK-results.html">Eclipse Yasson 2.0.0</a>,
    which implements Jakarta JSON Binding 2.0</li>
  <li><a href="https://github.com/jakartaredhat/cdi-tck/wiki/Jakarta-CDI-3.0-TCK-Results">Weld 4.0.0</a>,
    which implements Jakarta Contexts and Dependency Injection (CDI) 3.0</li>
  <li><a href="https://eclipse-ee4j.github.io/krazo/certifications/jakarta-mvc/2.0/TCK-Results.html">Eclipse
      Krazo 2.0.0</a>, which implements Jakarta MVC 2.0</li>
  <li><a
    href="https://jenkins.webtide.net/job/tck/job/servlettck-run-jetty-11.0.x/lastCompletedBuild/TCK_20Report/"
  >Eclipse Jetty 11.0.1</a>, which implements Jakarta Servlet 5.0</li>
</ul>
<p>After publishing the TCK test results, send an email to tck@eclipse.org to make the Eclipse
  Foundation aware your software has passed the TCK tests.</p>
<h2>2. File a Certification Request</h2>
<p>The next step is to file a certification request in the Jakarta EE specification&rsquo;s issue
  tracker.</p>
<p>
  A certification request must follow the template shown in Figure 2, and linked<a
    href="https://raw.githubusercontent.com/jakartaee/specification-committee/master/compatibility-certification-request.md"
  > </a><a
    href="https://raw.githubusercontent.com/jakartaee/specification-committee/master/compatibility-certification-request.md"
  >here</a>, to provide the necessary information.
</p>
<p>Figure 2: Certification Request Template</p>
<p><img src="images/2-2.png" alt="Figure 2: Certification Request Template"></p>
<p>When a specification team receives a certification request, they verify the contents of the
  request, then add the certification label. For examples of certification requests, see:</p>
<ul>
  <li><a href="https://github.com/eclipse-ee4j/jsonb-api/issues/257">Eclipse Yasson 2.0.0</a></li>
  <li><a href="https://github.com/eclipse-ee4j/cdi/issues/442">Weld 4.0.0</a></li>
  <li><a href="https://github.com/eclipse-ee4j/mvc-api/issues/53">Eclipse Krazo 2.0.0</a></li>
  <li><a
    href="https://jenkins.webtide.net/job/tck/job/servlettck-run-jetty-11.0.x/lastCompletedBuild/TCK_20Report/"
  >Eclipse Jetty 11.0.1</a></li>
</ul>
<h2>3. Submit a PR to Have Your Compatible Implementation Listed</h2>
<p>
  When the certification request has been approved, submit a<a
    href="https://github.com/jakartaee/specifications/pulls"
  > </a><a href="https://github.com/jakartaee/specifications/pulls">PR</a> to the<a
    href="https://github.com/jakartaee/specifications"
  > </a><a href="https://github.com/jakartaee/specifications">specifications repository</a> and add
  your implementation to the list of compatible implementations for the specification.
</p>
<p>
  To add your implementation, update the _index.md file in the version sub-directory of the
  specification you&rsquo;re implementing. For example, to add a new compatible implementation
  called <strong>OpenKangaroo 1.0.0</strong> to the <strong>Jakarta Wombat</strong> specification,
  follow the format shown in the last line of Figure 3, marked with a +.
</p>
<p>Figure 3: Compatible Implementation List Addition Example</p>
<p><img src="images/2-3.png" alt="Figure 3: Compatible Implementation List Addition Example"></p>
<h2>Get More Information</h2>
<p>
  For a list of compatible implementations and the Jakarta EE compatible product they are included
  in, see Arjan Tijms&rsquo; list of<a
    href="https://arjan-tijms.omnifaces.org/2020/05/implementation-components-used-by.html"
  > </a><a href="https://arjan-tijms.omnifaces.org/2020/05/implementation-components-used-by.html">implementation
    components used by Jakarta EE servers</a>.
</p>
<p>
  If you have any questions about the process, don&rsquo;t hesitate to reach out to the Jakarta EE
  community through the<a href="https://jakarta.ee/connect/mailing-lists/"> </a><a
    href="https://jakarta.ee/connect/mailing-lists/"
  >Jakarta EE mailing lists</a>.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/ivar.png" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Ivar Grimstad is the Jakarta EE Developer Advocate at the Eclipse Foundation. He is a Java
  Champion and Java User Group (JUG) leader based in Sweden. Ivar is also involved in<a
    href="https://microprofile.io/"
  > </a><a href="https://microprofile.io/">MicroProfile</a> and Apache NetBeans, and is a frequent
  speaker at international developer conferences.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>