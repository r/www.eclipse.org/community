<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>As soon as Jakarta&trade; EE 9 was released, the community shifted its focus to Jakarta EE 9.1
  and support for Java&trade; SE 11. During development of Jakarta EE 9, it became clear that
  supporting Java SE 8 and Java SE 11 was not feasible in the desired timeframe. In June 2020, it
  was decided that support for Java SE 11 would be pushed to a near-term Jakarta EE 9.1 release.</p>
<p>Here&rsquo;s a look at what is, and is not, included in the Jakarta EE 9.1 release, along with
  some thoughts on the current status of the release.</p>
<h2>Community Discussions Started the Process</h2>
<p>
  The first order of business was to define the<a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9.1ReleasePlan"
  > </a><a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9.1ReleasePlan"
  >Jakarta EE 9.1 Release Plan</a>. There were many thoughts and ideas about what should, and should
  not, be included in the release. As was the case with Jakarta EE 9, community members were not shy
  about sharing their opinions. We welcomed these ideas and discussions!
</p>
<p>A generic Google Doc was created to capture the ideas provided through discussions, emails,
  tweets, surveys, and mailing lists. Topics included questions such as:</p>
<ul>
  <li>Should this release be a version 9.1 or a version 10?</li>
  <li>If we support Java SE 11, does that mean we should also increase the minimum Java SE version
    &mdash; from 8 to 11?</li>
  <li>What should we do about Common Object Request Broker Architecture (CORBA) and Remote Method
    Invocation over Internet Inter-Orb Protocol (RMI-IIOP) requirements?</li>
  <li>Should we allow component specification updates?</li>
  <li>Should we support Java Platform Module System (JPMS) modules?</li>
</ul>
<p>These discussions eventually led to the belief that if we truly wanted a near-term release, we
  would have to limit our expectations and wish list. So, we went back to our original plan of
  supporting Java SE 11 as the sole requirement.</p>
<p>
  After another round of discussions with the community using a simplified Google Doc, we eventually
  ended up with the<a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9.1ReleasePlan"
  > </a><a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9.1ReleasePlan"
  >Jakarta EE 9.1 Release Plan</a>.
</p>
<h2>Jakarta EE 9.1 Release Highlights</h2>
<p>The requirements for Jakarta EE 9.1 can be stated as:</p>
<ul>
  <li>Support for the Java SE 11 runtime.</li>
  <li><a href="https://jakarta.ee/specifications/platform/">Jakarta EE Platform</a> and<a
    href="https://jakarta.ee/specifications/webprofile/"
  > </a><a href="https://jakarta.ee/specifications/webprofile/">Jakarta EE Web Profile</a> will be
    the only specifications affected.
    <ul>
      <li>Specification documents, Technology Compatibility Kits (TCKs), and compatible
        implementations for the platform and web profile will be the only deliverables.</li>
      <li>No other Jakarta EE component specifications will be included in Jakarta EE 9.1.</li>
    </ul></li>
  <li>The top-level convenience jars for the platform and web profile will be<a
    href="https://search.maven.org/artifact/jakarta.platform/jakarta.jakartaee-web-api/9.1.0-RC1/jar"
  > </a><a
    href="https://search.maven.org/artifact/jakarta.platform/jakarta.jakartaee-web-api/9.1.0-RC1/jar"
  >reproduced at the 9.1 version</a>, but the API content will be the same as Jakarta EE 9.
  </li>
</ul>
<h2>There&rsquo;s a Lot of Testing to Be Done</h2>
<p>
  As with most projects this size, the majority of the effort is spent on testing. In this
  particular case, it&rsquo;s testing the compatible implementation &mdash;<a
    href="https://github.com/eclipse-ee4j/glassfish"
  > </a><a href="https://github.com/eclipse-ee4j/glassfish">Eclipse Glassfish</a> &mdash; with the
  updated Jakarta EE 9.1 TCK. Our original goal was to deliver Jakarta EE 9.1 by the end of March
  2021. However, our test effort assessment indicates we will have to move the delivery date into
  the second quarter.
</p>
<p>
  Although we are still experiencing a<a
    href="https://github.com/eclipse-ee4j/jakartaee-tck/wiki/Jakarta-EE-9.1-TCK-results"
  > </a><a href="https://github.com/eclipse-ee4j/jakartaee-tck/wiki/Jakarta-EE-9.1-TCK-results">couple
    thousand failures with the TCK</a> (remember the Platform TCK includes close to 50,000 tests),
  the good news is that very few of these failures are resulting in updates to Glassfish. Almost all
  of the necessary changes are contained in the TCK itself. They include scripting updates due to
  Java SE 11, demarcation of some optional aspects of Java and Jakarta EE such as CORBA and
  RMI-IIOP, and API signature tests required for Java SE 11.
</p>
<p>
  Some additional good news is coming from the<a href="https://openliberty.io/"> </a><a
    href="https://openliberty.io/"
  >Open Liberty</a> compatibility testing effort. Early testing against the<a
    href="https://download.eclipse.org/ee4j/jakartaee-tck/jakartaee9-eftl/staged-910/jakarta-jakartaeetck-9.1.0.zip"
  > </a><a
    href="https://download.eclipse.org/ee4j/jakartaee-tck/jakartaee9-eftl/staged-910/jakarta-jakartaeetck-9.1.0.zip"
  >Jakarta EE 9.1 TCK</a> is showing a 99.9 percent success rate with Java SE 11. And this is
  occurring without any updates to the Open Liberty codebase used for certifying Jakarta EE 9.
  We&rsquo;re expecting Glassfish to be rounding this corner very soon.
</p>
<h2>We Need Community Involvement</h2>
<p>We&rsquo;re always looking for additional community involvement. As I mentioned earlier, we
  received some excellent input and feedback when defining the Jakarta EE 9.1 release. Now that
  we&rsquo;re nearing the homestretch, additional community participation in the testing and triage
  of failures is more than welcome.</p>
<p>
  Many of the problems encountered with the TCK effort are first logged as<a
    href="https://github.com/eclipse-ee4j/jakartaee-tck/issues?q=is%3Aopen+is%3Aissue+label%3A9.1"
  > </a><a
    href="https://github.com/eclipse-ee4j/jakartaee-tck/issues?q=is%3Aopen+is%3Aissue+label%3A9.1"
  >issues in the TCK repository</a>. As more debugging occurs, other issues may be logged against
  Glassfish or the individual component implementations. Please take a few minutes to review the
  issues and see if you can help resolve them.
</p>
<p>Jakarta EE would not exist without the continued support from the extended community. We
  appreciate everything you do to support this project!</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/kevin.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Kevin Sutter is co-lead of the <a
    href="https://projects.eclipse.org/projects/ee4j.jakartaee-platform"
  >Jakarta EE Platform project</a>, as well as a member of the <a
    href="https://projects.eclipse.org/projects/ee4j"
  >Eclipse EE4J</a> Project Management Committee (PMC). Kevin also helps guide the overall Jakarta
  EE mission by participating on the <a href="https://jakarta.ee/">Jakarta EE</a> Steering and
  Specification Committees. Kevin&#39;s interest in the Enterprise Java programming models also
  extends into the <a href="https://microprofile.io/">MicroProfile</a> arena, where he participates
  in the working group as well as co-lead for the <a
    href="https://projects.eclipse.org/projects/technology.microprofile"
  >MicroProfile project</a>.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>