<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Introducing Our Newest Working Group: Adoptium</h2>
<p>With the creation of the <a href="https://adoptium.net/">Adoptium Working Group</a>, the next chapter in the AdoptOpenJDK initiative
  to deliver high-quality Java runtimes under open source licenses has officially begun.</p>
<p>The Adoptium Working Group was created in collaboration with the AdoptOpenJDK Technical Steering
  Committee, and will support the Eclipse Adoptium top-level project. The working group will provide
  the infrastructure, marketing, community building, and developer advocacy needed to ensure timely
  releases of vendor neutral Java runtimes and long-term community growth.</p>
<p>The Eclipse Adoptium project is the continuation of the AdoptOpenJDK organization&rsquo;s work.
  The project provides a trusted location where developers and enterprises can download fully
  compatible, high-quality distributions of Java runtimes based on OpenJDK source code.</p>
<p>AdoptOpenJDK was created in 2017 to give developers a dependable source of Java runtimes that
  have open source licenses, and are supported with timely patches and updates. Since then, more
  than 240 million instances of high-quality Java runtimes, patches, and updates have been
  downloaded.</p>
<p>
  To learn more about the Adoptium Working Group, <a href="https://www.eclipse.org/org/workinggroups/adoptium-charter.php">read the charter</a>.
</p>
<hr />
<h2>Coming Soon: Jakarta EE Developer Survey</h2>
<p>Watch for news items, blogs, and emails with the link to this year&rsquo;s Jakarta EE Developer
  Survey. This is your opportunity to add your voice to the global Java ecosystem.</p>
<p>With your feedback, the entire Java ecosystem will have a better understanding of the
  requirements, priorities, and perceptions in the global Java developer community. This
  understanding enables a clearer view of the Java industry landscape, the challenges Java
  developers are facing, and the opportunities for enterprise Java stakeholders in the cloud native
  era.</p>
<p>Last year, we learned that Jakarta EE had become the second-place cloud native framework, and
  that combined, Java 8 and Jakarta EE 8 hit the mainstream with 55 percent adoption. It will be
  very interesting to see how the Java landscape has evolved in the past year.</p>
<p>
  We encourage all Java developers to participate in the survey! See our <a
    href="https://f.hubspotusercontent30.net/hubfs/4753786/JakartaEE-Infographic.pdf"
  >new infographic</a> for a recap of our 2020 findings.
</p>
<hr />
<h2>Cloud IDE Day: Call for Papers</h2>
<p>The deadline for paper submissions to this half-day online event hosted by the Eclipse Cloud
  Development Tools Working Group is March 28.</p>
<p>Cloud IDE Day is May 19. The virtual event focuses on tools and best practices for development in
  the cloud and is open to everyone who is interested in the future of IDE development.</p>
<p>
  For event details, visit the <a href="https://ecdtools.eclipse.org/events/idesummit/2021/">website</a>.
</p>
<hr />
<h2>Eclipse SAAM Mobility 2021: Call for Papers</h2>
<p>Submit your paper to the Eclipse Security, AI, Architecture, and Modelling (SAAM) for Next
  Generation Mobility virtual conference by April 16. This year&rsquo;s conference is June 15-16.</p>
<p>The Eclipse SAAM Mobility conference brings together industry experts and researchers working on
  innovative software and systems solutions for the next generations of mobility.</p>
<p>Conference organizers encourage submissions that focus on constructive, design-oriented research
  on innovations in software, models, and methods related to the following themes:</p>
<ul>
  <li>Security and privacy for mobility</li>
  <li>Artificial intelligence for autonomous mobility</li>
  <li>Architecture for mobility</li>
  <li>Modeling for complex mobility systems and services</li>
</ul>
<p>
  For submission guidelines and event details, visit the <a href="https://events.eclipse.org/2021/saam-mobility/">website</a>.
</p>
<hr />
<h2>Adoptium to Host Outreachy Interns</h2>
<p>The Adoptium Working Group will be welcoming two Outreachy interns from May to August 2021.</p>
<p>Outreachy provides paid, remote, three-month internships to support diversity in Free and Open
  Source Software (FOSS). Interns work with experienced mentors from open source communities to
  further their knowledge and experience in a variety of areas, including programming, user
  experience, documentation, illustration, graphic design, data science, project marketing, user
  advocacy, and community event planning.</p>
<p>Adoptium is excited to collaborate with people who will bring new viewpoints, valuable questions,
  and insights to the project. The community firmly believes that having more unique perspectives
  leads to better solutions. Red Hat, an Eclipse Foundation Strategic Member, is sponsoring the
  internship.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>