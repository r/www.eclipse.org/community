<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Since it was announced that Java EE would find new life as Jakarta EE at the Eclipse Foundation,
  there have been many discussions and questions about the relationship between<a
    href="https://jakarta.ee/"
  > Jakarta EE</a> and<a href="https://microprofile.io/"> MicroProfile</a>. To help clarify and
  simplify the relationship, the two communities have joined forces in the Cloud Native for Java
  (CN4J) Alliance.
</p>
<p>
  The CN4J Alliance has analyzed three different approaches to improve alignment between Jakarta EE
  and MicroProfile and simplify their use together. The advantages and disadvantages of each option
  are summarized in this<a
    href="https://reza-rahman.me/2021/02/09/your-input-needed-to-determine-path-for-jakarta-ee-microprofile-alignment/"
  > blog post</a> by CN4J community participant, Reza Rahman. The post also includes a link to a
  short<a href="https://www.surveymonkey.com/r/5DQYHYB"> survey</a> where you can choose your
  preferred alignment option and explain the rationale behind your choice.
</p>
<p>I recently spoke with Reza about the need to improve Jakarta EE and MicroProfile alignment and
  why everyone with an interest in cloud native Java should voice their opinion on the best path
  forward. Here&rsquo;s an edited version of our conversation.</p>
<h2>Q. What is the current relationship between Jakarta EE and MicroProfile and how did we get to
  this point?</h2>
<p>
  <strong>A. </strong>At the moment, Jakarta EE and MicroProfile are complementary, but there are
  some technology areas where there is overlap and others where the alignment is not clear.
</p>
<p>We&rsquo;re at this point because there was a technology gap a few years ago that had to be
  filled. Developers needed technologies to accelerate cloud native application development and
  address the issues that arise when creating microservices architectures. But at the time, Oracle
  was no longer advancing Java EE.</p>
<p>MicroProfile was created to fill this gap and give developers the technologies they needed. In
  fact, the existence of MicroProfile was part of the impetus for Oracle to contribute the Java EE
  technologies to the Eclipse Foundation.</p>
<h2>Q. Why is more alignment between Jakarta EE and MicroProfile needed?</h2>
<p>
  <strong>A. </strong>Jakarta EE and MicroProfile are currently established as separate entities,
  and it&rsquo;s causing a lot of confusion. Developers are not sure how the technologies relate to
  one other, or how the two sets of specifications can be used together. It&rsquo;s time to resolve
  this confusion.
</p>
<p>Today, Jakarta EE can be used on its own for monolithic applications that are primarily designed
  for on-premises deployments. You can use Jakarta EE to build cloud native microservices
  architectures, but having MicroProfile technologies to develop microservices for cloud native
  applications certainly helps.</p>
<p>Similarly, MicroProfile on its own is not truly self-sufficient. It depends on Jakarta EE
  technologies such as RESTful web services, JSON binding, bean validation, and dependency
  injection. Even technologies such as Quarkus and Helidon, which are based on MicroProfile, use
  more Jakarta EE technologies than MicroProfile requires.</p>
<p>We have two highly co-dependent sets of technologies, hosted at the same open source foundation,
  with many of the same stakeholders. There is a desire in the ecosystem to see better alignment
  between Jakarta EE and MicroProfile, and perhaps even convergence at some point.</p>
<h2>Q. What is the role of the CN4J Alliance in improving alignment between Jakarta EE and
  MicroProfile?</h2>
<p>
  <strong>A. </strong>We want the entire ecosystem to understand how MicroProfile technologies can
  be used in Jakarta EE &mdash; for microservices and cloud native development as well as for
  on-premises monolithic technologies.
</p>
<p>To achieve this goal, we&rsquo;re trying to make it clearer where Jakarta EE and MicroProfile
  overlap and where they complement one another. We&rsquo;re also trying to determine how to clarify
  and streamline the areas where they do overlap and identify potential areas for convergence.</p>
<h2>Q. Why is it important for developers to review the alignment options and complete the survey?</h2>
<p>
  <strong>A. </strong>It&rsquo;s in developers&rsquo; best interests for Jakarta EE and MicroProfile
  to be better aligned. Using the technologies will be more straightforward, and it&rsquo;s more
  likely they will remain relevant over the long term. This will help developers and enterprises
  protect their existing investments in Java technologies. It will also help to ensure there&rsquo;s
  a serious multivendor and specifications-based alternative to Spring, which means the overall Java
  ecosystem will remain healthier and more competitive.
</p>
<h2>Q. What should developers be thinking about as they consider the alignment options?</h2>
<p>
  <strong>A. </strong>Developers should be thinking about their own ease of use and their own
  requirements, not about how the technologies are organized, structured, or branded today.
</p>
<p>Right now, developers face complexity burdens when using Jakarta EE and MicroProfile:</p>
<ul>
  <li>Which versions of the specifications work with each other?</li>
  <li>Which namespace should be used?</li>
  <li>What should Maven dependency declarations look like?</li>
  <li>Where should improvements be contributed?</li>
</ul>
<p>At the end of the day, developers just need the simplest possible solution to the problem
  they&rsquo;re trying to solve. As developers review the different alignment options, they should
  ask themselves which approach is best to minimize the complexity burden and make their life
  easier.</p>
<h2>Q. What&rsquo;s the best way to get started?</h2>
<p>
  <strong>A. </strong> Start by<a href="https://reza-rahman.me/blog/"> reviewing the pros and cons of the three alignment
    options</a>, then specify your preference in the<a href="https://www.surveymonkey.com/r/5DQYHYB">
    survey</a>. Please also use the comment box in the survey to explain your preference.
</p>
<p>We&rsquo;re looking for the broadest possible spectrum of input, so we encourage everyone with an
  interest in cloud native Java to consider the alignment options and join the conversation.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/karen.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">Karen McNaughton is the Senior Marketing Manager, Cloud Native Java,
            at the Eclipse Foundation and currently contributes to the execution of the global
            marketing strategy for the Jakarta EE Working Group. The Jakarta EE working group drives
            the evolution and broad adoption of technologies derived from or related to the Eclipse
            Enterprise for Java (EE4J) project.</p>
        </div>
      </div>
    </div>
  </div>
</div>