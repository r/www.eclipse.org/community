<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Jason Mehrens</h2>

<div style="display:inline;">
<img width="160" class="float-left margin-right-40 img img-responsive" src="images/jason.jpg" alt="Jason Mehrens">
</div>
<div style="display:inline;">
  <ul>
    <li>Involved in open source since: 2005</li>
    <li>Works for: Inpixon</li>
    <li>Eclipse Foundation contributor since: 2019</li>
    <li>Involved in:<a href="https://eclipse-ee4j.github.io/mail/"> </a><a
      href="https://eclipse-ee4j.github.io/mail/"
    >Jakarta Mail</a></li>
    <li>Committer to: Jakarta Mail</li>
    <li>Committer since: 2019</li>
    <li>Fun Fact: In addition to his full-time job at Inpixon and his work on Jakarta Mail, Jason
      runs the family farm, growing corn and soybeans.</li>
  </ul>
</div>

<hr />
<h2>Why did you first get involved in open source software communities?</h2>
<p>In 2005, I was interested in Java 5 and concurrency. The source code was being developed in the
  open, so I read the code and the mailing list to improve my own skillset and see how experts in
  the field were developing software.</p>
<p>After a while, I was able to recognize potential issues, and I thought the fair thing would be to
  contribute back and share those thoughts to improve the overall health of the project. I primarily
  offered suggestions and provided feedback on code.</p>
<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>
<p>The Java platform&rsquo;s core logging facility didn&rsquo;t have built-in support for logging to
  email. A few years after Java was made open source, I thought that Java Mail would be a good place
  for the logging-mailhandler to live and evolve. Bill Shannon, the project maintainer at the time,
  welcomed that code contribution and shepherded me through the process.</p>
<p>When Java EE transitioned to the Eclipse Foundation, I was hesitant to join because I had a lot
  of other commitments at the time. But Bill really pushed me to join and to continue being part of
  Jakarta Mail development. So I&rsquo;ve been helping to code and maintain Jakarta Mail as a
  committer since 2019.</p>
<h2>How would you summarize your experiences as a committer?</h2>
<p>It can be challenging to find enough time to review issues, create patches, maintain features,
  and interact with the community. But you have access to the source code for some really well-known
  projects and can influence their direction. You can repair issues at their origin which can
  improve the longevity of the project and its dependents.</p>
<p>I like the openness, the access to the code base, the expertise, and the people who are
  developing the software. It&rsquo;s a great knowledge base.</p>
<h2>What are your next steps and goals as a committer and Eclipse Foundation community member?</h2>
<p>For now, I&rsquo;m going to continue maintaining the code base for Jakarta Mail. As I become more
  comfortable with the process, I might get involved in some of the larger Jakarta EE projects.</p>
<h2>What would you say to developers who are considering getting more involved in open source
  software projects at the Eclipse Foundation?</h2>
<p>I would say there are many ways to contribute, from answering questions on the various project
  mailing lists to filing issues or creating pull requests. You can influence a project by issuing a
  patch, having an expert review that patch, and having it committed to a project. The big advantage
  of being more involved is access to expertise.</p>
<p>I would also say that getting more involved can help you resolve your own issues faster. You have
  to spend some time writing the code, going through the review cycle, and integrating the patch.
  But it can end up saving you time because you don&rsquo;t have to maintain a fork of the code or
  workarounds.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
