<?php
//if name of the file requested is the same as the current file, the script will exit directly.
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}

$common = array();
$common['style_blue'] = 'color:#303355 !important;';
$common['style_black'] = 'color:#000000 !important;';
$common['style_white'] = 'color:#EFEDF9 !important;';
$common['style_grey'] = 'color:#666666 !important;';