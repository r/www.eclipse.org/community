<?php
//if name of the file requested is the same as the current file, the script will exit directly.
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}

function innerHTML($el) {
	$doc = new DOMDocument();
	$doc->appendChild($doc->importNode($el, TRUE));
	$html =  preg_replace('@^<' . $el->nodeName . '[^>]*>|</' . $el->nodeName . '>$@', '', trim($doc->saveHTML()));
	return $html;

}

	$file = basename($App->getScriptName(), ".php").'.html';
	$doc = new DOMDocument;
	$doc->loadhtmlfile($file);

	$body =  innerHTML($doc->getElementsByTagName('body')->item(0));
	$title = innerHTML($doc->getElementsByTagName('title')->item(0));
	//$css = innerHTML($doc->getElementsByTagName('style')->item(0));