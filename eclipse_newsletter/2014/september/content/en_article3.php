<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


    <p>This article demonstrates how to create UI tests with RCP Testing
      Tool. Existing Eclipse bugs from Eclipse bugzilla were used as
      scenarios for test cases.</p>

    <h2>What is RCP Testing Tool?</h2>
    <p>
      <a href="http://eclipse.org/rcptt">RCP Testing Tool</a> (RCPTT) is
      an open-source Eclipse project for UI testing of Eclipse-based
      applications. It does not test web, Swing or JavaFX. It works only
      with Eclipse applications, but does it in the best possible way.
      The focus on a single platform allows our development team to
      build the solution that dramatically increases productivity of
      test case design and reliability of test results. It is easy to
      demonstrate and I will do this below.
    </p>

    <p>Here is a very short list of what I consider the strongest points
      of RCPTT:</p>
    <ul>
      <li><b>State control.</b> RCPTT uses declarative <a
        href="http://www.eclipse.org/rcptt/documentation/userguide/contexts/">contexts</a>
        to represent a state of Eclipse application, required by a test
        case. There are various kinds of contexts for describing
        different aspects of an application state &#x2013; workspace,
        workbench, preferences, launch configurations, etc. So, if a
        test case is intended to test an editor, it just describes the
        required state (i.e. there should be a project in workspace and
        some file should be opened in an editor) using contexts. Think
        about contexts as reusable pieces of application state
        description &#x2013; once a state is captured, any number of
        test cases can reuse it. This feature significantly speeds up
        test case development and isolates test cases from each other
        &#x2013; test cases can be executed in any order, and test case
        failures don't affect other tests.</li>
      <li><b>Recording.</b> By any means it gives a huge productivity
        boost, which is hard to overestimate. There are few cases when
        recorded script can not be replayed as is (for instance when you
        need to provide some unique name), but even in these cases
        recording gives an excellent starting point for further
        modifications.</li>
      <li><b>Smart runtime.</b> During test case execution, RCPTT
        runtime automatically tracks all asynchronous operations inside
        an application and waits for its completion. Hence, if UI action
        triggers some operation which should be waited for, the next
        action won't start until background operation completes. This
        eleminates, in particular, the necessity to put manual waitings
        or operation retry logic.</li>
      <li><b>Flexible DSL.</b> <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/ecl/">ECL</a>
        is the scripting language used for test cases. It easy to read
        and understand and you don't need to be a programmer to do this.
        At the same time ECL is a programming language. Complicated
        business logic, extension with user-defined commands? No
        problem.</li>
    </ul>

    <p>In this article, I'm going to illustrate the process of test case
      design. I took few real Eclipse UI bugs from Eclipse Bugzilla and
      created test cases for them.</p>

    <h2>Preparing test development environment</h2>
    <p>This is straightforward process and takes less than 10 minutes:</p>
    <ol>
      <li><a href="http://eclipse.org/rcptt/download">Download</a>,
        unpack, and launch RCPTT.</li>
      <li>In a context menu of 'Test Explorer' view select 'New &#8594;
        RCP Testing Tool Project'.</li>
      <li>In a context menu of 'Applications' view select 'New&#8230;'
        and browse for an application-under-test (I'm going to use
        Eclipse Luna).</li>
      <li>Once you add an application-under-test, double-click it in
        'Applications' view to launch.</li>
    </ol>
    <p>The final setup should look like this:</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/init.png"><img
        src="/community/eclipse_newsletter/2014/september/images/init.png"
        class="img-responsive" alt="" /></a>
    </p>
    <p>That's it. Now you are ready to go. The general workflow for test
      case design looks like this:</p>
    <ol>
      <li>Describe an initial state using contexts.</li>
      <li>Create a test case with required context references.</li>
      <li>Record actions and assertions.</li>
      <li>Modify recorded script if necessary.</li>
    </ol>

    <p>Now, let's design our first test case.</p>

    <h2>
      Eclipse bugzilla <a
        href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=318826">#318826</a>:
      Comparing identical files opens editor instead of showing dialog
    </h2>
    <p>That is pretty old and not really annoying bug. You can easily
      reproduce it manually &ndash; just select two files with the same
      content and compare them with each other:</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/compare-aut.png"><img
        src="/community/eclipse_newsletter/2014/september/images/compare-aut.png" alt="" /></a>
    </p>

    <p>However how difficult is it to create an automated UI test for
      this, and how much time would it take? For me it took just 3
      minutes and 15 seconds, because I was talking over the phone at
      the same time =). The process in details:</p>

    <h3>Describing initial state</h3>

    <p>For initial state of test, assume the following:</p>
    <ul>
      <li>There is a resource project in workspace, named <code>project</code>,
        with folders <code>folder1</code> and <code>folder2</code>. Each
        folder contains an empty file <code>file.txt</code>.
      </li>
      <li>Project Explorer view is open.</li>
    </ul>

    <p>
      To describe this state, we need to create <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/contexts/workspace/">workspace</a>
      context with required project, and <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/contexts/workbench/">workbench</a>
      context with required view.
    </p>

    <p>You can do the following to create a required workspace context:
    </p>
    <ol>
      <li>Right-click project, select 'New &#8594; Context' select
        Workspace context type, name it like "workspace for comparison"
        and click 'Finish'. Empty workspace context is created and
        opened in the editor.</li>
      <li>Go to application-under-test and bring your workspace into
        desired state manually (i.e. create project, folders and files).
      </li>
      <li>Go back to RCPTT IDE and click 'Capture' button in context
        editor &#x2013; all projects from the workspace will be copied
        from an application-under-test into a context.</li>
    </ol>

    <p>The resulting workspace context editor should look like this:</p>


    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/compare-workspace.png"><img
        src="/community/eclipse_newsletter/2014/september/images/compare-workspace.png"
        alt="Screenshot of workspace context" /></a>
    </p>


    <p>Now this workspace context contains a copy of a project from
      application-under-test workspace. When this context is applied, it
      automatically replaces existing projects from a workspace with
      it's contents. Test case does not care about the previous state of
      a workspace &ndash; context guarantees that it will match test's
      expectations.</p>

    <p>Let's create a workbench context. The process of context creation
      is the same as above, but this time you should select Workbench
      context type and name it like "Project Explorer view is open". You
      can capture it from AUT, so it would automatically remember
      selected perspective, open views and open editors. However, for a
      current test case you don't care about perspective and editors
      &#x2013; you just want to ensure that Project Explorer view is
      open. You can just click a Plus icon in Views section and add
      project explorer view. Your Workbench context should look like the
      following:</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/compare-workbench.png"><img
        src="/community/eclipse_newsletter/2014/september/images/compare-workbench.png"
        alt="Screenshot of workbench context" /></a>
    </p>

    <h3>Recording a test case</h3>

    <p>
      Right-click a project in Test Explorer to create a test case,
      select 'New &#8594; Test Case', set test case name as "318826
      Comparing identical resources" and click 'Finish'. Your new test
      case will be automatically opened in the editor. Add contexts to a
      test case in 'Contexts' section of an editor and hit 'Record'
      button. RCPTT IDE will minimize itself, open <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/controlpanel/">Control
        Panel</a>, activate AUT, apply contexts and start recording your
      actions.
    </p>

    <p>Now you can do the the following actions (control panel will be
      updated with recorded script immediately):</p>
    <ol>
      <li>Select <code>folder1</code> and <code>folder2</code> in
        Project explorer, right-click and hit 'Compare &#8594; With each
        other'.
      </li>
      <li>Switch to <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/assertions/">assertion
          mode</a> as soon as the message dialog appear. Click the label
        'There are no differences between selected inputs', and specify
        that you want to assert it's caption:
        <p>
          ><a
            href="/community/eclipse_newsletter/2014/september/images/compare-assertion.png"><img
            src="/community/eclipse_newsletter/2014/september/images/compare-assertion.png"
            alt="compare-assertion.png" /></a>
        </p>
      </li>
      <li>Switch back to recording mode and close the message dialog.</li>
      <li>Select <code>folder1/file</code> and <code>folder2/file</code>
        in project explorer and compare them with each other again.
      </li>
      <li>Simply close an empty compare editor.</li>
    </ol>

    <p>Recording is done. Go back to Control panel, stop recording, save
      a test case and click Home icon on toolbar to go back to RCPTT
      IDE.</p>

    <p>Now your test case editor should look like this:</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/compare-testcase.png"><img
        src="/community/eclipse_newsletter/2014/september/images/compare-testcase.png"
        alt="test case editor" /></a>
    </p>

    <p>In addition (if you want) you can provide some test case
      description and specify a link to external resource (i.e. bug
      tracker).</p>

    <p>You can replay this test case right from the editor. It will be
      executed successfully, however that's not what you really want
      &ndash; right now test case verifies wrong behavior, caused by a
      bug in an application. The test case should pass only after the
      bug will be fixed. Before describing a way to make this test case
      failing, let's quickly go through recorded script:</p>

    <pre>with [get-view "Package Explorer" | get-tree] {
    select "project/folder1" "project/folder2"
    get-menu "Compare With/Each Other" | click
}
get-window Compare | get-label "There are no differences between the selected inputs." | get-property caption
    | equals "There are no differences between the selected inputs." | verify-true
get-window Compare | get-button OK | click
with [get-view "Package Explorer" | get-tree] {
    select "project/folder1/file.txt" "project/folder2/file.txt"
    get-menu "Compare With/Each Other" | click
}
get-editor "Compare ('project/folder1/file.txt' - 'project/folder2/file.txt')" | close </pre>

    <p>
      The scripting language (ECL) is inspired by TCL and Powershell
      &ndash; it consists of commands with arguments, connected by pipes
      and an output of the left command can be passed as an input to the
      right command. Square brackets (
      <code>[]</code>
      ) are used to pass an output of a command as an argument to
      another command. Curly brackets (
      <code>{}</code>
      ) are used to pass a script as an argument. It allows command to
      execute this script in some context.


    <p>


    <p>
      Here the script begins with
      <code>with</code>
      command. This command takes two arguments &ndash; object and
      script. This command executes the script, while passing given
      object as an input to each command in the script. The first four
      lines can be interpreted as this:
    </p>
    <ol>
      <li>Take a Project Explorer view (<code>get-view "Project
          Explorer"</code>).
      </li>
      <li>Find a tree in this view (<code>get-tree</code>).

      <li>With this tree, perform the following actions:
        <ul>
          <li>Select two elements, denoted by paths (<code>select
              "project/folder1"...</code>).
          </li>
          <li>Click 'Compare With &#8594; Each Other' in context menu.</li>
        </ul>
      </li>
    </ol>

    <p>
      Next line,
      <code>get-window Compare | get-label ...</code>
      , was recorded from an assertion mode. The general pattern here
      looks like this:
    </p>
    <pre>get some UI object | get-property 'propertyName' | equals/not-equals 'expectedValue' | verify-true</pre>

    <p>
      In this particular case, we added an assertion for a property,
      which is used to control location, so in fact there is some
      repetition here, that does not affect anything, but we could just
      use
      <code>get-window Compare | get-label "There are..."</code>
      as well &ndash; in case when there's no label with this text, the
      test would fail any way.
    </p>

    <p>Another important thing to notice about this script is that it
      does not contain any waiting logic &ndash; it waits for background
      operations automatically (calculation of comparison results in
      this case).</p>

    <h3>Make test to fail</h3>
    <p>
      Now let's make our test case to fail correctly. At first, we don't
      need a last line (
      <code>get-editor ... | close</code>
      ), because in correct behaviour, there should be no such editor.
      Instead we want to ensure that there is a window with correct
      label, so I'm replacing a last line of this script with this:
    </p>
    <pre>get-window Compare | get-label "There are no differences between the selected inputs."</pre>

    <p>Now, if you execute the resulting test case, it will fail as
      expected:</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/compare-execution.png"><img
        src="/community/eclipse_newsletter/2014/september/images/compare-execution.png"
        alt="The window 'Compare' could not be found" /></a>
    </p>

    <h3>Summary</h3>

    <ul class="org-ul">
      <li>Contexts are used to describe an initial state of an
        application.</li>
      <li>Recorded script can be modified.</li>
      <li>During replay, RCPTT runtime automatically waits for
        background operations.</li>
    </ul>

    <p>Let's take a fresh regression bug for the next test case. It has
      been introduced in Luna, so we can run test on Kepler and Luna to
      see that it affects only Luna.</p>

    <h2>
      Eclipse bugzilla <a
        href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=443671">#443671</a>:
      Add required bundles option looks completely broken
    </h2>
    <p>For this bug we can create a test case, that will be doing the
      following:</p>
    <ol>
      <li>Open launch configurations dialog.</li>
      <li>Select OSGi launch configuration, which includes only <code>org.eclipse.ui</code>
        plugin.
      </li>
      <li>Press 'Add Required Bundles'.</li>
      <li>Press 'Validate Bundles'.</li>
      <li>Make sure that validation message box says that no problems
        detected.</li>
    </ol>

    <h3>State description</h3>

    <p>
      We don't care about workspace for this test case, but instead but
      we care about current launch configurations. And there is a
      perfect match for this in RCPTT &#x2013; <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/contexts/launch/">Launch</a>
      contexts. So we go to AUT, create launch configuration, create
      launch context and capture it. The resulting Launch context looks
      like this:
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/launch-launch.png"><img
        src="/community/eclipse_newsletter/2014/september/images/launch-launch.png"
        alt="launch-launch.png" /></a>
    </p>

    <p>We are going to use 'Run &#8594; Run Configurations&#8230;' menu,
      so we need to ensure that this menu item exist. As there are
      perspectives without Run menu (i.e. Resources perspective), we
      want to state that this test case requires some perspective, which
      definitely has 'Run' menu, for instance 'Plug-in Development'
      perspective. To do this, we create a workbench context, switch AUT
      to 'Plug-in Development' perspective, and capture workbench
      context from AUT.</p>

    <h3>Recording actions</h3>

    <p>As usual, create a test case, add launch and workbench contexts
      to it, and record test actions &ndash; open 'Launch
      configurations' dialog, select launch configuration and press
      required buttons. The resulting script will look like this:</p>

    <pre>get-menu "Run/Run Configurations..." | click
with [get-window "Run Configurations"] {
  get-tree | select "OSGi Framework/org.eclipse.ui osgi launch"
  get-button "Add Required Bundles" | click
  get-button "Validate Bundles" | click
  get-window Validation | get-button OK | click
}</pre>

    <p>We can replay it and obviously the test case will pass, as it
      does not contain any assertions yet.</p>

    <h3>Asserting results</h3>
    <p>To assert that validation completed successfully, change the last
      line to this:</p>

    <pre>get-window Validation | get-label "No problems were detected."</pre>

    <p>Once a test case is replayed again, it correctly fails with a
      message 'The Label "No problems were detected." could not be
      found'. To make sure that the test is correct, run the same test
      without any modifications on Kepler to see that it passes:</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/launch-kepler-luna.png"><img
        src="/community/eclipse_newsletter/2014/september/images/launch-kepler-luna.png"
        class="img-responsive" alt="launch-kepler-luna.png" /></a>
    </p>

    <h2>
      Eclipse bugzilla <a
        href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=394900">#394900</a>:
      Import existing projects broken by malformed .project file
    </h2>

    <p>Again, this issue is easy to reproduce manually, but how hard is
      it to make an automated test for this?</p>

    <h3>Preparing a state</h3>

    <p>
      To prepare a state for this test case, we can use <a
        href="https://www.eclipse.org/rcptt/documentation/userguide/contexts/folder">Folder</a>
      context. At first, create two resource projects in AUT workspace
      &#x2013;
      <code>corruptedProject</code>
      and
      <code>goodProject</code>
      , then create a folder in home directory,
      <code>projectsToImport</code>
      , and copy projects from AUT workspace into this folder. Then
      modify
      <code>.project</code>
      file in
      <code>corruptedProject</code>
      to produce an invalid XML.
    </p>

    <p>
      Next, go back to RCPTT and create a new Folder context. To specify
      a folder context root, browse for a
      <code>projectsToImport</code>
      folder and click 'Capture': (on a screenshot below I also opened
      the captured file to demonstrate that the XML is really corrupted)
    </p>


    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/import-folder.png"><img
        src="/community/eclipse_newsletter/2014/september/images/import-folder.png"
        alt="import-folder.png" /></a>
    </p>

    <p>
      Similar to workspace context, which stores a state of a workspace,
      a Folder context can be used to control a state of a file system
      outside of workspace, which is especially useful for testing of
      import functionality. Note that Folder context editor was smart
      enough to record a path to a folder in machine-independent way (
      <code>home://projectsToImport/</code>
      ), so that this context can be used on other machines.
    </p>

    <p>For this test case we also need a workspace context, which
      ensures that workspace is empty. You can just create a new
      workspace context, name it 'empty workspace' and there's nothing
      else to do &ndash; by default workspace contexts have no projecs
      inside and have an option 'Clear workspace' turned on.</p>

    <h3>Recording actions</h3>
    <p>Create a new test case, add two contexts into it, and record the
      following actions:</p>
    <ol class="org-ol">
      <li>Go to File/Import&#x2026; menu.</li>
      <li>Select General &#8594; Existing projects.</li>
      <li>In import dialog, browse for <code>projectsToImport</code>
        folder.
      </li>
      <li>Click Refresh button.</li>
      <li>Switch to assertion mode and assert that a Projects section is
        empty &ndash; click on a tree and add an assertion for <code>itemCount</code>
        property.
      </li>
    </ol>

    <p>The recorded script is below:</p>

    <pre>get-menu "File/Import..." | click
with [get-window Import] {
    get-tree | select "General/Existing Projects into Workspace"
    get-button "Next &gt;" | click
}

set-dialog-result Folder "/Users/ivaninozemtsev/projectsToImport"
with [get-window Import] {
    get-button "Browse..." | click
    get-button Refresh | click
}
get-window Import | get-tree | get-property itemCount | equals 0 | verify-true</pre>

    <p>
      You may notice that there's an interesting command recorded
      &#x2013;
      <code>set-dialog-result</code>
      . That's how RCPTT handles native dialogs: during recording, it
      remembers a value, selected by user in a native dialog, and stores
      it as a
      <code>set-dialog-result</code>
      command. During test case execution RCPTT runtime automatically
      returns stored value to application code instead of opening a
      native dialog.
    </p>

    <p>
      However, in order to make a test case fully portable across
      machines and operating systes, we need to slightly modify this
      script in order to replace an absolute path with a value, relative
      to a home folder. This can be achieved by using two ECL commands
      &#x2013;
      <code>get-java-propery</code>
      and
      <code>format</code>
      :
    </p>

    <pre>set-dialog-result Folder [format "%s/projectsToImport" [get-java-property "user.home"]]</pre>

    <h3>Asserting results</h3>
    <p>
      Next, we also need to modify the last part of a script, which
      asserts the results &#x2013; instead of asserting that there are
      no children, actually we want to assert the correct behavior, i.e.
      there is one child,
      <code>goodProject</code>
      . This can be done like this:
    </p>
    <pre>with [get-window Import | get-tree] {
    select "goodProject.*"
    get-property itemCount | equals 1 | verify-true
}</pre>

    <p>
      Pay attention to this line &ndash;
      <code>select "goodProject.*"</code>
      . Here we use a regular expression instead of full text of a tree
      item (full text also includes an absolute path on a file system).
      It is possible to use
      <code>format</code>
      command again to construct a full text here, but in this case it
      is faster to simply strip an end text by
      <code>.*</code>
      .
    </p>


    <h2>Conclusion</h2>

    <p>Finally, you can run all three tests together and see that all of
      them are failing with expected messages:</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2014/september/images/final.png"><img
        src="/community/eclipse_newsletter/2014/september/images/final.png"
        class="img-responsive" alt="final.png" /></a>
    </p>

    <p>You can use RCPTT in order to produce independent test cases
      efficiently and quickly. RCPTT allows to execute them without
      modifications on different operating systems and different
      versions of Eclipse platform. Give it a try, and share your
      opinion!</p>
    <p>Here are some useful links to get started:</p>

    <ul>
      <li><a href="http://eclipse.org/rcptt">RCPTT site</a></li>
      <li><a href="http://eclipse.org/rcptt/download">RCPTT downloads</a></li>
      <li><a
        href="http://eclipse.org/rcptt/documentation/userguide/getstarted">Get
          started guide</a></li>
      <li><a href="https://www.eclipse.org/forums/eclipse.rcptt">RCPTT
          forum</a></li>
    </ul>



<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/september/images/Ivan.jpg"
        width="90" alt="ivan inozemtsev" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ivan Inozemtsev<br />
        <a target="_blank" href="http://www.xored.com/">Xored</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://github.com/komaz">GitHub</a></li>
        <li><a target="_blank" href="http://twitter.com/ivaninozemtsev">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/+StefanOehme/posts">Google +</a></li>
					$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


