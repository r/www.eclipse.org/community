<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <h2>What is Productivity Intelligence?</h2>
    <p>
      <a target="_blank"
        href="http://www.eng.it/home/home-eng.dot?com.dotmarketing.htmlpage.language=1">Engineering
        Group</a> has adopted an innovative approach within its Software
      Labs to measure quality in development processes. This approach is
      named <b>Productivity Intelligence</b> for two reasons.
    </p>
    <p>
      The former is related to the meaning of "productivity". It does
      not simply refer to lines of code written per day or man-hours
      worked per day! It’s not just the economic efficiency of company
      employees. It’s about integrating all the dimensions involved in
      code production. I mean adding to the two traditional dimensions <b>Economic</b>
      - development costs and efficiency - and <b>Technical</b> -
      software measures, bug fixes, test coverage - the <b>Social</b>
      one: customers' and developers' satisfaction. This allows to give
      stage to the voice of the users as well as of developers.
    </p>
    <p>The latter refers to a technological reason: the adoption of
      business intelligence techniques to monitor and improve
      productivity, through integration of data coming from different
      sources. This helps to efficiently integrate various development
      tools in a common infrastructure collecting different production
      information. It’s about building bridges between different
      technologies and development practices.</p>

    <h2>I’m not a developer</h2>
    <p>My current focus is not on building solutions, but on building
      integrations. Probably it’s because I’m quite aged - working in
      the IT field from a very long time – or probably it comes from my
      attitude in community building in open source. I’m currently
      working to help create an environment letting developers proudly
      do their best and meet the corporate requirements at the same
      time. This helps to bridge the gap between developers and
      enterprise standards and offers a more comprehensive vision of how
      software development performed under “quality effectiveness”
      practices can significantly improve the results.</p>
    <p>This article focuses on my current experience in a large
      industrial software factory where I’m trying to combine
      developers’ expectations with project managers’ needs and
      corporate procedures through an integrated approach in order to
      achieve the right level of quality performance. At the same time,
      I’m trying to combine agility with effectiveness.</p>

    <h2>The enterprise point of view</h2>
    <p>A software company must provide its customers with software at
      the right level of quality, producing it at the right level of
      economic and organizational efficiency, in compliance with
      consolidated quality standards, most of time using
      state-of-the-art technologies and, finally, bringing innovation
      into the Information Technology market.</p>
    <p>
      The pressure for standardization is very high, in particular if
      your development process must comply with quality certifications
      such as <a target="_blank"
        href="https://en.wikipedia.org/wiki/ISO_9000">ISO</a>, <a
        target="_blank"
        href="http://en.wikipedia.org/wiki/Information_Technology_Infrastructure_Library">ITIL</a>
      and <a target="_blank"
        href="http://en.wikipedia.org/wiki/Capability_Maturity_Model_Integration">CMMi</a>
      standards and you must provide both your certification authorities
      with the evidence of the continuous improvement of quality
      practices through an effective measurement process, and your top
      managers with the continuous enhancement of productivity.
    </p>

    <h2>The developer’s point of view</h2>
    <p>This opens a wide debate: what is a developer and, mainly, a good
      developer? If I knew the answer, I could give him the right
      support and tools. Once again, am I facing this question from the
      developer’s point of view or from the enterprise point of view?</p>
    <p>In any organization you can find many types of developers:
      workhorses, good programmers, rock stars and great programmers. We
      must balance all talents and attitudes, but let’s stay in the
      middle now.</p>
    <p>A good programmer wants to release good code. It is measured by
      evaluating what he puts out, instead of what he knows. He is
      brilliant and reliable; he stands for simplicity and excellence.
      He can inspire his team to do the same. A good programmer is
      competent and writes “solid” code. This is not “clever code”: it’s
      well-organized, adopt standards and use comments, it is reusable
      and well-tested. A good programmer does things in a
      straightforward way.</p>
    <p>But a really good programmer carries an abiding passion for
      programming. It is competent, wants to achieve the expected
      outcomes and strives to give its contribution to the team. It’s
      open to new approaches and refines its skills by learning and
      testing new technologies, trying out new languages, exploring new
      tools and reading about programming. He likes to solve challenging
      problems and finds his satisfaction in finding the best solution
      for every requirement.</p>
    <p>At the end, the real question is not “what a good programmer is”
      but “what a good programmer needs”. It’s about giving stage to
      developers’ voice.</p>

    <h2>The need of a different point of view</h2>
    <p>According to my open approach, which I have adopted long time
      ago, I know that you cannot impose a development model, but that
      you can influence its affirmation. You must be able to integrate
      different points of view, which follow.</p>
    <p>The economic one – it’s obvious; moreover only commoditizing
      repeatable tasks, can an enterprise focus on funding new and
      brilliant developments.</p>
    <p>The technical one, because programmers want to release good code
      and the enterprise must adopt good development processes.</p>
    <p>The social one, meaning listening to end users, to integrators
      that are enhancing our products and giving stage to the voice of
      project managers as well as developers.</p>
    <p>It’s about collecting both quantitative and qualitative data that
      can be used to measure performances in order to actually measure
      my quality and productivity.</p>
    <p>Consequently, at Engineering Group I’m currently promoting a new
      approach to measure productivity. Engineering’s Software Labs
      (ESL) include hundreds of software engineers and developers
      committed to providing enterprises and public administrations with
      software applications based on state-of-the-art technologies and
      consolidated quality standards, bringing innovation into the
      Information Technology market. Nowadays this market demands
      agility, i.e.: reduction of delivery times, iterative cycles,
      effectiveness and quality.</p>
    <p>
      ESL has developed a specific platform which includes processes and
      tools supporting the effectiveness of project management and <a
        target="_blank"
        href="http://en.wikipedia.org/wiki/Application_lifecycle_management">Application
        Lifecycle Management</a> (ALM) activities, also integrating the
      infrastructure of the Quality Assurance (QA) department. The
      general infrastructure supports the continuous improvement of
      quality practices, the measurement and enhancement of
      productivity, as well as the assessment of development processes
      in compliance with quality standards, such as ISO and CMMi.
    </p>

    <h2>The technological solution</h2>
    <p>The technical infrastructure is based on a portal integrating a
      set of applications allowing to manage project documents, risks,
      the traceability and development of requirements, test cases,
      through reports, graphs, statistics and dashboards, supporting an
      effective activity monitoring.</p>
    <p>This allows the immediate use of dedicated tools and to retrieve
      updated information supporting the historical data analysis. At
      the same time, this information becomes part of the corporate
      repository of lessons learned in the project management domain.</p>
    <p>
      The requirements traceability and development are managed through
      a project management tool, which is free for open source projects
      (<a target="_blank"
        href="https://www.atlassian.com/software/jira/">Atlassian Jira</a>).
      Its flexibility and the availability of a powerful workflow engine
      facilitate the implementation of the processes defined by the QA
      department for Requirements Management and ALM, through general
      processes that are applicable to any new project, without
      necessarily reinventing the wheel every time. Thanks to this tool,
      the communication among the project management environments and
      the development ones has been enhanced. This improves the task
      notification and effort estimation processes and the recording of
      the time spent on each activity, which the developers can perform
      through the user-friendly interface of their development tool.
    </p>
    <p>
      In fact, the <a target="_blank"
        href="http://en.wikipedia.org/wiki/Integrated_development_environment">Integrated
        Development Environment</a> (IDE) is Eclipse. In particular,
      adoption of <a target="_blank"
        href="http://www.eclipse.org/mylyn/">Eclipse Mylyn</a> solve the
      context switching issue, reducing information overload and making
      multitasking activities easy. This allows the programmer to focus
      every time only on the current requirement, connecting all the
      related source code, to make impact analysis and facilitate
      handover of development tasks.
    </p>

    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.1.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.1.png"
      alt="mylyn screen" class="img-responsive" /></a><br />
    <p>Fig. 1 – Mylyn screen</p>
    <br />
    <p>
      The integration with a versioning system (<a target="_blank"
        href="https://subversion.apache.org/">Subversion</a>) allowed an
      effective information traceability, from the requirement
      definition to the realisation of the final work product.
    </p>

    <p>Thanks to the adoption of a flexible model, the QA department can
      use the same project management tool to track internal audits on
      projects. The information on each performed audit is linked to
      possible issues raised during the process, as well as the
      instructions to solve them. This way, the QA department has full
      traceability of its activities, shared with the addressees of the
      audits, ensuring project and process improvement.</p>
    <p>
      Testing activities are managed through a specific open source tool
      (<a target="_blank" href="http://testlink.org/">TestLink</a>) that
      is integrated with the project management tool, allowing you to
      link requirements to test cases and to record bugs, test cases and
      test results .


    <p>
      The system is monitored by <a target="_blank"
        href="http://www.spagoworld.org/xwiki/bin/view/Spago4Q/">Spago4Q</a>,
      which is an analytical application of <a target="_blank"
        href="http://www.spagobi.org/">SpagoBI</a> - the open source
      business intelligence suite. Spago4Q is a vertical solution
      supporting the measurement, analysis and monitoring of products,
      processes and services. It allows to build reports, graphs,
      statistics and dashboards on a single or a set of projects,
      enriched with KPIs to monitor project performances, retrieve
      information from different sources and integrate them in coherent
      meta-models.
    </p>

    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.2.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.2.png"
      alt="Spago4Q architecture" width="500" /></a><br />
    <p>Fig. 2 – Spago4Q architecture</p>
    <br />

    <p>All analytic views are built with SpagoBI Studio - a report and
      dashboard development environment based on Eclipse.</p>
    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.3.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.3.png"
      alt="SpagoBI Studio" class="img-responsive" /></a><br />
    <p>Fig. 3a & 3b – SpagoBI Studio</p>
    <br>

    <p>
      Moreover, a specific Jira connector, leveraging Jira REST API, was
      realized to allow the easy realization of <a target="_blank"
        href="http://www.eclipse.org/birt/">Eclipse BIRT</a> reports,
      that are integrated in SpagoBI, eliminating any need of
      interaction with databases and the correlated security concerns.
    </p>
    <p>Spago4Q collect measures from the previously described tools and
      provides performance values on three dimensions of analysis:
      Economic, Social and Technical. It allows the software factory to
      measure and increase the productivity level, combining
      quantitative data (economic and technical measures and metrics)
      and qualitative data (customers', integrators' and users' level of
      satisfaction), according to the QEST 3D model.</p>
    <p>
      Qualitative data are collected by means of an open source survey
      software tool (<a target="_blank"
        href="https://www.limesurvey.org/en/">LimeSurvey</a>) where
      respondents’ answers are evaluated adopting the <a target="_blank"
        href="http://www.netpromoter.com/why-net-promoter/know/">Net
        Promoter Score</a> (NPS) approach, using <a target="_blank"
        href="http://en.wikipedia.org/wiki/Six_Sigma">Six-Sigma</a>
      Transfer Functions.
    </p>

    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.4.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.4.png"
      alt="QEST dashboard" width="500" /></a><br />
    <p>Fig. 4 – QEST dashboard</p>
    <br>

    <p>The iterative definition, collection and analysis of
      multidimensional measures at each life cycle phase offers the
      feedback required to make adjustments to the project processes in
      a timely fashion, both for the next phase and for designing future
      improvements to the process of the preceding phase. Drill-down
      capabilities provide both a unified view of global performance,
      detailed views on single processes, as well as performance
      comparisons.</p>

    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.5.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.5.png"
      alt="QEST drilldown" class="img-responsive" /></a><br />
    <p>Fig. 5 – QEST drill-down</p>
    <br>

    <p>The current list of goals defined for each dimension of analysis
      follows.</p>
    <ul>
      <li>Economic dimension:
      <ul>
        <li>improve productivity</li>
        <li>reduce maintenance effort</li>
        <li>reduce rework</li>
        <li>improve development resource allocation</li>
        <li>optimize usage of hardware resources</li>
      </ul></li>
      <li>Technical dimension:
      <ul>
        <li>reduce resolution time for defects and technical issues</li>
        <li>improve software delivery time</li>
        <li>improve quality of testing processes</li>
        <li>improve quality of source code</li>
      </ul></li>
      <li>Social dimension:
      <ul>
        <li>improve software compliance with corporate standards and
          procedures</li>
        <li>improve function reuse</li>
        <li>evaluate improvements on training skills for organizational
          resources</li>
        <li>improve the level of satisfaction of customers, integrators
          and developers</li>
        <li>foster knowledge sharing.</li>
      </ul></li>
    </ul>
    <p>All the tools listed in this paragraph are just a selection of a
      wider list of tools currently used at the Engineering Software
      Labs. The technological infrastructure grants high level of
      flexibility and scalability to integrate different tools, also to
      process the same activity (e.g.: a different tool for project
      management or a new tool for code quality analysis).</p>

    <h2>The theoretical support</h2>
    <p>
      Any model should be reliable. Consequently, a theoretical support
      is requested. Very few open source solutions deal with the
      measurement, monitoring and control of projects, accomplishing
      with the requirements from well-known models such as CMMi or <a
        target="_blank"
        href="http://en.wikipedia.org/wiki/ISO/IEC_15504">ISO 15504</a>
      (aka SPICE). Also, only limited experience has been done in using
      open source tools to achieve a real, comprehensive, measurable
      quality level in OSS development itself.
    </p>
    <p>
      The technological infrastructure described above implements <a
        target="_blank"
        href="http://citeseerx.ist.psu.edu/viewdoc/download?rep=rep1&type=pdf&doi=10.1.1.15.1413">QEST
        nD model</a>, a performance measurement model, according to its
      three-dimensional (3D) implementation (Quality factor + Economic,
      Social and Technical).
    </p>

    <a
      href="/community/eclipse_newsletter/2014/september/images/article2.6.png"><img
      src="/community/eclipse_newsletter/2014/september/images/article2.6.png"
      alt="QEST model" width="400" /></a><br />
    <p>Fig. 6 – QEST model</p>
    <br>

    <p>
      The general reference model is <a target="_blank"
        href="http://books.google.it/books?id=jOgswrMBsp4C&pg=PA7&lpg=PA7&dq=PMAI+Plan-Measure-Assess-Improve&source=bl&ots=aQZUo7dtRY&sig=MnCLSETHH7b4lD-cQJuJk9uznXA&hl=it&sa=X&ei=tIr6UertLKzn4QSm8oDADg&ved=0CDEQ6AEwAA#v=onepage&q=PMAI%20Plan-Measure-Assess-Improve&f=true">PMAI</a>,
      which enables the constant monitoring and assessment of the level
      of quality and cost-effectiveness of software development
      processes and practices. Specifically, the process includes four
      steps:
    </p>
    <ul>
      <li>Plan: define dimensions and metrics of analysis</li>
      <li>Measure: collect data to measure global performance value</li>
      <li>Assess: analyze results through business intelligence tools</li>
      <li>Improve: identify bottlenecks, solve issue and define areas
        for improvement</li>
    </ul>
    <p>Achieved goals include the continuous improvement of quality
      practices, the measurement and enhancement of productivity, as
      well as the assessment of development processes in compliance with
      quality standards, such as ISO, ITIL and CMMi.</p>

    <h2>Building bridges</h2>
    <p>The use of an integrated, low-cost and mostly open source
      solution, which can be easily extended and integrated with other
      software development tools, has been a key success factor at
      Engineering Group, fostering the adoption of well-defined ALM
      processes and an effective software lifecycle management.</p>
    <p>This solution can integrate other applications and tools,
      reducing the duplication of information and fostering the sharing
      of lessons learned. Moreover, it can be used in different
      contexts, such as supporting the trustworthiness of information
      provided by open source communities and projects.</p>
    <p>The innovative approach of this multi-dimensional analysis of the
      productivity – in a wider sense - offers access to complete
      knowledge of software production, its quality and of the human
      relations involved in the development phase. Standard models like
      QEST that are not bound to a specific context are suitable to a
      large variety of heterogeneous software productions environments.
    </p>

    <p>At the beginning of this article I outlined my role in building
      integrations. This can be implemented both in bridging existing
      gaps in project quality activities and in building bridges between
      communities and organizations. In this article I mentioned tools
      provided by different open source communities or vendors. Open
      source is evolving, IT technology is evolving as well. To manage
      this, you must adopt an open approach, continuously monitor what’s
      happening in the market, make the right choices and be ready to
      make a "turnaround" when necessary.</p>
    <p>This has a lot to do with openness, collaboration and innovation.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/september/images/Gabriele.png"
        width="90" alt="gabriele ruffatti" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Gabriele Ruffatti<br />
        <a target="_blank" href="http://www.eng.it/">Engineering Group</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.spagoworld.org/blog/">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/gruffatti">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
