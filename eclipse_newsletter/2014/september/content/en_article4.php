<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  <a target="_blank" href="http://www.bonitasoft.com/">Bonitasoft</a> is
  a long-time user of <a href="https://www.eclipse.org/swtbot/">SWTBot
    framework</a>. The framework has been used to test UI since the
  beginning of Bonitasoft, 5 years ago. It was first used exclusively by
  "Eclipse-knowledgeable" developers because of its maintainability and
  stabilization difficulties; and also with the limitations on the
  testable components. Based on experience and evolutions, it is now
  used by Developers and QA Team in a smoother way.
</p>
<h2>1. Test components you wish</h2>
<p>
  SWTBot supports various components. Of course, all SWT components are
  supported but it also provides tools for advanced components such as <a
    href="https://www.eclipse.org/gef/">GEF</a>, <a
    href="https://www.eclipse.org/articles/Article-Forms/article.html">Eclipse
    Forms</a> or <a href="https://www.eclipse.org/nebula/">Nebula Widget</a>.
</p>

<p>Are you using a specific component?</p>

<p>
  No problem! Adding support for a <b>custom component</b> is quite
  easy. For instance, we implemented support for <a
    href="https://www.eclipse.org/nebula/widgets/gallery/gallery.php">Nebula
    Gallery</a> and <a
    href="https://www.eclipse.org/nebula/widgets/tablecombo/tablecombo.php">Nebula
    TableCombo</a>. You can find the general idea of how to implement
  yours in this <a
    href="https://wiki.eclipse.org/SWTBot/Testing_Custom_Controls">wiki
    page</a>.
</p>

<p>
  Nebula Gallery support has been integrated into the SWTBot project and
  will be available with the next release (thanks to <a target="_blank"
    href="https://code.google.com/p/gerrit/">Gerrit</a> and SWTBot
  Project Lead <a target="_blank"
    href="https://twitter.com/mickaelistria">Mickaël Istria</a>).
</p>

<a
  href="/community/eclipse_newsletter/2014/september/images/article4.1.jpg"><img
  src="/community/eclipse_newsletter/2014/september/images/article4.1.jpg"
  alt="gallery" width="600" /></a>
<br />

<h2>2. Let Testers write Functional tests</h2>

<p>
  At the beginning, writing SWTBot tests was an Eclipse experts' affair.
  As mentioned on the project description page, <i>"SWTBot provides APIs
    that are simple to read and write"</i>. That’s totally true... <i>for
    "Java developers"</i>.
</p>
<p>The first tests are quite straightforward to implement, the
  developers have an easy access to the source code that they just wrote
  and the SWTBot API is really easy to use when you have minimal
  Eclipse/SWT skills.</p>

<p>It becomes tricky when you consider the following statements:</p>
<ul>
  <li>You have to <b>find the correct widget</b> in old source code -
    Who is able to tell the difference between a <i>Combo</i> and a <i>CCombo</i>
    at first sight?
  </li>
  <li>The execution of the test requires that you <b>start the whole
      Platform</b> - For us, in debug mode, it can take between 30
    seconds and 1 minute.
  </li>
  <li>The test requires that there be <b>no interaction with the UI</b>
    during its execution - Don't touch the keyboard or move your mouse!
    (The exception is that you are using Linux and have configured the
    tests to launch on another Display as explained <a
    href="https://wiki.eclipse.org/SWTBot/Automate_test_execution#use_another_DISPLAY_to_save_time">here</a>.)
    </li>

</ul>
<p>
  Considering these issues, watching a test failing because a <i>".click()"</i>
  is missing or there is a misspelled button label can be frustrating!
  In addition, each new test was resulting in - if not duplicating - a
  lot of similar code.
</p>

<p>
  With experience we start gathering some of the most recurrent actions
  in static methods (e.g : <i>SWTBotTestUtil.createNewDiagram(bot))</i>.
  That was a little better but not very satisfying as a lot of these
  methods were emerging here and there.
</p>

<p>
  Up to this point only developers were writing tests. One objective of
  our Engineering Team was to <b>enable the automation of functional
    tests for our QA Team</b>. To be able to do this we decided to
  develop an in-house framework on top of SWTBot using the <b>Page
    Pattern</b> (see <a target="_blank"
    href="http://martinfowler.com/bliki/PageObject.html">Martin Fowler's
    PageObject</a> article and <a target="_blank"
    href="https://code.google.com/p/selenium/wiki/PageObjects">Selenium
    wiki</a>). This pattern was already used internally for our web
  component.
</p>

<a
  href="/community/eclipse_newsletter/2014/september/images/article4.2.png"><img
  src="/community/eclipse_newsletter/2014/september/images/article4.2.png"
  alt="swtbot_page_pattern" width="600" /></a>
<br />


<a
  href="/community/eclipse_newsletter/2014/september/images/article4.3.png"><img
  style="padding-left: 5px"
  src="/community/eclipse_newsletter/2014/september/images/article4.3.png"
  alt="variablePage" width="300" /></a>
<br />

<p>
  This new framework provides a <b>fluent API</b>, for example:
</p>

<pre>
  <i>
  BotDataDialog botDataDialog =
  myBotApplication.newDiagram()
.getPropertiesView().
selectDataTab().addData();
botDataDialog .setName(“userName”)
.setType(“Text”)
.setDefaultValue(“john”).finish();
</i>
</pre>

<p>This snippet will add a variable in a process and fill out this
  following wizard page.</p>

<p>
  An API like this opens up the SWTBot testing capabilities to users
  with less technical profiles because of its Application Oriented
  Language. It is <b>easier to develop, read and maintain</b>. Even as a
  developer, you will spend less time writing a new <i>"PageObject"</i>
  and reuse it than writing specific SWTBot code for your test.


<h2>3. Ensure repeatable tests</h2>
<p>Writing repeatable tests is not completely trivial as we are dealing
  with asynchronous code (the UI is executed in a dedicated thread).</p>

<p>Let’s illustrate with a little story. All of our SWTBot tests were
  running smoothly but slowly on our Continuous Integration (CI) server.
  A great effort has been made to improve performance of the whole CI
  infrastructure. One phase consisted of updating hardware. The test
  execution was so much faster than before!!!... that it broke a lot of
  our UI tests. Wait what? While going faster, the SWTBot test thread is
  going too fast for the UI one.</p>

<p>What can you do to make sure you have a robust test suite?</p>
<p>You can follow these little guidelines:</p>
<ul>
  <li>Use <b>Wait Condition</b> intensively to ensure that the UI thread
    is where you expect it to be. For instance, wait for a shell to be
    opened, wait until a validation ends its run and enables a <i>"Finish"</i>
    button.
  </li>
  <li><b>Close remaining opened shells</b> after a test has finished to
    avoid failing all upcoming tests of the test suite. You can use a
    JUnit RunListener as explained <a target="_blank"
    href="http://community.bonitasoft.com/user/login?destination=node/15853">in
      this blog post</a> or a common abstract class as explained <a
    target="_blank"
    href="http://www.lorenzobettini.it/2014/01/testing-a-plain-swt-application-with-swtbot/">here</a>
    for a plain SWT application.</li>
  <li>Use the <b>Internationalized String</b> and not the hardcoded
    value of text displayed in the UI (even if hardcoding seems simpler
    at test writing time). For instance, while using NLS framework, you
    can simply export the package containing your NLS class as x-friends
    for your test plugin. You can check this <a target="_blank"
    href="http://community.bonitasoft.com/user/login?destination=node/15854">article</a>
    for more details.
  </li>
</ul>
<h2>What you should remember</h2>
<p>
  Following some simple rules, developers are able to provide an
  easy-to-use and reliable Test Framework on top of their application.
  This framework eases <b>writing and maintaining Functional Tests for
    Testers AND Developers</b>.


<h2>What's next?</h2>
<p>
  Now that we have a clean Application Oriented API, it will be much
  easier to get <b>stakeholders and user advocates to join the party</b>
  using some Business Driven Testing technology. This <a target="_blank"
    href="https://www.eclipsecon.org/europe2014/session/cooking-eclipse-plugins-bdd-cucumber-swtbot-and-tycho-0">talk</a>
  planned for <a target="_blank"
    href="https://www.eclipsecon.org/europe2014/pqd">Project Quality Day</a>
  at <a target="_blank" href="https://www.eclipsecon.org/europe2014/">EclipseCon
    Europe</a> might provide some inspiration

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/aurelien100.jpg"
        width="90" alt="Aurélien Pupier" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Aurélien Pupier<br />
        <a target="_blank" href="http://www.bonitasoft.com/">Bonitasoft</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://www.bonitasoft.com/for-you-to-read/blog">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/apupier">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/september/images/romain.jpg"
        alt="romain bioteau" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Romain Bioteau<br />
        <a target="_blank" href="http://www.bonitasoft.com/">Bonitasoft</a>
      </p>
      <ul class="author-link">
        <!-- <li><a target="_blank" href="http://koehnlein.blogspot.de/">Blog</a> </li> -->
        <li><a target="_blank" href="https://twitter.com/rbioteau">Twitter</a></li>
        <!-- <li><a target="_blank" href="https://plus.google.com/116635061773096754601/posts">Google +</a></li>
					$og  -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

