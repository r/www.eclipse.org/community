<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    The IoT needs standard protocols.<br> Two of the most promising for
    small devices are MQTT and CoAP.
  </p>

  <p>Both MQTT and CoAP:</p>
  <ul>
    <li>Are open standards</li>
    <li>Are better suited to constrained environments than HTTP</li>
    <li>Provide mechanisms for asynchronous communication</li>
    <li>Run on IP</li>
    <li>Have a range of implementations</li>
  </ul>
  <p>
    MQTT gives flexibility in communication patterns and acts purely as
    a pipe for binary data.<br> CoAP is designed for interoperability
    with the web.
  </p>

  <h2>MQTT</h2>

  <p>MQTT is a publish/subscribe messaging protocol designed for
    lightweight M2M communications. It was originally developed by IBM
    and is now an open standard.</p>

  <h3>Architecture</h3>

  <p>MQTT has a client/server model, where every sensor is a client and
    connects to a server, known as a broker, over TCP.</p>

  <p>MQTT is message oriented. Every message is a discrete chunk of
    data, opaque to the broker.</p>
  <p>Every message is published to an address, known as a topic. Clients
    may subscribe to multiple topics. Every client subscribed to a topic
    receives every message published to the topic.</p>

  <p>For example, imagine a simple network with three clients and a
    central broker.</p>
  <p>
    All three clients open TCP connections with the broker. Clients B
    and C subscribe to the
    <code>topic temperature</code>
    .
  </p>

  <img
    src="/community/eclipse_newsletter/2014/february/images/febarticle2.1.png"
    alt="temperature" />
  <br>

  <p>
    At a later time, Client A publishes a value of
    <code>22.5</code>
    for topic
    <code>temperature</code>
    . The broker forwards the message to all subscribed clients.
  </p>

  <img
    src="/community/eclipse_newsletter/2014/february/images/febarticle2.2.png"
    alt="temperature 22" />
  <br>

  <p>The publisher subscriber model allows MQTT clients to communicate
    one-to-one, one-to-many and many-to-one.</p>

  <h3>Topic matching</h3>

  <p>In MQTT, topics are hierarchical, like a filing system (eg.
    kitchen/oven/temperature). Wildcards are allowed when registering a
    subscription (but not when publishing) allowing whole hierarchies to
    be observed by clients.</p>
  <p>The wildcard + matches any single directory name, # matches any
    number of directories of any name.</p>
  <p>
    For example, the topic
    <code>kitchen/+/temperature matches kitchen/foo/temperature but not
      kitchen/foo/bar/temperature</code>
  </p>
  <p>
    <code>kitchen/# matches kitchen/fridge/compressor/valve1/temperature</code>
  </p>

  <h3>Application Level QoS</h3>

  <p>MQTT supports three quality of service levels, “Fire and forget”,
    “delivered at least once” and “delivered exactly once”.</p>

  <h3>Last Will And Testament</h3>

  <p>MQTT clients can register a custom “last will and testament”
    message to be sent by the broker if they disconnect. These messages
    can be used to signal to subscribers when a device disconnects.</p>

  <h3>Persistence</h3>

  <p>MQTT has support for persistent messages stored on the broker. When
    publishing messages, clients may request that the broker persists
    the message. Only the most recent persistent message is stored. When
    a client subscribes to a topic, any persisted message will be sent
    to the client.</p>
  <p>Unlike a message queue, MQTT brokers do not allow persisted
    messages to back up inside the server.</p>

  <h3>Security</h3>

  <p>MQTT brokers may require username and password authentication from
    clients to connect. To ensure privacy, the TCP connection may be
    encrypted with SSL/TLS.</p>

  <h3>MQTT-SN</h3>

  <p>Even though MQTT is designed to be lightweight, it has two
    drawbacks for very constrained devices.</p>
  <p>Every MQTT client must support TCP and will typically hold a
    connection open to the broker at all times. For some environments
    where packet loss is high or computing resources are scarce, this is
    a problem.</p>
  <p>MQTT topic names are often long strings which make them impractical
    for 802.15.4.</p>

  <p>Both of these shortcomings are addressed by the MQTT-SN protocol,
    which defines a UDP mapping of MQTT and adds broker support for
    indexing topic names.</p>

  <h2>CoAP</h2>

  <p>CoAP is the Constrained Application Protocol from the CoRE
    (Constrained Resource Environments) IETF group.</p>

  <h3>Architecture</h3>

  <p>Like HTTP, CoAP is a document transfer protocol. Unlike HTTP, CoAP
    is designed for the needs of constrained devices.</p>

  <p>CoAP packets are much smaller than HTTP TCP flows. Bitfields and
    mappings from strings to integers are used extensively to save
    space. Packets are simple to generate and can be parsed in place
    without consuming extra RAM in constrained devices.</p>

  <p>CoAP runs over UDP, not TCP. Clients and servers communicate
    through connectionless datagrams. Retries and reordering are
    implemented in the application stack. Removing the need for TCP may
    allow full IP networking in small microcontrollers. CoAP allows UDP
    broadcast and multicast to be used for addressing.</p>

  <p>CoAP follows a client/server model. Clients make requests to
    servers, servers send back responses. Clients may GET, PUT, POST and
    DELETE resources.</p>

  <p>CoAP is designed to interoperate with HTTP and the RESTful web at
    large through simple proxies.</p>

  <p>Because CoAP is datagram based, it may be used on top of SMS and
    other packet based communications protocols.</p>

  <h3>Application Level QoS</h3>

  <p>Requests and response messages may be marked as “confirmable” or
    “nonconfirmable”. Confirmable messages must be acknowledged by the
    receiver with an ack packet.</p>
  <p>Nonconfirmable messages are “fire and forget”.</p>

  <h3>Content Negotiation</h3>

  <p>
    Like HTTP, CoAP supports content negotiation. Clients use
    <code>Accept</code>
    options to express a preferred representation of a resource and
    servers reply with a
    <code>Content-Type</code>
    option to tell clients what they’re getting. As with HTTP, this
    allows client and server to evolve independently, adding new
    representations without affecting each other.
  </p>

  <p>
    CoAP requests may use query strings in the form
    <code>?a=b&amp;c=d</code>
    . These can be used to provide search, paging and other features to
    clients.
  </p>

  <h3>Security</h3>

  <p>Because CoAP is built on top of UDP not TCP, SSL/TLS are not
    available to provide security. DTLS, Datagram Transport Layer
    Security provides the same assurances as TLS but for transfers of
    data over UDP. Typically, DTLS capable CoAP devices will support RSA
    and AES or ECC and AES.</p>

  <h3>Observe</h3>

  <p>CoAP extends the HTTP request model with the ability to observe a
    resource. When the observe flag is set on a CoAP GET request, the
    server may continue to reply after the initial document has been
    transferred. This allows servers to stream state changes to clients
    as they occur. Either end may cancel the observation.</p>

  <h3>Resource Discovery</h3>

  <p>CoAP defines a standard mechanism for resource discovery. Servers
    provide a list of their resources (along with metadata about them)
    at /.well-known/core. These links are in the application/link-format
    media type and allow a client to discover what resources are
    provided and what media types they are.</p>

  <h3>NAT Issues</h3>

  <p>In CoAP, a sensor node is typically a server, not a client (though
    it may be both). The sensor (or actuator) provides resources which
    can be accessed by clients to read or alter the state of the sensor.</p>
  <p>As CoAP sensors are servers, they must be able to receive inbound
    packets. To function properly behind NAT, a device may first send a
    request out to the server, as is done in LWM2M, allowing the router
    to associate the two. Although CoAP does not require IPv6, it is
    easiest used in IP environments where devices are directly routable.</p>

  <h2>Comparison</h2>

  <p>MQTT and CoAP are both useful as IoT protocols, but have
    fundamental differences.</p>

  <p>MQTT is a many-to-many communication protocol for passing messages
    between multiple clients through a central broker. It decouples
    producer and consumer by letting clients publish and having the
    broker decide where to route and copy messages. While MQTT has some
    support for persistence, it does best as a communications bus for
    live data.</p>

  <p>CoAP is, primarily, a one-to-one protocol for transferring state
    information between client and server. While it has support for
    observing resources, CoAP is best suited to a state transfer model,
    not purely event based.</p>

  <p>MQTT clients make a long-lived outgoing TCP connection to a broker.
    This usually presents no problem for devices behind NAT. CoAP
    clients and servers both send and receive UDP packets. In NAT
    environments, tunnelling or port forwarding can be used to allow
    CoAP, or devices may first initiate a connection to the head-end as
    in LWM2M.</p>

  <p>MQTT provides no support for labelling messages with types or other
    metadata to help clients understand it. MQTT messages can be used
    for any purpose, but all clients must know the message formats
    up-front to allow communication. CoAP, conversely, provides inbuilt
    support for content negotiation and discovery allowing devices to
    probe each other to find ways of exchanging data.</p>

  <p>Both protocols have pros and cons, choosing the right one depends
    on your application.</p>
  <p>Do experiments, build prototypes and deploy test devices on
    networks.</p>

  <p>
    Toby Jaffey is Founder and CTO at <a target="_blank"
      href="http://1248.io">1248</a> which provides knowledge and
    systems to help manufacturers connect their products quickly and
    effectively, at scale.
  </p>

  <h2>Further Reading</h2>

  <h3>MQTT</h3>
  <ul>
    <li><a target="_blank" href="http://mqtt.org/">Community website</a></li>
    <li><a target="_blank"
      href="http://www.ibm.com/developerworks/webservices/library/ws-mqtt/index.html">Specification</a></li>
    <li>Open source implementations:
    <ul>
      <li><a target="_blank" href="http://www.eclipse.org/paho/">Paho</a></li>
      <li><a target="_blank" href="http://mosquitto.org/">Mosquitto</a></li>
      <li><a target="_blank" href="https://github.com/adamvr/MQTT.js/">GitHub</a></li>
    </ul></li>
    <li><a target="_blank"
      href="https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=mqtt">Standards
        working group</a></li>
  </ul>
  <h3>CoAP</h3>
  <ul>
    <li><a target="_blank" href="http://www.ipso-alliance.org/">IP for
        Smart Objects Alliance</a></li>
    <li><a target="_blank"
      href="http://tools.ietf.org/html/draft-ietf-core-coap">Specification</a></li>
    <li>Open source implementations:
    <ul>
      <li><a target="_blank"
        href="http://sourceforge.net/projects/libcoap/">SourceForge</a></li>
      <li><a target="_blank" href="https://github.com/morkai/h5.coap">GitHub</a></li>
      <li><a target="_blank" href="http://www.contiki-os.org/">Contiki</a></li>
    </ul></li>
    <li><a target="_blank"
      href="https://addons.mozilla.org/en-US/firefox/addon/copper-270430/">Browser
        plugin</a></li>
    <li><a target="_blank"
      href="http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm">REST</a></li>
    <li><a target="_blank" href="http://tools.ietf.org/wg/core/">Standards
        working group</a></li>
</ul>

    <p>
      This work is licensed under a <a target="_blank"
        href="http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US">Creative
        Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
        License</a>.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/february/images/toby.png"
        width="75" alt="toby jaffey" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Toby Jaffey<br />
        <a target="_blank" href="http://1248.io/">1248</a>
      </p>
      <ul class="author-link">
        <!-- <li><a target="_blank" href="http://robilad.livejournal.com/">Blog</a></li> -->
        <li><a target="_blank" href="https://twitter.com/tobyjaffey">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/+IanSkerrett">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
