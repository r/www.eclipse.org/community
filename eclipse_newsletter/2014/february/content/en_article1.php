<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <a target="_blank" href="http://iot.eclipse.org/protocols.html"><img
    src="/community/eclipse_newsletter/2014/february/images/iot_news.png"
    width="250" alt="iot logo" /></a>
  <br>
  <br />
  <p>The Internet of Things (IoT) and Machine to Machine (M2M) are
    becoming important trends in the technology industry. The potential
    to connect sensors, actuators and a wide range of devices opens up
    an enormous opportunity for creating new types of solutions and
    applications in almost any industry. However, the current state of
    the IoT industry is characterised by closed proprietary solutions
    that limit interoperability between solutions and lock customers
    into a particular technology. If the Internet of Things is going to
    be successful it needs to be built on the principles that made the
    Internet successful – open standards and open source software.</p>
  <p>At Eclipse we are creating an open source community that will
    provide the open source building blocks for creating an open and
    interoperable Internet of Things. The Eclipse IoT community provides
    open source projects that developers can use to build their IoT
    solutions. The focus is on providing 1) open source implementations
    of popular IoT standards, 2) frameworks and services required for
    building IoT solutions, and 3) tools required by IoT developers.</p>
  <p>The first Eclipse IoT project started in November 2012. We now have
    12 different open source projects targeting IoT and M2M. Here is a
    quick tour of these projects.</p>

  <h2>Standards and Protocols</h2>
  <ol>
    <li><a target="_blank" href="http://www.eclipse.org/paho/">Paho</a>
      provides the client implementation of the Oasis MQTT messaging
      protocol. Paho includes Java, C, C++, Python, JavaScript and other
      language implementation of the MQTT standard.</li>
    <li><a target="_blank"
      href="http://projects.eclipse.org/projects/technology.mosquitto">Mosquitto</a>
      is the sister project of Paho, providing a server implementation
      of MQTT. We are hosting a test instance of Mosquitto at
      iot.eclipse.org so developers can easily test out their MQTT based
      applications.</li>
    <li><a target="_blank"
      href="http://www.eclipse.org/proposals/technology.californium/">Californium</a>
      is a Java implementation of Constrained Application Protocol. The
      project is just getting started at Eclipse but the code is
      available on GitHub.</li>
    <li><a target="_blank"
      href="http://projects.eclipse.org/projects/technology.om2m">OM2M</a>
      is an implementation of the OneM2M standard (previously called
      ETSi M2M standard). OM2M is a set of Java and OSGi services that
      implement the OneM2M standard.</li>
    <li><a target="_blank"
      href="http://www.eclipse.org/proposals/technology.liblwm2m/">Wakaama</a>
      is an implementation of the OMA Lightweight M2M protocol for
      device and service management. Wakaama is written in C and
      designed to be portable to POSIX compliant systems.</li>
  </ol>

  <h2>Services and Frameworks</h2>
  <ol>
    <li><a target="_blank"
      href="http://projects.eclipse.org/projects/technology.kura">Kura</a>
      is a set of Java and OSGi services that implement the common
      services required for an IoT gateway, such as 1) I/O connection
      with serial ports, USB, Bluetooth, GPS, Clock, etc., 2) Data
      Services, 3) Remote Management, and more. We have a more detailed
      introduction to Kura available.</li>
    <li><a target="_blank" href="http://www.eclipse.org/eclipsescada/">Eclipse
        SCADA</a> (Supervisory Control and Data Acquisition) is a set of
      Java and OSGi services for creating industrial control systems
      that monitor and control industrial processes, such as factory
      floors or solar farms.</li>
    <li><a target="_blank" href="http://eclipse.org/smarthome/">Eclipse
        SmartHome</a> is a Java and OSGi set of services for home
      automation integration. This project provides a uniform access
      point for the many different home automation devices and
      protocols.</li>
    <li><a target="_blank" href="http://eclipse.org/ponte/">Ponte</a> is
      a broker that will bridge different IoT standards (such as MQTT
      and CoAP) and provide a REST API to these standards.</li>
    <li><a target="_blank"
      href="http://projects.eclipse.org/projects/rt.concierge">Concierge</a>
      is an implementation of the OSGi standard that aims to have a very
      small runtime footprint so it is suited for small IoT devices. The
      target size for the Concierge OSGi runtime is 300-400KB.</li>
    <li><a target="_blank"
      href="http://eclipse.org/proposals/technology.krikkit/">Krikkit</a>
      is a project to define rules for the messages that pass through an
      edge device.</li>
    <li><a target="_blank" href="http://www.eclipse.org/mihini/">Mihini</a>
      is a Lua-based framework for creating applications running on IoT
      and M2M gateways. Koneki is a closely related project that
      implements a Lua IDE and makes it easy to deploy applications to
      Mihini.</li>
  </ol>

  <p>IoT is an exciting new area for software developers. New hardware
    platforms like Arduino, BeagleBone and Raspberry Pi makes it easy
    for developers to experiment with new solutions. Now is a great time
    to experiment with our open source projects. Use them to build new
    IoT solutions, and perhaps start the next project. An open source
    IoT community is what will drive the innovation in the IoT industry.
    We invite you to join us.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/december/images/ians75.PNG"
        alt="ian skerrett" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ian Skerrett<br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://ianskerrett.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/IanSkerrett">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+IanSkerrett">Google +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
