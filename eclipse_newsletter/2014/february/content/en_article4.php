<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>Classical home automation used to be easy: You installed your
    devices at home (or probably rather had them installed by an
    electrician), wired things up and started using it at home. But then
    came the smartphone era: The consumer got used to the fact that
    everything is controllable through an app from anywhere in the
    world. This expectation did not stop at Smart Home products either,
    through which a new product category - the "IoT gadgets" - was born.</p>

  <p>A very common scheme of how these products work is the following:</p>
  <ul>
    <li>You hook the device(s) up to your local network, either through
      wifi or ethernet cable.</li>
    <li>The device(s) use your permanent Internet connection and create
      a connection to a specific cloud server to which it sends all its
      data and listens for commands.</li>
    <li>A free smartphone app connects to the cloud service, lets you
      configure and control your device(s) and displays you nice
      colorful charts and diagrams.</li>
  </ul>

  <p>There are some obvious drawbacks of such products:</p>
  <ul>
    <li>They only work when your Internet connection is available.</li>
    <li>There is an app per product, you cannot easily achieve
      over-arching integration.</li>
    <li>You depend on the availability of the cloud server - if this is
      shut down (e.g. because the company went out of business), you
      cannot use your device anymore.</li>
  </ul>

  <p>These points alone should be reason enough to consider such
    products as nice gadgets, but not as a serious (meaning permanent
    and reliable) part of your house.</p>

  <p>But there are even many more subtle drawbacks:</p>
  <ul>
    <li>You are not the owner of your data; everything is sent to the
      cloud server, if you wish it or not. What happens with the data is
      not decided by yourself, but by the cloud service. You will only
      receive results of the data mining processes, be it as "smart"
      actions being triggered or as a colorful time series chart. I
      always thought of this as a no-go and wondered that other people
      did not mind this fact. With the acquisition of Nest by Google the
      public awareness seems to have increased a lot, though. Google is
      now seen as <a target="_blank"
      href="http://siliconangle.com/blog/2014/01/14/4-questions-from-google-acquiring-nest-big-brother-now-has-eyes-in-your-smart-home/">Big
        Brother in the smart home</a>, people consider <a
      target="_blank"
      href="http://blogs.forrester.com/fatemeh_khatibloo/14-01-15-googles_nest_acquisition_will_force_the_internet_of_things_privacy_discussion">not
        buying Nest products anymore</a> and even the tabloid press <a
      target="_blank"
      href="http://www.bild.de/digital/multimedia/smarthome/smarthome-wie-sicher-ist-das-34245950.bild.html">contemplates
        about privacy in the smart home</a>.
    </li>
    <li>Even if you have full trust the cloud service company, the NSA
      affair should have shown you that your data is sniffed and stored
      in dubious places around the world. People who say that they do
      not care about being spied on as it is done by "the good ones"
      should definitely read <a target="_blank"
      href="http://www.planetebook.com/ebooks/1984.pdf">"1984"</a> -
      after all, in this novel the surveillance is also only done by the
      "good ones"...
    </li>
    <li>Every device that creates a connection to a cloud service is a
      potential security risk. Most of these devices are embedded
      systems and many lack the possibility of receiving firmware
      updates for vulnerabilities. There are already many examples where
      such systems have been hacked - e.g. for <a target="_blank"
      href="http://www.heise.de/security/meldung/Vaillant-Heizungen-mit-Sicherheits-Leck-1840919.html">heating
        systems</a> or <a target="_blank"
      href="http://www.networkworld.com/community/blog/hacks-turn-your-wireless-ip-surveillance-cameras-against-you">IP
        cameras</a>. <a target="_blank"
      href="http://www.wired.com/opinion/2014/01/theres-no-good-way-to-patch-the-internet-of-things-and-thats-a-huge-problem/">Bruce
        Schneier sees this as one of the major challenges</a> for the
      Internet of Things in the coming years.
    </li>
  </ul>
  <p>Now what is the solution to all these problems? Well, I like to
    call it the INTRANET OF THINGS - all your devices should be in your
    LAN, hidden behind a firewall and locally controlled. All data needs
    to stay in the local network in the first place and only the user
    should decide whether it should be shared with somebody else. For
    remote access, there should be a single channel into your house, not
    one per device.</p>

  <img
    src="/community/eclipse_newsletter/2014/february/images/febarticle4.1.png"
    width="600" alt="internetofthings" />
  <br>
  <p>
    <i>An Intranet of Things protects your privacy and reduces
      vulnerability risks</i>
  </p>

  <p>
    A solution that allows you to create such an Intranet of Things is <a
      target="_blank" href="http://www.openhab.org/">openHAB - the open
      Home Automation Bus</a>. When starting this project 6 years ago, I
    took the perspective of an end user and not of a company - this is
    why data privacy is one of the top priorities since its beginning.
    It is good to see that this discussion is gaining momentum now.
  </p>

  <p>
    The latest version <a target="_blank"
      href="https://github.com/openhab/openhab/wiki/Release-Notes-1.4">openHAB
      1.4 has just been released</a> - it can now integrate more than 60
    different smart Home technologies and protocols into a single
    solution. Just to name a few, this new release brings support for <a
      target="_blank"
      href="https://github.com/openhab/openhab/wiki/Insteon-Hub-Binding">
      INSTEON</a>, <a target="_blank"
      href="https://github.com/openhab/openhab/wiki/Netatmo-Binding">Netatmo</a>,
    <a target="_blank"
      href="https://github.com/openhab/openhab/wiki/Pioneer-AVR-Binding">Pioneer
      AV receivers</a> and <a target="_blank"
      href="https://github.com/openhab/openhab/wiki/MAX%21Cube-Binding">MAX!</a>.
    New native apps for Android and iOS are available as well. To cater
    for secure remote access, we have furthermore just started a private
    beta of a new service: <a target="_blank"
      href="https://my.openhab.org/">my.openHAB</a> will provide you the
    ability to connect to your openHAB over the Internet, securely,
    through commercial SSL certificates, without a need for making any
    holes in your home router and without a need for a static IP or
    dynamic DNS service. It does not store any data, but simply acts as
    a proxy that blindly forwards the communication.
  </p>

  <p>
    The <a target="_blank" href="http://eclipse.org/smarthome/">Eclipse
      SmartHome</a> framework, which is derived from openHAB, is also a
    perfect fit for building smart home solutions that follow the
    "Intranet of Things" idea. We are close to having the first binary
    builds available of Eclipse SmartHome and work on openHAB 2.0 will
    start shortly after (as it will be based on Eclipse SmartHome).
  </p>

  <p>So if you are considering to invest in a smart home solution, I
    hope that this post has brought some important aspects to your
    attention - let's hope that the IoT does NOT bring the loss of all
    privacy, but that we will in future see smart homes that fully
    respect your privacy! If you like the idea of an Intranet of Things,
    start building one yourself and spread the word!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/february/images/kaikreuzer.jpg"
        width="75" alt="kai kreuzer" />
      <p class="author-name">
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Kai Kreuzer<br />
        <a target="_blank" href="http://www.openhab.org/">openHAB UG
          (haftungsbeschränkt)</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://kaikreuzer.blogspot.de/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/kaikreuzer">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+KaiKreuzer">Google +</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
