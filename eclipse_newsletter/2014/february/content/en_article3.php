<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <a target="_blank"
    href="http://www.eclipse.org/proposals/technology.kura/"><img
    src="/community/eclipse_newsletter/2014/february/images/kura_logo_small.png"
    width="200" alt="kura logo" /></a>
  <br>
  <br />
  <p>
    Deploying and configuring one device to act as a node in the
    Internet of Things is relatively easy. Doing the same for hundreds
    or thousands of devices is not so easy though. This is where the new
    Eclipse project <a target="_blank"
      href="http://www.eclipse.org/proposals/technology.kura/">Kura</a>
    comes in.
  </p>

  <p>Eclipse Kura offers a platform that can live at the boundary between the
    private device network and the local network, public Internet or
    cellular network providing a manageable and intelligent gateway for
    that boundary capable of running applications that can harvest
    locally gathered information and deliver it reliably to the cloud.</p>

  <p>
    Kura was contributed to Eclipse by <a target="_blank"
      href="http://www.eurotech.com/en/">Eurotech</a> who developed the
    original technology to run on everything from general purpose
    devices, rugged mobile computers, wearable devices, service gateways
    and vehicle consoles, all the way down to the Raspberry Pi.
    Implemented as a Java-based platform, Kura can be installed on Linux
    based devices and provides a remotely manageable system, complete
    with all the core services applications need and a device
    abstraction layer for accessing the gateway’s own hardware.
  </p>

  <p>
    Developers working with Kura will find that, as it is within an OSGi
    container - <a target="_blank"
      href="http://www.eclipse.org/equinox/">Eclipse Equinox</a>, they
    will be working with a standard framework for handling events,
    packaging code and a range of standard services. An application in
    Kura is delivered as an OSGi module and is run within the container
    along with the other components of Kura. Using the Eclipse Paho MQTT
    library, Kura provides a store-and-forward repository service for
    those applications to take the information gathered from the locally
    attached devices or network-attached devices sending that data
    onwards to MQTT brokers and other cloud services.
  </p>

  <p>Applications can be remotely deployed as OSGi bundles and their
    configuration imported (or exported) through a snapshot service. The
    same configuration service can be used to setup Kura’s other OSGi
    compatible services – DHCP, DNS, firewall, routing and wifi – which
    can be used to manage the networking setup of a gateway and
    provision private LANs and WLANs. Other bundled services available
    include position, a GPS location service so you can geolocate your
    gateways; click, a time service to ensure good time synchronisation;
    db, a database service for local storage using a embedded SQL
    database and process and watchdog services to keep things running
    smoothly.</p>

  <p>To talk to network attached devices, applications can use Java’s
    own networking capabilities (or optional support field protocols,
    such as ModBUS and CANBUS) to plug into existing device
    infrastructure. By abstracting the hardware using OSGi services for
    Serial, USB and Bluetooth communications, Kura gives application
    developers portable access to a wide range of common devices though
    they can still use Java’s own range of communications APIs when
    appropriate. An API for devices attached via GPIO, I2C, PWM or SPI
    will allow a system integrator to incorporate custom hardware as
    part of their gateway.</p>

  <p>The Kura package is completed with a web front end which allows the
    developer or administrator to remotely log in and configure all the
    OSGi-compliant bundles and which developers can utilise to provide a
    web facing aspect to their own application’s configuration needs.
    Developers should find that the Eclipse IDE’s OSGi tooling makes the
    route from code conception to installation on Kura an easily
    navigable path too.</p>

  <p>With a combination of an OSGi container, MQTT messaging, a rich
    range of network and local connectivity, remote web and command-line
    control and the familiar Eclipse tooling support, Kura is set to
    provide a compelling option for enterprise and IoT/M2M Java
    developers.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/february/images/djwalker.jpg"
        width="75" alt="DJ Walker-Morgan" />
      <p class="author-name">
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        D.J. Walker-Morgan<br />
        <a target="_blank" href="http://www.codepope.com/">Codepope</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://codescaling.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/Codepope">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/110944853355774677851/posts">Google
            +</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
