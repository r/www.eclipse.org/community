<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Introduction</h2>
    <p>
      This article demonstrates how to use <a
        href="https://wiki.eclipse.org/ECF#OSGi_Remote_Services">Eclipse
        Communication Framework (ECF)</a> to interact with networked
      devices based upon <a
        href="http://www.raspberrypi.org/documentation/usage/gpio/">Raspberry
        Pi GPIO</a>. The use of Remote Services provides clear
      advantages:
    </p>

    <ol>
      <li>The Service Interface provides a simple, clear, and flexible
        abstraction for accessing individual GPIO pins. This enables
        system-wide loose coupling and high cohesion, two very desirable
        design characteristics for networked services.</li>
      <li>There are multiple technical advantages from using Remote
        Services
      <ol>
        <li>Dynamics - Hardware-based services come and go (especially
          when made available via a network), and applications can be
          notified and adjust appropriately</li>
        <li>Service Versioning - Hardware capabilities and their
          associated services may evolve over time, and this may require
          versioning the Service Interface, and multiple versions to
          coexisting in a single runtime</li>
        <li>Support for Multiple Injection Frameworks - Declarative
          Services, Blueprint, Spring, and other standard (or
          proprietary) frameworks can be used to inject services and
          dependencies</li>
        <li>Support for Service-Level Security - Flexible policies to
          control, restrict, and/or manage services may be defined as
          appropriate for the application</li>
        <li>Transport-Independent Architecture - ECF Remote Services
          provides full interoperability through open standards, and
          open, extensible, community-developed-and-tested APIs and
          implementations</li>
      </ol></li>
    </ol>
    <p>In this article, we step through the declaration, implementation,
      and running of remote services for controlling individual GPIO
      Pins. After showing how these services can be used to control a
      simple light application for the Raspberry Pi, we then describe
      how to use ECF Remote Services to distribute these services across
      a network. We then demonstrate how these remote services can be
      easily used by an Eclipse-based user interface to remotely control
      the state of individual GPIO Pins.</p>

    <h2>Accessing GPIO Pin State</h2>
    <p>
      Each GPIO digital pins have two modes: output and input. For
      output, each pin can be in either the 'HIGH' state, or the 'LOW'
      state, and the control of the pin state is what we would like to
      manipulate to give our application desired behavior. After
      connecting a single LED to the Raspberry Pi GPIO, changing the
      output on a given pin to HIGH will turn the LED on, while setting
      to low will turn it off. See <a target="_blank"
        href="https://projects.drogon.net/raspberry-pi/gpio-examples/tux-crossing/gpio-examples-1-a-single-led/">these
        instructions</a> for how to physically connect a single LED to
      the Raspberry Pi GPIO.
    </p>

    <p>
      Below is a picture of my Raspberry Pi with an LED connected to
      GPIO digital output pin 0, as per the <a target="_blank"
        href="https://projects.drogon.net/raspberry-pi/gpio-examples/tux-crossing/gpio-examples-1-a-single-led/">these
        instructions</a>.
    </p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.1.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.1.png"
      width="400" alt="" /></a><br />

    <p>
      We start by declaring a GPIO pin output service interface. This <b>service
        interface</b> declares methods for getting and setting the state
      of a single GPIO pin. Here is the IGPIOPinOutput service
      interface:
    </p>


    <pre class="prettyprint lang-xtend">
public interface IGPIOPinOutput extends IGPIOPin {

  public boolean getState();

  public void setState(boolean value);

...

}
</pre>
    <br />

    <p>
      For this article, we will be interested only in the <b>getState</b>
      and <b>setState methods</b> from this service interface. Note that
      at runtime we may create as many instances of this service
      interface as desired. Modern Raspberry Pi's have two rows of 10
      pins for a total of 20 physical GPIO pins. To control our LED
      application, we only need to control a single pin, so will only
      need a single instance of the IGPIOPinOutput service that
      communicates with pin id=0.
    </p>

    <p>
      To find active instances of the IGPIOPinOutput service there are
      several mechanisms available (e.g. Declarative Services,
      Blueprint, Spring, ServiceTracker, ServiceReferences, etc.). For
      this tutorial, we will use a very simple ServiceTracker. The
      complete code for this example can be located in the start method
      of <a target="_blank"
        href="https://github.com/ECF/RaspberryPI/blob/master/test/bundles/org.eclipse.ecf.raspberrypi.test.gpio/src/org/eclipse/ecf/raspberrypi/test/gpio/Activator.java">this
        test code</a>. The important fragments is the implementation of
      the <b>addingService method</b>, which is called by the OSGi
      framework when instances of IGPIOPinOutput are registered with the
      OSGi service registry:
    </p>

    <pre class="prettyprint lang-xtend">
public IGPIOPinOutput addingService(ServiceReference&lt;IGPIOPinOutput&gt; reference) {
    ....
    // Get the service instance
    IGPIOPinOutput pin = context.getService(reference);
    // Print out info about it's current state
    System.out.println("  current pin state is "+(pin.getState()?"HIGH":"LOW"));
    System.out.println("  setting state to HIGH");
    // Set the pin's state to true/HIGH
    pin.setState(true);
    return pin;
}


    </pre>
    <br />

    <p>Note that when a IGPIOPinService is added, the
      service.setState(true) method is called, setting the state of the
      pin to HIGH, and turning on/lighting the LED connected to that
      pin. When this test code is run on a Raspberry Pi, this appears on
      the OSGi console:</p>

    <pre class="prettyprint lang-xtend">
osgi> start 3
Adding GPIO Pin Output service.   id=0
  current pin state is LOW
  setting state to HIGH
</pre>
    <br />
    <p>and as a result of the call to pin.setState(true) the LED
      connected to GPIO pin 0 lights up</p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.2.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.2.png"
      width="400" alt="" /></a><br />

    <p>Here is a short video showing the running of this test code.
      Thanks to ECF committer Wim Jongman for duplicating this example
      and making the video available.</p>
    <iframe width="560" height="315"
      src="//www.youtube.com/embed/BtS_JDyiKkU"
      allowfullscreen></iframe>
    <br>

    <h2>Exporting the GPIO Pin Output Service</h2>
    <p>
      To make the control of GPIO pin 0 available for remote access,
      with ECF Remote Services all we need to do is to provide values
      for some standardized service properties when registering the
      IGPIOPinOutput service instance. The names and appropriate
      types/values of these service properties are standardized by OSGi
      Remote Services, which is specified by <a target="_blank"
        href="http://www.osgi.org/Specifications/HomePage">OSGi
        Enterprise specification</a>.
    </p>

    <p>Here is the code to register and export the GPIOPinOutput service</p>

    <pre class="prettyprint lang-xtend">
Map&lt;String , Object&gt; pinProps = new HashMap&lt;String , Object&gt;();
pinProps.put("service.exported.interfaces", "*");
pinProps.put("service.exported.configs", "ecf.generic.server");
pinProps.put("ecf.generic.server.port", "3288");
pinProps.put("ecf.generic.server.hostname",InetAddress.getLocalHost().getHostAddress());
pinProps.put("ecf.exported.async.interfaces", "*");
...

// register and export GPIOPin 0
reg = Pi4jGPIOPinOutput.registerGPIOPinOutput(0, pinProps, context);


    </pre>
    <br />


    <p>
      See <a target="_blank"
        href="https://github.com/ECF/RaspberryPI/blob/master/test/bundles/org.eclipse.ecf.raspberrypi.test.gpio/src/org/eclipse/ecf/raspberrypi/test/gpio/Activator.java">here
        for the entire source for this example</a>.
    </p>

    <p>
      With ECF's Remote Services implementation present in the runtime,
      the properties given above will result in the IGPIOPinOutput
      service being exported as a remote service. The export happens
      automatically during the registration of the IGPIOPinOutput
      service (inside the registerGPIOPinOutput method in the above
      code). With the default discovery policy, exporting this remote
      service will also advertise/publish it for network-based
      discovery. For this <a target="_blank"
        href="https://github.com/ECF/RaspberryPI/tree/master/test/features/org.eclipse.ecf.raspberrypi.test.gpio.feature">example</a>,
      we use the Service Locator Protocol (<a target="_blank"
        href="https://tools.ietf.org/html/rfc2608">RFC 2608</a>) for
      LAN-based network discovery. ECF Remote Services provides <a
        target="_blank"
        href="https://wiki.eclipse.org/ECF#Customization_of_ECF_Remote_Services_.28new_Discovery_and.2For_Distribution_providers.29">multiple
        alternatives for discovery</a>, with support for both networked
      an non-network discovery alternatives.
    </p>

    <h2>Discovering Remote GPIOPinOutput Services</h2>
    <p>
      Once exported, remote consumers (clients) may discover and then
      call methods on the IGPIOPinOutput remote service. To demonstrate
      this, here is a simple Eclipse-based user interface to present the
      discovered IGPIOPinOutput remote service instances. The code for
      this Eclipse view is <a target="_blank"
        href="https://github.com/ECF/RaspberryPI/tree/master/test/bundles/org.eclipse.ecf.raspberrypi.test.gpio.ide">in
        the test/bundles/org.eclipse.ecf.raspberrypi.test.gpio.ide
        project</a>. When run in the Eclipse ide, the empty view (no
      discovered IGPIOPinOutput services) looks like this:
    </p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.3.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.3.png"
      width="600" alt="" /></a><br />

    <p>After discovering an instance of IGPIOPinOutput remote service
      via SLP LAN-based discovery the view is automatically updated with
      the IGPIOPinOutput proxy, which is automatically created by the
      OSGi Remote Services implementation.</p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.4.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.4.png"
      width="600" alt="" /></a><br />

    <p>With the discovery of this IGPIOPinOutput remote service, and
      it's appearance in this view, a simple click on the 'State (click
      to toggle)' cell results in the pin toggling it's state via a call
      to IGPIOPinUpdate.setState, resulting in both the Eclipse UI being
      updated with a different icon.</p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.5.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.5.png"
      width="600" alt="" /></a><br />

    <p>and the LED that's connected to the Raspberry Pi lights up.</p>

    <a
      href="/community/eclipse_newsletter/2014/october/images/article2.6.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article2.6.png"
      width="600" alt="" /></a><br />

    <p>The same as if we had accessed the IGPIOPinOutput service from
      the Raspberry Pi directly.</p>


    <h2>Summary</h2>

    <p>ECF Remote Services provides an easy way to declare arbitrary
      services, and expose/export them for remote access. Representing
      Raspberry Pi GPIO as remote services makes a great example of how
      such remote services can be a boon for the easy creation and
      distribution of services between devices that make up the Internet
      of Things.</p>



    <h2>Technical Notes</h2>

    <ol>
      <li>The full source for this example is available via the <a
        target="_blank" href="https://github.com/ECF/RaspberryPI">ECF
          Raspberry Pi Github Repository</a>. ECF also maintains a
        number of other examples and custom providers via it's <a
        target="_blank" href="https://github.com/ECF">Github
          repositories</a>.
      </li>
      <li>ECF provides asynchronous remote services for non-blocking
        remote procedure call. The example Eclipse code uses the
        IGPIOPinOutputAsync service interface, which is available <a
        target="_blank"
        href="https://github.com/ECF/RaspberryPI/blob/master/bundles/org.eclipse.ecf.raspberrypi.gpio/src/org/eclipse/ecf/raspberrypi/gpio/IGPIOPinOutputAsync.java">here</a>.
        The Eclipse UI code for this example uses this asynchronous
        remote service to control the GPIO Pin 0 without any methods
        that might block the Eclipse UI. The implementation of the
        IGPIOPinOutputAsync interface is automatically created by the <a
        href="https://wiki.eclipse.org/ECF#OSGi_Remote_Services">ECF
          Remote Services</a> proxy creation. Here is a snippet from the
        implementation of the GPIO Pin View class, showing the use of
        the asynchronous remote service, as well as the use of Java 8
        lambda expressions.
      </li>
    </ol>

    <pre class="prettyprint lang-xtend">
public void toggle() {
  // Set waiting to true
  waiting = true;
  boolean newState = !this.state;
  // If we have asynchronous access to service,
  // then use it
  if (pinOutputAsync != null) {
    // Set state asynchronously to newState,
    // and when complete change the UI state
    // and refresh the viewer
    pinOutputAsync.setStateAsync(newState).whenComplete(
        (result, exception) -> {
          this.waiting = false;
          if (viewer != null) {
            // No exception means success
            if (exception == null) {
              // Set UI state to newState
              state = newState;
              asyncRefresh();
            } else
              showCommErrorDialog(exception);
          }
        });
  } else {
    // If we do not have access to async service, then
    // call pinOutput synchronously
    pinOutput.setState(newState);
    state = newState;
  }
}
</pre>
    <br />

    <p>
      The entire source for this view class is available <a
        target="_blank"
        href="https://github.com/ECF/RaspberryPI/blob/master/test/bundles/org.eclipse.ecf.raspberrypi.test.gpio.ide/src/org/eclipse/ecf/raspberrypi/internal/test/gpio/ide/views/RpiGPIOPinView.java">here</a>.
    </p>

    <p>
      More about asynchronous remote services is described in <a
        href="https://wiki.eclipse.org/ECF/Asynchronous_Remote_Services">Asynchronous
        Remote Services</a>.
    </p>

    <h2>Related Articles and Documentation</h2>
    <ul>
      <li><a
        href="https://wiki.eclipse.org/Getting_Started_with_ECF%27s_OSGi_Remote_Services_Implementation">Getting
          Started with ECF's OSGi Remote Services Implementation</a></li>
      <li><a href="https://wiki.eclipse.org/EIG:Download">Download ECF
          Remote Services/RSA Implementation</a></li>
      <li><a href="https://wiki.eclipse.org/EIG:Add_to_Target_Platform">How
          to Add Remote Services/RSA to Your Target Platform</a></li>
    </ul>

    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
      type="text/javascript"></script>
    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
      type="text/javascript"></script>
    <script type="text/javascript">
        prettyPrint();
      </script>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="author-picture"
        src="/community/eclipse_newsletter/2014/october/images/scott.jpeg"
        width="90" alt="scott lewis" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Scott Lewis<br />
        <a target="_blank" href="http://composent.com/">Composent, Inc.</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Blog</a> </li>
          <li><a target="_blank" href="">Twitter</a></li> -->
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

