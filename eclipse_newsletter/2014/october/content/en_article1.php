<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>
      So you've been hearing about the Internet of Things (IoT) so much
      that you've now decided it's time to get a closer look at what it
      actually changes to the way applications are being built. But IoT
      is very vast, and you could quickly get lost in a jungle of
      protocols, frameworks, ... so here are <strong>five things</strong>
      that will get you up to speed with the exciting world of IoT!
    </p>
    <h2 id="open-hardware-platforms-will-help-you-build-your-thing">
      1. Open Hardware platforms will help you build <em>your</em> thing
    </h2>
    <p>
      It's amazing to see that only a few years back, it was virtually
      impossible for someone to have access to actual hardware to run
      IoT solutions. That is, cheap and reasonably-sized equipment that
      you could easily deploy in a house, a car, ... Nowadays, platforms
      like Arduino, mbed, BeagleBone and Raspberry Pi are providing IoT
      developers with a wide ecosystem of hardware that can be used to
      easily prototype and even go to production for small batches. The
      Eclipse IoT projects have always been very active in the Open
      Hardware communities and you should definitely check out some of
      the nice resources out there to e.g. <a
        href="http://developer.mbed.org/teams/mqtt/code/HelloMQTT">run
        MQTT on your mbed device</a>, or <a
        href="http://eclipse.github.io/kura/doc/beaglebone-quick-start.html">install
        the Kura framework on a BeagleBone Black</a>.
    </p>
    <h2 id="learn-about-iot-standards">2. Learn about IoT standards</h2>
    <a href="http://iot.eclipse.org"><img
      src="/community/eclipse_newsletter/2014/october/images/tag-cloud.png"
      style="float: right; margin-left: 2em;" alt="" /></a>
    <p>
      What is really interesting in the Internet of Things space is that
      we're talking about millions of devices that are pretty limited in
      terms of processing power and communication capabilities — think a
      tiny battery-powered microcontroller monitoring a solar panel in
      the middle of the Death Valley! — and yet, one needs reliable ways
      to access sensor data or manage the software running in the
      device, all of this usually happening over the air and with
      limited human intervention. <br />While the term IoT is new, the
      use cases we see today have been around for several years, and
      there are interesting open standards that help build
      interoperable, efficient IoT solutions.
    </p>
    <p>
      In order to allow IoT data to flow from producers (sensors on the
      field) to consumers (IT backends, web apps, etc.), <strong>MQTT</strong>
      is an OASIS standard that implements a publish-subscribe
      communication model. It has several QoS levels making it easy to
      find the perfect tradeoff between reliability and
      resources/bandwidth usage. <br />You should definitely read the <a
        href="http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/cos02/mqtt-v3.1.1-cos02.html">MQTT
        3.1.1 specification</a> document to get more familiar with the
      protocol, and see by yourself that it is indeed a simple and
      interesting protocol for IoT. As a reminder, <a
        href="http://eclipse.org/paho">Eclipse Paho</a> provides
      open-source implementations of MQTT clients in many programming
      languages (latest addition is <a
        href="https://www.eclipse.org/paho/clients/dotnet/">a client for
        .Net and WinRT platforms</a>), and Eclipse <a
        href="http://mosquitto.org">Mosquitto</a> Mosquitto and Moquette
      are broker implementations that can be deployed in IoT servers.
    </p>
    <p>
      <strong>OMA Lightweight M2M</strong> is another interesting <a
        href="http://technical.openmobilealliance.org/Technical/technical-information/release-program/current-releases/oma-lightweightm2m-v1-0">standard
        from the Open Mobile Alliance</a> that is getting lot of
      traction in the domain of Device Management. LwM2M is proposing a
      standard way to do things like: reboot a device, install a new
      software image (yes, similarly to what happens on your smartphone
      and that is based on an ancestor of LwM2M called OMA-DM), etc. <br />Eclipse
      <a href="https://eclipse.org/wakaama">Wakaama</a> and the proposed
      project <a href="https://projects.eclipse.org/proposals/leshan">Leshan</a>
      are implementing LwM2M stacks in C and Java that you can use to
      manage your IoT devices.
    </p>
    <p>
      At the transport level Lightweight M2M is using <strong>CoAP</strong>
      (Constrained Application Protocol), which is an <a
        href="https://tools.ietf.org/html/rfc7252">IETF standard</a>
      targetting very constrained environments in which it's still
      desirable to have the kind of features you would expect from HTTP,
      in particular the manipulation of resources that you can
      &quot;GET&quot;, &quot;PUT&quot;, store in a local cache, etc. <a
        href="https://eclipse.org/californium">Eclipse Californium</a>
      is an implementation of CoAP in Java that provides all the APIs
      you need to build your own CoAP resources.
    </p>
    <h2 id="get-out-of-your-comfort-zone">3. Get out of your comfort
      zone!</h2>
    <p>
      IoT is an incredible opportunity for all developers to learn about
      new technologies. If you are used to developing backend
      applications, you will learn a lot about embedded development if
      you start playing with embedded runtimes like <a
        href="http://www.contiki-os.org/">Contiki</a>. Being closer to
      the silicon means having to think about how to optimize radio
      communications, power consumption, etc. and it is truly
      fascinating. If you are more into embedded development, then start
      experimenting with the various cloud services (time series
      databases, <a href="http://iot.eclipse.org/sandbox.html">IoT
        brokers</a>, ...) you can connect your embedded system to, and
      learn how to turn your IoT data into something useful. You should
      also look at how <a href="https://eclipse.org/birt">Eclipse BIRT</a>
      can <a href="https://www.youtube.com/watch?v=KzMtlCmHtCU"> help
        you create nice visual dashboards</a>.
    </p>
    <h2 id="follow-our-end-to-end-tutorial">4. Follow our end-to-end
      tutorial</h2>
    <p>
      We were very excited to announce the <a
        href="http://iot.eclipse.org/java">Open IoT Stack for Java</a>
      at the end of September, and we've been working on making it super
      simple for any developer interested in IoT to essentially go from
      a bare Raspberry Pi to a complete end-to-end IoT solution in less
      than 30 minutes. <br />So whether you already have a Raspberry Pi
      and always wondered what to do with it, or you don't and just
      would like to get familiar with IoT at large, just head over to <a
        href="http://iot.eclipse.org/java/tutorial/">our tutorial</a>,
      buy the hardware, and start connecting all the things!
    </p>
    <h2 id="get-involved-with-the-eclipse-iot-community">5. Get involved
      with the Eclipse IoT community</h2>
    <a href="http://iot.eclipse.org"><img
      src="/community/eclipse_newsletter/2014/october/images/iot_logo_large_transparent.png"
      style="float: right; margin-left: 2em;" alt=""></a>
    <p>
      We just gave you a few ideas of what you should look at to get
      started with IoT, but there are certainly many other ways for you
      to do so. It is very likely that you will find under the <a
        href="https://iot.eclipse.org">Eclipse IoT</a> umbrella a
      project that will prick your curiosity, so go download the code,
      try it, and let us know what you're building!
    </p>
    <p>
      Also, please <a href="https://twitter.com/eclipseiot">get in touch
        with us on Twitter</a> and make sure to use the project's
      mailing lists to engage with the committers and developers making
      Eclipse IoT such an exciting community!
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/october/images/ben.jpg"
        alt="benjamin cabe" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Benjamin Cabé<br />
        <a target="_blank" href="http://www.eclipse.org">Eclipse
          Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://blog.benjamin-cabe.com">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/kartben">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+BenjaminCabé">Google +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

