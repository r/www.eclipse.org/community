<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Environment Monitoring is becoming an increasingly pressing need
      for public and private institutions that need to control multiple
      pollution indicators within their territory.</p>
    <img style="padding-left: 5px"
      src="/community/eclipse_newsletter/2014/october/images/equipement.jpg"
       width="120" alt="">
    <p>Traditionally, environmental monitoring has relied on only a few
      large footprint reference equipment, which meant high upfront
      costs and maintenance costs. While being able to provide highly
      precise readings, those stations are only capable of providing
      infrequent measurements resulting in very sparse data in space and
      time.</p>

    <img style="padding-right: 5px"
      src="/community/eclipse_newsletter/2014/october/images/newequipment.png"
      width="90" alt=""> <br />
    <p>Recently, a new class of environment monitoring stations has
      emerged to complement the reference systems. These stations, while
      more compact and affordable in price, offer a relatively high
      precision level at a high frequency data stream, down to
      minute-base flow rates. When massively deployed on the territory,
      these indicative stations enable a widespread monitoring. The
      large amount of data they collect feeds the mathematical models
      that track the dynamics of pollutants concentrations in urban
      areas.</p>

    <br />
    <p>Although these systems are great, building them presents great
      challenges to the embedded application developer:</p>

    <ol>
      <li>The huge amount of data collected from the sensor on the
        embedded board must often be pre-emptively processed and
        analyzed to unveil relevant events and correlation that are
        worth sending to the remote server. This means that
        communications has to be optimized or data streams rate have to
        be modified to save battery power based on the information on
        the consumption parameters provide.
        <a target="_blank" href="/community/eclipse_newsletter/2014/october/images/map.jpg"><img
        src="/community/eclipse_newsletter/2014/october/images/map.jpg"
        class="img-responsive" alt="" /></a>
        </li>

      <li>The possibility of an unstable cellular data connection
        requires careful management of local storage to preserve the
        data to be sent when communication is restored.</li>
      <li>These systems must be remotely managed and administered. It is
        essential to be able to access deployed systems remotely to
        perform diagnostics. As the application’s needs evolve, it is
        also critical to be able to upgrade the embedded application
        remotely.</li>
      <li>The response of air pollutant sensors to gas concentration may
        vary over time due to chemical modification, a phenomenon
        forcing periodic recalibration of the sensors. This is generally
        a serious maintenance issue that requires tearing off the
        environment monitoring systems and testing it in a laboratory.
        Since the system is placed in a similar artificial environment
        it undergoes a similar percentage changes in response to
        polluting agents. This can be exploited to solve the issue at
        the embedded software level: a unit accurately re-calibrated in
        a lab can be used to model the response change in all the
        homologous systems, thus defining the recalibration factors to
        be applied.</li>
      <li>Finally, given the unreliability and the costs of cellular
        data connection, special care needs to be devoted to the
        communication protocol of embedded applications, which rely on
        such links to connect the on board computer with the remote data
        center.</li>
    </ol>

    <p>
      These challenges can be effectively addressed by a solution which
      relies at its foundation on Java and OSGi, leveraging event-based
      technologies for Java Embedded can result into a much more
      flexible and manageable design with regards to the filtering of
      the sensors data streams or computing running aggregates. This
      kind of system can be remotely managed through the Eclipse Kura
      framework, a Java and OSGi framework for embedded Internet of
      Things applications. <a href="http://www.eclipse.org/kura/">Eclipse
        Kura</a>, an open source project within the <a
        href="http://iot.eclipse.org/">Eclipse IoT working group</a>,
      provides the ability of remotely configure OSGi services through
      the MQ Telemetry Transport (<a target="_blank"
        href="http://mqtt.org/">MQTT</a>) protocol, a publish-subscribe
      connectivity protocol optimized for the Internet of Things
      currently being standardized by OASIS.
    </p>

    <p>
      By using real world solutions for environmental monitoring, the
      above technologies come together to offer the state of the art
      development environment for edge computing nodes in the IoT era. <a
        target="_blank"
        href="http://www.eurotech.com/en/products/ReliaSENS%2018-12">ReliaSens
        smart environment sensor</a>, by Eurotech, which runs <a
        target="_blank"
        href="http://www.eurotech.com/en/products/software+services/everyware+software+framework">ESF
        Everyware Software Framework</a>, Eurotech’s commercial
      distribution of Kura, is an example of real world solutions for
      environmental monitoring.

 <div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="author-picture"
        src="/community/eclipse_newsletter/2014/october/images/aceiner.jpg"
        width="90" alt="Andrea Ceiner" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Andrea Ceiner<br />
        <a target="_blank" href="http://www.eurotech.com/en/">Eurotech</a>
      </p>
      <ul class="author-link">
        <!-- <li><a target="_blank" href="http://koehnlein.blogspot.de/">Blog</a> </li> -->
        <li><a target="_blank" href="https://twitter.com/andreaceiner">Twitter</a></li>
        <!-- <li><a target="_blank" href="https://plus.google.com/116635061773096754601/posts">Google +</a></li>
          <?php echo $og; ?>  -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
