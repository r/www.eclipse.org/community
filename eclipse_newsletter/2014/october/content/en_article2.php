<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Introduction</h2>
    <p>
      Everybody talks about the Internet of Things nowadays.
      Increasingly affordable micro controllers like Arduino and
      Raspberry Pi are enabling cheap devices that measure sensor data
      and send it over the internet. <strong>The goal of this post is to
        introduce the lightweight protocol MQTT and its capabilities to
        send data between devices and other systems and to demonstrate
        them by implementing two clients with Eclipse Paho.</strong>
    </p>
    <p>
      The term Internet of Things was first used by <a
        href="http://www.rfidjournal.com/article/view/4986"
        target="_blank">Kevin Ashton</a> in 2009 for interconnecting
      physical devices over the internet. The basic idea is very simple:
      <strong>Physical devices can exchange data between each other or
        being controlled by others.</strong> Examples of such devices
      would be a refrigerator, a car, a building or basically any other
      electronic device. One of the most common use cases is the
      collection, transmission, consolidation and displaying of sensor
      data. The results could be a web dashboard with the aggregated
      values or an alarm, when a threshold is exceeded.<br /> The
      application scenarios are almost unlimited. <em>Imagine your alarm
        clock would know that your train to work is 15 minutes late and
        adjust itself accordingly. Also your coffee maker is switched on
        automatically 15 minutes later to make you a hot cup of coffee
        before you leave for work.</em> Sounds like the future ? All
      that is already possible today. <a
        href="http://www.akos-rs.si/files/Telekomunikacije/Digitalna_agenda/Internetni_protokol_Ipv6/More-than-50-billion-connected-devices.pdf"
        target="_blank">Ericsson</a> predicts that in 2020 50 billion
      devices are connected over the internet. The communication between
      the huge amount of devices is enabled by IPv6 and lightweight
      communication protocols like MQTT.
    </p>

    <h2>MQTT</h2>
    <p>
      MQTT was developed by Andy Stanford-Clark (IBM) and Arlen Nipper
      (Eurotech; now Cirrus Link) in 1999 for the monitoring of an oil
      pipeline through the desert. The goals were to have a protocol,
      which is <strong>bandwidth-efficient and uses little battery power</strong>,
      because the devices were connected via satellite link and this was
      extremely expensive at that time.<br /> <strong>The protocol uses
        a publish/subscribe architecture in contrast to HTTP with its
        request/response paradigm.</strong> Publish/Subscribe is
      event-driven and enables messages to be pushed to clients. The
      central communication point is the MQTT broker, it is in charge of
      dispatching all messages between the senders and the rightful
      receivers. Each client that publishes a message to the broker,
      includes a topic into the message. <strong>The topic is the
        routing information for the broker.</strong> Each client that
      wants to receive messages subscribes to a certain topic and the
      broker delivers all messages with the matching topic to the
      client. Therefore the clients don’t have to know each other, they
      only communicate over the topic. This architecture enables highly
      scalable solutions without dependencies between the data producers
      and the data consumers.
    </p>
    <a
      href="/community/eclipse_newsletter/2014/october/images/article1.1.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article1.1.png"
      alt="MQTT Publish/Subscribe" width="600" /></a>
    <p class="wp-caption-text">MQTT Publish/Subscribe Architecture</p>
    <br />
    <p>
      The difference to HTTP is that a client doesn’t have to pull the
      information it needs, but the broker pushes the information to the
      client, in the case there is something new. Therefore each MQTT
      client has a permanently open TCP connection to the broker. If
      this connection is interrupted by any circumstances, the MQTT
      broker can buffer all messages and send them to the client when it
      is back online.<br /> As mentioned before the central concept in
      MQTT to dispatch messages are topics. <strong>A topic is a simple
        string that can have more hierarchy levels, which are separated
        by a slash.</strong> A sample topic for sending temperature data
      of the living room could be <em>house/living-room/temperature</em>.
      On one hand the client can subscribe to the exact topic or on the
      other hand use a wildcard. The subscription to <em>house/+/temperature</em>
      would result in all message send to the previously mention topic <em>house/living-room/temperature</em>
      as well as any topic with an arbitrary value in the place of
      living room, for example <em>house/kitchen/temperature</em>. The
      plus sign is a <strong>single level wild card</strong> and only
      allows arbitrary values for one hierarchy. If you need to
      subscribe to more than one level, for example to the entire
      subtree, there is also a <strong>multilevel wildcard</strong> (<em>#</em>).
      It allows to subscribe to all underlying hierarchy levels. For
      example <em>house/#</em> is subscribing to all topics beginning
      with <em>house</em>.
    </p>

    <h2>Eclipse Paho</h2>
    <p>
      Now with the concept of topics explained, it is time to jump right
      into the first implementation and to show how the publishing and
      subscribing can be done using source code. But first a quick
      introduction of <a href="http://www.eclipse.org/paho"
        target="_blank">Eclipse Paho</a>, the MQTT implementation used
      in this example. It has been founded under the umbrella of the
      Eclipse Foundation at the beginning of 2012 with the goal to
      provide open IoT protocol implementations. <strong>The
        implementation of MQTT is the de-facto reference implementation
        and available in Java, C, C++, JavaScript, Lua, Python and soon
        also C#.</strong> The origin of most of the implementations is
      the codebase of IBM and Eurotech, who have used them in many
      internal projects in production.
    </p>

    <h2>Use Case</h2>
    <p>In order to make the subsequent code more understandable, we will
      use the transferring of sensor data from a temperature and
      brightness sensor to a control center over the internet as an
      example. The sensors will be connected to a Raspberry Pi, which
      acts as gateway to the MQTT broker, which resides in the cloud. On
      the other side is a second device, the control center, that also
      has an MQTT client and receives the data. Additionally we will
      implement a notification, which alerts the control center if the
      sensor is disconnected.</p>
    <a
      href="/community/eclipse_newsletter/2014/october/images/article1.2.png"><img
      src="/community/eclipse_newsletter/2014/october/images/article1.2.png"
      alt="Use Case Communication Flow" width="600" /></a>
    <p class="wp-caption-text">Communication between the sensor client
      and the control center over MQTT</p>
    <br />

    <h2>Implementation</h2>
    <p>
      First we will implement the sensor client, which is simulating a
      thermometer and a brightness sensor. It should send a current
      value every second to the MQTT broker. In this case we are using
      the <a href="http://www.hivemq.com" target="_blank">HiveMQ</a>
      public broker on the <a href="http://www.mqttdashboard.com"
        target="_blank">MQTT Dashboard</a>. The first step is to create
      an instance of the <em>MqttClient</em> class.
    </p>
    <p></p>


    <pre class="prettyprint lang-xtend">
public class Publisher
{
     public static final String BROKER_URL = "tcp://broker.mqttdashboard.com:1883";
     private MqttClient client;

     public Publisher()
     {

          String clientId = Utils.getMacAddress() + "-pub";
          try
          {
               client = new MqttClient(BROKER_URL, clientId);
          }
          catch (MqttException e)
         {
              e.printStackTrace();
              System.exit(1);
          }
     }
}
</pre>

    <p></p>
    <p>
      As parameters for the constructor it is necessary to specify the
      URL of the broker (tcp://broker.mqttdashboard.com:1883) and also
      the client id. The latter is an unique identifier overall the
      broker. A good choice for the client id is the MAC address of the
      computer, because that is automatically unique. In the example <em>-pub</em>
      is added to the mac address, because otherwise it wouldn’t work
      starting both clients on the same machine for testing. After
      creating the instance, it is possible to try to connect to the
      broker with calling <em>client.connect()</em>. Apart from a simple
      connect, it is also possible to hand over more parameters. One
      example is the clean session flag. When it is set to true, the
      broker will wipe the session every time the client disconnects,
      otherwise it will keep the subscription and buffer the messages
      sent with Quality of Service 1 and 2 (more on this later). The
      session is assigned to the client id, therefore it is truly
      important to have it unique. Another option is <strong>Last Will
        and Testament (LWT), which helps detecting failures of other
        clients</strong>. As stated above every client has an open
      connection, so when the client disconnects ungracefully the broker
      can detect that. If the client has set a LWT topic and message on
      connect, the broker will send that to the specified topic, which
      allows the client to notify others about its failure.
    </p>

    <p></p>

    <pre class="prettyprint lang-xtend">
//...

MqttConnectOptions options = new MqttConnectOptions();
options.setCleanSession(false);
options.setWill(client.getTopic("home/LWT"),
"I'm gone".getBytes(), 2, true);

client.connect(options);

//...
</pre>
    <p></p>
    <p>
      Now we have to implement the business logic to retrieve the values
      and send them every second, therefore we use an infinite loop and
      the methods <em>publishTemperature</em> and <em>publishBrightness</em>.
      Each method creates a <em>MqttTopic</em> object and a random
      value, which will then be published. The complete examples of both
      clients can be found on <a
        href="https://github.com/goetzchr/paho-publish-subscribe"
        target="_blank">GitHub</a>.
    </p>
    <p></p>

    <pre class="prettyprint lang-xtend">
     public static final String TOPIC_TEMPERATURE = "home/temperature";

     //...
     while (true)
     {
          publishBrightness();
          Thread.sleep(500);
          publishTemperature();
          Thread.sleep(500);
     }
     //...

     private void publishTemperature() throws MqttException {
         final MqttTopic temperatureTopic = client.getTopic(TOPIC_TEMPERATURE);

         final int temperatureNumber = Utils.createRandomNumberBetween(20, 30);
         final String temperature = temperatureNumber + "°C";

         temperatureTopic.publish(new MqttMessage(temperature.getBytes()));
     }

     //publishBrightness() wird analog zu publishTemperature() implementiert
</pre>
    <p></p>
    <p>Each message can be published with one of three quality of
      service levels (QoS). These levels are associated with different
      guarantees. A message send with level 0 doesn’t have a guarantee
      at all, it implies fire and forget. Level 1 guarantees that the
      message will at least arrive once, but can arrive more than once.
      Level 2 is the most sophisticated choice, which guarantees that
      the message arrives at the destination exactly once. The choice of
      QoS is a trade-off, between protocol overhead and the guarantee
      that the message arrives, because ensuring QoS 2 is using more
      bandwidth than QoS 0.</p>
    <p>
      The next step is implementing the subscribing client, which is
      reading the values on the topics <em>home/temperature</em> and
      “home/brightness”, plus observes the <em>home/LWT</em> topic for
      the last will message to detect a failure of the sensor client.
      The initialization of the <em>MqttClient</em> instance is almost
      the same, except we use <em>-sub</em> as a suffix for the client
      id. For receiving the messages sent by the sensor simulator, it is
      necessary to implement the <em>MqttCallback</em> interface. The <em>MqttCallback</em>
      interface defines three methods that need to implemented:
      connectionLost, messageArrived and deliveryComplete.
      connectionLost is called when the connection is unexpectedly
      closed form the MQTT broker. This method is the best place for a
      reconnect logic. The method messageArrived is called when the
      broker sends a new message to this particular client. Finally,
      there is the deliveryComplete method that is called after a
      message with QoS 1 or 2 reaches the broker. For our use case
      implementing the messageArrived method is enough. All arriving
      messages should be print out with topic and payload and if it is
      sent on the <em>home/LWT</em> topic, we additionally print out <em>Sensor
        gone!</em>.
    </p>
    <p></p>


    <pre class="prettyprint lang-xtend">
public class SubscribeCallback implements MqttCallback
{

     @Override
     public void connectionLost(Throwable cause) {}

     @Override
     public void messageArrived(MqttTopic topic, MqttMessage message)
     {
          System.out.println("Message arrived. Topic: " + topic.getName() + " Message: " + message.toString());

          if ("home/LWT".equals(topic.getName()))
          {
               System.err.println("Sensor gone!");
          }
     }

     @Override
     public void deliveryComplete(MqttDeliveryToken token) {}

}
</pre>
    <p></p>
    <p>
      After implementing the callback, we have to make it known to the <em>MqttClient</em>
      before connecting. Also after the successful connection is
      established, it is necessary to subscribe to all topics, which
      should be sent to the client. In this case it should be everything
      that starts with <em>home</em>, so the usage of <strong>a
        multi-level wildcard saves us from subscribing to 3 different
        topics</strong>: <em>home/#</em>.
    </p>

    <pre class="prettyprint lang-xtend">
mqttClient.setCallback(new SubscribeCallback());
mqttClient.connect();
mqttClient.subscribe("home/#");
</pre>

    <h2>Run both clients</h2>
    <p>
      Now that the sensor client and the Control Center are implemented,
      it is time to run both applications. We start the Control Center
      first and then the sensor client. Once the sensor client starts,
      messages arrive at the Control Center. If you will now exit the
      sensor client ungracefully, this is recognized by the Control
      Center immediately through the Last Will and Testament message.<br />
      <strong>Congratulations, you have now build your first Internet of
        Things application with MQTT! </strong>
    </p>
    <h2>Summary</h2>
    <p>
      <strong>This simple application is just an example and can be
        easily adapted and reused for more advanced purposes.</strong>
      You could simply extend the existing code so that multiple clients
      in different rooms measure the temperature and brightness, and the
      Control Center also indicates that. Another possibility would be
      to replace the Control Center through a web dashboard where the
      values are displayed in several charts using the Paho JavaScript
      library.
    </p>
    <p>
      <strong>As we have seen, it is possible with very little effort to
        create application that communicates over MQTT.</strong> The
      Eclipse Paho library implements all the functionality that is
      described in the MQTT specification and MQTT should be a tool in
      the repertoire of every developer, who deals with the Internet of
      Things or the requirement of connecting mobile devices.
    </p>

    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
      type="text/javascript"></script>
    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
      type="text/javascript"></script>
    <script type="text/javascript">
  prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="author-picture"
        src="/community/eclipse_newsletter/2014/october/images/christiangotz.jpg"
        alt="christian gotz" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Christian Götz<br />
        <a target="_blank" href="http://www.dc-square.de/">dc-square
          GmbH</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.hivemq.com/blog">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/goetzchr">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/107228567531222602469">Google +</a></li>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

