<?php 
include_once('settings.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/community/eclipse_newsletter/assets/common.php');
//print_r($settings);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="<?php print $settings['title'];?> - <?php print $settings['slogan'];?>" />
        
        <title><?php print $settings['title'];?> - <?php print $settings['slogan'];?></title>
		<style type="text/css">
			/* Client-specific Styles */
			#outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
			body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
			body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */
			
			/* Reset Styles */
			body{margin:0; padding:0;}
			img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
			table td{border-collapse:collapse;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}
			
			/* Template Styles */

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: COMMON PAGE ELEMENTS /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Page
			* @section background color
			* @tip Set the background color for your email. You may want to choose one that matches your company's branding.
			* @theme page
			*/
			body, #backgroundTable{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Page
			* @section email border
			* @tip Set the border for your email.
			*/
			#templateContainer{
				/*@editable border: 1px solid #DDDDDD;*/
			}

			/**
			* @tab Page
			* @section heading 1
			* @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
			* @style heading 1
			*/
			h1, .h1{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 2
			* @tip Set the styling for all second-level headings in your emails.
			* @style heading 2
			*/
			h2, .h2{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:30px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 3
			* @tip Set the styling for all third-level headings in your emails.
			* @style heading 3
			*/
			h3, .h3{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:26px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Page
			* @section heading 4
			* @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
			* @style heading 4
			*/
			h4, .h4{
				/*@editable*/ color:#202020;
				display:block;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:22px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				margin-top:0;
				margin-right:0;
				margin-bottom:10px;
				margin-left:0;
				/*@editable*/ text-align:left;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: PREHEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section preheader style
			* @tip Set the background color for your email's preheader area.
			* @theme page
			*/
			#templatePreheader{
				/*@editable*/ background-color:#FAFAFA;
			}

			/**
			* @tab Header
			* @section preheader text
			* @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
			*/
			.preheaderContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:10px;
				/*@editable*/ line-height:100%;
				/*@editable*/ text-align:left;
			}

			/**
			* @tab Header
			* @section preheader link
			* @tip Set the styling for your email's preheader links. Choose a color that helps them stand out from your text.
			*/
			.preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#303355;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: HEADER /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Header
			* @section header style
			* @tip Set the background color and border for your email's header area.
			* @theme header
			*/
			#templateHeader{
				/*@editable*/ background-color:#FFFFFF;
				/*@editable*/ border-bottom:0;
			}

			/**
			* @tab Header
			* @section header text
			* @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
			*/
			.headerContent{
				/*@editable*/ color:#202020;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:34px;
				/*@editable*/ font-weight:bold;
				/*@editable*/ line-height:100%;
				/*@editable*/ padding:0;
				/*@editable*/ text-align:left;
				/*@editable*/ vertical-align:middle;
			}
			/**
			* @tab Header
			* @section header link
			* @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
			*/
			.headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#336699;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:underline;
			}

			#headerImage{
				height:auto;
				max-width:602px;
			}

			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: MAIN BODY /\/\/\/\/\/\/\/\/\/\ */

			/**
			* @tab Body
			* @section body style
			* @tip Set the background color for your email's body area.
			*/
			#templateContainer, .bodyContent{
				/*@editable background-color:#FFFFFF;*/
			}
			
			/**
			* @tab Body
			* @section body text
			* @tip Set the styling for your email's main content text. Choose a size and color that is easy to read.
			* @theme main
			*/
			.bodyContent div{
				/*@editable*/ color:#333333;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:14px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Body
			* @section body link
			* @tip Set the styling for your email's main content links. Choose a color that helps them stand out from your text.
			*/
			.bodyContent div a
			.bodyContent div a:active, 
			.bodyContent div a:link, 
			.bodyContent div a:visited, 
			/* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#303355;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:none;
			}
			
			.bodyContent img{
				display:inline;
				height:auto;
			}
			
			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: SIDEBAR /\/\/\/\/\/\/\/\/\/\ */
			
			/**
			* @tab Sidebar
			* @section sidebar style
			* @tip Set the background color and border for your email's sidebar area.
			*/
			#templateSidebar{
				/*@editable background-color:#FFFFFF;*/
				/*@editable*/ border-left:0;
			}
			
			/**
			* @tab Sidebar
			* @section sidebar text
			* @tip Set the styling for your email's sidebar text. Choose a size and color that is easy to read.
			*/
			.sidebarContent div{
				/*@editable*/ color:#505050;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:150%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Sidebar
			* @section sidebar link
			* @tip Set the styling for your email's sidebar links. Choose a color that helps them stand out from your text.
			*/
			.sidebarContent div a:link, .sidebarContent div a:visited, /* Yahoo! Mail Override */ .sidebarContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#303355;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:none;
			}
			
			.sidebarContent img{
				display:inline;
				height:auto;
			}
			
			/* /\/\/\/\/\/\/\/\/\/\ STANDARD STYLING: FOOTER /\/\/\/\/\/\/\/\/\/\ */
			
			/**
			* @tab Footer
			* @section footer style
			* @tip Set the background color and top border for your email's footer area.
			* @theme footer
			*/
			#templateFooter{
				padding-top:10px;
				/*@editable background-color:#FFFFFF;*/
				/*@editable*/ border-top:0;
			}
			
			/**
			* @tab Footer
			* @section footer text
			* @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
			* @theme footer
			*/
			.footerContent div{
				/*@editable*/ color:#707070;
				/*@editable*/ font-family:Arial;
				/*@editable*/ font-size:12px;
				/*@editable*/ line-height:125%;
				/*@editable*/ text-align:left;
			}
			
			/**
			* @tab Footer
			* @section footer link
			* @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
			*/
			.footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
				/*@editable*/ color:#303355;
				/*@editable*/ font-weight:normal;
				/*@editable*/ text-decoration:none;
			}
			
			.footerContent img{
				display:inline;
			}
			
			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			* @theme footer
			*/
			#social{
				/*@editable*/ background-color:#FAFAFA;
				/*@editable*/ border:0;
			}
			
			/**
			* @tab Footer
			* @section social bar style
			* @tip Set the background color and border for your email's footer social bar.
			*/
			#social div{
				/*@editable*/ text-align:center;
			}
			
			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			* @theme footer
			*/
			#utility{
				/*@editable background-color:#FFFFFF;*/
				/*@editable*/ border:0;
			}

			/**
			* @tab Footer
			* @section utility bar style
			* @tip Set the background color and border for your email's footer utility bar.
			*/
			#utility div{
				/*@editable*/ text-align:center;
			}
			
			#monkeyRewards img{
				max-width:190px;
			}
			
			/**
			* @custom styles
			*/
						
			.bodyContent div h3 a:link,
			.sidebarContent div a:link{
			
			}
		
			.bodyContent div.future-spotlight a,
			.bodyContent div.future-spotlight a:link,
			.bodyContent div.future-spotlight a:visited,
			.bodyContent div.future-spotlight a:active{
				padding:6px 8px 6px 16px;
				display: block;
				font-size: 12px;
				text-decoration: underline;
			}
			.bodyContent div.future-spotlight a:hover{
				text-decoration: none;
			}
			.bodyContent div a.read_more,
			.bodyContent div a.read_more:link,
			.bodyContent div a.read_more:active,
			.bodyContent div a.read_more:visited,
			.sidebarContent div a.read_more,
			.sidebarContent div a.read_more:link,
			.sidebarContent div a.read_more:visited,
			.sidebarContent div a.read_more:active
			{
				clear:both;
				display: block;
				text-decoration: none;
				margin-top:4px;
				color:#c6333a;
				font-weight: normal;
			
			}
			
			.bodyContent div a:hover,
			.sidebarContent div a:hover,
			.bodyContent div a.read_more:hover,
			.sidebarContent div a.read_more:hover,
			.footerContent div a:hover{
				text-decoration: underline;
			}
								
		</style>
	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
            	<tr>
                	<td align="center" valign="top">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="0" cellspacing="0" width="602" id="templatePreheader">
                            <tr>
                                <td valign="top" class="preheaderContent">
                                
                                	<!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    	<tr>
                                        	<td valign="top" id="preheader-padding-1" style="padding:15px 20px 10px 0;">
                                            	<div>
                                                	 <?php print $settings['teaser'];?>
                                                </div>
                                            </td>
                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->
											<td valign="top" width="190" id="preheader-padding-2" style="padding:15px 0px 10px 20px;">
                                            	<div>
                                                	Is this email not displaying correctly?<br /><a href="http://eclipse.org/community/eclipse_newsletter/<?php print $settings['url'];?>/index.php" target="_blank">View it in your browser</a>.
                                                </div>
                                            </td>
											<!-- *|END:IF|* -->
                                        </tr>
                                    </table>
                                	<!-- // End Module: Standard Preheader \ -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="602" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Header \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="602" id="templateHeader" style="background-image: url('http://eclipse.org/community/eclipse_newsletter/template/images/bg-content.gif');background-color:#fff; background-repeat:repeat-y;">
                                        <tr>
                                            <td class="headerContent"><a href="http://eclipse.org/community/eclipse_newsletter/"><img src="http://www.eclipse.org/community/eclipse_newsletter/template/images/header.jpg"  id="headerImage campaign-icon" border="0" /></a></td>
											<td class="headerContent"><a href="https://www.facebook.com/eclipse.org" target="_blank"><img src="http://www.eclipse.org/community/eclipse_newsletter/template/images/header-facebook.jpg" border="0"/></a></td>
												
											<td class="headerContent"><a href="https://twitter.com/eclipsefdn" target="_blank"><img src="http://www.eclipse.org/community/eclipse_newsletter/template/images/header-twitter.jpg" border="0"/></a></td>
											<td class="headerContent"><a href="http://eclipse.org/community/eclipse_newsletter/"><img src="http://www.eclipse.org/community/eclipse_newsletter/template/images/header-end.jpg" border="0"/></a></td>                               
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Body \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="602" id="templateBody" style="background-image: url('http://eclipse.org/community/eclipse_newsletter/template/images/bg-content.gif');background-color:#fff; background-repeat:repeat-y;">
                                    	<tr>
                                        	<td valign="top">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td valign="top" class="bodyContent">
                                            
                                                            <!-- // Begin Articles \\ -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
			                                                            <div id="std_content">
			                                                            <h2 class="title" style="<?php print $common['style_blue'];?>font-size:20px;font-weight: bold;color:#303355;padding:20px 21px !important;margin:0 !important;"><?php print $settings['slogan'];?></h2>
			                                                            <?php if(!empty($settings['articles'])):?>
			                                                            	<h4 class="h4" id="h4_title_articles" style="<?php print $common['style_white'];?>background:#303355!important;padding:0px 5px 9px 0px;width: 120px;font-size: 12px;color:#efedf9;">
			                                                            		<img src="http://eclipse.org/community/eclipse_newsletter/template/images/icon-articles.gif" align="left" style="padding:0 16px 1px 21px;"/><span style="display:block;padding-top:9px;">Articles</span>
			                                                            	</h4>
			                                                            	<!-- // Begin Module: Standard Content \\ -->                                                        				
                                                           					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="table_article" style="padding-left:21px !important;">
                                                           						<?php foreach($settings['articles'] as $a): ?>
                                                           						<tr>
                                                                    				<td valign="top" class="article-icon" style="width:48px;padding-top:8px !important;"><img src="http://eclipse.org/community/eclipse_newsletter/template/images/icon-news-item.gif" style="padding:0 5px;"/></td>
                                                                    				<td valign="top" class="article-content" style="padding:8px 42px 16px 0px !important;">
                                                                    					<h3 style="<?php print $common['style_blue'];?>margin:0px !important;line-height: 20px;font-size:15px;"><a href="<?php print $a['url'];?>" style="<?php print $common['style_blue'];?>font-weight: bold;"><?php print $a['title'];?></a></h3>
                                                                    					<span class="author" style="font-style: italic;font-size: 11px;"><?php print $a['author'];?></span>                     	
																						<p style="font-size: 11px;margin-top:4px;line-height: 15px;"><?php print $a['teaser'];?>
																						<a href="<?php print $a['url'];?>" class="read_more">Read more →</a></p> 
                                                                    				</td>                                                                  
			                                                            		</tr>                                                       					
                                                           						<?php endforeach; ?>   					         
			                                                            	</table>
			                                                            <?php endif;?>
			                                                            <?php if(!empty($settings['links'])):?>
			                                                            	<div id="link-container" style="padding:0px 21px 21px 21px !important;">
			                                                            		<h3 style="<?php print $common['style_black'];?>font-size:13px;padding-bottom:0;">Recommended Reading</h3>
			                                                            		<div style="margin-top:1px;">
			                                                            		<?php foreach($settings['links'] as $l): ?>
			                                                            			<div style="font-size: 11px;padding-left:15px !important;">• <a href="<?php print $l['url'];?>" target="_blank"><?php print $l['title'];?></a></div>
			                                                            		<?php endforeach; ?>  	
			                                                            		</div>
			                                                            	</div>	
			                                                            <?php endif;?>	
			                                                            <?php if(!empty($settings['spotlight'])):?>
			                                                            	<?php foreach($settings['spotlight'] as $s): ?>
			                                                            		<div class="user-spotlight" style="background: #edede3;	margin:0 30px 16px 21px !important;">
			                                                            			<h4 class="h4" style="<?php print $common['style_white'];?>width: auto;background:#303355;font-size: 12px;color:#efedf9;"><img src="http://eclipse.org/community/eclipse_newsletter/template/images/icon-user.gif" style="padding:8px 6px" align="left"/><span style="display:block;padding:11px 5px 10px 33px;">Eclipse User Spotlight</span></h4>
			                                                            			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="table-user-spotlight">
			                                                            				<tr>
			                                                            					<td valign="top" class="user-spotlight-picture" style="width: 66px;padding-left:20px;padding-right:20px;"><img src="<?php print $s['image'];?>" alt="Eclipse User Spotlight - <?php print $s['name'];?>"/></td>
			                                                            					<td valign="top">
			                                                            						<h2 class="h4" style="<?php print $common['style_blue'];?>margin:8px 16px 0 0;font-size: 16px;line-height: 17px;color:#303355;"><?php print $s['name'];?><br/><span style="font-size:12px;padding-bottom:1px;padding-top:1px;"><?php print $s['position'];?></span></h2>                   				
			                                                            					</td>
			                                                            				</tr>
			                                                            				<?php if(!empty($s['content'])):?>		                                                           		
			                                                            					<tr>
			                                                            						<td colspan="2">
			                                                            							<?php foreach($s['content'] as $sc): ?>
			                                                            								<h4 class="questions" style="<?php print $common['style_black'];?>font-size: 12px;margin-top:16px;line-height:14px;margin-bottom:0;padding-bottom:0;clear:both;	padding-left:20px;padding-right:20px;"><?php print($sc['question']);?></h4>
			                                                            								<p class="answer" style="font-size: 11px;line-height: 16px;margin-top:3px;padding-left:20px;padding-right:20px;"><?php print($sc['answer']);?></p>
			                                                            							<?php endforeach; ?>
			                                                            						</td>
			                                                            					</tr>
			                                                            				<?php endif;?>	
			                                                            			</table>
			                                                            			<div class="future-spotlight" style="margin-top:6px;border-top:5px dotted #fff; "><?php print $s['footer'];?></div>                          	
			                                                            		</div>
			                                                            	<?php endforeach; ?>
			                                                            <?php endif;?>			                                                            
			                                                            <?php if(!empty($settings['news'])):?>
			                                                            	<h4 class="h4" style="<?php print $common['style_white'];?>background: url('http://eclipse.org/community/eclipse_newsletter/template/images/icon-news.gif') 21px 5px #303355;width: 55px;padding:11px 5px 9px 55px;;font-size: 12px;background-repeat: no-repeat;color:#efedf9;">News</h4>
			                                                            	<table border="0" cellpadding="0" cellspacing="0" width="100%"  class="table_article" style="padding-left:21px !important;">
                                                                				<?php foreach($settings['news'] as $n): ?>
                                                                					<tr>
                                                                    					<td valign="top" class="article-content" style="padding-right:42px;padding-bottom:16px;">
                                                                    						<h3 style="<?php print $common['style_blue'];?>margin:0;line-height:18px;font-size:15px;"><a href="<?php print $n['url'];?>" style="<?php print $common['style_blue'];?>font-weight:bold;"><?php print $n['title'];?></a></h3>
																							<span class="post-date" style="font-style: italic;font-size: 11px;"><?php print $n['date'];?></span>
																							<p style="font-size: 11px;margin-top:4px;line-height: 15px;"><?php print $n['content'];?>
																							<a href="<?php print $n['url'];?>" class="read_more">Read more →</a></p>
                                                                    					</td>
			                                                            			</tr>
			                                                            		<?php endforeach; ?>			                                                            	
			                                                            	</table>
			                                                            <?php endif;?>			                                                           
			                                                            </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Articles \\ -->
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        	<!-- // Begin Sidebar \\  -->
                                        	<td valign="top" width="200" id="templateSidebar">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="200">
                                                	<tr>
                                                    	<td valign="top" class="sidebarContent">
                                                            <!-- // Begin Module: Top Image with Content \\ -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding:6px 20px;">
                                                                <tr>
                                                                    <td valign="top">
                                                                    <h2 style="<?php print $common['style_grey'];?>display:block;clear:both;color:#666666;font-size:14px;margin-top:0;text-align: center;margin-top:6px;margin-bottom:20px;"><span><?php print $settings['title'];?></span></h2>
                                                                    	<?php if(!empty($settings['ads'])):?>
                                                                    		<div id="ad_area">
                                                                    			<?php foreach($settings['ads'] as $ad):?>
                                                                        			<a href="<?php print $ad['url'];?>"><img src="<?php print $ad['image'];?>" style="max-width: 160px;padding-bottom:14px;"/></a>
                                                                        		<?php endforeach;?>
                                                                        	</div>
                                                                        <?php endif;?>
                                                                        <?php if(!empty($settings['release']['content'])):?>
                                                                       		<div id="sidebar_projects" class="block" style="padding-bottom:14px;"> 
			 	                                                            	<h4 class="h4" style="<?php print $common['style_black'];?>font-size:12px;background-image: url('http://eclipse.org/community/eclipse_newsletter/template/images/icon-projects.gif');font-size: 12px;padding:10px 5px 8px 22px;background-repeat: no-repeat;background-position: 4px 8px;background-color: #e8e8dc;">New Releases</h4>
			 	                                                            	<div style="padding-left:0;margin-top:12px;margin-bottom:12px;">
			 	                                                            		<?php foreach($settings['release']['content'] as $re):?>
			                                                              				<div style="margin-bottom:8px;"><a href="<?php print $re['url'];?>" style="<?php print $common['style_blue'];?>font-weight:bold;"><?php print $re['title'];?></a></div>
																					<?php endforeach;?>
																				</div>	
																				<a href="<?php print $settings['release']['more_url'];?>" class="read_more">More Releases →</a> 
                                                                        	</div>
                                                                        <?php endif;?>
                                                                        <?php if(!empty($settings['projects']['content'])):?>
                                                                       		<div id="sidebar_projects" class="block" style="padding-bottom:14px;"> 
			 	                                                            	<h4 class="h4" style="<?php print $common['style_black'];?>font-size:12px;background-image: url('http://eclipse.org/community/eclipse_newsletter/template/images/icon-projects.gif');font-size: 12px;padding:10px 5px 8px 22px;background-repeat: no-repeat;background-position: 4px 8px;background-color: #e8e8dc;">New Projects</h4>
			 	                                                            	<div style="padding-left:0;margin-top:12px;margin-bottom:12px;">
			 	                                                            		<?php foreach($settings['projects']['content'] as $p):?>
			                                                              				<div style="margin-bottom:8px !important;"><a href="<?php print $p['url'];?>" style="<?php print $common['style_blue'];?>font-weight:bold;"><?php print $p['title'];?></a></div>
																					<?php endforeach;?>
																				</div>	
																				<a href="<?php print $settings['projects']['more_url'];?>" class="read_more">More projects →</a> 
                                                                        	</div>
                                                                        <?php endif;?>
                                                                        <?php if(!empty($settings['events'])):?>
                                                                         	<div id="sidebar_events" class="block" style="padding-bottom:14px;">
			 	                                                            	<h4 class="h4" style="<?php print $common['style_black'];?>font-size:12px;font-size: 12px;padding:10px 5px 8px 22px;background-repeat: no-repeat;background-position: 4px 8px;background-image: url('http://eclipse.org/community/eclipse_newsletter/template/images/icon-events.gif');background-color: #e8e8dc;">Upcoming Events</h4>
			 	                                                            	<?php foreach($settings['events'] as $e):?>
			 	                                                            		<p style="padding:0px 0 12px 0 !important;margin:0 !important;"><a href="<?php print $e['url'];?>" style="<?php print $common['style_blue'];?>font-weight:bold;"><?php print $e['title'];?></a>
			 	                                                            		<a href="<?php print $e['url'];?>"  class="read_more">Visit site →</a></p> 
																				<?php endforeach;?>
																				</div>
                                                                        <?php endif;?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Top Image with Content \\ -->
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // End Sidebar \\ -->                                                                                 
                                        </tr>
                                           
                                            <!-- // Begin Content Footer \\ -->
                                         <tr>
                                         	<td colspan="2" id="templateBodyFooter" style="background: url('http://eclipse.org/community/eclipse_newsletter/template/images/footer-container.png') no-repeat;height: 11px;"></td>
                                         </tr> 
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="602" id="templateFooter">
                                    	<tr>
                                        	<td valign="top" class="footerContent">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">                                                    
                                                    <tr>
                                                        <td valign="top" width="120">
                                                            <div>
																<img src="http://www.eclipse.org/community/eclipse_newsletter/template/images/logo-eclipse.jpg"/>
                                                            </div>
                                                        </td>
                                                        <td valign="top" width="420" id="eclipse-info">
                                                            <div>
                                                            	<p style="margin-bottom:5px;margin-top:5px;text-align: right;">
																	<a href="http://www.eclipse.org/legal/privacy.php" style="padding-right:6px;<?php print $common['style_blue'];?>">Privacy Policy</a> 
																	<a href="http://www.eclipse.org/legal/termsofuse.php" style="padding-right:6px;<?php print $common['style_blue'];?>">Terms of Use</a> 
																	<a href="http://www.eclipse.org/legal/copyright.php" style="padding-right:6px;<?php print $common['style_blue'];?>">Copyright Agent</a> 
																	<a href="http://www.eclipse.org/legal/" style="padding-right:6px;<?php print $common['style_blue'];?>">Legal</a> 
																	<a href="http://www.eclipse.org/org/foundation/contact.php" style="padding-right:2px;<?php print $common['style_blue'];?>">Contact Us</a> 
																</p>
                                                              <p style="margin-bottom:5px;margin-top:5px;text-align: right;">Copyright &copy; 2013 The Eclipse Foundation. All rights reserved.</p>										
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="utility" style="padding-top:20px !important;">
                                                            <div>
                                                                &nbsp;<a href="*|UNSUB|*" style="<?php print $common['style_blue'];?>">unsubscribe from this list</a> | <a href="*|UPDATE_PROFILE|*" style="<?php print $common['style_blue'];?>">update subscription preferences</a>&nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>