<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    <a target="_blank" href="http://www.azavea.com/">Azavea</a> is
    pleased to introduce the open source <a
      target="_blank" href="https://www.locationtech.org/projects/technology.geotrellis">GeoTrellis</a>
    geospatial data processing framework to the Eclipse community.
    Released under an Apache 2 license, GeoTrellis is a pure-Scala, open
    source project developed to support geospatial processing at both
    web-scale and cluster-scale. The framework was designed to solve
    three core problems, with an initial focus on raster processing:


  <ul>
    <li>Create low latency, scalable geoprocessing web services;</li>
    <li>Create batch geoprocessing services that can act on large data
      sets by operating on a distributed architecture;</li>
    <li>Parallelize geoprocessing operations to take full advantage of
      multi-core architectures</li>
  </ul>

  <h2>Project History</h2>
  <p>Azavea has been experimenting with techniques for accelerating
    processing of spatial data for almost 10 years. Early efforts
    focused on understanding and overcoming performance bottlenecks in a
    single type of raster processing activity, a weighted overlay
    operation that could support geographic prioritization. After
    creating an initial prototype application that supported real-time
    weighted overlay operations for prioritizing residential real estate
    decisions, Azavea received a small research and development grant
    under the U.S. Department of Agriculture’s Small Business Innovation
    Research (SBIR) grant program (#2006-33610-16777). This seed grant
    enabled the development of DecisionTree, a software framework for
    geographic prioritization. This early framework successfully
    supported real-time weighted overlay operations at the city and
    county scale, but the fact that it only supported a single Map
    Algebra operation limited its utility as a general framework.


  <p>
    In 2010, work on two projects – a sustainable transit web
    application for the <a target="_blank"
      href="http://www.williampennfoundation.org/">William Penn
      Foundation</a> and an educational game focused on watershed
    modeling for the <a target="_blank"
      href="http://www.stroudcenter.org/">Stroud Water Research Center</a>
    – gave Azavea an opportunity to implement changes that would result
    in a more generic low latency geospatial data processing framework.
    While Map Reduce and its Hadoop implementation were attracting a
    great deal of attention for distributed processing, we elected to
    take a different approach. Our primary use case was to provide
    real-time processing for web and mobile applications in which users
    could manipulate model parameters to generate new spatial data.
    Since then, the framework has been used to support a variety of
    applications, including planning, digital humanities, government
    infrastructure investment, and forest growth simulation and
    modeling. Recent work has also extended the framework to support
    machine learning applications for crime risk forecasting and low
    latency processing of data streams. In 2011, Azavea decided to
    release the new software, now called GeoTrellis, as an open source
    project under an Apache 2 license. The project was submitted to the
    Eclipse LocationTech working group in late 2013.
  </p>

  <p>
    While the original goal of the GeoTrellis project was to transform
    user interaction with geospatial data by bringing the power of
    spatial analysis to real time, interactive web applications, this
    has recently been extended to include cluster-scale batch processing
    through the integration of <a target="_blank"
      href="https://spark.incubator.apache.org/">Spark</a>. The
    web-scale use-case is fairly mature, and enables analysts to run
    sub-second response time operations ranging from simple raster math
    (*, +, /, etc.) to fairly sophisticated raster and vector operations
    like Kernel Density and Cost Distance. The cluster-scale use-case is
    a relatively new effort to port GeoTrellis' rich library of
    algorithms to Spark, and thereby support both batch and interactive
    processing on large geospatial datasets such as satellite imagery.
    The Spark effort is a collaboration between Azavea and <a
      target="_blank" href="https://www.digitalglobe.com/">DigitalGlobe</a>.
  </p>

  <img
    src="/community/eclipse_newsletter/2014/march/images/ex-geotrellis-transit2.png"
    width="600" alt="" />
  <br />
  <p>
    <i>Transit Web App Example</i>
  </p>
  <br />
  <img
    src="/community/eclipse_newsletter/2014/march/images/ex-watershed-modeling.png"
    width="600" alt="" />
  <br />
  <p>
    <i>Watershed Modeling Example</i>
  </p>

  <h2>Why GeoTrellis?</h2>

  <p>
    The core GeoTrellis framework provides an ability to process large
    and small data sets with low latency by distributing the computation
    across multiple threads, cores, CPUs and machines. After evaluating
    several language and architectural approaches, Azavea selected <a
      target="_blank" href="http://www.scala-lang.org/">Scala</a> as the
    language and the <a target="_blank" href="http://akka.io/">Akka</a>
    framework to implement an Actor model of distributed processing.
    GeoTrellis includes the ability to rapidly process and distribute
    processing of both raster and vector data, as well as data import
    and conversion tools for an optimized raster data structure, known
    as an ARG file. GeoTrellis is complementary to other open source
    geospatial projects such as GeoServer, OpenLayers and PostGIS.
  </p>

  <p>GeoTrellis spatial data processing is organized into operations.
    Following the formal Map Algebra nomenclature developed by C. Dana
    Tomlin, operations include Local, Focal and Zonal operations for
    raster data, a few vector operations and network operations.
    Multiple operations can be composed into Models. A geoprocessing
    model in GeoTrellis is composed of smaller geoprocessing operations
    with well-defined inputs and outputs.</p>

  <p>GeoTrellis is designed to help a developer create simple, standard
    REST services that return the results of geoprocessing models. Like
    an RDBS that can optimize queries, GeoTrellis will automatically
    parallelize and optimize geoprocessing models where possible.</p>

  <img
    src="/community/eclipse_newsletter/2014/march/images/geotrellis-fast-geoprocessing.jpg"
    width="600" alt="" />
  <br />
  <p>
    <i>Geotrellis Fast Geoprocessing</i>
  </p>


  <h2>New Features and Documentation</h2>
  <p>
    For the recent GeoTrellis 0.9 release, the <a target="_blank"
      href="http://geotrellis.io/">GeoTrellis documentation site</a> was
    significantly revised. It includes both case studies and some
    samples that were developed since the 0.8 release. There is a full
    set of release notes available on the site, but major enhancements
    include:
  </p>
  <ul>
    <li><b>API Refactor</b>: We are moving away from requiring users to
      manually create operations and pass in rasters as arguments. In
      0.9, objects called “DataSources” represent the source of data,
      with the operations to transform or combine that data as methods
      on those objects.</li>
    <li><b>File I/O</b>: Reading ARGs from disk is significantly faster.
      In some cases, improvements are an order of magnitude or more.</li>
    <li><b>Tile Operation Improvements</b>: Running multiple operations
      over tiled data has been greatly improved.</li>
    <li><b>Clustering Improvements</b>: Several steps have been taken to
      make it easier to distribute operations over a cluster using Akka
      clustering.</li>
  </ul>

  <img
    src="/community/eclipse_newsletter/2014/march/images/geotrellis-cluster-processing.jpg"
    width="600" alt="" />
  <br />
  <p>
    <i>Geotrellis Cluster Processing</i>
  </p>


  <h2>Looking Ahead</h2>
  <p>We have big plans for GeoTrellis that will extend its utility and
    make it easier to implement across a broad range of domains.
    Upcoming milestones will include:</p>
  <ul>
    <li>Integrate Apache Spark with GeoTrellis to allow for interactive
      analysis of terabyte raster data sets.</li>
    <li>Support for operating on data stored in the Hadoop Distributed
      File System (HDFS)</li>
    <li>Support for multi-band rasters</li>
    <li>Develop a Scala wrapper for another Eclipse LocationTech
      project, the Java Topology Suite (JTS)</li>
    <li>Add more Map Algebra operations</li>
  </ul>

  <p>This will move us closer to our long-term vision of a general
    purpose, high performance raster geoprocessing library and runtime
    designed to perform and scale for the web.</p>

  <h2>Resources</h2>
  <ul>
    <li><a target="_blank"
      href="https://github.com/geotrellis/geotrellis">GeoTrellis on
        GitHub</a></li>
    <li><a target="_blank"
      href="https://oss.sonatype.org/content/repositories/releases/">Maven
        repository</a></li>
    <li><a target="_blank"
      href="http://geotrellis.github.io/geotrellis/latest/api/#package">API
        Scaladocs</a></li>
    <li><a target="_blank"
      href="https://github.com/geotrellis/geotrellis/issues?milestone=6&page=1&state=open">Issue
        tracker for 0.10</a></li>
    <li><a target="_blank"
      href="https://groups.google.com/forum/#!forum/geotrellis-user">Mailing
        list</a></li>
    <li>IRC: #geotrellis on freenode</li>
  </ul>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/march/images/robert75.png"
        width="75" alt="Robert Cheetham" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Robert Cheetham<br />
        <a target="_blank" href="http://www.azavea.com/">Azavea</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.azavea.com/news/blogs/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/rcheetham">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/+KaiKreuzer">Google +</a></li>
          $og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
