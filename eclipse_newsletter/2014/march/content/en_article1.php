<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>As a developer you may be looking for tools and standards that make
    location information integrate more seamlessly with your application
    or solution. The OGC’s international consensus standards process is
    designed to define, document, and maintain standards that enable
    integration of location content into any application, service, or
    software tool. Providing such seamless integration may seem
    straightforward. Lat/lon coordinates – simple, right? Sure, but what
    if you need to work with polygons and lines? If you’re building a
    service, does your client expect floats or ints? What about
    metadata, measures of uncertainty or units of measurement? Can it
    handle elevation or altitude? What about all those different Earth
    coordinate reference systems (aka projections)?</p>
  <p>OGC working groups consist of very technically qualified
    professionals who have delivered everything from satellite-borne
    Earth imaging system control interface standards to service
    interface standards for layering and presenting GIS raster and
    vector data layers. Some of the 25+ OGC working groups focus on
    problems like data quality, indoor/outdoor location convergence, and
    sensor fusion. The problems are difficult, but everyone
    participating in the OGC hears again and again about the importance
    of simplicity when the goal is interoperability.</p>
  <p>Lack of interoperability when using location content reduces
    customer choice, reduces options for developers, reduces available
    ad markets, reduces opportunities for technology convergence and
    generally retards market growth. Noninteroperability makes the price
    of products and services too great for too many, and thus the market
    demands interoperability. At the same time, developers need to
    provide apps that run on resource-constrained mobile devices, and
    they need standards that help them accelerate – not slow down –
    development schedules. Standards documents, especially when
    accompanied by open source reference implementations, can be seen as
    free intellectual property that saves weeks of development time as
    well as downstream "remorse of non-interoperability."</p>
  <p>This article is the first in a series that will show developers how
    they can maximize their market and business opportunities and reduce
    development times and maintenance costs by implementing simple
    interface and encoding standards that have been developed by the
    OGC.</p>

  <h2>WMS: the granddaddy of web mapping standards</h2>
  <p>The Web Mapping Service (WMS) was OGC’s first Web Services API, and
    back in 1999 represented a prescient commitment to building a
    spatial data sharing architecture on the same architecture as the
    Web. We continue to foster and strengthen that commitment to this
    day as the Web turns 25 this year, and both OGC and the World Wide
    Web Consortium turn 20.</p>
  <p>
    Like most OGC services, WMS has an HTTP GET KVP interface and a HTTP
    POST/SOAP interface. In this article we’ll only cover the KVP, or
    key-value-pair, interfaces. Another commonality across most OGC
    services is that there are three primary operations to the API. One
    is called “GetCapabilities”, and that one gives you access to a lot
    of metadata about the service including what data it holds and what
    the URLs are to access the other operations. Another operation lets
    you look up all the information for a particular feature (the
    geospatial equivalent of a database record). For WMS this operation
    is called “GetFeatureInfo”. We won’t cover that in this article, so
    get the details on that one in the standard available at <a
      target="_blank" href="http://www.opengeospatial.org/standards/wms">http://www.opengeospatial.org/standards/wms</a>.
  </p>

  <img
    src="/community/eclipse_newsletter/2014/march/images/article1.1.png"
    width="600" alt="" />
  <br>
  <p>
    <i><b>Figure 1</b>: A client that implements the OGC WMS Interface
      Standard can get "map images" (in formats such as PNG and JPEG)
      from diverse types of servers (such as GIS, Earth imaging systems,
      and some spatial database systems) that are able to produce such
      images and serve them via interfaces that implement the WMS
      standard. A WMS implementation does not return the actual data,
      but only a raster map image created from the data.</i>
  </p>
  <p>For the purposes of this article, we’ll go straight into the main
    operation of the WMS service, which is “GetMap”. What you basically
    want to do with a WMS (the SERVICE key) is tell it to give you a map
    (the REQUEST key) of a certain geographic area (the BBOX key), a
    certain pixel size on the screen (the HEIGHT and WIDTH keys), having
    certain data layers (the LAYERS key), with those layers styled in
    certain ways (the STYLE key but the value for this key can be blank
    to accept the defaults), in a certain spatial reference system (the
    SRS key) and finally return the image in a specified image format
    like PNG (the FORMAT key).</p>

  <p>Here’s a sample request that gets you an area similar to the image
    in Figure 2 (but the image won’t look the same because this is live
    Radar data and the weather will be different than on the afternoon
    of March 12, 2014):</p>
  <pre>
    <code>
http://gis.srh.noaa.gov/arcgis/services/RIDGERadar/MapServer/WMSServer?
  request=GetMap&amp;service=WMS&amp;version=1.1.0
  &amp;BBOX=-73.929,41.758,-71.747,43.3
  &amp;HEIGHT=500&amp;WIDTH=600
  &amp;LAYERS=0&amp;STYLES=
  &amp;SRS=4326&amp;FORMAT=image/png



  </code>
  </pre>
  <br />
  <p>All WMS services work in this same way. Make a GetCapabilities
    request such as:</p>
  <p>
    <a target="_blank"
      href="http://gis.srh.noaa.gov/arcgis/services/RIDGERadar/MapServer/WMSServer?request=GetCapabilities&service=WMS">http://gis.srh.noaa.gov/arcgis/services/RIDGERadar/MapServer/WMSServer?request=GetCapabilities&service=WMS</a>
  </p>
  <p>And in the XML response find the layers and styles you want to use
    (HINT: most WMSes don’t publish styles so you can usually ignore
    this key) and then just map like crazy!</p>
  <img
    src="/community/eclipse_newsletter/2014/march/images/article1.2.png"
    width="600" alt="" />
  <br>
  <p>
    <i><b>Figure 2</b>: US National Weather Service Radar over the
      Northeast US on March 12, 2014 (WMS service accessed in uDig).</i>
  </p>

  <h2>WFS: getting the raw geographic data</h2>
  <p>Often you need more than a pre-composed graphic of a map in your
    application. You may want to draw the points, lines and polygons
    with your own code so that you control the cartographic aspects of
    map design.</p>

  <p>
    Or you may need the raw data to compute distances or areas or other
    more advanced spatial analyses. Basically, for the same reasons you
    often need to build database queries into an application, you need
    geospatial data queries. And OGC has an interface for that – Web
    Feature Service (WFS). I’ve written a short tutorial on how to
    quickly get acclimated to WFS at <a
      target="_blank" href="http://www.ogcnetwork.net/wfstutorial">http://www.ogcnetwork.net/wfstutorial</a>.
    And once you get your feet wet and need more advance information,
    you can refer to the full WFS version 2.0 standard at <a
      target="_blank" href="http://www.opengeospatial.org/standards/wfs">http://www.opengeospatial.org/standards/wfs</a>.
  </p>

  <img
    src="/community/eclipse_newsletter/2014/march/images/article1.3.png"
    width="600" alt="" />
  <br>
  <p>
    <i><b>Figure 3</b>: Servers that implement the OGC Web Feature
      Service (WFS) Interface Standard return vector source data
      (points, lines, and polygons) encoded in the widely used OGC
      Geography Markup Language (<a target="_blank"
      href="http://www.opengeospatial.org/standards/gml">GML</a>)
      format. The entire GML specification is comprehensive and "fat,"
      but most applications use GML profiles, which can be
      application-specific and very "thin" in terms of storage and
      bandwidth requirements.</i>
  </p>
  <p>One interesting WFS to play around with as you learn the interface
    is the geographic names service from the US Geologic Survey (USGS).
    Once you follow the tutorial, you’ll learn that the service’s
    capabilities document has all the information required to use it.
    For this service, you’ll find that here:</p>
  <p>
    <a target="_blank"
      href="http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetCapabilities">http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetCapabilities</a>
  </p>
  <p>A quick way to see the data available is request everything, but
    limit the records returned to five, like so:</p>
  <p>
    <a target="_blank"
      href="http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetFeature&typename=WFS_geonames:Administrative&maxfeatures=5">http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetFeature&typename=WFS_geonames:Administrative&maxfeatures=5</a>
  </p>
  <p>
    Looking at that response, you can get a flavor for what the data
    looks like. It’s in a standard flavor of XML defined by OGC called <a
      target="_blank" href="http://www.opengeospatial.org/standards/gml">GML</a>.
    The details of GML make up a topic for another article, but suffice
    it to say that the GML for every data set may be different, but once
    you look at a few records, its pretty easy to build an application
    that processes that XML.
  </p>
  <p>To construct advanced queries, WFS uses a language called Filter,
    which is the geospatial equivalent of SQL, but many developers never
    even need to use Filter. What people usually want to do is search
    within a particular area. In fact, this may be the only request you
    use on a regular basis once you’re familiar with the service. Once
    again, using that USGS geographic names service, this query gives
    you places in the Monterrey, California area:</p>
  <p>
    <a target="_blank"
      href="http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetFeature&typename=WFS_geonames:Administrative&bbox=-121.897,36.579,-121.815,36.624">http://services.nationalmap.gov/arcgis/services/WFS/geonames/MapServer/WFSServer?service=WFS&request=GetFeature&typename=WFS_geonames:Administrative&bbox=-121.897,36.579,-121.815,36.624</a>


  <p>
    Instead of using the "maxfeatures" key, we use a "bbox", and specify
    the lower left and upper right coordinates of a bounding box to
    search. And here is a truncated listing of the query response. The
    main sections of the XML are the root element,
    <code>&lt;wfs:FeatureCollection&gt;</code>, which contains a <code>&lt;gml:boundedBy&gt;</code> that defines the geographic extent of all the data in the response and a series of <code>&lt;gml:featureMember&gt;</code> elements, each of which is a “record” in the geographic database:

  </p>

  <pre class="prettyprint lang-xml">
&lt;wfs:FeatureCollection&gt;
  &lt;gml:boundedBy&gt;
    &lt;gml:Envelope srsName="EPSG:4326"&gt;
      &lt;gml:lowerCorner&gt;-14.545293023256136
      -179.11623072720781&lt;/gml:lowerCorner&gt;
      &lt;gml:upperCorner&gt;71.330629333030572
      179.74942126888595&lt;/gml:upperCorner&gt;
    &lt;/gml:Envelope&gt;
  &lt;/gml:boundedBy&gt;
  &lt;gml:featureMember&gt;
    &lt;WFS_geonames:Administrative gml:id="F59__3098"&gt;
      &lt;WFS_geonames:OBJECTID&gt;3098&lt;/WFS_geonames:OBJECTID&gt;
      &lt;WFS_geonames:Shape&gt;
        &lt;gml:MultiPoint&gt;
          &lt;gml:pointMember&gt;
            &lt;gml:Point&gt;
              &lt;gml:pos&gt;36.593571199449457
              -121.83689600049178&lt;/gml:pos&gt;
            &lt;/gml:Point&gt;
          &lt;/gml:pointMember&gt;
        &lt;/gml:MultiPoint&gt;
      &lt;/WFS_geonames:Shape&gt;
      &lt;WFS_geonames:feature_id&gt;222239&lt;/WFS_geonames:feature_id&gt;
      &lt;WFS_geonames:gaz_name&gt;Del Rey Park&lt;/WFS_geonames:gaz_name&gt;
      &lt;WFS_geonames:gaz_featureclass&gt;Park
        &lt;/WFS_geonames:gaz_featureclass&gt;
      &lt;WFS_geonames:state_alpha&gt;CA&lt;/WFS_geonames:state_alpha&gt;
      &lt;WFS_geonames:county_name&gt;MONTEREY&lt;/WFS_geonames:county_name&gt;
    &lt;/WFS_geonames:Administrative&gt;
  &lt;/gml:featureMember&gt;
  &lt;gml:featureMember&gt;
    &lt;WFS_geonames:Administrative gml:id="F59__3376"&gt;
      &lt;WFS_geonames:OBJECTID&gt;3376&lt;/WFS_geonames:OBJECTID&gt;
      &lt;WFS_geonames:Shape&gt;
        &lt;gml:MultiPoint&gt;
          &lt;gml:pointMember&gt;
            &lt;gml:Point&gt;
              &lt;gml:pos&gt;36.596071099445567
              -121.84356300048142&lt;/gml:pos&gt;
            &lt;/gml:Point&gt;
          &lt;/gml:pointMember&gt;
        &lt;/gml:MultiPoint&gt;
      &lt;/WFS_geonames:Shape&gt;
      &lt;WFS_geonames:feature_id&gt;234363&lt;/WFS_geonames:feature_id&gt;
      &lt;WFS_geonames:gaz_name&gt;Noche Buena&lt;/WFS_geonames:gaz_name&gt;
      &lt;WFS_geonames:gaz_featureclass&gt;Civil
        &lt;/WFS_geonames:gaz_featureclass&gt;
      &lt;WFS_geonames:state_alpha&gt;CA&lt;/WFS_geonames:state_alpha&gt;
      &lt;WFS_geonames:county_name&gt;MONTEREY&lt;/WFS_geonames:county_name&gt;
    &lt;/WFS_geonames:Administrative&gt;
  &lt;/gml:featureMember&gt;
&lt;/wfs:FeatureCollection&gt;
</pre>
  <br />
  <p>Now that you’ve played around with the WFS interface a bit, it
    should be fun to use. There are many WFS servers around the world,
    especially with environmental data. So take advantage of all that
    open geodata around the web.</p>

  <h2>GeoSMS, ready for blast-off...</h2>
  <p>The OGC Open GeoSMS Standard allows applications to location-enable
    Short Message Service (SMS) messages. SMS, of course, is the text
    communication service component of phone, web and mobile
    communication systems. SMS uses standardized communications
    protocols that allow the exchange of short text messages between
    fixed line or mobile phone devices.</p>
  <img
    src="/community/eclipse_newsletter/2014/march/images/article1.4.png"
    width="600" alt="" />
  <br>
  <p>
    <i><b>Figure 4</b>: In Chinese Taipei, where earthquakes and heavy
      rains combine to cause disastrous mudslides and debris flows, Open
      GeoSMS is used both to get data from sensors and to send alerts to
      citizens via their phones.</i>
  </p>

  <p>The Open GeoSMS expression uses the following structure:</p>
  <pre>
    <code>
HTTP/HTTPS URI
Payload (Optional, freestyle text.)

</code>
  </pre>

  <br />
  <p>The rules are simple but explicit. In the HTTP/HTTPS URI that
    provides location, for example, "The value of the latitude and
    longitude are described using the Decimal Degree format based on
    WGS84 without the symbol of “°”. The values of latitude and
    longitude are bounded by ±90° and ±180° respectively. Positive
    latitudes are north of the equator, negative latitudes are south of
    the equator."</p>

  <p>The first line of an Open GeoSMS message is an HTTP/HTTPS URI.
    Following lines can contain an optional human-readable message.
    Here’s an example:</p>
  <pre>
    <code>
<a target="_blank"
        href="http://maps.geosms.cc/showmap?geo=23.9572,120.6860&GeoSMS">http://maps.geosms.cc/showmap?geo=23.9572,120.6860&GeoSMS</a>
I NEED TOWING SERVICE NOW

</code>
  </pre>

  <br />
  <p>This simple SMS extension enables developers to facilitate
    communication of location content between different LBS devices or
    applications. The encoding is extremely lightweight and the
    standards document is a quick read.</p>
  <p>
    The OGC Open GeoSMS Standard can be found at: <a target="_blank"
      href="http://www.opengeospatial.org/standards/opengeosms">http://www.opengeospatial.org/standards/opengeosms</a>.
  </p>
  <p>
    Why is Open GeoSMS important? Consider two facts from
    mobi-thinking’s February 2013 “The essential compendium of
    need-to-know statistics” <a target="_blank"
      href="http://mobithinking.com/mobile-marketing-tools/latest-mobile-stats#mobile-use">http://mobithinking.com/mobile-marketing-tools/latest-mobile-stats#mobile-use:</a>
  </p>
  <ul>
    <li>SMS is king of mobile messaging with more than 7.8 trillion SMS
      trillion messages sent in 2011.</li>
    <li>The most effective form of ads was opt-in SMS in many countries.</li>
  </ul>
  <p>Open GeoSMS not only provides a practical, streamlined way to add
    location to SMS, it provides it in an open standards-based manner,
    which means that anyone and everyone can freely use the same
    approach. Open GeoSMS coordinates are GML-compatible, so they will
    work with emergency services, e-transportation systems, World
    Meteorological Organization weather data, DigSafe systems, and
    enterprise systems that use any of the major database software
    products for interoperable location applications.</p>
  <p>Bottom line: widespread implementation of Open GeoSMS will mean
    fewer stovepipes and bigger available markets for app developers and
    advertisers. It may provide new ways of communicating via SMS that
    are unforeseen.</p>

  <h2>Useful OGC services</h2>
  <p>Now that you know a bit about coding to OGC standard interfaces,
    having a list of reliable services from which to get data is nice to
    have on hand. You’ll find that many government agencies around the
    world provide access to their holdings via OGC interfaces. Here are
    a handful that you can try.


  <ul>
    <li><a target="_blank"
      href="http://nsidc.org/data/atlas/ogc_services.html">Atlas of the
        Cryosphere</a>

    <li><a target="_blank" href="http://inspire-geoportal.ec.europa.eu/">EU
        INSPIRE Geoportal</a>

    <li><a target="_blank"
      href="http://www.geoportal.org/web/guest/geo_home_stp">Global
        Earth Observation System of Systems (GEOSS)</a>

    <li><a target="_blank"
      href="http://www.fws.gov/wetlands/Data/Web-Map-Services.html">US
        Fish & Wildlife Service National Wetlands Inventory</a>

    <li><a target="_blank"
      href="http://www.nws.noaa.gov/gis/otherpage.html">US National
        Weather Service</a>

    <li><a target="_blank"
      href="http://www.nationalatlas.gov/infodocs/ogcwms.html">US
        National Atlas</a>

  </ul>

  <h2>More to come</h2>
  <p>
    OGC is always looking for input from developers if you have any
    feedback for us. Please visit the OGC website at <a target="_blank"
      href="http://www.opengeospatial.org/contact">http://www.opengeospatial.org/contact</a>
    and find more developer resources at <a target="_blank"
      href="http://www.ogcnetwork.net/tutorials">http://www.ogcnetwork.net/tutorials</a>.

    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
      type="text/javascript"></script>
    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
      type="text/javascript"></script>
    <script type="text/javascript">
  prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/march/images/raj75.png"
        alt="rej singh" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Raj Singh<br />
        <a target="_blank" href="http://www.opengeospatial.org/">Open
          Geospatial Consortium</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://www.opengeospatial.org/blogs/raj-singh">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/rajrsingh">Twitter</a></li>
        <li><a target="_blank"
          href="https://www.linkedin.com/in/rajrsingh">LinkedIn</a></li>
        <!--og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
