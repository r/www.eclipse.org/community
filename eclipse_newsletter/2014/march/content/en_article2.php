<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>Every project working with geospatial information eventually faces
    the problem of managing change over time. At the center of the issue
    is data provenance: where the data originated, whom it belongs to,
    and what set of individual changes were made to a particular piece
    of information in order to reach its current state. While versioning
    approaches have existed for a while, they are cumbersome and have
    been a challenge in many workflows, especially those that involve
    more than one individual. We created GeoGit to help solve these
    problems.</p>

  <p>GeoGit takes concepts and lessons learned from working with code in
    open source communities and applies them to managing geospatial
    information. GeoGit allows for decentralized management of versioned
    data and enables new and innovative workflows for collaboration.
    Users are able to track edits to geospatial information by importing
    raw data into repositories where they can view history, revert to
    older versions, branch into sandboxed areas, merge back in, and push
    to remote repositories.</p>

  <h2>Working with GeoGit</h2>
  <p>Once installed, a simple working session might look like this (data
    references are from the freely available Natural Earth collection):</p>

  <p>
    1. Create a repository and import raw geospatial data (from
    Shapefiles and spatial databases such as PostGIS, Oracle Spatial or
    SQL Server):</p>


  <pre>
    <code>
mkdir repo
cd repo
geogit init
geogit shp import ne_110m_coastline.shp
</code>
  </pre>
  <p>
    2. Add the imported data to the staging area. This command signals
    that this is information to be versioned and tracked and prepares it
    for final insertion into the repository.</p>


  <pre>
    <code>
geogit add
</code>
  </pre>

  <p>
    3. Commit the information to the repository. Developers familiar
    with Git will appreciate the familiar API and command line options.
    In this case, we are passing a commit message that will be
    associated with this change.</p>


  <pre>
    <code>
geogit commit -m “Add coastline”
</code>
  </pre>

  <p>
    4. In order to make changes and collaborate with others, a typical
    workflow involves creating branches to isolate changes from the
    master branch. Creating a branch in GeoGit is as easy as issuing the
    following command:</p>


  <pre>
    <code>
geogit branch branch1
</code>
  </pre>

  <img
    src="/community/eclipse_newsletter/2014/march/images/article2.1.png"
    width="600" alt="" />
  <br />

  <p>This creates a new branch called branch1 where all commits will go
    to until another branch is chosen. Branching is an important concept
    in GeoGit as it enables editors of geospatial content to modify
    information without worrying about interfering with the quality of
    the main version, usually stored in the master branch.</p>

  <p>
    5. When changes are ready to be brought back into the main version,
    they can be merged into another branch using the merge command.</p>


  <pre>
    <code>
geogit checkout master (switches to the master branch)
geogit merge edits
</code>
  </pre>

  <p>Upon merging, if conflicts are detected (for example, two users
    independently modify the same geometry with different outcomes) a
    merge conflict is returned and a commit cannot happen until the
    conflict is resolved. This is an important feature that prevents
    geospatial data corruption and enforces workflows that involve data
    quality assurance.</p>

  <h2>Voila!</h2>
  <p>Anyone familiar with tools like Git, which handles distributed
    version control for source code, will immediately see the advantages
    this approach brings.</p>

  <p>
    GeoGit is an open source project based on the Java platform and is
    developed by committers across several organizations. It has
    recently been submitted as a project of the LocationTech working
    group within the Eclipse Foundation. GeoGit has also been designed
    to be extensible, and there is already a <a target="_blank"
      href="https://github.com/boundlessgeo/geogit-py">Python wrapper
      library</a> that make these operations easier and enables
    automation.
  </p>

  <p>
    To learn more, visit <a target="_blank" href="http://geogit.org/">http://geogit.org/</a>
    or watch a full presentation on how we're redefining geospatial data
    versioning with GeoGit below.
  </p>

  <iframe width="560" height="315"
    src="//www.youtube.com/embed/buy-G4VsHiw"
    allowfullscreen></iframe>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/march/images/jmarin75.png"
        width="75" alt="Juan Marin" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Juan Marin<br />
        <a target="_blank" href="http://boundlessgeo.com/">Boundless</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://guachintoneando.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/jmarinotero">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/+IanSkerrett">Google +</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
