<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>For almost 10 years, the Java programming language has been stable;
    the small language enhancements added in Java 7 did not require much
    learning. It has been said that a new programming language is only
    relevant if it changes the way you think about software. For some of
    us, Java 8 will fit this description; to embrace Java 8 means to
    shed some old habits and approach some problems of day-to-day
    programming in an entirely new way.</p>
  <p>
    The new version of Java may look nice on paper and slides, but for
    developers to become productive writing programs in Java 8, an IDE
    is needed. In Eclipse this IDE is provided by the <a
      href="http://www.eclipse.org/jdt/">Java Development Tools Project
      (JDT)</a>. The JDT team has worked hard for close to 2 years; we
    are well on schedule to deliver a full-featured IDE for Java 8 right
    when Java 8 is released in March.
  </p>
  <p>So what support for Java 8 may Eclipse users expect from JDT?</p>

  <h1>Tooling for Java 8</h1>
  <p>The first tool needed for working with Java is a compiler. As many
    readers will know, at JDT we maintain our own Eclipse Compiler for
    Java™ (ECJ). We are occasionally asked, why we do so, rather than
    just using javac.</p>
  <p>I have three answers:</p>
  <ol>
    <li>ECJ supports incremental compilation where the IDE can trigger
      compilation of only those files that have changed or are
      transitively affected by a change.</li>
    <li>ECJ is used behind the scenes for all IDE features that require
      structured knowledge about Java programs (vs. just operating on
      flat text files).</li>
    <li>Competition between different compiler teams is actually healthy
      for the community.</li>
  </ol>

  <h1>Evolving the compiler for Java 8</h1>
  <p>Extending ECJ for Java 8 was by far the biggest effort for the JDT
    team. While the team is still very busy wrapping up the various
    tasks, we are confident that in March we will ship a full-fledged
    Java 8 compiler.</p>
  <p>As with any evolution task, you never know up-front, whether the
    existing architecture will actually support the new requirements. In
    particular, the parser generator jikespg, used for creating the JDT
    parser, strongly disliked the new syntax rules. During an early
    stage it was not clear at all, whether it would be technically
    possible to tweak the new grammar rules so that they would be
    accepted by the parser generator. If the answer had been negative,
    we would have had to implement an entirely new parser. Given that
    the parser is the very bottom of a tall tool stack, exchanging the
    parser would have endangered the entire development schedule.
    Luckily, some new interaction between scanner and parser could be
    invented, that allowed us to bypass the limitations of the parser
    generator. The existing architecture can thus be maintained.</p>
  <p>Another challenge was in coping with updates of the Java Language
    Specification trickling in until late in the game. Development of a
    central new component, the new type inference engine, could only be
    seriously started as late as fall 2013.</p>

  <h1>Empower the IDE to handle Java 8</h1>
  <p>On top of the updated compiler, all IDE functionality can cope with
    the new language features. This includes all code browsing
    functionality, like hyperlinking, text hovers and search. Also
    formatting, code assist, quick assists, quick fixes, refactoring and
    debugging; all understand the new constructs and should work in Java
    8 programs exactly as you are accustomed to. Much of this is due to
    a major update of the DOM AST API, which also allows third-party
    plugins to inspect and manipulate Java 8 programs.</p>
  <p>JDT even offers a new quick assist that lets you convert an
    anonymous class into a lambda expression (if possible).</p>

  <h1>Leverage type annotations for null analysis</h1>
  <p>
    The existing support for annotation-based null analysis has been
    lifted to use type annotations (<a target="_blank"
      href="https://jcp.org/en/jsr/detail?id=308">JSR 308</a>). What may
    sound like a small change is actually a major step towards
    statically detecting all potential null pointer exceptions. To
    understand the significance consider a variable
    <code>"@NonNull List&lt;Person&gt; persons"</code>
    . In previous versions we had no chance to tell if an expression
    like
    <code>"persons.get(i).getName()"</code>
    is safe, because we wouldn't know whether
    <code>"get(i)"</code>
    could possibly return null or not. With type annotations we can now
    extend the declaration to saying
    <code>"&#64;NonNull List&lt;&#64;NonNull Person> persons"</code>
    , which will effectively protect the list against insertion of a
    null value. This establishes the guarantee that
    <code>"get()"</code>
    on this particular list will never return null! Conversely, a
    <code>"List&lt;&#64;Nullable Person>"</code>
    will require a null-check each time we extract and use an element
    from the list.
  </p>
  <p>Migrating to the new scheme requires a few adjustments for programs
    already using null annotations; in particular annotations on
    qualified type names and annotations on arrays or varargs need to be
    updated to the new syntax. A migration guide will be provided in
    time for the release.</p>

  <h1>The team behind the tool</h1>
  <p>In the initial several months of the effort, the core JDT team at
    IBM saw some staffing challenges due to experienced committers
    leaving the team for other projects, higher studies, or to start up
    on their own. Fortunately, the team received excellent commitment
    with follow through from the broader community.</p>
  <p>During this period, JDT/Core could welcome two additional non-IBM
    committers: Jesper Møller and Andy Clement. It shouldn't come as a
    surprise that Andy, just like myself, came to JDT from the
    background of having worked on other language IDEs on top of JDT. In
    Andy's case it's for AspectJ and Groovy, in my case for Object
    Teams. Speaking for myself, I can report that working on Java 8
    support in JDT was a big enabler for integrating the new Java
    features also in Object Teams; the plan is indeed, to publish a
    Java-8-enabled version of the Object Teams Development Tooling on
    the same day as JDT for Java 8. Jesper, finally, is what project
    lead Srikanth enthusiastically calls a “plug and play" team member –
    providing high quality contributions across several different
    subsystems of JDT.</p>
  <p>If JDT will see continued help from its three non-IBM committers
    even after the Java 8 story has been completed, we are looking
    forward to splendid times.</p>
  <p>In addition, we have received a great lot of excellent bug reports.
    Provided test programs, precisely pin-pointing a defect in the
    compiler, have been of great help in improving the implementation.
    We have also received more code contributions and proposed patches
    than in “normal” times. Thanks to all!</p>
  <p>For the last mile towards the release we are extending our request
    for testing our early access builds. For January 31 we plan our next
    “early access release”, starting a 45 day beta program before GA in
    March. Please see the following link for installation details, try
    it and report any bugs you might find:</p>
  <p>
    <code>
      <a
        href="http://wiki.eclipse.org/JDT/Eclipse_Java_8_Support_(BETA)">http://wiki.eclipse.org/JDT/Eclipse_Java_8_Support_(BETA)</a>
    </code>
  </p>
  <p>If you happen to attend EclipseCon North America in March, be sure
    to look for our presentations, titled “JDT embraces type
    annotations” and the main talk at “JDT embraces lambda expressions”.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/january/images/stephan75.jpg"
        alt="stephan hermann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Stephan Hermann<br />
        <a target="_blank" href="http://www.gk-software.com">GK Software</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://objectteams.wordpress.com/">Blog</a></li>
        <!--<li><a target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
          <li><a target="_blank" href="">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
