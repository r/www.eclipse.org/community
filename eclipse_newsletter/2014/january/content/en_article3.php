<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <h3>What is e(fx)clipse</h3>
  <h4>Tooling</h4>
  <p>e(fx)clipse is an Eclipse.org project that provides JavaFX
    capabilities for the Eclipse IDE, so that people can easily develop
    JavaFX applications inside their favorite IDE.</p>
  <p>To make the development process as smooth as possible, it provides
    the following:</p>
  <ul>
    <li>automatic detection of JavaFX inside JDK7 and JDK8</li>
    <li>specialized CSS-Editor who knows all custom JavaFX’ attributes</li>
    <li>specialized FXML-Editor on top of WST-XML including a live
      preview</li>
    <li>wizards to bootstrap standard JavaFX projects and OSGi enabled
      JavaFX projects</li>
  </ul>

  <h4>Runtime</h4>
  <p>Additionally, e(fx)clipse also provides runtime components to
    develop OSGi enabled JavaFX applications, including otherwise
    missing features when using JavaFX in an OSGi environment.</p>
  <p>Besides providing single components to develop pure OSGi
    applications, it also provides a complete framework implementation
    on top of the core Eclipse 4 technologies (modelled application and
    DI container), allowing people to develop RCP applications with any
    level of complexity.</p>

  <h3>Tooling</h3>
  <h4>Basic IDE integration</h4>
  <p>In Java 7, JavaFX is part of the default JDK/JRE install, but
    surprisingly it is not found on any of the default classpaths, so
    e(fx)clipse comes with a helper ClasspathLibrary that fixes this
    problem. In Java 8, JavaFX is within the extension classpath, but
    because the source code is not part of the ordinary JDK src.zip,
    stepping through the JavaFX source code using JDT would not be
    possible for the developer while debugging. e(fx)clipse works around
    this limitation, and so developers can step through JavaFX code
    while debugging and are shown JavaFX related JavaDoc when hovering
    over JavaFX APIs in their Java editor.</p>

  <img
    src="/community/eclipse_newsletter/2014/january/images/article3.1.png"
    width="600" alt="button class" />
  <br>

  <h4>JavaFX CSS Editor</h4>
  <p>CSS is a very important technology for people developing JavaFX
    applications, because all the theming is done through
    CSS-Stylesheets. JavaFX’ CSS-Attributes are quite different from the
    known HTML CSS-Attributes, instead it comes with a huge set of
    custom properties, some working similar to their web counterparts,
    others completely custom.</p>
  <p>e(fx)clipse provides a very smart CSS-Editor that knows exactly,
    what attribute can be applied to which element definition. It is
    even clever enough to detect the used JavaFX version and shows
    different proposals based upon the targeted runtime.</p>

  <img
    src="/community/eclipse_newsletter/2014/january/images/article3.2.png"
    width="600" alt="background fills" />
  <br>

  <h3>FXML Editor</h3>
  <p>Next to Java and CSS, the 3rd important technology used by JavaFX
    developers is FXML. FXML is a declarative language to define JavaFX
    UIs. While Oracle provides a WYSIWYG tool called SceneBuilder, many
    developers still prefer to the define their UIs by directly editing
    the FXML-File.</p>
  <p>e(fx)clipse’s specialized FXML-Editor supports Java editor like
    features like auto completion, javadoc hovers, quick fixes and much
    more.</p>

  <img
    src="/community/eclipse_newsletter/2014/january/images/article3.3.png"
    width="250" alt="button" />
  <br>

  <h4>FXGraph Editor</h4>
  <p>FXGraph is a custom DSL provided only by e(fx)clipse, that removes
    much of the noise when using FXML to define UIs. Its syntax looks
    similar to JSON and “compiles” FXML so that no additional runtime
    libraries are needed.</p>

  <img
    src="/community/eclipse_newsletter/2014/january/images/article3.4.png"
    width="600" alt="button" />
  <br>
  <p>
    More information on FXGraph can be found in the e(fx)clipse wiki: <a
      target="_blank"
      href="http://wiki.eclipse.org/Efxclipse/Tooling/FXGraph">http://wiki.eclipse.org/Efxclipse/Tooling/FXGraph</a>.




  <h4>Embedded & Mobile support</h4>
  <p>Although not backed by an Oracle JVM, JavaFX applications can run
    on Android (Davlik) and iOS (AOT with RoboVM) devices. e(fx)clipse
    provides a preview mode for mobile resolutions and a small runtime
    framework that provides basic transition effects between screens.</p>
  <p>Another important issue when running on resource constrained
    devices, ie. with less powerful CPUs, is that the overhead of
    constructing UIs from FXML is too big. Because of that, e(fx)clipse
    provides a FXML to Java “compiler” (or better converter), that runs
    at build time.</p>

  <h3>Runtime</h3>
  <h4>Basic runtime components</h4>
  <p>JavaFX comes with a default set of layout containers, but people
    switching from SWT to JavaFX are probably used to the layout
    containers used in SWT. e(fx)clipse provides layout panels that use
    the same layout algorithms as known from the SWT world.</p>

  <h4>EMF-Edit integration</h4>
  <p>EMF itself has support to populate structured SWT controls like
    Lists, Tables, Trees and TreeTables with EMF objects. e(fx)clipse
    provides EMF edit hooks that can be used by JavaFX controls like
    ListView, TableView and TreeView. JavaFX8 added a TreeTableView that
    is supported as well.</p>

  <h4>OSGi integration</h4>
  <p>JavaFX does not work out of the box in OSGi. On Java 7, it was not
    found on any classpath and even though it is on the extension
    classpath in Java 8, it is not part of an EE. So Equinox will not
    wire JavaFX packages by default. e(fx)clipse provides an Equinox
    Adapter Hook, that integrates JavaFX into OSGi, so that developers
    don’t have to worry about this issue, too.</p>

  <h4>e4 integration</h4>
  <p>The Eclipse 4 framework was designed to be widget agnostic since
    day one. e(fx)clipse comes with a full implementation of the widget
    specific parts of the Eclipse 4 framework. So people can easily
    implement JavaFX applications of any complexity on top of the core
    Eclipse 4 concepts, like modelled applications, dependency injection
    and OSGi.</p>

  <img
    src="/community/eclipse_newsletter/2014/january/images/article3.5.png"
    width="600" alt="details" />
  <br>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/january/images/tom75.jpg"
        alt="tom schindl" />
        </div>
        <div class="col-sm-16">
         <p class="author-name">
        Tom Schindl<br />
        <a target="_blank" href="http://www.bestsolution.at">BestSolution.at</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://tomsondev.bestsolution.at">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/tomsontom">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/b/107434916948787284891/109870559592385719776/posts">Google +</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
