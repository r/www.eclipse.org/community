<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <h2>JVM Convergence</h2>

  <p>
    As Henrik St&aring;hl, VP of Product Management in the Java Platform
    Group at Oracle outlined Oracle’s JVM strategy at the JavaOne
    conference back in 2010, the high level plan presented to the
    JavaOne audience and on Henrik’s blog foresaw that the best features
    from HotSpot and JRockit would be merged into a single VM. Dubbed <i>JVM
      convergence</i>, this process was expected to take multiple years.
  </p>
  <p>While the first fruits of this labor showed up in JDK 7, the bulk
    of JVM convergence effort has been planned for JDK 8. A quick glance
    at the planned JDK 8 feature list, which is available on the
    corresponding OpenJDK Project’s web site, shows a number of
    improvements in the JVM area, like the removal of the permanent
    generation, that have been implemented for JDK 8 as part of the JVM
    convergence effort.</p>
  <p>Fortunately, you don’t have to wait until JDK 8 is released to
    benefit from a small selection of the new JVM features, as a few of
    them have, after careful backporting and testing, made their way
    into JDK 7 Update releases. One of the most notable of them is Java
    Mission Control, which is bundled with the HotSpot JVM starting with
    the release of Oracle JDK 7 Update 40 (7u40).</p>

  <h2>History</h2>

  <p>Java Mission Control’s ancestor is JRockit Mission Control, the
    production time profiling and diagnostics tools suite for the
    JRockit JVM. For in-depth background and information on the JRockit
    JVM and JRockit Mission Control, I’d recommend the book “Oracle
    JRockit: The Definitive Guide” by Marcus Hirt and Marcus Lagergren.</p>
  <p>What began as an effort to create tooling to analyze how real
    production systems were using the JRockit JVM, evolved into an
    efficient, low overhead way to gather and analyze telemetry data of
    applications running on the JRockit JVM, allowing the development
    team to reproduce and address customer issues without requiring
    access to their source code, or their production environment.</p>
  <p>
    Instead of using more traditional and invasive techniques like
    bytecode instrumentation (which has the obvious drawback that the so
    instrumented application being analyzed is no longer made up of the
    same bytecode that was running in production and exhibited some
    interesting behavior) or JVMTI, Java Mission Control relies on a
    ‘flight recorder’ feature in the HotSpot JVM - which is accordingly
    called <i>Java Flight Recorder</i>.
  </p>
  <p>Recordings of JVM telemetry data can be produced by the Java Flight
    Recorder on demand, analyzed directly or stored for later analysis
    inside Java Mission Control using the Flight Recorder tool. The data
    includes an execution profile, memory allocation and garbage
    collection statistics, optimization decisions, as well as
    information on I/O and locking events. Given that its focus is on
    gathering data with the lowest possible impact on the running
    application, Java Mission Control is suitable for use in both
    development and production environments.</p>

  <h2>Availability</h2>

  <p>Oracle Java Mission Control and Oracle Java Flight Recorder are
    commercial features, which are available as part of the Oracle Java
    SE Advanced product. They are freely available for download for
    development and evaluation purposes as part of Oracle JDK 7 Update
    40, as per the terms in the Oracle Binary Code License Agreement,
    but require an Oracle Java SE Advanced license for production use.</p>

  <h2>Getting Started</h2>

  <p>
    If you have downloaded and installed Oracle JDK 7 Update 40, or a
    later release, you can start Java Mission Control by running the jmc
    application in the <i>bin</i> directory of an Oracle JDK 7 Update 40
    installation. If you are an Eclipse user, you’ll be greeted by a
    familiar looking user interface - which may come as a surprise.
  </p>
  <p>Java Mission Control is built upon the Eclipse RCP technology. If
    you are interested in the background and details behind getting an
    Eclipse RCP based application into the JDK, I’d recommend the
    upcoming “Experiences from building one of the world's most
    distributed RCP apps” presentation at EclipseCon 2014 by Klara Ward
    and Marcus Hirt from Oracle. And of course, if you’d like to see
    Oracle Java Mission Control in action at EclipseCon, Marcus Hirt and
    David Lindholm have a session titled “The new profiling tools in the
    Oracle JDK!” that goes into the details.</p>
  <p>
    Besides the standalone Java Mission Control tool, if you instead
    prefer to run your development tools inside the Eclipse IDE, the
    Oracle Java Mission Control home page at <a target="_blank"
      href="http://download.oracle.com/technology/products/missioncontrol/updatesites/base/5.5.0/eclipse/">http://oracle.com/missioncontrol</a>
    provides a pointer to the Eclipse Update site for the respective set
    of plugins. For the best user experience, it is recommended to use
    Eclipse IDE 3.8.2. Please note that in that case you’ll need to run
    your Eclipse IDE on the JDK in order for all the feature of Java
    Mission Control to work as expected. Detailed installation
    instructions are linked off the Oracle Java Mission Control home
    page.
  </p>

  <h2>Using the JMX Console</h2>

  <p>If you are familiar with the JConsole utility in the JDK, you’ll
    find the Java Mission Control JXM Console easily accessible,
    providing you with a superset of the functionality. For a start, it
    lets you detect and connect to Java processes running either locally
    on your machine, or remotely, through the Java Discovery Protocol
    (JDP) or given a JMX agent URL.</p>
  <p>Once connected, the JMX console allows you to monitor and visualize
    the JMX data of the Java process, to take actions depending on the
    observed values triggering conditions, to store recorded data for
    later analysis, and, of course, to load it back into Java Mission
    Control to take a deeper look into the observed behavior.</p>
  <p>Typically, the JMX Console would be used to continuously monitor a
    running process, by observing a small set of data of interest like
    the CPU load or heap usage. Once a certain condition is met, the
    console could be configured to dump the Flight Recorder data, for
    example, for later evaluation.</p>

  <h2>The Java Flight Recorder</h2>

  <p>When something goes wrong unexpectedly, it’s very important to be
    able to find out the exact circumstances in order to track down the
    root cause of failure. The Java Flight Recorder can be thought of as
    such a tool for the HotSpot JVM - it efficiently records information
    about the JVM and its environment, for as long as it’s running. When
    something interesting happens, the recorded data can be dumped and
    stored for later, off-line analysis, or it can be simply stored on
    an ongoing basis. It has a low performance impact, less then 2% for
    typical Java applications, when collecting data and events.</p>

  <h2>Creating Flight Recordings</h2>

  <p>
    In order to create flight recordings, you have to explicitly enable
    the Java Flight Recorder in the HotSpot JVM you’d like to monitor by
    passing a few additional flags to it upon startup. For details,
    please consult the extensive Java Flight Recorder Runtime Guide
    available at <a target="_blank"
      href="http://docs.oracle.com/javase/7/docs/technotes/guides/jfr">http://docs.oracle.com/javase/7/docs/technotes/guides/jfr</a>.
  </p>
  <p>Recordings can be timed or continuous. Timed recordings run for a
    given amount of time, and stop automatically once the time is up.
    When you create a timed recording in Java Mission Control, it will
    be automatically stored, downloaded and loaded into the tool for
    analysis once the recording completes. Continuous recordings, on the
    other hand, run until they are stopped by the end user.</p>
  <p>While creating recordings directly from Java Mission Control is the
    easiest choice for developers, users who need more flexibility can
    use the jcmd command line tool, or pass command line flags to the
    HotSpot JVM to get the desired behavior.</p>

  <h2>Analyzing Flight Recordings</h2>

  <p>Once you load up a flight recording in Java Mission Control, you
    can look at a number of different areas like code, memory, threads,
    locks and I/O and analyze various aspects of runtime behavior of
    your application.</p>
  <p>For example, for CPU-bound applications, the code view in the user
    interface will let you drill down into hotspots of your application,
    and discover optimization opportunities. If you’re tuning the GC, or
    tracking down memory allocation issues, then you can use the memory
    view to get a detailed view on individual GC events, allocation
    sites, GC pauses, etc. You can visualize the latency profile of your
    application by looking at I/O and Threads views, and even drill down
    into a view representing individual events in the recording.</p>

  <h2>Summary</h2>

  <p>Fortunately, you don’t have to wait for Java 8 to try out Java
    Mission Control and Java Flight Recorder. If you’re curious about
    the Eclipse RCP based application in the Oracle JDK, as I hope you’d
    be, check out the jmc application in the bin directory, and give it
    a spin.</p>

  <h2>Resources</h2>
  <ul>
    <li><a target="_blank" href="http://download.oracle.com/technology/products/missioncontrol/updatesites/base/5.5.0/eclipse/">Oracle
        Java Mission Control home page</a></li>
    <li><a target="_blank"
      href="http://www.youtube.com/watch?v=WMEpRUgp9Y4">Demo video</a></li>
    <li><a target="_blank"
      href="https://forums.oracle.com/thread/2579717">FAQ</a></li>
    <li><a target="_blank" href="http://hirt.se/blog/?cat=3">Marcus
        Hirt’s blog</a></li>
    <li><a target="_blank"
      href="https://blogs.oracle.com/henrik/entry/oracles_jvm_strategy">Oracle’s
        JVM Strategy</a></li>
    <li><a target="_blank"
      href="http://openjdk.java.net/projects/jdk8/features">Planned JDK
        8 Features</a></li>
  </ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/january/images/DaliborTopic75.jpg"
        alt="dalibor topic" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Dalibor Topic<br />
        <a target="_blank" href="http://www.oracle.com/index.html">Oracle</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://robilad.livejournal.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/robilad">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/+IanSkerrett">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
