<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <br>
  <img
    src="/community/eclipse_newsletter/2014/april/images/logo_side_transparent.png"
    width="200" alt="" />
  <br />
  <br />
  <h2>A vision for extensible tool chains for Embedded Systems</h2>
  <p>In 2011, even before starting PolarSys officially, the team
    designed the first version of our technology vision:</p>

  <img
    src="/community/eclipse_newsletter/2014/april/images/article1.1.png"
    width="600" alt="" />
  <br>

  <p>It is the fundamental vision behind PolarSys development and
    collaboration. What you should remember is that:</p>
  <ul>
    <li>PolarSys relies on Eclipse in terms of infrastructure and
      platform.</li>
    <li>PolarSys provides and catalogs technology bricks that provide
      single features like modeling support, documentation and code
      generation and model verification.</li>
    <li>PolarSys leverages these technology bricks into engineering
      tools that fully address one specific activity of Embedded System
      Engineering like Architecture Design, Development or Testing.</li>
    <li>Tools vendors, service providers or large organization (mainly
      their methods and tools teams), put together engineering tools and
      add their specific features to add extra value to the tool chain,
      or to adapt the tools to a specific company workflow and
      environment.</li>
  </ul>

  <p>In 2014, this vision becomes reality as major PolarSys projects are
    about to publish their 1.0 version and reach a new level of
    maturity.</p>
  <p>During last EclipseCon, we were doing a status about our
    technologies and designed the following image:</p>

  <img
    src="/community/eclipse_newsletter/2014/april/images/PolarSysTechnologyVision.png"
    width="600" alt="" />
  <br />

  <p>This diagram is not about the actual size of each component, but it
    puts forward the fact that the shared technologies between the
    different tools (the Eclipse platform, Eclipse modeling technologies
    and the shared components) represent a significant piece of software
    and millions of line of code that we leverage to build the tools.</p>
  <p>This diagram exactly shows the different kind of tools we are
    creating for the Polarsys engineering layer, among them:</p>
  <ul>
    <li>Capella for system architecture and design</li>
    <li>Papyrus for UML/SysML modeling</li>
    <li>CDT for realtime and embedded development</li>
  </ul>
  <p>The cool things is that with Luna, we are in the unique situation
    to deliver 1.0 version of two major kinds of Modeling projects:</p>
  <ul>
    <li><a href="https://www.eclipse.org/papyrus/">Papyrus</a>, the
      Eclipse project for UML and SysML reaches its long awaited 1.0
      version. The project provides advanced customization features on
      top of UML and SysML. The team is now working on better user
      experience, and more documentation that will be ready for Luna. (<a
      href="http://www.eclipse.org/community/eclipse_newsletter/2014/april/article3.php">See
        this specific article about Papyrus</a>.)</li>
    <li><a href="http://www.eclipse.org/sirius/">Sirius</a> reaches its
      1.0 version and makes it much easier and quicker than GMF to
      develop specific modeling tools (<a
      href="http://www.eclipse.org/community/eclipse_newsletter/2013/november/">see
        the Sirius newsletter</a>). This can be used to create tools
      that help visualize and analyze architecture constraints with
      various domain specific models.</li>
  </ul>
  <p>
    We also share more and more components like <a
      href="http://www.eclipse.org/acceleo/">Acceleo</a> for code
    generation, <a
      href="http://www.eclipse.org/proposals/modeling.gendoc/">Gendoc</a>
    for documentation generation, <a
      href="http://www.eclipse.org/modeling/mdt/?project=ocl">OCL</a>
    for model verification, <a
      href="http://www.eclipse.org/proposals/polarsys.reqcycle/">Reqcycle</a>
    for requirements engineering, <a
      href="http://projects.eclipse.org/proposals/uml-generators">UML
      Generators</a>, and the most interesting projects from the <a
      href="http://www.topcased.org/">Topcased</a> community finally
    migrated to PolarSys.
  </p>

  <h2>What's next</h2>
  <p>We have several upcoming projects that cover new System Engineering
    tasks and bring new innovations.</p>
  <ul>
    <li><a href="https://www.polarsys.org/projects/polarsys.pop">POP</a>
      offers a synchronous modeling infrastructure to verify or
      transform models or generate C or Java code from e.g. AADL,
      Geneauto Simulink models.</li>
    <li><a href="https://www.polarsys.org/projects/polarsys.chess">Chess</a>
      brings a method and a Papyrus customization for the development of
      high-integrity embedded systems in domains like satellite on board
      systems.</li>
    <li>On top of Sirius, Thales is open sourcing <a
      href="http://eclipse.org/proposals/polarsys.kitalpha/">KitAlpha</a>
      for the definition of multi-viewpoint oriented tools, and will
      open source Capella, the system engineering tool they use in
      several significant projects.
    </li>
  </ul>

  <h2>Join us and contribute!</h2>
  <p>Of course, it is still a work in progress, and some of you are
    impatient to get to use these tools. I would say that it is
    certainly the perfect time to step in the community.</p>
  <p>First, to challenge us with your expectations, and to submit bugs,
    request for documentation or to ask questions on the mailing list.</p>
  <p>
    Second, we are starting to work an idea we have since several
    months: our <a href="https://polarsys.org/integrated-demo">integrated
      demo</a>.
  </p>
  <p>The goal is to design, develop and build a nice and extensible
    rover system and to show how the PolarSys tools help design this
    system.</p>
  <p>The process will be iterative and fully open. We will collect new
    ideas, and we will regularly select several ideas to inject in the
    next iteration.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/april/images/gael2.jpg"
        alt="gael blondelle" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Gaël Blondelle<br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://polarsys.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/gblondelle">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/111816960159011824933/posts">Google
            +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
