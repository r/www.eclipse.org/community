<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <h2>Description</h2>
  <p>Using requirements is good practice for any project. Requirements
    exist all along the V-Model, from stakeholders to code to testing,
    with several intermediate levels such as system and software
    requirements. During the engineering process, designers need to
    trace those requirements with different artifacts: model elements,
    documents, code, tests, and so on. And different models are used for
    different languages.</p>
  <p>
    <a href="http://www.polarsys.org/projects/polarsys.reqcycle">ReqCycle</a>
    is an open source solution able to manage requirements and their
    traceability during industrial development. ReqCycle is included in
    the Eclipse <a href="http://www.polarsys.org/projects/polarsys">PolarSys
      project</a>.
  </p>
  <p>The objective of ReqCycle is to be integrated into an industrial
    environment. The "big picture" diagram below summarizes how the tool
    is designed to be used.</p>


  <img
    src="/community/eclipse_newsletter/2014/april/images/article2.1.png"
    width="600" alt="" />
  <br />
  <br />

  <h2>Main features</h2>
  <p>The main features of ReqCycle are divided into two parts:
    requirements management and traceability analysis.</p>
  <p>The requirements management capabilities of ReqCycle can rely on a
    data model definition. This data model allows the definition of
    business rules and constraints. To connect data to this data model,
    ReqCycle provides</p>
  <ul>
    <li>ReqIF import</li>
    <li>Document import feature (doc, docx, ods, odt) based on a future
      Kitalpha project (http://polarsys.org/projects/polarsys.kitalpha)
      named Doc2Model</li>
    <li>OSLC connection for DOORS, ClearQuest, etc.</li>
    <li>Model Import (SysML, etc.)</li>
  </ul>
  <p>The traceability core engine of ReqCycle allows the capture and the
    creation of traceability links. The traceability links can be
    located at multi-level, and can be captured from high level
    requirements to code traversing low level requirements and design.</p>

  <img
    src="/community/eclipse_newsletter/2014/april/images/article2.2.png" alt="" />
  <br />

  <p>
    <i>Figure 1 : Example of traceability analyzers</i>
  </p>

  <p>In the illustration below, the traceability is captured from design
    to high level requirements. An allocate relationship exists between
    one block and another one that implements a requirement.</p>

  <img
    src="/community/eclipse_newsletter/2014/april/images/article2.3.png"
    width="600" alt="" />
  <br />

  <p>
    <i>Figure 2 : Example of multi-level traceability captured</i>
  </p>

  <p>To enable team working, ReqCycle will also provide features to
    connect requirements to an external database or to plug into version
    control systems (such as SVN and GIT).</p>


  <h2>Roadmap</h2>
  <ul>
    <li>Migrate to Eclipse PolarSys working group
    <ul>
      <li>May 2014</li>
    </ul></li>
    <li>Finalize engineering support to get the complete cycle
    <ul>
      <li>EMF Traceability export for May 2014</li>
      <li>July 2014 for other features</li>
    </ul></li>
    <li>More connectors to improve demonstration and interest
    <ul>
      <li>Before end of year for document imports</li>
      <li>UI action language and industrialization (end of 2014)</li>
    </ul></li>
  </ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/april/images/tristan2.jpg"
        width="75" alt="Tristan Faure" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Tristan Faure<br />
        <a target="_blank" href="http://atos.net">Atos</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://eclipseatos.blogspot.fr/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/TristanFAURE">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <h6>Co-Authors</h6>
      <p>
        <strong>Stéphane Duprat, Atos<br>Raphaël Faudou, Samares
          Engineering
        </strong>
      </p>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
