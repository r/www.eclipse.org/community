<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <img src="/community/eclipse_newsletter/2014/april/images/papyrus.jpg"
    width="200" alt="" />
  <br>
  <h2>What is Papyrus?</h2>

  <p>
    This article introduces the <a
      href="https://www.eclipse.org/papyrus/">Papyrus</a> tool, an
    Eclipse-based visual editor for Unified Modeling Language (UML [1])
    modeling. This open source application has two principal objectives:
    first, it aims to implement the complete UML specification
    (currently version 2.5), enabling it to be used as the reference
    implementation of the Object Management Group (<a target="_blank"
      href="http://www.omg.org">OMG</a>) standard. Second, it intends to
    provide an open, robust, highly scalable, and highly customizable
    tool for defining Domain-Specific Languages (DSLs) and corresponding
    tools via the UML profile mechanism. It enables hence to benefit
    advantages of standard-based solutions in one hand and efficiency of
    domain-specific modeling solutions in the other hand. Finally,
    Papyrus is also intended to support large-scale industrial projects,
    as indicated by the growing list of industrial supporters.
    Consequently, it provides an efficient and effective alternative to
    custom and proprietary DSL tools, without losing the benefits of an
    international standard.
  </p>
  <p>Finally, Papyrus will also serve as an experimental platform for
    researchers who need to construct proof-of-concept prototypes. Built
    on top of Eclipse as an open source project, Papyrus is an ideal
    candidate for this purpose.</p>

  <h3>Background</h3>

  <p>As part of its Model-Driven Architecture (MDA) initiative, OMG, an
    international consortium representing numerous industrial and
    academic institutions, has provided a comprehensive series of
    standardized technology recommendations in support of model-based
    development of both software and systems in general. These
    recommendations cover core facilities such as meta-modeling, model
    transformations, general-purpose languages, and Domain-Specific
    Modeling Languages (DMSLs). A key component in the latter category
    is UML, which has emerged as the most widely used modeling language
    in both industry and academia.</p>
  <p>A number of tools supporting UML are available from a variety of
    sources. However, these are generally proprietary solutions whose
    capabilities and market availability are controlled by their
    respective vendors. This can be problematic for industrial users,
    who may require highly specific tool capabilities as well as
    long-term support that often extends beyond the point of commercial
    viability.</p>
  <p>Consequently, some industrial enterprises are seeking open source
    solutions for their UML tools. Researchers have a similar need,
    since proprietary products are often too constraining and inflexible
    to allow optimized implementation of new ideas and prototypes.</p>

  <h2>Papyrus 1.0</h2>
  <p>
    The Eclipse platform along with its modeling high-level project, and
    the <a href="http://polarsys.org/">PolarSys</a> Eclipse working
    group, are the environment of choice for developing open source
    tools for enabling and fostering model-driven engineering (MDE).
    Indeed, within this context, there are two projects dedicated to
    implement UML. First, there is the UML2 project providing an
    EMF-based implementation of the <a
      href="http://wiki.eclipse.org/MDT-UML2">UML2 meta-model</a>. This
    component has become the de facto standard implementation of the
    UML2 meta-model. Secondly, based on this UML2 component, Papyrus
    provides user-oriented tools to enable UML2 visual modeling within
    Eclipse. It includes all its usual graphical editors as defined in
    the standards, but also other editors enabling tabular-based,
    text-based, tree-based and form-based views.
  </p>
  <p>Papyrus became an Eclipse open source project in August 2008, and
    the first code was delivered in November 2008. The Eclipse Luna
    simultaneous release, scheduled for June 2014, will include Papyrus
    1.0. This release represents an important milestone for Papyrus. The
    project has reached true maturity and its product has developed into
    an industrial-strength tool. Release 1.0 will include new features
    and major advances in usability and robustness.</p>

  <a href="/community/eclipse_newsletter/2014/april/images/CustoDSL.png"
    target="_blank"><img
    src="/community/eclipse_newsletter/2014/april/images/CustoDSL.png"
    width="200" alt="" /></a>
  <a href="/community/eclipse_newsletter/2014/april/images/Image1.png"
    target="_blank"><img
    src="/community/eclipse_newsletter/2014/april/images/Image1.png"
    width="200" alt="" /></a>
  <a href="/community/eclipse_newsletter/2014/april/images/Image9.png"
    target="_blank"><img
    src="/community/eclipse_newsletter/2014/april/images/Image9.png"
    width="200" alt="" /></a>
  <br>
  <p>
    <small>Click on images to enlarge</small>
  </p>
  <br />
  <h3>Who’s Using Papyrus?</h3>
  <p>Papyrus has already been adopted by different industries, including
    Esterel Technologies:</p>
  <cite>"Esterel Technologies has chosen the Papyrus technology platform
    as a core component of its SCADE System model-based systems
    engineering product offering and is extremely pleased with its
    quality and efficiency as well as the common developments undertaken
    in partnership with and CEA LIST within our common LISTEREL
    Laboratory."</cite>
  <br />
  <p>- Eric Bantegnie, President and CEO, Esterel Technologies</p>

  <h3>Functionalities</h3>
  <p>Papyrus allows support for all diagram types defined in UML2. A
    Papyrus user can therefore build UML2 models that strictly conform
    to the standardized specification of the language. Papyrus can then
    be used to check against the UML specification. If something cannot
    be represented in Papyrus, this generally means that it is not
    allowed by the specification.</p>
  <p>Papyrus provides numerous functions in support of UML 2 modeling.
    The following list, while not complete, represents some of its
    advanced modeling features:</p>
  <ul>
    <li><b>A tools palette highly customizable.</b> Each diagram type
      has its own palette of tools allowing creation of UML elements in
      the diagram and configurable easily to fit user needs.</li>
    <li><b>A property view.</b> This allows the editing of any property
      of a UML element as well as any of its graphical properties. These
      properties are organized by categories for ease of use. There is
      also an "expert" category, allowing access to all the properties
      defined in the UML meta-model.
    <a href="/community/eclipse_newsletter/2014/april/images/Image6.png"
      target="_blank"><img
      src="/community/eclipse_newsletter/2014/april/images/Image6.png"
      width="200" alt="" /></a>
    <br>
    <p>
      <small>Click on image to enlarge</small>
    </p></li>
    <li><b>Model navigation.</b> Papyrus enables the addition of
      hyperlinks from one model element shown in a view to a model
      element shown in another view, to another view, or even to an
      external document (such as a pdf file or a web page). Hyperlinks
      may be created either within one model or between elements of
      different models.</li>
    <li><b>Contextual text editors enabling syntax highlight,
        completion, and content assist.</b> When a text-based model
      element (such as a property) is accessed, Papyrus helps by
      providing a list of possible values, such as a list of existing
      types or existing cardinalities. Papyrus also validates the text
      according to its grammar (if defined). This functionality is
      customizable; a user can provide an implementation (such as a
      specific text editor and validation rules) for specific needs.</li>
    <li><b>OCL constraint specification and checking.</b> OCL
      constraints can be specified for each UML element at the model
      level or at the meta-model level, such as in a profile definition.
      Furthermore, these constraints can be checked against the current
      model.</li>
    <li><b>Model import.</b> Papyrus can import models, profiles, or
      elements from other files. Imported elements can be used in the
      model and edited as for any other elements.</li>
  </ul>
  <p>To meet the primary goal of realizing the UML2 specification,
    Papyrus also provides extensive support for UML profiles. It
    includes all facilities for defining and applying UML profiles in a
    very rich and efficient manner. It also provides powerful tool
    customization capabilities similar to DSML-like meta-tools.</p>
  <p>The main intent here is to enable the profile plugin of Papyrus to
    dynamically drive its customization as defined by a profile. When
    applying a profile, Papyrus may adapt its visual rendering and GUI
    to fit the specific domain supported by that profile. If the profile
    is unapplied later, the tool resumes its previous configuration.</p>
  <p>When designing a UML2 profile, it is sometimes necessary to
    customize one or more existing UML2 diagram editors. So Papyrus
    supports customization of existing editors with the added capability
    of extending such customizations by adding new tools relevant to the
    stereotypes defined in the UML profile. For example, the SysML
    requirements diagram editor is designed as a customization of the
    classic UML2 class diagram editor, with additional features for
    direct manipulation of all the concepts defined in the SysML
    requirements diagram.</p>
  <p>Finally, when embedding a profile within an Eclipse plugin, a
    designer can also provide a specific properties view that simplifies
    the manipulation of the stereotypes and properties to make them more
    user-friendly. The outline editor and the tool menu can also be
    customized to fit the domain-specific concerns of the profile.</p>

  <h5>References</h5>
  <p>
    <small>1. OMG: UML Version v2.5, formal/07-02-05, <a target="_blank"
      href="http://www.omg.org/spec/UML/2.5/">http://www.omg.org/spec/UML/2.5/</a>.
    </small>
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/april/images/seb.jpg"
        width="75" alt="Sebastien Gerard" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Sébastien Gérard<br />
            <a target="_blank" href="http://www-list.cea.fr/en">CEA List</a>
          </p>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <h6>Co-Authors</h6>
      <p>
        <strong>CEA's Papyrus Team</strong><br>Ansgar Radermacher<br>Arnaud
        Cuccuru<br>Benoit Maggy<br>Camille Letavernier<br>Florian Noyrit<br>Juan
        Cadavid<br>Laurent Wouters<br>Patrick Tessier<br>Rémi
        Schnekenburger<br>Vincent Lorenzo
      </p>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
