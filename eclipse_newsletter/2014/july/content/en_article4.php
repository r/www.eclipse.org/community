<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    The Eclipse Foundation performs a great deal of work to ensure that
    the intellectual property embodied in our Project(s) has been well
    managed. We have full-time professional staff who works diligently
    to ensure that the code which ships from Eclipse can be included in
    proprietary Projects. The Eclipse <a
      href="http://eclipse.org/org/documents/Eclipse_IP_Policy.pdf">Intellectual
      Property Policy</a> provides the framework, and ultimately the <a
      href="http://eclipse.org/legal/EclipseLegalProcessPoster.pdf">Due
      Diligence Process</a> provides the guide for our community.
  </p>

  <h2>Origin of Code</h2>

  <p>Code originates from three sources at Eclipse:</p>
  <ol>
    <li>Eclipse Committers</li>
    <li>Community Contributions (typically in the form of bug fixes)</li>
    <li>Content developed and maintained somewhere other than Eclipse
      (other open source projects)</li>
  </ol>

  <p>Each is handled differently, with the greatest complexities from a
    due diligence standpoint arising in relation to content developed
    somewhere other than Eclipse.</p>

  <p>We have legal agreements in place with our Committers and an
    acknowledgement from their employers regarding their ability to
    contribute to Eclipse Projects. As trusted members of our community,
    Committers have write access to our repositories, and further due
    diligence of the code is not completed by the Foundation.</p>

  <p>We have Contribution License Agreements in place for all community
    contributors. It’s worth noting that the Eclipse Foundation CLA is
    quite a bit different from those in use in other organizations in
    that it simply says that the contributor agrees that their
    contributions will be provided under the license(s) for the project
    they are contributing to. The Eclipse Foundation does not acquire
    any ownership in the contribution, nor does it receive a broad
    license to the contribution which is the equivalent to ownership, as
    is the case in other organizations.</p>

  <br />
  <img
    src="/community/eclipse_newsletter/2014/july/images/cla_roxanne.png"
    alt="find cla" class="img-responsive" />

  <p>
    <i>Where to find the Contribution License Agreement</i>
  </p>
  <br />

  <p>All significant community code contributions are reviewed by our
    staff. The measure of significant was historically 250 lines of code
    but has recently been raised to 1000 lines of code based on our
    satisfaction with historic contributions below this level.</p>

  <p>The review of code originating from other open source communities
    is carefully reviewed at source level. It presents the added
    challenge that nesting is commonplace, with many open source
    projects including other open source projects, often many layers
    deep. In these cases, each open source project is segregated and
    looked at individually at the source level, at all layers of
    nesting. What may appear to be a single project may result in the
    review of 20 or more open source projects.</p>

  <h2>Licensing and Provenance</h2>

  <p>The code itself is reviewed with the help of tooling, issues
    related to licensing or the origin of the code identified, and in
    many cases, rectified before the code is distributed at Eclipse.
    Potential licensing issues vary, and may include such things as
    license incompatibilities, licenses with terms that are not
    considered commercially “friendly”, and licenses where the license
    provisions are inconsistent with the manner in which the Eclipse
    Project wishes to use the code.</p>

  <p>The code review will also examine the provenance of the code, and
    by that we mean “who wrote the code and how they agreed to the
    license”. Projects may manage the provenance of their code in a
    variety of ways. Some open source projects address it through their
    Terms of Use, others have a formal Contributor License Agreement in
    place, and others may individually confirm with their authors their
    ability to contribute the code under the project’s license. Open
    source projects with no management in this regard are not accepted
    for distribution at Eclipse.</p>

  <p>The review will also examine whether there has been a historic
    change in the license and if so, whether it was done with the
    permission of all of the original copyright holders. In our
    experience this is not always the case and remedial action may be
    required to use the code under the license the code is currently
    offered under.</p>

  <p>These practices support a vibrant commercial ecosystem surrounding
    Eclipse, one which we expect will continue to thrive in the years to
    come.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/Janet.jpg"
        alt="janet campbell" height="130" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Janet Campbell<br />
        <a href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <p>Director, Intellectual Property Management, Secretary and Legal
        Counsel</p>
      <ul class="author-link">
        <!--<li><a href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a> </li>
          <li><a href="https://twitter.com/JonasHelming">Twitter</a></li>
          $og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
