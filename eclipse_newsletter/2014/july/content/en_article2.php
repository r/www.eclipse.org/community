<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2 id="why-we-re-here">Why the Eclipse Foundation is Here?</h2>
    <p>
      We believe that open source and our <a target="_blank"
        href="https://www.eclipse.org/projects/dev_process/development_process.php#2_Principles">guiding
        principles</a> of openness, transparency and meritocracy provide
      an environment to win.
    </p>
    <p>To that end, we exist to foster a community for individuals and
      organizations who wish to collaborate on commercially-friendly
      open source software.</p>
    <p>
      The <a target="_blank" href="https://www.eclipse.org/org/#about">Eclipse
        Foundation</a> is not-for-profit, member supported corporation
      that hosts the Eclipse projects and numerous working groups. We
      work to help cultivate both an open source community and an
      ecosystem of complementary products and services.
    </p>
    <h2 id="ok-how-do-i-move-to-the-eclipse-foundation-">How Do I Move
      To The Eclipse Foundation?</h2>
    <p>Okay, great!</p>
    <p>
      A little bit of homework is needed first. We recommend that you
      read over the <a target="_blank"
        href="https://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
        Development Process (EDP)</a> and the <a
        href="https://www.eclipse.org/legal/">Legal Resources</a> pages.
      These will help you understand a projects responsibilities at the
      Eclipse Foundation.
    </p>
    <p>
      All projects start out with a proposal. We make this easy for you.
      Just fill in an <a target="_blank"
        href="https://wiki.eclipse.org/Development_Resources/HOWTO/Starting_A_New_Project#Proposal_Creation_Links">online
        form</a>, follow the instructions provided in each section and
      submit. The <a target="_blank"
        href="https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf#page=19">Eclipse
        Management Organization (EMO)</a> will review and email you some
      preliminary questions. These clarify and help us understand the
      proposals intent. We'll do a more in-depth review with further
      questions and suggestions. Our job is to help craft the proposal,
      in particular the scope so that it truly reflects the projects
      intentions. As you can expect, this is a collaborative effort with
      the project. Please be patient with this process, it can take a
      few emails and perhaps a conference call before we get it right.
    </p>
    <p>Here are a few tips that will help your proposal and project sail
      along through our processes:</p>
    <h3 id="proposal-tips">Proposal Tips</h3>
    <p>The best tip I can give, is to take the time to clearly and
      concisely explain the problem it addresses and the technology
      used. It always helps to imagine someone coming to your proposal
      who knows nothing about this problem area or technology.</p>
    <ul>
      <li>Remember that the background is just that, historical reasons
        and major inflection points that got the project to where it is
        today.</li>
      <li>The scope and description sections may seem similar but they
        are not. The scope is all about the bounds of the project; what
        is to be accomplished, produced and equally what is not going to
        be worked on or created. Add a line that states what's out of
        scope for the project. Think of the description as a
        non-technical pitch of the project, graphics do help. Make the
        first paragraph of the description an elevator style pitch; why
        would someone want use this or get involved in contributing
        code.</li>
      <li>Add project repository information. What type of repository do
        you want the project to use? Git (hosted on our infrastructure)
        or <a target="_blank"
        href="https://wiki.eclipse.org/Social_Coding/Hosting_a_Project_at_GitHub">GitHub</a>
        (gets mirrored back to our infrastructure)? Where's the initial
        code contribution coming from? If it's GitHub, provide the
        URL's.
      </li>
      <li>Fill in the future work section. What are some of the projects
        long term goals in both development and community building.</li>
      <li>Ensure you select a license type. Our projects are typically
        licensed under the <a target="_blank"
        href="https://www.eclipse.org/org/documents/epl-v10.php">Eclipse
          Public License (EPL)</a>. Each <a target="_blank"
        href="https://www.eclipse.org/org/workinggroups/">working group</a>
        may allow a different default license. In all cases, dual
        licensing a project will require Board approval. If you have
        questions, we have answers.
      </li>
      <li>Provide a project lead(s). These names can change over the
        course of a project but we need a name to allow project
        creation.</li>
      <li>Add committers now. When we create the project, the folks
        listed on the proposal will be initial committers. Adding anyone
        downstream will require an election to be run (this is pretty
        easy).</li>
      <li>Get everyone whose name is listed on the proposal to create
        Eclipse accounts. This is a big help to us and will help get
        your project created quicker.</li>
      <li>Expand and explain any acronyms. You understand the domain but
        many don't.</li>
      <li>Adding URL's to are a great way to help explain complex
        concepts and offer more in-depth information.</li>
      <li>Be sure to list out third party dependencies and their
        corresponding licenses under the Initial Contribution section.</li>
    </ul>
    <p>With the proposal finalized, we'll ask our Executive Director for
      approval to post for community review. Some additional questions
      may need to be answered before getting the approval. Community
      review lasts for at least two weeks. During this time we initiate
      a trademark search for the project name and look for project
      mentors. Once both of these conditions has been satisfied along
      with no pending legal issues, we will schedule the project for its
      creation review.</p>
    <p>Once we have a successful creation review, the project is now
      officially an Eclipse project. Welcome aboard and congratulations!</p>
    <p>Our Web and IP Teams work closely to provision the projects
      infrastructure (repositories, build servers, mailing lists etc.)
      and ensure that all the necessary legal documentation (committer
      paperwork etc) is in place.</p>
    <p>
      When the project has been provisioned (repositories created,
      download space granted etc.), the project assembles and submits
      its <a target="_blank"
        href="https://wiki.eclipse.org/Development_Resources/Initial_Contribution">initial
        code contribution</a>. The IP Team will review and work closely
      with you to during this stage. Please be patient during this
      process. IP management is time consuming but one of the
      Foundations strengths and a major benefit to your project and
      community.
    </p>
    <p>
      After you receive <a target="_blank"
        href="https://wiki.eclipse.org/Development_Resources/HOWTO/Parallel_IP_Process">Parallel
        IP</a> approval to check-in the code to your repository, the
      project is off to the races developing. You'll be bound by our <a
        target="_blank"
        href="https://www.eclipse.org/projects/dev_process/development_process.php">Eclipse
        Development Process (EDP)</a> and the <a
        href="http://www.eclipse.org/legal/EclipseLegalProcessPoster.pdf">IP
        Due Diligence</a> processes. You'll have to wait until the IP
      Team fully approves the initial code contribution and go through a
      <a target="_blank"
        href="https://wiki.eclipse.org/Development_Resources/HOWTO/Release_Cycle#Release_Review">release
        review</a> before you can let users download your projects code.
      While our development policies contribute some project
      administration and overhead, it ultimately benefits project
      adoption and the community around it.
    </p>

    <h2 id="i-have-questions">Questions?</h2>
    <p>
      We're sure you have questions. This is a quick overview of getting
      a <a target="_blank"
        href="https://wiki.eclipse.org/Development_Resources/HOWTO/Starting_A_New_Project">project
        started</a> at the Eclipse Foundation. There are many details
      that we didn't cover.
    </p>
    <p>We encourage you to review the links embedded in this article.</p>
    <p>Remember, we're here for you; the projects.</p>
    <p>
      Reach us at <a href="mailto:emo@eclipse.org">emo@eclipse.org</a>.
    </p>

 <div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/richard.jpg"
        width="75" alt="richard burcher" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Richard Burcher<br />
        <a href="http://www.eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a href="http://richardburcher.com/">Blog</a></li>
        <li><a href="https://twitter.com/richardburcher">Twitter</a></li>
        <!--$og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

