<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <h2 id="gerrit-overview">Gerrit Overview</h2>

    <p>Gerrit is a Web based code review system that is being deployed
      to projects at Eclipse. The service is available at
      https://git.eclipse.org/r/</p>

    <h2 id="creating-and-configuring-your-gerrit-account">Creating and
      configuring your Gerrit account</h2>

    <p>Gerrit does not know your account exists until you login for the
      first time. Simply login to Gerrit once if you have not already
      did to initialize your Gerrit account. You can login at
      https://git.eclipse.org/r/ using your eclipse.org credentials.</p>

    <p>For checking out repositories, Gerrit does not let you use the
      same password as your eclipse.org account password. Instead if you
      are using SSH authentication Gerrit requires you provide it with
      your SSH public key. If you are using HTTP authentication then
      Gerrit will generate a random password for you.</p>

    <p>SSH key based authentication is a bit more secure and easier as
      you do not need to save your password on your system. GitHub has a
      pretty decent documentation on how to Generate SSH key pair if you
      are interested in trying this method
      https://help.github.com/articles/generating-ssh-keys</p>

    <h3 id="upload-your-ssh-keys">Upload your SSH keys</h3>

    <p>If you would like to use SSH then you must upload your SSH public
      key to the Gerrit server.</p>

    <ol>
      <li>Login to Gerrit</li>
      <li>Click your name at the top right</li>
      <li>Click <strong>Settings &gt; SSH Public Keys</strong>
        (https://git.eclipse.org/r/#/settings/ssh-keys)
      </li>
      <li>Click <strong>Add Key ..</strong></li>
      <li>Paste the text of your public key into the text box</li>
      <li>Click <strong>Add</strong></li>
    </ol>

    <p>Your key may look something like this:</p>

    <pre>
      <code>ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDEELNeLbxkyFI3JfIC7sutF2NLJwizVDDljw6h2KB9dwUrVGUBQM7r9+4Ndp/ojJ+lEk8OuNh+Kicc0hwHLHz+v81ejN62yQe+c16fvard6MdkrA3xr1WuvNZDvBQhVUkNmEoYYa3C+GpvEmQssvrPhpU0RD6AELzBnrG+VME9Vb2ObvIHKj8OulxD96zk2GTRHM0KaR9XhLPsLQ7U0ML715BDA3k1Zf66DOmZiyzckZZD+YtiV3qnAfwW5hU9Xi+M92vqf5Z5mC7t6aX9Pu5TXb614NE1GKUZ6yDEWFLspo4ihl+X2pA2oMONjbgOG5gqlnBAArsG0WP6dVF+jKQ5 your_email@example.com
</code>
    </pre>

    <p>Note: The last part where it says your_email@example.com is
      actually just a comment and can be anything you want here.
      Typically I put my email address or some word to identify who’s
      key this is or where it came from in case you have multiple keys.</p>

    <h3 id="generate-http-password">Generate HTTP password</h3>

    <p>If using SSH key based authentication is not possible for you, or
      you would rather use a password. You will need to tell Gerrit to
      generate a random password for you before you can use password
      authentication.</p>

    <ol>
      <li>Login to Gerrit</li>
      <li>Click your name at the top right</li>
      <li>Click <strong>Settings &gt; HTTP Password</strong>
        (https://git.eclipse.org/r/#/settings/http-password)
      </li>
      <li>Click <strong>Generate</strong></li>
    </ol>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-http-password.png"
        alt="HTTP Password" title="HTTP Password" />
    </p>

    <p>This random password will be the password you use to work with
      Git via Gerrit URLs. If you are ever worried that your password is
      compromised simply clear your password and re-generate a new one.</p>

    <h2 id="understanding-eclipse-git-and-gerrit-urls">Understanding
      Eclipse Git and Gerrit URLs</h2>

    <p>At Eclipse we support 3 types of URLs git://, ssh://, and
      https://.</p>

    <p>You can find the Git URLs via cGit at https://git.eclipse.org/c/</p>

    <p>For Git the URLs are:</p>

    <ul>
      <li>git://git.eclipse.org/gitroot/project/repo.git</li>
      <li>ssh://git.eclipse.org/gitroot/project/repo.git</li>
      <li>http://git.eclipse.org/gitroot/project/repo.git</li>
    </ul>

    <p>You can find the Gerrit URLs via Gerrit at
      https://git.eclipse.org/r/</p>

    <p>For Gerrit the URLs are:</p>

    <ul>
      <li>git://git.eclipse.org/gitroot/project/repo</li>
      <li>ssh://git.eclipse.org:29418/project/repo</li>
      <li>https://git.eclipse.org/r/project/repo</li>
    </ul>

    <p>
      <strong>git:// url</strong>
    </p>

    <p>
      The first is a read-only URL via the git:// protocol. This URL is
      git://git.eclipse.org/gitroot/<strong>project/repo.git</strong>
      and is the same no matter if you copied it from cGit or Gerrit.
    </p>

    <ul>
      <li><strong>project</strong> is a directory containing all the
        repos for a specific project. For example the CBI project is
        under /gitroot/<strong>cbi</strong>/repo.git</li>
      <li><strong>repo.git</strong> is the specific git repository for a
        project, some projects may have more than 1 repo</li>
      <li>Tip: The final “.git” part of the URL is actually optional</li>
    </ul>

    <p>
      <strong>ssh:// urls</strong>
    </p>

    <p>Next the ssh:// protocol URLs are slightly different depending on
      if you are pulling from Git or Gerrit. If your project is Gerrit
      enabled you should prefer the Gerrit URL otherwise you won’t be
      able to push to the repository.</p>

    <p>The reason the URLs are different is because Gerrit provides it’s
      own SSH service on port 29418 and does not use the same SSH
      service that is on the default port on git.eclipse.org.</p>

    <p>Gerrit trims the “/gitroot” portion of the URL and adds a port
      “:29418” in it’s place. This is the only difference between the 2
      URLs.</p>

    <p>
      <strong>http:// urls</strong>
    </p>

    <p>Again the URLs are slightly different depending on if you are
      using Git or Gerrit. If your project is Gerrit enabled you should
      prefer the Gerrit URL otherwise you won’t be able to push to the
      repository.</p>

    <p>The reason the URL pattern is different here is because Gerrit
      hosts it’s own HTTP service for Git. The difference here is Gerrit
      only supports https (secure) and again Gerrit trims the “/gitroot”
      part of the URL and replaces it with “/r” which is the Gerrit web
      URL.</p>

    <h2 id="updating-a-existing-git-repo-to-use-gerrit-url">Updating a
      existing Git repo to use Gerrit URL</h2>

    <p>If you have already cloned a Git project at Eclipse you do not
      need to reclone, simply update your Git URLs to point to the new
      Gerrit URLs and you can start using Gerrit.</p>

    <p>
      <strong>Using Git CLI</strong>
    </p>

    <pre>
      <code>git remote set-url origin &lt;gerrit-url&gt;
</code>
    </pre>

    <p>
      <strong>Using EGit</strong>
    </p>

    <ol>
      <li>Navigate to the Git perspective (Window &gt; Open Perspective
        &gt; Other &gt; Git)</li>
      <li>Right click on the repo you wish to modify</li>
      <li>Click <strong>Properties</strong></li>
      <li>Under Configuration tab, look for remote &gt; origin &gt; url</li>
      <li>Enter the new URL</li>
      <li>Click <strong>Apply</strong></li>
      <li>Click <strong>Ok</strong></li>
    </ol>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-egit-url.png"
        alt="EGit Remote URL" title="Egit Remote URL" />
    </p>

    <h2 id="checkout-a-gerrit-project">Checkout a Gerrit Project</h2>

    <p>If you have not already checked out the repository you want to
      work on then you can clone the repo directly.</p>

    <p>
      <strong>Using Git CLI</strong>
    </p>

    <pre>
      <code>git clone &lt;gerrit-url&gt;
</code>
    </pre>

    <p>
      <strong>Using EGit</strong>
    </p>

    <ol>
      <li>Navigate to the Git perspective (Window &gt; Open Perspective
        &gt; Other &gt; Git)</li>
      <li>Click the Clone Repository button in the Git Repository view</li>
      <li>Enter the Gerrit URL into the URI link</li>
      <li>Enter your credentials if applicable (If you are using SSH
        you’ll have to configure your public/private keys)</li>
    </ol>

    <h2 id="push-to-gerrit">Push to Gerrit</h2>

    <p>Pushing to Gerrit is no different than pushing to a regular Git
      repo the only difference is you are pointing to a new URL so make
      sure your URL is configured currently. Simply push as you would
      with any other repository and your changes will go directly into
      the repo.</p>

    <p>I like to be as specific as possible so I always push a branch
      specifically.</p>

    <p>
      <strong>Using Git CLI</strong>
    </p>

    <pre>
      <code>git push origin master
</code>
    </pre>

    <p>Change master to the name of the branch you want to push to.</p>

    <p>
      <strong>Using EGit</strong>
    </p>

    <ol>
      <li>Navigate to the Git perspective (Window &gt; Open Perspective
        &gt; Other &gt; Git)</li>
      <li>Right click on the repo you are making changes to</li>
      <li>Click <strong>Push branch…</strong></li>
      <li>Ensure branch is <strong>master</strong> (or whatever branch
        you’re pushing)
      </li>
      <li>Click <strong>Next</strong> (Note this will immediately push
        your branch!)
      </li>
    </ol>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-egit-push.png"
        alt="EGit Push" title="EGit Push" />
    </p>

    <h2 id="push-to-gerrit-for-review">Push to Gerrit for Review</h2>

    <p>Pushing for Review is a feature of Gerrit which allows you to
      push a patch online for developers to review. This is the method
      you should use to push if you are not a committer on the project
      or if you’d like a second opinion on your patch before it goes
      live.</p>

    <p>
      Gerrit provides a special refspec to push your changes to which
      will instead of merging into the upstream repository commits it to
      be reviewed. This refspec is <strong>refs/for/*</strong> if where
      the * tells Gerrit which branch you want your patch to be reviewed
      against. For example if you’d like to push your patch to be
      reviewed against the <strong>master</strong> branch then you push
      to <strong>refs/for/master</strong>. Change master to be any other
      existing branch online for it to be reviewed against another
      branch.
    </p>

    <p>
      In the example below we are pushing a special Git keyword called <strong>HEAD</strong>.
      In this case this simply means the latest commit on the current
      branch you are on.
    </p>

    <p>
      <strong>Using Git CLI</strong>
    </p>

    <pre>
      <code>git push origin HEAD:refs/for/master
</code>
    </pre>

    <p>
      Change <strong>master</strong> to whichever branch you want to
      push to.
    </p>

    <p>
      <strong>Using EGit</strong>
    </p>

    <ol>
      <li>Navigate to the Git perspective (Window &gt; Open Perspective
        &gt; Other &gt; Git)</li>
      <li>Right click on the repo you are making changes to</li>
      <li>Click <strong>Remote &gt; Push…</strong></li>
      <li>Select <strong>Configure Remote Repository</strong> and choose
        <strong>origin</strong></li>
      <li>Click <strong>Next</strong></li>
      <li>Set <strong>Source ref</strong> to <strong>HEAD</strong></li>
      <li>Set <strong>Destination ref</strong> to <strong>refs/for/master</strong>
        (Change master to the branch you want the patch to be reviewed
        against)
      </li>
      <li>Click <strong>Add Spec</strong></li>
      <li>Click <strong>Finish</strong></li>
    </ol>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-egit-refspec.png"
        alt="EGit Refspec" title="EGit Refspec" />
    </p>

    <p>
      <strong>Note:</strong> After you push Gerrit will provide you with
      a link to your review. Note this down as it will be the link to
      your patch should you need it. If you are working on a Bug it is
      good practice to link to this URL in the Bug. The URL should look
      similar to this https://git.eclipse.org/r/24873/
    </p>

    <h2 id="updating-a-patch-for-review">Updating a patch for Review</h2>

    <p>When your code is reviewed you might have some feedback asking
      you to address some issues with your code. In this case you will
      need to update your patch with a new version that addresses
      feedback comments.</p>

    <p>
      The Gerrit way to update patches is to amend your existing patch
      commit continuously until it is accepted. When you amend your
      patch though you need to ensure that your commit message contains
      the correct <strong>Change-Id</strong> field. This field is how
      Gerrit knows that you are updating an existing Gerrit review. This
      is a unique string that helps Gerrit identify an existing Review
      and update it. You can find the <strong>Change-Id</strong> on the
      review page for your patch near the top you should see a box
      similar to this:
    </p>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-change-id.png"
        alt="Change-Id" title="Change-Id" />
    </p>

    <p>
      <strong>Using Git CLI</strong>
    </p>

    <pre>
      <code>git add &lt;files&gt;
git commit --amend
</code>
    </pre>

    <p>
      <strong>Using EGit</strong>
    </p>

    <ol>
      <li>Navigate to the Git perspective (Window &gt; Open Perspective
        &gt; Other &gt; Git)</li>
      <li>Right click on the repo you are making changes to</li>
      <li>Click <strong>Commit…</strong></li>
      <li>Select the files you modified</li>
      <li>Check the <strong>Amend Previous Commit</strong> button
      </li>
    </ol>

    <p>
      <img
        src="/community/eclipse_newsletter/2014/july/images/gerrit-egit-amend.png"
        alt="EGit Amend" title="Egit Amend" />
    </p>

    <p>
      <strong>Amended Commit message</strong>
    </p>

    <p>Your amended commit message should look similar to the following:</p>

    <pre>
      <code>Bug 420891 - [CBI] Builds too many projects

According discussions in Bug 420891 it seems CBI build is building too
many projects in JSDT. This patch removes the development directory from
the build list which does not need to be built.
Change-Id: I5bc9dfe47569e854b40d4fe9a400bb1dd16781d6
Signed-off-by: Thanh Ha &lt;thanh.ha@eclipse.org&gt;
</code>
    </pre>

    <p>
      Once amended you can push using the same method as in the previous
      section <strong>Push to Gerrit for Review</strong>.
    </p>

  <div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/thanh.jpg"
        width="75" alt="Thanh Ha" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Thanh Ha<br />
        <a target="_blank" href="http://www.eclipse.org/">Eclipse
          Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.zxiiro.ca/blog/">Blog</a></li>
        <li><a target="_blank" href="http://twitter.com/zxiiro">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/u/0/107854830702276814626/posts">Google
            +</a></li> <?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

