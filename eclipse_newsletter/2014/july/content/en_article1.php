<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>
      The Eclipse Foundation has had an activity <a target="_blank"
        href="http://dash.eclipse.org/dash/commits/web-app/">dashboard</a>
      for our open source projects for a long time. It has served us
      well, but it's time to move on.
    </p>
    <p>The existing “classic” dashboard—implemented as part of the Dash
      project—is entirely custom. It was build many years ago by Eclipse
      Foundation staff members with very specific requirements. It was
      originally built for CVS, but it built with a modular architecture
      that made it relatively easy to extend to support SVN and then
      Git. However, many of the original design decisions have made
      leveraging the enhanced metadata available through Git
      challenging.</p>
    <p>The CVS implementation was exclusively concerned with activity by
      official Eclipse Foundation-sanctioned committers. This was
      primarily due to a limitation imposed by CVS that only those
      individuals with write access (i.e. committers) could commit to
      the repository, and there was no consistent means of annotating
      the actual author of the code. It's still true with Git that only
      those individuals with write access can commit, but we do have an
      easy and consistent means of capturing the author.</p>
    <p>The transition from CVS to Git was painful as it became more and
      more difficult to know if a commit was authored by a committer or
      a contributor. It became clear that the distinction wasn’t
      interesting from a metrics point-of-view, and over time the
      classic dashboard has become less and less concerned with
      committer activity and more and more concerned with contribution
      activity (which includes committers).</p>
    <p>
      With the new dashboard, we're making a leveraging the hard work of
      others by leveraging the open source <a target="_blank"
        href="https://github.com/MetricsGrimoire/">MetricsGrimoire</a>
      project. MetricsGrimoire has built-in facilities to harvest
      information from most of our sources. It is very responsive and
      makes it very easy and painless to hop from one dataset to the
      next.
    </p>
    <p>
      Our implementation is currently in beta; it supports all three of
      our existing open source software forges: <a target="_blank"
        href="http://dashboard.eclipse.org/">Eclipse</a>, <a
        target="_blank"
        href="http://dashboard.eclipse.org/index.html?data_dir=data/locationtech/json">LocationTech</a>,
      and <a target="_blank"
        href="http://dashboard.eclipse.org/index.html?data_dir=data/polarsys/json">PolarSys</a>.
      It provides a holistic view of project activity by tracking source
      code, mailing list, Bugzilla, and Gerrit activity. At some point
      in the future, we intend to extend it to gather metrics from the
      forums. Like the classic dashboard, the new dashboard is able to
      map activity to member companies (where appropriate) based on the
      employment status of the individual contributors.
    </p>
    <p>We made a decision to focus source code metrics on Git
      exclusively, so it doesn’t gather any source code repository
      information for the Orbit project which still uses CVS (see Bug
      349048), or any project using Subversion (which we've announced
      we'll be retiring at the end of 2014).</p>
    <p>
      To make all this work, we leverage data from a couple of different
      sources. The first source of data is the <a target="_blank"
        href="https://wiki.eclipse.org/Project_Management_Infrastructure">Project
        Management Infrastructure</a> (PMI) from which we get the
      association between projects and corresponding resources (source
      code repositories, mailing lists, Gerrit, and Bugzilla). It's very
      important that project teams keep this information up-to-date to
      ensure that the information harvested and presented by the
      dashboard is accurate.
    </p>
    <p>Associations between individuals and activity is a bit of a
      moving target. Email addresses are used by Git to record the
      contributor information. Email addresses change over time and some
      people work with multiple email addresses. To consistently map
      commits to a particular individual, we keep track of multiple
      email addresses in Eclipse Foundation accounts. Individuals can
      provide this information themselves in their "My Account" page.
      Eclipse Foundation staffers can also provide this information when
      we notice that it's missing.</p>
    <p>The results are impressive. From the main page, you will find
      overview charts of all our resources. The "subprojects" link at
      the top of the page lets you drill down into top-level projects
      and then into individual subprojects.</p>
    <br /> <a target="_blank"
      href="/community/eclipse_newsletter/2014/july/images/eclipsedashboard.png"><img
      src="/community/eclipse_newsletter/2014/july/images/eclipsedashboard.png"
      alt="eclispe dashboard" class="img-responsive" /></a>

    <p>
      <i>The new dashboard focused on the <a target="_blank"
        href="https://www.google.com/url?q=https%3A%2F%2Fprojects.eclipse.org%2Fprojects%2Ftechnology.recommenders&sa=D&sntz=1&usg=AFQjCNG_zd_zR-aQ5TeKZ_K_OCZNpAKXZA">Code
          Recommenders</a> project.
      </i>
    </p>
    <br />
    <p>You can find other views (like contributions by member companies)
      via the menu in the top-left corner of the page. Click on the "All
      History" banner on the top right to open a view of the data
      restricted to the past five years (remember that this is in beta,
      we have some user interface issues to work out). You can focus in
      on a subset of any chart by dragging the mouse across that subset.
      You can click anywhere in a chart to reset to view the entire
      dataset.</p>
    <p>
      We hope that you find the new dashboard interesting and useful.
      We're using <a target="_blank"
        href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=405792">Bug
        405792</a> to track the effort; this bug is an umbrella bug,
      with most of the real work tracked by blocker bugs. If you’d like
      to bring something to the discussion, please focus your efforts
      there.
    </p>
    <p>Enjoy!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/Wayne.jpg"
        width="75" alt="wayne beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Wayne Beaton<br />
        <a href="http://www.eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a href="http://waynebeaton.wordpress.com/">Blog</a></li>
        <li><a href="https://twitter.com/waynebeaton">Twitter</a></li>
        <!--<li><a href="https://twitter.com/noopur2507">Google+</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

