<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <p dir="ltr">
    In <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/03/12/introducing-emf-forms/">my
      previous blog</a>, I introduced EMF Forms, a subcomponent of EMF
    Client Platform (ECP), which supports the development of form-based
    user interfaces based on a view model. The approach allows the
    effective development of forms without manual and tedious layout
    coding or manually binding controls to data models.
  </p>
  <p dir="ltr">
    The technological basis of EMF Forms has been used actively for more
    than a year in numerous user projects. In October, with release
    1.1.0 of ECP, EMF Forms (still without a name) was presented
    publicly to the community for the first time. Since October, we’ve
    been able to win over many new users, we’ve received a lot of
    feedback and, above all, we’ve continued to develop the software. In
    this post, I would like to take a close look at EMF Forms. In
    particular, I would like to share our experience and feedback from
    user projects and compare EMF Forms to manual UI programming. In
    this context, the first and perhaps most relevant questions about
    new technology is: does it save effort and how much does it cost? I
    start with a short introduction to EMF Forms. For more details,
    please refer to the website. Next, I compare the effort to set up an
    interface with and without EMF Forms, the effort to create an
    initial version of the UI and the effort to execute notable changes
    in the UI. If you already know about EMF Forms, you might want to
    continue reading <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/04/15/emf-forms-a-question-of-effort/#skip-intro">here</a>.
  </p>
  <h1>What is EMF Forms?</h1>
  <p dir="ltr">Many business applications are focused on the in- and
    output of data as well as on subsequent data processing. Examples of
    such data-centric applications can be found in almost all
    industries, such as in CRM or ERP systems. Regardless of the
    specific domain, the corresponding data is often presented in
    forms-based UIs. These forms show the contents of one or more
    entities of the application and its attributes.</p>
  <p dir="ltr">Displayed below is a screen shot of a simple example of a
    form-based UI. It shows a possible form for an entity “Person” with
    four attributes. Each attribute is identified by a label and a
    corresponding control (input field). In the example, the attributes
    are displayed in a simple two-column layout.</p>
  <p dir="ltr"
    id="docs-internal-guid-de456998-26ff-467b-9175-5c4542132ea4">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article2.1.png"><img
      class="alignnone size-full wp-image-19529"
      alt="image001 EMF Forms: A Question of Effort"
      src="/community/eclipse_newsletter/2014/may/images/article2.1.png"
      width="404" height="94" title="EMF Forms: A Question of Effort" /></a>
  </p>
  <p dir="ltr">
    <em>Figure 1: Simplified example of a form-based UI for the entity
      “Person” with four attributes</em>
  </p>
  <p dir="ltr">The implementation of this kind of user interface mainly
    includes the programming of individual controls such as text boxes,
    the binding of these controls to the data model and the creation of
    a layout, i.e., the placement of controls, labels and possible
    additional layout elements. Although the development of individual
    controls and their binding has been supported well with frameworks
    such as EMF or data binding, creating and customizing layouts is
    often a largely manual process. This means that all the visible
    elements, such as labels and input fields, are created manually in
    the source code, are bonded to the data model and are placed in the
    layout.</p>
  <p dir="ltr">EMF Forms is a radically different approach. Instead of
    describing a user interface in source code, the UI is expressed by a
    simple model. It specifies which elements, specifically which
    attributes of the data model, are displayed at which position in the
    UI. The actual user interface is then rendered based on this model
    by interpreting the model. First, the renderer translates the
    controls in the view model into actual implementations. A string
    attribute is displayed, for example, as a text field that is bound
    to that attribute. Next, the renderer translates the defined
    structure of the user interface in the view model into a specific
    layout. Figure 2 shows a very simplified example of a view model
    that would describe the UI used in the previous example (Figure 1).</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article2.2.png"><img
      class="alignnone size-full wp-image-19531"
      alt="image021 EMF Forms: A Question of Effort"
      src="/community/eclipse_newsletter/2014/may/images/article2.2.png"
      width="237" height="169" title="EMF Forms: A Question of Effort" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-de456998-2700-5a6c-983a-1e7dda89f64a">
    <em>Figure 2: A view model describes the form-based UI. This view
      model is interpreted by a renderer.</em>
  </p>
  <p dir="ltr">Details on the use of EMF Forms, the available view model
    elements and detailed tutorials can be found on the EMF Forms
    website. In this blog post, I will share our experiences from
    projects and feedback from users to compare the approach of EMF
    Forms with the traditional manual way of programming interfaces. Is
    EMF Forms’ approach really effective? Of course, there are two ways
    to create interfaces manually. In the first, manual code can be
    implemented in a particular UI toolkit. In the other, one can use a
    UI editor such as WindowBuilder. In general, the second version is
    of course more efficient but also has the limitation of not allowing
    use of custom controls or the reuse of interface elements. In the
    end, UI editors generate source code, a core difference to how EMF
    Forms works.</p>
  <p>
    <a> </a>
  </p>
  <h1>Faster?</h1>
  <p dir="ltr">The first interesting question when using EMF Forms, of
    course, is whether the approach is actually more efficient than
    manually developing interfaces, whether it actually supports the
    development of a forms-based user interface with less effort.</p>
  <p dir="ltr">The view model approach has at first an initial
    disadvantage: developers must invest time to evaluate the approach,
    to integrate it and to learn how to use it. Whether this effort is
    justified, of course, depends on the size of the developed UI, on
    the complexity and on the number of developers working on the forms.</p>
  <p dir="ltr">We were able to observe, however, that the manual
    programming of user interfaces in most projects is indeed perceived
    as unnecessarily time-consuming and even an annoying activity. The
    willingness to adopt a new approach is therefore very high for most
    developers. In projects in which EMF Forms is already being used for
    form-based user interfaces, the framework is used throughout the
    project, including very simple UIs such as for setting dialogs or
    wizards.</p>
  <p dir="ltr">EMF Forms and the view model are explicitly focused on
    the development of form-based interfaces, therefore it offers a
    significantly lower complexity level than a traditional UI toolkit.
    The explicit goal of view modeling is to provide better concepts for
    describing form-based user interfaces. EMF Forms offers, for
    example, an item “Control”, which allows developers to specify that
    a particular attribute from the data model (for example, “First
    Name”) shall be displayed in the user interface. A control is
    translated by a renderer into a label or a widget (for example, a
    text field). If such a control were to be implemented manually
    without EMF Forms, a label and a text box would have to be created
    manually.</p>
  <p dir="ltr">In EMF Forms, it is sufficient to specify that a
    particular attribute be displayed. This information is specified in
    the element “Control”. By placing the control within the structure
    of the view model, the layout is implicitly defined. The renderer is
    then responsible for the actual implementation of the UI. Therefore,
    significantly fewer inputs are required for the specification of an
    interface in EMF Forms, which is much easier than manual coding in
    both the initial creation as well as in changing forms. The
    following screenshot compares the two approaches and shows what
    would be necessary when creating a similar interface in SWT, with a
    view model on the left and source code on the right. Of course this
    is just an example and not statistical proof, but describing UIs in
    a view model is generally much more concise.</p>
  <p dir="ltr">To stay fair: On the side of the view model, the tree
    items shown contain additional information, which are not shown in
    the screenshot. In the example, however, the only additional
    information specified is which attribute of the data model to
    display in a certain control. This information can be entered
    efficiently via a selection dialog. Other attributes of the view
    model, such as whether a label for a control should be shown, are
    optional. In the example, the default values are used. When manually
    developing UIs, those kinds of default options must always be
    implemented. Furthermore, in the sample code shown in the
    screenshot, the created widgets are not bound to the data model,
    which would mean additional expense. In the case of EMF Forms, the
    renderer takes over this task. Controls are not only bound to the
    data model, they also provide additional functionality such as input
    validation, which would have to be implemented manually again.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article2.3.png"><img
      class="alignnone size-full wp-image-19532"
      alt="image03 EMF Forms: A Question of Effort"
      src="/community/eclipse_newsletter/2014/may/images/article2.3.png"
      width="628" height="523" title="EMF Forms: A Question of Effort" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-de456998-2701-08f6-9e8a-270fffcae18f">
    <em>Figure 3: Comparison between a user interface specified using
      the view model and the manual implementation in SWT</em>
  </p>
  <h1>The initial spark</h1>
  <p dir="ltr">When considering efficiency, an important criterion is
    the required effort to create an initial interface that displays all
    attributes of an entity in a simple layout. Such first versions of
    forms are particularly helpful for newly defined data entities, for
    example, to check the data model to see if it is complete. For this
    use case, the exact layout is often not important yet. The final
    specification of a user interface for the entities is sometimes
    developed too early, while the data model is still subject to
    changes.</p>
  <p dir="ltr">When manually developing UIs, UI editors or even UI
    mock-up tools can be helpful and allow faster results than manual
    programming. However, the created UIs are not functional; they are
    not bound to the data model. Using the model-based approach of EMF
    Forms, user interfaces can also be generated from scratch. In this
    case, the data model is read and the framework creates a view model
    on the fly that displays all attributes in a list. This approach is
    used by default for all entities from the data model for which no
    explicit view model has been defined yet. Figure 4 shows an example
    of the generation of a view model from the model data entity user.
    The generation of a default view model can also be customized, e.g.,
    the default could be a two-column layout. The default view models
    provide a good starting point to adjusting a user interface step by
    step, which is the typical process in an agile project. In the
    following section, we describe the experience with EMF Forms when
    changes or additions are applied on an existing user interface.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article2.4.png"><img
      class="alignnone size-full wp-image-19530"
      alt="image011 EMF Forms: A Question of Effort"
      src="/community/eclipse_newsletter/2014/may/images/article2.4.png"
      width="585" height="247" title="EMF Forms: A Question of Effort" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-de456998-2702-0648-d8c6-79afba78c953">
    <em>Figure 4: EMF Forms allows the initial generation of a default
      view model from a data model</em>
  </p>
  <h1>Agile?</h1>
  <p dir="ltr">When it comes to the development costs of software, the
    initial cost is typically only half the truth. Equally interesting
    are the costs when changes occur. Particularly in agile development,
    changes are an accepted and integral part of the development
    process. Therefore, a crucial criterion for a framework like EMF
    Forms is how well the approach supports changing existing form-based
    user interfaces, either because the layout needs to be adjusted or
    because there are changes in the underlying data model. To apply
    changes to an existing user interface, whether in a view model or in
    manual code, it is first necessary to identify the correct location
    for the corresponding adjustment. Manually written layout code can
    be quite difficult to read; the structure of the code often differs
    significantly from that of the structure of the user interface. In
    Figure 2, an example of a two-column layout in SWT, the controls are
    created line-by-line even though the structure of the interface is
    column-based. The view model, as a specialized concept for
    form-based UIs, follows the logic of the user interface more closely
    and is therefore often easier to read and understand.</p>
  <p dir="ltr">There are two different ways to make the actual change in
    an interface, which makes quite a difference for EMF Forms. The
    first case is a modification that can be done in the view model. An
    example of such a change would be moving an attribute inside a form,
    adding a structural element (for example, a new column) or adding
    new controls.</p>
  <p dir="ltr">Adding a new attribute is the same as initially
    generating the view model; there is a simple wizard. It&#8217;s even
    easier to change the position of an existing element, be it a
    control, a group of controls or a whole element of the structure. It
    can be moved simply by drag and drop in the tree view of the view
    model.</p>
  <p dir="ltr">In these two examples, the model-based approach fully
    shows its strengths &#8211; even highly structured manually written
    UI code is rarely as simple and understandable as a corresponding
    view model.</p>
  <p dir="ltr">More interesting is the second case in which a change
    isn’t made directly in the view model but affects the renderer. An
    example of such a change would be increasing the margins of a
    rendered control. Many of these settings can be specified in a
    so-called template model in EMF Forms. Specifying these settings
    would then affect the layout of the entire application and thus
    result in a homogeneous look-and -feel.</p>
  <p dir="ltr">If a general setting is not (yet) supported, the renderer
    shipped with EMF Forms can be extended or even replaced with a
    custom renderer. A typical example of such an adaptation would be
    adding special controls such as an input field for email addresses.
    For this purpose, manual UI programming is, of course, still
    necessary. However, each missing concept has to be implemented only
    once and can be combined with any existing concept. The additional
    expense thus refers only to proprietary, custom components. With
    manual UI programming, these would need to be developed in any case.</p>
  <p dir="ltr">There will always be parts of a form-based UI that are
    difficult to express in a view model without resulting in a similar
    complexity to manual UI programming. In these cases, EMF Forms
    pragmatically concedes and allows embedding of so-called custom
    areas in a form. This way, very specific parts of the UI can be
    programmed manually just as before. Of course, it is these types of
    UI element that should be avoided if possible. In practice, they can
    often be replaced in the medium term by adapting generic concepts.</p>
  <h1>Conclusion</h1>
  <p dir="ltr">
    This blog post compared the efficiency in programming form-based
    user interfaces using a model-based approach, such as EMF Forms, on
    the one hand with manual UI programming on the other hand. It is not
    surprising that the first is seen to be better overall. EMF Forms
    was designed for exactly this purpose &#8211; form-based user
    interfaces &#8211; while UI toolkits have to support any type of
    user interface. Last but not least, I am of course very involved
    with EMF Forms, so this post cannot be considered an objective
    comparison. However, we are interested in feedback, even negative.
    Open source technologies are continually developed and improved only
    through feedback, especially regarding things that do not work as
    desired or use cases in which the framework has not been used
    before. Of course we are happy for positive feedback, too! For more
    information on EMF Forms, please visit the <a target="_blank"
      href="http://eclipse.org/ecp/emfforms/index.html">EMF project
      website</a>.
  </p>
  <p>
    If you interested in an evaluation of EMF Forms, if you need support
    with embedding EMF Forms into your application, or you want to adapt
    the framework to your needs, we provide <a target="_blank"
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> for these purposes.
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/jhelming.jpg"
        alt="jonas helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jonas Helming <br />
        <a href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>
        <li><a href="https://twitter.com/JonasHelming">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
