<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    The Eclipse Graphical Modeling Framework (<a target="_blank"
      href="https://wiki.eclipse.org/Graphical_Modeling_Framework">GMF</a>)
    provides a generative component and runtime infrastructure for
    developing graphical editors based on <a target="_blank"
      href="http://www.eclipse.org/modeling/emf/">EMF</a> and <a
      target="_blank" href="http://www.eclipse.org/gef/">GEF</a>. The <a
      target="_blank" href="http://www.bonitasoft.com/">Bonita BPM
      Studio</a> provides a powerful and user-friendly interface by
    leveraging <a target="_blank"
      href="http://fr.slideshare.net/AurelienPupier/tools-and-methodologies-to-leverage-gmf-power-eclipseday-delft-2012">GMF
      customization power</a>.
  </p>
  <p>
    Bonita BPM Studio is the design/modeling component of a complete
    open source BPM Solution. This Eclipse RCP application is the winner
    of an Eclipse Community Award in 2011, in the "<a target="_blank"
      href="http://www.eclipse.org/org/foundation/eclipseawards/winners11.php">Best
      Modeling Application</a>" category. The author of this article,
    Aurélien Pupier, is one of the lead developers at Bonitasoft and has
    been an Eclipse Committer since 2010.
  </p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article4.1.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article4.1.png"
      width="600" alt="" /></a>
  </p>
  <p>The core of GMF (GMF-Runtime) is stable. The project is evolving
    mainly to follow the Release Train and to ensure compatibility with
    new Eclipse releases.</p>
  <p>What does this mean? It means that release after release, every
    GMF-based product benefits from high stability and
    backward-compatibility. It allows the Bonitasoft developer team to
    upgrade easily from one version to another.</p>
  <p>Relying on the compatibility layer, GMF works nicely with Eclipse
    4.x version. The last version of Bonita BPM is based on Eclipse
    4.3.1, and we plan to move to 4.4 for the next one.</p>
  <p>Although the core of GMF does not offer big shiny new features, the
    ecosystem around GMF is evolving and it is a good sign for the
    future health of the project. The following features are worth
    mentioning:</p>
  <ul>
    <li><a target="_blank"
      href="https://wiki.eclipse.org/EMF_Compare/ReleaseReview/Kepler#Graphical_comparison">Graphical
        comparison</a> within the scope of the EMF Compare project</li>
    <li>Task-focused UI using <a target="_blank"
      href="https://wiki.eclipse.org/Mylyn/Context/Modeling_Bridge">Mylyn
        bridge</a></li>
    <li>Different kind of integration with <a target="_blank"
      href="http://www.eclipse.org/Xtext/">Xtext</a>, such as an <a
      target="_blank"
      href="http://muelder.blogspot.fr/2011/04/xtext-celleditor-integration.html">Xtext
        Cell Editor</a> or a sample to use Xtext Resource for graphical
      rendering using GMF
    </li>
  </ul>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article4.2.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article4.2.png" alt="" /></a>
  </p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article4.3.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article4.3.png" alt="" /></a>
  </p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article4.4.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article4.4.png"
      width="600" alt="" /></a>
  </p>
  <p>Tools to facilitate building applications that rely on GMF-Runtime
    continue to evolve:</p>
  <ul>
    <li><a target="_blank"
      href="http://www.eclipse.org/modeling/gmp/?project=gmf-tooling">GMF-Tooling</a>
      is the historical tooling project. Among other things, they
      followed the "eat your own dog food" principle by integrating a
      new graphical editor to design your own GMF diagrams.</li>
    <li>The freshly open-sourced <a target="_blank"
      href="http://www.eclipse.org/sirius/">Sirius</a> project is
      integrated in the release train this year. It provides a new
      interpretative approach and comes with a Viewpoint feature.
    </li>
    <li>Eugenia is still maintaining their tooling for new Eclipse
      releases.</li>
    <li>On the Commercial tools side, we can mention <a target="_blank"
      href="http://www.obeodesigner.com/">Obeo Designer</a> which offers
      Collaborative Modeling.
    </li>
  </ul>
  <p>We're looking forward to the next set of changes coming with
    Eclipse Luna!</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/aurelien100.jpg"
        width="90" alt="Aurélien Pupier" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Aurélien Pupier<br />
        <a target="_blank" href="http://www.bonitasoft.com/">Bonitasoft</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://www.bonitasoft.com/for-you-to-read/blog">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/apupier">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
