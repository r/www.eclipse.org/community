<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>Consider the tasks needed each time you set up a fresh development
    environment to work with a particular version of a specific project:</p>
  <ul>
    <li>Install or update an Eclipse product with appropriate tooling.
      <ul>
        <li>Which tools need to be available for editing, compiling,
          debugging, and testing the project's sources?</li>
      </ul>

    <li>Materialize the appropriate bundles and features in the
      workspace.
      <ul>
        <li>What needs to be imported into the workspace and from which
          source code repositories?</li>
        <li>How are those bundles and features organized into meaningful
          working sets?</li>
      </ul>

    <li>Materialize the appropriate target platform.
    <ul>
      <li>What needs to be in the target platform to compile the bundles
        and features, as well as to launch the associated tests?</li>
    </ul></li>
    <li>Manage a clean, warning-free workspace.
    <ul>
      <li>How do you avoid workspace-scoped preferences which must be
        maintained independently of the bundles and features?</li>
      <li>How do you effectively manage consistent project-scoped
        preferences across a multitude of bundles and features?</li>
    </ul></li>
    <li>Install your personal tools.
    <ul>
      <li>Which of your favorite tools can't you live without?</li>
    </ul></li>
    <li>Manage your personal preferences.
    <ul>
      <li>How do you enable things such as key bindings and color
        schemes that make Eclipse comfortable for you?</li>
    </ul></li>
  </ul>

  <p>These tasks need to be well documented and evolve from release to
    release for each and every project.</p>
  <ul>
    <li>Who writes and maintains the instructions for these tasks?</li>
    <li>How many people need to follow these instructions and how often?</li>
    <li>How do your contributors find the instructions for your project?</li>
    <li>Why is anyone doing this manually?</li>
  </ul>
  <p>With Oomph you can manage all this more effectively by formalizing
    the setup instructions so they can be performed automatically with
    the click of a button. In this article we'll focus on the user's
    experience as opposed to how authors create setup descriptions for
    their projects. For the end users, Oomph provides a wizard to drive
    all aspects of the provisioning process. For provisioning new
    installations from scratch, Oomph provides an RCP application to
    launch the installer wizard; it's downloaded and unzipped just as
    for any Eclipse Package download. Launching that results in the
    following:</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.1.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.1.png"
      width="600" alt="" /></a>
  </p>
  <p>The initial page allows the user to choose a product to install from
  one or more product catalogs. The Eclipse product catalog is generated
  from the Eclipse Packaging Project's p2 repositories, so it allows
  users to install any version of any product they'd normally find on
  Eclipse's main download page. Oomph's underlying infrastructure
  supports bundle pooling for all aspects of the installation (and,
  optionally, even of the target platform), i.e., when installing
  multiple products using Oomph or when provisioning multiple target
  platforms, the installations and target platforms can share all the
  common bundles and will download each bundle only once. This
  dramatically reduces disk space as well as speeding up installation
  and target platform provisioning time. Of course one can disable
  bundle pooling to produce an installation exactly like you get with an
  unzipped package download. You can also see there is a dialog to
  manage the bundle pools.
  </p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.2.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.2.png" alt="" /></a>
  </p>

  <p>This allows one to manage the pools and agents, do garbage
    collection of unused bundles, and even repair damaged bundles by
    downloading the same version of that bundle from the internet. With
    Oomph the user has flexible control.</p>

  <p>Note that for those familiar with the early prototype version of
    Oomph, the current architecture is dramatically more flexible and
    less invasive. You'll notice that on the next page of the wizard.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.3.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.3.png"
      width="600" alt="" /></a>
  </p>

  <p>On this page you can select projects that have provided Oomph setup
    descriptions for provisioning a workspace to work with the source
    code for that project. But this aspect is optional, i.e., you can
    use Oomph only to install a product, or to install a product as well
    as provision a workspace for that product to use. If you double
    click on the Oomph project, you'll see it's added to the bottom of
    the dialog, where you can choose which stream of the project you'd
    like to have in the workspace. Of course Oomph only has a master
    stream at this point. Oomph is designed with composition in mind,
    i.e., it's possible to provision multiple projects into one
    workspace. This is definitely a technical challenge for which Oomph
    provides a more flexible, high-performance target platform
    implementation. Proceeding to the next page, the user is prompted
    for information needed to complete the product installation and the
    project provisioning.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.4.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.4.png"
      width="600" alt="" /></a>
  </p>

  <p>Most of the choices made are recorded, so the very first time you
    install something, you'll have quite a few choices to make, but on
    subsequent reuse, that's dramatically reduced. Proceeding to the
    confirmation page, you'll see the follow:</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.5.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.5.png"
      width="600" alt="" /></a>
  </p>

  <p>These are the tasks that will be performed to produce the
    installation and to preconfigure the workspace for the projects
    being provisioned. Tasks can be disabled, if desired, but during
    this initial installation most are absolutely required. Proceeding
    beyond this page will kick off the installation process.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.6.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.6.png"
      width="600" alt="" /></a>
  </p>

  <p>The first time users install a product, this process can take quite
    long because a large number of bundles need to be downloaded from
    the internet, but subsequent installations can take as little as 20
    seconds. When the process completes, the installed product will be
    launched and any remaining aspects of the installation process are
    performed in the launched product. The running product will
    automatically bring up the installer wizard to show progress on the
    remaining tasks.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.7.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.7.png"
      width="600" alt="" /></a>
  </p>

  <p>In this article, the Oomph project itself was selected for
    provisioning, so the tasks include cloning the Git repository,
    provisioning the target platform needed by the Oomph source
    projects, and defining working sets to organize those projects. Upon
    completion, the resulting workspace looks as follows:</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.8.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.8.png"
      width="600" alt="" /></a>
  </p>

  <p>Note that because Oomph's artifacts require tools such as EMF's
    generator and Ecore Tool's graphical editor, those tools have
    already been installed along with the product.</p>

  <p>That's it, the users can now work on the Oomph project source the
    same way as an Oomph committer, and can commit their changes to
    Gerrit for review.</p>

  <p>Another exciting aspect of Oomph is its support for managing, among
    other things, personal preferences. The editor for user tasks can be
    opened via the toolbar and then the Eclipse preferences dialog can
    be opened via the editor's toolbar contribution:</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article3.9.png"><img
      class="alignnone size-full wp-image-19529"
      src="/community/eclipse_newsletter/2014/may/images/article3.9.png" alt="" /></a>
  </p>

  <p>All preferences changed will be recorded as tasks, and those tasks
    will automatically be performed in each Oomph-installed product. So
    now users can decide which workspace-specific or
    installation-specific preferences they want universally the same in
    all their installations. In addition, any type of task can be
    authored in the user tasks editor, i.e., additional p2 tasks for
    installing a user's personal favorite tools into each and every
    product.</p>

  <p>Revisiting the initial question of "the tasks needed each time you
    set up a fresh development environment to work with a particular
    version of a specific project", with Oomph that boils down to
    launching the wizard, choosing what you want and where you want it
    to be, and letting Oomph do all the rest. The expensive parts of the
    installation and provisioning process are generally those that
    involved internet access, i.e., downloading bundles and cloning
    repositories, so even if those take quite long, they can run
    unattended, while you focus on real work.</p>

  <p>
    For more information you can read the <a target="_blank"
      href="http://projects.eclipse.org/proposals/oomph">project
      proposal</a> and if you want to try it, you can follow the
    instructions in the <a target="_blank" href="http://wiki.eclipse.org/Eclipse_Oomph_Installer">wiki</a>.
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/Eike_Stepper2.jpg"
        width="75" alt="Eike Stepper" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Eike Stepper<br />
        <a target="_blank" href="http://www.esc-net.de/">ES-Computersysteme</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://thegordian.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/eikestepper">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
