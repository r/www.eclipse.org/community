<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p dir="ltr"
    id="docs-internal-guid-117fd027-05d3-e08a-8793-5dae736cf233">
    This tutorial will give a short introduction on EMF Forms and
    describe how to get started. If you already know EMF Forms, you can
    browse directly to<a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/emf-client-platform-how-to-customize-the-editor-layout/#tutorial">
      the getting started part</a>. This tutorial is based on ECP
    version 1.3.x, please find the 1.2.x version <a target="_blank"
      href="http://eclipsesource.com/blogs/getting-started-with-emf-forms-1-2-x">here</a>.
  </p>
  <h2>Introduction</h2>
  <p dir="ltr"
    id="docs-internal-guid-3d0950c2-bf48-ac68-6d28-381f7b2b5ce7">This
    tutorial describes a framework called EMF Forms. The purpose of the
    framework is to ease the development and layout definition of
    form-based UIs that allow browsing, creation and data entry. EMF
    Forms also includes typical features of a form-based UI such as
    validation and rule-based visibilities of certain attributes.</p>
  <p dir="ltr">EMF Forms has already been applied in several industrial
    projects. Until now, it has always been hidden in the container
    project EMF Client Platform. However, EMF Forms is independent of
    EMF Client Platform, it just integrates well into the frame of ECP.
    For ECP Release 1.2, we therefore decided to give the component an
    explicit name: EMF Forms.</p>

  <h2 dir="ltr"
    id="docs-internal-guid-22903857-bf49-52d9-d0ff-c31bf19a2379">So what
    is the idea behind it?</h2>
  <p dir="ltr">Many business applications are data-driven and allow the
    user to browse, view and modify entities of an underlying data
    model. The entities are presented mostly in form-based layouts,
    whether as a desktop application, as a web application or as a
    mobile application.</p>
  <p dir="ltr">
    <b><b> <a
        href="/community/eclipse_newsletter/2014/may/images/article1.1.png"><img
          class="aligncenter size-medium wp-image-18558"
          alt="allofthem 300x180 Getting started with EMF Forms"
          src="/community/eclipse_newsletter/2014/may/images/article1.1.png"
          width="300" height="180"
          title="Getting started with EMF Forms" /></a><br />
    </b></b>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-22903857-bf4a-cf96-3025-a3017b3bfb0d">Manual
    development of user interfaces for business application &#8212; even
    when using a visual editor &#8212; has several drawbacks. Visible
    components such as forms or reports are typically subject to
    constant change based on user feedback or changes in the data model.
    All changes must be done manually in the UI code. Maintenance and
    testing of manually coded form layouts is costly, time-consuming and
    error-prone. Additionally, there are often many forms developed in
    parallel that all need to comply with a uniform look and feel, and
    manual UI coding cannot really enforce UI guidelines. And in the
    end, few developers really enjoy coding UIs manually.</p>
  <p dir="ltr">The main problem is that typical UI layout technologies,
    such as SWT, JavaFX or Swing, are powerful but complex to use since
    they have to support any kind of possible layout. They are not
    really focused on the use case of form-based business UIs.</p>
  <p dir="ltr">BUT, since the release of ECP 1.2.0, EMF Forms provides a
    new way of developing form-based UIs. Instead of manually coding
    form-based layouts, it allows you to describe the UI with a simple
    model instead of with code. The model defines only concepts, which
    are allowed by the given user interface guideline.</p>
  <p>The goals of the approach are to:</p>
  <ul id="docs-internal-guid-22903857-bf4a-4452-1a95-9c8323a51af1">
    <li dir="ltr">
      <p dir="ltr">more efficiently produce form-based UIs</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">adapt form-based UIs to changes in data models with
        minimum effort</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">maintain or easily change a uniform look and feel</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">enable a much shorter turnaround with other
        stakeholders (e.g., product manager, customer)</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">lower the technical entry barrier to create
        form-based UIs</p>
    </li>
  </ul>
  <p dir="ltr">A modeled UI part is interpreted by a rendering engine,
    which depends on a certain UI Toolkit and translates the model into
    concrete concepts such as a GridLayout. EMF Forms thereby even
    abstracts the custom layout of the user interface from the applied
    UI technology and allows you to switch between the UI technology
    stack to Swing, SWT, JavaFX or Web – just by replacing the renderer.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.2.png"><img
      class="size-full wp-image-18769 aligncenter"
      alt="image00 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.2.png"
      width="617" height="441" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-22903857-bf4b-76da-ba7e-be9ad413c411">The UIs
    produced by EMF Forms can be integrated like a manually developed
    UI. An obvious use case in an RCP application is the integration
    into a view, but even more projects use it for forms on websites,
    for wizards, preference pages or any other place where you display
    and modify data in an application.</p>
  <p dir="ltr">Since EMF Forms focuses on form-based UIs, it is also a
    perfect match with the concept of the application model in e4. While
    the e4 application model describes the general design of an
    application down to elements such as a view, EMF Forms describes the
    content of these elements, i.e., what’s displayed within an Eclipse
    4 view. Since the 1.2 release, EMF Forms is also fully compatible
    with Eclipse 4 native applications.</p>
  <p dir="ltr">So now you need to decide:</p>
  <p>
    Do you still want to manually code UIs or do you want to have a look
    at our <a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/emf-client-platform-how-to-customize-the-editor-layout/#tutorial%20">getting
      started tutorial</a>?
  </p>

  <h2 id="tutorial">Tutorial</h2>
  <p>
    For this tutorial, we will customite the layout of the editor of the
    demo application of the EMF Client Platform, which is based on EMF
    Forms. Note that you can <a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/emf-client-platform-how-to-customize-the-editor-layout/#embbededitor">embbed
      the form-based UI</a> anywhere in your application, as a view, as
    part of a view or even in dialogs. The editor provided by the demo
    application is just a simple way to try out the features of EMF
    Forms. Please also note that by default, EMF Forms reflectively
    processes all attributes of an object, so no manual coding or code
    generation is needed. It is also possible to add custom controls to
    EMF Forms (<a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/tutorialshow-to-customize-the-emf-client-platform/#customizecontrols">see
      here</a>) if you only want to customize the used controls.
  </p>
  <p dir="ltr">
    For this tutorial, we will use an example model called “Make it
    happen!”. If you want to learn more about the example model, please
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2013/06/13/emf-client-platform-make-it-happen-day-0-and-1/">see
      here</a>. The example models a simple task management system
    including the entities “User”, “UserGroup” and“Task”. This tutorial
    describes how to create a simple view model for the entity Task. The
    example model as well as the created view model can be retrieved
    from GitHub:
  </p>
  <p dir="ltr">
    <a target="_blank"
      href="https://github.com/eclipsesource/makeithappen/tree/master/core">http://github.com/eclipsesource/makeithappen/</a>
    (only projects in the sub-folder &#8220;core&#8221;)
  </p>
  <p dir="ltr">The repository contains three example bundles:</p>
  <ul>
    <li dir="ltr">
      <p dir="ltr">com.eclipsesource.makeithappen.model: The example
        model</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">com.eclipsesource.makeithappen.model.edit: The
        generated edit bundle</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">com.eclipsesource.makeithappen.ui: Contains the view
        model</p>
    </li>
  </ul>
  <p dir="ltr">
    However, you can of course also try this with your own model.
    Further, the tutorial is based on the demo application provided by
    the EMF Client Platform, including the model explorer. If you only
    want to use the created form-based UI, it is possible to embed EMF
    Forms independently of the other components of ECP. It also allows
    you to embed the form-based UI into any part of your application,
    e.g., into a view or a wizard. This is described in the section “<a
      target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/emf-client-platform-how-to-customize-the-editor-layout/#embbededitor">Embbed
      the Editor</a>”.
  </p>

  <h1 dir="ltr">Setup</h1>
  <p dir="ltr"
    id="docs-internal-guid-017d7b88-d73d-ee73-0e63-f322039cf576">
    The first step is to get the latest version of the EMF Forms
    available <a target="_blank"
      href="http://eclipse.org/ecp/emfforms/download.html">here</a>.
    Please note that the features described in this tutorial are
    available starting from version 1.3.x, so choose this or any newer
    version for the installation. To try EMF Forms, you need to install
    it into your target. If you are working in Eclipse using the default
    configuration, the IDE itself is your target platform. Beginning
    with the release of Eclipse Luna, EMF Forms is already a part of the
    package Eclipse Modeling Tools, so you do not need to install it
    separately.
  </p>
  <p dir="ltr">
    Please note that most projects use a dedicated target platform.
    Using the IDE as a target is just a simple short cut. To try EMF
    Forms, the simplest way to get started is to use the other
    components of the EMF Client Platform, too, so just install the ECP
    SDK 3.x, which includes EMF Forms from the update site (into the IDE
    or better yet, into your target platform). However, it is possible
    to embed the form-based UI into a custom application without other
    components of ECP (see section “<a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/emf-client-platform-how-to-customize-the-editor-layout/#embbededitor">Embed
      the Editor</a>”).
  </p>

  <h1 dir="ltr">Create a View Model</h1>
  <p dir="ltr">A view model describes the form-based UI of a certain
    entity type. To be available during runtime, it has to be deployed
    with the application. There are multiple ways to achieve this,
    however, the simplest one is to contain the view model in a bundle
    that is delivered as part of the application. The tooling of EMF
    Forms provides a wizard to create a new view model as well as a
    corresponding bundle containing it for an existing EMF model
    (*.ecore file). Both options are available in the New Dialog of
    Eclipse. As a short-cut, you can also right-click directly on an
    Ecore model file (*.ecore) and select EMF Forms &gt; Create View
    Model Project. In the “make it happen” example, the Ecore is located
    here: “com.eclipsesource.makeithappen.model/model/task.ecore”.
    Choose a name for the bundle, which will contain the view model,
    e.g., the default: “com.eclipsesource.makeithappen.model.viewmodel”</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.3.png"><img
      class="alignnone size-full wp-image-20196"
      alt="image06 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.3.png"
      width="530" height="312" title="Getting started with EMF Forms" /></a>
  </p>
  <p>On the next page of the wizard, you need to select the entity of
    the underlying domain model (defined in the Ecore) for which we want
    to define the form-based UI. This will be the EClass User for this
    tutorial. Additionally, you can choose whether the generated view
    model will be already filled with default controls. In this case,
    EMF Forms will create a default layout showing all attributes of the
    entity object (User). If you want to start with an empty view model,
    uncheck this option at the bottom.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.4.png"><img
      class="alignnone size-full wp-image-20193"
      alt="image031 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.4.png"
      width="530" height="363" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d74a-5a21-da53-c41ef7b5cf9c">Once
    you finish the wizard, it creates three things for you:</p>
  <ol>
    <li dir="ltr">
      <p dir="ltr">A new project containing the view model</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">A view model contained in the file
        viewmodels/User.view</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">A registration of that view model using the EMF Forms
        extension point (see the plugin.xml)</p>
    </li>
  </ol>
  <p>The view model is automatically opened in the view model editor,
    which will be described in the following section.</p>

  <h1 dir="ltr">The View Model Editor</h1>
  <p dir="ltr">EMF Forms provides an editor that supports creating and
    modifying view models.It also allows you to preview your current
    layout. To open the view model editor, double click any *.viewmodel
    file in your workspace. The view model editor shows the current view
    model in a tree on the left side and the details of the selected
    view model element on the right side. The tree allows you to create
    new elements by right clicking a parent element, delete existing
    elements and change the hierarchy using drag and drop. The details
    view allows you to enter data specific for one element, e.g.,
    assigning a name. By default the wizard described in the last
    section by default creates a control for every attribute of the
    domain model entity (User in the example).</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.5.png"><img
      class="alignnone size-full wp-image-20192"
      alt="image022 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.5.png"
      width="637" height="321" title="Getting started with EMF Forms" /></a>
  </p>
  <p>If you want to create controls manually, the view model editor
    provides a wizard for that, too. To try this, select all the
    generated controls (not the parent view), right click on them and
    delete them. Now, right click on the remaining root node (View) and
    select “Generate Controls”. The dialog allows you to select the
    attributes of the entity User that you want to create controls for.
    By marking “Show only unreferenced Attributes”, the dialog will only
    offer attributes that are not yet included in the view.</p>
  <p style="text-align: center;">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.6.png"><img
      class="alignnone size-full wp-image-20198"
      alt="image08 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.6.png"
      width="354" height="425" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d74c-8d75-1da9-6c1e48c41ddc">To
    control the results of the current view model, EMF Forms tooling
    also provides a preview. You can open it with the button located in
    the top right of the view model editor. This will open a second
    view, which presents the rendered view, that is currently described
    in the view model editor. If you open the preview in the current
    state of the view model, it will display a very simple layout,
    showing all controls in a vertical order. We will refine the layout
    in the next section. Now, whenever you change something in the view
    model editor, you can refresh the preview by clicking on the refresh
    button in the top right corner of the preview. Alternatively, you
    can turn the “auto-refresh” option on, which will refresh the
    preview on every change on the view model. Try to change the order
    of the generated controls in the view model editor using drag and
    drop and see how the changes are reflected in the preview.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.7.png"><img
      class="alignnone size-full wp-image-20195"
      alt="image05 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.7.png"
      width="598" height="381" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr">In the view model editor, we will create two projects.
    The first one will point to the view model file we previously
    created that allows us to edit the layout of a “Task” view. The
    second one will contain an instance of the “Make it happen!” example
    model. This allows us to create an instance of the domain entity
    “Task” to test the current layout we have modeled. The View Model
    Editor is combining runtime and development time features in one
    application. Once you deploy your view, only the editor showing
    instances of the domain model will be used, because users usually
    don’t change the layout of an application.</p>

  <h2 dir="ltr">The first View Model</h2>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d74d-99cf-3f14-d94ea0702d80">Let’s
    refine the view for the entity User by creating a custom layout for
    the view. Basically, a layout consists of two types of layout
    elements. The first type is a Container, which contains other
    elements. A container element specifies how children are arranged.
    Every view model has at least one container, which is the view
    itself. The view is always the parent container of all other
    elements and, as seen before, it arranges all children elements
    vertically.</p>
  <p dir="ltr">In this example, we will now use two additional
    containers, “VerticalLayout” and “HorizontalLayout”. A
    VerticalLayout arranges all its children vertically, a
    HorizontalLayout arranges all its children horizontally.</p>
  <p dir="ltr">The second type of element is a control; it does not
    contain other elements. A control renders a certain attribute or
    reference of the domain object that is shown in the view.
    Additionally, there can be elements that are bound directly to an
    attribute or reference and show static content, e.g., a label.</p>
  <p dir="ltr">Container elements can also be contained within each
    other. For example, to produce a classic two-column layout, you
    would create a HorizontalLayout with two VerticalLayouts in it. The
    VerticalLayouts contain the actual controls in this case.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.8.png"><img
      class="alignnone size-full wp-image-20201"
      alt="image11 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.8.png"
      width="404" height="295" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr">The View Model Editor allows us to create all available
    elements in the tree view. To create any kind of element, right
    click on the parent you want to create an element in. The context
    menu allows all elements that can be contained in the selected
    parent. The root parent of any layout is the view itself. Right
    click on the view itself to create a HorizontalLayout. By right
    clicking on the HorizontalLayout, you can create two
    VerticalLayouts. It is possible to assign names to all elements,
    which improves the understandability of the layout. Please note that
    you can move the control using drag and drop if you want to change
    the location within a layout. The model should now look something
    like this:</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.9.png"><img
      class="alignnone size-full wp-image-20197"
      alt="image07 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.9.png"
      width="244" height="265" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr">After saving the view model and the preview, the new
    layout will be reflected in the UI:</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.10.png"><img
      class="alignnone size-full wp-image-20203"
      alt="image002 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.10.png"
      width="614" height="420" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-dee10201-d74f-9629-c129-3a6a74cb9551">
    For this tutorial, we have only used two simple layouts. However,
    more complex layout elements are available or are currently under
    development. Additionally, it is absolutely possible and intended to
    develop a custom layout that fits specific project requirements.
    Please <a target="_blank"
      href="http://eclipse.org/emfclient/communication.html">get in
      touch with us</a> to learn about advanced and customized layouts.
    In addition, we provide <a target="_blank"
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> for EMF Forms and EMF for your custom project.
  </p>

  <h2 dir="ltr">Try the layout in the demo application</h2>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d750-0ea3-5f8d-649d28cf8027">The
    next step would be to embedd the modelled view into a custom
    application. Please see the following section for how to do that.
    However, the EMF Client Platform provides a demo application that by
    default embeds EMF Forms as an editor. Therefore, the demo
    application is a very efficient way to try the result in a real
    application.</p>
  <p dir="ltr">To create a run configuration, click on menu Run =&gt;
    Run Configurations. Add a new run configuration by clicking on the
    node “Eclipse Application”. On the first tab select to run the
    application “org.eclipse.emf.ecp.application.e3.application”.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.11.png"><img
      class="alignnone size-full wp-image-20200"
      alt="image10 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.11.png"
      width="684" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d750-a05d-09ea-20aa0ce3aa93">Select
    the tab “Plugin-Ins” to define which bundles are started. Select
    “Launch with:” =&gt; “Features selected below”. First, deselect all
    features. Scroll to the feature
    “org.eclipse.emf.ecp.demo.e3.feature” (for e4, scroll to
    “org.eclipse.emf.ecp.demo.e4.feature” instead) and check it. Press
    on the button “Select Required” to automatically add all other
    required features as well. Finally, you need to add your model to
    this run configuration. Click on “Add Plug-Ins” (below “Select
    Required”) and add all models you want to use. Always add the model
    bundle, the view model bundle and the corresponding *.edit bundle,
    too.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.12.png"><img
      class="alignnone size-full wp-image-20199"
      alt="image09 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.12.png"
      width="600" title="Getting started with EMF Forms" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d751-0ca7-388d-b3b555b2fff5">
    After starting the demo application, you need to create a project to
    contain the domain model instances. See <a target="_blank"
      href="http://eclipsesource.com/blogs/tutorials/getting-started-with-the-emf-client-platform/">here
      to learn more about the demo application of the EMF Client
      Platform</a>. For convenience, we use an EMFStore project this
    time because it requires zero setup.
  </p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/may/images/article1.13.png"><img
      class="alignnone size-full wp-image-20194"
      alt="image04 Getting started with EMF Forms"
      src="/community/eclipse_newsletter/2014/may/images/article1.13.png"
      width="377" height="273" title="Getting started with EMF Forms" /></a>
  </p>
  <p>Within the domain instance project, we create an instance of type
    “User”, right click on the project, select “New Modelelement” and
    select “User”. The User will be opened in the editor, which shows
    the layout previously specified in the view model.</p>

  <h2 dir="ltr">Embedding the Editor in an Application</h2>
  <p dir="ltr"
    id="docs-internal-guid-cf6782ba-d752-51b7-2fc5-81d3463bc9e9">Till
    now, we have only used the preview and the ECP demo application to
    show the result of view models in the UI. However, you might want to
    use only the editor independently of any other component. Therefore,
    EMF Forms provides API to embed the editor into any SWT view or
    element, e.g., a wizard.</p>
  <p dir="ltr">To create a form-based UI for an EObject, you need a
    dependency:</p>
  <p dir="ltr">org.eclipse.emf.ecp.ui.view.swt</p>
  <p dir="ltr">To render an embedded view, the ECPSWTViewRenderer in
    this bundle is used:</p>
  <p dir="ltr">ECPSWTViewRenderer.INSTANCE.render(Composite parent,
    EObject domainObject);</p>
  <p dir="ltr">This method will pick up any registered view model and
    render a view for the given object on the given SWT Composite. If no
    view model is registered, the default layout will be used. Please
    make sure that the following two bundles are in your Run/Product
    configuration:</p>
  <ul>
    <li dir="ltr">
      <p dir="ltr">org.eclipse.emf.ecp.view.model.provider.xmi: Provides
        the view models registered via extension point</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">org.eclipse.emf.ecp.view.model.provider.generator:
        Provides the default layout if no view model is explicitly
        registered</p>
    </li>
  </ul>
  <p dir="ltr">
    Please note that if you embed the editor, reference features will
    not work out of the box. This is because they require information
    about the context of an entity, e.g., which other entities it
    references. ECP provides API to provide this context, please <a
      target="_blank"
      href="http://eclipse.org/emfclient/communication.html">contact us</a>
    to learn more about this.
  </p>
  <p dir="ltr">
    If you need support with embedding EMF Forms into your application,
    if you are interested in an evaluation or you want to adapt the
    framework to your needs, we provide <a target="_blank"
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> for these purposes.
  </p>
  <p dir="ltr">
    Please visit <a target="_blank"
      href="http://eclipse.org/emfclient/emfforms/documentation.html">this
      site</a> to learn more about other EMF Forms features.
  </p>
  <p dir="ltr">
    Please visit <a target="_blank"
      href="http://eclipse.org/emfclient/documentation.html">this site</a>
    to learn more about other ECP features.
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/jhelming.jpg"
        alt="jonas helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jonas Helming <br />
        <a href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>
        <li><a href="https://twitter.com/JonasHelming">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>