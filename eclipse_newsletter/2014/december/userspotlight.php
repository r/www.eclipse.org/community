<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Lars Vogel";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2014/december">December 2014</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2014/december/images/lars.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I'm in the lucky position to live my personal Open Source dream. 
					After approx. 15 years in the industrial software industry I co-founded the 
					vogella GmbH which has the target to acquire and distribute knowledge in the Open Source world. 
					We spend approximately 50 % our time working for customers (training, consulting, development) and 50 % 
					of our time working on Open Source projects. In our Open Source work, the Eclipse project is definitely 
					the area where we mainly spend our time, because we love the technology and the community. We also 
					believe that enhancing Eclipse has a high impact on the efficiency of the Java development community.</p> 

				<p>Last but not least, I spend a significant amout of time writing about Open Source technology on our 
					website vogella.com and in the form of books.</p>
					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>Honestly, I don't remember. I think I started with a version of Eclipse 2, which should make it around 2003 
					or 2004. When Eclipse 3.0 was released I started with the Eclipse RCP platform, at the beginning just for 
					fun and later to write tools to simplify my daily work. For example while working for SAP AG I used to be 
					responsible for the performance test activities for a software component. I wrote an RCP application for 
					automating the setup and configuration process and afterwards our team was able to setup a new performance 
					test system within hours instead of months.	</p>
					
			<h3>Name five plugins you use and recommend:</h3>
					<p>Besides the obvious things, like platform, SWT and JDT which every Eclipse user has to use, my favorite are:</p>
					<ul>
						<li><a href="https://www.eclipse.org/egit/">EGit</a>: I cannot imagine better Git support within the Eclipse IDE. And don't get me wrong here, 
					I think EGit still has room to improve (hint: less blocking popups for success messages), but the community behind EGit 
					is amazing. Matthias Sohn the leader is very open and responsive to feedback. They have a strong community and the core 
					EGit developers (like Robin Stocker, who contributes in his spare time) have an eye for performance and usability issues. 
					Their superb Gerrit support makes the contribution process to the Eclipse project as smooth as possible, hence they are a 
					strong catalyst for several projects. A very strong #1.</li>
						<li><a href="http://marketplace.eclipse.org/content/tycho-build-tools">Tycho</a>: A close second to EGit is the Tycho build tooling for Eclipse components. While building is 
					commonly considered a bit dull (== it just should work), I feel that a good build story is essential for an open source 
					project. Also I enjoy the interaction with the Tycho community and their leader Jan Sievers. It is fun to report an issue 
					in Tycho, because the Tycho developers are immediately analyzing the issue and either helping you or improving their tooling. 
					Tycho is also the cornerstone of the CBI build initiative of Eclipse and has enabled the platform team to provide an Eclipse 
					build setup which allows everyone to run an Eclipse SDK build with just a few commands. </li>
						<li><a href="https://www.eclipse.org/m2e/">M2E</a>: The Maven tooling in Eclipse is a bit of a love/ hate relationship for me. I value the recent work 
					Igor Fedorenko and Jason van Zyl have put into Maven and m2e and think it is great that Redhat developers are helping. Good 
					Maven support is crucial for the Eclipse community and I believe the M2E developers start addressing a lot of outstanding 
					issues these days. </li>
						<li><b>Automatic error reporting</b>: A relative new initiative by Marcel Bruch which allows to automatically 
					report errors in the Eclipse IDE to Eclipse.org. Marcel and I used to work together and I cherish his energy to improve 
					the development experience in the Eclipse IDE. His latest tooling, the automatic error reporting, can be a game changer 
					for the Eclipse IDE. If we as a developer community get aggregated data about the problems areas within the Eclipse IDE, 
					we can focus our energy within these areas. His tooling also integrates nicely with our (Eclipse platform) new interactive 
					performance monitor which Google contributed. This tooling allows to report UI freezes and Marcels error reporting tool 
					picks these up and create bug reports for the corresponding component. We already see several performance related fixes 
					triggered by these reports. I believe that this will help us to provide the fastest IDE out there.</li>
						<li><a href="http://saneclipse.vogella.com">saneclipse</a>: That is a very personal pick but we at vogella GmbH started recently a small project 
					to provide better default settings and templates for the Eclipse IDE. Whenever I'm at the customer side, it almost breaks 
					my heart to see how many Java developers struggle with the Eclipse IDE, because of certain default settings. So instead of 
					hoping that every developer learns more about the Eclipse IDE, we now provide a set of plug-ins which we think make the life 
					of the developer easier. We plan to extend this activity, e.g. by developing additional functionality in saneclipse and to 
					apply our learnings as much as possible back to the Eclipse project.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>Playing with my kids and in general spending time with my family is the greatest time I have. Having small kids is 
					very awesome, because you can play with Lego, learn to ride the Wave Board and the like. I also picked up the 
					habit of regular running and find it these days very relaxing to run 2-3 times per week.</p>				
				
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="http://www.vogella.com">Website</a></li>
					<li><a target="_blank" href="http://www.twitter.com/vogella">Twitter</a></li>
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>