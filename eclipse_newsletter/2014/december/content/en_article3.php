<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>For the first time in a hundred years, the electric power utility
      industry is undergoing a momentous change. Distributed renewable
      power generation is introducing competition into an industry that
      has been managed as regulated monopolies. Consumers with solar
      photovoltaic (PV) panels on their roofs and companies like Solar
      City are fundamentally changing the traditional utility business
      model. As a result the electric power industry is undergoing an
      unprecedented transformation that is changing every aspect of the
      utility industry. One of these changes involves the role that
      geospatial data and technology play in the electric power
      industry.</p>

    <p>Geographic information systems (GIS) are currently applied
      tactically by utilities in a number of areas. For example, GIS has
      been widely used by utilities for years for automated
      mapping/facilities management, back office records management,
      asset management, transmission line siting, and more recently for
      design and construction, energy conservation, vegetation
      management, mobile workforce management (MWFM), and outage
      management (OMS). Utilities are integrating GIS with automated
      meter infrastructure (AMI) and supervisory control and data
      acquisition (SCADA) systems.</p>

    <p>
      Improving process efficiency is a major challenge for the utility
      industry. Geospatial technology is a key technology that helps
      utilities improve the efficiency of their business processes (such
      as designing-constructing-maintaining and operating their network
      infrastructure) and the productivity of their staff. An <a
        target="_blank"
        href="http://www.researchandmarkets.com/research/b8njrs/global_gis_market">analysis
        by Research and Markets</a> identified the key factors driving
      growth in the utility GIS market including the growing need for
      knowledge infrastructure, data quality, managing the mobile
      workforce, and changing the organizational structure of utilities.
    </p>

    <p>
      A recent <a target="_blank"
        href="http://www.navigantresearch.com/newsroom/smart-grid-technology-market-will-reach-73-billion-in-annual-revenue-by-2020">report
        from Navigant Research</a> estimates that the market for smart
      grid technologies will reach $73 billion in annual revenue by the
      end of 2020. Navigant forecasts that the increasing penetration of
      GIS into smart grid workflow applications, such as mobile
      workforce management (MWFM), distribution management system (DMS),
      energy management systems (EMS), outage management system {OMS},
      customer information systems (CIS), and analytics will be the
      primary driver for electric utility GIS software and services
      growth. Research and Markets sees one of the key factors driving
      this market growth is the growing need for knowledge
      infrastructure.
    </p>

    <p>Navigant Research's analysis forecasts that geospatial will
      become a foundational technology for the smart grid - "the smart
      grid is all about situation awareness and effective anticipation
      of and response to events that might disrupt the performance of
      the power grid. Since spatial data underlies everything an
      electric utility does, GIS is the only foundational view that can
      potentially link every operational activity of an electric utility
      including design and construction, asset management, workforce
      management, and outage management as well as supervisory control
      and data acquisition (SCADA), distribution management systems
      (DMS), renewables, and strategy planning."</p>

    <p>
      The move to smart grid creates a major challenge for small to
      medium utilities. Smaller utilities don't have the in-house
      information technology (IT) skills or the budget to implement
      expensive proprietary GIS and other IT applications required by
      the industry move to smart grid. This has created a major
      opportunity for commercial open source software vendors such as
      those represented by the LocationTech community. The Research and
      Markets report <a target="_blank"
        href="http://www.researchandmarkets.com/research/b8njrs/global_gis_market">Global
        GIS Market in the Utility Industry 2014-2018</a> identifies an
      important new market trend in the <u>increasing penetration of
        open-source geospatial software into the utility market</u>.
      According to the report many small and medium-sized organizations
      that cannot afford to invest in expensive proprietary GIS
      solutions are now choosing open-source software. In the U.S. most
      of the 3000 electric power utilities are small to medium-sized
      organizations, primarily municipal and rural electric coops.
    </p>

    <p>The Research and Markets report forecasts that the global utility
      GIS market will grow at a compound annual growth rate (CAGR) of
      9.27 % over the period 2014-2018 with improving process efficiency
      a key market driver. The Research and Markets report assesses the
      present global utility GIS market and its projected growth
      prospects for the period 2014-2018. The report covers the
      Americas, and the Europe, Middle East, and Africa (EMEA) and Asia
      Pacific (APAC) regions. To estimate market size and vendor share,
      analysts looked at revenue generated from sales of software, data,
      and services in several segments including traditional GIS, global
      positioning system (GPS), photogrammetry and remote sensing,
      geospatial engineering, and other GIS applications. The report was
      prepared based on an in-depth market analysis with inputs from
      industry experts. Vendors included in the study are Bentley
      Systems Inc., GE Energy, Hexagon AB, Autodesk Inc., AvisMap GIS
      Technologies, Beijing SuperMap Software Co. Ltd., China
      Information Technology, Esri Inc., Google Map, Hitachi Zosen
      Corp., MacDonald, Dettwiler and Associates Ltd., Pitney Bowes Inc.
      and the US Geological Survey.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/december/images/geoff.JPG"
        width="90" alt="geoff zeiss" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Geoff Zeiss<br />
        <a target="_blank" href="http://www.betweenthepoles.info/">Between
          The Poles</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://geospatial.blogs.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/gzeiss">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
