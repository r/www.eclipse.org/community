<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <img
      src="/community/eclipse_newsletter/2014/december/images/LocationTech.png"
      width="300" alt="" /><br>
    <p>
      Simply stated, <a target="_blank"
        href="https://www.locationtech.org/">LocationTech</a> is a
      community of people who care about place. <br /> <br /> <br />


    <p>Each participant brings their own unique inspiration. After
      travelling the world and hearing their stories, I've decided the
      strong common value is to help society be the best it can be. What
      does this mean?</p>
    <p>Each perspective is unique. I have heard smart and passionate
      people talk about better urban planning, better stewardship for
      nature, better safety and security, pursuing knowledge, advancing
      the state of the art, teaching & learning skills, entrepreneurs &
      intrapreneurs building their businesses, people who just want to
      tell a story, and more. These ideas give us purpose. We would
      strive for them no matter the tools and technologies we have.</p>

    <p>While we would strive for them no matter, better tools and
      technologies make it easier to achieve our goals. Collaborating
      helps us do more, faster, better, and with less risk.</p>

    <p>There are practical aspects to the group like great geospatial
      software under open source licenses, collaborative infrastructure,
      good governance, intellectual property review services for the
      software and data, great events around the world, warm camaraderie
      & networking, challenging but rewarding internships, and more.
      These things are wonderful, but they don't bind the community
      together. What unites us are the desires to collaborate and to do
      so under the same model.</p>

    <p>The software libraries at LocationTech provide fundamental
      implementations of data types, algorithms, indexing, data
      management, and similar features that enable more complex
      operations. These components underpin everything built on top of
      them.</p>

    <p>Technology change has created an inflection point for geodata.
      Mobile devices, social media, retail transactions, and more
      generate a tremendous amount of data. The volume, variety, and
      velocity of data is ever increasing. The high performance
      geoprocessing services enable orders of magnitude faster
      processing velocity and storage capacity. These services change
      the rules of what can be done.</p>

    <p>Data has become crucial. A considerable amount of geodata is
      commodity. This is to say that it is not a source of competitive
      advantage if it does not differ from organization to organization.
      Collaboration based on a common pool of data makes sense. The data
      commons is a new initiative for sharing open data.</p>

    <p>Tools to provide distributed and intelligent version control
      allow for mobile devices that have just the right data, but can
      update it based on context or to incorporate updated information.
      Flexible and easy to use version control features enable useful
      multi-directional workflows.</p>


    <p>The visualization components turn data into rich user interfaces.
      While they do often enable maps, they also enable models, charts,
      graphs, and others. They do so on mobile devices as well as other
      platforms. The off-line abilities of the mobile libraries,
      combined with version control allow much greater flexibility.</p>

    <p>Each of the components can be used alone, or together to develop
      more complex systems. They can be built into proprietary or open
      solutions. Collections of components help can be used to unite big
      data on the server with small data on mobile devices. They enable
      rich & intuitive user interfaces and help solve interesting
      problems.</p>

    <p>This is what we need to achieve our purpose and to help society
      be the best it can be. This is LocationTech, and everyone is
      welcome.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/december/images/andrew.png"
        width="90" alt="andrew ross" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Andrew Ross<br /> <a target="_blank"
          href="http://www.eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://42aross.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="http://twitter.com/42aross">Twitter</a></li>
        <!--<li><a target="_blank" href="https://www.linkedin.com/pub/max-rydahl-andersen">Lindedin</a></li>
            $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
