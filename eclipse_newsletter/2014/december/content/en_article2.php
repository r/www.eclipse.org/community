<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Do you know where the border between Russia and Ukraine is? As we
      all know, folks on different sides of that border may have very
      different ideas about it. What if it was your job to maintain an
      “authoritative” set of international boundaries? You would
      probably have many “versions” of the border, depending on who your
      audience was, or what historical period you were expected to
      illustrate.</p>

    <p>Or, if you work for a utilities company, you probably have
      multiple versions of your assets. The current built state, sure,
      but also various future possible states: What if your leadership
      chooses a different capital plan? What if a new right of way is
      developed next year, or five years from now?</p>

    <p>Or, if you are an emergency manager (or a company commander) and
      your field staff are returning data to you on the state of
      operations in real time, you’ll have a collection of multiple
      views of reality. Sometimes they’ll be consistent, sometimes they
      won’t.</p>

    <p>All these problems share a common information management
      challenge: they are about maintaining simultaneous, separate
      versions of present (or future) versions, being able to
      independently update those versions, while retaining the
      capability to merge them together into a unified version at any
      point.</p>

    <p>
      Software developers have long had extremely versatile tools, such
      as <a target="_blank" href="http://git-scm.com/">Git</a>, for
      managing their software, retaining a detailed log of all changes
      made, and who made them through time on multiple versions. We
      looked at these tools, which allow different authors to maintain
      their own versions of the software, while collaborating and
      merging changes between their versions, and asked ourselves: Why
      don’t we have this capability for spatial data management?
    </p>

    <p>
      The answer was, because of the size and complexity of spatial
      data, solving the problem is quite difficult. So we took the time
      to create <a target="_blank" href="http://geogig.org/">GeoGig</a>,
      which takes the core computer science concepts used in distributed
      version control and applies them to managing versioned spatial
      data.
    </p>

    <a target="_blank" href="http://geogig.org/"><img
      src="/community/eclipse_newsletter/2014/december/images/decarticle1.1.png"
      class="img-responsive" alt="download geogig" /></a> <br />
    <br />
    <p>With GeoGig, developers can create and edit data with colleagues,
      the crowd, or anyone using a variety of workflows. Scenarios can
      be as simple as a pair of friends editing a shared online copy of
      data, or as complex as a company with multiple independent
      disconnected versions being synchronized on an as-available basis.</p>

    <p>Because GeoGig tracks every edit, addition and deletion, data
      provenance is maintained at the finest possible granularity.
      Developers can track and visualize every change that is made to
      data for quality control, and each change is saved and previous
      versions can be restored at any time.</p>

    <p>
      To further leverage the abilities of GeoGig, <a target="_blank"
        href="http://boundlessgeo.com/">Boundless</a> recently announced
      a new solution — called Versio — for organizations to manage their
      spatial data. <a target="_blank" href="http://vers.io/">Versio</a>
      is hosted service built on GeoGig that provides easier toolks for
      data authors to collaborate on spatial data while streamlining the
      creation, maintenance, and distribution of spatial data. No other
      solution has these capabilities.
    </p>


    <a target="_blank" href="http://vers.io/"><img
      src="/community/eclipse_newsletter/2014/december/images/decarticle1.2.png"
      class="img-responsive" alt="versio" /></a>

    <p>Collaboratively organizing and publishing spatial data used to be
      a daunting task, but now, if executed in the right way, it can be
      the most effective way to accomplish business processes. And with
      a never-ending flow of ubiquitous location data, companies now
      have a system to effectively harness spatial data and give
      meaningful structure to the avalanche.</p>

    <p>
      <i>Based on article previously published on GIS Lounge: <a
        target="_blank"
        href="http://www.gislounge.com/versio-moving-spatial-industry-forward/">Versio:
          Moving the Spatial IT Industry Forward</a></i>
    </p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/december/images/paul.jpg"
        alt="paul ramsey" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Paul Ramsey<br />
        <a target="_blank" href="http://boundlessgeo.com/">Boundless</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://boundlessgeo.com/blog">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/boundlessgeo">Twitter</a></li>
        <!--<li><a target="_blank" href="https://www.linkedin.com/in/marcelbruch">Linkedin</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

