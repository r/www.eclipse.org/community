<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    <a target="_blank" href="http://docs.oracle.com/javase/8/">Java SE 8</a>
    is the most significant expansion of the Java programming model yet
    and <a target="_blank" href="https://www.eclipse.org/luna/">Eclipse
      Luna (R 4.4)</a> has been reloaded to be ready for Java 8.
  </p>
  <p>Here is a glimpse of goodies provided by Eclipse as part of the
    Java 8 support:</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article1.1.png"><img
      class="img-responsive"
      src="/community/eclipse_newsletter/2014/june/images/article1.1.png" alt="" /></a>
  </p>
  <br />
  <h1>
    Under the hood
  </h1>
  <p>The Java tooling in Eclipse has its own compiler implementation
    which not only generates the class files, but also produces a Java
    document model which forms the basis for implementing a large number
    of tooling features.</p>
  <p>Preparing Eclipse to be Java 8 ready involved a series of steps
    filled with technical and process challenges to support all the
    incremental updates of the Java Language Specification: the parser,
    compiler and indexer were evolved, the new type inference engine was
    developed, debugger evaluation support was added, major updates were
    introduced in DOM AST and Java model APIs, existing functionality
    was retooled to blend with the new language features and new
    functionality was added to make your experience with Java 8 more
    pleasant.</p>

  <h1>
    Highlights
  </h1>
  <ul>
    <li>The Eclipse Compiler for Java (ECJ) implements all the new Java
      8 language enhancements.<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.2.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.2.png" alt="" /></a>
    </li>
    <li>Quickly change a project to be compatible with Java 8:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.3.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.3.png" alt="" /></a></li>
    <li>Convert anonymous class creations to lambda expressions (and
      back) by invoking the <b>Quick Assists (Ctrl+1)</b>:
      <ul>
        <li>Convert to lambda expression</li>
        <li>Convert to anonymous class creation</li>
      </ul> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.4.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.4.png" alt="" /></a>
    </li>
    <li> Or invoke
    <b>Source > Clean Up...</b> to migrate all your existing code to use
    lambda expressions where applicable:
    <br />

    <a
      href="/community/eclipse_newsletter/2014/june/images/article1.5.png"><img
      width="600"
      src="/community/eclipse_newsletter/2014/june/images/article1.5.png" alt="" /></a>
    <br />
    <br /> The functionality to migrate anonymous classes to lambda
    expressions has been made intelligent to make sure that only valid
    code is generated. It ignores the conversion where it is not
    possible, and it even updates the variable names to prevent name
    conflicts with variables from an enclosing scope. For example, the
    conversion is not performed if the anonymous class does not
    implement a functional interface, implements a functional interface
    with a generic abstract method, contains additional members,
    contains unqualified ‘this’ or ‘super’ references etc.
    </li>
    <li>Change lambda expression’s body to block or expression by
      invoking the Quick Assists (Ctrl+1):
    <ul>
      <li>Change body expression to block <a
        href="/community/eclipse_newsletter/2014/june/images/article1.6.png"><img
          width="600"
          src="/community/eclipse_newsletter/2014/june/images/article1.6.png" alt="" /></a></li>
      <li>Change body block to expression <a
        href="/community/eclipse_newsletter/2014/june/images/article1.7.png"><img
          width="600"
          src="/community/eclipse_newsletter/2014/june/images/article1.7.png" alt="" /></a></li>
    </ul></li>
    <li>The new concise syntax of lambda expressions and method
      references looks nice, but it may not be so intuitive for
      developers due to inferred target types, inferred parameter types,
      various shorthand syntaxes for method and constructor references
      etc. This is where Eclipse helps you to become more productive
      while writing or understanding programs with Java 8 constructs.
    <ul>
      <li>To <b>view</b> the method implemented by the functional
        interface instances, just <b>hover</b> on:

      <ul>
        <li>‘->’ in a lambda expression<br /> <a
          href="/community/eclipse_newsletter/2014/june/images/article1.8.png"><img
            width="600"
            src="/community/eclipse_newsletter/2014/june/images/article1.8.png" alt="" /></a></li>
        <li>‘::’ in a method reference<br /> <a
          href="/community/eclipse_newsletter/2014/june/images/article1.9.png"><img
            width="600"
            src="/community/eclipse_newsletter/2014/june/images/article1.9.png" alt="" /></a></li>
      </ul></li>
      <li>To <b>navigate</b> to the functional method declaration, hold
        <b>Ctrl</b>, move the mouse pointer over '->' or '::', and click
        the hyperlink:
        <ul>
          <li>In a lambda expression:<br /> <a
            href="/community/eclipse_newsletter/2014/june/images/article1.10.png"><img
              src="/community/eclipse_newsletter/2014/june/images/article1.10.png" alt="" /></a></li>
          <li>‘::’ in a method reference<br /> <a
            href="/community/eclipse_newsletter/2014/june/images/article1.11.png"><img
              src="/community/eclipse_newsletter/2014/june/images/article1.11.png" alt="" /></a></li>
        </ul>

    </ul></li>
    <li>You can control the formatting of lambda expressions via new <b>Formatter</b>
      options:

    <ul>
      <li>Configure the position of <b>braces</b> in lambda body<br /> <a
        href="/community/eclipse_newsletter/2014/june/images/article1.12.png"><img
          src="/community/eclipse_newsletter/2014/june/images/article1.12.png" alt="" /></a></li>
      <li>Configure the <b>whitespace</b> before and after the arrow
        operator<br /> <a
        href="/community/eclipse_newsletter/2014/june/images/article1.13.png"><img
          src="/community/eclipse_newsletter/2014/june/images/article1.13.png" alt="" /></a></li>
    </ul></li>
    <li>You can search for method reference expressions in <b>Java
        Search</b> by limiting the match location of methods or
      constructors to method references:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.14.png"><img
        width="450"
        src="/community/eclipse_newsletter/2014/june/images/article1.14.png" alt="" /></a></li>
    <li>The <b>Type Hierarchy</b> view and <b>Quick Hierarchy (Ctrl+T)</b>
      (on the functional interface and its abstract method) show lambda
      expressions also as implementations of the functional interface:<br />
      <a
      href="/community/eclipse_newsletter/2014/june/images/article1.15.png"><img
        src="/community/eclipse_newsletter/2014/june/images/article1.15.png" alt="" /></a></li>
    <li>Eclipse also provides <b>Content Assist (Ctrl+Space)</b> support
      for method references:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.16.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.16.png" alt="" /></a></li>
    <li>Quickly identify the abstract (A), default (D) and static (S)
      methods of an interface in views like Outline, Search, Type
      Hierarchy, Quick Outline / Hierarchy with the new icon <b>adornments</b>:<br />
      <a
      href="/community/eclipse_newsletter/2014/june/images/article1.17.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.17.png" alt="" /></a>
    </li>
    <li>The <b>Java debugger</b> supports breakpoints in lambda
      expressions and evaluation or inspection in the context of
      interface methods:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.18.png"><img
        src="/community/eclipse_newsletter/2014/june/images/article1.18.png" alt="" /></a></li>
    <li><b>JEP 118</b> provides the option to store information about
      method parameters in class files when a programmer opts for it.
      This information is then usable via reflection. In 1.8 compliance
      mode, you can enable this with the new preference option:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.19.png"><img
        width="450"
        src="/community/eclipse_newsletter/2014/june/images/article1.19.png" alt="" /></a></li>
    <li>Java tooling features like Refactorings, Source actions, Content
      assist (Ctrl+Space), Mark occurrences, Expand selection,
      Reconciler, Annotation Processor support etc. have been updated to
      work well with the new Java 8 features. Here is an example of the
      extract method support:</li>
    <li><a
      href="/community/eclipse_newsletter/2014/june/images/article1.20.png"><img
      width="600"
      src="/community/eclipse_newsletter/2014/june/images/article1.20.png" alt="" /></a>
    </li>
    <li><b>JSR 308</b> introduces <b>Type Annotations</b> in Java 8, and
      Eclipse provides the support for <a target="_blank"
      href="http://help.eclipse.org/luna/topic/org.eclipse.jdt.doc.user/tasks/task-using_null_type_annotations.htm">type
        annotations based static null analysis</a> by shipping a set of
      nullness annotations as version 2.0.0 of the
      org.eclipse.jdt.annotation bundle. With these annotations, compile
      time type checking is possible to ensure that the specified null
      constraints are not violated and all possible
      NullPointerExceptions are reported (active if annotation-based
      null analysis is enabled in Java Compiler > Errors/Warnings
      preferences): <a
      href="/community/eclipse_newsletter/2014/june/images/article1.21.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.21.png" alt="" /></a><br />
      <br />This supports API designers to create better interfaces with
      stricter rules about what can be passed to and returned from an
      API:<br /> <a
      href="/community/eclipse_newsletter/2014/june/images/article1.22.png"><img
        width="600"
        src="/community/eclipse_newsletter/2014/june/images/article1.22.png" alt="" /></a></li>
    <li>The Eclipse Compiler for Java (with its new Java 8 type
      inference engine) has also been verified to successfully build
      major projects like JRE8, OpenJFX and Eclipse SDK (~250 projects).</li>
  </ul>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/june/images/Noopur.jpg"
        width="75" alt="noopur gupta" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Noopur Gupta <br />
        <a href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link">
        <li><a href="http://about.me/noopur2507">About the author</a></li>
        <!--<li><a href="https://twitter.com/noopur2507">Twitter</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
