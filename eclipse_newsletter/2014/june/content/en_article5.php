<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <A target="_blank" HREF="http://eclipse.org/luna">Eclipse Luna</a> is
  here! 10 years after the first Eclipse annual summer release shipped,
  the Eclipse team is still shipping quality software on-time and
  on-budget. Eclipse Luna contains 76 different Eclipse projects and
  over 10 million lines of new code was contributed this year.
  <p>Since 2007 I've been counting down the 10 most New and Noteworthy
    features of each release. These articles highlight the features I'm
    most excited about. Here is my list for 2014.


  <h2>
    <A target="_blank"
      HREF="http://eclipsesource.com/blogs/2014/06/12/rcp-improvements-top-eclipse-luna-feature-10/">10.
      RCP Improvements</A>
  </h2>
  <p>2014 marks the 10th anniversary of Eclipse RCP, the Rich Client
    Platform. Using the Eclipse programming model, with OSGi, extension
    points and SWT, developers can build Rich Client Applications for
    the Desktop. This year, assembling RCP applications got a lot
    easier. The Eclipse product editor now supports platform specific
    launching arguments and config.ini properties. You can set program
    arguments and VM settings on a per-platform basis and bundle start
    levels can also be set in the product editor. Finally, software
    update sites can now be added to your product with ease.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/rcp.png" alt="rcp" />


  <H2>
    <A target="_blank"
      HREF="http://eclipsesource.com/blogs/2014/06/13/sirius-top-eclipse-luna-feature-9/">9.
      Sirius</A>
  </H2>
  <p>
    The <a target="_blank" href="http://www.eclipse.org/sirius/">Sirius
      project</a> combines modelling and graphical editors. Using
    Sirius, you can create graphical editors and a modelling workbench
    for your domain specific language. The elements in an EMF model can
    be mapped to graphical elements such as nodes, edges, images or
    containers and visual properties (such as colour, icons, line width,
    etc.) can be mapped to model properties. Editors and tool palettes
    can be integrated, providing a fully working graphical editor for
    your model. The Ecore Tools project, a graphical editor for EMF
    itself, has also be completely re-written using Sirius, providing an
    exemplary tool for this great technology.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/sirius.png" alt="sirius" />


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/16/emf-forms-top-eclipse-luna-feature-8/">8.
      EMF Forms</a>
  </h2>
  <p>
    EMF Forms is part of the <a href="http://eclipse.org/ecp/">EMF
      Client Platform Project</a>, a framework for building EMF-based
    Rich Client applications. Instead of manually coding form-based UIs,
    EMF forms renders the user interface from a view model. The view
    model defines layout, components and the bindings to the EMF Model.



  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/emf-form.png" alt="emf forms" />


  <p>Master detail views, tables, trees and input dialogs can all be
    integrated by mapping the model element to the particular user
    interface widget. You can customize the label, hint text and supply
    custom validators to your forms. With EMF Forms, the data and view
    models are completely decoupled, easing both the development and the
    maintenance of these UIs.


  <p>


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/17/tcf-terminal-top-eclipse-luna-feature-7/">7.
      TCF Terminal</a>
  </h2>
  <p>
    While developing software, staying focused and <a
      href="http://www.cs.umd.edu/localphp/hcil/tech-reports-search.php?number=2003-37">in
      the flow</a> helps maintain productivity. Context switching,
    distractions and continually moving between tools can easily break
    the <em>flow</em>. Flow is one of the reasons Eclipse is such a
    powerful IDE, since it brings all your development tools into a
    single workbench. But as good as Eclipse is, and as large as the
    ecosystem is, sometimes a tool is just not available as a plug-in.
    To assist you here, Eclipse now ships with a powerful terminal,
    providing you access to your system terminal directly from within
    the IDE.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/tcf.png" alt="tcf" />


  <p>The terminal is available on all major platforms and supports
    colours (syntax highlighting), tab completion, copy and paste and
    even VI Editing. The shell also supports remote system access,
    allowing you to connect to other systems to ease deployment, start a
    remote debugger or kick-off a command line build.


  <p>


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/18/rap-top-eclipse-luna-feature-6/">6.
      RAP Improvements</a>
  </h2>
  <p>
    Originally a platform for Rich Ajax applications, RAP has
    transitioned into a general purpose Remote Application Platform. In
    addition to the web client, RAP now supports <a
      href="http://developer.eclipsesource.com/tabris/">mobile clients</a>
    and a desktop client is currently under development. Over the past
    year, a number of improvements have been added to RAP. The file
    upload dialog now supports Drag and Drop, the DropDown widget used
    for AutoSuggest has graduated from incubation and a powerful row
    template mechanism for tables has been introduced.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/rap.png" alt="rap" />


  <p>
    Instead of simple grids, row templates allow you to layout all
    aspects of each row. Images can be aligned in different positions,
    elements can be aligned relative to each other, and areas can be
    designated as <i>selectable</i>.


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/20/dark-theme-top-eclipse-luna-feature-5/">5.
      Dark Theme</a>
  </h2>
  <p>Developers are passionate about their tools, and many developers
    take great pride in customizing the look and feel of their
    development machines. With the dark theme, Eclipse now looks
    beautiful on a development machines with other dark system settings
    enabled.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/dark.png" alt="dark theme" />


  <p>Eclipse Luna is also shipping with a new set of platform icons
    which improve the look on both light and dark themes and of course
    the theme is cross-platform, supporting Mac, Linux and Windows.


  <h2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/20/snip-match-top-eclipse-luna-feature-4/">4.
      SnipMatch</a>
  </h2>
  <p>
    <a target="_blank" href="http://www.eclipse.org/recommenders/">Code
      Recommenders</a> is one of the most exciting and innovative
    plug-ins available for Eclipse. With standard <em>content-assist</em>,
    Eclipse will show you all the method calls, variables uses, template
    options, etc... for the current context. Most of these options are
    irrelevant, and if you're learning a new API, all these options can
    be confusing. Furthermore, listing the code completion options in
    alphabetical order provides very little value. Code recommenders
    aims to fix this by displaying the available completions options
    according to their relevance.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/snipmatch.png" alt="snipmatch" />


  <p>SnipMatch by Code Recommenders lets developers search for, and
    quickly insert, code snippets. API usage is often more than method
    calls, requiring proper setup, usage and possibly teardown code.
    With SnipMatch, entire blocks of templated code can be inserted with
    a single click. To use SnipMatch, install code recommenders and
    press CTRL+ALT+Space.


  <h2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/23/git-improvements-top-eclipse-feature-luna-3/">3.
      EGit Improvements</a>
  </h2>
  <p>
    Git tools for Eclipse (EGit) started in 2009 and has improved by
    leaps and bounds each year. Over the past 12 months a number of
    notable improvements have been added to EGit. You can now directly
    edit commits from the history view. You can also select multiple
    consecutive commits and <em>squash</em> them.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/git.png" alt="git" />


  <p>Interactive rebase has also been implemented. With interactive
    rebase, commits can be removed, squashed, edited or modified. The
    order of the commits can also be altered. Finally, the blame
    annotations in EGit have been greatly improved. A diff is shown
    in-line as well as links to the parent commit.


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/24/split-editors-top-eclipse-luna-feature-2/">2.
      Split Editors</a>
  </h2>
  <p>After 12 years, 200 votes, and over 181 comments, split editors
    have finally be introduced to Eclipse. Editors can be split
    horizontally ( CTRL+_ ) or vertically ( CTRL+{ ) allowing you to
    edit two parts of a file at the same time.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/split.png" width="700" alt="split" />


  <p>

    In addition to split editors, a number of other notable improvements
    have been added to platform. Perspectives can be re-ordered, GTK+3
    is a proper SWT port, and line numbers are now on by default. For a
    full list of what's new in the Eclipse Platform, checkout the <a
      href="http://www.eclipse.org/eclipse/news/4.4/final/eclipse-news-part1.php">New
      and Noteworthy</a>.


  <H2>
    <a target="_blank"
      href="http://eclipsesource.com/blogs/2014/06/25/java-8-support-top-eclipse-luna-feature/">1.
      Java 8 Support</a>
  </H2>
  <p>Eclipse now has first class tool support for Java 8! From the
    Eclipse Java Compiler, to Maven integration. From the Memory
    Analyzer to the Java tooling, full Java 8 support is here.


  <p>
    <img src="/community/eclipse_newsletter/2013/june/images/jdt.png" alt="jdt" />


  <p>
    The Java Tools provide <em>quick-fixes</em> for converting anonymous
    inner classes to lambda expressions as well as in-line lambda
    refactoring support. Eclipse also has support for lambda debugging
    and breakpoints. Code formatters for Java 8 constructs are available
    as is support for type annotations, including a set of annotations
    for null analysis. There is also a number of other enhancements such
    as better type inference, support for effectively final variables
    and default methods.


  <p>
    Java 8 represents one of the biggest Java releases ever. The Eclipse
    committers as a whole, and the JDT team in particular, should be
    really proud of the great work they did with <A
      HREF="http://eclipse.org/luna">Luna</a>.


  <p>
    Thank-you to everyone involved with the Eclipse Luna release! For
    more Eclipse Tips and Tricks, you can follow me on <a
      href="https://twitter.com/irbull">Twitter</a>.


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/june/images/ian_bull2.PNG"
        alt="Ian Bull" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ian Bull <br />
        <a target="_blank" href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://eclipsesource.com/blogs/author/irbull/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/irbull">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/b/107434916948787284891/110293252086584362394/posts">Google
            +</a></li>
        <!--$og; -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
