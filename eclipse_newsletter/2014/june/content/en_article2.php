<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    With Eclipse Luna comes a complete re-implementation of <a
      href="http://www.eclipse.org/ecoretools">EcoreTools</a>, the
    diagram editor for Ecore. This is an important piece of news because
    EcoreTools is often the first step our adopters have to go through.
    Users are expecting to have a diagram editor to design a domain
    model. They are expecting this because they are used to this
    notation, they learnt it at school or university and even just this
    ubiquity makes it powerful in itself.
  </p>
  <p>
    <a
      target="_blank" href="/community/eclipse_newsletter/2014/june/images/article2.1.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.1.png" alt="" /></a>
  </p>
  <p>
    And that's why EcoreTools matter. And that's why EcoreTools matter
    even more for an organization like <a href="http://www.obeo.fr/">Obeo</a>.
  </p>

  <p>But then what happened ? Why did this project have been stale for
    years ?</p>
  <br />

  <h4>How it all started</h4>
  <p>Let's come back to the creation of this project. The proposal has
    been submited in 2007, and in 2008 EcoreTools 0.8 was shipped with
    Ganymede.</p>
  <p>At that time building such a graphical modeler was quite costly,
    even with the GMF runtime and Tools which were leveraged by the
    original team, going from a very basic diagram editor to a complete
    tool with a consistent user experience was a lot of work. And they
    tackled this work and had progress from version to version up to the
    end of 2009 when the company behind this effort got acquired and
    changed its focus. The project entered hibernation then.</p>
  <p>Because it was a key component of the whole modeling eco-system, Ed
    Merks and I stepped up to take care of the project, leading it
    through the bare minimum so that it can be part of the release train
    and still exist. The plan at that time was to find other people
    interested in investing in it, unfortunately it did not happen and I
    can't blame them, as I said that was costly, an investment you could
    not do without clear gains to expect.</p>
  <p>
    Then Thales and Obeo open-sourced <a target="_blank"
      href="http://www.eclipse.org/sirius">Sirius</a> within Eclipse : a
    complete tooling and framework designed to build rich modeling
    environment efficiently, also designed on our experience with the
    shortcomings of the GMF Tool project. <b>Creating an Ecore modeling
      environment then became barely more work than maintaining its
      build</b>. I already had a starting point as we built an Ecore
    modeler with Sirius before, but many things in this tool were not
    quite exactly as I wanted it. That lead me to rethink all the
    interactions to provide a tool which actually assist you in design a
    <b>good</b>&nbsp;Ecore model easily.
  </p>
  <p>
    I started by inspecting the usages of <i>"EcoreTools 1"</i> or our
    own commercial modeler internally. At Obeo we are designing Ecore
    models all the time. We have customers which do need to design Ecore
    models often, we do consultancy projects in which the Ecore model is
    the backbone of all the tooling we are building, yet EcoreTools 1
    was not used at all and our commercial modeler was almost
    exclusively used to produce diagrams for the documentation and
    deliverables. And when I digged deeper, I realized the tool was just
    barely helping compared to the tree editor provided by the EMF
    Project itself, and sometime was even "getting into the way".
  </p>
  <p>
    I also wrote down scenarios of usage for the tool using <a
      href="http://en.wikipedia.org/wiki/Persona_(user_experience)">personnas</a>.
    Both these efforts have been enlightening : I could clearly identify
    usability problems and expectations regarding the tool. I came up
    with a few key points to focus on :
  </p>
  <ol>
    <li>making sure EcoreTools was providing more value to users knowing
      Ecore than the Ecore Tree Editor.</li>
    <li>making sure that EcoreTools will not make you "look bad" when
      using it with a customer.</li>
  </ol>
  <h4>Ecore Support</h4>
  <p>EcoreTools 2 obviously allows you to design Classes, Datatypes,
    References and all the classical Ecore constructions, but Ecore is
    way more than that. There are different concerns involved in
    expressing your domain model: documenting, reviewing the referencing
    mechanism of elements, identifying business constraints, exploring
    the model... Specific tools have been implemented for these concerns
    and most of them are "opt-in" in a given diagram through the
    activation of a dedicated layer.</p>
  <p>
    <a
      target="_blank" href="/community/eclipse_newsletter/2014/june/images/article2.2.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.2.png" alt="" /></a>
  </p>
  <h3>Documentation</h3>
  <p>Your Ecore model represents your domain. It's all about picking
    good names, but names are not nearly enough. Documenting your Ecore
    model is important and EcoreTools assists you with a dedicated layer
    named "Documentation" and a table editor to quickly go through all
    of your elements and document them.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.3.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.3.png" alt="" /></a>
  </p>
  <br />
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.4.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.4.png"
      width="500" alt="" /></a>
  </p>
  <br />
  <h3>Bi-directional references</h3>

  <p>With 2.0 the bidirectional references (also known as EOpposites)
    are displayed using the well known notation which comes from UML
    Class Diagram.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.5.png"
      style="margin-left: 1em; margin-right: 1em;"><img width="500"
      src="/community/eclipse_newsletter/2014/june/images/article2.5.png" alt="" /></a>
  </p>

  <br />
  <h3>Constraints</h3>
  <p>
    Domain constraints can be listed directly from the diagram once you
    activate the "Constraints" layer. EMF will then use this information
    to generate all the required plumbing so that you just have to fill
    a java method to implement the actual check.<br />
  </p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.6.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      width="600"
      src="/community/eclipse_newsletter/2014/june/images/article2.6.png" alt="" /></a>
  </p>

  <h3>Generics</h3>
  <p>Ecore has been supporting Generics for quite a few years already,
    EcoreTools allows you now to express Parameter types and use them in
    references or operations.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.7.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.7.png"
      width="400" alt="" /></a>
  </p>
  <br />
  <h3>Packages Dependency Analysis</h3>
  <p>Often domains have relationships to other domains, and this is
    reflected in Ecore models through direct referencing. But keeping
    track of those dependencies between domain packages proves to be
    hard in practice, you have to digg through every Class to see if it
    refers to something which is external. EcoreTools provides a diagram
    which is dedicated to see and analyze those dependencies.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.8.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.8.png" alt="" /></a>
  </p>
  <p>And also a dedicated decorator for classes which are from another
    package.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.9.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.9.png" alt="" /></a>
  </p>
  <br />
  <h4>Productivity</h4>
  <p>To be productive the tool needs to be at your fingertips.
    EcoreTools provides many shorctuts to make your life easier,
    especially regarding references and attributes. Typing "1" will make
    the reference or the attribute mandatory. Typing "*" will make it a
    "many". Just typing a name will only change the name, but typing ":
    someTypeName" will set the type.</p>
  <p>You can't really discover those shortcuts on your own, so feel free
    to have a look on the documentation, but once you'll know them
    you'll be quicker in designing your Ecore using the diagram editor
    compared to the classical tree editor.</p>
  <p>Many more things have been done to make sure the tool doesn't get
    into the way. All the references can be reconnected graphically, the
    modeler can be used in "full screen" with no other Eclipse views
    around while keeping a good usability, EcoreTools will take care of
    your GenModel too by reloading it when necessary.</p>
  <br />
  <h4>Design and Feedback</h4>
  <p>From a graphical point of view I tried to keep the original
    visuals, only make them slightly more appealing. Colors have been
    aligned to the Eclipse Standard palette. Classifiers now have
    rounded borders, icons and text style are used to convey the
    difference between Classes, Abstract Classes and Interfaces.</p>
  <p>Boldness is used for anything which is mandatory, blue for anything
    which is derived.</p>
  <p>These are the kind of things you can think of if you can quickly
    turn-around and try in your graphical modeler, and Eclipse Sirius
    enables that.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.10.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.10.png" alt="" /></a>
  </p>

  <p>A tool is helpful when it is giving you the right information at
    the right time. This is all about feedback.</p>
  <p>EcoreTools 2 is highlighting in red any construction which is not
    valid. You want to know that as soon as possible.</p>

  <p>But feedback is not always immediate, only if you enable the
    "documentation" layer to annotate your model, then the red borders
    will be used so that you can quickly identify elements which have no
    documentation.</p>

  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.11.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.11.png" alt="" /></a>
  </p>
  <h4>How much work was that ?</h4>
  <p>Of course it is slightly biased because I know Sirius really well.
    I was actually part of the team building it for years now so
    anything I want to do in EcoreTools I can quickly see how I'm going
    to do it leveraging Sirius.</p>
  <p>Nevertheless here is a typical change introducing the constraints
    support in EcoreTools, this is mostly about expressing, in the
    Viewpoint Specification Model of Sirius, what you want to achieve.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.12.png"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.12.png"
      height="395" width="640" alt="" /></a>
  </p>
  <br />
  <p>No code generation is involved, my plugin defining the modeler is
    just a standard Eclipse plugin, I can use JUnit, SWTbot, Tycho,
    nothing fancy here is imposed to me by Sirius. Here are a few stats
    :</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article2.13.png"
      style="margin-left: 1em; margin-right: 1em;"><img
      src="/community/eclipse_newsletter/2014/june/images/article2.13.png"
      height="152" width="640" alt="" /></a>
  </p>
  <p>The diagram and table editors of EcoreTools 2 are representing less
    than 2700 lines of code.</p>
  <br />
  <h4>Now what ?</h4>
  <p>
    This is the 2.0 version, freshly built for you. I think the net gain
    compared to the 1.x stream is already huge but of course the paint
    is slightly fresh, you might find issues and if you do<a
      href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Ecoretools">
      please report a bug</a>. If you like it, please tell me through <a
      href="https://twitter.com/bruncedric">twitter</a>,<a
      href="https://plus.google.com/+C%C3%A9dricBrun"> google plus</a>
    or the <a href="https://www.eclipse.org/forums/index.php/f/165/">Eclipse
      Forums</a>, this will be appreciated.
  </p>
  <p>
    You can install EcoreTools using the <a
      href="http://marketplace.eclipse.org/content/ecoretools-ecore-diagram-editor#.U5sZznVJXyQ">Eclipse
      Marketplace</a>, you will also find it in the <a
      href="https://www.eclipse.org/downloads/">Modeling Package </a>which
    is shipped with Luna.<br />


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/june/images/cedric.JPG"
        width="75" alt="cedric brun" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Cédric Brun<br />
        <a href="http://www.obeo.fr/">Obeo</a>
      </p>
      <ul class="author-link">
        <li><a href="http://model-driven-blogging.blogspot.fr/">Blog</a>
        </li>
        <li><a href="https://twitter.com/bruncedric">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
