<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    <a
      href="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_cloud.png"><img
      class="size-full wp-image-1134 alignleft"
      style="margin-left: 15px; margin-right: 15px;" alt="scout_cloud"
      src="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_cloud.png"
      width="336" height="164" /></a>
  </p>
  <p>This article describes the initial cloud support that was added to
    Eclipse Scout with the Luna release. In the text below, we first
    motivate our efforts in this area and describe the approach chosen
    for the Scout framework. Then, the abstraction layer to implement
    access to distributed caching and messaging is discussed. And
    finally, we list the necessary requirements for Platform as a
    Service (PaaS) offerings and provide an overview of suitable public
    PaaS provider.</p>
  <br>
  <h2>
    <strong>Motivation<br />
    </strong>
  </h2>
  <p>Since 2006, when Amazon started its Web Services (AWS) offering,
    the cloud market has become a growing multi billion dollar market.
    Following customer demand, many software vendors start to provide
    their products on a Software as a Service (SaaS) basis. For
    organisations developing in-house applications, the usage of
    Platform as a Service (PaaS) offerings can help to significantly
    reduce time and costs to develop, deploy and operate applications.
    This is especially true for startup organisations, that need to
    focus their efforts on their product and the time to market for new
    features and critical bug fixes. In addition, many PaaS offerings
    support dynamic scaling by adding additional server nodes when the
    load on the existing infrastructure increases.</p>
  <p>We are convinced that the trend to move applications to the cloud
    has only begun, and will grow even stronger in the future. With the
    Luna release we targeted public PaaS providers for Scout&#8217;s
    initial cloud support. This should minimize developers efforts to
    build scalable Scout applications and to actually run such
    application in the cloud. And because we know that you won&#8217;t
    be happy to rewrite your application when you need (or want) to
    change your PaaS provider in the future, Scout&#8217;s cloud support
    works as independent of any specific PaaS as possible. Exactly
    according to the spirit of Scout&#8217;s long term strategy.</p>
  <p>&nbsp;</p>
  <h2>How to scale Scout Applications</h2>
  <p>The classical way to scale web applications is to add/remove
    application server nodes depending on the actual load on the current
    nodes. Communication between the application&#8217;s clients to the
    server are running over a common load balancer that is distributing
    the requests from clients to the available server nodes. And the
    server nodes are accessing a common database infrastructure to store
    and retrieve data that is relevant to the specific application.</p>
  <p>This is exactly the scenario that is supported by the initial Scout
    cloud support shipped with the Luna release train. As shown in the
    figure below, a Scout application may now work with a dynamic number
    of application servers operating behind a common load balancer. The
    load balancer then distributes incoming client requests to the next
    available application server. The individual servers then use a
    common infrastructure to access a distributed cache, the message
    queue and the data base.</p>
  <p>
    <a
      href="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_cloud_architecture.png"><img
      class="alignnone  wp-image-1126" alt="scout_cloud_architecture"
      src="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_cloud_architecture.png"
      width="731" height="423" /></a>
  </p>
  <p>
    <span id="more-1124"></span>
  </p>
  <p>
    In the past, a Scout server created a <em>ServerSession</em> for
    each individual client. This server session was then used to
    identify the Scout client and to store client specific data.
    Unfortunately, the new application architecture no longer guarantees
    that subsequent requests of a specific client are handled by the
    same Scout server instance. To ensure that each client request can
    be processed correctly the server sessions are now held in a
    distributed cache that is accessed through a caching service. This
    caching service is one of the newly added components to the Scout
    framework.
  </p>
  <p>An additional change was necessary to properly handle client
    notification in scalable Scout applications. Client notifications
    are used in the Scout framework to push messages from the Scout
    server to the Scout clients. To manage the client notification
    messages, a queue is used to hold the messages that need to be
    delivered. With the scalable server architecture coming with the
    Luna release, Scout server applications hold individual queues that
    are updated from a common message queue using the Pub/Sub pattern.
    When a specific Scout server adds a client notification to the
    queue, or sends a notification to a specified client, the
    information is now distributed among all server instances using the
    newly added message queue.</p>
  <p>&nbsp;</p>
  <h2>PaaS Independence</h2>
  <p>To allow for long term planning, Scout applications should be as
    independent of individual PaaS providers as possible. Therefore, the
    Scout framework must not directly depend on the availability of
    specific distributed caching or message queues technologies. Rather,
    as shown in the diagram below, it takes advantage of its service
    oriented architecture. In the Scout runtime server bundle the
    necessary services to access caching and the message queue are
    located using the OSGi service registry.</p>
  <p>
    <a
      href="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_server_with_cache_and_messaging.png"><img
      class="alignnone  wp-image-1127"
      alt="scout_server_with_cache_and_messaging"
      src="http://www.bsiag.com/scout/wp-content/uploads/2014/06/scout_server_with_cache_and_messaging.png"
      width="760" height="361" /></a>
  </p>
  <p>&nbsp;</p>
  <p>
    By providing alternative implementations to specific caching and
    message queue technologies, the exact same server code can be used
    for different PaaS environments. To build Scout applications for a
    specific PaaS provider, the developer only needs to select matching
    service plugins and add these to the the server product. Scout
    caching support is already available on the <a
      href="https://marketplace.eclipse.org/content/cloud-support-eclipse-scout"
      target="_blank">Eclipse marketplace</a> for <a
      href="http://memcached.org/" target="_blank">memcached</a> and <a
      href="http://redis.io/" target="_blank">redis</a>. In the case of
    message queues, support is available for <a
      href="http://activemq.apache.org/" target="_blank">ActiveMQ</a>
    and <a href="http://www.rabbitmq.com/" target="_blank">RabbitMQ</a>
    . Together with the previously existing support for <a
      href="http://www.mysql.com/" target="_blank">MySQL</a> and <a
      href="http://www.postgresql.org/" target="_blank">PostgreSQL</a>
    Scout allows to implement web application that run in many different
    environments.
  </p>
  <h2>Suitable PaaS Providers</h2>
  <p>Currently, most PaaS providers agree on a number of important core
    aspects such as support for Java 1.7, or providing Tomcat
    application servers. To quickly get started with Scout cloud
    application development, we tried to compile a short list of public
    PaaS providers that also offer application hosting. And for some of
    them, we did actual trials to verify, that the cloud support with
    the Scout Luna release actually works as expected.</p>
  <p>Prior to the selection process for the short list, some hard and
    soft requirements where compiled. The goal of the list of
    requirements is to find PaaS providers that seem suitable to host
    realistically complex Scout applications including dynamic scaling
    of server nodes, and access to shared databases, message queues and
    caching. At the same time, these services should be based on open
    standards to allow for a painless migration of the application to a
    different PaaS provider.</p>
  <p>The hard requirements also include full Java 1.7 support, a servlet
    container and access to a relational database, as well as messaging
    and caching. Once these requirements are met, dynamic scaling and
    the support of open standards for databases, caching and messaging
    have been added as soft requirements.</p>
  <p>
    <strong>Hard Requirements</strong>
  </p>
  <ul>
    <li>Public PaaS with Hosting</li>
    <li>Java 1.7</li>
    <li>Servlet Container</li>
    <li>Relational Database</li>
    <li>Message Service</li>
    <li>Cache Service</li>
  </ul>
  <p>
    <strong>Soft Requirements</strong>
  </p>
  <ul>
    <li>Automatic Scaling</li>
    <li>Open Source Database</li>
    <li>Open Source Caching</li>
    <li>Open Source Messaging</li>
  </ul>
  <p>
    The overview provided below presents the short list of public PaaS
    provider. To create this shortlist, we started with the offerings
    listed of <a href="http://www.linkedin.com/in/thecloud">Khazret
      Sapenov&#8217;s</a> <a
      href="https://docs.google.com/spreadsheet/ccc?key=0AiIXCd1D_TmGdFluZEJQakV5M0QwWXNWaXREcWR0Q0E"
      target="_blank">PaaS list</a>. The hard requirements to offer Java
    support reduced the list to roughly 35 entries. Requiring public
    hosting and eliminating double entries, offerings no longer
    available reduced the list to 14 elements. The HANA Cloud Platform,
    Uhuru PaaS and the WSO2 Stratos Live were also removed from the list
    as its offerings primarily focus on supporting proprietary software
    of the vendor itself. This process resulted in the list of the 11
    &#8220;surviving&#8221; PaaS offerings.
  </p>
  <table>
    <tbody>
      <tr>
        <th>Provider</th>
        <th>Product</th>
        <th>Hard Requirements</th>
        <th>Autoscaling</th>
        <th>Other Soft Requirements</th>
      </tr>
      <tr>
        <td>Amazon</td>
        <td><a href="http://aws.amazon.com/elasticbeanstalk/"
          target="_blank">Elastic Beanstalk</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL, Memacached,
          RabbitMQ</td>
      </tr>
      <tr>
        <td>CentruyLink</td>
        <td><a href="https://www.appfog.com/" target="_blank">AppFog</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: #ffaaaa;">NOK</td>
        <td style="background-color: lightgreen;">MySQL, Redis, RabbitMQ</td>
      </tr>
      <tr>
        <td>Clever Cloud</td>
        <td><a href="https://www.clever-cloud.com/en/" target="_blank">Clever
            Cloud</a></td>
        <td style="background-color: #ffaaaa;">NOK: Message and cache
          services missing</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL</td>
      </tr>
      <tr>
        <td>CloudBees</td>
        <td><a href="http://www.cloudbees.com/run" target="_blank">Run@Cloud</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL, Memacached,
          RabbitMQ</td>
      </tr>
      <tr>
        <td>Google</td>
        <td><a href="https://cloud.google.com/products/app-engine/"
          target="_blank">AppEngine</a></td>
        <td style="background-color: #ffaaaa;">Java Sandbox only,
          Message service missing</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL, Memcached</td>
      </tr>
      <tr>
        <td>Hivext</td>
        <td><a href="http://jelastic.com/" target="_blank">Jelastic</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL, Memcached, JMS</td>
      </tr>
      <tr>
        <td>Microsoft</td>
        <td><a href="http://azure.microsoft.com/en-us/" target="_blank">Microsoft
            Azure</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: #ffaaaa;">Azure SQL Database, Azure
          Cache, Azure Service Bus</td>
      </tr>
      <tr>
        <td>Oracle</td>
        <td><a href="https://cloud.oracle.com/java" target="_blank">Cloud
            Java</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: #ffaaaa;">NOK</td>
        <td style="background-color: #ffaaaa;">Oracle Database Cloud
          Service, Coherence, Oracle Messaging Cloud Service</td>
      </tr>
      <tr>
        <td>Pivotal</td>
        <td><a href="https://www.gopivotal.com/about" target="_blank">Pivotal
            One</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: #ffaaaa;">NOK</td>
        <td style="background-color: lightgreen;">MySQL, Memcached,
          RabbitMQ</td>
      </tr>
      <tr>
        <td>Red Hat</td>
        <td><a href="https://www.openshift.com/" target="_blank">OpenShift</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: lightgreen;">MySQL, Redis, ActiveMQ</td>
      </tr>
      <tr>
        <td>Salesforce</td>
        <td><a href="https://www.heroku.com/" target="_blank">Heroku</a></td>
        <td style="background-color: lightgreen;">OK</td>
        <td style="background-color: #ffaaaa;">NOK</td>
        <td style="background-color: lightgreen;">PostgreSQL, Memcached,
          RabbitMQ</td>
      </tr>
    </tbody>
  </table>
  <br>
  <p>
    Please note that the above list is bound to be out of date almost
    immediately. Some of the offerings have been around for a while, but
    the dynamics in the PaaS space is very high. Just to name an
    example, we should have included the Stratos PaaS, as this offering
    has been donated to Apache last year and been promoted to an Apache
    top level project <a
      href="https://twitter.com/TheASF/status/473781285169938432"
      target="_blank">recently</a>.
  </p>
  <h2>Conclusions and Outlook</h2>
  <p>With the Eclipse Luna relase, Scout adds initial cloud support for
    building enterprise applications. This support makes writing
    scalable business applications as simple as possible. And with
    Scout&#8217;s abstraction layer for databases, caching and
    messaging, Scout applications become de-facto independent of
    specific PaaS provider. This let&#8217;s you focus on implementing
    functionality and removes the worry of selecting the
    &#8220;right&#8221; PaaS provider.</p>
  <p>When working with Scout desktop clients, complete scaling is
    already supported with the Scout Luna release. A logical next step
    is to add support for scaling the web servers. As the Scout web
    servers are running the web-clients of Scout web applications, this
    addition will then allow to scale complete Scout web applications.</p>
  <h2>Further Reading</h2>
  <ul>
    <li><a href="https://www.eclipsecon.org/na2014/session/scout-cloud"
      target="_blank">Scout in the Cloud</a> session at EclipseCon, San
      Francisco, 2014</li>
    <li><a
      href="http://searchcloudapplications.techtarget.com/news/2240216336/Eclipse-Scout-Open-source-framework-offers-enterprise-cloud-support"
      target="_blank">Techtarget interview</a> by <i>Caroline de
        Lacvivier</i>, 18 Mar 2014</li>
    <li><a href="https://wiki.eclipse.org/Scout/Tutorial/4.0/Cloud"
      target="_blank">Wiki Tutorial</a> to add Scout cloud support (work
      in progress)</li>
    <li>Other: <a href="http://eclipse.org/scout/">Project Home</a>, <a
      href="http://www.eclipse.org/forums/index.php?t=thread&amp;frm_id=174">Forum</a>,
      <a href="http://wiki.eclipse.org/Scout">Wiki</a>, <a
      href="https://twitter.com/#!/EclipseScout">Twitter</a></li>
  </ul>
  <h2>
    <i>Acknowlegements</i>
  </h2>
  <p>
    <i>This article is based on the master thesis of Thomas Schweigler,
      that has been also supervised by Scout committer Judith Gull. We
      expect to be able to provide access to the full thesis (in German)
      in the near future. </i><i>The cloud icon used at the beginning is
      available at <a href="http://www.iconarchive.com/">www.iconarchive.com</a>.
      According to its <a
      href="http://www.iconarchive.com/show/ios7-style-metro-ui-icons-by-igh0zt/MetroUI-Apps-iCloud-icon.html">terms</a>,
      the icon can be used freely for non-commercial use.
    </i>
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/june/images/mzi3.png"
        width="75" alt="Matthias Zimmermann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Matthias Zimmermann<br />
        <a target="_blank" href="http://www.bsiag.com/">BSI</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.bsiag.com/scout/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/EclipseScout">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
