<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a56e-a90d-cf2b-8085421c97c9">We are
    happy to announce that EMF Forms (and the parent project EMF Client
    Platform) joins the Eclipse Luna release train and will be available
    as part of the Eclipse Modeling Tools package. As we are new on
    board, I would like to introduce EMF Forms a bit, especially the new
    IDE tooling, which is introduced with the new 1.3.0 release.</p>
  <p dir="ltr">
    EMF Forms is a framework to efficiently develop form-based UIs based
    on a given data model. Instead of manually coding these in SWT,
    JavaFX or a web framework, the UI is described in a simple view
    model. This view model is then interpreted by an adaptable and
    exchangeable rendering component. This even allows you to render the
    same UI using different UI technologies. The approach is very
    similar to the e4 Application model, but instead of the workbench
    layout, EMF Forms view models describe the content of the views. It
    therefore perfectly combines with e4 (but also with 3.x). Please see
    <a
      href="http://eclipsesource.com/blogs/tutorials/getting-started-with-EMF-Forms/">here</a>
    for a comparison between manual UI programming and the approach of
    EMF Forms. Please see <a
      href="http://eclipsesource.com/blogs/2014/04/15/emf-forms-a-question-of-effort/">here</a>
    for a more detailed description of EMF Forms. <a
      href="/community/eclipse_newsletter/2014/june/images/article4.1.png"><img
      class="alignnone size-full wp-image-20925"
      alt="image06 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.1.png"
      width="700" title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <h1 dir="ltr">IDE Tooling</h1>
  <p dir="ltr">The most prominent new feature for the Eclipse Luna
    release is the new IDE integration for view models. View models
    describe the form-based UI of data entities, which should be
    displayed and modified in an application. During runtime, the
    rendering component of EMF Forms translates these view models into
    the actual UI. View models are EMF models and are deployed as XMI
    files in extra bundles along with the application. Every view model
    describes the form-based UI of one entity and optionally some
    sub-entities.</p>
  <p>In the past, developers needed to start a dedicated application to
    create and modify view models. In Eclipse Luna, view models can be
    created and edited directly within the IDE. In the following, we use
    an example for a domain model, i.e., the data model containing the
    entities, we want to display in a form-based UI. The example is
    called “Make it Happen!” and is the data model of a simple task
    management application. In the following, we will mainly focus on
    the data entity “User” and demonstrate how to use the new tooling to
    create a form-based UI for it.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.2.png"><img
      class="alignnone size-full wp-image-20920"
      alt="image011 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.2.png"
      width="219" height="188"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a570-e7e0-a7f6-716157b6c757">The
    complete “Make it Happen!” example contains two more entities, Tasks
    (which can be assigned to users) and UserGroups.</p>
  <p dir="ltr">If you want to use this model to try out EMF Forms, the
    Luna release allows you to import it directly as source code into
    your workspace (New =&gt; Example =&gt; EMF Forms =&gt; Make it
    Happen: example model). Of course, you can also try the new tooling
    with your own EMF model.</p>
  <p dir="ltr">To create a new view model, you can use the “New Wizard”
    in the Eclipse IDE, too. Alternatively, there is a shortcut. To
    create a view model for a certain entity, you can right-click on the
    Ecore file defining the entity and select either:</p>
  <ul>
    <li dir="ltr">
      <p dir="ltr">Create new view model: Creates a new view model into
        an existing bundle</p>
    </li>
    <li dir="ltr">
      <p dir="ltr">Create new view model project: Creates a new view
        model and a bundle containing it (view models are ideally
        deployed in separate bundles)</p>
    </li>
  </ul>
  <p dir="ltr">In the example model, the Ecore is located here:
    “org.eclipse.emf.ecp.makeithappen.model/model/task.ecore”</p>
  <p dir="ltr">Subsequently, the wizard allows you to select for which
    entity a view model shall be created (e.g., “User”) and whether the
    view model should also be filled with a sample UI (displaying all
    attributes of an entity).</p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a571-8f7f-56f6-58eed94d04ff">The
    created view model is now opened by the new View Model Editor. It
    works very similarly to other Eclipse Editor. On the left site, a
    hierachy of the current model is shown as a tree. New elements can
    be created by right-clicking on the parent element and the hierarchy
    can be changed using drag and drop. When selecting an element in the
    tree, its properties are shown on the right side. The View Model
    Editor also provides a preview, which can be opened with the button
    on the top right of the editor. The preview shows the result of the
    current view model rendered in SWT. It is a very useful tool to
    check the form-based UI without needing to start a full application.
    When the view model is changed, the preview needs to be updated
    (green arrow to the top right of the preview). Alternatively the
    preview can be set to auto-update.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.3.png"><img
      class="alignnone size-full wp-image-20924"
      alt="image05 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.3.png"
      width="515" height="426"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a572-320e-5fb2-57431a3d822a">The
    initial view model contains only two types of view model elements.
    The root element of a view model is always a “view”. It defines the
    UI of the entity type of the domain model described by the view
    model. Contained in the view, there are controls. Controls map to
    attributes of the underlying domain entity. Controls are rendered as
    widgets showing the content of attributes and allowing modification,
    e.g., a text field for a string attribute. The widget is created by
    the associated control renderer. EMF Forms ships out-of-the-box with
    a control renderer for all basic data types. Custom widgets can be
    used in the form-based UI, too, by extending or replacing the
    control renderer.</p>
  <p dir="ltr">The initial view model created by the wizard contains
    exactly one control per attribute of the underlying domain model,
    more precisely, of the entity “User”. It is therefore a good base to
    refine the layout of the user view. On the left side of the view
    model editor, you can see the view model displayed as a tree. By
    right-clicking a parent element, you can add new elements to it.
    Existing elements can be moved using drag and drop. In general,
    there are two different types of elements. Container elements, e.g.,
    the view itself or a group, can contain other elements as children.
    Thereby, a hierarchy is defined that will be translated into a
    layout by the renderers.</p>
  <p dir="ltr">The second type of element is a control that specifies
    the widgets displayed within this layout. The easiest way to create
    new controls is to right-click a container element you want to
    create controls in (e.g., the view itself) and select “Generate
    Controls”. A wizard allows you to select which attributes you want
    to create controls for.</p>
  <p dir="ltr">The following screenshot shows an example for a refined
    layout. The view contains a “HorizontalLayout”, which arranges its
    children horizontally. Within this layout, we defined two groups,
    “Important” and “LessImportant”. The groups contain the controls.
    The controls can either be moved from the initial location (the
    view) or be generated directly in the groups (“Generate Controls”).
    The screenshot also shows the preview with the resulting layout.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.4.png"><img
      class="alignnone size-full wp-image-20922"
      alt="image031 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.4.png"
      width="711" height="625"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a573-2d73-deda-00353a828cfa">The
    screenshot also shows a table directly below the two groups. This
    table shows a list of tasks (another entity of the “Make it Happen!”
    example model) that are assigned to the user.</p>
  <p dir="ltr">
    This table is also described within the view model. The element
    TableControl defines which sub-elements or referenced elements of
    the entity User should be displayed in the table, e.g., the tasks in
    this example. It also defines which columns shall be displayed, in
    this case “Name”, “Description”, “Due Date” and “Done”. Tables are
    used very often. For the 1.3.0 release, we extended the table
    implementations with many new features such as the ability to
    configure whether entries inline are edited or appear in a separate
    dialog. More information on tables and other available view model
    elements can be found <a
      href="http://eclipsesource.com/blogs/tutorials/emf-forms-view-model-elements/">here</a>.
  </p>
  <h2 dir="ltr">Embedding EMF Forms</h2>
  <p dir="ltr">The form-based UIs created by EMF Forms can now be
    embedded anywhere within an existing application. When using the SWT
    renderer, the following line of code will create a fully functional
    form-based UI for a domain entity. The first parameter is the parent
    SWT composite the view should be rendered on. The second parameter
    is the entity object that should be displayed in this UI, e.g., an
    instance of the class user. The view model is by default retrieved
    from a registry based on the type. Alternatively, you can pass it to
    the renderer explicitly.</p>

  <pre>ECPSWTViewRenderer.INSTANCE.render(Composite parent, EObject domainObject);</pre>

  <p dir="ltr">
    If you do not have your own application yet but still want to try
    out the result of EMF Forms, the EMF Client Platform ships with a
    demo application already embedding EMF Forms as an editor, as shown
    in the following screenshot. The demo application also supports
    features such as creating elements or storing them on a server.
    Please find a description of how to start this demo application <a
      href="http://eclipsesource.com/blogs/tutorials/getting-started-with-EMF-Forms/">here</a>.
    With the new Luna release, this demo application is also available
    as a native e4 application. Therefore, EMF Forms is fully compatible
    with e4. Additionally, since release 1.2.0, EMF Forms also fully
    supports building applications with the <a
      href="http://eclipse.org/rap">Remote Application Platform (RAP)</a>,
    and is therefore also available for Web and mobile applications.
  </p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.5.png"><img
      class="alignnone size-full wp-image-20926"
      alt="image07 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.5.png"
      width="823" height="419"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <h2 dir="ltr">More elements?</h2>
  <p dir="ltr">The Luna release also introduces a couple of new view
    model elements to cover typical requirements for more complex
    form-based UIs. For example, if there are a large number of
    attributes to be displayed, the controls must often be separated
    over several pages in the UI. For this, EMF Forms offers
    “Categorizations”. Categorizations are sub-parts of the UI, and
    every category contains a dedicated layout and a number of
    attributes. Based on the configuration, categories are rendered as
    Tabs, as a master detail view or even as both.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.6.png"><img
      class="alignnone size-full wp-image-20927"
      alt="image08 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.6.png"
      width="336" height="87"
      title="EMF Forms Joins the Eclipse Release Train!" /></a> <a
      href="/community/eclipse_newsletter/2014/june/images/article4.7.png"><img
      class="alignnone size-full wp-image-20921"
      alt="image021 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.7.png"
      width="256" height="114"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a576-0887-9388-32115e461b87">If you
    do not want to display only one root element in a form-based UI, EMF
    Forms offers two options. Values of sub-elements can be embedded as
    controls or the hierarchy of an element can be shown in a tree
    master detail view, allowing you to create sub-elements and browse
    through the tree. This element of EMF Forms is used in the view
    model editor itself and can be seen in the previous screenshot in
    the section IDE Tooling</p>
  <p dir="ltr">Besides layout elements, form-based UIs often require you
    to set the visibility or enablement of certain parts of the UI based
    on some rules. For example, the visibility of a text field could
    depend on a value set previously in a drop-down box. These kinds of
    rules can also be expressed within the view model and will be
    interpreted by the renderer.</p>
  <p dir="ltr">
    EMF Forms supports even more elements. The growing list can be found
    <a
      href="http://eclipsesource.com/blogs/tutorials/emf-forms-view-model-elements/">here</a>.
    If you find something missing, we do provide the extension of EMF
    Forms as a service as well as <a
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> for your team.
  </p>
  <h2 dir="ltr">Adaptations and extensions</h2>
  <p dir="ltr">EMF Forms ships with default view model elements and
    renderers. However, in many cases, a project wants to adapt the way
    a view is described or rendered. Therefore, EMF Forms supports many
    ways of being extended and adapted. A very common use case is the
    adaptation or replacement of renderers. In so doing, you can
    influence how certain view model elements are rendered in the
    form-based UI. For example, by adding new control renderers, you can
    add custom widgets to the existing UI, e.g., a text field supporting
    auto-completion.</p>
  <p dir="ltr">The renderer of layout elements can also be extended or
    replaced. The following code example implements a renderer for the
    view model element group. While the default renderer uses a SWT
    group, this example uses a PGroup (Nebula Project).</p>

  <pre>public class PGroupRenderer extends ContainerSWTRenderer&lt;VGroup&gt; {
    @Override
    protected Composite getComposite(Composite parent) {
        parent.setBackgroundMode(SWT.INHERIT_FORCE);
        PGroup group = new PGroup(parent, SWT.SMOOTH);
        if (getVElement().getName() != null) {
            group.setText(getVElement().getName());
        }
        return group;
    }
}

  </pre>

  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a577-02ed-831d-ff675f92e974">When
    adding this custom renderer, a group will be rendered as shown in
    the following screenshot.</p>
  <p dir="ltr">
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.8.png"><img
      class="alignnone size-full wp-image-20919"
      alt="image001 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.8.png"
      width="328" height="182"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p dir="ltr"
    id="docs-internal-guid-8af3a530-a577-7f8e-f4f0-f2495ec52b4f">The IDE
    Tooling in Luna allows you to import two example bundles containing
    custom renderers as sources into your workspace ( New =&gt; Example
    =&gt; EMF Forms). These renderers can be used as a template for the
    implementation of own renderers.</p>
  <p dir="ltr">In addition to the adaptation of renders, even the view
    model itself can be extended by new elements. The goal is to find
    the best fitting concepts to describe a UI as concise and
    understandable as possible. For your own view model elements, a
    renderer has to be implemented, too.</p>
  <p dir="ltr">
    There are many more ways to extend EMF Forms, please refer to our <a
      href="http://http//eclipse.org/ecp/emfforms/documentation.html">documentation</a>.
    If you have open questions about EMF Forms or need help to extend
    it, we provide training and <a
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> for these purposes.
  </p>
  <h2 dir="ltr">More Renderer capabilities</h2>
  <p dir="ltr">The active development of EMF Forms will be continued in
    several areas. For example, the team is working on a JavaFX renderer
    to translate a view model into the Swing successor instead of SWT.
    So just by exchanging the renderer, you will be able to switch all
    your form-based UIs to a new UI Toolkit. Besides JavaFX, we are
    currently also working on adding EMF Forms to the mobile framework
    Tabris to enable form-based UIs even on smartphones and tablets.
    Tabris has a SWT compatible UI, however, different screen sizes
    require some adaptations. Finally, we have started to implement a
    pure web version of EMF Forms using native Web technologies such as
    JSON, HTML5, JavaScript and AngularJS to render the forms.</p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.9.png"><img
      class="aligncenter size-medium wp-image-20932"
      alt="Abbildung 6a 300x217 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.9.png"
      width="300" height="217"
      title="EMF Forms Joins the Eclipse Release Train!" /></a>
  </p>
  <p>
    <a
      href="/community/eclipse_newsletter/2014/june/images/article4.10.png"><img
      class="aligncenter size-medium wp-image-20933"
      alt="Abbildung 6b 169x300 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.10.png"
      width="169" height="300"
      title="EMF Forms Joins the Eclipse Release Train!" /><br></a><img
      class="aligncenter size-thumbnail wp-image-20931"
      alt="Abbildung 6c 150x150 EMF Forms Joins the Eclipse Release Train!"
      src="/community/eclipse_newsletter/2014/june/images/article4.11.png"
      width="150" height="150"
      title="EMF Forms Joins the Eclipse Release Train!" />
  </p>
  <h2 dir="ltr">Conclusion</h2>
  <p dir="ltr">The Luna release of EMF Forms mainly delivers new tooling
    to create and modify view models directly within the Eclipse IDE.
    Additionally, the available view model elements have been extended
    and the rendering architecture has been improved, making it even
    simpler than before to extend the framework. Even more features are
    in the pipeline. Since version 1.2.0, EMF Forms has supported the
    capability to influence the look of UI elements using a second model
    (similar to CSS). We are working on supporting this model with the
    IDE tooling in the future.</p>
  <p dir="ltr">
    As with every open source project, we are always interested in any
    feedback. Only by getting new ideas, Bug Reports or feature request,
    can we continuously improve the framework. If you are missing
    anything, please do not hesitate <a
      href="https://www.eclipse.org/ecp/emfforms/communication.html">to
      report a bug, ask a question in the newsgroup or contact us</a>.
  </p>
  <p dir="ltr">
    If you need support with embedding EMF Forms into your application,
    if you are interested in an evaluation or you want to adapt the
    framework to your needs, we provide <a
      href="http://eclipsesource.com/en/services/developer-support/">development
      support</a> and training for these purposes.
  </p>
  <p dir="ltr">
    Please visit <a
      href="http://eclipse.org/emfclient/emfforms/documentation.html">this
      site</a> to learn more about other EMF Forms features.
  </p>
  <p dir="ltr">
    Please visit <a
      href="http://eclipse.org/emfclient/documentation.html">this site</a>
    to learn more about other ECP features.
  </p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/jhelming.jpg"
        alt="jonas helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jonas Helming <br />
        <a href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>
        <li><a href="https://twitter.com/JonasHelming">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
