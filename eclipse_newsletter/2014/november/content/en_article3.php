<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>
      The <a target="_blank" href="http://tools.jboss.org">JBoss Tools</a>
      team wanted to get good support for doing development with Apache
      Cordova in Eclipse. Apache Cordova offers a hybrid mobile
      development model where you are using HTML5 technologies mixed in
      with a native container on the mobile device. This container gives
      you access to native features like camera, microphone, motion
      sensor etc. while just using one codebase instead of having
      different one for Android, iOS, etc.
    </p>

    <p>
      When starting, we looked at the available tools for developing
      Cordova applications. We found out that there were no open source
      solutions that we could contribute and use as part of our tools.
      Furthermore, interoperability among what very little existed was
      poor. Of course, our main goal is <i>creating good tools for
        Apache Cordova development</i>, but while doing that we always
      keep an eye on interoperability and extendibility.
    </p>

    <p>
      That work started in 2013 and in July 2014 we moved the
      development of our tools for Cordova based application development
      and formed the Eclipse <a href="http://www.eclipse.org/thym">THyM</a>
      project.
    </p>

    <h2>Trying out THym today</h2>

    <p>THym is planned to be part of Eclipse Mars release in 2015.</p>

    <p>THym includes Project wizards, project management, plugin
      discovery, and support for running iOS and Android simulators.</p>

    <p>
      You can use <a href="http://download.eclipse.org/thym/snapshots">THym
        update site</a> directly, but in the walkthrough below we will
      JBoss Developer Studio which have Thym included and some extra
      value add-ons like the Cordova Simulator.
    </p>

    <p>
      JBoss Developer Studio 8 and JBoss Tools 4.2 release enables
      developers to use these tools to build mobile applications by
      using the <a target="_blank" href="http://cordova.apache.org">Apache
        Cordova</a> framework. Here, we will explore different scenarios
      for getting started with the tools and Apache Cordova.
    </p>

    <h2>Where are the tools?</h2>

    <p>
      Once you have <a target="_blank"
        href="https://www.jboss.org/download-manager/file/jboss-devstudio-8.0.0.GA-jar_universal.jar">
        downloaded and installed JBoss Developer Studio 8</a> or
      installed from <a
        href="http://marketplace.eclipse.org/content/red-hat-jboss-developer-studio-luna">Eclipse
        Marketplace</a> Hybrid mobile tools are available from JBoss
      Central to pull them in. Here is how.
    </p>

    <p>
      First switch to
      <code>Software/Update</code>
      tab on JBoss Central. Next select
      <code>JBoss Hybrid Mobile Tools + CordovaSim</code>
      . You can select the
      <code>AngularJS for Web Tools</code>
      option before hitting the btn:[Install/Update], in order to get
      the improved support for <a href="http://ionicframework.com">Ionic
        framework</a>.


    <p>Here is a screeen cast for getting started with the hybrid mobile
      tools.</p>

    <iframe width="420" height="315"
      src="//www.youtube.com/embed/H5ry5WpziVw"
      allowfullscreen></iframe>

    <h2>1. Start a new Cordova project</h2>

    <p>
      This is the option when starting a new project or just
      experimenting with Apache Cordova development.It gives a basic
      project template that you can easily change and you can select the
      initial set of cordova plug-ins to include into your project. I
      usually add the <a target="_blank"
        href="http://plugins.cordova.io/#/package/org.apache.cordova.console">console</a>
      plugin at this time, to be able to use
      <code>console.log()</code>
      during development and remove it before the final export.
    </p>

    <p>The video above not only shows how a project is created but also
      introduces the tools at your disposal going forward.</p>

    <h2>2. Import your existing Cordova application</h2>

    <p>
      If your Cordova application already exists and it was developed
      using a compatible tool such as Cordova CLI, you can import it
      easily and start using all the functionality available. Import
      process will also restore your plugins if you have been using the
      new
      <code>cordova save</code>
      command.
    </p>

    <p>Here is a short video that shows the import feature in action.</p>

    <iframe width="420" height="315"
      src="//www.youtube.com/embed/E0YdJNdnOYk"
      allowfullscreen></iframe>

    <h2>3. Use existing mobile web site code</h2>

    <p>Often getting started with your packaged mobile application
      requires sharing the assets with a web application. We do support
      linking of resources from different locations to create a Cordova
      based mobile application. Although I do not recommend using
      artifacts, let alone whole web sites, that were created for
      desktop to be turned to mobile applications, but it helps to know
      that your tools are capable when it gets desperate.</p>

    <iframe width="420" height="315"
      src="//www.youtube.com/embed/rrU-j5hrh3k"
      allowfullscreen></iframe>

    <h2>Where is the code</h2>

    <p>
      The development for THyM happens on GitHub in the <a
        target="_blank" href="https://github.com/eclipse/thym">https://github.com/eclipse/thym</a>repository.
    </p>

    <p>
      We use <a
        href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Thym">bugzilla</a>,
      and <a href="https://dev.eclipse.org/mailman/listinfo/thym-dev">thym-dev</a>
      mailing list and project documentation is at the <a
        href="https://wiki.eclipse.org/Thym">wiki</a>. The builds will
      be running on eclipse.org build server <a
        href="https://hudson.eclipse.org/thym/">instance</a>.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/november/images/Max_Rydahl_Andersen.jpg"
        width="90" alt="max rydahl andersen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Max Rydahl Andersen<br />
        <a target="_blank" href="http://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://tools.jboss.org/blog">Blog</a></li>
        <li><a target="_blank" href="http://twitter.com/maxandersen">Twitter</a></li>
        <li><a target="_blank"
          href="https://www.linkedin.com/pub/max-rydahl-andersen">Lindedin</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/november/images/gorkem.jpg"
        alt="ed merks" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Gorkem Ercan<br />
        <a target="_blank" href="http://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.gorkem-ercan.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/gorkemercan">Twitter</a></li>
        <li><a target="_blank"
          href="https://www.linkedin.com/in/gorkemercan">Linkedin</a></li>
        <!-- $og  -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
