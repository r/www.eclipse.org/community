<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>If there were one best way to do everything, and everyone agreed,
      life would be simple. But life isn't simple. Each person has an
      opinion on what's best. When we provide user interfaces, we
      generally try to take that into account. I recall well my
      corporate days designing user interfaces with the involvement of
      usability specialists. The specialists of course have their own
      opinions, often contrary to the developers' opinions. But they are
      professionals so they can back their opinions up with facts,
      including with usability studies involving real live users using
      the software. The basic premise is that the user is king. What the
      user says is confusing, is by definition confusing. What the user
      says she wants, she should get. Of course as soon as you have two
      users they will contradict each other, but each is nevertheless
      king of the world and necessarily correct.</p>
    <p>This is the situation where preferences seem to be a great
      solution. Just support both desired approaches and let each user
      choose what they prefer. Unfortunately, that introduces five new
      problems.</p>

    <ol>
      <li>Where to find the preference?</li>
      <li>What does the preference mean?</li>
      <li>What should be the preference default?</li>
      <li>Why heck are there 1000 preferences?</li>
      <li>Do the preferences interact sensibly?</li>
    </ol>

    <p>Welcome to a world where everyone has an opinion. There's simply
      no pleasing everyone. Take careful note too that even if you
      provide all the possible preferences that should in theory please
      anyone and everyone, that still isn't going to do the trick,
      because it will be confusing and overwhelming for the new user.</p>
    <p>Eclipse's rich offering suffers from all these problems in
      spades. There are just way too many preferences, some of the
      defaults are arguably wrong, and, worst of all, Eclipse's
      preference architecture simply does not match the actual needs.</p>
    <p>The astute Eclipse user will realize that preferences are scoped,
      but I'm willing to bet very few users, or even Eclipse developers
      themselves for that matter, fully understand the preference
      architecture. If you're interested, Oomph provides a preference
      tool that allows you to explore all Eclipse's preferences.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/november/images/novarticle2.1.png"><img
        src="/community/eclipse_newsletter/2014/november/images/novarticle2.1.png"
        width="400" alt="" /></a>
    </p>
    <br />
    <p>I'll bet you didn't know it was this complex!</p>
    <p>The instance preference scope is the one you likely all know and
      love (to hate). Most of the preferences you change in Eclipse's
      Preference dialog are of this scope, i.e., are specific to the
      current workspace. But ask yourself, if I want line numbers for my
      editors in one workspace, isn't it highly likely I'll want them in
      all my workspaces? In fact, isn't that the case for most of the
      preferences you change? So why do I have to set them for each
      workspace? Of course there are exceptions, for example, the name
      of the workspace. Did you know you can set the name of the
      workspace? On some operating systems, that even makes the task bar
      more useful, i.e., you can tell all your running Eclipse instances
      apart. Of course that wouldn't be useful if every workspace has
      the same name, would it? So all in all, instance scope is actually
      very annoying and doesn't generally fit well with our needs.</p>
    <p>An extremely powerful capability that's badly under used is
      project-scoped preferences. For example, you can set all your Java
      preferences on a per-project basis. This way, every workspace that
      contains those projects will have the appropriate preferences,
      eliminating the need for instance scoped preferences. Moreover,
      different projects can have different preferences. For example, if
      you have Java projects with generated code, you probably will need
      to have different preferences for those, because the code is
      likely not generated to match your personal preferences. So that's
      all great, but what happens if you have 100 projects? Given that
      JDT has 1000 preferences, this means you'll be setting, 100,000
      preferences. No one does that, except those with obsessive
      compulsive disorder. Yes, okay, people find reasonable hacks,
      e.g., scripts to copy the *.pref files from one project to
      another, but that's still a pain should you ever need to change
      them, or when JDT introduces new preferences, which it does every
      release. So most people just use instance scope instead.</p>
    <p>Oomph aims to address these problems. To deal with the
      preferences that severely traumatize the common user, Oomph has an
      introductory questionnaire to make users aware of some of the most
      likely things they’ll want to configure.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2014/november/images/novarticle2.2.png"><img
        src="/community/eclipse_newsletter/2014/november/images/novarticle2.2.png"
        width="400" alt="" /></a>
    </p>
    <br>
    <p>We picked a few of the most commonly stated reasons why people
      hate Eclipse that can simply be preferred away. The questionnaire
      lets the user choose, once and for all, which preference she likes
      best, e.g., line numbers or not? I hate them, but many of you
      probably love them. Why shouldn't we all be happy?</p>
    <p>The preference choices are recorded as Oomph tasks that are
      applied to each and every workspace opened with an IDE in which
      Oomph is installed. But that's not all; with Oomph installed the
      preference dialog itself will have a record button.</p>


    <p>
      <a
        href="/community/eclipse_newsletter/2014/november/images/novarticle2.3.png"><img
        src="/community/eclipse_newsletter/2014/november/images/novarticle2.3.png"
        width="600" alt="" /></a>
    </p>
    <br>

    <p>As such, all changes the user ever makes can be recorded as Oomph
      tasks that are uniformly applied to all workspaces. Before the
      task is recorded, the user is asked if in fact she wants that
      preference to be the same in all workspaces. If so, she can be
      sure she'll never need to set it again. If not, she won't be asked
      about it again. Of course the user can turn the recorder off, too,
      if she prefers to manage things the old fashioned way.</p>
    <p>Also in the works, though not yet ready for prime time, is
      Oomph's Project Configuration tool. It helps manage
      project-specific preferences. The basic idea is that all
      project-specific preferences should be managed by preference
      profiles. The tool identifies unmanaged preferences and helps set
      up the management infrastructure for them. For example, if a Java
      project has project-specific Java preferences---all Java Projects
      ought to have them though unfortunately most don't---that project
      should either be the source for those preferences or should be a
      target for those preferences. So the user picks one Java project
      as having a "JDT Preference Profile" and identifies which
      preferences are parts of that profile using patterns. Then she
      specifies the rule for which other projects should have exactly
      those same preferences, for example, a rule that all projects in
      the same repository and with a Java Nature should have those same
      preferences.</p>
    <p>A preference listener listens to any changes to project-specific
      preferences, and automatically propagates them to all the target
      projects. Create a new Java project in your repository, and it
      automatically has the right preferences. The same generic approach
      is applicable for all project-specific preferences. With this
      approach project-specific preferences can easily be managed for
      large numbers of projects.</p>
    <p>We hope such a tool will help encourage more widespread use of
      project-specific preferences. This will make it easier to
      contribute to projects because the projects themselves will
      specify things like formatting rules and compiler preferences to
      ensure that contributed code conforms to the project's guidelines.
      By making these types of things project-specific, contributors can
      work on many projects in the same workspace without disagreement
      on what the workspace's overall instance preferences should be;
      those preferences should reflect the user's personal choices, not
      the project's choices.</p>
    <p>We believe that the sum total of Oomph's technologies tames the
      savage preference beast. That it makes the life of Eclipse users
      and Eclipse developers easier and more enjoyable, so they can
      focus on creativity, undistracted by battles to whip Eclipse into
      the shape they want.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/may/images/Eike_Stepper2.jpg"
        width="90" alt="eike stepper" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Eike Stepper<br />
        <a target="_blank" href="http://www.esc-net.de/">ES-Computersysteme</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://thegordian.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/eikestepper">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/november/images/Edmerks.JPG"
        alt="ed merks" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ed Merks<br />
        <a target="_blank" href="http://www.itemis.com/">itemis</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://ed-merks.blogspot.de/">Blog</a></li>
        <!--<li><a target="_blank" href="https://twitter.com/rbioteau">Twitter</a></li>
          <li><a target="_blank" href="https://plus.google.com/116635061773096754601/posts">Google +</a></li>
          $og  -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


