<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Our vision is to build leading desktop and cloud-based development
  solutions, but more importantly to offer a seamless development
  experience across them. Our goal is to ensure that developers will
  have the ability to build, deploy, and manage their assets using the
  device, location and platform best suited for the job at hand.</p>
<p>Cloud-based development is becoming a reality, but with an estimated
  six million users, leveraging thirty years of investment in the
  desktop, Eclipse will remain a significant part of the developer’s
  platform for the foreseeable future. With great Java™ 8 support,
  innovative refactoring, static analysis, and tight integration with
  services like source code management tools and issue trackers, desktop
  Eclipse has a lot to offer.</p>
<p>I know what you’re thinking: thirty years? Yes, thirty. Eclipse
  itself has been open source for almost a decade and a half, but it
  leverages the skills and experience of a team that has been building
  tools for developers for a very long time. Eclipse’s modular
  architecture makes it very easy to extend, enabling a vast ecosystem
  to provide all sorts of solutions to every sort of problem that you
  can imagine. While this architecture is modular and loosely coupled,
  the do-it-all locally nature of plug-ins tends to result in a rather
  large footprint on the desktop: everything that you need is in the
  desktop in a single (albeit loosely-coupled) application.</p>
<p>Orion takes a different approach. Orion has been quietly building
  steam in the browser-based web (JavaScript, HTML, CSS, etc.)
  development space. Rather than take control of the developer
  experience, Orion manifests comfortably as just another tab in your
  browser. Where desktop Eclipse integrates with external services like
  issue trackers and code review services using tightly integrated (but
  loosely coupled) Eclipse-specific views and editors, Orion leverages
  the existing web-based interfaces. Your code editors exist as a tabs
  in the browser, sitting beside other tabs and windows open on your
  issue tracker, code review, Git repositories, and more.</p>
<p>The Che project—recently added as a new project by the Eclipse
  Foundation—takes on development in the cloud. Che leverages a modular
  micro-services based architecture to deliver a complete browser-based
  development experience with support for writing, compiling, building,
  and testing applications of diverse collections of technology directly
  in the browser. Che strikes a balance between Eclipse and Orion: like
  Eclipse, Che provides a comprehensive and immersive development
  experience; but like Orion, Che leverages distributed services (SaaS).
  In fact, Che actually uses parts of Orion.</p>
<p>We will continue to reap the dividends of the effort we’ve invested
  in desktop IDE for years to come. Web- and cloud-based developer tools
  are emerging as a real alternative, but there’s room for both and it’s
  important that we make the ability to choose the right set of tools
  for the job as easy as possible. Moreover, it needs to be possible to
  “mix and match” solutions and move from one to the other and back
  again. This will take time and coordination between our project teams.</p>
<p>Over the next three years, the out-of-the-box experience for desktop
  Eclipse will improve. With the new Oomph project, we have technology
  that makes it easy to install and configure Eclipse (we’ll deliver
  this as part of the Mars simultaneous release in 2015). The Oomph
  launcher knows how to detect whether or not an appropriate JRE is
  available and provide instructions if one cannot found. It sounds like
  a little thing, but this will be a huge improvement in the initial
  experience for a great many users.</p>
<p>Continued focus on quality and performance, out-of-the-box
  experience, Java 9, and first class Maven and Gradle support also
  figure prominently in our vision of a powerful developer’s platform.</p>
<p>Eclipse projects, the community, and ecosystem will all continue to
  invest in and grow desktop Eclipse. Full-function cloud-based
  developer tools delivered in the browser will emerge and revolutionize
  software development. Developers will be able to switch between and
  mix-and-match their tool platform as required. Quality, performance,
  functionality, and out-of-the-box experience will to continue to
  improve. This is our vision of the Eclipse developer’s platform over
  the next three years and the foreseeable future.</p>

<h3>References</h3>
<ul>
  <li><a href="http://www.eclipse.org/eclipse">The Eclipse Project</a></li>
  <li><a href="http://www.eclipse.org/orion ">Orion</a></li>
  <li><a href="http://www.eclipse.org/che">Che</a></li>
  <li><a href="https://www.eclipse.org/oomph">Oomph</a></li>
</ul>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/Wayne.jpg"
        width="75" alt="wayne beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Wayne Beaton<br />
        <a href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://waynebeaton.wordpress.com/">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
        <!--<li><a href="https://twitter.com/noopur2507">Google+</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

