<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Simon Kaegi";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2014/november">November 2014</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I'm a software engineer at the IBM Ottawa Software Lab working on developer tools and open standards. I've been actively involved in the Eclipse community for more than ten years and these days co-lead the Orion project and serve on the PMC of the newly created Eclipse Cloud Development top-level project. Inside of IBM I'm the architect of the Orion-based Web IDE we use for Bluemix (IBM's cloud platform) and represent the company for the development of the JavaScript Language (ECMA TC39) and browser API (W3C Webapps).</p>
				
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I've been using Eclipse since close to the very beginning. Back in late 2001 / early 2002 I was in grad-school and my advisor gave me (as I recall) an Eclipse 2.0 beta CD and I was instantly hooked. I had an Eclipse scholarship and developed a peer-to-peer code sharing plug-in and presented it at OOPSLA in Seattle later on that year. I can remember being terrified presenting right after Kent Beck and Erich Gamma at an Eclipse BOF where 400 or 500 hundred people were packed into a smallish meeting room.</p>

			<h3>Name five plugins you use and recommend:</h3>
					<ul>
						<li><a href="http://www.eclipse.org/jdt/">JDT</a>: My hats off goes to this remarkable set of plugins and the team that develops them. The feature-set provided is so rich and of such high quality. This is the bar I chase after as I try to build JavaScript tooling.</li>
						<li><a href="http://www.eclipse.org/jgit/">JGit</a>: Much of the development I do is server-side where we use JGit to work with Git repositories. I'm grateful for this library and only with there was a JavaScript version I could use for Node development.</li>
						<li><a href="http://www.eclipse.org/equinox/server/">Servlet Bridge</a>: I wrote this originally as a stop gap measure before app servers exposed OSGi natively. Ten years on and I'm still using it.</li>
						<li><a href="https://github.com/eclipse/orion.client/tree/master/modules/cf-launcher"> CF-Launcher(Orion)</a>: Day-to-day I work with Cloud Foundry writing and tooling Node.js applications. I couldn't do it without the debug and shell capabilities of CF-Launcher.</li>
						<li><a href="https://github.com/eclipse/orion.client/blob/master/bundles/org.eclipse.orion.client.ui/web/plugins/webdavFilePlugin.html">WebDAV(Orion) </a>: The first plugin I hack to the environment and install in my Orion instances. This plugin allows me to both mount my local desktop and more recently attach to running applications in Cloud Foundry.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>I love technology and always am reading about something new and have some sort of geeky project on the go. Currently I'm building a low-power personal cloud server farm at home. I also enjoy coaching soccer, relaxing at the cottage, fly-fishing in the spring, and playing guitar badly.</p>
									
				
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>