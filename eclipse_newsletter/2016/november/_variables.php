<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  // Begin: page-specific settings. Change these.
  $pageTitle = "Eclipse Loves Science";
  $pageKeywords = "eclipse, newsletter, science, working group";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "Love Science? Did you know that there are many great Eclipse Science projects and plugins? We even have an Eclipse Science Working Group! Read on.";