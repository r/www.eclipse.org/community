<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p align="center"><img class="img-responsive" src="images/image01.jpg" alt="Triquetrum" /></p>
<p align="center">Source: <a href="https://de.wikipedia.org/wiki/Datei:Dreistab_01.jpg" target="_blank">Wikipedia</a></p>

<h3>What is Eclipse Triquetrum?</h3>
<p><a href="https://projects.eclipse.org/projects/technology.triquetrum" target="_blank">Eclipse Triquetrum</a> delivers an open platform for managing and executing scientific workflows. The goal of Triquetrum is to support a wide range of use cases, ranging from automated processes based on predefined models, to replaying ad-hoc research workflows recorded from a user's actions in a scientific workbench UI. It will allow the user to define and execute models from personal pipelines with a few steps to massive models with thousands of elements.</p>

<p>Besides delivering a generic workflow environment, Triquetrum will also deliver extensions with a focus on scientific software. There is no a-priori limitation on target scientific domains, but the currently interested organizations are big research institutions in materials research (synchrotrons), physics and engineering.</p>

<p>Compared to other platforms for scientific workflows, Triquetrum differentiates itself by:</p>

<ul>
<li>combining the mature actor-oriented <a href="http://ptolemy.eecs.berkeley.edu/ptolemyII/" target="_blank">Ptolemy II framework</a> [1] with Eclipse and OSGi technology</li>
<li>being designed explicitly for integration in larger scientific software systems</li>
</ul>

<p>The combination of Eclipse/OSGi with Ptolemy II delivers a solid platform for a wide range of workflow applications, and in particular for scientific workflows. The modularity and dynamism offered by OSGi, the rich set of frameworks and technologies offered through the Eclipse Foundation, and the community of the Eclipse Science Working Group together result in a very powerful ecosystem for projects like Triquetrum.</p>

<p>The project is structured on three lines of work:</p>

<ol>
<li>Workflow-related features:</li>
<ul>
<li>an execution runtime that must be easy to integrate in different environments, ranging from a personal RCP workbench to large-scale distributed systems.</li>
<li>a graphical model editor delivered as plugins for an Eclipse RCP application.</li>
</ul>
<li>APIs and OSGi service implementations for task-based processing. This is a Triquetrum subsystem that is designed to be reusable independently of the workflows as well. It can easily be integrated in other process-oriented applications to decompose processes in sequences of traceable steps, including support for concurrent and asynchronous work.</li>
<li>Supporting APIs and tools, e.g. integration adapters to all kinds of things like external software packages, grid resource managers, data sources etc.</li>
</ol>

<p>Triquetrum's initial contribution is based on <a href="http://isencia.be/passerelle-edm-for-new-massif-beamlines/" target="_blank">Passerelle</a>  [2] which is in use since many years in several European synchrotrons that use workflows to automate experiment control, data acquisition and analysis.</p>

<h3>An example</h3>
<p>An interesting example is the fully automated MASSIF beamline of ESRF and EMBL at Grenoble, France. From the <a href="http://www.esrf.eu/home/news/general/content-news/general/massif-1-rise-of-the-robots-transforms-mx.html" target="_blank">ESRF website</a> [3]: <i>"MASSIF-1 ... is a world leading unique facility for the fully automatic, high throughput characterisation and data collection from macromolecular crystals."</i></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/image02.jpg" alt="example" /></p>

<p>Crystallographic samples are stored at cryogenic temperatures in a Dewar sample container (1). A robotic arm (2) automatically picks each sample in sequence and transfers it to the measurement location (3) where X-ray diffraction patterns can be obtained by the detector (4). The sample must first be positioned and oriented very finely (micrometer precision) to be able to find the microscopic crystal and to capture the best possible diffraction images. The images must be processed by several dedicated programs to identify the molecular structure of e.g. proteins.</p>

<p>In the past such experiments and the data analysis had to be done as a sequence of manual actions, e.g. by visual inspection of the crystal position, passing image files to scripts and analysis programs etc. Now the whole sequence can be driven by one integrated workflow that automatically finds the optimal position and orientation of a sample, collects and analyses diffraction images and stores the results in the LIMS database.</p>

<p>Andrew McCarthy, Team Leader at EMBL, during a presentation of the Passerelle pilot:</p>
<blockquote><i>"[executable] graphical workflow models definitely make it easier for us to collaborate on the automation of our MX experiments. After a limited introduction, they become almost self-documenting. The integration of Passerelle's execution traces with its graphical models allows our scientists to understand in detail what happened in each step of each experimental run."</i></blockquote>

<p>Matthew Bowler, Staff Scientist at EMBL noted:</p>
<blockquote><i>"The automatic routines developed are often able to locate crystals more effectively than the human eye and in many cases have obtained higher resolution data sets as all positions within a sample can be evaluated for diffraction quality.Initial experiments have been very successful, with users complimenting the accuracy, efficiency and simplicity of the facility."</i>
</blockquote>

<h3>Workflows?</h3>

<p>The term <i>workflow</i> is used in many different contexts and with different meanings, e.g. ranging from a very abstract concept of "the way something is being done" to models and software to run processes in a deterministic way.</p>

<p>In the context of Triquetrum, a workflow is considered to be:</p>
<ul>
<li>a sequence of tasks to achieve a certain result</li>
<li>that can be defined or recorded</li>
<li>and can be executed or orchestrated in a deterministic way by a software tool</li>
<li>potentially combining automated with interactive tasks</li>
</ul>

<p>In enterprise environments, workflow systems are typically used to orchestrate tasks in business processes or document-oriented work, assigning activities to the right persons, doing follow-up on uncompleted tasks etc.</p>

<p>Scientific research is not about passing documents around or going through purchase approval processes. But software and computing resources have also become crucial in many scientific disciplines as the design, setup and execution of experiments is becoming more complex, the volume of obtained data that must be analysed is exploding and the demands for scientific "output productivity" are always increasing.</p>

<p>The integration of a workflow system in a platform for scientific software can bring many benefits:</p>
<ul>
<li>The steps in scientific processes are made explicitly visible in the workflow models (instead of being hidden inside program code or scripts). Such models can serve as a means to present, discuss and share scientific processes in communities with different skills-sets.</li>
<li>Encapsulates technical services for automating complex processes in a scalable and maintainable way such as concurrent processing, support for integrating external programs and computing grids and their data transport and conversion needs, consistent error handling etc.</li>
<li>Integrates execution tracing, provenance data, etc.</li>
<li>Promotes modular solution design and reuse of model and software assets.</li>
</ul>

<p>Some example applications:</p>

<ul>
<li>process control for scientific experiments: data acquisition, equipment control, integrated error recognition and recovery, monitoring &amp alarming</li>
<li>in-silico experimentation through simulation or emulation</li>
<li>(semi-)automated data reduction and analysis</li>
<li>feedback between control &amp analysis in integrated workflows</li>
<li>interactive assistance &amp support automation</li>
</ul>

<h3>Ptolemy II in Eclipse</h3>
<p>The core of Triquetrum is an integration of <a href="http://ptolemy.eecs.berkeley.edu/ptolemyII/" target="_blank">Ptolemy II</a> [1] in an Eclipse and OSGi technology stack. Ptolemy II is an open source simulation and modeling tool from UC Berkeley. Originally intended for research and experimenting with system design techniques across engineering domains, it provides formalisms and tools for defining and executing hierarchical models of heterogeneous systems. Ptolemy II combines its actor-oriented architecture with a strong focus on concurrency, encapsulation and modularity. It comes with its own model design GUI called Vergil, based on Swing and a 2D graphical framework called Diva.</p>

<p>During its history of almost two decades it has been used internationally in varying domains, including in spin-offs for scientific workflow management like the <a href="https://kepler-project.org/" target="_blank">Kepler Project</a> [4] and <a href="https://github.com/eclipselabs/passerelle" target="_blank">Passerelle</a> [2]. Triquetrum continues on the track started by Passerelle but now as an official Eclipse project linked to the Science IWG, and with a much closer integration and collaboration with the Ptolemy II team at UC Berkeley.</p>

<p>Triquetrum takes its name from the three cornered astronomical instrument held by Ptolemy. The name is meant to evoke the Model View Controller design pattern used in Ptolemy II.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/image03.png" alt="Ptolemy" /></p>

<p align="center"><i>Source: <a href="https://commons.wikimedia.org/wiki/File:PSM_V78_D326_Ptolemy.png" target="_blank">Wikipedia</a></i></p>

<p>Ptolemy II is an actor-oriented system where data moves between software components that are known as actors. Each actor encapsulates a well-defined function or responsibility, with a strict decoupling between actor implementations. Actors only communicate via ports. Actors execute concurrently and may be composed hierarchically.</p>

<p>The semantics of how the model is executed is known as the model of computation. Ptolemy II supports many different models of computation and these models of computation can be used at different levels of hierarchy.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/november/images/image00.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/image00_sm.png" alt="model of computation" /></a></p>

<p>The Ptolemy group at Berkeley intends to use Triquetrum to provide an Eclipse-based actor-oriented IoT development environment called <a href="http://capecode.org/" target="_blank">Cape Code</a> [5]. The key idea behind Cape Code is <a href="http://accessors.org/" target="_blank">Accessors</a> [6], which are actors that provide access to a service, sensor or actuator. Accessors are instantiated by an accessor host, which is a program. Accessor hosts use the accessor as if it were a local source or sink for data and commands. Currently, accessor hosts are implemented in Java, JavaScript in a web browser, Node.js, C and Rust, where a typical design flow is to develop an IoT system using Cape Code and then generate code for Node and then for C. Accessors themselves are written in small JavaScript files that define the ports, parameters and functions to be implemented on each accessor host. Accessor hosts then implement the functions used in the accessor definition. An accessor host is to IoT what a web browser is to the internet in that they both render remote services by locally executing a proxy for that service.</p>

<h3>Status</h3>

<p>Triquetrum has done its first release, as part of the Science 2016 release on October 21st. This release defines the core APIs with basic implementations and demonstrates the goals of the three lines of work of Triquetrum:</p>
<ul>
<li>The workflow runtime subsystem provides APIs and initial implementations for model repositories and workflow executions. The RCP editor is in a prototype stage, to demonstrate our ideas and the selected underlying frameworks, and to gather feedback from initial users.</li>
<li>The task-based processing model provides a first iteration of the core APIs and some examples, e.g. of a basic integration in a workflow actor.</li>
<li>For the third line of work, about adapters to integrate with external systems and other technical services, we demonstrate integration options to run C-Python scripts.</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/november/images/image04_sm.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/image04.jpg" alt="Graphiti-based editor" /></a></p>

<p>The screenshot shows some customized features of the Graphiti-based editor: a palette with a filtered treeview and custom actor icons based on SVG and Ptolemy II's custom icon designs.</p>

<h3>Getting in touch</h3>

<p>The <a href="https://wiki.eclipse.org/Triquetrum" target="_blank">Triquetrum wiki</a> provides more information, e.g. how to <a href="https://wiki.eclipse.org/Triquetrum/Getting_Started" target="_blank">get started</a> with the RCP editor and an <a href="https://wiki.eclipse.org/Triquetrum/Extending_Triquetrum" target="_blank">example of how to extend the system</a>.</p>

<p>We use the <a href="https://dev.eclipse.org/mailman/listinfo/triquetrum-dev" target="_blank">triquetrum-dev</a> mailing list, a low-volume <a href="http://eclipse.github.io/triquetrum/" target="_blank">blog</a> and <a href="https://github.com/eclipse/triquetrum/issues" target="_blank">GitHub Issues</a> to provide support and communicate with our community.</p>

    <h3>References</h3>
   	 <p>[1] <a target="_blank" href="http://ptolemy.eecs.berkeley.edu/ptolemyII/">http://ptolemy.eecs.berkeley.edu/ptolemyII/</a><br>
		[2] <a target="_blank" href="http://isencia.be/automation-in-a-scientific-research-area/">http://isencia.be/automation-in-a-scientific-research-area/</a><br>
		      <a target="_blank" href="http://isencia.be/passerelle-edm-for-new-massif-beamlines/">http://isencia.be/passerelle-edm-for-new-massif-beamlines/</a><br>
		      <a target="_blank" href="https://github.com/eclipselabs/passerelle">https://github.com/eclipselabs/passerelle</a><br>
		[3] <a target="_blank" href="http://www.esrf.eu/home/news/general/content-news/general/massif-1-rise-of-the-robots-transforms-mx.html">http://www.esrf.eu/home/news/general/content-news/general/massif-1-rise-of-the-robots-transforms-mx.html</a><br>
		[4] <a target="_blank" href="https://kepler-project.org/">https://kepler-project.org/</a><br>
		[5] <a target="_blank" href="http://capecode.org">http://capecode.org</a><br>
		[6] <a target="_blank" href="http://accessors.org">http://accessors.org</a><br>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2016/november/images/erwin.jpg"
          alt="Erwin De Ley" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Erwin De Ley<br />
            <a target="_blank" href="http://www.isencia.be">iSencia Belgium</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/erwindl0">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://be.linkedin.com/in/erwin-de-ley-9200301">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/november/images/christopher.jpg"
        alt="Christopher Brooks" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Christopher Brooks<br />
            <a target="_blank" href="http://ptolemy.org/~cxh">University of California, Berkeley</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/cxbrooks">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
