<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>

   <p>The <a href="https://science.eclipse.org/">Eclipse Science Working Group</a> was established in 2014 in order to solve the problems of making science software interoperable and interchangeable. The group has a broad range of participants from industry, academia, and government, and has the goal of packaging science-related software components and to regularly release a trusted distribution of software.</p>

   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/eclipsescience.png" alt="Eclipse Science Logo"/>


	<p>Since its creation, working group members have proposed or contributed ten projects, including:</p>

	<ul>
	<li><b>StatET</b>provides tooling for the R language StatET is an Eclipse-based IDE for R.</p>
	<li><b>Apogy</b> provides a set of frameworks, EMF models, and Graphical User Interface components that simplify the creation of the software required to operate a physical system.</li>
	<li><b>Eclipse January</b> is a set of libraries for handling numerical data in Java. It is inspired in part by NumPy and aims to provide similar functionality.</li>
	<li><b>Eclipse Advanced Visualization Project</b> provides advanced visualization technologies for plotting, geometry and meshing and 3D visualization to address the needs of projects in the working groups. It provides a simple framework for exposing and developing visualization services.</li>
	<li><b>Eclipse Rich Beans</b> allows user interface to be created from beans or graphs of beans. The user interface available has standard widgets which have few dependencies to reuse. For instance there are widgets for editing numbers with bounds validation, units and that allow expressions of other boxes. There are widgets for entering a range of values and expanding out bean graphs to complete Design of Experiments work.</li>
	<li><b>Eclipse Triquetrum</b> delivers an open platform for managing and executing scientific workflows. The goal of Triquetrum is to support a wide range of use cases, ranging from automated processes based on predefined models, to replaying ad-hoc research workflows recorded from a user's actions in a scientific workbench UI. It will allow to define and execute models from personal pipelines with a few steps to massive models with thousands of elements.</li>
	<li><b>Eclipse ChemClipse</b> supports the user to analyse data acquired from systems used in analytical chemistry. In particular, chromatography coupled with mass spectrometry (GC/MS) or flame-ionization detectors (GC/FID) is used to identify and/or monitor chemical substances. It's an important task e.g. for quality control issues.</li>
	<li><b>Eclipse Integrated Computational Environment (Eclipse ICE)</b> is a scientific workbench and workflow environment developed to improve the user experience for computational scientists and make it possible for developers to deploy rich, graphical, interactive capabilities for their science codes and integrate many different scientific computing technologies in one common, cross-platform user environment. It is easily extended to new problems and has been successfully deployed for additive manufacturing, advanced batteries, advanced materials, nuclear energy, neutron science, quantum computing, and other areas.</li>
	<li><b>DAWNSci</b> provides features to allow scientific software to be inter-operable. Algorithms exist today which could be shared between existing Eclipse-based applications however in practice they describe data and do plotting using specific APIs which are not inter-operable or interchangeable. This project defines a structure to make this a thing of the past. It defines an inter-operable layer upon which to build RCP applications such that your user interface, data or plotting can be reused by others.</li>
	</ul>

	<p>Read more about the projects <a href="https://projects.eclipse.org/wg/science/projects">here</a>.</p>

	<p>In order to provide a central clearinghouse and repository for collaborative development efforts in science software, the Science Working Group sought approval from the Eclipse Board of Directors to establish a Science Top Level Project (TLP). In October 2016 the Board of Directors approved the creation of the eighth Eclipse TLP, “Science”. After a restructuring review, the science related projects will be moved to the new top level project.</p>


	<p>Planning and management of the Science TLP is the responsibility of the project management committee (PMC). Members of this committee are:</p>

	<ul>
	<li>Greg Watson (ORNL) - Project Lead</li>
	<li>Christopher Brooks (UC Berkeley)</li>
	<li>Erwin De Ley (iSencia Belgium NV)</li>
	<li>Jay Jay Billings (ORNL)</li>
	<li>Jonah Graham (Kichwa Coders)</li>
	<li>Matthew Gerring (Diamond Light Source)</li>
	<li>Peter Chang (Diamond Light Source)</li>
	<li>Philip Wenig (Lablicate GmbH)</li>
	<li>Stephan Wahlbrink</li>
	<li>Tobias Verbeke (Open Analytics NV)</li>
	<li>Torkild Ulv&oslash;y Resheim (Itema AS)</li>
	</ul>

	<p>The first meeting of the PMC will be held in December 2016. More information can be found at the <a href="https://science.eclipse.org/">Eclipse Science website</a>.</p>


	<p>Although not as high profile as other emerging disciplines, such as IoT, science software nevertheless has the potential to have a huge role in solving some of the major challenges facing our society. By establishing the Science TLP, the Eclipse community is sending a clear message that Eclipse plays a very important role in the design and development of cutting edge science software.</p>


	<p>We are very excited about what the future holds for the science community in and outside of the Eclipse Science working group and projects. We hope you enjoy reading the articles in this issue of the Eclipse Newsletter and learn more about what Eclipse Science has to offer. If you’re into science software, please <a href="https://science.eclipse.org/">get involved</a>!</p>



<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/gregwatson.jpg" alt="Greg Watson" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Greg Watson<br />
        <a target="_blank" href="www.ornl.gov">Oak Ridge National Laboratory</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="www.linkedin.com/in/grwatson">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
