<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Introduction</h2>
		<p>Almost two years ago I had the opportunity to tell you about our work to apply the Eclipse Workbench to scientific computing. That article, which can be found <a href="https://www.eclipse.org/community/eclipse_newsletter/2015/january/article1.php">here</a>, discussed how we are extending the Eclipse workbench and tooling for problems in scientific computing.</p>


		<p>It is a pleasure to have the opportunity to write you again with an update on our progress. While the scientific computing world hasn’t changed very much in two years’ time, our work has.</p>


		<p>With exciting challenges such as the Exascale Computing Project on the horizon, the need for powerful, Eclipse-based tools for modeling and simulation (M&S) has never been greater. Key drivers continue to be scientific workflows, developer productivity, and visualization, and no single tool can satisfy all of those needs. The Eclipse Platform, however, just might.</p>


		<p>The Eclipse Platform has been adopted across numerous domains and, in some cases, has revolutionized the state of the art. While it  is not used as frequently in modeling and simulation as much as other areas, it is widely known.</p>


		<p>So several years ago, we asked ourselves the question, “What if the tools in the Eclipse Platform for <i>authoring scientific software</i> could be repurposed for <i>using scientific software</i>?” The answer: the Eclipse Integrated Computational Environment (ICE), a general purpose, easily extended M&S platform that significantly changes the way computational scientists interact with their software. It improves productivity and streamlines the workflow for computational scientists creating input files, launches jobs on local or remote machines, and processes output.</p>


		<p>And since it is based on the Eclipse Rich Client Platform (RCP), developers can update it to support new codes or add new tools just as easily as users can perform simulations and visualize results.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/november/images/reporting.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/reporting_sm.png" alt="eavp visualisations" /></a></p>

		<p align="center"><i>Fig. 1 - Visualizations created by EAVP and shown embedded in ICE’s workbench. Clockwise from upper left: a mock LWR fuel assembly; phonon scattering data from nuclear scattering experiments; a model of single-loop cooling configuration in a nuclear plant; and the temperature distribution in a prismatic cell battery.</i></p><br/>

		<p align="center"><a href="/community/eclipse_newsletter/2016/november/images/3dgeometry.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/3dgeometry_sm.png" alt="Eclipse ICE 3D" /></a></p>

		<p align="center"><i>Fig. 2. - A screenshot showing the workbench of ICE with 3D visualizations from EAVP for pre- and post-processing in thermomechanical simulations of binder jet-printed cantilevers for additive manufacturing.</i></p></br>

		<p>Then, about a year ago, we realized that ICE was more versatile than we originally thought, so we began to create individual projects out of its individual parts. The first pieces removed from ICE were its extensive visualization tools, which became the initial contribution for the Eclipse Advanced Visualization Platform, (EAVP). EAVP makes it easy to view many different types of data by plugging into common high-performance visualization tools like VisIt and Paraview. It also provides support for plotting, 3D geometry editing, and a limited amount of 2D mesh editing.</p>

	<h2>Design and Implementation</h2>
		<p>ICE is designed around the idea that each task performed by a computational scientist - creating an input file, launching a job, post-processing data, etc. - can be done equally as well through automation if the program has the required data and knows the instructions necessary to perform the task. While this may seem broad, it works remarkably well for the common tasks associated with M&S.</p>


		<p>For example, writing an input file is more about knowing what information should go in the file than how it should be written. Likewise, launching a massively parallel job is more about knowing what should be launched than the details of how to execute it. Each “intelligent delegate” in ICE is an Eclipse Plugin that inherits from an Item base class and provides a normal, pluggable OSGi service. In the ICE parlance, plugin and Item are used interchangeably most of the time.</p>


		<p>ICE generates custom UI elements for these workflow tasks and embeds EAVP in those UI elements as required. This embedding is accomplished through both OSGI Declarative Services and Extension Points; each distinct visualization tool provided by EAVP is registered as an implementation of one of several service interfaces.</p>


		<p>This makes it possible to access all the visualization services in a uniform way, regardless of their implementation. Some EAVP visualization services, such as those for VisIt and Paraview, can be used either locally or remotely to accommodate different needs, including remote rendering on clusters.</p>

	<h2>Availability, Contributions, and Getting in Touch</h2>
		<p>Eclipse ICE is available at its homepage, <a href="https://www.eclipse.org/ice">https://www.eclipse.org/ice</a>. Version 2.2 was released on October 21st as part of the Science Working Group 2016 Simultaneous <a href="https://www.eclipse.org/org/press-release/20161021scienceICESoftware.php">Release</a>. The project is currently under active development by teams at Oak Ridge National Laboratory and other institutions, and is open to contributions from other organizations.</p>


		<p>EAVP is also available at its homepage as a p2 repository, <a href="https://www.eclipse.org/eavp">https://www.eclipse.org/eavp</a>. It was also part of the Science release, although it is still in early development and lacks its own workbench. Users who want to experiment with EAVP immediately instead of embedding it in their workbenches are encouraged to download ICE.</p>


		<p>Both projects welcome contributions in any form from the community (so long as they follow the Eclipse Contribution process, if it applies). Interested parties can reach out on project mailing lists.</p>


		<h3>Mailing Lists</h3>
		<p>ICE has two mailing lists, one for users and another for developers:</p>
		<p><u>Developers</u> - <a href="https://dev.eclipse.org/mailman/listinfo/ice-dev">https://dev.eclipse.org/mailman/listinfo/ice-dev</a><br>
		<u>Users</u> - <a href="https://dev.eclipse.org/mailman/listinfo/ice-users">https://dev.eclipse.org/mailman/listinfo/ice-users</a></p>


		<p>EAVP mailing list: <a href=" https://dev.eclipse.org/mailman/listinfo/eavp-dev">https://dev.eclipse.org/mailman/listinfo/eavp-dev</a></p>


		<p>For those who are not partial to the email lists, an email to <a href="mailto:eclipse.ice.project@gmail.com" target="_top">eclipse.ice.project@gmail.com</a> will suffice.</p>


	<h3>Source Code</h3>
		<p>You can find the source code for these projects on GitHub. Bug tracking for each is available in the “Issues” tab of the individual GitHub projects.</p>
		<p><u>ICE</u> - <a target="_blank" href="https://github.com/eclipse/ice">https://github.com/eclipse/ice</a><br>
		<u>EAVP</u> - <a target="_blank" href="https://github.com/eclipse/eavp">https://github.com/eclipse/eavp</a></p>


	<h3>Questions?</h3>
		<p>Other questions and comments can be directed to @jayjaybillings on Twitter or by email to <a href="mailto:billingsjj@ornl.gov" target="_top">billingsjj@ornl.gov</a>.</p>

	<h2>Acknowledgements</h2>
		<p>I gratefully acknowledge the support of our sponsors in the U.S. Department of Energy, Office of Nuclear Energy, Advanced Modeling and Simulation (NEAMS); the Department of Energy, Office of Energy Efficiency and Renewable Energy, Computer-Aided Engineering for Batteries (CAEBAT) project; the Consortium for Advanced Simulation of Light Water Reactors (CASL); and the Oak Ridge National Laboratory Director's Research and Development Fund. I would also like to acknowledge the support of my colleagues at ORNL on the Scientific Software Development Team and everyone else who has contributed to ICE and EAVP from other institutions.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/november/images/jayjay.jpg"
        alt="Jay Jay Billings" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         Jay Jay Billings<br />
        <a target="_blank" href="https://www.ornl.gov/">Oak Ridge National Laboratory</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/jayjaybillings">GitHub</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

