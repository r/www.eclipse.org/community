<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p><a href="https://projects.eclipse.org/projects/technology.january">Eclipse January</a> is a set of common data structures in Java, including a powerful library for handling numerical data. As the volumes and complexity of data dramatically increases - the so-called 'Big Data' - Eclipse January provides a numerical library that simplifies the handling and manipulation of data in the form of multi-dimensional arrays.</p>
<p>At the heart of the library are the IDataset and Dataset interfaces and classes. So, here are some reasons you might want to use Eclipse January</p>
<ul>
<li><strong>Familiar</strong>. Provide familiar functionality, especially to NumPy users.</li>
<li><strong>Robust</strong>. Has test suite and is used in production heavily at Diamond Light Source.</li>
<li><strong>No more passing double[]</strong>. IDataset provide a consistent object for basing APIs on with significantly improved clarity over using double arrays or similar.</li>
<li><strong>Optimized</strong>. Optimized for speed and getting better all the time.</li>
<li><strong>Scalable</strong>. Allows handling of data sets larger than available memory with "Lazy Datasets".</li>
<li><strong>Focus on your algorithms</strong>. By reusing this library it allows you to focus on your code.</li>
</ul>
<p>This article gives an overview of the functionality of the Dataset class for multi-dimensional arrays.</p>

<h2>Array Creation</h2>
<p>Eclipse January supports straightforward creation of arrays. Let's say we want to create a 2-dimensional array with the following data:</p>
<pre>[1, 2, 3,
 4, 5, 6,
 7, 8, 9]</pre>

<p>First we can create a new dataset:</p>
<code style="color:blue;">
Dataset dataset = DatasetFactory.createFromObject(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
</code></br>

<p>This gives us a 1-dimensional array with 9 elements, as shown below:</p>
<pre>
[1.0000000, 2.0000000, 3.0000000, 4.0000000, 5.0000000, 6.0000000, 7.0000000, 8.0000000, 9.0000000]
</pre>
<p>We then need to reshape it to be a 3x3 array:</p>
<code style="color:blue;">
dataset = dataset.reshape(3, 3);
</code></br>
<p>The reshaped dataset:</p>
<pre>
 [[1.0000000, 2.0000000, 3.0000000],
 [4.0000000, 5.0000000, 6.0000000],
 [7.0000000, 8.0000000, 9.0000000]]
</pre>
<p>Or we can do it all in just one step:</p>
<code style="color:blue;">
Dataset another = DatasetFactory.createFromObject(new double[] { 1, 1, 2, 3, 5, 8, 13, 21, 34 }).reshape(3, 3);
</code></br>

<p>Another dataset:</p>

<pre>
 [[1.0000000, 1.0000000, 2.0000000],
 [3.0000000, 5.0000000, 8.0000000],
 [13.000000, 21.000000, 34.000000]]
</pre>
<p>There are methods for obtaining the shape and number of dimensions of datasets</p>
<code style="color:blue;">
System.out.println("shape of dataset: " + Arrays.toString(dataset.getShape()));
System.out.println("number of dimensions: " + dataset.getRank());		
</code></br>
<p>Which gives us:</p>
<pre>
shape of dataset: [3, 3]
number of dimensions: 2
</pre>
<p>Datasets also provide functionality for ranges and a random function that all allow easy creation of arrays:</p>
<code style="color:blue;">
Dataset a = DatasetFactory.createRange(15, Dataset.INT32).reshape(3, 5);
</code></br>
<pre>
[[0, 1, 2, 3, 4],
 [5, 6, 7, 8, 9],
 [10, 11, 12, 13, 14]]
</pre>
<code style="color:blue;">
Dataset rand = Random.rand(new int[]{3,4});</code></br>

<pre>
[[0.37239989, 0.89414117, 0.94036325, 0.47739019],
 [0.47194246, 0.12534931, 0.41001452, 0.90583666],
 [0.81731075, 0.76468139, 0.97097539, 0.37182491]]
</pre>
<p>IDataset is not just for doubles, it can also be used with other types such as:</p>
<ul>
<li>int, float</li>
<li>complex</li>
<li>Compound types such as RGB</li>
<li>String</li>
<li>Any class really!</li>
</ul>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/dataset.png" alt="dataset" /></p>

<p align="center">Figure 1: DataSet Type Hierarchy</p>

<h2>Array Operations</h2>

<p>The <a href="https://github.com/eclipse/january/blob/master/org.eclipse.january/src/org/eclipse/january/dataset/Maths.java" target="_blank">org.eclipse.january.dataset.Maths</a> provides rich functionality for operating on the Dataset classes. For instance, here's how you could add 2 Dataset arrays:</p>
<code style="color:blue;">
Dataset add = Maths.add(dataset, another);
</code></br>
<p>Or you could do it as an inplace addition. The example below creates a new 3x3 array and then adds 100 to each element of the array.</p>
<code style="color:blue;">
Dataset inplace = DatasetFactory.createFromObject(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }).reshape(3, 3);<br>
inplace.iadd(100);
</code></br>

<pre>[[101.0000000, 102.0000000, 103.0000000],
 [104.0000000, 105.0000000, 106.0000000],
 [107.0000000, 108.0000000, 109.0000000]]</pre>
 
<p>The Math library also provides functionality for:</p>

<ul>
<li>Basic operations: add, sub</li>
<li>Exponential &amp logarithmic operations</li>
<li>Stats: min, max, mean, median, quantiles, covariance, kurtosis, etc</li>
<li>Trigonometric Functions: sin, cos, tan, etc</li>
<li>And more!</li>
</ul>

<p>Additionally there is also a <a href="https://github.com/eclipse/january/blob/master/org.eclipse.january/src/org/eclipse/january/dataset/LinearAlgebra.java" target="_blank">LinearAlgebra</a> class that operates on Datasets.</p>

<h2>Slicing</h2>
<p>Datasets simplify extracting portions of the data, known as 'slices'. For instance, given the array below, let's say we want to extract the data 2, 3, 5 and 6.</p>

<pre>
[1, 2, 3,
 4, 5, 6,
 7, 8, 9]
</pre>

<p>This data resides in the first and second rows and the second and third columns. For slicing, indices for rows and columns are zero-based. A basic slice consists of a start and stop index, where the start index is inclusive and the stop index is exclusive. An optional increment may also be specified. So this example would be expressed as:</p>
<code style="color:blue;">
Dataset slice = dataset.getSlice(new Slice(0, 2), new Slice(1, 3));
</code></br>

<p>slice of dataset:</p>
<pre>[[2.0000000, 3.0000000],
 [5.0000000, 6.0000000]]
</pre>
<p>Slicing and array manipulation functionality is particularly valuable when dealing with 3-dimensional or n-dimensional data. </p>

<h3>Try Eclipse January</h3>
<p>The <a href="https://github.com/eclipse/january/tree/master/org.eclipse.january.examples#eclipse-january-examples-and-getting-started" target="_blank">Getting Started Guide</a> shows how you can get started with the example project in Eclipse. Most of the code in this article is from the <a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/BasicExample.java" target="_blank"> BasicExample</a> class. Once you've tried the basics, there are some more advanced examples to have a look at and run:</p>
<ul>
<li><a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/NumpyExamples.java" target="_blank">NumPy Examples</a> shows how common NumPy constructs map to Eclipse Datasets.</li>
<li><a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/SlicingExamples.java" target="_blank">Slicing Examples</a> demonstrates slicing, including how to slice a small amount of data out of a dataset too large to fit in memory all at once.</li>
<li><a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/ErrorExamples.java" target="_blank">Error Examples</a> demonstrates applying an error to datasets.</li>
<li><a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/IterationExamples.java" target="_blank">Iteration Examples</a> demonstrates a few ways to iterate through your datasets.</li>
<li><a href="https://github.com/eclipse/january/blob/master/org.eclipse.january.examples/src/org/eclipse/january/examples/dataset/LazyExamples.java" target="_blank">Lazy Examples</a> demonstrates how to use datasets which are not entirely loaded in memory.</li>
</ul>
<p>Eclipse January is an incubating project from the Science Working Group at Eclipse Foundation. The data structures were developed and used over the years at Diamond Light Source and Oakridge National Labs, two facilities that have a lot of experience dealing with huge, complex amounts of data, in 2-D, 3-D and multi-dimensional formats.</p>
<p>The power of Eclipse January comes not just from the simplicity and convenience of being able to manipulate data, but also provides a basic standard for data storage. This allows for easy integration of tooling based on the Dataset class. So as data sizes continue to grow and be more complex, Eclipse January provides a convenient, powerful, robust library to simplify and standardise multi-dimensional arrays in Java. </p>

    <div class="bottomitem">
      <h3>About the Authors</h3>

      <div class="row">
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-8">
              <img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/tracym.jpg" alt="Tracy Miranda" />
            </div>
            <div class="col-sm-16">
              <p class="author-name">
                 Tracy Miranda<br />
                <a target="_blank" href="https://kichwacoders.com/">Kichwa Coders</a>
              </p>
              <ul class="list-inline">
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://twitter.com/tracymiranda">Twitter</a></li>
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://www.linkedin.com/in/tracymiranda">LinkedIn</a></li>
                <?php //echo $og; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
            <div class="col-sm-8">
              <img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/jonahg.jpg" alt="Jonah Graham" />
            </div>
            <div class="col-sm-16">
              <p class="author-name">
                 Jonah Graham<br />
                <a target="_blank" href="https://kichwacoders.com/">Kichwa Coders</a>
              </p>
              <ul class="list-inline">
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://twitter.com/JonahGrahamKC">Twitter</a></li>
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://www.linkedin.com/in/jonahgraham">LinkedIn</a></li>
                <?php //echo $og; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

     <!-- <div class="sideitem text-center">
      <h6>November Eclipse Newsletter</h6>
     <p class="author-picture"><a href="/community/eclipse_newsletter/2016/november/"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/november/images/science.png"
        alt="newsletter logos" /></a></p>
      <p class="author-name">Eclipse Science<br>
        <a target="_blank" href="/community/eclipse_newsletter/2016/november/">Read More</a>
      </p>
    </div> -->
    <!--<div class="sideitem">
        <h6>Related Links</h6>
        <ul>
          <li><a href="/community/eclipse_newsletter/">The Eclipse Newsletter</a></li>
        </ul>
      </div> -->
