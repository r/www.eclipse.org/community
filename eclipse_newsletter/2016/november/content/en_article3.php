<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>
		<p><a target="_blank" href="http://texlipse.sourceforge.net/">TeXlipse</a> is a well known <a target="_blank" href="https://www.latex-project.org">LaTeX</a> plugin for Eclipse and quite popular among scientists. LaTeX is a typesetting tool widely used in academia for communication and publication in many fields. It has an approach to typesetting similar to wiki markup; as the writer uses markup tagging conventions to define the structure of a document, to stylize text through the document and to add citations, cross-references and mathematical formulas.</p>

		<p>TeXlipse provides both LaTeX and BibTeX editors, a project creation wizard, and a complete user manual of the editor functions. Additional features include syntax highlighting, document outline, section folding, content assist, cite and reference completion, templates, builder integration, viewer integration with inverse search, and more. The plugin makes it possible for LaTeX documents to be edited and built like normal projects in the IDE, and the viewer support makes it easy to check the outcome.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/latex.png" alt="Origin of Latex"/></p>

		<p><i>While the LaTeX syntax can be daunting at first sight, it is far less demanding than printing a scientific document in the sixteenth century – <a target="_blank" href="https://commons.wikimedia.org/wiki/File:Printer_in_1568-ce.png">Jost Amman, De Stände, 1568</a>.</i></p>


		<p>Since the developers of the plugin have not been able to maintain the code for the past few years (the last release was in 2011), members of the Eclipse Science Working Group have agreed to step in and take it over. We have submitted a project proposal, which will have to go through the proposal phase of the Eclipse development process before it can be accepted. The source code appears to have a good pedigree so we expect progress to be swift.</p>

		<p>TeXlipse works just fine on Neon, even a long time after the last release. As one user puts it:</p>


		<blockquote>Even in 2016 (5+ years since the latest release), everything works as expected. Even the latex-specific features are comparable, if not better, than what the other dedicated latex editors provide. Plus you already get the Eclipse features. I don't get why dedicated latex editors reinvent the whole wheel instead of just implementing the latex-specific features for an extensible editor like Eclipse. – Abhishek Anand</blockquote>

		<p>The current code, representing version 1.5.0 have been moved from <a target="_blank" href="https://sourceforge.net/projects/texlipse/">SourceForge</a> to <a target="_blank" href="https://github.com/jarrah42/texlipse">GitHub</a> where the Eclipse Science team will start cleaning it up. The first step would be to prepare the code for a new life in the Eclipse infrastructure. That means setting up a continuous build system and restructure for future enhancements. We don’t expect to be doing many changes to begin with, but we’re happy to accept improvement proposals and bug reports. And if you’d like to contribute, we’re open to that too.</p>


		<p>TeXlipse up to version 1.0 was developed by the Texlapse-team as a software project on the SoberIT lab of the <a target="_blank" href="http://www.aalto.fi/en/">Helsinki University of Technology</a>. TeXlipse version 1.5.0 was developed from 1.0 as an open source project on SourceForge. We wish to thank them all for their efforts!</p>

		</br>
		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/november/images/report.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/report_sm.png" alt="Report TeXclipse"/></a></p><br/>

		<p>If you want to get started using TeXlipse you should install it from the <a href="https://marketplace.eclipse.org/content/texlipse/">Eclipse Marketplace</a> where the latest version is available. It actually has over 42,000 installs which ranks it at 72th place amongst all the solutions available. You may also want to take a look at the <a target="_blank" href="http://texlipse.sourceforge.net/manual/">user guide</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/november/images/torkild.jpg"
        alt="Torkild Resheim" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Torkild Ulv&oslash;y Resheim<br />
            <a target="_blank" href="http://itema.no">Itema AS</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/turesheim">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.twitter.com/torkildr">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/torkildr">LinkedIn</a>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://resheim.net">Blog</a>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

