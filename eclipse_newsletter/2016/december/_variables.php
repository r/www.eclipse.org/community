<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  // Begin: page-specific settings. Change these.
  $pageTitle = "Ready, Set&#8230 2017!";
  $pageKeywords = "eclipse, newsletter, 2017, trends, modeling, build systems, languages, testing, cloud, ide";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "Every new year holds great innovations for the open source world! Let's see what's in store for the Eclipse Community.";