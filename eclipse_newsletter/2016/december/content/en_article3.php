<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<img class="img-responsive" align="right" src="/community/eclipse_newsletter/2016/december/images/ghosts_sm.png" alt="ghost testing" />
<p>Forgive me – if I’m asked to write an article about the future of testing just before Christmas, then I obviously think of “A Christmas Carol”. Since I’ve misplaced my crystal ball anyway, it might make sense to see what the ghosts of testing past and present tell us about what we can expect.</p>

<h2>What do we know?</h2>

<p>Somewhere, a test is failing. If there’s one thing that ten years of testing has taught me, it is that it’s sometimes difficult to know what we’re expecting when testing. And when we do have a pretty clear idea of our expectations, then testing often shows us that there is a gap between our expectations and an implementation or behavior. We can see the gap due to a failing automated test in an area of the software that “definitely isn’t affected by these changes”, or we might see it by finding a new risk that we discover by exploring around a new feature. I doubt that these aspects of testing are going to change in the future. Nevertheless, the contexts and ways that we apply testing are definitely changing. I see three main areas:</p>

<h2>New technological contexts</h2>
<p>No article about the future would be complete without a list of the current hype topics – Internet of Things, Microservices and Industry 4.0. Before you shout “bingo” though, these subjects are highly relevant to testing. At the EclipseCon Europe conference in autumn 2016, I was asked: “How are we ever going to test the Internet of Things?”. My honest answer at the moment is a mix of “I don’t know”, “carefully!” and “using similar techniques and structures as we’ve been using for years”.</p>

<p>The challenges of testing IoT systems start with functional aspects – how can we test functionality across multiple protocols, networks and devices? Alongside functional testing, non-functional areas are also going to be critical. Security, efficiency and robustness against fallouts are aspects easily forgotten when testing applications – we won’t be able to allow ourselves that luxury for IoT.</p>

<p>The more connected we become, the more we need to think about ethicality as well. An excellent Project Quality Day 2016 talk was dedicated to ethics in software development. As software eats more and more of the world, how are we mitigating ethicality risks?</p>

<h2>New opportunities</h2>

<p>Maybe the news isn’t all gloomy. Over the last years, we’ve seen companies and teams moving from long release cycles and manual test phases to continuous integration, then continuous delivery, and DevOps. I attended a talk this November by a team that has reduced some of their pre-release tests and instead use much more monitoring in production. They know that their pre-release tests will find the worst problems that they can check for in advance. Monitoring in production lets them see what is actually happening in the system and react quickly.</p>

<p>For applications where it can be used, and where the impact of a fallout isn’t life-endangering, I’m excited about the prospect of technology that lets us monitor the system in real time. Nevertheless, I don’t see that monitoring in production means that there is no need to test continuously during development. In fact, I think the contrary: continuous testing at a well-defined level is what lets us release frequently and monitor / adapt in production in the first place. The worst knowable errors need to be caught before releasing.</p>

<h2>The role of the tester</h2>

<p>Which brings me to my final point. I don’t see that the role of the tester is going anywhere soon. Yes, we are automating many more things – but we can (currently) only automate what we can define. Anyone who has heard me talk about exploratory testing knows that exploration and investigation are highly important for identifying unknown risks. Just because the technological world is changing doesn’t remove this truth.</p>

<p>The role of the tester has been changing due to agile processes – and this is likely to continue. Testers are integrated team members and the whole team shares the responsibility for quality. Testers are growing into roles like test consultants, test coaches, test experts and also communication experts. The last point is especially relevant in my mind. As much as we might like to develop one, there is no formalized language that removes the need for discussion, questions, clarification and conversation amongst stakeholders, team members and users. We are already seeing testers working to improve team communication about quality and user expectations – and I see this trend continuing.</p>

<h2>The mists of the future</h2>

<p>One thing that has to happen for this to come true is that customers, IT leaders and managers need to see quality as something worth investing in. Too often we hear in projects that testing is too expensive or that we can scrimp on the testing effort. It needs to be accepted that quality (most specifically quality implemented and supported by experts in the field) is worth paying for. I think we’ll definitely get there someday, but I would prefer it to be before something awful happens. I guess that’s my ghost of Christmas yet to come – we can change our future for the better if we start to place more importance and invest more in quality now.</p>


<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/alex.jpg"
        alt="Alexandra Schladebeck" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Alexandra Schladebeck<br />
            <a target="_blank" href="https://www.bredex.de/">BREDEX GmbH</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/alex_schl">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/alexandraschladebeck">LinkedIn</a>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.schladebeck.de/">Website</a>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
