<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Pair Programming is a tenet of agile software development in which two programmers focus their energies simultaneously on one code base. Typically there is a “driver” who types at the keyboard while a “navigator” or “observer” reviews the changes. Proponents claim productivity is improved while critics argue that Pair Programming suffers from a “watch the master” phenomenon.</p>
<p>One reason that Pair Programming may improve productivity is that it forces two people to understand code as it is being written. It is commonly understood that explaining an idea to another helps solidify the understanding for both people. Sometimes, outlining a concept can reveal errors and inconsistencies during the explanation process.</p>
<p>There is another benefit:  When two people are responsible for a solution and one is a novice, the novice may feel more at ease during shared development. When the expert is supervising, the novice feels they are doing it the right way. In this manner, a natural transfer of knowledge from expert to novice can occur without explicit teaching.</p>

<h2>“Here, let me drive …”</h2>
<p>Typically, pairs usually gather around one workstation and switch roles frequently. This can involve handing over the keyboard or moving chairs. Because the work in progress is usually stored on a developer’s desktop, changing computers requires a commit or transfer of the current state of all changed files. When the pair is collocated, it’s much easier to hand over the keyboard.</p>

<h2>Remote Pair Programming</h2>
<p>When one of a pair is remote, face to face interaction is replaced with phone calls, screen shares, chat programs, voice and video sharing. This can reduce bandwidth and result in some confusion and limitations as to who has the keyboard.  A number of collaboration tools exist today to help with remote pairing. These usually take the form of screen and document sharing.</p>

<h3>“No, no … move the mouse … over to the left”</h3>
<p>Screen sharing almost always implies a driver and observer relationship and falls-down when the pair needs to change rolls. The current state is on the desktop of the driver and switching rolls involves transferring state and handing over the screen share. Programs like Screen Hero and VNC improve the situation by providing interactive screen sharing for both parties on one person’s computer.</p>
<p>Screen Hero allows two people to share a screen and gives each person their own mouse pointer. This helps with the problem of switching roles because it allows each person to type and point on the screen. The driver’s computer is the shared resource and works normally for the driver, however, the observer is second class. Typically, the screen resolution and performance for the observer are poor. If the connection is dropped, the observer can no longer work. Finally, while both programmers can type and click at the same time, there is only a single mouse and keyboard focus. Characters become intermixed when typed at the same time and windows can fight for focus. The pair must actively cooperate and the driver must take their hands off the keyboard when the observer is using it.</p>

<h3>“I see what you’re doing …”</h3>
<p>Document sharing programs such as Google Docs and Box Notes provide shared awareness in a single file. While the file is being edited, each person has an insertion point and can type at the same time without interference. Like interactive screen sharing, document sharing allows you to know who is editing a file, but more importantly, you can see exactly which line they are on. On the surface, this seems to solve the problems of interactive screen sharing. State is shared in the cloud and pairs can work together and switch roles easily. However, sometimes it is important, especially when debugging, to see exactly the same set of windows, not just a single document.</p>

<h2>Towards a Shared Developer Awareness</h2>

<p>Imagine an environment where people work in their own space, but at any time they can collaborate and share state easily. Here is how such an environment might work:</p>
<p>The collaborative environment of the future for Pair Programming and shared developer awareness has seamless document sharing and instant screen collaboration at the core. Things like voice, chat and video are table stakes. However, in this new environment, working together is seamless. Working with others does not involve stopping your work, starting a screen share or instant messaging a special URL to collaborate. Everyone is already in the environment and working.</p>
<p>While the team is working day to day, most of the time people are working in their own area. However, at all times, team members are nominally aware of where and how others are working.  The user interface for this is subtle and does not get in the way as you work.</p>
<p>In this new environment, when another team member needs access to someone’s work, they simply go there. It’s easy to see the directory other team members are in, the files they are opening and the tests they are running. A team member currently may not be working in their space, but their work in progress is always available to everyone. Of course, access controls are in place to allow a user to control what others see, but the default is an open, trusting and sharing environment.</p>
<p>If you are editing a file and have a question or need help, you can query another person right from within the file and continue coding. When a question is answered, you are notified. If the person happens to be around, they can respond immediately, come right in and help edit your file. Perhaps the question is not that critical. When you ask, you can indicate severity. At any point in time, developers are aware of their outstanding questions. If the person you are asking doesn’t know the answer, they can assign the question to someone else or add another person to the discussion. The environment remembers who answers the question and learns who the experts are.  All of this happens seamlessly in the context of the source files, while the team codes.</p>
<p>When ideas are first forming and code is being written, there is no need for lengthy back and forth discussions in email, chat, commits or pull requests. Team members communicate directly within the environment. It’s easy for an expert to see when another team member is on the right track or off in the weeds. When more than one person is working in a directory tree, it is easy to find and follow a specific team member. A novice can follow a master (or vice versa) and interactively edit the same files to ensure there are no big surprises during a formal code review. Major and minor problems are found and fixed early.</p>
<p>Collaboration is not limited to a single file. Imagine a hackathon where one team member is writing the server code, another is writing the front end and a third person is researching a database. Of course, everyone is actively communicating using voice and video while they program to avoid stepping on each other’s toes. However, anyone can watch as the code is written, including team members who are not actively programming. Directories and files appear as the solution progresses from nothing to working code.</p>
<p>Imagine a pair, working on a task. At some point, the next step is well understood and the solution is to change many different files. The pair could decide to tackle it separately (“You do the file references, I’ll take the code ones”) or one person could make the changes while the other researches a new topic or codes something else. When the pair resumes working together, everything is up to date because the state is shared in the same space.</p>
<p>Sometimes a less experienced person needs guidance. What have they tried? Where are they stuck? They may even be in a different time zone. At a glance, an expert can see what they have done, which files they have changed and make suggestions to get the new person back on track. When pairing, the more experienced person hits the “Let me drive button” and the newbie automatically follows along as the expert browses and edits the code. This interaction is not limited to a single file. The environment records everything while you work so that anyone, including you, can go back in time and see what happened. Audio is automatically converted to text so it can be searched and synced with code changes.</p>
<p>The difference between productive and superstar programmers is startling. Productive programmers, enthusiastic about learning, soak up everything around them and become the superstars of tomorrow. Sometimes skills can be explicitly taught however, often just working with someone who is better than you, makes you smarter. It may not be clear exactly how the knowledge is transferred, but this sort of implicit learning is common in music and other disciplines.</p>
<p>Programmers have been working together for years. It’s time that we worked in an environment that fosters impromptu learning and sharing. The superior Pair Programming environment of the future will both simplify and expand the collaborative programming experience.</p>
    <p>In the coming months, <a target="_blank" href="https://orionhub.org/">Eclipse Orion</a> will be investigating Pair Programming in the Cloud and the shared developer experience. Stay tuned!</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/Steve.jpg"
        alt="Steve Northover" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Steve Northover<br />
            <a target="_blank" href="http://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/stevenorthover">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
