<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>It is that time of year again where we attempt to look into the future and make some predictions. I'm not crazy enough to predict specific events or outcomes, but I do have some thoughts on some general trends and directions for the Eclipse open source community. So here goes nothing...</p>

<h2>Trends and Directions</h2>
<h3>Languages</h3>
	<p>2017 is going to be the year that language tools go server-side. The <a href="https://github.com/Microsoft/language-server-protocol" target="_blank">Language Server Protocol</a> is enabling a whole new generation of development environments. In my experience it is unprecedented to see a new technology get adopted as quickly within the Eclipse community, and by multiple projects, as LSP. The Eclipse Che project <a href="http://blog.codenvy.com/announcement-eclipse-che-now-strategic-platform-red-hat/" target="_blank">announced support for it</a> back in June. The Eclipse Orion project <a href="https://www.eclipsecon.org/europe2016/session/orion-and-language-server-protocol-perfect-coupleing-sponsored-ibm" target="_blank">is using it</a> for Java support, based on the new <a href="https://projects.eclipse.org/proposals/jdt-language-server" target="_blank">JDT Language Server project</a> that provides an LSP implementation built on top of the Eclipse Java development tools. The <a href="https://projects.eclipse.org/proposals/eclipse-lsp4j" target="_blank">Eclipse LSP4J</a> project provides a full Java implementation of the protocol, the <a href="https://projects.eclipse.org/proposals/eclipse-lsp4e" target="_blank">Eclipse LSP4E</a> project provides the glue code to plug LSPs into the IDE, and the new <a href="https://developers.redhat.com/blog/2016/11/24/deliver-support-for-new-languages-in-eclipse-ide-faster-with-generic-editor-and-language-servers/" target="_blank">Generic Editor</a> in the Eclipse Platform makes it easier to extend the desktop IDE to support new languages. Basically, all of these new initiatives have come into being since June 2016. It has been fascinating to watch the community rally around this new approach to IDE language enablement.</p>

<h3>Internet of Things</h3>
	<p>The Internet of Things will continue to be a significant growth area for the Eclipse community. Since its inception, the Eclipse IoT working group has been quietly working on building open source building blocks for IoT. In 2016 the group changed from creating building blocks to focusing on <a href="https://iot.eclipse.org/white-paper-iot-architectures" target="_blank">the three platforms needed for IoT solutions</a>. In particular, Eclipse IoT is the only community-led effort to create a complete cloud platform for the Internet of Things. New projects like Eclipse <a href="https://projects.eclipse.org/projects/iot.hono" target="_blank">Hono</a> and <a href="https://projects.eclipse.org/projects/iot.kapua" target="_blank">Eclipse Kapua</a> are creating open source cloud infrastructure that will enable a whole new class of solutions. In particular, Eclipse IoT is particularly well positioned to play a role in the industrial application of the IoT.</p>

<h3>Open Source in Industry</h3>
	<p>Eclipse technology will continue to be an important player in the industrial adoption of open source. Open source was first adopted by software vendors. More recently it has become an important part of the technology portfolio strategy for enterprises and industrials. We have numerous initiatives that are helping to further this trend. To mention just a couple, <a href="https://www.polarsys.org/" target="_blank">PolarSys</a> is spurring the adoption of open source tools like <a href="https://www.eclipse.org/papyrus/" target="_blank">Eclipse Papyrus</a> and <a href="https://polarsys.org/capella/" target="_blank">Polarsys Capella</a> for embedded systems. The <a href="https://projects.eclipse.org/proposals/simopenpass" target="_blank">openPASS</a> working group is bringing together companies like Volkswagen, Daimler and BMW to collaborate on the simulating traffic situations to predict the real-world effectiveness of advanced driver assistance systems or automated driving functions. Open source techniques for open collaboration are changing the face of pretty much every industry. Open source software really is eating the world.</p>

<h3>Diversification</h3>
	<p>Over the past couple of years, we have seen a significant diversification in the makeup of the Eclipse community in both projects and membership. Five years ago, the vast majority of our projects and membership were focused on desktop developer tools, written in Java, and using the Eclipse plug-in model. Today we have 28 projects in IoT, and 17 in our <a href="https://www.locationtech.org/" target="_blank">LocationTech</a> geospatial community, none of which are tools. The <a href="https://projects.eclipse.org/projects/technology.omr" target="_blank">Eclipse OMR</a> project is building cross-platform language runtimes. <a href="https://projects.eclipse.org/proposals/eclipse-microprofile" target="_blank">Eclipse MicroProfile</a> is bringing us into the realm of Java microservices. This trend has also been reflected in our membership. Almost all of the growth we have experienced over the past few years has been related to our new technology areas. This is one of my favorite trends, as there is nothing I personally enjoy more than watching the Eclipse community organically grow into entirely new technology areas.</p>

<h2>Innovative Eclipse Communities</h2>
<p>Those are a few trends that I expect to see for the Eclipse community in 2017. In addition to those, the Eclipse Foundation is continuing to be a wonderful place in which to grow community and to innovate. The <a href="http://science.eclipse.org/" target="_blank">Eclipse Science</a> and the <a href="https://www.locationtech.org/" target="_blank">LocationTech</a> working groups are not mentioned above as "new trends." Nor is the <a href="https://eclipse.org/modeling/" target="_blank">Eclipse Modeling</a> community, or about twenty other cool groups under the Eclipse banner. But they are all continuing to be dynamic, innovative communities that are building exciting new technologies The Eclipse Way.</p>

<p>I am very much looking forward to 2017, and all that it will bring for the Eclipse community.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/mike.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Mike Milinkovich<br />
            <a target="_blank" href="www.eclipse.org">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://mmilinkov.wordpress.com/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

