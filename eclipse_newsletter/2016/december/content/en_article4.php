<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>One of the most important aspects of a build tool is to make developers and build engineers highly productive. In 2017 the <a target="_blank" href="http://gradle.org">Gradle</a> team will bring many innovations that used to be reserved to the most advanced engineering companies to the mainstream.</p>

<h2>Faster Builds</h2>

<p>Take a moment to think about how often the same build tasks with the exact same inputs are executed inside your organization every day. First on the implementing developer’s laptop, when they introduce a new feature. Next, on each stage of the master pipeline when the feature is merged. Then on all other developers’ laptops when they pull the changes the next morning. And of course each time a developer issues a 'clean build' thereafter. Today, cross-build, cross-machine reuse is limited to libraries published in artifact repositories. What if we could achieve the same level of reuse we enjoy with libraries, but for any intermediate step of the build?</p>

<p>That’s exactly what Gradle’s <i>task output cache</i> will make possible. By thoroughly specifying all the inputs and outputs of a task, Gradle can compute a fingerprint of the inputs and store the corresponding outputs of the task in a shared cache. Subsequent build executions—no matter if on CI or on developer machines—can then look up that fingerprint and download the stored outputs instead of rerunning the task locally. For larger projects with time consuming tasks this could save each of your team members several minutes on each 'git pull', while speeding up CI builds at the same time.</p>

<p align="center"><a href="/community/eclipse_newsletter/2016/december/images/performancetest.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/performancetest_sm.png" alt="performance test gradle"/></a></p>
<p align="center">Performance test results for cached vs. non-cached clean builds on a large Java project.</p><br/>

<p>Another way to avoid unnecessary work is to better define the boundaries between your software components. Until now, making changes to a given library required all its downstream consumers to be recompiled. But what if you only changed an implementation detail, like a method body or a dependency used only internally? In these cases, recompilation is actually unnecessary, because none of the symbols being compiled against have changed. Gradle’s forthcoming <a target="_blank" href="https://melix.github.io/javaone2016-juggling-jigsaw/#/-simple-project">compile avoidance</a> and strong encapsulation features will provide you with a way to define your library’s API precisely, such that downstream consumers are recompiled only when that API actually changes. This new support dramatically speeds up the edit-compile-run loop, meaning you spend more time in the flow of development.</p>


<p>Finally, when work cannot be avoided, Gradle will soon default to doing it in parallel whenever it is safe to do so. All of these efforts together will make sure that developers can spend their time doing what they care most about: writing high quality software.</p>

<h2>Deeper Insight</h2>

<p>Besides slow builds, one of the biggest time sinks is troubleshooting when a build fails. Build engineers often spend long hours trying to reproduce a developer’s problem on their machine or trying to find out why an innocent change broke seemingly unrelated tests. Gradle <a target="_blank" href="https://gradle.com/"><i>build scans</i></a> streamline this process by providing a detailed view of everything that happened during a build in an easily sharable format. In 2017, build scans will become even more powerful and allow you to compare builds against each other, for instance in order to find the problematic transitive dependency that broke your integration tests. They will also provide a detailed performance timeline view, so you’ll always know what parts of your build need to be improved in order to make your developer’s lives better.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177490829?color=00c489" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>

<h2>Rich IDE Integration</h2>

<p>The focus for <a href="https://github.com/eclipse/buildship/blob/master/README.md">Eclipse Buildship</a> and IntelliJ IDEA’s Gradle integration so far was on synchronizing project settings and running tasks—things that application developers do on a daily basis. Next year, we are going to bring more productivity to the <i>build authors</i> by allowing them to write their build scripts in statically-typed <a target="_blank" href="https://kotlinlang.org/">Kotlin</a>, with syntax highlighting, auto completion, code navigation and all the other features that you expect from a modern IDE.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/LfNVznLLfos" frameborder="0" allowfullscreen></iframe>
</div>

<p>It is going to be an exciting year. Be sure to keep an eye on our <a target="_blank" href="http://blog.gradle.org">blog</a> so you don’t miss out on the fun!</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/stefan.png"
        alt="Stefan Oehme" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Stefan Oehme<br />
            <a target="_blank" href="https://gradle.org/">Gradle Inc.</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/stefanoehme">Twitter</a></li>
           	<li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/oehme">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

