<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>The Eclipse IDE is a mature and modular platform for all kinds of software tools. It is used by millions of engineers and developers getting their work done every day. Reaching this state of maturity has been a huge effort for the Eclipse community and took more than 15 years. The immense care for code quality, pluggability, and API stability has always been one of the main reasons for me to leverage Eclipse technology in many projects.</p>


<h2>The World Has Changed</h2>

<p>While the Eclipse Platform grew naturally over the years through its extensible plug-in model, it still requires a set of technologies that constrains how an extension is developed.</p>

<p>At the basis, there is the requirement for Java, which seems to be fine as Java is still one of the most popular programming languages in the world. But of course, it is problematic if you want to reuse an Eclipse technology that is implemented in another language. Furthermore, it is unnatural to require Java, if you are developing programming language support for another language, such as C/C++.</p>

<p>An even bigger problem is the UI technology. Over the past few years there have been many discussions in the Eclipse community about how to get over the limitations of the Standard Widget Toolkit (SWT) or to move to a modern UI technology. With the arrival of e4 Eclipse got support for styling UIs with CSS. Since that is incomplete due to technical limitations in CSS, there have been efforts to replace SWT or at least the rendering underneath with other technologies, such as JavaFX or OpenGL. Even Eclipse RAP is an attempt to preserve what we have by porting SWT to a modern UI technology (web).</p>

<p>While these efforts would solve a few issues, they still require us writing our code against the aged SWT API and concepts. If you are building a product, the UI is probably the last place where you want to make compromises.</p>

<p>Other movements in the community realized that sticking to SWT would possibly be problematic in the long run and started writing new IDEs from scratch. Eclipse Che and Eclipse Orion are both IDEs running in the browser and are implemented using web technology. While this clean slate approach gives you the most flexibility, it also means you need to rewrite everything, or at least the parts that you still consider important.</p>

<p>To be honest, SWT has done a fairly good job over time, but it didn't catch up with the alternative technologies. Today I can only recommend that everyone has a look at web technologies for building UIs. Frameworks like <a target="_blank" href="https://facebook.github.io/react/">React</a> or <a target="_blank" href="https://angular.io/">Angular 2</a> provide new, exciting programming models for developing beautiful user interfaces. <a target="_blank" href="http://electron.atom.io/">Electron</a> even lets users build desktop applications with web technologies, and through a browser widget, it’s possible to embed web components in Eclipse RCP applications, too.</p>


<h2>… And It Changes Faster</h2>

<p>The Eclipse Community, as well as all other IDE vendors, are having a hard time keeping track of the latest developments. Especially in the web technology space, we see new languages and frameworks popping up every other Monday, and it is almost impossible to build a plug-in for them before it is outdated again. To build Eclipse tool support for a language or framework, often we need to reimplement a certain amount of semantics in the plug-in, which means duplicating code and information in the plug-in that is already present in the technology itself. As a result, tool support will always lag behind if we cannot find a sustainable way that lets the language and framework developers themselves care about it.</p>

<h2>Protocols Are Here to Help</h2>

<p>Microsoft is aware of these advancements, too. Their latest cross-platform development tool, Visual Studio Code (VS Code), is designed in a way that allows users to reuse existing code written in any language. The idea is to use platform-independent protocols to communicate with external processes. With such a separation you have a clear interaction layer, that separates two components from each other in distinct ways. It not only allows the implementation of a VS Code extension in any programming language, but it also lets users use those extensions in other UIs or IDEs, as long as they understand the same protocol.</p>

<p>The separation of extensions in external processes has other benefits, too. For instance, in VS Code a plug-in cannot tear down the whole editor. It is also impossible to block the rendering thread through such an extension.</p>


<p>VS Code has defined two such protocols so far, a <a target="_blank" href="https://github.com/Microsoft/vscode-debugadapter-node">debug protocol</a> to interact with running processes and a <a target="_blank" href="https://github.com/Microsoft/language-server-protocol">language server protocol (LSP)</a> to interact with components that can provide language-specific services such as completion, refactorings and so on.</p>


<h2>The Language Server Protocol @ Eclipse</h2>

<p>In April my team was working on a product based on Eclipse Che. For that project, we needed to enhance Che with tool support for a language implemented in Eclipse Xtext. Since Xtext already supported the Eclipse IDE, Intellij IDEA, and several web editors at that time it had grown a big fat with all those platforms and adding support for yet another editor seemed wrong. Instead, I was looking for a more sustainable way to make Xtext future-proof without the need to add and maintain new editor platforms every six months.</p>

<p>A ubiquitous protocol, like VS Code's LSP, would solve this issue, especially when it is popular enough that the editor vendors would support it natively. Given the credibility and impressive track-record of the team behind VS Code (they worked on several IDEs before, including the Eclipse IDE) and the backup of Microsoft, it seemed like a good idea to push this forward.</p>

<p>So I discussed with the Che developers to see if they were interested in supporting the LSP and built a proof-of-concept for them. Luckily, they were very open and interested and took it over to fully implement it. Today, Eclipse Che 5.0 comes bundled with support for several additional programming languages based on the <a target="_blank" href="https://medium.com/eclipse-che-blog/eclipse-che-5-0-announced-at-checonf-2016-8d7759aca777#.tcsyddsy1">LSP</a>.</p>

<p>At the around same time, Red Hat and IBM joined forces to work on a JDT-based language server, which you can already find in the VS Code's extension <a target="_blank" href="https://marketplace.visualstudio.com/items?itemName=redhat.java">marketplace</a>. With that, they leveraged the hard work that was put into JDT and use it in other IDEs and editors. For instance, just back in November, IBM presented a prototype at EclipseCon Europe that showed <a target="_blank" href="https://www.eclipsecon.org/europe2016/session/orion-and-language-server-protocol-perfect-coupleing-sponsored-ibm">Eclipse Orion</a> using the JDT language server.</p>

<p>The <a href="https://projects.eclipse.org/projects/technology.lsp4e">Eclipse LSP4E</a> project aims at bringing language servers to the Eclipse IDE. Language support for C#, JSON Schema, and <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/wiki/Protocol-Implementations">much more</a>, will soon be connected to our favorite IDE through this.</p>

<p>Finally, with <a target="_blank" href="http://typefox.io/xtext-2-11-beta-1-is-here">Eclipse Xtext 2.11</a> supporting the LSP, we now have a mature language development framework that lets you build language servers quickly. In parallel to the LSP support, we have split-up Xtext into smaller modules and the new Xtext-core module only contains the platform-independent code to implement a language server.</p>

<p>Beyond all these efforts, we have the <a target="_blank" href="https://github.com/eclipse/lsp4j">Eclipse LSP4J project</a>, which defines Java types and signatures of the LSP. It is used by the various Eclipse technology efforts listed above and by other language servers implemented in Java.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2GqpdfIAhz8" frameborder="0" allowfullscreen></iframe>
</div>

<h2>Conclusion</h2>

<p>The adoption of the LSP is a huge win-win for many efforts within the Eclipse community. On one side, it allows using the rich language support infrastructures from projects like JDT and Xtext in other tools and on the other side, it provides an easy way for the Eclipse IDE to support new languages and frameworks in the future.</p>

<p>Furthermore, web technologies have become the most promising UI technology for both desktop and web applications. Vendors that have chosen Eclipse RCP as a basis in the past will more likely resort to a web-based alternative in the future. Using platform-independent protocols will allow us to keep using core Eclipse technologies in these new exciting environments.</p>

<p>The LSP has only started to shake up a couple of Eclipse technologies giving new perspectives for future developments. I’m certain there will be even more projects joining in on this effort in 2017. So stay tuned or better engage!</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/sven.jpg"
        alt="Sven Efftinge" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Sven Efftinge<br />
            <a target="_blank" href="http://typefox.io/">TypeFox</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/svenefftinge">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.efftinge.de/">Blog</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

