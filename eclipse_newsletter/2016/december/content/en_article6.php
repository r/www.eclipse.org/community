<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

   <p>At Obeo, we believe that modeling is the right way to help IT and industry engineers collaborate efficiently on the design of their smart products. Our innovative approach consists of building specific modeling tools that completely suit users' business domains. Modeling is a means to an end: by using modeling technologies we make sure that such a tool can be defined faster, as well as deployed and maintained better.</p>

<p>To achieve this goal, we develop highly customizable open source software, such as <a href="http://www.eclipse.org/sirius/">Eclipse Sirius</a>. We consider that a modeling tool must be adaptable, flexible, and user-friendly. This year again, we worked hard to focus on that!</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/environment.png" alt="environments"/></p>

<p>As you may know, Sirius is the easiest way to get your own modeling tool and to do it rapidly. Indeed, it dramatically reduces the time spent on creating domain-specific modeling workbenches thanks to an interpretation mode that allows very short feedback-loops. In 2016, we invested time in the Eclipse Ecore Tools project to facilitate the definition of modeling languages by providing a very intuitive and powerful Ecore graphical editor.


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/graphicaleditor.png" alt="graphical editor"/></p>

<p>It’s been three years since Sirius was made open source and the community is growing every year. A few weeks ago, part of this community gathered for the second edition of <a target="_blank" href="http://www.siriuscon.org/">SiriusCon</a> in Paris. More than 100 attendees coming from more than 10 different countries participated in this international conference on graphical modeling. If that is not proof that Sirius is a worldwide technology, we don’t know what is!</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/siriuscon.jpg" alt="siriuscon 2016"/></p>


<p>At SiriusCon, we had the opportunity to present one of the latest key features of Sirius: <a target="_blank" href="http://www.eclipse.org/sirius/whatsnew/whatsnew4-1.html">the properties view</a>. It turned out that with all the improvements Sirius brings to the specification of the modeling view, the bottleneck was in the definition of the properties view to be linked to each graphical element. The Obeo team addressed that problem.</p>

<p>Now, Sirius provides an integrated way to define properties views in the same way the user is used to defining them in other parts of the designer: no need for coding - it is dynamic and query based. In addition, with <a target="_blank" href="http://www.eclipse.org/sirius/download.html">Sirius 4.1</a>, the user is now able to specify exactly how the properties view should be represented.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/query_sm.png" alt="image queries"/></p>

<p>Sirius 4.1 has default rules based on the type of the elements defined in the metamodel. For example, if the user has defined a string attribute in his metamodel, it will be automatically represented by a text widget; a boolean will be represented by a checkbox, and so on. If the default properties view does not fit the user's needs, no problem: it can be customized.</p>


<p align="center"><a href="/community/eclipse_newsletter/2016/december/images/designfile.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/designfile_sm.png" alt="design file"/></a></p>

<p>In 2017, we want to go further building on the same fundamentals. We will focus on technologies that are <b>real-world ready, adaptable, and give instant feedback</b>.</p>

<p>We’ve been working on the codebase for months already, but next year will see a nice scalability offspring: a core runtime that can scale any number of diagrams or their size while keeping everything consistent like it currently does today. And that’s only under the hood, in a more visible way we’ll hunt for every break a user might encounter in the workflow of using a modeling tool. Here is an example when the user ends up trying to set up diagrams and models while not being the modeling perspective:</p>

<p align="center"><a href="/community/eclipse_newsletter/2016/december/images/modelingperspective.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/modelingperspective_sm.png" alt="modeling perspectives"/></a></p>

<p>The model content or diagrams are not visible in the package explorer yet, the Eclipse IDE doesn't have an editor for <code>.aird</code> files, and double clicking it will not help. We plan to address this next year by providing a default editor for <code>.aird</code> files.</p>

<p>This editor gives us a whole new dimension to present your tooling features and is the starting point to a project that will grow during the next few years: making the tooling aware of the process to achieve better usability.</p>

<p>Hear me well, the word “aware” is picked with care and “process driven” is banned in this context. In the end the user gets to decide and the tool should never get in the way, but by making the tool aware of the process or methodology we can make it more helpful. This will first be translated by the integration of the <i>Activity Explorer</i> which got contributed to Amalgam by Thales last year. This allows anyone to define the process activities without writing a single line of code, in the very same way you can currently define diagrams, tables or the properties view, right into the <code>.odesign</code> file.</p>


<p align="center"><a href="/community/eclipse_newsletter/2016/december/images/views.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/views_sm.png"/></a></p>


<p>Other improvements especially focused on the diagrams are in the works. Here is a mockup of a new mechanism to enrich existing diagram editors, you can think of it as “decorators on steroids”. Follow this <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=506259">bug</a> if you are interested.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/diagrams.png"/></p>

<p>We are in a continuous evolution. We strive to continually improve the user experience and to streamline the complete model environment building process. This means that we  have our hands in many Eclipse projects, from Ecore Tools, EMF Compare, Acceleo, Amalgam, EEF to Sirius and improve each of those. We are building various technologies independently while making sure they integrate seamlessly in the final product.</p>

<p align="center"><a href="/community/eclipse_newsletter/2016/december/images/product.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/december/images/product_sm.png"/></a></p>

<p><a href="https://polarsys.org/capella/">Capella</a>, one of the solutions provided by Eclipse PolarSys Working Group, is one example of a product aggregating such technologies. It is already a field-proven Model-Based Systems Engineering (MBSE) workbench.</p>

<p>Capella was developed by Thales to help engineers formalize system specifications and master their architectural design. It is sustainable and adaptable and has already been successfully deployed in a wide variety of industrial contexts (aerospace, communication, transportation, etc.). This is a modeling environment focused on a domain tooling a methodology, based on many of the technologies mentioned before. It is 100% open source. <a href="https://polarsys.org/capella/">Check out what can be done with it</a>!</p>

<p>Is modeling in 2017 going better, faster, stronger? Challenge accepted! The Obeo team is up to the task! We will do our possible to reach a new level and deliver cutting edge modeling tools.</p>
To achieve anything we need the support of our enthusiastic community. We know that in 2017 we will be able to rely on the Eclipse users as we have always done. We want to get closer to the users and receive fine-grained feedback to improve our technologies even more. We are currently working on a new online (and IRL!) way to deal with that… but you will have to stay tuned to get more information. Keep your eyes peeled for the upcoming <a target="_blank" href="http://www.siriuscon.org/">SiriusCon</a>, it’s the best place to interact with us!</p>

 <div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/cedric.jpg"
        alt="Cedric Brun" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         Cédric Brun<br />
        <a target="_blank" href="https://www.obeo.fr/en/">Obeo</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://cedric.brun.io/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/bruncedric">Twitter</a></li>
        <?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

