<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

		<p>Since June 2015, with the Eclipse Mars simultaneous release, all Eclipse packages were shipped with the <b>Automated Error Reporting client</b> (called AERI). Since then, two minor releases of Eclipse Mars were made (Mars.1 and Mars.2) and its successor, Eclipse Neon, was released on June 22 of this year – with AERI, of course. Now is a good time to look back at how the Automated Error Reporting Initiative has helped to fix Eclipse project bugs.</p>
		<p>Ever since AERI became a part of the Eclipse packages, eclipse.org has seen a steady stream of incoming error reports. Eclipse users currently send around <b>70,000 reports per week</b>. That is a whopping total of <b>3 million reports</b> in the past 12 months!</p>
		<p>Of course, not every error report is indicative of a new problem. Often multiple users encounter and report the same problem. For the initial Mars release the Eclipse projects received error reports for 17,253 distinct problems – and immediately began fixing them. One thousand problems were fixed in the three months prior to the Mars.1 release. With Mars.2, we saw the number of problems drop significantly, from 16,266 to 11,967.</p>

			<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/problemsfixed.png"
        alt="problems fixed" /></p>

		<p align="center"><i>Open Problems per Release. From Mars.0 to Mars.2, 7.800 problems have been fixed. Overall the number of known problems dropped by more than 30%.</i></p>
		<p>This means that the three million error reports that Eclipse projects received for Eclipse Mars had a visible effect on the overall quality of the releases. All together, more than 7,800 problems have been fixed over the course of the three Mars releases.</p>
		<p>Although most users don’t bother reporting problems they encounter in their day-to-day work using classic bug trackers like Bugzilla, they are apparently still eager to help out the various Eclipse projects – if only reporting problems is made easy enough. The statistics, 70,000 reports per week, show that using AERI makes reporting errors very convenient. Many users even take the time to send comments with their error report to describe the situation in which the problem occurred.</p>
		<p>While these comments are usually short, they are helpful when fixing the bugs. As it turns out, problems <b>with a user comment</b> are more than <b>twice as likely to be fixed</b> than problems without a user comment. In other words, it pays off to spend a bit more time providing some steps to help the project developers reproduce the issue in question. The best part is, users don’t even need to log into Bugzilla for that! They can submit the entire error report directly from within their Eclipse IDE.</p>

	<h2>Leveraging Error Reporting</h2>

		<p>Along with the comments, the detailed technical information (stack traces, plug-in versions, operating system details) included in the report help Eclipse projects diagnose and fix problems faster than ever before. Some of the projects really stepped up to the plate in fixing a large percentage the problems reported by their users. The projects that stand-out are: Code Recommenders, Xtext, and most all Oomph, a project that like AERI made its big entrance with Eclipse Mars.</p>

			<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/projects.png"
        alt="projects" /></p>

		<p align="center"><i>Closed vs. open (unreviewed) problems per project. Top 15 projects (in absolute numbers) fully embracing your error reports.</i></p>
		<p>Even though other projects may have more open problems than closed problems, the Automated Error Reporting Initiative is still a success for them. As Matthias Sohn, the project co-lead of JGit and EGit, notes “the provided details and error statistics are a great help to fix these bugs and to decide which bugs to fix first. Automated error reporting helped us to quickly detect and fix dozens of bugs in JGit and EGit.”</p>
		<p>To summarize: the Automated Error Reporting Initiative has been a clear success for Eclipse Mars. Many thanks go to the 350,000 Eclipse IDE users who helped committers identify and fix problems! Keep up reporting all errors you encounter! And remember to include a comment.</p>
		<p>But what’s in store for Eclipse Neon?</p>

	<h2>AERI for Eclipse Neon</h2>

		<p>Based on user feedback and AERI’s initial success in Eclipse Mars, automated error reporting has been made available to any Eclipse plug-in through a simple extension point with Eclipse Neon. In other words, with the June release it’s even easier for an Eclipse plugin (whether it be commercial, private, or open-source project) to receive error reports and find out where their software breaks in the field. This allows developers to get closer to the user and make their project even better.</p>

	<h2>Get Started Today</h2>
		<ul>
			<li><a target="_blank" href="https://www.youtube.com/watch?v=TDSv9Tm7hGA">Watch our webinar</a> to learn more about how to set up error reporting for your plugins.</li>
			<li><a target="_blank" href="https://demo.ctrlflow.com/login">Login to our demo server</a> for a tour of the tool.</li>
			<li><a target="_blank" href="https://ctrlflow.com/automated-error-reporting/">Sign up</a> for the free Automated Error Reporting Service hosted by Codetrails and get started in less than an hour!</li>
		</ul>

		<p>Many thanks again to all reporters and all bug fixers. Congratulations to all of you for creating a great Eclipse Neon release!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/andreas.jpg"
        alt="Andreas Sewe" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Andreas Sewe<br />
        <a target="_blank" href="http://www.codetrails.com/">Codetrails GmbH</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/andreas-sewe-900a76a0">LinkedIn</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/sewe">GitHub</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
