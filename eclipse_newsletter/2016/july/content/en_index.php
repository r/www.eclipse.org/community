<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2016.07.21</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="30%"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContent">
                            <img
                            src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxanne.jpg" />
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="55%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Editor's Note</h3>
                         	<p>As you know, <a style="color:#000;" href="https://eclipse.org/neon">Eclipse Neon</a> was released at the end of June and there is still so much to talk about!</p>
                            <p>This month, the newsletter features four Eclipse projects that make Eclipse Neon shine bright. Scroll down to read about Cloud Foundry and Docker Tools for Eclipse, Buildship, the Gradle integration for Eclipse, and Automated Error Reporting.</p>
                            <p>I hope you're reading these great articles on a beach :). Enjoy the sun!</p>
                          	<p>Roxanne<br>
                          	<a target="_blank" style="color:#000;" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article1.php"><h3>Cloud Foundry Tools for Neon</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                             <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/july/images/cloudfoundry.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Discover how to use Eclipse Tools for Cloud Foundry out of the box to connect to Cloud Foundry and deploy applications and more.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article2.php"><h3>Docker Tooling for Neon</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                             <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/july/images/docker.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse Docker Tooling has many great new features for Eclipse Neon. Read all about them in this article.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article3.php"><h3>Automated Error Reporting</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                             <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/july/images/aer.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Automated Error Reporting was launched with Eclipse Mars in 2015 and has contributed greatly to the improvement of Eclipse Mars, as well as Eclipse Neon. Find out how.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article4.php"><h3>Buildship into the Future</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                             <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/july/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/july/images/buildship.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Buildship, the Gradle integration for Eclipse, is steadily improving your developer experience. Find out about the exciting upcoming features in this article.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // --> <!--
                        The flexible container has a set width
                        that gets overridden by the media query.
                        Most content tables within can then be
                        given 100% widths.
                     -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                            The content table is the first element
                              that's entirely separate from the structural
                              framework of the email.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse News</h3>
                            <ul>
                              <li><a target="_blank" style="color:#000;"  href="https://www.eclipse.org/org/press-release/openpass.php">New Proposal for OpenPASS Working Group</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://mmilinkov.wordpress.com/2016/06/29/overhauling-ip-management-at-the-eclipse-foundation/">Overhauling IP Management at the Eclipse Foundation</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/20160622_neon.php">Eclipse Neon Release Train Now Available</a></li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/capra">Capra</a></li>
                            	<li><a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">More project activity</a></li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="http://che.eclipse.org/release-notes-eclipse-che-4-4/">Eclipse Che 4.4</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.elk/reviews/0.1.0-release-review">ELK - Eclipse Layout Kernel 0.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/VIATRA/Releases/NewAndNoteworthy1.3">VIATRA 1.3</a></li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
  

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event.<a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://eclipsesummit.in/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/june/images/india.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p><a target="_blank" style="color:#000;" href="http://eclipsesummit.in/">Eclipse Summit India</a><br>
                            Aug 25-27, 2016 | Bangalore, India</p>
                            <p><a target="_blank" style="color:#000;" href="http://www.vogella.com/training/eclipse/eclipsercp_en.html">Eclipse Rich Client Platform (RCP) Training</a><br>
                            Oct 10-14, 2016 | Hamburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/iot">EclipseCon IoT Day 2016</a><br>
                            Oct 25, 2016 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/">EclipseCon Europe 2016</a><br>
                            Oct 25-27, 2016 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://devoxx.us/">Devoxx US</a><br>
                            Mar 21-23, 2017 | San Jose, United States</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

 
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <a target="_blank" href="https://www.eclipsecon.org/europe2016/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/june/images/ece.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a>
                          </td>
                          </tr>
                      
                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
  


      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 100%; margin: 0 auto; margin-top: 30px; text-align: center;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
