<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
		<img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/gradle-mechaphant.png" alt="mechaphant"/><p><a target="_blank" href="https://projects.eclipse.org/projects/tools.buildship">Buildship</a> is the official <a target="_blank" href="https://gradle.org/">Gradle</a> integration for Eclipse. It helps Eclipse users synchronize Gradle builds with their workspace, execute tasks without leaving their IDE and also serves as a reference implementation for how to build great IDE support for the Gradle Build Tool.</p>
		<p>Buildship has come a long way since its inclusion into Eclipse Mars.1. The user experience has been improved significantly, with more information defined in the Gradle build being applied by Eclipse Buildship. Synchronization performance has improved by orders of magnitude. And there are exciting new features coming up, with Buildship 2.x, that will keep up your development flow even better!</p>

	<h2>Buildship 1.0.x - Polished Features</h2>

		<p>Buildship 1.0.17 is our most recent release and comes with many improved features. It is a drop-in replacement, so go ahead and update to get the following highlights.</p>

	<h3>More Accurate Project Synchronization</h3>

		<p>Our vision for Buildship has always been: “the build is the single source of truth”. Developers should not spend time manually configuring Eclipse projects. Instead, everything should just work out-of-the-box after importing a Gradle project. With each new release of the Gradle Build Tool and of Buildship, we get a step closer to realizing this vision.</p>
		<p>As of Gradle 3.0, you will be able to configure everything about your <i>.project</i> and <i>.classpath</i> files via the Gradle DSL. This will make it possible to extract common configuration into Gradle plugins and apply them to all your projects.</p>
		<p>Thanks to an Open Source Partnership with one of our customers, there is now initial support for <a target="_blank" href="https://discuss.gradle.org/t/buildship-1-0-16-is-now-available/18056">Eclipse WTP in Buildship</a>. The classpath entries of web projects are correctly categorized into deployed and non-deployed dependencies.</p>

	<h3>Improved Usability</h3>

		<p>Buildship now allows you to run single test cases through Gradle, just like you are used to with the JUnit plugin. This has the added benefit that all additional logic defined in your Gradle tasks - like setup and teardown of the test environment - is executed as part of running the tests.</p>
		<p>In the Project Explorer, you can now choose to show or hide the build folder as well as sub-project folders. And, the Gradle synchronization can be triggered from several convenient locations, like the context menu of the build script.</p>
		<p>By popular demand, the Buildship configuration files no longer contain user-specific paths, so you can check them into your version control system.</p>

	<h3>Performance Matters</h3>

		<p>Every time you add a dependency or change a setting in your Gradle build, you want to synchronize your workspace projects. This should not break your flow, which means it needs to be very fast. We have invested heavily into synchronization performance, to the point where our 500 subprojects example build is synchronized in less than 10 seconds. Further optimizations are still possible. The Gradle Build Tool team and the Buildship team are continuously improving workspace synchronization performance in Buildship, as well as configuration and dependency resolution performance in the Gradle Build Tool to make the import even faster.</p>
		<p>What’s more, Buildship now caches the information collected from the build, in particular the classpath entries, such that no synchronization is needed on startup. You can immediately start working on your projects once your Eclipse workspace has started.</p>

	<h2>Buildship 2.x - Exciting New Features</h2>

		<p>For the next major upgrade, we want to further improve the developer experience beyond just project synchronization accuracy and performance. Working with multiple repositories, editing build scripts, and getting more insights into your build will be central themes of the Buildship 2.x releases.</p>

	<h3>Composite Build Support</h3>

		<p>If you work with a multi-repository setting, then you probably know this pain: there is a bug in one of your libraries, but it only occurs in one specific application. You write a fix and are ready to try it out in your application. The current state of the art is to:</p>

		<ul>
			<li>publish the library to your local repository</li>
			<li>update the application’s build script to consume that local artifact</li>
			<li>run some tests</li>
			<li>repeat until the bug is fixed</li>
		</ul>
		<p>This is time consuming and breaks your flow.</p>

		<p>To solve this problem, both on the command line and in the IDE, Gradle 2.13 introduced the concept of a Composite Build. It allows you to handle several distinct Gradle builds as if they were one big multi-project build. Binary dependencies are automatically replaced with project dependencies. Any changes in upstream projects become immediately visible to dependents without intermediate publishing steps.</p>
		<p>Buildship 2.x will be the first tool to leverage the new Composite Build functionality. Buildship will automatically replace external dependencies with project dependencies. Also, it will de-duplicate project names across all builds in the workspace. Finally, any task executed in Buildship will be aware of the other imported projects and automatically build those first, if needed.</p>

	<h3>Kotlin Support</h3>

		<p>One of the most requested features for Buildship is to provide great support for editing Gradle build scripts. Unfortunately, the Groovy support for Eclipse is in an unmaintained state and therefore not something we can build upon. Thankfully, Groovy is no longer the only language available to write your Gradle build scripts in. In collaboration with JetBrains, we are <a target="_blank" href="https://gradle.org/blog/kotlin-meets-gradle/">embracing Kotlin</a> as the future preferred language for writing Gradle build scripts.</p>

			<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

		<p>The engineers at JetBrains are committed to bringing great <a target="_blank" href="https://marketplace.eclipse.org/content/kotlin-plugin-eclipse">Kotlin support</a> to Eclipse; the plugin is already available on Eclipse Marketplace. Based on their work, Buildship will be provide a smooth editor experience for Gradle build scripts, with syntax highlighting, auto completion, code navigation, and all the other goodness you have come to expect from an Eclipse editor.</p>

	<h3>Build Scan Integration</h3>

		<p>Gradle has become a platform consisting of the <a target="_blank" href="https://gradle.org/">Gradle Build Tool</a> and the <a target="_blank" href="https://gradle.com/whats-new-gradle/">Gradle Cloud Services</a>. The first available service is the <a target="_blank" href="https://gradle.com/">Build Scan Service</a>. Build Scans provide deep insights into your build. They allow you to understand, debug, and optimize your Gradle builds. Organizations can use Build Scans to analyse all executed builds, both on CI servers and developer machines. They allow you to make data-driven decisions about what part of the build automation to improve next.</p>

		<a href="/community/eclipse_newsletter/2016/july/images/scanservices.png"><p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/scanservices_small.png"
        alt="gradle scan services" /></p></a>

		<p>Buildship will provide a smooth integration with the Build Scan Service, allowing you to easily create Build Scans and quickly navigate to their visual presentation. When encountering a problem, instead of painstakingly explaining your build setup, your environment, and your build issues to a colleague, you will be able to share a Build Scan that provides deep insights into your build with one single click.</p>

	<h2>Try It Now</h2>

		<p>Buildship 1.0.15 is included in several packages of the Eclipse Neon release. For the best developer experience, update to the latest 1.0.x release via the <a target="_blank" href="https://marketplace.eclipse.org/content/buildship-gradle-integration">Eclipse Marketplace</a>. To try out Buildship 2.x and the new Composite Build functionality, install the latest <a target="_blank" href="http://download.eclipse.org/buildship/updates/e45/milestones/2.x/">milestone</a>. Make sure you are using at least Gradle 2.14 for dependency substitution to work.</p>

	<h2>Provide Feedback</h2>

		<p>We are always happy to receive feedback via the <a target="_blank" href="https://discuss.gradle.org/c/help-discuss/buildship">Buildship forum</a> or reports on <a target="_blank" href="https://bugs.eclipse.org/bugs/buglist.cgi?component=General&list_id=14779838&product=Buildship&resolution=---">Bugzilla</a>. Many of the improvements that have made it into Buildship releases were inspired by feedback from our valued community. The forum is also the place to go if there is a <a target="_blank" href="https://discuss.gradle.org/t/buildship-open-source-partnership-opportunities/18228">feature</a> that you would like to help develop.</p>

	<h2>Accelerate the Development of New Features</h2>

		<p>Many of the great features in the Gradle Build Tool and in Buildship were developed through Open Source Partnerships. If there is a <a target="_blank" href="https://discuss.gradle.org/t/buildship-open-source-partnership-opportunities/18228">feature</a> you would love to see in Buildship very soon and you are willing to sponsor it, please do not hesitate to <a target="_blank" href="https://gradle.org/contact/">reach out to us</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/stefan.png"
        alt="Stefan Oehme" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Stefan Oehme<br />
        <a target="_blank" href="https://gradle.org/">Gradle Inc.</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/stefanoehme">Twitter</a></li>
       	<li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/oehme">GitHub</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

