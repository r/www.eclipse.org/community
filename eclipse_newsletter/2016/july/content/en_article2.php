<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	    <p>Eclipse Neon (4.6.0) marks the second major release (2.0.0) of the Eclipse Docker Tooling. You can get the tooling
	    from within the Neon update sites themselves that ship with Eclipse, the
	    <a href="https://marketplace.eclipse.org/content/eclipse-docker-tooling">Eclipse Marketplace</a>, or from the
		<a target="_blank" href="http://download.eclipse.org/linuxtools/update-docker-2.0.0">Linux Tools
	    Project's Eclipse Docker Tooling</a> update site.</p>

		<p>While we certainly wanted to provide newer features for users, a lot of our efforts went into fixing many different
		bugs and usability issues that came up since our initial Mars release, and that hadn't been addressed in any of the
		subsequent minor releases (Mars.1, and Mars.2). In total,
		we fixed <a href="https://bugs.eclipse.org/bugs/buglist.cgi?bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&classification=Tools&component=Docker&list_id=14751514&order=Importance&product=Linux%20Tools&query_format=advanced&resolution=FIXED&target_milestone=5.0.0">56 reported bugs</a>
		for the 2.0.0 release (part of the 5.0.0 Linux Tools release). As a result the tooling has become more stable.</p>

		<p>However there are a few noteworthy features that have made the simultaneous release with Eclipse Neon.</p>


	<h2>Dockerfile Editor</h2>

		<p>We've wanted to provide some kind of Dockerfile editor as part of our tooling for a <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=467558">while now</a>,
		so it was great to receive such a contribution from Tobias Verbeke of Open Analytics.</p>

		<p>The editor provides users with content assist and hover help for various Dockerfile commands along with some basic syntax highlighting.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/dockerfile-editor.png"/></p>


	<h2>Executing Shell in a Container</h2>

		<p>Often, containers are started with some process running in the foreground making it impossible to get a shell environment
		into a container. By using the docker-exec endpoint of the remote API, and launching we're able to get a shell into
		pretty much any running container.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-exec-shell.png"/></p>


		<p>This is provided through a Target Management (TM) Terminal which provides a better experience than the standard Eclipse Console.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-exec-shell-terminal.png"/></p>

	<h2>Improved Connection Support</h2>

		<p>While users will often create Docker daemon connections that are active, these connection can be taken down, or go
		offline for various reasons. Such a state should be reflected to the users, with the option to remedy the situation if possible.</p>

		<p>We now make the connection icons for disabled connections appear grey in the Docker Explorer View, with no underlying
		images/containers being shown from any view (Containers, Images, Explorer views). Users can try enabling
		the selected connection by clicking the enable connection button from the Explorer View's toolbar.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-disabled-connection.png"/></p>

		<p>In additon to this, connections may now be edited  in the event that certain information has changed
		(eg. IP address has changed). This is done by right clicking on a selected connection in the Explorer View and clicking the "Edit..." option.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-edit-connection.png"/></p>


	<h2>Pushing, Pulling, and Searching for Authenticated Registries</h2>

		<p>We've supported pushing, pulling, and searching against the Docker Hub registry, but never against 3rd party registries,
		or any kind of account authentication. In Neon we now add support for pulling, pushing, and searching against standard
		registries (v1, and v2) along with the ability to store authentication credentials for the operations.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-registry-accounts.png"/></p>

		<p>As part of the pull, or push operation, users can specify the host (and port) on which the registry may be reached.
		For a pull, users can then specify the image directly, or query the registry using the Search Wizard.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-pull-image.png"/></p>

		<p>For push operations, users need not qualify the tag they wish to push with the location of the registry, as this is done automatically.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/docker-push-image.png"/></p>

		<p>These are just some of the new features available in Neon. We've received a lot of feedback and we're hoping to use it to improve
		some of our newer features as well as in deciding what we need to be working on next. If you find yourself using Docker
		images/containers, we would recommend you try out this plugin as it's sure to simplify various use cases.
		For more detailed information on features, be sure to have a look at our
		<a href="http://wiki.eclipse.org/Linux_Tools_Project/Docker_Tooling/User_Guide">Eclipse Docker Tooling User Guide</a>.

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/rgrunber.jpg"
        alt="Jonas Helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Roland Grunberg<br />
        <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/rgrunber">GitHub</a>
        </li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
