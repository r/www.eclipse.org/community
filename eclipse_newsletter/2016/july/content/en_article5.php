<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Usability - aka Speeding Up Development</h2>
	<h3>Use Quick Access</h3>

		<p>Recently Wayne Beaton blogged about the usage of the <a target="_blank" href="https://waynebeaton.wordpress.com/2016/04/06/quick-access-to-eclipse-ide-features/">Quick Access in Eclipse</a>.</p>

		<p>It is a really useful little helper to reach commonly used parts of the IDE such as views, editors, commands, perspectives, preferences, and menus really fast.</p>

		<p>So if you are used to open a View by using the menu > Window > Show View > Other and then choosing a view in a dialog (navigating through menus can be time consuming). Then you should really try to simply press Ctrl + 3 and type the name of the desired view, which is a lot faster.</p>

		<p>Right before Eclipse Neon has been released some additional enhancements have been added to the Quick Access.</p>

		<p>You can now search for particular categories in the Quick Access. To only get View results in the Quick Access you can type “Views:” and only get Views as Quick Access Proposals.</p>


    <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

Also the hints right at the bottom of the Quick Access have been improved. So by pressing Ctrl + 3 once more all results of a certain category a shown.

But please do not stop at the View level as many developers do, because as mentioned before there is a lot more like opening “New Wizards” for Java Projects, Classes and more. For example  “New:” is also considered as category.


        <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

Coding Java

Not really a Platform topic, but we´re all Java Developer´s, right? ;-)
So how do you create a Java Class? No, do not touch the mouse! From the previous chapter we learned that Ctrl + 3 is to way to got and using key bindings is always faster!

So we press Ctrl + 3 and type “new class” and hit Enter.

<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

Then the New Java Class Dialog appears where the Java class can be created.

Now imagine you intended to create a class with several constructor parameters and want to assign these parameters to fields. In earlier days you had to use Ctrl + 1 for each and every parameter and choose “Assign parameter to new field” in the Quick Fix proposals.

With Eclipse Neon you can now specify all parameters and choose “Assign all parameters to new fields” in the Quick Fix proposals.


        <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

It a little enhancement, but this is usually done quite often so it helps a lot for faster class creation.
Speed up Eclipse using the Jobs API

Long running operations should always be done in an Eclipse Job rather than doing heavy processing in the Eclipse IDE´s main thread.

Therefore the Eclipse Platform provides a Job API, which visualizes the progress of these Jobs or Threads in the Progress View. But only in case the one who implemented the particular Job provides proper feedback by using the IProgressMonitor instance, which is passed to every Job.

Unfortunately some Job implementations do not really care about the given IProgressMonitor instance and just do their progress. But besides giving visual feedback the given IProgressMonitor  also offer´s a .isCanceled() method, where the Job implementation can actively ask whether the Job has been canceled by the user.


Recently there also was another nice blog entry about running Jobs in the Eclipse IDE and how annoying it can be when a Job is canceled by the user, but it still continues to run. [http://kaloyanraev.blogspot.de/2016/03/why-does-canceling-operations-in.html]

So how could Eclipse Neon help with this issue? It provides a new .split() method in the SubMonitor implementation, which automatically checks whether a Job has been canceled and then throws an OperationCanceledException in that case.

So this makes it a lot easier for developers to also consider cancellation in their Job implementations. And the .split() method does this checks only when appropriate.

Job job = Job.create("CancelAbleJob", monitor -> {
  SubMonitor subMonitor = SubMonitor.convert(monitor, todos.size());

  for (Todo todo : todos) {
    processTodo(subMonitor.split(1), todo);
  }

  // no return value needed when using an ICoreRunnable (since Neon)
});
job.schedule();

A great article about using “Using Progress Monitors” by Stefan Xenos can be found here: https://eclipse.org/articles/Article-Progress-Monitors/article.html

In addition to the advice for developers to use this new API the Eclipse Platform already has an implementation for Monitoring these “bad” Jobs.

        <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/kotlin.png"
        alt="kotlin preview" /></p>

[https://mikael-barbero.tumblr.com/post/144085408850/non-cancelable-background-tasks]

Besides the progress monitoring API a new ICoreRunnable interface has been created, which can be passed to the new create and createSystem methods of the Job class. And since this ICoreRunnable is a functional interface a lambda expression can be used to create a Job, rather than deriving from the abstract Job class.

As an aside there´s much more new API, which plays well with Java 8.

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/september/images/"
        alt="Jonas Helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jonas Helming <br />
        <a target="_blank" href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/JonasHelming">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
