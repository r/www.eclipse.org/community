<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    	<p>The <a href="http://projects.eclipse.org/projects/ecd.cft">Eclipse Tools for Cloud Foundry</a>,
    	also known as "Cloud Foundry Tools", or CFT, is a member of the cloud family at Eclipse, and is now
		part of Neon, including the Neon JEE EPP package. Cloud Foundry itself is an
    	open platform as a service that provides a choice of clouds, runtime frameworks, and application services.</p>

		<p>CFT can be used out of the box to connect to Cloud Foundry and deploy applications with a simple drag and drop
		operation. It can also be extended by third parties to contribute their own Cloud Foundry flavors, and supports
		both DEA and newer platforms based on Diego.</p>

		<p>We will introduce the project and show how to use this open source tool to deploy and manage Java web
		applications on Cloud Foundry without leaving the Eclipse IDE.</p>

	<h2>Prerequisite</h2>

		<p>CFT is included in Neon JEE EPP package and can be used without any further installations. This article
		will assume a Neon JEE EPP package as a development platform.</p>

		<p>Users also need a Cloud Foundry account to connect to, for example,
		<a target="_blank" href="https://console.ng.bluemix.net/registration/">IBM BlueMix</a> or
		<a target="_blank" href="https://run.pivotal.io/">Pivotal Web Services</a>,
		as well as a Java web project in their Eclipse workspace.</p>

		<p>CFT currently supports deploying Dynamic Web Projects and other war-packaged applications out of the
		box. Other application types like Node.js and Spring Boot are also supported by third party Eclipse
		tools that extend CFT, like BlueMix Tools and Spring Tool Suite, respectively. However, these
		additional application types require further tool installation, and therefore will not be covered in this article.</p>

	<h2>Creating a Cloud Foundry Server</h2>

		<p>To deploy an application to Cloud Foundry, a Cloud Foundry server must first be created.
		This can be done easily by opening the Eclipse Servers view, and either right-clicking to open
		the context menu, and selecting <b>New &#8594; Server</b>, or clicking on the link in the view if no other server exists.</p>

		<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/servers.png"
        alt="server" /></p>

        <p>This will open the New Server wizard. The Cloud Foundry server type will appear under “Cloud Foundry”, and after
        selecting it, users have the option to edit the name of the Cloud Foundry server instance that will be created.</p>

		 <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/newserver.png"
        alt="new server" /></p>

        <p>Details for the Cloud Foundry server also need to be entered by clicking “Next”. This opens the next page
        where a server URL needs to be added through “Manage Cloud”, as well as the user’s Cloud Foundry account
        credentials. Users must have an account with a Cloud Foundry provider prior to continuing. </p>

        <p>For example, if connecting to Pivotal Web Services, the server URL would be:
		<code>http://api.run.pivotal.io</code></p>

		<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/newaccount.png"
        alt="new account" /></p>

        <p>Once account details are entered, users can click “Next” to select a Cloud Foundry organization
        and space to connect to. When users click “Finish”, the server instance will appear in the Servers view.</p>

	<h2>Deploying a Java Web Project</h2>

		<p>Application deployment to Cloud Foundry can be done simply by dragging and dropping a
		Java web project to the Cloud Foundry server instance in the Servers view.</p>

        <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/explorer.png"
        alt="explorer" /></p>

        <p>An application deployment wizard will then be opened, allowing users to edit default
        deployment details like the application name, URL, memory, and service bindings.</p>

		<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/appdetails.png"
        alt="app details" /></p>

        <p>A <b>new feature</b> in Neon is application URL host-taken detection, which automatically suggests
        a new name if it detects that a host using the initial application name is already taken,
        as by default application names are used as hosts. In the screenshot above, the default
        application name was automatically modified by the wizard to “samplewebapp1”, as the
         original “samplewebapp” was already used by another application URL.</p>

		<p>In addition, this new feature allows URL validation in the next page, for
		cases where users want to specify a different URL than the one generated by the wizard.</p>

		<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/deploy.png"
        alt="deploy" /></p>

        <p>On the following page, users also have the option to create and bind services to an application.</p>

        <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/service.png"
        alt="services selection" /></p>

        <p>Once users click “Finish”, the application deployment will begin, and the Eclipse console will
        automatically open showing deployment progress and log streaming from Cloud Foundry.</p>


	<h2>Managing Applications and Services</h2>

		<p>A deployed application will appear in the Servers view under the Cloud Foundry server instance.</p>

		 <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/serverview.png"
        alt="server view" /></p>

        <p>To manage the application, including scaling memory and number of application instances, binding
        and unbinding services, and restarting and stopping the application, users can double-click on
        the application in the Servers view. This opens the Cloud Foundry editor for that server instance.</p>

        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/july/images/appsandservices.png"
        alt="applications and services" /></p>

        <p>Applications can also be restarted, stopped, and deleted using the Servers view context menu.</p>


	<h2>Open Source Project</h2>

		<p>CFT is an open source project with a growing community, and is actively maintained and
		updated by various Eclipse contributors, including Pivotal and IBM.</p>

		<p>It is part of the Eclipse release train, and it is aligned with the
			Eclipse Simultaneous Release schedule.</p>

		<p>The CFT code repository is hosted on github, and both the main
		<a target="_blank" href="https://github.com/eclipse/cft">CFT GitHub page</a>,
		as well as the
		<a href="https://projects.eclipse.org/projects/ecd.cft">Eclipse project page for CFT</a>,
		contain additional information on
		release schedules as well as how to contribute code and participate in the CFT development.</p>

		<p>CFT releases frequent milestones with bug fixes and new features, and
			users can find the latest version from both the CFT <a href="https://projects.eclipse.org/projects/ecd.cft/downloads">downloads</a> page
			as well as the <a href="http://marketplace.eclipse.org/content/cloud-foundry-integration-eclipse ">Eclipse Marketplace</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/nieraj.jpg"
        alt="Nieraj Singh" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Nieraj Singh<br />
        <a target="_blank" href="http://pivotal.io/">Pivotal</a>
      </p>
      <ul class="author-link list-inline">
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/july/images/elson.JPG"
        alt="Elson Yuen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Elson Yuen<br />
        <a target="_blank" href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/elson_yuen">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

