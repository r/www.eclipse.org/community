<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	    <p>Eclipse Neon will
	    <a href="https://www.eclipse.org/projects/project-plan.php?planurl=http://www.eclipse.org/eclipse/development/plans/eclipse_project_plan_4_6.xml#target_environments">require a Java 8 runtime</a>,
	    which will help foster more adoption of the latest Java runtime.
	    The timing is appropriate to look forward at JDK 9 and ensure that code is ready to run on the next major version
	    of the Java platform. One major consideration for developers is to begin migrating away from JDK internal APIs that
	    are <a target="_blank" href="http://www.oracle.com/technetwork/java/faq-sun-packages-142232.html">always subject to change or removal</a>.
	    JDK 8 and JDK 9 already include a tool called <a target="_blank" href="https://dzone.com/articles/jdeps-jdk-8-command-line">jdeps</a> that can scan bytecode
	    (including libraries) to identify where applications rely on these JDK internal APIs. The OpenJDK wiki includes a
	    <a target="_blank" href="https://wiki.openjdk.java.net/display/JDK8/Java+Dependency+Analysis+Tool">list of known replacements for many internal APIs</a> that developers can use instead.</p>

	    <p>Internal APIs are most often inside packages such as sun.*, com.sun.*, or other similar designations.
	    These APIs represent implementation details and are not considered part of the <a href="http://docs.oracle.com/javase/8/docs/api/">public API</a> and may change, move, or
	    disappear without notice. The jdeps tool will walk through and perform a static analysis to identify calls into any
	    of these internal APIs. This is effective for most cases but does not cover the less-frequent runtime-generated
	    invocations like Class.forName, Reflection, or MethodHandles.</p>

		<p>Developers can run jdeps through a command line. For example to look through all JAR files in a directory,
		the following command will identify internal API usage.</p>

	    <code>jdeps -cp . -jdkinternals *.jar</code><br><br>
<p>Sample output snippet:</p>
<code>hadoop-common-2.2.0.jar -> ...\lib\rt.jar<br>
   org.apache.hadoop.io.FastByteComparisons$... (hadoop-common-2.2.0.jar)<br>
   &nbsp;&nbsp;&nbsp; -> com.sun.jndi.ldap.LdapCtxFactory                   JDK internal API (rt.jar)</code><br><br>

	 <p>As we guide developers away from using internal APIs, the results will be more successful by recommending what to use instead. The jdeps command output also offers suggestions based on its analysis:</p>

		 <table class="table table-condensed">
			<tr>
			<td><code>JDK Internal API</code></td>
			<td><code>Suggested Replacement</code></td>
			</tr>
			<tr>
			<td><code>sun.misc.Cleaner</code></td>
			<td><code>Use java.lang.ref.PhantomReference @since 1.2</code></td>
			</tr>
		</table>


 	<p>The jdeps command line will suit many cases. <a target="_blank" href="https://twitter.com/rfscholte">Robert Scholte</a> has done excellent work
 	on a <a target="_blank" href="https://maven.apache.org/plugins/maven-jdeps-plugin/">jdeps maven plugin</a> to assist developers looking at build integration.</p>

	<p>In some cases, the suggested replacement may not be available until JDK 9. Library developers are not expected to maintain two
	 copies of their code and should instead use another new feature: <a target="_blank" href="http://openjdk.java.net/jeps/238">multi-release JAR files</a>. Multi-Release JAR files are intended
	  to help library developers adopt new language features and APIs without breaking backwards compatibility. For example,
	   java.util.Base64 was introduced in JDK 8 as a way to replace three APIs in sun.misc and internal Apache XML. Access to
	    those internal APIs will be restricted in JDK 9 and developers – As JDK 9 limits access to its internals, how should code be
	    updated in a way that does not break backwards compatibility with versions before java.util.Base64 was introduced (such as JDK 7)?</p>

	 <p>Backwards compatibility can be preserved by using jdeps to see where these internal APIs are used and then making a multi-release JAR.
	 The application would be written to have two class files:</p>
	 	<ul>
	 		<li>com/example/MyClass.class – this would be loaded by older JDK implementations, such as JDK 7 and JDK 8. This class may continue the
	 		bad practice of using the older sun.misc.BASE64Decoder.</li>
	 		<li>META-INF/versions/8/com/example/MyClass.class – this class would use the java.util.Base64 that was introduced in Java 8.
	 		It will be loaded by JDK versions that understand multi-release JAR files.</li>
	 	</ul>


	<p>Oracle has also been active in the engaging open source projects through the <a target="_blank" href="https://wiki.openjdk.java.net/display/quality/Quality+Outreach">OpenJDK Quality Outreach Campaign</a>.
	This group has coordinated with a number of projects to help guide them on running jdeps and integrate <a target="_blank" href="https://jdk9.java.net/">early access of JDK 9</a>
	into their test process. Open Source project maintainers should consider joining the list to learn ways to test upcoming changes.
	The jdeps tool is available for use now in JDK 8 and even better, <a target="_blank" href="https://jdk9.java.net/">early access of JDK 9</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/february/images/erik.jpg"
        alt="Erik Costlow" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Erik Costlow<br />
        <a target="_blank" href="http://www.oracle.com/">Oracle</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://blogs.oracle.com/java-platform-group/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/costlow">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/costlow">LinkedIn</a></li>

        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
