<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>I work out of a co-working space where there are a number of software developers. We’re a multicultural community both in the
    languages and the editors that we use. I’m always interested to hear why people use the editors they do, and their reactions
     to using Eclipse as an IDE. There are some typical complaints: "too memory hungry" or "too much chrome". Some assume Eclipse
      is for Java only and are surprised to hear of PDT, CDT, or PyDev.</p>

	<p>But what really surprised me is how quickly many dismissed Eclipse as a contender for reasons that I knew were easily fixed.
	 They couldn’t figure out how to get started (what’s a project?). They didn’t like some of the default settings (no line numbers?)
	 and couldn’t figure out how to change them.  The UI was overwhelming (look at that toolbar! so many icons!).  Find a tutorial?
	 Pah, they’re trying to get work done — they move along.</p>

	<p>Although some of my colleagues express a willingness to try Eclipse again, I know it’s unlikely to happen: switching has a cost,
	and they’re comfortable with their current tools and workflows. There’s no reason to change until they have a <i>need</i> to change.
	I’m fairly certain my coworking space is a reflection of the world at large.</p>

	<p>We have a perception problem: Eclipse can be hard to get started with. We have an overwhelming number of knobs and dials
	to customize the experience, but they’re hard to discover.</p>

	<p>The Eclipse Foundation is working to focus our IDEs around our users. This work started with the Europa release in 2007
	with the creation custom IDE packages. These packages are tailored to particular domains, such as Eclipse IDE for C/C++
	Developers and Eclipse for Parallel Application Developers. The maintainers of these packages curate the components that
	are included in their package.</p>

	<p>Another area of focus is to surface more relevant information to the user sooner. One of the obvious places to start is
	with what our users usually see first on launch: the Welcome screen. The Welcome historically presented information
	contributed from the installed components, such as CDT, JDT, EGit, PDE. But few users have the patience to read through
	several different manuals. We’re re-imagining the contents of the Welcome’s start page, the “root”, to also allow users to
	directly trigger actions, using the Commands framework, that make sense for those users. To do so, we’re turning over control
	of this page to our package maintainers. For example, the first action for many users is to create a new project. But what kind?
	The Eclipse IDE for JEE Developers’ New Wizard lists more than 20 project types! The package maintainers know the kinds of projects
	best suited to their prospective users. The package maintainers can also change the descriptions of the actions to be better suited to
	their audience, as sometimes it’s just the wording that matters.</p>

	<p>With this change, we can provide some meaningful starting points for newcomers. We can also direct them to the new Setup
	Questionnaire to set up their IDE with the most contended settings — like showing line numbers. Those settings are automatically
	applied to any new workspaces thanks to the new Preferences Syncing feature from the Oomph team. This feature works behind the
	scenes to store and retrieve user preferences from the new Foundation-hosted <a href="https://wiki.eclipse.org/Eclipse_USS">User Storage Service (USS)</a>.
	The USS will be used by other services too: the Marketplace Client plans to store details of the user’s favorite installed plugins to allow for easy
	re-installation for other installs.</p>

	<p>Coupled with features like the <a href="/community/eclipse_newsletter/2015/june/article2.php">Eclipse Installer</a> for accelerating installations, we can get new users hooked and wanting to discover more!
	I’ll be talking more about this effort at <a target="_blank" href="https://www.eclipsecon.org/na2016/session/rethinking-out-box-experience-eclipse-ide">EclipseCon 2016</a>. Hope to see you there!</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/ide.png" alt="ide"/></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/february/images/brian.png"
        alt="Brian de Alwis" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Brian de Alwis<br />
        <a target="_blank" href="http://manumitting.com/">Manumitting Technologies Inc</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/delaweeza">Twitter</a>
        </li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
