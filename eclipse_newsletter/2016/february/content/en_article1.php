<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


    	 <p>With Mars.2, we release EMF Forms 1.8.0. <a href="https://www.eclipse.org/ecp/emfforms/">EMF Forms</a> makes it really simple to create forms that edit your data, based on an EMF model.
    	 To get started with EMF Forms please refer to <a target="_blank"href="http://eclipsesource.com/blogs/tutorials/getting-started-with-EMF-Forms/">our getting started tutorial</a>. In this post,
    	 we want to outline one of the new features: a new version of the Ecore editor based on EMF Forms. We will also have a <a target="_blank" href="https://www.eclipsecon.org/na2016/session/ecore-editor-reloaded">
    	 talk on EclipseCon North America 2016</a> about it. Along with the new version of the standard Ecore editor, we also provide a generic editor to be used with your custom model,
    	 an alternative for the "generated editor".</p>
	<h2>Ecore Editor</h2>
		<p>Most of the developers who use EMF have used the good-old default Ecore editor:</p>
		<p><a href="/community/eclipse_newsletter/2016/february/images/image06.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/image06.png" alt="ecore editor"/></a></p>
		<p>While there are other tools such as the graphical Ecore Tools or Xcore to create models, the tree-based editor is still extremely beneficial. It is simple to use and
		allows you to specify models fairly efficiently. However, the implementation is over 10 years old and hasn’t received much “love” since then. EMF Forms was created
		to build form-based UIs based on existing EMF data models. Those forms are often used in editors, which allow the modification of models. Luckily, there is an EMF
		model for Ecore itself, that means, Ecore itself is just another EMF model. Therefore, we were able to use the full power of EMF Forms to build a new version of
		the standard Ecore editor:</p>
		<p><a href="/community/eclipse_newsletter/2016/february/images/image01-1.png"><img class="img-responsive"
		src="/community/eclipse_newsletter/2016/february/images/image01-1.png" alt="ecore standard editor"/></a></p>
		<p>As you can see, we got rid of the properties view and combined the tree and the properties in one editor. We ordered the properties
		by importance (instead of the former alphabetical order), grouped them, and combined some fields, such as lower and upper bounds. Where
		it made sense, we added custom controls, e.g. to support auto-completion. Finally, we added shortcuts and dialogs for the creation of new
		elements. This enables you to specify a model without leaving the keyboard and therefore, improves efficiency.</p>
		<p><a href="/community/eclipse_newsletter/2016/february/images/image02.png"><img class="img-responsive"
		src="/community/eclipse_newsletter/2016/february/images/image02.png" alt="create child"/></a></p>
		<p>Of course an Ecore editor does not make much sense without a Genmodel editor, therefore we reworked this as well:</p>
		<p><a href="/community/eclipse_newsletter/2016/february/images/image03-1.png"><img class="img-responsive"
		src="/community/eclipse_newsletter/2016/february/images/image03-1.png" alt="genmodel editor"/></a></p>
		<p>Both editors are unfortunately not yet part of an Eclipse package, but they are part of the EMF Client Platform 1.8.0 release.
		<a target="_blank" href="http://eclipsesource.com/blogs/tutorials/emf-forms-editors/">Please see this tutorial</a> on how to install and how to use them.</p>
		<p>As the implementation of both editors shares most of the code, we even went step further and also implemented a completely generic editor, which is capable of
		opening any kind of custom model. This editor is described in the following section.</p>
	<h2>Generic Editor</h2>
		<p>Another tool that is frequently used by EMF developers is the generated editor. It allows you to create instances of a given EMF model and thereby
		provides an easy way of testing a defined model. As the Ecore editor, the generated editor has room for improvement. It was initially only meant as a
		simple code example. It lacks extensibility and adaptability and is therefore, not a very good starting point for the implementation of a custom editor.
		To get around this, we also provide a generic editor based on EMF Forms, which works with any custom model. It supports features such as loading/saving,
		DnD, and undo out of the box. Further it uses EMF Forms for rendering, so you can adapt the controls and layouts used in the detail views as you wish. To
		make that clear: It provides you with a fully functional Eclipse editor for creating and modifying instances of your custom model, without any coding, code
		generation, or adaptation!</p>
		<p><a target="_blank" href="/community/eclipse_newsletter/2016/february/images/image00-2.png"><img class="img-responsive"
		src="/community/eclipse_newsletter/2016/february/images/image00-2.png" alt="model editor"/></a></p>
		<p>All three editors, the Ecore editor, the Genmodel editor and the generic editor are currently under active development and will be
		contributing to Neon. If you are interested in trying them out, <a target="_blank" href="http://eclipsesource.com/blogs/tutorials/emf-forms-editors/">please follow this tutorial</a>.
		Please provide feedback by <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=ecp">submitting bugs or feature requests</a> or <a href="mailto:munich@eclipsesource.com">contact us</a>
		if you are interested in enhancements or support.</p>
		<p>If you want to see these editors live in action, join us on <a href="http://eclipsecon.org">EclipseCon North America</a> and join our
		<a target="_blank" href="https://www.eclipsecon.org/na2016/session/ecore-editor-reloaded">talk about them</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/jonasandmax.jpg"
        alt="Jonas and Maximilian" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Maximilian Koegel<br />
       Jonas Helming<br />
        <a target="_blank" href="http://eclipsesource.com/en/about/company/eclipsesource-munich/">EclipseSource Munich</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://eclipsesource.com/blogs/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/EclipseSourceM">Twitter</a></li>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

