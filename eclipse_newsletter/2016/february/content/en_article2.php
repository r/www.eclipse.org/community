<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

			<p>Many things have happened since Eclipse Mars! The upcoming Eclipse Neon Milestone 6 is fast approaching,
			which is why we're currently pretty busy cleaning up old code, providing a fresh codebase based on Java 8.
			We've also added more and more usability improvements to the IDE. It's always nice to see a feature rich new
			and noteworthy section each and every milestone. Thanks for all your great contributions and thanks to everyone,
			who made the process of contributing more convenient.</p>
			<p>Here are some of the things to look forward to:</p>

	<h2>Improved IDE Usability</h2>

			<p>Some features around the Editor have changed, e.g., increasing or decreasing font size in the editor can now
			be done  by using “CTRL” + “+” and “CTRL” + “-”. No more seeking for the font settings in the preferences. Besides
			the increased font you can also see automatically wrapped text in the screenshot below, which can be toggled with
			the highlighted tool item <img src="/community/eclipse_newsletter/2016/february/images/tool_icon.png" alt="tool icon"/></p>

			  <br>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/java_library.png" alt="java library"/></a>
			  <br><br>

			<p>Having this word wrap functionality was the most voted open bug since 2003 so far and is available since milestone 4 of Eclipse Neon.</p>

			<P>When using code completion subwords will also be sufficient to find a match for a proper completion proposal. In addition to
			 that the typed subword, like “selection” from the screenshot, is also highlighted in bold.</P>

			  <br>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/button_selection.png" alt="button selection"/></a>
			  <br><br>

			  <p>We've also added various shortcuts to make you more efficient. Here are three examples:</p>

		    <img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/eclipse_launcher.png" alt="eclipse launcher"/>
			  <br><br>
			  <img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/eclipse_workingsets.png" alt="working sets"/>
			  <br><br>
			  <img class="img-responsive" src="/community/eclipse_newsletter/2016/february/images/properties.png" alt="properties"/>
			  <br><br>

			<p>Now, recent workspaces are shown as links, buttons to open file locations have been added and a faster way to create new WorkingSets has been added.</p>

			<p>We are currently working on enhanced filter functionalities, so desired objects can be found even faster in the IDE. For example the
			org.eclipse.e4.ui.dialogs.filteredtree.FilteredTree will give you such filter functionalities without having many extra efforts.</p>


		<h2>New API and Bugfixes</h2>
			<ul>
				<li>The generification of the platform code evolved so that the databinding project now supports generic types, as well as the thread
				safe ListenerList&lt;MyListener&gt;.<br>
				Java code snippet:<br>
				<code>ObservableList&lt;String&gt; observableList = new WritableList&lt;&gt;();</code></li>

				<li>Aside from the generification, the Eclipse databinding framework also comes up with a new ISideEffect implementation.
				(No attaching of listeners, bulk updates and more...)</li>

				<li>Sample code is now available for the ISideEffect class in the org.eclipse.jface.examples.databinding project.</li>

				<li>Commonly used Platform objects can be created easier now, e.g., IConverter.create(...), UpdateValueStrategy.create(...), Job.create(...)
				and Job.createSystem(...).</li>

				<li>In several cases outdated API was deprecated and the 2.0 compatibility layer was removed, so that clients know which is the
				latest and greatest API. Some of the old API now have alternatives with vararg parameters where it is appropriate.</li>

				<li>The majority of platform projects have been upgraded to Java 8, so that we can offer more modern API and can make use of Java 8
				 functionality like Lambdas and Streams. New API, which might return null, now also returns java.util.Optional&lt;T&gt;
				 rather than returning null.</li>
				 <li>The support for GTK3 und Wayland is way better than it used to be now.</li>
			</ul>
		<h2>Get Involved</h2>
			<p>Of course you're more than welcome to join the Hackathon sessions, where you can try it, get involved and be able to tackle
			 interesting Eclipse issues yourself. Further information about contributing can be found
			 <a target="_blank" href="http://www.vogella.com/books/eclipsecontribution.html">here</a> as free download. Denis Roy, Eclipse Foundation,
			  recently introduced a new <a href="https://dev.eclipse.org/mhonarc/lists/incubation/msg00002.html">incubation mailing list</a>,
			  where new contributors and committers can ask questions. Looking forward to see you at the EclipseCon's Hackathon!</p>

			<p>I will be at EclipseCon 2016 giving a talk about the "Eclipse Platform News - Rise and Shine". Attend this talk to hear about the
			latest platform enhancements that will make your life easier and more convenient. The first part of this talk gives you a glimpse
			  how we work, what are our aims and highlights new features, performance improvements, simplifications and usability shortcuts
			   to work more efficient. In the second part new API and framework features for developers using the platform will be explained.
			   So how can you benefit from new enhancements in the platform in your own plugins for the IDE or in your Eclipse RCP applications.
			   Demo screens and  sample code will be shown and illustrated as well.</p>

			<p>Upcoming features and plans are in motion, but please feel free to suggest new features or improvements
			 for the platform in general. I mean this is why open source rocks, because you can make a difference. ;-)</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/february/images/simon.jpg"
        alt="Simon Scholz" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Simon Scholz <br />
        <a target="_blank" href="http://www.vogella.com/">Vogella GmbH</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.simonscholz.de/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/simonscholz">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

