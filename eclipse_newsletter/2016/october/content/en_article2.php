<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

		<img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/october/images/logo-golo.png" alt="golo logo"/><p>Yet another language? That is a good question to ask, especially when the Java Virtual Machine (JVM) is host to many languages with proven adoption.</p>

		<p>Eclipse <a target="_blank" href="http://golo-lang.org/">Golo</a> started out of the experiments from the <a target="_blank" href="http://dynamid.citi-lab.fr">Dynamid research team</a> at <a target="_blank" href="http://www.citi-lab.fr">CITI-Inria laboratory</a> and <a target="_blank" href="http://www.insa-lyon.fr">INSA-Lyon</a>. As we were investigating the possibilities offered by the new <code>invokedynamic</code> JVM bytecode instruction in Java 7, we ended up designing a simple programming language around that instruction and the corresponding <code>java.lang.invoke</code> API. We thought it would be interesting to pursue the development of Golo for the following reasons:</p>

			<ol>
			<li>some other JVM languages port their semantics on top of <code>invokedynamic</code>, while we went the other way by designing the language around it with performance patterns in mind, and</li>
			<li>other proven JVM languages tend to have intimidating code bases, especially for students, and we needed something more approachable to experiment with language or runtime research.</li>
			</ol>

		<p>While Golo is indeed <i>"yet another JVM language"</i>, it has interesting applications for tinkerers as a standalone language, as an embedded language into other JVM applications, or even for quick prototyping in IoT settings.</p>

		<p>Golo is now a <i>Technology Project</i> at the Eclipse Foundation, and a small friendly community is taking care of its evolution.</p>

	<h2>Getting started with Golo</h2>

		<p>Golo files usually use the <code>.golo</code> extension. Each Golo file defines a module, as in:</p>

<pre>
module EchoArgs

function main = |args| {
  println("Here are the command line arguments, if any:")
  foreach arg in args {
    println("-> " + arg)
  }
}
</pre>

<p>The <code>main</code> function of a module can serve as a program entry-point with command-line arguments being passed as an array. Golo programs can be compiled to JVM bytecode, or they can be executed directly. In each case the sole <code>golo</code> command-line tool provides a specific sub-command:</p>

<pre>
$ golo compile echo-args.golo
$ golo golo --files echo-args.golo --args 1 2 3 4 5
Here are the command line arguments, if any:
-> 1
-> 2
-> 3
-> 4
-> 5
$
</pre>

		<p>The <code>compile</code> sub-command compiles to JVM bytecode, <code>run</code> executes some already-compiled Golo code, and <code>golo</code> serves as a shortcut to compile and directly run some code. There also exists a support for Unix-style <i>"shebang"</i> scripts:</p>

<pre>
module EchoArgs

function main = |args| {
  println("Here are the command line arguments, if any:")
  foreach arg in args {
    println("-> " + arg)
  }

</pre>

		<p>By making the <code>.golo</code> file executable, we can directly run it:</p>

<pre>
$ chmod +x echo-args.golo
$ ./echo-args.golo 1 2 3 4 5
Here are the command line arguments, if any:
-> 1
-> 2
-> 3
-> 4
-> 5
$
</pre>

	<h2>Collection Literals, Anonymous Functions and Collection Comprehensions</h2>

<p>Golo provides support for collection literals: tuples, lists, vectors, sets, and maps. It is possible to create a list as follows:</p>

<pre>
# Note: an instance of java.util.ArrayList
#
# This is equivalent to:
#   let numbers = java.util.ArrayList()
#   numbers: add(1)
#   (...)
let numbers = list[1, 2, 3, 4, 5]

</pre>

<p>All collection literals are backed by <code>java.util</code> collections bar <code>n</code>-ary tuples that rely on a Golo-specific implementation.
This provides great interoperability with Java libraries.</p>

<p>It is possible to invoke methods on objects in Golo using <code>:</code> between a receiver object and a method.
Collections have additional methods to support functional idioms, so the following code takes the <code>numbers</code> list above, generates a new list with items incremented by 10 using <code>map</code>, and then computes the sum of all elements using <code>reduce</code>:</p>

<pre>
let sum = numbers:
  map(|n| -> n + 10):
  reduce(0, |acc, next| -> acc + next)
</pre>

<p>We can also see how <i>anonymous functions</i> are being passed to the <code>map</code> and <code>reduce</code> methods. It is possible to assign a reference to a function, compose it with another function, and call it by name:</p>

<pre>
let f = (|n| -> n + 1): andThen(|n| -> n * 2)
println(f(3))   # prints 8
</pre>

<p>Finally Golo provides <i>collection comprehensions</i> that are very similar to those from Python:</p>


<p>Here <code>nums</code> is a set of tuples <code>[i, j]</code> where <code>i</code> is an even number between 1 and 100, and <code>j</code> is a character between <code>a</code> and <code>z</code>. This also introduced <i>range</i> notations, as in <code>[1..100]</code>.</p>

	<h2>Defining Types</h2>

		<p>Golo does not support the definition of classes, but it supports structures, unions and dynamic object types.</p>

	<h3>Dynamic Objects</h3>

<p>The simplest type definition is <code>DynamicObject</code>:</p>

<pre>
let p1 = DynamicObject():
  define("name", "Someone"):
  define("email", "someone@some-provider.tld"):
  define("prettyPrint", |this| -> println(this: name() + " <" + this: email() + ">"))

p1: prettyPrint()
</pre>

<p>The <code>define</code> reserved method allows creating attributes or methods, which can then be used by name. In many ways this construction is similar to the <i>"expando"</i> objects in Groovy. There also exists a <i>fallback</i> method definition that captures all invocations to any method that hasn't been defined.
This can be useful for designing flexible APIs, much like some idioms from the Ruby / Rails communities.</p>

	<h3>Structures</h3>

<p>Structures can be created as follows:</p>

<pre>
struct Point = {x, y}
</pre>

<p>This defines a data structure for points, and it can be created and manipulated as follows:</p>

<pre>
var p2 = Point(1, 2)
println(p2)   # "struct Point{x=1, y=2}"

p2: x(4)
println(p2: x())

let x, y = p2
println(x)
println(y)
</pre>

<p>Note that Golo supports <i>destructuring</i>, so that <code>x</code> and <code>y</code> get the values from the structure object. This also works for collections, including map entries.</p>

<p>This <code>Point</code> definition does not do much alone, but we can add methods using <i>augmentations</i>. Augmentations can add methods to any type, including existing Java classes. An augmentation applies to code from the same module, or to code from another module that imports its definition. Here is how we can add <code>shift</code> and <code>dump</code> methods to <code>Point</code> objects:</p>

<pre>
augment Point {

  function shift = |this, dx, dy| ->
    Point(this: x() + dx, this: y() + dy)

  function dump = |this| ->
    println("(" + this: x() + ", " + this: y() + ")")
}
</pre>

<p>With this augmentation visible we can then use the methods:</p>

<pre>
p2 = p2: shift(1, 4)
p2: dump()
</pre>

	<h3>Enumerations</h3>

<p>Enumerations provide an elegant way to define custom types as <i>tagged unions</i>, sometimes also referred to as <i>sum algebraic data types</i>. Here is how one could define binary trees using an empty type, a leaf type and a node type:</p>

<pre>
union Tree = {
  Empty
  Leaf = { value }
  Node = { left, right }
}
</pre>

<p>With this definition, we can create both a tree and an empty tree:</p>

<pre>
let t0 = Tree.Empty()
let t1 = Tree.Node(
  Tree.Leaf(1), Tree.Node(
    Tree.Leaf(2), Tree.Leaf(3) ))
    </pre>

<p>Union types get query methods to check for their types, so that:</p>

<pre>
println(t0: isEmpty())
println(t1: isNode())
</pre>

<p>prints <code>true</code> for each test, since <code>t0</code> is an empty tree and <code>t1</code> is a leaf. It is also possible to check not just for the type but also for values:</p>

<pre>
let _ = Unknown.get()
println(t1: isNode(Tree.Leaf(1), _))
</pre>

<p><code>Unknown.get()</code> returns a placeholder value for union types, which allows us to check that <code>t1</code> is a node whose <code>left</code> value is a leaf of value 1, while the <code>right</code> value is being ignored.</p>

	<h2>Decorators</h2>

<p>Golo also supports Python-style function <i>decorators</i>. A decorator is basically a higher-order function, that is, a function that returns a function. Let's get more practical, and let's define the <code>trace</code> function below:</p>

<pre>
function trace = |message| -> |fun| -> |args...| {
  println("Calling " + message)
  println(">>> arguments: " + args: toString())
  let res = fun: invoke(args)
  println(">>> return value: " + res)
  return res
}
</pre>

<p>That function takes a message, and returns a function that decorates another function and captures its arguments (<code>args...</code> is for variable length arguments, so it can capture any function arity). We can then decorate a function, as in:</p>

<pre>
@trace("Sum function")
function add = |a, b| -> a + b
</pre>

<p>When we call <code>add(1, 2)</code>, we get the following console output:</p>

<pre>
Calling Sum function
>>> arguments: [1, 2]
>>> return value: 3
</pre>

	<h2>That Was Only a Brief Overview!</h2>

<p>This article did not highlight all the features of Eclipse Golo, and we encourage you to give it a try and discuss with the community.</p>

<p>We would like to make a special call to other Eclipse projects: ironically Golo does not have a proper Eclipse IDE support yet.
The best support remains the Atom or Vim editors. You will be more than welcome if you feel like helping us in bringing first-class support for Eclipse Golo in the Eclipse IDE!</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/october/images/julienp.jpg"
        alt="Julien Ponge" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Julien Ponge<br />
        <a target="_blank" href="http://www.citi-lab.fr/">CITI Laboratory</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://julien.ponge.org/blog/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jponge">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

