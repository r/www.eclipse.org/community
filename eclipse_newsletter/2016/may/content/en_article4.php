<?php
/*******************************************************************************
 <b> Copyright (c) 2015 Eclipse Foundation and others.
 <b> All rights reserved. This program and the accompanying materials
 <b> are made available under the terms of the Eclipse Public License v1.0
 <b> which accompanies this distribution, and is available at
 <b> http://eclipse.org/legal/epl-v10.html
 <b>
 <b> Contributors:
 <b>    Eric Poirier (Eclipse Foundation) - Initial implementation
 <b>******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

		<p>Nowadays Front-end developers have to handle a lot of routine in every single project. Here just a few examples:</p>

<div class="row">
  <div class="col-md-17">
  <ul>
				<li>CSS compressing</li>
				<li>Compiling Sass into CSS</li>
				<li>JavaScript minification</li>
				<li>Concatenating css / js files into a singe one for production</li>
			</ul>

  <p>In order to automate this repetitive work, JavaScript Build Systems / Task Runners were created. At the moment the most popular are
  <a target="_blank" href="http://gulpjs.com/">Gulp</a> and <a target="_blank" href="http://gruntjs.com/">Grunt</a>.</p>

  </div>
  <div class="col-md-7"><p align="center"><img src="/community/eclipse_newsletter/2016/may/images/grunt.png" alt="Grunt logo" />&nbsp;&nbsp;&nbsp;&nbsp;  <img src="/community/eclipse_newsletter/2016/may/images/gulp.png" alt="Gulp logo" /></p>
	</div>
</div>

<h2>Pre-Requirements</h2>
  <p>Eclipse Grunt / Gulp tools use the <b>system</b> installation, hence the following software must be pre-installed:</p>
			<ul>
				<li><a target="_blank" href="https://nodejs.org/en/">Node.js</a></li>
				<li><a target="_blank" href="https://www.npmjs.com/">npm</a></li>
				<li>gulp-cli (<code>npm install -g gulp-cli</code>)</li>
				<li>grunt-cli (<code>npm install -g grunt-cli</code>)</li>
				</ul>

		<blockquote><b>TIP</b>: Installation instructions for <b>Node.js</b> and <b>npm</b> can be found <a href="https://docs.npmjs.com/getting-started/installing-node">here</a>.</blockquote>

		<blockquote><b>NOTE</b>: <code>gulp-cli</code>/ <code>grunt-cli</code> do <b>not</b> install Grunt / Gulp on the system, but rather in charge of running the specific version of Gulp / Grunt
		installed in the project (defined in <code>package.json</code>). This allows having multiple versions support on the same machine simultaneously.</blockquote>

	<h2>Quick Start Guide</h2>

		<p>First thing one should do is to define all required dependencies in <code>package.json</code>. Both Gulp and Grunt have massive plugin library that can satisfy most, if not all, of developers' needs:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/dependencies.png" alt="grunt / gulp dependencies" /></p> </br>

		<p>After that <code>npm Install</code> should be executed. This command installs all the dependencies to the <code>node_modules</code> folder:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/install.png" alt="Dependencies installation" /></p></br>

		<p>Now is a hight time to start working with <code>Gruntfile.js</code> / <code>gulpfile.js</code> and define tasks. All tasks will be available in the <b>Project Explorer</b> view under
		<code>Gruntfile.js</code> / <code>gulpfile.js</code>:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/task.png" alt="gulp task" /></p></br>

		<p>For running a task just right-click on it <b>&#8594; Run As &#8594; Gulp / Grunt Task</b></p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/run_task.png" alt="Running gulp task" /></p></br>

		<p>The execution output will be available in the <b>Console View</b>:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/execution.png" alt="Console output" /></p></br>

	<h2>Demo</h2>

		<p>Here is a short demo video covering Eclipse Gulp / Grunt tools functionality:</p>

		<p align="center"><iframe src="https://player.vimeo.com/video/165203009" width="640" height="291" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>

		<blockquote><b>TIP</b>: The source code is available on <a target="_blank" href="https://github.com/ibuziuk/jsdt-grunt-gulp-newsletter-demo">GitHub</a></blockquote>

	<h2>How to give it a go?</h2>

	<p>Gulp / Grunt tools are available via <code>Eclipse IDE for Java EE Developers</code> / <code>Eclipse IDE for JavaScript and Web Developers</code>
	<a href="https://eclipse.org/downloads/index-developer.php">developer builds</a> before the official Eclipse Neon release.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/ilya.jpeg"
        alt="Ilya Buziuk" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ilya Buziuk
      <br />
        <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ilyabuziuk">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ibuziuk">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/ilya-buziuk-08939185">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

