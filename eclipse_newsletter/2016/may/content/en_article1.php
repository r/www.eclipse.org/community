<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <div class="row">
  <div class="col-md-17"> <p><a target="_blank" href="http://bower.io/">Bower</a> and <a target="_blank" href="https://www.npmjs.com/">npm</a> are considered to be the most popular JavaScript package managers. Bower focuses on pure front-end, whereas npm mainly deals with Node.js modules. The main advantages of using package managers are:</p></div>
  <div class="col-md-7"><p align="center"><img src="/community/eclipse_newsletter/2016/may/images/bower.png" alt="Bower logo" />&nbsp;&nbsp; <img src="/community/eclipse_newsletter/2016/may/images/npm.png" alt="npm logo" /></p>
	</div>
</div>
    	<ul>
			 	<li>Dependencies are defined in a single manifest file: <code>bower.json</code> / <code>package.json</code></li>
				<li>No need to commit dependencies to version control repository </li>
			 	<li>Easy to distribute among team members</li>
			 </ul>

		<blockquote><b>TIP</b>: Even though npm is mainly used for managing Node.js modules, many developers prefer using it together tools such as <a target="_blank" href="http://browserify.org/">Browserify</a>
		for front-end development as a substitution of Bower. Also many use both: <code>Bower</code> for client-side packages / <code>npm</code> for server-side packages and JavaScript tools like Grunt / Gulp etc.</blockquote>

	<h2>Pre-Requirements</h2>

		<p>Eclipse Bower / npm tools use the <b>system</b> installation, hence the following software must be pre-installed:</p>
			<ul>
				 <li><a target="_blank" href="https://nodejs.org/en/">Node.js</a></li>
				 <li><a target="_blank" href="https://www.npmjs.com/">npm</a></li>
				 <li><a target="_blank" href="http://bower.io/">Bower</a></li>
			</ul>

		<blockquote><b>TIP</b>: Installation instructions for Node.js and npm can be found <a target="_blank" href="https://docs.npmjs.com/getting-started/installing-node">here</a>. Bower is a command line utility which
		is installed via npm command <code>npm install -g bower</code></blockquote>

	<h2>Quick Start Guide</h2>
		<p>Getting started with JavaScript package manager is pretty straightforward. In order to start working with Bower one need to select <b>File &#8594; New... &#8594; Other...</b> and choose
		<b>"Bower Init"</b> wizard which helps to create <code>bower.json</code> file depending on a set of preferences:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/wizard.png" alt="Bower Init wizard" /></p> <br/><br/>

		<p>After pressing <b>"Finish"</b> button `bower.json` will be created under specified directory.</p>

		<p>In order to add new dependencies user needs to specify them in <code>bower.json</code> via JSON editor.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/json_editor.png" alt="JSON editor" /></p> <br/><br/>

		<p>JSON Editor provides the following functionality:</p>
			<ul>
				 <li>Syntax Coloring (customizable via preferences)</li>
				 <li>Content assist</li>
				 <li>Outline support</li>
				 <li>Text hovering on JSON Objects' / Array' keys and values</li>
				 <li>Text folding on JSON Objects and Arrays</li>
				 <li>Editor extensions for custom hyperlinks, json schema validation & hover help</li>
			 </ul>

		<p>For installing specified dependencies just right-click on the <code>bower.json</code> &#8594; <b>Run As...</b> &#8594; <b>Bower Install</b></p>

		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/install_launch.png" alt="Bower Init launch" /> <br/><br/>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/console.png" alt="Bower Init execution" /></p> <br/>

		<p>After the command execution specified packages will be available under <code>bower_components</code> folder:</p>

		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/components.png" alt="Bower components" /> <br/><br/>

		<p>Basically, that is it - JavaScrtipt libraries / frameworks are ready for future front-end development.</p>

		<blockquote><b>TIP</b>: npm support is implemented in a very similar way. There is a wizard for creating package.json (<b>File &#8594; New... &#8594; Other...</b> &#8594; <b>npm Init</b>)
		and <b>npm Install</b> / <b>npm Update</b> launch shortcuts for commands execution</blockquote>

	<h2>Demo</h2>

		<p>Here is a short demo https://youtu.be/PU8YoWNAkK0[video] covering Bower, npm and JSON editor functionality:</p>

		<p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/PU8YoWNAkK0" frameborder="0" allowfullscreen></iframe></p> <br/><br/>

		<blockquote><b>TIP</b>: The source code is available on <a target="_blank" href="https://github.com/ibuziuk/jsdt-eclipse-newsletter-demo">GitHub</a></blockquote>

	<h2>How to give it a go?</h2>

		<p>Bower, npm & JSON editor are available via <code>Eclipse IDE for Java EE Developers</code> / <code>Eclipse IDE for JavaScript and Web Developers</code>
		<a href="https://eclipse.org/downloads/index-developer.php">developer builds</a> before the official Eclipse Neon release.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/ilya.jpeg"
        alt="Ilya Buziuk" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ilya Buziuk
      <br />
        <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ilyabuziuk">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ibuziuk">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/ilya-buziuk-08939185">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
