<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

		<p>An important part of rebooting the JavaScript Development Tools (JSDT) project was updating the JavaScript
		language level supported with JSDT. In this release, we are replacing the JSDT’s parser/compiler tool chain
		with <a target="_blank" href="http://esprima.org/">esprima</a> parser that supports the ECMAScript 2015 (ES6)
		which is the latest JavaScript specification release
		at this time. Previous toolchain only supported up to ES3 level JavaScript. Since the new language JavaScript
		features was not recognized JSDT often reported false errors on valid Javascript code. As developers adopted to
		later JavaScript language features, false errors became even more common and was likely to get worse with the
		wider adoption of ES6.</p>

		<p>A parser/compiler is central to a language development tool, the new parser had effected
		many JSDT features. I would like to highlight a few in this article that are essential to daily development.</p>

		<p>ES6 has introduced new keywords such as the <code>for-of</code>, <code>const</code> or <code>let</code>. All the new keywords are
		now fully recognized. We have added templates for new keywords where appropriate, in addition
		to keyword content assist. ES6 also had added new constructs such as class definition syntax,
		or template literals to JavaScript. These new constructs are also recognized and acted upon.
		As you can see on Figure 1, class definitions are now used by features such as outline view.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/ES6Editor.gif" alt="ES6 features on Editor" /></p>
		<p><i>Figure 1. ES6 features on Editor</i></p>

		<p>A second goal with the renewal of the language tools is to allow extensibility of JSDT with the vibrant JavaScript tooling ecosystem outside
		Eclipse community. For this reason we have refactored JavaScript parser into validators which are easier extend and replace.</p>

		<p>The transition of JSDT is not over yet! Some of the JSDT features depended on compiler internal data to work
		correctly and are disabled. The work continues on enabling or replacing them with more powerful ones with hope
		to provide an interesting "new and noteworthy" for every release.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/gorkem.jpg"
        alt="Gorkem Ercan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Gorkem Ercan
      <br />
        <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/gorkemercan">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/gorkem">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/gorkemercan">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

