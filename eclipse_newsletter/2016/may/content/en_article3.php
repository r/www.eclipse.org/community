<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<div class="row">
  <div class="col-md-17">
  <p>Node.js is the world’s fastest growing open source platform with over 3.5 million users and an annual growth rate of 100 percent.
  It is used for web applications, IoT, mobile, enterprise application development and microservice architectures. With the creation of the
  <a target="_blank" href="https://nodejs.org/en/foundation/">Node.js Foundation</a> and adoption amongst several companies the future of the platform is bright and clear.
  This is exactly why adding Node.js support to JSDT was the next obvious step.</p></div>
  <div class="col-md-7"><p align="center"><img src="/community/eclipse_newsletter/2016/may/images/nodejs.png" alt="nodejs logo" /></p>
	</div>
</div>

	<h2>Pre-requirements</h2>

		<p>The following software must be pre-installed:</p>
			<ul>
				<li><a target="_blank" href="https://nodejs.org/en/">Node.js</a></li>
				<li><a target="_blank" href="https://www.npmjs.com/">npm</a></li>
			</ul>

		<blockquote><b>TIP</b>: Installation instructions for <b>Node.js</b> and <b>npm</b> can be found <a href="https://docs.npmjs.com/getting-started/installing-node">here</a>.</blockquote>

	<h2>Node.js runtime definition</h2>
		<p>By default, the Node.js launch configuration will try to use the system-wide Node.js installation which is automatically identified on workbench startup.</p>

		<p>However, it is also posible to define customized Node.js binary paths, switch the Node.js used by default to run or debug applications and fully control the
		Node.js runtimes available in the workspace. To do that, navigate to <b>Eclipse Preferences &#8594; JavaScript &#8594; Runtimes</b>, where the following dialog will appear:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1.png">
		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1_small.png" alt="Node.js Runtime Preferences Page" /></a></p></br>

		<p>The identified global Node.js (if there is one) will be listed there, however you can use the buttons on the right side to work with runtimes as follow:</p>

			<ul>
				<li><b>Add</b>: Provide the definition of a new Node.js installation.</li>
				<li><b>Edit</b>: Modify an existing Node.js runtime definition. For non-user defined runtimes this will show the runtime settings in read-only mode.</li>
				<li><b>Duplicate</b>: Create a copy of an existing definition of a Node.js runtime. This is specially useful if you want to run the same installation with different runtime arguments.</li>
				<li><b>Remove</b>: Delete an existing runtime definition. Only available for user-defined Node.js runtimes.</li>
			</ul>

		<p>When there are multiple Node.js installations defined, it is possible to switch the default one used to run the workbench Node.js applications. This can be achieved
		by clicking on the checkbox at the left of the desired installation:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1.png">
		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_2_small.png" alt="Node.js Runtime Preferences Page 2"/></a></p></br>

		<blockquote><b>NOTE</b>: This change will affect <b>all</b> Node.js launch configurations since this is a workbench-wide setting.</blockquote>

	<h2>Quick Start Guide</h2>

		<p>Running and debugging Node.js applications is pretty straightforward. In order to run/debug a Node.js app you need to select
		 <b>Run &#8594; Run / Debug Configurations…</b> and double click <b>Node.js Application</b>. This will create a new Node.js Application launch
		 configuration where you need to specify a project and a main file to be run / debugged:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1.png">
		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/Node.js_Application-Run_Configurations_small.png" alt="Node.js Application - Run Configuration" /></a></p></br>

		<p>Optionally in Arguments tab you can specify <code>Node arguments</code>, <code>Application arguments</code> and the working directory from which the Node.js application will be run / debugged:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1.png">
		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/Node.js_Application_(Arguments)-Run_Configurations_small.png" alt="Node.js Application (Arguments tab) - Run Configuration" /></a></p></br>

		<p>After pressing <b>Run / Debug</b> you can see application's output in the Console view and / or debug your app in the Debug perspective.</p>

		<blockquote><b>NOTE</b>: Node.js Application Launch shortcut will be availbale for projects containing a <code>package.json</code> file or a <b>.js</b> file that is not in
		<code>bower_component</code> or <code>node_modules</code> folder. To use the shortcut just right click on the project / .js file &#8594; <b>Run/Debug As &#8594; Node.js Application</b>.</blockquote>

		<p align="center"><a href="/community/eclipse_newsletter/2016/may/images/Node.js_Runtime_Preferences_1.png">
		<img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/Node.js_Application_Shortcut_small.png" alt="Node.js Application Shortcut" /></a></p></br>

		<p>In order to debug the application right click on the project / .js file &#8594; <b>Debug As &#8594; Node.js Application</b>.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/debug.png" alt="Debugging Node.js app" /></p></br>

		<p>While debugging, all JavaScript variables will be available in the <b>Variables</b> view:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/variables.png" alt="Variables tab" /></p></br>

		<p>Hovering over variables in the editor is also supported:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/hover.png" alt="Hovering over function"/></p></br>

		<p>If you want to change the code during debug session just save the file with new changes, right click on it <b>&#8594; V8 Debugging &#8594; Push Source Changes to VM</b>:</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/may/images/push_changes.png" alt="Push changes to VM"/></p></br>

		<p>After that new changes will be applied and available in the debug session.</p>

	<h2>Demo</h2>

		<p>Here is a short demo video covering Node.js debugger functionality:</p>

		<p align="center"><iframe src="https://player.vimeo.com/video/165423967" width="640" height="302" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>

	<h2>How to give it a go?</h2>

		<p>Node.js tools are available via <code>Eclipse IDE for Java EE Developers</code> / <code>Eclipse IDE for JavaScript and Web Developers</code>
		<a href="https://eclipse.org/downloads/index-developer.php">developer builds</a> before the official Eclipse Neon release.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/orlando.jpg"
        alt="Orlando Ezequiel Rincón Ferrera" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Orlando Ezequiel Rincón Ferrera
      <br />
        <a target="_blank" href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/orlandoibm">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/orlandorincon">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/adalberto.jpg"
        alt="Adalberto Lopez Venegas" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Adalberto Lopez Venegas
      <br />
        <a target="_blank" href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/adalberto_lv">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/adalbertolv">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/adalberto-lopez-venegas-873062a6">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/may/images/ilya.jpeg"
        alt="Ilya Buziuk" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ilya Buziuk
      <br />
        <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ilyabuziuk">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ibuziuk">GitHub</a></li>
         <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/ilya-buziuk-08939185">LinkedIn</a>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

