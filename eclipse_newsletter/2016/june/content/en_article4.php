<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

   	 	<p>Eclipse has a long history in providing tools for PHP development. The <a href="https://eclipse.org/pdt/">Eclipse PHP Development Tools (PDT)</a>
     	project was created back in 2006. The Neon release is the 10-years anniversary of the project.</p>

		<p>Let's take a tour of what the Neon release brings for PHP developers.</p>

    <h2>PHP 7 Support</h2>
		<p>The initial support introduced with PDT 3.6 is now complete in PDT 4.0. The code editor supports all the
		<a target="_blank" href="http://devzone.zend.com/6656/php-7-quick-overview/">new cool features in PHP 7</a>, including:</p>

			<ul>
				<li>Uniform Variable Syntax</li>
				<li>Anonymous Classes</li>
				<li>Scalar Type Hints</li>
				<li>Return Type Declarations</li>
				<li>Spaceship Operator</li>
				<li>Null Coalesce Operator</li>
				<li>Group Use Statements</li>
				<li>Generator Delegation</li>
	   		</ul>


	   		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image11.png" class="img-responsive" alt="php support"/></p><br/>

	<h2>New, Faster PHP Indexer</h2>

		<p>Indexers are fundamental for IDEs. They rip the source code to basic lexical units: variables, function name, parameters,
		etc. This data is persisted in an organized structure – the index. The IDE queries the index whenever you invoke the code
		assist or request a search. The performance of indexing operations is crucial for the user experience with the IDE.</p>

		<p>The PHP indexer is re-implemented using Apache Lucene – a high-performance text search engine. This allows significant
		performance improvement. Building the index is now 3x faster, while consuming 3x less space on the file system. Code assist,
		code navigation, validation and search are also faster. They scale better on multi-core CPUs.</p>

		<p>The new indexer allows data querying while the index is still building. This make the code assist and code navigation more
		responsive. There are no more UI freezes when invoking code assist during index building.</p>

	<h2>More Performance Improvements</h2>
		<p>Besides the new indexer, the latest PDT comes with some more performance improvements:</p>
			<ul>
				<li>The code assist works faster in huge files containing lots of assignment operators.</li>
				<li>The PHP editor works faster in configuration files containing huge arrays (thousands of lines).</li>
				<li>The smart indentation and the generation of PHPDoc are faster too.</li>
			</ul>

	<h2>PHP Editor</h2>
		<p>The PHP editor improves code reading by enabling more semantic highlighters.
		This makes easier recognizing classes, constants, access modifiers and deprecated usages.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image12.png" class="img-responsive" alt="php editor"/></p><br/>

		<p>The code assist dialog received much needed care:</p>
			<ul>
				<li>If you resize the dialog, it saves the new size for the next time you invoke code assist.</li>
				<li>Methods and functions display also the return type.</li>
				<li>Colors for secondary data improve the readability.</li>
			</ul>

		<p>New quick fixes and quick assists help coding faster and cleaner:</p>
			<ul>
				<li>Quick assist for assigning a statement to a new local variable.</li>
				<li>Quick assist for adding a @var comment to a variable for improving code assist.</li>


			<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image13.png" class="img-responsive" alt="var comment"/></p><br/>

				<li>Quick fix for organizing use statements.</li>
				<li>Quick fix for removing the unused use statements.</li>

			<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image14.png" class="img-responsive" alt="use statement"/></p><br/>

			</ul>

		<p>Code assist inside PHPDoc blocks works better after <i>@param</i>, <i>@return</i> and <i>@throws</i> tags.</p>

			<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image15.png" class="img-responsive" alt="return"/></p><br/>


	<h2>Debugging</h2>
		<p>The debugging support received a lot of attention during the last year. The quality improved for both Xdebug and Zend Debugger integrations.</p>
		<p>One new feature is the support for PHP breakpoint exceptions. It works only for Xdebug, because Zend Debugger does not support this kind of exceptions.</p>

			<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image16.png" class="img-responsive" alt="exception breakpoint"/></p><br/>

	<h2>Smart Import</h2>
		<p>Importing existing PHP code is easier than ever. The latest PDT takes advantage of the new Smart Import wizard.
		Just call <code>File &#8594; Open Projects from File System…</code> from the main menu. Then browse the file location of your PHP code.</p>


		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image17.png" class="img-responsive" alt="project import"/></p><br/>

	<h2>And More...</h2>

		<p>We've just seen the major new features and improvements. There are many more little improvements included in the Neon release.
		The PDT team closed more than 300 Bugzilla items in the last 12 months.</p>

		<p>Visit the New & Noteworthy pages for <a href="https://wiki.eclipse.org/PDT/NewIn36">PDT 3.6</a>,
		<a href="https://wiki.eclipse.org/PDT/NewIn37">PDT 3.7</a> and
		<a href="https://wiki.eclipse.org/PDT/NewIn40">PDT 4.0</a> for the complete list of changes. Yes, the PDT project had three
		feature releases during the Neon development.</p>

	<h2>How to get it</h2>
		<p>There are many ways for getting the latest Eclipse PDT tools:</p>
			<ul>
				<li>Download <a href="https://www.eclipse.org/downloads/">Eclipse Neon</a> and select the "Eclipse for PHP Developers" package.</li>
				<li>Download the "Eclipse for PHP Developers" package direcly from the <a href="https://www.eclipse.org/downloads/eclipse-packages/">Eclipse package page</a>.</li>
				<li>Extend your existing Eclipse installation by installing the "PHP Development Tools (PDT)" feature from:</li>
					<ul>
						<li>The Eclipse Marketplace</li>
						<li>The Eclipse Neon Simultaneous Release software repository</li>
					</ul>
			</ul>
	<h2>Contact Us</h2>
	<p>We want to hear from you! You can reach us in our user
	<a href="https://www.eclipse.org/forums/index.php/f/85/">forum</a> and in our developers
	<a href="https://dev.eclipse.org/mailman/listinfo/pdt-dev">mailing list</a>. Tell us what you like and what not in Eclipse PDT.</p>

	<p>Please report bugs in the project's <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=pdt">Bugzilla</a>.</p>

	<p>Happy PHPing!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/june/images/kaloyan.jpg"
        alt="Kaloyan Raev" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Kaloyan Raev<br />
        <a target="_blank" href="http://www.zend.com/de">Zend, a Rogue Wave company</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/kaloyanraev">LinkedIn</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kaloyanraev">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
