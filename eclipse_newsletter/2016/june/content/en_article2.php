<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	    <p>I would like to introduce a new service available for the user community: Eclipse User Storage, aka the USS.
	    It is an API for projects to store data on Eclipse Foundation servers, linked to a user account.</p>

		<p>A few plugins are currently using it and are available in Eclipse Neon: the Marketplace Client and Oomph Preferences Synchronizer.</p>

	<h2>The Marketplace Client</h2>



		<p>Its intent is to help users share their favorite plugins between many installations or workstations:
		the Marketplace Client.</p>

		<p>Available since Neon M7, the Marketplace Client or “MPC” is very useful to install and share your favorite Eclipse plugins.
		By sharing, I mean that if you are using the same Eclipse account on many workstations, you are able to retrieve your
		favorites through the MPC, and install them. Life made easy, in a sense, no?</p>

		<p>So, where to find this wonderful tool? Just open the “Help” menu, and look for “Eclipse Marketplace”.</p>

		<p>It opens a new window, dedicated to the MPC. Your workbench is still available while you’re browsing and installing packages.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/mpc-1.png" class="img-responsive" alt="php support"/>&nbsp;&nbsp;
		<img src="/community/eclipse_newsletter/2016/june/images/mpc-2.png" class="img-responsive" alt="php support"/></p><br/>

		<p>Now, let’s see how you can use the sharing feature. Go to the “Favorites” tab. The first time you use the MPC, this tab will be empty.
		Just click on “Log in to view your favorites” and fill the form with your Eclipse account information.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/mpc-3.png" class="img-responsive" alt="php support"/></p><br/>

		<p>If you have the “Secure Storage” service activated in Eclipse, you might have to configure that too.</p>

		<p>To add a package as a favorite, just click on the star button below a project’s logo. You can do that on any tab of the MPC.
		Of course, you can search some packages on the “Search” tab. However, I would recommend that you go in the “Installed”
		tab, and add your favorites from there.</p>

		<p>Of course, you can also go to the <a href="https://marketplace.eclipse.org/">Eclipse Marketplace website</a> and manage your favorites there.
		And if you already have favorites there, they will be automatically imported in the MPC.</p>

		<p>Go back to the “Favorites” tab:</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/mpc-4.png" class="img-responsive" alt="php support"/></p><br/>

		<p>You can also click on “Import Favorites List…” and copy paste the link of the favorites of a user. You can try this feature right now with my very own favourite link:
		<a href="https://marketplace.eclipse.org/user/athomaseh2/favorites">https://marketplace.eclipse.org/user/athomaseh2/favorites</a></p>


		<p>If you are using Eclipse on many devices, just repeat the same thing. You can install your favorite packages across
		multiple Eclipse installations using the MPC. Just click on the “Install” button of a package. Of course, you might have
		to restart Eclipse once you’re done.</p>

		<p>The MPC is available in Eclipse Neon. And if you find a bug, please report it <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=MPC">here</a>.
		 Feedback will allow us to make it even better!</p>


	<h2>Oomph Preference Synchronizer</h2>
		<p>The other Eclipse plugin using the USS is allowing to synchronize your preferences between many Eclipse installations:
		on one workstation, or between several workstations.</p>

		<p>Just open the Eclipse Preferences window. You will find at the bottom left a red circle, just click on it, it becomes red.
		The Preference Synchronizer is now activated.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/oomph.gif" class="img-responsive" alt="php support"/></p><br/>

		<p>Now, when you will change a setting in the preferences, it will be recorded. The first time a preference is recorded,
		a new window will open: you will be able to select whether you want to store the setting locally or online. This
		is available because sometime, you might not want to share something.</p>


		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/oomph-2.png" class="img-responsive" alt="php support"/></p><br/>


		<p>It will then remember your choice. I tested it between my Mac and my Ubuntu laptops. And this is working well. If you want
		to force the synchronisation, one way is to go in the menu, “Help / Perform Setup Tasks” and it’s done.</p>

	<h3>How It Works</h3>
		<p>One last point to explain: where are your favorites and preferences being stored? As explained at the beginning of this article,
		the Marketplace Client and Oomph Preference Synchronizer are using the USS (User Storage Service). So your favorites are saved with
		your Eclipse user account.</p>

		<p>At the moment, only MPC and Oomph are using the USS. But of course, this it can be used by any Eclipse project. The USS SDK
		is actually part of the Oomph project, and will be soon taken outside as an independant Eclipse project.</p>

		<p>So, if you want to play with test the api, here are some interesting resources:</p>
		<ul>
			<li><a href="https://wiki.eclipse.org/Eclipse_USS">https://wiki.eclipse.org/Eclipse_USS</a></li>
			<li><a href="http://git.eclipse.org/c/oomph/uss.git/">http://git.eclipse.org/c/oomph/uss.git/</a></li>
		</ul>

    	<p>To conclude, I would like to thank a lot Eike Stepper, Carsten Reckord, Brian de Alwis, and Christopher Guindon for the great work they have done and the help they gave me.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/june/images/antoine.jpg"
        alt="Antoine Thomas" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Antoine Thomas<br />
        <a target="_blank" href="https://eclipse.org/">Eclipse Foundation, Inc.</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.ttoine.net/en/author/ttoine/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ttoine/">Twitter</a></li>
		<?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

