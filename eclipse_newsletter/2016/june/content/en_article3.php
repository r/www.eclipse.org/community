<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	    <p>Over the last few years at the North America EclipseCon, we have held an <a href="http://www.eclipse.org/cdt/">Eclipse CDT</a> (C/C++ Development Tooling) Summit. It’s one of the few times we
	    have a chance to get together in a room and be a real team and we carry on that spirit as we spread around the globe during the rest of the year. This year,
	    there was a special moment at the Summit in Reston during the hackathon we held in the evening.</p>

		<p>Jonah Graham from Kichwa Coders, our newest committer, had pushed up a patch that implemented our biggest feature for CDT 9.0. And we finally cornered Marc Khouzam
		from Ericsson, our debugger master, to review it. I captured the moment in a picture I posted to Twitter. The coolest thing, was that Jonah wasn’t physically there.
		His wife, the famous Tracy Miranda, was there and she Skyped him into the room and Marc and Jonah went to work.</p><br/>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/twitter.png" class="img-responsive" alt="twitter moment"/></p><br/>

		<p>It just really struck me at that moment what a great team we have working on the CDT. Our contributors from different companies from different continents working together
		for the greater good of the community. A huge thanks to Jonah, for being up after midnight in England to dial into the hackathon, and for Marc for dragging us line by line
		 to make sure the patch was great and doing so despite me cat calling (joking of course) from the sidelines to “just push it”. I don’t think I’ve been as proud of the team
		 as I was that day and we have had many good ones.</p>

		<p>So what was this great feature they were working on? Well, it wasn’t really a feature. It was the final payment on a huge technical debt we had accumulated long ago in the CDT.
		It was the removal of 84,000 lines of code that was the old C Debug Interface (CDI) debugger framework. It was big enough that it made Mike’s closing slides of EclipseCon stats.</p><br/>


		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/linesdeleted.png" class="img-responsive" alt="slide"/></p><br/>


	    <p>The CDT community had long ago transitioned to our newer Debug Services Framework (DSF) and contributions on the CDI had dwindled to nothing for a long time.
	    We’re not even sure it worked anymore and it led to a lot of confusion over what framework adopters should be using. We have to give CDI it’s due respect. It did
	    get us started and we wouldn’t be where we are today without it’s solid debug support. But as the community grew and adopters were trying to make it work with
	    really interesting but complex distributed systems, they found they needed the greater flexibility that DSF provided. So it was time to move on.</p>

	<h2>CDT 9.0 New and Noteworthy</h2>

		<p>In the end, CDT 9.0 for Eclipse Neon is mainly about opening the door with a major API change to clean up CDI and other technical debt that we’ve grown in our API that
		has grown over the years. A complete list of the changes can be found at the end of our
		<a href="https://wiki.eclipse.org/CDT/User/NewIn90">CDT 9.0 New and Noteworthy</a>. You’ll also find a nice collection of other features we’ve added for this release.</p>

		<p>I have a few favorites:</p>

	<h3>Mapping</h3>

		<p>First is our move to leverage gdb’s built-in path mapping feature so we don’t have to duplicate it in the CDT. This feature was also done
		by Jonah. If you’re running your program on a different machine than where the build is done, as is often done with embedded systems, the path seen by gdb can be
		different than what CDT sees. It’s very hard to set that up correctly, but by letting gdb take care of it, CDT will always receive paths it understands.</p>


		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/mapping.png" class="img-responsive" alt="mapping"/></p><br/>

	<h3>Codan</h3>
		<p>Codan, our static analysis code checker, is one of the coolest features of CDT that people don’t know about. It finds logic errors in your code using CDT’s
		built-in parsers and presents them in the editor without having to do a build. It saves a lot of time. For CDT 9.0, we’ve started adding <i>Quick Fixes</i> to
		common problems. If you have a variable that is an instance of a class you haven’t defined yet, you can use a <i>Quick Fix</i> to start up the <i>New Class</i> wizard.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/class.png" class="img-responsive" alt="class"/></p><br/>

		<p>Now, we all know that C and C++ are hard languages to parse and sometimes we get things wrong. To help cut down on false errors, we have added the ability to suppress warnings with magic comments.</p>

		<p><code>x = x; // @suppress("assignment in condition") because I know better</code></p>

		<p>Of course, I have been promised that <i>Quick Fixes</i> will be added in the future to make this even easier. And it would be great to see the community add new checkers. There is so much we can do here.</p>

	<h2>What’s next for CDT?</h2>

		<p>The biggest growth area we see for the CDT is to better support common open application frameworks and platforms that C and C++ developers are using today. Many of those frameworks come with
		their own build systems and our existing build system lacks the flexibility to support these systems. In fact, our current system is probably overkill for them. To help address that, we’ve started
		working on a new build system infrastructure that gives us a fresh new start. It co-exists with the old system but gives integrators the flexibility to totally replace it.</p>

		<p>With the help of this new build system, we’ve rebooted our Qt support to give Qt a renewed focus for CDT. It is one of the most popular C++ application frameworks in use today, and
		we really need to make sure CDT users can take advantage of it. To help with that, we now have a QML editor and have started work on giving this editor standard Eclipse features such as
		content assist. Since this is a reboot of the Qt support, we are offering it as a preview and plan to build on it over the next few releases with the help of the community.</p>

		<p>CDT has been in development for over 15 years now and continues to evolve as our communities evolve. There are many places where C and C++ are still kings, especially
		where performance matters. Compiled languages will always provide the best MIPS/Watt for these systems and the CDT along with the rest of the Eclipse ecosystem will continue
		to provide a great environment that helps developers build cool things.</p>

	<h2>Getting It</h2>
		<p>Download Eclipse CDT 9.0 from the <a href="https://eclipse.org/cdt/">Eclipse CDT website</a> or download the <a href="https://eclipse.org/downloads/">Eclipse Neon "Eclipse IDE for C/C++ Developers" package</a> now to get started!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/june/images/doug.jpg"
        alt="Doug Schaefer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Doug Schaefer<br />
        <a target="_blank" href="http://www.qnx.com/content/qnx/en.html">QNX</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://cdtdoug.ca/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/dougschaefer">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

