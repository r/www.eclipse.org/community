<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	    <p>On June 22/2016, the Eclipse community released Eclipse Neon, the eleventh <i>named</i> release of Eclipse software.
	    Combining the efforts of almost 800 software developers organized into more than one hundred individual development
	    teams, the release is a massive undertaking that involved months of collaboration, communication, and hard work.</p>

		<p>To insiders, the Eclipse Neon release is actually the thirteenth <i>simultaneous release</i>. Eclipse development teams are
		organized into open source projects that each focus on different functional areas. Individual teams set their own goals
		and priorities, but work directly with other teams to manage interdependencies between projects to ensure that the end
		result is a single coherent offering. The primary means of coordination between teams is the production of regular
		milestone builds during the release cycle: a total of seven milestones and four release candidates are built from the
		combined efforts of all the teams before the final bits are assembled and shipped.</p>

		<p>As usual, this release brings many new features and enhancements, but Neon is the first release that can benefit from the
		<a target="_blank" href="https://www.infoq.com/news/2015/03/eclipse-mars-reporting">Automated Error Reporting Initiative</a> added to the last year release, a.k.a. Eclipse Mars.
		The <a href="https://dev.eclipse.org/mhonarc/lists/ide-dev/msg01251.html">numbers</a> as compiled by Marcel Bruch (the project lead of the error reporting system) are quite impressive, and demonstrate that the community took great care in fixing users issues.</p>

		<p>For the Eclipse Mars (2015) release, 350,000 users sent 3,000,000 error reports. Don’t rush on Twitter to say that last year Eclipse release contained 3 millions issues :)
		These reports has been grouped (via automated and manual analysis) into 25,000 issues. What is impressive is the amount of issues have been fixed:
		around 30% of them were fixed before the second service release (Mars.2) released in March 2016.
		It'll be interested to see how many issues have been fixed since then for the Neon release!</p>

		<p>Here are some of my top ten favourite things in the Eclipse Neon release.</p>


    <h2>1. Word Wrap</h2>

    	<p>If you follow the trail of duplicate issues reported, support for soft word wrap in Eclipse Platform editors has been on our
    	wishlist since 2002. The issue was closed in December 2015, just in time for inclusion in the fourth milestone build of Eclipse Neon.
    	It’s turned off by default, but you can easily enable it via a toolbar item, or via the <code>Alt+Shift+Y</code> keyboard binding.</p>

    	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/word-wrap.png" class="img-responsive" alt="word wrap"/></p><br/>

    <h2>2. Editor Font Size Zoom</h2>
    	<p>As a regular presenter at conferences with a love of doing live demos, making it possible for everybody in the presentation
    	room to actually see my Eclipse Workbench has been a challenge. In earlier versions, it is possible to modify the fonts via the preferences,
    	but with the Eclipse Neon release, you can now use <i>Zoom In</i> <code>(Ctrl++ or Ctrl+=)</code> and <i>Zoom Out</i> <code>(Ctrl+-)</code> commands to dynamically increase and
    	decrease the font size in text editors. This doesn't apply to views, so presentations to a large group may still require some preferences tweaking.</p>

		<p>Like a change in the <code>General &#8594; Appearance &#8594; Colors</code> and Fonts preference page, the zoom commands persist the change for all editors of the same type.
		If the editor type's font is configured to use a default font, then that default font will be zoomed. To temporarily zoom the editor font in the current
		editor only (changes are neither propagated to other editors nor persisted), use a "pinch" gesture on a touchpad (OSX and Windows only).</p>

    <h2>3. UI/UX Improvements</h2>

    	<p>A number of other helpful user interface improvements that will both make day-to-day development easier, but are also helpful when you need to do
    	a live demo. You can, for example, toggle the Eclipse Workbench into and out of fullscreen mode via the <code>Alt+F11</code> key binding. To give yourself even more
    	available screen space, you can turn off the main toolbar, all window toolbars and the status bar.</p>

		<p>There’s a handy menu entry to turn off the main tool bar under <code>Window &#8594; Appearance;</code> to toggle the state of the window toolbars, you’ll need to use
		the Ctrl+3 <i>Quick Access feature</i>. Type <code>Ctrl+3 and “toolbar”</code> to access commands, views, etc. related to toolbars.</p>

    	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image07.png" class="img-responsive" alt="ui improvements"/></p><br/>

    	<p>The Neon release features HiDPI support on Windows and Linux, so the toolbar icons will be usable on your monstrously large monitor.</p>

    	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/swt-high-dpi.png" class="img-responsive" alt="hidpi"/></p><br/>

		<p>Finally, if you don’t care about things like the dark theme in particular or CSS theming of your workbench in general, you can turn off
		theming in the preferences. This should result in some performance improvements, most notably on startup.</p>

    	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/disable-theming-pref.png" class="img-responsive" alt="disable theming"/></p><br/>

    <h2>4. Smart Import</h2>

    	<p>The new Smart Import wizard will help you get up and running quickly by configuring one or more Eclipse projects based on the contents of a directory.
    	The wizard automatically detects different programming languages used and installs the necessary <i>natures</i> and <i>builders</i> into the newly-minted Eclipse projects.
    	Open the new wizard via the <code>File &#8594; Open Projects...</code> menu.</p>

		<p>Smart import currently detects and constructs Java, PHP, and JavaScript projects, and automatically configures Git integration if Git metadata is present.
		Note that you must actually have the necessary features installed for the smart import to work. That is, to be successful in importing a Java project, you
		must have the Java development tools installed; to import a PHP project, you must have the PHP development tools installed; and so on.</p>

    	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image06.png" class="img-responsive" alt="smart import"/></p><br/>

    	<p>Click on the <i>installed project configurators</i> to get a list of the configurations supported. The wizard is extensible, so support for other types of content will grow over time.</p>

    <h2>5. File Editors</h2>

    	<p>One of my pet peeves with the Eclipse Workbench is the default behaviour when I click on a file type that’s not directly supported: it opens a system editor
    	outside of the IDE. You can now decide for yourself how your IDE will behave.</p>
		<p>There are currently four options:</p>
			<ol>
				<li><b>Search Marketplace</b> (default) will use the Eclipse Marketplace Client to search the Eclipse Marketplace for plug-ins that support the file type;</li>
				<li><b>System Editor; if none: Text Editor</b> will open the system editor associated with the file, if available. If no system editor is associated with the given file, fall back to the Eclipse Text Editor;</li>
				<li><b>Text Editor</b> will always open Eclipse’s Text Editor on unassociated file types; and</li>
				<li><b>Ask via pop-up</b> will open the same dialog as using <code>Open With &#8594; Other...</code> on a file and let you choose which editor to use (inside or outside the IDE).</li>
			</ol>
		<p>The default behaviour is to search the <a href="https://marketplace.eclipse.org/">Eclipse Marketplace</a> for a solution to install.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image08.png" class="img-responsive" alt="file editors"/></p><br/>

		<p>The Eclipse Marketplace contains a wealth of plug-ins, extensions, and solutions that greatly enhance the Eclipse IDE experience.</p>

	<h2>6. JavaScript and JSON</h2>

		<p>Significant new work has been invested in the JavaScript development tools, starting with a new parser with
		<a href="http://www.eclipse.org/community/eclipse_newsletter/2016/may/article2.php">EcmaScript 2015 (ES6)</a> support. With this,
		the Eclipse IDE recognizes and support ES6 language constructs and provides better validation accuracy compared to the earlier parser.</p>

	<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/ES6Editor.gif" class="img-responsive" alt="es6 editor"/></p><br/>

		<p>Both Bower and npm are supported with creation wizards and launch shortcuts for executing update and install commands.
		<a href="http://www.eclipse.org/community/eclipse_newsletter/2016/may/article4.php">Grunt and gulp</a> build systems
		are accessible from within the <i>Project Explorer</i> view and can be executed via launch shortcuts.</p>

		<p><a href="http://www.eclipse.org/community/eclipse_newsletter/2016/may/article3.php">Node.js runtimes</a> are now directly
		supported (<code>Eclipse Preferences &#8594; JavaScript &#8594; Runtimes</code>) including a new launch configuration type can be used to run and
		debug Node.js applications. Node.js applications can be debugged directly from the editor via the Chromium V8 debugger, with full support for breakpoints
		and inspecting JavaScript variable values in the <i>Variables</i> view (and when you hover over an in-scope variable).</p>


		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image01.png" class="img-responsive" alt="nodejs demo"/></p><br/>

		<p>The new <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/may/article1.php">JSON editor</a> provides support to edit some
		well-known JSON file types including <code>package.json, bower.json, .bowerrc, and .jshintrc.</code>
		This support includes the following features:</p>
			<ul>
				<li>Syntax Coloring which can be customized with preferences;</li>
				<li>An Outline Tree view;</li>
				<li>Text hover on JSON Objects/Array keys and values;</li>
				<li>Text folding on JSON Objects and Arrays; and</li>
				<li>Editor extensions for custom hyperlinks, json schema validation, and hover help</li>
			</ul>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image05.png" class="img-responsive" alt="json editor"/></p><br/>

		Read more about JavaScript and JSON features in the previous issue of the Eclipse Newsletter
		<a href="http://www.eclipse.org/community/eclipse_newsletter/2016/may/">JavaScript Development Tools Rebooted</a>.


	<h2>7. Java Language</h2>

		<p>A lot of work has gone into improving and stabilizing the support for Java 8 language features, including more than 35 separate fixes for lambda expression support
		(including numerous compiler and type inference fixes along with user interface improvements).</p>

		<p>Some new options have been added to the code formatter: you can control the wrapping policy of <code>for</code> loop headers, the <i>Align fields in columns</i> feature
		in the <i>Indentation</i> section can now be configured to recognize groups separated by empty lines and align them independently, and you can control if new
		lines should be added after annotations on enum constants.</p>

		<p><code>Null</code> analysis with generics has been improved, and includes more configuration options. A new <i>Info</i> problem type and decorator for “information”
		severity problems detected by the Eclipse Compiler for Java has been added along with a new <i>Quick Fix</i> to configure how the compiler reports problem severity.</p>

		<p>Java content assist provides substring completion. It even helps you identify which part matched what you typed by highlighting matched characters.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/substring-code-completion.png" class="img-responsive" alt="code completion"/></p><br/>

		<p>Though not strictly part of the Eclipse Neon release, a technology preview of <a href="http://marketplace.eclipse.org/content/java-9-support-beta-neon">Java 9 support</a>
		is available via the Eclipse Marketplace. In order to run this functionality, you must download and install a Java 9 <i>Java Development Kit (JDK)</i> and launch your Eclipse IDE.
		The Eclipse IDE itself requires a <i>Java Runtime Environment (JRE)</i> to run. With the new modular delivery of the base Java libraries,
		command-line switches are required to make required to ensure that required functionality is available to the Eclipse runtime. There
		is more information in the <a href="https://www.eclipse.org/eclipse/news/4.6/platform.php#java-9">New and Noteworthy</a>
		documentation. Once you have your Eclipse IDE running, you can add the Java 9 support by
		dragging the “Install” button from the Eclipse Marketplace onto your running Eclipse instance.</p>

		<p>The technology preview supports the new image and module layouts, editing module info files, and more. Note that this is an implementation
		of an early-draft specification developed under the Java Community Process (JCP) and is made available for testing and evaluation purposes only.
		The code is not compatible with any specification of the JCP.</p>


	<h2>8. Welcome</h2>
		<p>The Eclipse Foundation's website theme has been extended into the Eclipse IDE in the form of a revamped <i>Welcome</i> page.</p>

			<p align="center"><a href="/community/eclipse_newsletter/2016/june/images/image09.png">
			<img src="/community/eclipse_newsletter/2016/june/images/image09small.png" class="img-responsive" alt="welcome"/></a></p><br/>

		<p>The Welcome page has become more useful as well, with valuable functionality that has previously been buried brought to the top.</p>

		<p>The <i>IDE configuration settings</i>, for example, has been a part of the Eclipse IDE since the Mars release, but was difficult to find.
		By placing this front-and-center on the Welcome screen, you can now readily leverage it to configure some of the most common options
		like whether or not to show line numbers in editors, whether or not to run the spell checker, preferred character encoding, and more.</p>

			<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/image00.png" class="img-responsive" alt="hello world tutorial"/></p><br/>

		<p>If you’re new to the Eclipse IDE and want to learn more about building Java applications,
		run the “Hello World” tutorial. Again, the tutorial itself isn’t new, but it sure is easier to find.</p>

	<h2>9. User Storage Service</h2>
		<p>The new <a href="https://wiki.eclipse.org/Eclipse_USS">Eclipse User Storage Service (USS)</a> provides a means for you to store metadata regarding the configuration of your Eclipse workspace(s)
		on an Eclipse Foundation server. The intent is to make it easier to create a common configuration for your environment that you can share across
		workspaces on one or more workstations.</p>
		<p>The storage service is implemented as a generic service that can be leveraged by plug-ins. With the Neon release, the preferences recorder
		that was introduced with last year’s <i>Mars</i> release has been extended to use the storage service to record common workspace preferences. The Eclipse
		Marketplace also leverages the storage service to record your favourites, making it easy to add your favourite plug-ins to all of your Eclipse IDE installations.</p>
		<p>Note that the USS is intended to be used for data that may be exposed publicly. Take care when using the USS to
		avoid saving preferences that may contain personally identifiable information.</p>
		<p>Read more about USS <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/june/article2.php">here</a>.</p>


	<h2>10. GTK3</h2>
		<p>As a long time Fedora user, I couldn’t be more excited about the improvements made to the GTK3 support in the Eclipse Neon release.
		There is certainly more work to do, but the latest versions of Eclipse IDE all run very well on the platform.</p>

		<p>Note that if you’re using Ubuntu 16.04, you may have to <a target="_blank" href="http://stackoverflow.com/questions/36822242/eclipse-doesnt-work-with-ubuntu-16-04/36859485#36859485">fallback to GTK 2</a>,
		as there are still <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=492371">outstanding issues</a> with this distribution. It seems that some specific changes Ubuntu made in GTK3 for the 16.04 release it
		breaking normal consumer of GTK3. If you face the issue, I highly recommend you
		<a target="_blank" href="https://bugs.launchpad.net/ubuntu/+source/java-common/+bug/1552764">ask to Ubuntu to revert to standard GTK3</a>. Join the forum discussion <a href="https://www.eclipse.org/forums/index.php/t/1071268/">here</a>.</p>

	<h2>More...</h2>
		<h3>11. PHP 7 Support</h3>
		<p>Neon brings complete support for PHP 7 in the <a href="https://eclipse.org/pdt/">PHP Development Tools</a>. You can now enjoy typing the return type of your function!
		Read more about it in this Eclipse Newsletter article "<a href="http://www.eclipse.org/community/eclipse_newsletter/2016/june/article4.php">What's New in Eclipse PHP Development Tools</a>".

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/pdt.png" class="img-responsive" alt="pdt"/></p><br/>

	<h3>12. Gerrit Integration</h3>
		<img align="right" src="/community/eclipse_newsletter/2016/june/images/egerrit-logo.png" class="img-responsive" alt="egerrit logo"/><p>EGerrit,
		a new project joining the simultaneous release this year, provides an awesome <a href="https://www.eclipse.org/egerrit/">Gerrit integration into the Eclipse IDE</a>.
		It minimizes the context switch from the IDE to the web and leverages IDE specific features such as syntax coloring, navigation, etc.
		Eclipse Gerrit integration allows to perform most operations on reviews such as voting, submitting, abandoning, etc. Finally, it also gives the user the
		ability to directly comment in the file when reviewing the changes.</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/egerrit.png" class="img-responsive" alt="gerrit"/></p><br/>

	<h3>13. Autosave</h3>
		<p>You can activate the auto saving for all opened editors and say goodbye to the <code>ctrl+s</code> (<code>cmd+s</code> on macOS) habit!
		Note that this feature has been implemented thanks to the Friends of Eclipse Enhancement Program (FEEP). Read the Mikaël Barbero's <a target="_blank" href="https://mikael-barbero.tumblr.com/post/145308146515/its-feep-ing-time">blog
		post</a> about this program to know more about what has been done and what is the current plan for next release. Also,
		don’t forget to continue to <a href="https://www.eclipse.org/donate/">donate to the program</a> to see more features and bug fixes coming (or to get a limited edition Neon t-shirt).</p>

		<p align="center"><img src="/community/eclipse_newsletter/2016/june/images/autosave-preference-page.png" class="img-responsive" alt="autosave"/></p><br/>


	<h3>14. Packaged Distributions Updates</h3>
		<p>At last, features included in <a href="https://www.eclipse.org/downloads/eclipse-packages/">Eclipse Packaged distributions</a> can now be updated (or removed) individually.
		In the past, when you were using an Eclipse Packages (e.g. Eclipse IDE for Java EE Developers, Eclipse IDE for C/C++ Developers,
		Eclipse for PHP Developers…), it was not possible to remove or update any of it's feature independently. You had to wait for a
		new version of the full Package. Now, if you download the Eclipse IDE for Java EE Developers, but don’t want Eclipse Mylyn, you
		can remove it. Or, if you're annoyed with a bug in the Eclipse Git integration, you can now update it to a snapshot
		build that fixes your bug or to a newer release that is not yet integrated in a new version of your Package.</p>

	<h3>15. And Even More...</h3>
		<p>We tend to focus on IDE (and especially Java development tools) improvements when we talk assemble a <i>top ten</i> list. But there are a lot of other
		projects that participate in the release that contribute great functionality that often plays more of a supporting role. The Eclipse community has a
		rich modeling tradition, for example, and with the Eclipse Neon release, we’ve stepped up our game to make modeling even more appealing. In addition
		to a great many fixes that improve the stability and performance of the modeling tools, and visual improvements (e.g. high quality rendering of SVG images)
		in generated editors and views, there is some very cool experimental new tools for visualizing models.</p>

		<p>The Eclipse Neon release brings experimental support for comparing model diagrams visually.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/june/images/image02.png">
		<img src="/community/eclipse_newsletter/2016/june/images/image02small.png" class="img-responsive" alt="hello world tutorial"/></a></p><br/>

		<p>You can easily merge all non-conflicting differences that are contained in a selected model element or resource with one click.
		This works in both directions when comparing local models, or one-way when comparing against a static source (e.g. when comparing against a previous version in Git).</p>

		<p>Also experimental with this release, you can now define rich property views alongside the diagram definition without having to write a line of code.
		This capability will be extended in the next months based on the user community feedback.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/june/images/image04.png">
		<img src="/community/eclipse_newsletter/2016/june/images/image04small.png" class="img-responsive" alt="hello world tutorial"/></a></p><br/>

	<h2>Conclusion</h2>

		<p>The Eclipse Foundation maintains a database that indexes every Git commit in our repositories. According to this index, more than 2,300 distinct
		individuals have contributed at least one commit to at least one of the open source projects that are part of the Eclipse Neon release since the
		beginning of time (the first commit is dated May 2001). This does not include any contributors to our SVN support (the Eclipse Foundation has
		mercifully granted an exception to let the Subversive project team use SVN for their work), nor does, the number include all authors from the
		time that our project teams used CVS (these commits were migrated to Git, but since CVS tracks committer, but not author information, some
		commits are not properly attributed; this attribution is tracked elsewhere). So the actual number of contributors to Eclipse Neon is considerably higher than 2,300.</p>

		<p>The Eclipse Neon release is a marvel of software engineering. But, it is perhaps equally interesting that Eclipse Neon is the result of years of practiced
		communication and coordination between project teams that separately work out their own priorities without coordinated direction from above: it is a marvel
		of coopetition with forces pulling project teams in different directions chasing different priorities, but still coming together to produce a great set of products.</p>

		<p>If you’ve made it this far, you’ve no-doubt already noticed that my top ten list actually contains fifteen items. It’s actually more than that if you consider
		that I’ve mashed together some features that could easily be kept apart. There’s a lot of cool stuff in the the Eclipse Neon release:
		<a href="https://www.eclipse.org/downloads/">download it today</a> and take a look.</p>

	<h2>Credits</h2>

		<p>Some of the content (especially screenshots) and most of the inspiration for this document comes from new and noteworthy documents provided by the project teams, including:</p>
			<ul>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M1/">Eclipse Project Neon (4.6) M1 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M2/">Eclipse Project Neon (4.6) M2 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M3/">Eclipse Project Neon (4.6) M3 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M4/">Eclipse Project Neon (4.6) M4 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M5/">Eclipse Project Neon (4.6) M5 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M6/">Eclipse Project Neon (4.6) M6 New and Noteworthy</a></li>
				<li><a href="https://www.eclipse.org/eclipse/news/4.6/M7/">Eclipse Project Neon (4.6) M7 New and Noteworthy</a></li>
				<li><a href="https://wiki.eclipse.org/CDT/User/NewIn90">Eclipse C/C++ Development Tools 9.0 New and Noteworthy</a></li>
				<li><a href="https://wiki.eclipse.org/PDT/NewIn37">Eclipse PHP Development Tools 3.7 New and Noteworthy</a></li>
				<li><a href="https://wiki.eclipse.org/PDT/NewIn40">Eclipse PHP Development Tools 4.0 New and Noteworthy</a></li>
				<li><a href="https://wiki.eclipse.org/Eclipse_USS">Eclipse User Storage Service</a></li>
				<li><a href="https://eclipse.org/webtools/development/news/3.8M6/">Eclipse Web Tools Platform 3.8M6 New and Noteworthy</a></li>
				<li><a href="https://eclipse.org/webtools/development/news/3.8M7/">Eclipse Web Tools Platform 3.8M7 New and Noteworthy</a></li>
			</ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/june/images/wayne.jpg"
        alt="Wayne Beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Wayne Beaton<br />
        <a target="_blank" href="https://eclipse.org/">Eclipse Foundation, Inc.</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://waynebeaton.wordpress.com/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

