<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    	<p>At Red Hat, we think that software is one of the drivers of today’s world, and we feel that this trend will continue until software is the primary force of the future economy. This will transform everything and everyone, from the largest enterprise, to the hobbyist at home, and we think that success will come to those who have the ability to create, deliver and manage quality software – our job at Red Hat is to empower those software creators. We believe that the best way to empower software creators is to enable them to write better software, faster.</p>

		<p>When we create software today, we often hit a brick wall and waste many hours because creating copies of another system is far too hard. We create copies for many reasons – a few common examples are replicating production issues, asking another developer for help or running test suites. Sometimes we need to copy the runtime (e.g. app server, web server, LAMP stack…), sometimes the development workspace and sometimes both.</p>

		<p>Linux containers provide the necessary encapsulation of your application and its environment, giving us the ability to easily copy the runtime – something we have never had before. The concept of universal workspaces, championed by Eclipse Che, give us the ability to easily replicate and distribute the entire development workspace. Red Hat and Codenvy have integrated Che with <a target="_blank" href="https://www.openshift.com">OpenShift</a>, providing an easy way to copy both your runtime, and your development workspace.</p>

		<p>OpenShift 3 provides developers with a self-service platform for provisioning, building and deploying containerized applications, and Eclipse Che provides universal workspaces which are on-demand, collaborative, and standardised – this includes both a workspace server and an integrated browser based IDE.</p>

	<h2>Walkthrough</h2>

	<p align="center"><iframe width="700" height="415" src="https://www.youtube.com/embed/IjSP2tLMl6k" frameborder="0" allowfullscreen></iframe></p>

    	<p>In this video you can see a developer working with OpenShift and Che through our example scenario – we’ve been asked by the marketing team to make a simple change to our app.</p>

		<p>First the developer got hold of the source for the project, by doing a git import. They then connected the workspace to OpenShift, authenticating using OAuth, and finally, configured the OpenShift environment. In this case we are using Red Hat JBoss Enterprise Application Platform with a Java EE application.</p>


		<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/templates.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/templates_sm.png" alt="templates"/></a></p>

		<p>OpenShift also provides templates for other Java servers and frameworks such as Tomcat, and ActiveMQ, as well other languages and frameworks such as Node, Python, the LAMP stack, Ruby, and more. OpenShift also provides access to many common data stores such as MongoDB, MySQL and Postgres.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/build.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/build_sm.png" alt="build"/></a></p>

		<p>Once the application has built, it’s immediately available. Build logs are available within Che so you don’t even need to go to the OpenShift console to view them.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/application.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/application_sm.png" alt="application"/></a></p>

		<p>Manually triggering a deploy is useful, but of course we really want to start to build out a continuous delivery pipeline. We can easily configure OpenShift so that each time we commit to the git repo, a webhook triggers an OpenShift source to image build.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/configuration.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/configuration_sm.png" alt="configuration"/></a></p>

		<p>OpenShift uses the source to image technology to create a war file using the standard Maven, and then wrap it in a docker image. This is pretty magic, as it allows us to take a run of the mill project, and have it deployed in container on OpenShift.</p>

		<p>Now, of course, we would need to spend some time making the code changes, checking they look right and creating the patchset, that we can then commit and push for code review.</p>

		<p>But, before we do that, we want to run the test suite. Arquillian, the Java testing framework created and maintained by Red Hat, allows us to easily run the tests, from Eclipse Che, on OpenShift. By running the tests in an exact copy of production, we are even more likely to catch bugs early.</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/demo.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/demo_sm.png" alt="demo"/></a></p>

		<p>We all know how frustrating it can be to get your workspace and runtime environment set up every time you need to change context. We think that together Eclipse Che and OpenShift are a great step towards making this problem go away.

We’re looking forward to taking the work we’ve started, and shown you here today, a lot further.

To find more about Openshift 3, visit <a target="_blank" href="https://www.openshift.com/">https://www.openshift.com/</a>. To find out more about Eclipse Che visit <a target="_blank" href="http://www.eclipse.org/che/">http://www.eclipse.org/che/</a>.

We always welcome contributions! Find OpenShift and Eclipse Che on github – <a target="_blank" href="https://github.com/openshift">https://github.com/openshift</a> and <a target="_blank" href="https://github.com/eclipse/che">https://github.com/eclipse/che</a>.

This was also posted on <a target="_blank" href="http://developers.redhat.com/blog/2016/04/05/integrate-openshift-and-eclipse-che-for-faster-development/">developers.redhat.com</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/august/images/pete.png"
        alt="Pete Muir" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Pete Muir<br />
        <a target="_blank" href="https://www.redhat.com">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/pmuir">LinkedIn</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/plmuir">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/pmuir">GitHub</a></li>
        <?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

