<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    	<p>I remember one of my first Eclipse contributions. It was in 2008, on the Standard Widget Toolkit (SWT) project. I submitted a bugzilla issue and uploaded a patch with a one-line change. It took me some time to achieve this very simple task:</p>
			<ul>
				<li>Setup and run Eclipse in debug mode</li>
				<li>Reproduce the bug</li>
				<li>Identify the problem</li>
				<li>Find the right Concurrent Versions System (CVS) repository</li>
				<li>Setup and run Eclipse with the checked-out sources</li>
				<li>Fix the bug</li>
				<li>Make sure the fix works</li>
				<li>Create the patch (thankfully I was using Linux ^^)</li>
				<li>Upload the patch to bugzilla</li>
				<li>And a lot more steps as the reviewer suggested some improvements to the initial patch</li>
    		</ul>

    	<p>Today, Distributed Version Control Systems (DVCS) make repository forking and branching a lot simpler. For instance, with GitHub’s pull request, you can submit a patch in two clicks. This is a huge change for open source projects. Still, for real project contributions we can add a few more steps: contributors need to build the project, use code completion in their favorite IDE, test the modified application before submitting the patch, etc. All these steps have to be done locally and involve a lot of installation and configuration.</p>

		<p>Moreover, the creation of a Github fork and the management of several remote Git repositories and branches will probably discourage the Git beginner.</p>

	<h2>The Contribution Workflow</h2>

		<p>A year ago, the Serli team was asked to work on a contribution workflow within Che and Codenvy. We believed that Eclipse Che could bring an on-demand contribution workspace and make pull request creation as easy as clicking on a URL. Let's have a look at what has been done so far on the Spring Petclinic sample project.</p>

		<p align="center"><iframe width="420" height="315" src="https://www.youtube.com/embed/tvnmcslJLe4" frameborder="0" allowfullscreen></iframe></p>
		<p>1. Go to <a target="_blank" href="https://github.com/codenvy-demos/spring-petclinic/">https://github.com/codenvy-demos/spring-petclinic/</a>.</p>
		<p>2. Click on the Github badge.</p>

				<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image09.png" alt="github badge"/></p>

		<p>3. If this is the first time use oauth with your Github account.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image01.png" alt="github badge"/></p>

		<p>4. You are in Eclipse Che</p>

			<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image05.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image05_sm.png" alt="eclipse che workspace"/></a></p>

		<p>5. Open a Java class and make any modification. You’ll notice that the project is already configured and that the Java code completion is working.</p>
		<p>6. Build and run the pet clinic app with the preconfigured command.</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image11.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image11_sm.png" alt="pet clinic app"/></a></p>

		<p>7. Once you are happy with it, open the pull request panel on the right.</p>
		<p>8.Fill out (or complete) the form: PR name, branch name, description.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image00.png" alt="form fill out"/></p>

		<p>9. Che will suggest committing your changes if you have not already done so (beware of blocked pop-ups).</p>
		<p>10. Access the newly created PR.</p>

		<p>Good isn't it? In 10 simple steps, we managed to complete a real contribution use-case.
So now, I guess you might want to have this kind of badge for your own Github open source project. Let’s see how I did it for … my blog :)</p>

	<h2>A Contribution Badge for my Blog</h2>
	<p>It may sound crazy to build and edit a blog with an IDE, but stick with me on this one. I’ve chosen Github pages and Jekyll as a blog engine for various reasons:</p>
		<ul>
			<li>no database,</li>
			<li>can be hosted anywhere else if Github dies,</li>
			<li>Git keeps history, etc.</li>
		</ul>
		<p>However, it is not always that simple to edit or create a blog post with Github pages. To get a preview of a blog post that your are editing there is no other choice but to checkout the source code and run Jekyll locally. That’s where Eclipse Che helps me: I can edit my blog site in markdown and at the same time have a live preview thanks to my custom Dockerized Jekyll runtime. With a Codenvy Github badge, not only can anyone help me improve my blog post, I have a complete online editor for my own usage and don’t need to install anything locally.</p>

		<p>The first step to create this contributor badge is to set up an Eclipse Che developer workspace. Once the workspace is up and running, we can extract the configuration and make a Codenvy factory URL that will be used to regenerate a new ready-to-use workspace each time someone clicks on it. Lastly, we can slightly modify the factory configuration to display the pull request panel.</p>

	<h3>Mister Jekyll and Dockerfile</h3>
		<p>For a Jekyll blog, we are interested in editing the blog with a nice text editor and previewing the modified site before publishing the modifications. In Eclipse Che, you can set up developer workspaces with Docker. Docker images will contain not only your developer tools but also the runtime: all the dependencies that are needed to run your application. In our case, the runtime will be Jekyll.</p>

		<p>For my blog, I started by creating my Dockerfile. The runtime is based on an existing Codenvy Docker image that has basic developer tools such as Git, but this is not mandatory. You could, for instance, use the official Jekyll Docker image. You just won't be able to use Git from the terminal. To test a Dockerfile, I usually build and run the base image locally with Docker or with Che, run the installation commands in the terminal, and copy the working commands in the Dockerfile.</p>

<code>
FROM codenvy/node
RUN sudo gem install github-pages pygments.rb
EXPOSE 4000
</code>

		<p>In this Dockerfile, I reuse an existing image containing all the Ruby dependencies and basic tools such as Git.</p>

		<p>The Dockerfile is ready, I can build it and push it to any Docker registry like Docker hub.</p>

<code>
docker build -t sunix/jekyll4che .
docker push sunix/jekyll4che
</code>

		<p>This Docker image is publicly available. You can even use it for your own Jekyll-based Che workspace.</p>

	<h3>Eclipse Che Workspace Setup</h3>
		<p>Now that I have my Dockerfile with the Jekyll runtime, I can use it as an Eclipse Che custom workspace in Codenvy</p>

		<p>1. Go to <a target="_blank" href="https://beta.codenvy.com">https://beta.codenvy.com</a></p>
		<p>2. Create a new account if necessary</p>
		<p>3. Login</p>
		<p>4. Create a new project</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image04.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image04_sm.png" alt="create new project"/></a></p>

		<p>5. Import the existing Jekyll project from Github</p>
		<p>6. Create a workspace from Custom Stack: “FROM sunix/jekyll4che”</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image06.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image06_sm.png" alt="create workspace"/></a></p>

		<p>7. Give nice names for the workspace and the project</p>
		<p>8. Create and open the project</p>

		<p>At that point, I have a workspace with the project in Eclipse Che.</p>

		<p>Before exporting the workspace configuration, I will create a command to run the blog with Jekyll. New contributors won’t have to figure out how to run and test the blog site.</p>

		<p>Create a new custom command:</p>

			<ul>
				<li>Name: Run Jekyll</li>
				<li>Command line: cd ${current.project.path} && jekyll serve --host=0.0.0.0</li>
				<li>Preview URL: http://${server.port.4000}/</li>
			</ul>

		<p>You will notice that the command contains macros that will be replaced when someone runs the command. More about macros can be found in <a target="_blank" href="https://eclipse-che.readme.io/docs/commands#macros">Eclipse Che documentation</a>.</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image02.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image02_sm.png" alt="command"/></a></p>

		<p>I can run the command and make live updates to a blog post :)</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image07.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image07_sm.png" alt="live blog post"/></a></p>

	<h3>Generate a Factory URL</h3>
		<p>Now that the workspace is ready, let’s export it as a factory URL.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image08.png" alt="create factory"/></p>

    	<p>A factory URL refers to an entry in the Codenvy database containing all the necessary information to recreate the workspace:</p>
    		<ul>
				<li>Source location</li>
				<li>Project type</li>
				<li>Dockerfile for the Eclipse Che workspace stack</li>
				<li>Commands</li>
			</ul>
		<p>When a user clicks on the link, Codenvy will regenerate a new Eclipse Che workplace based on these metadata.</p>

		<p>The factory URLs are all available in the factories section of the owner’s Codenvy dashboard.</p>

		<p>Now my contribution badge is ready to be integrated into the README.md file. I will copy the factory URL and add it to the README.md file.</p>

<code>
[![Open workspace in Eclipse Che](http://beta.codenvy.com/factory/resources/codenvy-contribute.svg)](https://beta.codenvy.com/f?id=h0e3e8tqk5ytooh2)
</code>

	<h3>Factory URL from Source</h3>
		<p>A factory URL is owned by a Codenvy user and only they can modify it and access its statistics. If you are not interested in these statistics and would like to allow any of your project committers to change the factory metadata, it is possible to store the Json file in the Github project rather than as a factory url entry in the Codenvy database.</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image03.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image03_sm.png" alt="factory url"/></a></p>

		<p>To make this kind of URL, I create a .codenvy.json file in the root folder of the Github project, then add the factory json content to it. I need to remove the ID and owner metadata. At last, I can create the new factory URL providing the complete Github project url: https://beta.codenvy.com/f?url=https://github.com/sunix/blog.sunix.org/tree/gh-pages.</p>

		<p>In this screenshot I’m actually using the previous factory URL to make this contribution :D.</p>
		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2016/august/images/image10.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/image10_sm.png" alt="previous factory url"/></a></p>

		<p>I’m finishing it by adding the badge to the README.md file:</p>

<code>
[![Open workspace in Eclipse Che](http://beta.codenvy.com/factory/resources/codenvy-contribute.svg)](https://beta.codenvy.com/f?url=https://github.com/sunix/blog.sunix.org/tree/gh-pages)
</code>

		<p>More about factory URLs can be found in the <a target="_blank" href="https://codenvy.readme.io/docs/factories ">Codenvy documentation</a>.</p>
		<p>You could do a lot more than I have done here: open a file by default, trigger a command, etc...</p>

	<h2>Eclipse Projects</h2>
		<p>That's all, I hope you will all create these Github contribution badges for your Github projects and that you will get more and more contributions. For Eclipse projects, some of them (like <a target="_blank" href="https://github.com/vert-x3/vertx-examples">Vert.x</a>) already have their Github badge. We can create contributor badges for any of them. However, as most of them rely on Eclipse Plug-in Development Environment (PDE), they won't have code completion in Eclipse Che, but hopefully you will still be able to build and run any project. In addition, we are looking forward to Visual Studio language protocol to help us in improving this in the near future.</p>
		<p>Happy coding :)</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/august/images/sun.jpg"
        alt="Sun Tan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Sun Tan<br />
        <a target="_blank" href="http://www.serli.com/">Serli</a>
      </p>
      <ul class="author-link list-inline">
      	<li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/sunsengdavidtan">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/sunix">GitHub</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

