<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	    <p>Being a developer today is perhaps more exhilarating than at any other time in history. We have incredible choice when it comes to languages and frameworks, and tools like GitHub and StackOverflow have connected millions to make sharing code and expertise simple and fast. These conveniences have allowed us all to spend more of our time being creative and honing our craft, rather than fighting with source code repos and ancient languages. But in this age of global sharing and constant collaboration, one of our most important development tools, the IDE, has remained stubbornly individual and private. Why?</p>

		<p>IDEs are very personal — perhaps to a fault. Before developers can start work on a project, we have to set up our IDE’s workspace, from configuring the project to marrying that workspace to a runtime that we download and configure separately. If the runtime is using a desktop or laptop, we may have to contend with resource limitations and can’t easily share their runtime with others (in fact the steps we took to make it work might not even work for someone else). If the runtime is shared on a Virtual Machine (VM), then it’s likely a large one that’s hard to keep synchronized with the ever-evolving codebase and dependencies. Today, when most developers contribute to many projects (often simultaneously), this setup tax becomes prohibitive — and seems to go against the sharing and collaboration that’s swept the rest of the development tooling world.</p>

		<p>So why can’t anyone, anywhere, contribute to a project without installing or configuring any software? To fully realize this dream, we need to redefine the developer workspace.</p>

		<p><a href="http://www.eclipse.org/che/">Eclipse Che</a> is first to deliver on this vision by integrating a cloud IDE, workspace server and plug-in platform into a universal developer workspace that’s portable and extensible. Che workspaces include the IDE, project sources and runtime in a single, versionable unit. Wherever that workspace lives and runs, it has identical behavior. A universal workspace allows the IDE to be as collaborative and portable as the rest of our tools.</p>

	<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/cheworkspace.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/cheworkspace_sm.png" alt="eclipse che workspace"/></a></p>

	<h2>Defining a universal workspace</h2>
		<p>Che’s universal workspace has five key attributes:</p>

	<h3>Runtime-included</h3>
		<p>In Che, workspaces bring their own runtimes that run in Docker containers and are activated with the workspace. Projects are mounted into those runtimes, and the workspace serves up its own browser IDE with an Eclipse Orion editor, although any desktop IDE can be connected to the workspace with a super-fast Fuse-based mount and sync mechanism based on SSH and unison. Run as many workspaces in parallel as you want.</p>

	<h3>Portable and Shareable</h3>
		<p>Che’s workspaces are naturally portable - all you need to use them is a browser so you can start a project on your desktop, tweak it over lunch on an iPad and then ask a friend to try it on her Chromebook. You can share access to your workspace to collaborate with others, or send someone your configuration and they can have an exact replica (including the runtime) running on any OS in less than a minute.</p>

		<p align="center"><iframe width="560" height="315" src="https://www.youtube.com/embed/OZNceE-sWto" frameborder="0" allowfullscreen></iframe></p>

	<h3>Programmable</h3>
		<p>Che offers RESTful APIs for the workspace server and for each workspace that is created. This lets you programmatically control the workspace lifecycle and resourcing through the server, or interact with an individual workspace. Eclipse Che plugins installed into the workspace can alter that API set, which is available to the Che server and any remote client such as a browser or desktop IDE.</p>

		<p>SAP leveraged Che’s programmability to build the developer tooling for their SAP HANA cloud products. The SAP HANA IDE simplifies development of full-stack business applications encompassing user interface apps, business logic, data models and analytics. SAP preserved the Che server and workspaces, but created a custom IDE that communicates with Che over its RESTful APIs.</p>

	<h3>Versionable</h3>
		<p>If your workspace contains a runtime, what happens if that runtime needs to change during development? Che uses versionable configuration files to describe the entire structure of the workspace and its dependencies. Docker runtimes allow us to define the construction of the image through Dockerfiles and images, so it’s possible to reconstitute workspaces that are identical (including their internal state) in a number of locations.</p>

		<p>Red Hat has <a target="_blank" href="http://che.eclipse.org/redhat-duality-dev-prod/">integrated Che with OpenShift</a> (a self-service platform for provisioning, building and deploying containerized applications) to provide on-demand production replicas of both development and production systems.</p>

	<h3>Extensible</h3>
		<p>A universal workspace is nothing without an ecosystem and community to build upon it. Extensions can be built to alter any aspect of Che. There are three types of extensions:</p>
		<ul>
			<li>Extensions inside the browser, loaded from your workspace</li>
			<li>Extensions in the Che server</li>
			<li>Extensions customized for the workspace, injected as an agent into the runtime based upon the project types selected by users</li>
		</ul>

		<p>You can then package multiple extensions into plugins, compile them with the Che core and create your own assemblies. Package these as your own hosted server or as a desktop application to run on someone else’s computer.</p>

		<p>While Che has its own set of plugins around the Java/Javascript tool stack, it has also embraced Eclipse Orion editor, diff and git as well as the JDT core.</p>

	<h2>A new landscape</h2>
		<p>Che fulfills a powerful vision for anyone contributing to projects, from professionals to new practitioners. No longer will we spend hours installing or configuring runtimes in order to experiment with or contribute to a project — instead, universal workspaces make it easy to share what we love to do with others. </p>

		<p>Find out more about Eclipse Che and the evolution of IDEs <a target="_blank" href="https://www.infoq.com/presentations/eclipse-che-eclipsecon-2016">here</a>.</p>


			<p align="center"><a target="_blank" href="https://www.infoq.com/presentations/eclipse-che-eclipsecon-2016"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/jetsons.jpg" alt="Jetsons video link"/></a></p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/january/images/tyler.jpg"
        alt="Tyler Jewell" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Tyler Jewell<br />
        <a target="_blank" href="https://codenvy.com/">
Founder & CEO, Codenvy</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/TylerJewell">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/tylerjewell">LinkedIn</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

