<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Overview</h2>
		<p>This piece describes how to extend Eclipse Che with custom plugins, e.g. to provide support for new languages. It covers various aspects from adding new file types, extending the code editor, adding IntelliSense features, defining a specific project-type, and accessing the workspace.</p>

		<p>In general, Che can be extended in its three different components: the IDE running in the browser, the Che server, and the workspace (see diagram below).</p>
		<ul>
			<li><b>The IDE running in the browser</b> can be extended by new local features, such as simple actions, new editors, and views, or immediate syntax highlighting. Some of these extensions can run completely local, some will use the Che server API.</li>
			<li><b>The Che server</b> can be extended by new plugins. Those extensions will also affect the IDE, e.g. defining a new project type in a plugin, which is done on the server, will create a menu entry in the “New” dialog of the IDE. Server plugins might provide new APIs to be consumed by IDE plugins, e.g. to provide new IntelliSense features. Finally, server plugins can also access the current workspace, e.g. to access files, projects or even the current target machine. Again, there is existing API provided by the workspace agent to be used.</li>
			<li><b>The workspace</b> can be extended to provide new API to be consumed by the Che server.</li>
		</ul>

			<p align="center"><a href="/community/eclipse_newsletter/2016/august/images/overview.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/overview_sm.png" alt="overview"/></a></p>

		<p>Typical extensions of Che are deployed to up to three different components, depending on the use case. However, as many extensions include plugin parts for the IDE, the server and even the workspace, the extension documentation is not organized by Che components, but by extension use cases (e.g. adding a new file type or implementing a client server communication accessing the workspace).

		<p>Technically, client and server extensions are different components. However, as they conceptually belong together, they are organized in one plugin containing several sub-components.

		<p>Let’s start with an introduction to the following prerequisites to build a custom extensions. If you want to follow the tutorial hands-on, you should read those sections, if you are just interested in learning about a specific extension use case, you might skip them for now.

		<ul>
			<li>How to <a target="_blank" href="https://eclipse-che.readme.io/v4.5/docs/setup-che-workspace">set-up a workspace</a> for developing a Che extension.</li>
			<li>How to <a target="_blank" href="https://eclipse-che.readme.io/v4.5/docs/setup-che-workspace">create and build extensions</a> including a description of the general plugin structure used in Che.</li>
			<li>A brief introduction on <a target="_blank" href="https://eclipse-che.readme.io/v4.5/docs/dependency-injection-basics">dependency injection</a> and its usage in Che, on the client and on the server side. (If you are already familiar with Guice and Gin, you might want to skip this part)</li>
		</ul>

		<p>The remaining tutorial is structured along extension use cases. For every extension capability, we provide a general introduction containing code examples.

		<p>The tutorial often refers to a continuous example: The implementation of simple JSON support in Eclipse Che. If you are interested in learning about a certain extension use case, please directly navigate to the respective part of the tutorial. If you are interested in getting an overview about the most important extension features, please have a look at <a target="_blank" href="https://eclipse-che.readme.io/docs/introduction-1#section-the-json-example">the introduction of the JSON example in the following section</a>. It provides a use case oriented overview of the contents of this tutorial and contains links to the detailed parts.

	<h2>The JSON Example</h2>
		<p>In this section, we give a functional overview of the continuous example that we use in most parts of this tutorial, the “JSON Example.” It provides simple support for creating, modifying and validating JSON files within Eclipse Che. Note that the JSON example is not designed to provide perfect JSON support in Che. In fact, it is designed to cover most aspects of providing support for a custom language in Eclipse Che while remaining as simple as possible. This section will provide an overview about the example and at the same time about Che’s various extension use cases.</p>

		<p>The source code for the JSON example is part of the Eclipse Che project itself, which you can find <a target="_blank" href="https://github.com/eclipse/che/tree/master/samples/sample-plugin-json">here</a>.</p>

		<p>The example includes the following parts, which can be found in the respective parts of this tutorial.</p>

	<h3>File Type & Code Editor</h3>
		<p>The example provides <a target="_blank" href="https://eclipse-che.readme.io/docs/code-editors#section-file-types">a custom file type</a> for JSON including a custom icon. For this file type it includes <a target="_blank" href="https://eclipse-che.readme.io/docs/code-editors">a custom code editor</a> for JSON files. The code editor provides <a target="_blank" href="https://eclipse-che.readme.io/docs/code-editors#section-syntax-highlighting">syntax highlighting</a> for JSON files.</p>


	    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/projectexplorer.png" alt="project explorer"/></p>

	    <p>Additionally, the example implements <a target="_blank" href="https://eclipse-che.readme.io/docs/code-editors">code completion</a> based on a list of suggestions. There are two sources for the list of suggestions, the first and simple one is <a target="_blank" href="https://eclipse-che.readme.io/docs/code-editors">directly calculated on the client</a>, it can therefore only operate on information available in the context (e.g. the current file opened). The second one is <a target="_blank" href="https://eclipse-che.readme.io/docs/serverworkspace-access#section-server-services">calculated on the server</a> and can therefore access the complete workspace to calculate the suggestions.</p>

	    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/firstname.png" alt="code completion"/></p>

	   <h3>Project Type</h3>
		<p>There is a <a target="_blank" href="https://eclipse-che.readme.io/docs/project-types">custom project type</a> for working with JSON files. The example provides <a target="_blank" href="https://eclipse-che.readme.io/docs/project-types#section-project-creation-wizard">a custom project creation wizard</a> which allows to enter project specific data. In case of the JSON example, it allows to specify a URL pointing to a JSON schema, which is later used to validate the JSON files within the project. The project wizard is available in the standard “Create New Project” dialog of Che:</p>

	    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/newproject.png" alt="new project json"/></p>

	    <p>The JSON example wizard will initialize a new project with two existing JSON files and a directory to contain custom ones:</p>

	    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/directory.png" alt="directory"/></p>

	    <h3>Actions</h3>

	    <p>The JSON example register two <a target="_blank" href="https://github.com/eclipse/che/blob/df409bdf078ed44a74a4f8917f250213532f9213/samples/sample-plugin-json/che-sample-plugin-json-ide/src/main/java/org/eclipse/che/plugin/jsonexample/ide/action/HelloWorldAction.java">project-specific actions</a> for the custom project type. The first one implements a <a target="_blank" href="https://github.com/eclipse/che/blob/master/samples/sample-plugin-json/che-sample-plugin-json-server/src/main/java/org/eclipse/che/plugin/jsonexample/JsonLocService.java">simple “Hello World”</a>. The <a target="_blank" href="https://eclipse-che.readme.io/docs/serverworkspace-access#section-workspace-services">second one</a> calls a custom service on the server which will access the workspace and count the number of lines of all JSON files within the project. It thereby also includes a template for client/server communication and for accessing source files (e.g. for validation or compilation).</p>

	    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/august/images/helloworld.png" alt="hello world app"/></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/august/images/brad.png"
        alt="Brad Micklea" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Brad Micklea <br />
        <a target="_blank" href="https://codenvy.com/">Codenvy</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://che.eclipse.org/author/brad/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/bradmicklea">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/bradmicklea">LinkedIn</a></li>
        <?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

