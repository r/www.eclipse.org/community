<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

			<p>The Internet of Things (IoT) needs no introduction: it is a widely-held view that billions of
			devices will be part of the Internet of Things in just a few short years. By giving everyday objects
			the ability to sense their surroundings and to interact with each other, the IoT is unleashing exciting
			possibilities to provide data with unparalleled temporal and spatial resolution. Yet, while this explosion
			of devices represents boundless opportunity for innovation, this ascendancy also presents daunting challenges
			such as fragmentation, vendor lock-in and the proliferation of information silos. The real potential of the IoT,
			like the potential of the Internet itself, lies in a richly connected “system of systems”. IoT systems that operate
			in isolated technology or vendor specific silos inhibit use, value, and interoperability. Sensors and “smart” devices
			have more value if they can be accessed by multiple applications, which may come and go over time. Because sensors
			and sensor systems will be connected in so many different ways and accessed by so many different applications, open
			and interoperable standards are critical to the continued evolution of the IoT.</p>

			<p>"Everything is related to everything else, but near things are more related to each other" is referred to
			as the "<a target="_blank" href="https://en.wikipedia.org/wiki/Tobler%27s_first_law_of_geography">first law of geography</a>,"
			and it applies to the IoT. The significance of the location of a smart object
			often depends on the locations of “near things,” and information about those “near things” usually comes from
			other sources. Connected cars, for example, need to communicate with other connected cars as well as traffic
			advisories, traffic signals, and routing applications. The
			<a target="_blank" href="http://www.opengeospatial.org/">OGC (Open Geospatial Consortium)</a>
			<a target="_blank" href="https://en.wikipedia.org/wiki/SensorThings_API">SensorThings API</a>
			is an OGC standard that allows IoT devices and their data to be connected in an easy-to-use and open way.
			SensorThings API’s location-enabled models allows sensor feeds from multiple sources to be integrated/aggregated
			and used more effectively for analytics, modeling, simulations, and so forth. Spatial interoperability enabled by
			SensorThings API, i.e., communicating location information and/or integrating it with location information from
			other sources, is thus a necessary requirement for IoT. IoT application developers can use the OGC SensorThings
			API to connect to various IoT devices over the Web and develop innovative applications without worrying about
			the heterogeneous protocols used by different IoT devices, gateways and services. IoT device manufacturers can
			embed the SensorThings API in IoT hardware and software platforms so that the various IoT devices in their
			offerings can have a common interface and can effortlessly connect with spatial data servers around the
			world that implement the full array of <a target="_blank" href="http://www.opengeospatial.org/ogc/markets-technologies/swe">
			OGC Sensor Web Enablement (SWE) standards</a>.</p>

			<p>SensorThings API consists two parts: (1) a standardized open data model and (2) an application
			programming interface for accessing sensors in the IoT. The core of the SensorThings API is the
			<a target="_blank" href="https://en.wikipedia.org/wiki/Observations_and_Measurements">ISO/OGC
			Observation and Measurement</a> (O&M) model. Use of a common conceptual standard model allows observation data
			from different sensors to be combined unambiguously. Figure 1 illustrate the concept of the O&M and relationship
			between the entities involved in observations.</p>

			<img class="img-responsive" src="/community/eclipse_newsletter/2016/march/images/sensorup.png"/>
			<br/>

			<p>SensorThings’ API is designed specifically for resource-constrained IoT devices and the Web developer community.
			The API employs an efficient and easy-to-use RESTful style, a JSON encoding, and the MQTT messaging transport. The URL
			pattern and query options are based on the
			<a target="_blank" href="http://docs.oasis-open.org/odata/odata/v4.0/odata-v4.0-part2-url-conventions.html">OASIS OData 4.0.</a>
			SensorThings API is part of the OGC Sensor Web Enablement
			(SWE) suite of open international standards. OGC SWE standards are a mature and integrated suite of web service
			interface and data encoding standards (v.2.0 now, v.1.0 published in 2005). SWE standards enable developers to
			make all types of sensors, transducers and sensor data repositories discoverable, accessible and useable via the
			Web. SWE standards are in use by countless large organizations such as NASA, NOAA, USGS, Natural Resources Canada,
			the World Meteorological Organization (WMO) and many other organizations, including many private sector companies.</p>

			<p>At <a target="_blank" href="http://www.sensorup.com/">SensorUp</a>, we firmly believe IoT system silos are
			hindering innovations (i.e., very very bad things!!) and the real potential of IoT depends on a
			richly connected “system of systems”. As the editor and working group
			chair of the standard, SensorUp led the development of the OGC SensorThings API. After three years of countless
			telecons and face-to-face meetings around the world, the OGC SensorThings API was approved as an official
			international standard on February 1st 2016.</p>

			<p>However, having an open standard specification is not enough. In
			order to truly realize SensorUp’s open IoT vision, IoT needs open source software codes that implements open
			standards. As a result, we are proposing the <a href="https://projects.eclipse.org/proposals/whiskers">Eclipse Whiskers project</a>.
			Whiskers is an <a target="_blank" href="https://en.wikipedia.org/wiki/SensorThings_API">OGC SensorThings API</a> framework consisting of a JavaScript
			client and a light-weight server for IoT gateways (e.g., Raspberry Pi). JavaScript is ubiquitous, powering client-side web applications
			and server-side services alike. The availability of an open source client library is an important step in the adoption of the
			OGC SensorThings standard, as it makes development quicker and easier. In addition the a client library,
			Whiskers will also have a SensorThings server module for IoT gateways, such as Raspberry Pi. Developers
			will be able to deploy and configure IoT gateways, make them SensorThings API-compliant, and connect with
			spatial data servers around the world that implement the full array of
			<a target="_blank" href="http://www.opengeospatial.org/ogc/markets-technologies/swe">OGC Sensor Web Enablement (SWE) standards</a>.</p>

			<p>The major goal of the Eclipse Whiskers is to foster a healthy and open IoT ecosystem, as opposed to one dominated by proprietary
			information silos. We aim to have Whiskers co-listed in both
			<a target="_blank" href="https://www.locationtech.org/">Eclipse LocationTech</a> and
			<a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a>.
			As Whiskers implements OGC standards, LocationTech is a perfect home for Whiskers. As OGC SensorThings
			API is an IoT standard, it also makes perfect sense for Whiskers to be an Eclipse IoT project.
			Please check out our proposal on the project web site, and join us to realize the open and
			interoperable IoT vision!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/march/images/steve.jpeg"
        alt="Steve Liang" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Steve Liang<br />
        <a target="_blank" href="http://www.sensorup.com/">SensorUp</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.sensorup.com/index.php/blog-home/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/steveliang">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

