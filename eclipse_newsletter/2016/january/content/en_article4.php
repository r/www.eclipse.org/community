<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p><a target=_blank" href="https://orionhub.org/">Eclipse Orion</a> has come a long way since the early days.  While officially becoming an Eclipse project in 2011, work had
    begun on browser based tooling for the web long before that time.  Out of the ashes of “e4” and the attempt to bring desktop
    IDE tooling and development to the web came the main idea behind Orion: “The web is the platform”.</p>

	<p>Time has shown us that this was the right way to go.  Orion consumes JavaScript based libraries and other web based
	technologies directly.  It provides stand-alone components (in particular, a JavaScript aware editor) that are
	consumed by many different organizations and projects. Finally, Orion is the basis for the
	<a target="_blank" href="http://www.ibm.com/cloud-computing/bluemix/eclipse/">
	IBM Bluemix</a> DevOps Services Web IDE, a cornerstone component of Bluemix and IBM’s cloud.</p>

	<p>Right now, it’s time to get back to our roots, with an eye toward the future: Orion is first and foremost an IDE
	 and must provide a first class development experience.  We need to reflect and ask ourselves, “What are the core
	 features of an IDE?”  The “I” stands for “Integrated” and this is well understood, but nowhere is the list of
	 core features formally specified for a “Development Environment”.</p>

	<p>Code development is a continuum that starts with an editor and ends with a fully integrated development experience.
	These days, developers use many different tools, languages and environments.  At least for web development, the idea
	of deep integration and a single environment might increasingly be a notion from the past.</p>

	<p>One thing is for sure: An IDE is about making programmers productive and this means great language tooling.
	Orion makes coding more productive by providing:</p>
		<ul>
			<li><b>Error checking (and Quick Fixes)</b> – to find errors in the code before it gets released</li>
			<li><b>Code Completion</b> – to help the write code but also see what code is available</li>
			<li><b>Navigation</b> – to move quickly around the code (for example, to the definition of a code element)</li>
			<li><b>Search</b> – to understand where and how code elements are used</li>
		</ul>

	<p>To find errors early, Orion uses ESLint. ESLint is integrated in the IDE however; ESLint works in the context of a single file. Starting at Orion 9,
	cross-file support was added to allow navigation between call sites and function declarations. This work made type inference data available for use in other contexts.
	Putting the two ideas together, future 	versions of Orion will be smarter about errors and be able to catch problems earlier for entire programs, not just single files.</p>

	<p>Navigation is important but even more important is the ability to understand a large code base.  This is particularly true when
	onboarding new team members, but it is also an issue for experienced members.  It’s hard to know every line and all developers need
	to find their way easily around a code base.</p>

	<p>Often, code does not get refactored.  This is particularly true for dynamic languages where refactoring is difficult due to
	missing type information.  Over time, new code is written to implement features and the code base grows.  As it grows, it
	becomes harder to maintain, team members with tribal knowledge leave and others come on board.</p>

	<p>Future versions of Orion will provide a Structured Search capability to allow programmers to reason about their code.
	From here, the next logical step is refactoring.  Refactoring with limited type information is difficult, but refactoring
	is a great reason to use an IDE.</p>

	<p>Orion is about productivity and first class tooling for developers, created by developers, makes them more efficient.
	Come to our talks at EclipseCon,
	<a target="_blank" href="https://www.eclipsecon.org/na2016/session/50-shades-ide">50 Shades of an IDE</a> and
	<a target="_blank" href="https://www.eclipsecon.org/na2016/session/eclipse-orion-fast-functional-and-your-fingertips">
	Eclipse Orion: Fast, Functional and at Your Fingertips</a>, and join
	the conversation as we "get back to our roots".</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/january/images/steve.jpg"
        alt="Steve Northover" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Steve Northover<br />
        <a target="_blank" href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/stevenorthover">LinkedIn</a>
        </li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

