<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>We are pleased to announce the beta milestone of Eclipse Che!</p>

	<p><a href="http://www.eclipse.org/che/">Eclipse Che</a> is a next generation Eclipse IDE and open source alternative to IntelliJ.</p>

	<p>We are building a world where anyone anywhere can contribute to a project without installing software. Essential to this is an on-demand IDE where both
	the tooling and its workspace runtime can be dynamically provisioned.</p>

	<p>Existing IDEs are not up to this task: complex per-computer installations, non-portable localhost workspaces that lead to "but it works on my machine"
	issues, and limited shared collaboration services.</p>

	<p>Eclipse Che is the first IDE where workspace portability, user collaboration, and browser-optimized UI are core tenets. And, with projects like Brackets
	and Orion focusing increasingly on impressive web-editing, Cloud 9 closing their SDK, and Codebox unsupported, Eclipse Che is now the only open source cloud IDE.</p>

	<p>Che defines a <a href="http://www.eclipse.org/che/features/#new-workspace">new type of workspace composed of projects and its associated runtimes</a>,
	making its state distributable, portable and versionable. We
	use VMs, containers, and web services to bring repeatability, consistency, and performance to workspaces. Workspace configuration is persisted as
	versionable assets used to create portable replicas. Migrate workspace projects and runtimes to other Che instances, whether localhost or in the
	cloud. Receiving systems use the configuration to create an identical workspace state matching the source system. Che manages your workspace
	lifecycle, both orchestrating project state and booting / suspending / imaging / stopping / destroying workspace runtime environments.</p>

	<br/>
	<a href="/community/eclipse_newsletter/2016/january/images/flow.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/flow.png"/></a>
	<br/><br/>

	<p>Workspace environments are powered by Docker (or any machine implementation you choose to provide). A machine is a runtime whose stack
	is instantiated by a recipe. Docker is a powerful and convenient implementation of machines providing near-instant activation, copy-on-write
	file system, and Dockerfile recipes for constructing custom stacks. Launch from our provided images, DockerHub, private registries, or through
	your own Dockerfiles. Your projects are mounted into the workspace, or if a remote machine, are rsync'd with long term project storage.</p>
	<br/>
	<a href="/community/eclipse_newsletter/2016/january/images/new_project.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/new_project.png"/></a>
	<br/><br/>

	<p>These workspaces are then accessed by Che’s browser IDE, through a web terminal, or by your existing desktop IDE through SSH. The IDE is meticulously
	designed with little details (Orion editor and light theme!) to make Che enjoyable for everyone.</p>

	<br/>
	<a href="/community/eclipse_newsletter/2016/january/images/ide.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/ide.png"/></a>
	<br/><br/>

	<p>Product teams can use Che as a workspace server, with access to a rich REST and Java library for controlling workspaces and building plug-ins. There has been early
	 support and contributions from Codenvy, eXo Platform, Serli, IBM, Microsoft, SAP, Red Hat - with interest from dozens of others.</p>
	<br/>
	<a href="/community/eclipse_newsletter/2016/january/images/swagger.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/swagger.png"/></a>
	<br/><br/>

	<p>Developers and plug-in authors can dive deeper learning about <a href="http://www.eclipse.org/che/features/#workspace-agents">workspace agents</a>,
	<a href="http://www.eclipse.org/che/features/#snapshot">workspace snapshots</a>, <a href="http://www.eclipse.org/che/features/#collaborative">collaborative workspaces</a>,
	<a href="http://www.eclipse.org/che/features/#ioe">integrated operations</a> (see the <a href="https://eclipse-che.readme.io/docs/openshift-config">OpenShift plug-in</a>),
	<a href="http://www.eclipse.org/che/features/#multi-machine">multi-machine workspaces</a>, <a href="http://www.eclipse.org/che/features/#multi-project">multi-project explorers</a>,
	<a href="http://www.eclipse.org/che/features/#commands">commands / previews</a>, and rich
	<a href="http://www.eclipse.org/che/features/#intellisense-java">Java / JavaScript Intellisense</a>.</p>

	<p>It seems that the market anticipation for Eclipse Che has been surprisingly high. We have received 1200 GitHub stars from users - and that’s before
	this announcement. Thank you!  Keep it coming, we appreciate and depend on the community to contribute for Che to be a success.</p>

	<p>Developers can use Che as their IDE today. Download and <a href="https://eclipse-che.readme.io/docs/get-started-with-java-and-che">start the step-by-step tutorials</a>. Or,
	<a href="http://www.eclipse.org/che/">visit the all-new Eclipse Che website</a> to see how Che works.</p>

	<p><a href="http://www.eclipse.org/che/download">Download</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
	<a href="https://github.com/codenvy/che/blob/master/CONTRIBUTING.md">Contribute</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
	<a href="https://eclipse-che.readme.io/docs/introduction">Docs</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://github.com/codenvy/che/issues">Issues</a></p>

	<p>Note that Che will use GitHub instead of Bugzilla for issues. We are uploading known issues (&acd;300). Please do take time to file any issues or improvement items.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/january/images/tyler.jpg"
        alt="Tyler Jewell" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Tyler Jewell<br />
        <a target="_blank" href="https://codenvy.com/">
Founder & CEO, Codenvy</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/TylerJewell">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/tylerjewell">LinkedIn</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

