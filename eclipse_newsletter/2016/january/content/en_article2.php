<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
			<p>Lately Red Hat teams involved with Eclipse plug-ins have been working
			on improving the support for Eclipse in context of Container based
			development - specifically, <a target="_blank" href="https://www.docker.com/">Docker</a>,
			<a target="_blank" href="https://www.vagrantup.com/">Vagrant</a> and
			<a target="_blank" href="http://kubernetes.io/">Kubernetes</a> (via
			<a target="_blank" href="https://www.openshift.com/">OpenShift tooling</a>).</p>

			<p>I'll be speaking at EclipseCon 2016, in Reston, on the details of this, but this
			article will outline some of the reasoning behind this tooling
			and some of its highlights.</p>

		<h2>Docker</h2>

			<p>The Docker tooling provides various views to browse your
			images and containers as well as commands for build and launch
			images.</p>

			<br/>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/docker_explorer_view.png"/>
			<br/><br/>

			<p>Docker Tooling was included in Eclipse Mars and have been improved and
			extended in both Mars.1 and upcoming Mars.2. One of the bigger ones
			are being able to have a Docker Run be managed as a Launch
			Configuration.</p>

			<br/>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/run_image_launch_configuration.png"/>
			<br/><br/>

			<p>Meaning you can now share and save your favorite Docker launches in
			your workspace for repeated and optional shared usage.</p>

		<h2>Vagrant</h2>

			<p>Vagrant Tooling was added recently and is thus not as complete as the
			Docker tooling, but still a good start for future integrations.</p>

			<br/>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/vagrant-toolbar.png"/>
			<br/><br/>

			<p>It contains views for listing known vagrant boxes and running vm's;
			but even more it contains a small dropdown toolbar where you can
			start/stop Vagrant and open a text editor for editing your vagrant
			files.</p>

		<h2>OpenShift</h2>

			<p>OpenShift Tooling is part of JBoss Tools and can be installed on its
			own. The recent version of OpenShift tooling works with OpenShift 3
			that is using 100% Docker images for running its containers.</p>

			<p>OpenShift 3 is based on Kubernetes thus the OpenShift Tooling is
			using the Kubernetes API and its concepts + some OpenShift extensions.

			<p>The OpenShift tooling provides similar to Docker a view to explore
			your OpenShift/Kubernetes connection to start/stop applications -
			which consist of one or more Docker images/containers.</p>

			<br/>
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/openshift-menus.gif"/>
			<br/><br/>

			<p>From within OpenShift Explorer you can launch, deploy and introspect your
			application.</p>
			<br/>
			<a href="/community/eclipse_newsletter/2016/january/images/search_docker_images.png">
			<img class="img-responsive" src="/community/eclipse_newsletter/2016/january/images/search_docker_images_small.png"/></a>
			<br/><br/>

			<p>It also allow you to use the Docker tooling to deploy a docker image
			to OpenShift.</p>

		<h2>Where to get it ?</h2>

			<p>Vagrant and Docker tooling are developed as part of Eclipse Linux
			Tools and part of Eclipse Mars releases. Note, that despite the
			overall project name the Docker and Vagrant tooling does run on other
			platforms, like Windows and OS X.</p>

			<p>The OpenShift tooling is available from JBoss Tools and thus not
			default included in Eclipse Mars. You can install it from Eclipse
			Marketplace or download it from <a target="_blank" href="http://tools.jboss.org">http://tools.jboss.org</a>.</p>

		<h2>Introducing Container Development Toolkit</h2>

			<p>Now, why are Red Hat interested in making this experience
			great from within Eclipse ?

			<p>One part is of course that we think container based development
			is here to stay and Eclipse as an IDE needs to be able to at least
			work with these technologies - having first class support for
			them is the one part that lets plug-in users and developers try
			and explore.</p>

			<p>The other is that with container based setup and the (almost)
			universal access to virtualization it is now possible for us to
			deliver a "PaaS-in-a-box" solution. A solution where you from
			any machine can install and run a "close-to-production" environment
			with access to containers (Docker), PaaS (OpenShift) all virtualized
			(Vagrant) running on Red Hat supported bits.</p>

			<p>All configured to work together and out-of-the-box from your
			command line *and* IDE (in this case Eclipse).</p>

			<p>We call this the Container Develoment Toolkit (CDK) and we are working
			on providing a Vagrant based install that comes optionally bundled
			with Eclipse pre-configured to work with the Vagrant box, Docker and
			OpenShift.</p>

			<p>All ready to run with after a single download and install.</p>

			<p>We are still developing on this but you can see a preview of the work
			demonstrated in this video</p>
			<br/>
			<iframe width="560" height="315" src="https://www.youtube.com/embed/h6GGYUdj3gg" frameborder="0" allowfullscreen></iframe>

		<h2>Want to hear/learn more ?</h2>

			<p>I will be giving <a target="_blank" href="https://www.eclipsecon.org/na2016/session/docker-vagrant-and-kubernetes-walks-eclipsed-bar">a talk</a>
			at EclipseCon 2016 which will demo these pieces and talk about where
			Red Hat will be going with moving container based development forward
			in the world of Eclipse.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/january/images/max.jpg"
        alt="Max Rydahl Andersen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Max Rydahl Andersen <br />
        <a target="_blank" href="http://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://xam.dk/blog/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/maxandersen">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
