<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  // Begin: page-specific settings. Change these.
  $pageTitle = "IoT is the New Black";
  $pageKeywords = "eclipse, newsletter, eclipse che";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "This month we're featuring Eclipse Internet of Things (IoT) articles. Start your Eclipse IoT journey by following along as we demonstrate how Eclipse Edje is headed towards a full IoT Stack for microcontrollers. Next, get started with Eclipse hawkBit and make the best of software update management services. Finish by reading up on the upcoming Eclipse SmartHome features and on the next version of MQTT, that will soon be released.";