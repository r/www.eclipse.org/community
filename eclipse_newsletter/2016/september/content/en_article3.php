<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
		<p><a target="_blank" href="http://mqtt.org/">MQTT</a> is a transport (not protocol as Andy Stanford-Clark would say), which allows applications to easily communicate with each other over TCP/IP. I have been working with MQTT in some regard since the early 2000s, and am now lead of the <a href="https://www.eclipse.org/paho/">Eclipse Paho</a> project, which supplies client libraries in a variety of programming languages. I've seen MQTT versions progress from 3.0, to 3.1 and last year 3.1.1, the first standardized version not published by IBM, but by the OASIS consortium. In fact when I implemented an early version of MQTT software, Andy Stanford-Clark asked me to include support for version 2.0, it was that long ago.</p>

		<p>The brief for the MQTT 3.1.1 standardization process was to make minimal changes to ensure that it could be completed quickly. But when it was done, I think that just about every committee member had some burning issues. As a result, we embarked on the next round of updates, attempting to balance the competing forces of stability and progress. Personally, I envision that the next version of MQTT should be good enough to be fundamentally untouched for perhaps a decade, and that 3.1.1 will coexist with it.</p>

		<p>One of the first problems is what to call this version. This seems simple, but there is only one byte to represent the protocol version in the MQTT connect packet. MQTT 3.1 used 3.  MQTT 3.1.1 uses 4. In writing Paho embedded client libraries, I've even referred to MQTT 3.1.1 as version 4 myself.  After not inconsiderable deliberation, it has been agreed this time to make the document and the wire versions the same. This means they are both 5, and that skipping 4 has no more significance than it has been used already. We expect this principle to be followed in the future, but time will tell.  (Actually the document version will be 5.0, because we've been told that the .0 must exist).</p>

		<p>For this revision of MQTT, the committee has had the welcome addition of members from Microsoft. This is one recognition of the importance that MQTT is attaining in the Internet of Things (IoT). One consequence is that we have set ourselves a quite aggressive schedule, to complete version 5 in the first quarter of 2017. This means having an implementable first public draft in the Autumn of 2016.</p>

		<p>A good way to understand the thinking driving the changes in MQTT 5.0 is the <a target="_blank" href="https://www.oasis-open.org/committees/download.php/57616/Big%20Ideas%20for%20MQTT%20v5.pdf">"Big Ideas" document</a>. The ideas are:</p>

		<h3>Improvements for Scalability and Large Scale Implementations</h3>
		<p>Retained messages, QoS 2 and cleansession false all require state to be held which can make it more difficult to implement a solution where clients may reconnect to a different endpoint. It will be possible for a server to indicate a lack of support for these features at connect time. The ability for multiple clients to share a single subscription will allow applications to be horizontally scaled.</p>

		<p>Another issue to be addressed under this banner is called <a target="_blank" href="https://issues.oasis-open.org/browse/MQTT-263">Simplified State Management</a>. This has at least two major advantages.</p>

		<p>1) As an application, I only want my session state to be discarded when I've completed all my work, not when my network connection fails. This was inconveniently hard in all previous versions of MQTT - not in version 5.</p>

		<p>2) The ability for session state to have an expiry time. If the client does not connect within a certain amount of time, the session state can be deleted by the server.  This obviates the need for a client to reconnect just to clean up session state.</p>

		<p>The connection state diagram* for MQTT 3.1.1 is shown here:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/mqttv3.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/mqttv3_sm.png" alt="MQTT v3 State Transition"/></a></p>


		<p>The new state diagram* for 5.0 is here:</p>

		<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/mqttv5.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/mqttv5_sm.png" alt="MQTT v5 State Transition"/></a></p>

		<p>As you can see, as well as having more functionality, the transitions are simpler.</p>

		<h3>Improved Error Reporting</h3>
		<p>In 3.1.1, negative acknowledgements (nacks) were added for subscribe commands. This still left publish requests without - this will <a target="_blank" href="https://issues.oasis-open.org/browse/MQTT-236">now be fixed</a>. All response packets will now be able to carry error codes, and the specification will list them and their meanings. MQTT servers will be allowed to send disconnect commands with reason codes, such as in the case of a server shutdown, which should help MQTT clients take more appropriate action.</p>

		<h3>Metadata for the <a target="_blank" href="https://issues.oasis-open.org/browse/MQTT-256">Publish Command</a></a></h3>
		<p>The format of the payload can be indicated, and some other possible fields such as time to live (message expiry), reply-to topic and correlation identifier for a request reply mode of operation. At the moment, it is planned that the available metadata fields will be predefined, for the sake of simplicity.</p>

		<h3>Resource Constrained Clients and Performance Improvements</h3>
		<p>Nolocal subscriptions - the ability for a subscriber to not receive its own publications. This will make it easier for the perennial first MQTT application to be written - the chat room! Topic registration also falls into this category, although the current proposal is a simplified version, where using a topic of length 0 indicates "re-use the last one I sent".  That may yet be revised.</p>

		<p>Working draft 4 is <a target="_blank" href="https://www.oasis-open.org/committees/download.php/58851/mqtt-v5.0-wd04.pdf">available now</a>. At the end of September the committee is meeting face to face at IBM Hursley - <a target="_blank" href="https://www.oasis-open.org/committees/download.php/58853/MQTT_F2F_September_Agenda_DRAFT_1.odt">the agenda</a>. If you are not on the committee but would like to submit a comment about the draft or any other subject, instructions for doing so can be found <a target="_blank" href="https://www.oasis-open.org/committees/comments/index.php?wg_abbrev=mqtt">here</a>.</p>

		<p><i><small>*Both MQTT diagrams created by Andy Banks (IBM)</small></i></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/september/images/ianc.jpg"
        alt="Ian Craggs" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ian Craggs<br />
        <a target="_blank" href="https://www.ibm.com">IBM</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/icraggs">LinkedIn</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/icraggs">Twitter</a></li>
        <?php echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

