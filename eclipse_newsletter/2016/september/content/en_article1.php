<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    	<a href="https://projects.eclipse.org/proposals/edje"><img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/edjelogo.png" alt="Eclipse Edje Logo"/></a>

    	<p>Developing for the embedded world is a really interesting challenge, as any object may become part of the Internet of Things (IoT). Unfortunately, to achieve that goal, companies may not be able to afford high end processors (such as the one found in a Rasberry Pi 3) for every device. Even though you have “only” few dollars to put in your “Thing” to make it smart, you can still manage to run multiple IoT libraries on it. Using processors like the ARM Cortex-M also enables you to reduce your device’s power consumption and extend your device’s precious battery lifetime.</p>

		<p>For my latest demo, I worked on a connected washing machine. From the user’s perspective, this washing machine should have an intuitive and appealing graphical user interface (GUI) and be able to send notifications to other mobile or IoT devices. The company manufacturing this device should be able to remotely manage it (installing new software, starting/stopping applications, getting use information…). As always, it should use cheap microelectronic parts. The GUI also needs to be portable for other devices and the IoT software components should be reusable and independent from each other.</p>

	<h2>Smart Appliance Demo Comes to Life</h2>
	<h3>Hardware Requirements</h3>
		<p>In order to use a cheap and low-power device, I targeted an ARM Cortex-M7 chip. The <b>STM32F746G-DISCO</b> evaluation board from STMicroelectronics was selected to be at the heart of the system because it meets all my requirements:</p>
		<ul>
			<li>Small</li>
			<li>Low-power</li>
			<li>An LCD and touch display with 2D graphics acceleration</li>
			<li>Arduino port</li>
			<li>USB port</li>
			<li>Ethernet port</li>
			<li>Extra external RAM and storage (SD card) if needed</li>
			<li>Available low-level board support package (BSP) and C toolchain</li>
		</ul>
		<p>I also selected a STMicroelectronics <b>STM32F412-DISCO</b> (ARM Cortex-M4) board to simulate a smartwatch and an <b>EBV Jakarta</b> (ARM Cortex-M0+) as a small device capable of receiving notifications over a wireless connection.</p>

		<p>The rest of the hardware includes:</p>
		<ul>
			<li>A PC as a server, hosting services that would normally run on the Internet</li>
			<li>A tablet to receive some notifications over a Bluetooth connection</li>
			<li>A Z-Wave switch and dongle to control a light</li>
			<li>An actual washing machine toy to play with its motor</li>
			<li>Other MCU boards for running the same washing machine application on different hardware (Renesas RZ, NXP Tower K65…). Note that a specific AllJoyn thin client is only available for Renesas boards at this time.</li>
		</ul>


	  	<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/setup.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/setup_sm.png" alt="demo setup"/></a></p>
	  	<p align="center"><b>Demo Setup</b></p>


		<h3>Software Integration and Development</h3>
		<p>Now that the hardware background was set, I had to select the software bricks to build my washing machine application.
			Browsing through Eclipse IoT projects gave me the components I needed:
		</p>
		<ul>
		<li>
			<b><a href="http://www.eclipse.org/paho/">Paho</a></b>
			: An implementation of MQ Telemetry Transport (MQTT)
			messaging protocol. I used it to send notifications (device
			state, choice of program…) from the STM32F746 to a server.
		</li>
		<li>
			<b><a href="http://eclipse.org/mosquitto">Mosquitto</a></b>
			: A server implementation for the MQTT messaging protocol. I
			used it on a Windows PC to receive the notifications from
			the washing machine application running on the STM32F746.
		</li>
		<b><a href="http://projects.eclipse.org/projects/iot.leshan">Leshan</a></b>
		: A Lightweight M2M (LWM2M) protocol implementation. It relies
		on Eclipse’s project <a href="https://www.eclipse.org/californium/">Californium</a> (implementing the Constrained
		Application Protocol: CoAP). I used both a Leshan server on a
		Windows PC and a Leshan client on the STM32F746. This
		demonstrates the device management, from the server I could get
		some information about the board (id, memory used, installed
		applications), and command it (start/stop applications).
		<li>
			<b><a href="http://projects.eclipse.org/projects/iot.edje">Edje</a></b>
			It defines a standard and portable high-level Java API
			called Hardware Abstraction Layer (HAL) for accessing
			hardware features delivered by microcontrollers. I used it
			in order to manage the motor's general purpose
			inputs/outputs (GPIO), a Bluetooth dongle through a UART
			port and a Z-Wave dongle over USB.
		</li>
		</ul>

		<p>I developed my application using the Java language running on MicroEJ OS because:</p>
		<ul>
			<li>All needed Eclipse IoT projects have an implementation in the Java language.</li>
			<li>MicroEJ OS provided me the virtualization I need to get my washing machine to run on different hardware.</li>
			<li>I had a really short deadline and using object oriented programming with Eclipse tools saves a lot of time.</li>
			<li>Thanks to MicroEJ Simulator, I could work on the GUI without losing time to flash the board.</li>
			<li>MicroEJ OS provides most of the libraries I needed.</li>
		</ul><br/>

		<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/demooverview.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/demooverview_sm.png" alt="demo overview"/></a></p>
		<p align="center"><b>Demo Overview</b></p>


	<h3>Adapting the Eclipse IoT Projects to Embedded Targets</h3>
		<p>After adapting the Eclipse projects I selected to make them run on the MicroEJ OS runtime, I could integrate them to work together.</p>
		<p>With only 340 KB of RAM available for both application needs and the different buffers (display, network), all libraries could not run at the same time as there were too many threads. As most of them were timers and “Executors” (a <a target="_blank" href="http://developer.microej.com/javadoc/microej_4.0/addons/java/util/concurrent/Executor.html">Java language implementation</a> of a Command <a target="_blank" href="https://en.wikipedia.org/wiki/Command_pattern">design pattern</a>). I could reduce the actual number of threads by using MicroEJ OS service framework.</p>
		<p>This framework presents a way to share services using a "<a target="_blank" href="https://en.wikipedia.org/wiki/Singleton_pattern">Singleton</a>" design pattern for each service. I declare Executors and timers as services. Then I could load the Singletons, instead of creating new threads each time.</p>

<pre>
// Gets the service loader Singleton.
ServiceLoader serviceLoader = ServiceLoaderFactory.getServiceLoader();
// Load a shared Timer.
Timer timer = serviceLoader.getService(Timer.class);
if(timer!=null){
   timer.schedule(myTask, 0);
}
</pre>

		<p>To keep the GUI reactive and smooth, I played with the thread priorities. Every thread managing the GUI has a higher priority than the background tasks. For instance, the controller uses a low-priority Executor notifying all the registered applications one by one, even though it may add a delay for the notification.</p>

	<h3>Using the Eclipse Edje Framework to Support Z-Wave over USB</h3>
	<p>I used a Z-Wave plug paired to a Z-Wave dongle to switch on some lights when the washing machine was running.</p>

	<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/equipmentedje.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/equipmentedje_sm.jpg" alt="z-wave setup"/></a></p>

	<p>MicroEJ OS provides a driver for using a Z-Wave dongle over USB. When the dongle is plugged to the USB port, the OS populates the peripheral manager with the paired devices. I could then load the Z-Wave switches and switch them on/off. </p>

<pre>
/**
 * Washing Machine status callback.
 *
 * @param status
 *            true if the washing machine is running.
 */
public void statusChanged(boolean status) {
  // Iterator for the SwitchWithReturnState devices (such as ZWave
  // switches).
  Iterator<SwitchWithReturnState> iterator = PeripheralManager.list(SwitchWithReturnState.class);
  while (iterator.hasNext()) {
    try {
       SwitchWithReturnState switchWithReturnState = iterator.next();
       if (status) {
	switchWithReturnState.on();
       } else {
	switchWithReturnState.off();
       }
    } catch (IOException e) {
       // A communication error with the device occured,
       e.printStackTrace();
    }
  }
</pre>

	<h3>Using the Eclipse Edje Framework to Support GPIO</h3>
	<p>The washing machine commands were tuned to interface with some GPIOs:</p>

	<table class="table">
	<tr>
		<td>Motor cycle ON on rising edge (idle state HIGH)</td>
		<td>OUTPUT</td>
	</tr>

	<tr>
		<td>Motor cycle OFF on rising edge (idle state HIGH)</td>
		<td>OUTPUT</td>
	</tr>

	<tr>
		<td>Motor State: Low Level = motor off, High Level = motor on</td>
		<td>INPUT (open drain)</td>
	</tr>

	<tr>
		<td>Door State: Low Level = closed door, High Level = opened door</td>
		<td>INPUT (pull up)</td>
	</tr>
	</table>

	<p>Once I connected the Arduino connector to the STM32F746 board and wired it to the motor, I could play with the motor using Edje's GPIO interface.</p>

<pre>
public class HalNotificationHandler implements WashingMachineListener {
  private static final int DOOR_PIN = 2;
  private static final int MOTOR_ON_PIN = 2;
  private static final int MOTOR_OFF_PIN = 3;

  private Port doorPort;
  private Port motorOnPort;
  private Port motorOffPort;


  public HalNotificationHandler() {
    super();
    // Initialize GPIOs
    doorPort = GPIO.getPort(“DIGITAL_ARDUINO”);
    motorOnPort = GPIO.getPort(“DIGITAL_ARDUINO”);
    motorOffPort = GPIO.getPort(“DIGITAL_ARDUINO”);

    doorPort.setMode(DOOR_PIN, GPIO.Mode.DIGITAL_INPUT_PULLUP);
    motorOnPort.setMode(MOTOR_ON_PIN, GPIO.Mode.DIGITAL_OUTPUT);
    motorOffPort.setMode(MOTOR_OFF_PIN, GPIO.Mode.DIGITAL_OUTPUT);

    motorOnPort.setDigitalValue(MOTOR_ON_PIN, true);
    motorOffPort.setDigitalValue(MOTOR_OFF_PIN, true);
  }

  /**
   * Washing Machine status callback.
   *
   * @param status
   *            true if the washing machine is running.
   */
  public void statusChanged(boolean status) {
    if (status) {
      // If door close
      if (!doorPort.getDigitalValue(DOOR_PIN))) {
        press(motorOnPort, MOTOR_ON_PIN);
      }
    } else {
      press(motorOffPort, MOTOR_OFF_PIN);
    }
  }

   /**
   * Press a button and release it.
   */
  public void press(Port port, int pin) {
    port.setDigitalValue(pin, false);
    sleep();
    port.setDigitalValue(pin, true);
  }

 //
  …
 //
}
</pre>

  	<h3>Using the Eclipse Edje Framework to Support Bluetooth over UART</h3>

		<p>I used HC-05 Bluetooth modules (a Bluetooth SPP  - Serial Port Protocol - module) to exchange data between the STM32F746, STM32F412 and Jakarta boards. This module provides an UART-like connection over Bluetooth. I could use Edje’s Com connection to drive the modules.</p>

<pre>
// Parameters for the connection. Com 42 is the virtual port linked by MicroEJ to the PIN CN4 D0 and D1.
final String CONNECTION_STRING = "comm:com42;baudrate=9600;bitsperchar=8;stopbits=1;parity=none";
try{
  String text = notification_value;
  CommConnection comm = (CommConnection)Connector.open(connectionString);
  OutputStream out = comm.openOutputStream();
  out.write(text.getBytes());
} catch(Exception e){
  //
  …
  //
}
</pre>


  		<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/raspberrypi.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/raspberrypi_sm.jpg" alt="raspberri pi connections"/></a></p>
  	<p align="center"><b>STM32F746 Connectors</b></p>

  	<h3>Using the Eclipse Paho Framework to Support MQTT</h3>
  	<p>For this demo, I setup a Mosquitto broker on a Windows PC with a fixed IP address. This went flawlessly, I just had to use the Microsoft Windows installer and install its dependencies.
Adding MQTT notifications to the board was pretty easy, I just needed to create the client with my broker’s address.</p>

<pre>
MqttClient client = new MqttClient(BROKER, PUBLISHER_ID);
client.connect();
</pre>

	<p>Then, I created a new message for any of the notifications sent by the washing-machine (e.g. program changed, laundry started, laundry stopped…)</p>

<pre>
MqttMessage message = new MqttMessage();
String text = notification_value;
message.setPayload(text.getBytes());
client.publish(NOTIFICATION_TOPIC, message);
</pre>

<h3>Using the Eclipse Leshan Framework to Support LWM2M</h3>

	<p>To demonstrate the use of LWM2M, I designed the notification services as distinct applications that register themselves as shared services.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/Software_Overview.png" alt="main board software overview"/></p>
  	<p align="center"><b>Main Board Software Overview</b></p><br/>

	<p>The LWM2M client connects to the server at a known address. Then the server can request information about the board (id, available memory, current number of threads…). It also asks for the available applications and start or stop them.</p>
	<br/>

	<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/lwm2mdemo.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/lwm2mdemo_sm.png" alt="Leshan Server UI"/></a></p>
  	<p align="center"><b>Leshan Server UI</b></p><br/><br/>

  	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/zwavedriver.png" alt="Leshan Server UI detail"/></p>
  	<p align="center"><b>Leshan Server UI Detail</b></p>

	<h2>The Demo is Alive!</h2>
	<p>Thanks to all those Eclipse frameworks, I achieved my goals and managed to get many protocols running on the same MCU, connecting the main board to two servers on a PC (Leshan, Mosquitto), two other MCU boards (Jakarta and STM32F412) and a couple of hardware elements (Z-Wave lights and the actual washing machine toy). And above all I could meet my deadline!</p>
	<p>Thanks to MicroEJ, my main washing machine application is small, efficient and maintainable with elegant architecture choices, appealing with different GUI skins, internationalized with multiple languages, and portable so it can be reused on a wide range of hardware platforms.</p>

	  	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/jakarta.PNG" alt="Jakarta and STM32F412 GUI"/></p>
  	<p align="center"><b>Jakarta and STM32F412 GUI</b></p> <br/><br/>

  	  	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/washingmachine.png" alt="STM32F746 GUI"/></p>
  	<p align="center"><b>STM32F746 GUI</b></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/september/images/pierre.jpg"
        alt="Pierre Rivron" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Pierre Rivron<br />
        <a target="_blank" href="https://microej.com/">
MicroEJ</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/privron/en">LinkedIn</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

