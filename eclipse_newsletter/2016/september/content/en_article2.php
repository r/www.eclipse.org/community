<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    	<a href="https://projects.eclipse.org/proposals/hawkbit"><img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/hawkbit.png" alt="hawkBit logo Process"/></p></a>
    	<p><a target="_blank" href="https://github.com/eclipse/hawkbit">Eclipse hawkBit</a> is a domain independent back-end system for rolling out software updates to constrained edge devices as well as more powerful controllers and gateways connected to IP based networking infrastructure.</p>

		<p>The project was started in November 2015, the initial contribution by Bosch followed soon after. The idea was from the beginning to create a set of Spring (Boot) modules that can be used to create a standalone software update cloud service or alternatively embed hawkBit into an existing application.</p>

		<p>hawkBit provides a repository for devices and software artifacts combined with a software update management focused on IoT use cases. That includes APIs and a user interfaces for repository and update management (Management UI & API) operations.</p>

		<p>For direct device integration hawkBit offers a simple HTTP based API with JSON payload (Direct Device Integration API – DDI). It allows device integrators to separate concerns by means of having distinguished channels for business data and general device management tasks on one side and software update on the other.</p>

		<p>Indirect device integration (allows projects to leverage hawkBit on top of another connectivity layer) is provided with the Device Management Federation API (DMF) that is currently based on AMQP 0.9 but the goal is to offer an alternative based on Eclipse Hono in the future.</p>

		<p>Indirect integration is especially useful if a constrained device cannot handle a TLS/HTTP connection, is supporting a standard device management protocol that covers also the software update part (e.g. TR-069, OMA-DM, LWM2M) or the device is already connected to a back-end and hawkBit is introduced later on.</p>

    <p>This combination of UI and API for repository management and software update management in addition to the direct and indirect device integration options and last but not least the flexible Spring (Boot) based programming model offers hawkBit users several options for leveraging its capabilities:</p>

		<ul>
			<li>Run hawkBit without the need to write even one line of code (except on the device) with DDI API and Management UI.</li>
			<li>Run hawkBit as an extra service that is integrated into the IoT system landscape with DMF and Management API.</li>
			<li>Embed hawkBit modules into another Java/Spring based cloud service.</li>
			<li>In addition, all kinds of combinations depending on the use case.</li>
		</ul>

			<p align="center"><a href="/community/eclipse_newsletter/2016/september/images/process.jpeg"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/process_sm.jpeg" alt="hawkbit Process"/></a></p>

	<h2>Getting Started</h2>

	<p>Unfortunately, the project has not managed to provide release on maven central until now. As a result, a GitHub clone followed by a maven build is required.</p>

    <p class="bg-info">&nbsp;<b>Listing 1 – Clone and build</b></p>
    <pre>$ git clone https://github.com/eclipse/hawkbit.git</pre>
    <pre>$ mvn clean install</pre>
    <p class="bg-info">&nbsp;End</p>

    <p>hawkBit comes with an example Spring Boot app that contains its core functionality. Run it directly from your IDE or from the command line:</p>

       <p class="bg-info">&nbsp;<b>Listing 2 – Run the example app</b></p>
    <pre>$ java -jar ./examples/hawkbit-example-app/target/hawkbit-example-app-0.2.0-SNAPSHOT.jar --hawkbit.dmf.rabbitmq.enabled=false</pre>
    <p class="bg-info">&nbsp;End</p>

    <p>If you open your browser you see hawkBit’s Management UI under <a target="_blank" href="http://localhost:8080/UI">http://localhost:8080/UI</a></p>

    <p align="center"><a href="/community/eclipse_newsletter/2016/september/images/deployment_mgmt.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/deployment_mgmt_sm.png" alt="Deployment Management Process"/></a></p>

    <p>However, an empty system is kind of boring so we run two of the simulators that come with the project. First, the device simulator that connects to the hawkBit server through the HTTP based Direct Device Integration (DDI) API. Note: the simulator supports the AMQP based Device Management Federation (DMF) API as well.</p>

    <p class="bg-info">&nbsp;<b>Listing 3 – Run the device simulator</b></p>
    <pre>$ java –jar ./examples/hawkbit-device-simulator/target/hawkbit-device-simulator-0.2.0-SNAPSHOT.jar --hawkbit.device.simulator.amqp.enabled=false</pre>
    <pre>$ curl "http://localhost:8083/start?amount=10&api=ddi&polldelay=10"</pre>
    <p class="bg-info">&nbsp;End</p>

    <p>We got devices in the system now. We will also need software to install. Again we will keep it simple by means of skipping the use of real artifacts but instead creating software package metadata only. For that hawkBit offers a Management API simulator: </p>

	   <p class="bg-info">&nbsp;<b>Listing 4 – Generate data</b></p>
    <pre>$ java -jar ./examples/hawkbit-example-mgmt-simulator/target/hawkbit-example-mgmt-simulator-0.2.0-SNAPSHOT.jar</pre>
    <p class="bg-info">&nbsp;End</p>

    <p>After that we are ready to update our simulated devices. Happy deploying &#128515;</p><br/><br/>

    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2016/september/images/deployment.gif" alt="Deployment Gif"/></p>

    <h2>Status & Roadmap</h2>
    <p>The main goals for 2016 were modularization of the code base after the initial contribution, add more examples, offer better runtime scalability and come up with a first release. Things are looking good; last item missing on our list is actually the release.</p>

	<p>2017 will be a all about the integration with other Eclipse IoT projects, especially <a target="_blank" href="https://github.com/eclipse/hono">Hono</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/september/images/kaiz.jpg"
        alt="Kai Zimmermmann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Kai Zimmermann <br />
        <a target="_blank" href="https://www.bosch-si.com/home/homepage.php">Bosch SI</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kaizimmerm">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/kai-zimmermann-627946123">LinkedIn</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
