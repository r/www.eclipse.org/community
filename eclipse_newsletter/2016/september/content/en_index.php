<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2016.09.27</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="30%"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContent">
                            <img
                            src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxanne.jpg" />
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="55%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Editor's Note</h3>
                            <p>It's September which means fall is here! This month we're featuring Eclipse Internet of Things (IoT) articles. Start your <a target="_blank" style="color:#000;" href="https://iot.eclipse.org/">Eclipse IoT</a> journey by following along as we demonstrate how Eclipse Edje is headed towards a full IoT Stack for microcontrollers. Next, get started with Eclipse hawkBit and make the best of software update management services. Finish by reading up on the upcoming Eclipse SmartHome features and on the next version of MQTT, that will soon be released.</p>
                            <p>Join the Virtual IoT Meetup tomorrow at 11 AM ET to learn more about <a target="_blank" style="color:#000;" href="http://www.meetup.com/Virtual-IoT/events/233426297/">Eclipse Edje: A Java API for Microcontrollers</a>.
                            <p>There is still time to <a target="_blank" style="color:#000;" href="https://devoxx.us/">propose a talk</a> for the first ever Devoxx US, taking place in San Jose, California. The call for papers deadline is Tuesday, October 11.</p>
                            <p>Don't forget to register for <a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/registration">EclipseCon Europe</a> before October 4 to take advantage of the early discount. Plan to attend the <a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/iot">IoT Day</a> at the same time!</p>
                            <p>Enjoy these articles and the fall weather!</p>
                            <p>Roxanne<br><a target="_blank" style="color:#000;" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article1.php"><h3>Eclipse Edje + Eclipse IoT: Towards a full IoT Stack for Microcontrollers</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/edjesq.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Check out the latest demo created by Pierre Rivron. Hint: it involves five Eclipse IoT projects and a washing machine!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article2.php"><h3>Eclipse hawkBit - Software Updates in IoT</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/hawkbitsq.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Discover Eclipse hawkBit, a project launched in November 2015. Learn how to get started with this awesome project!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article3.php"><h3>Towards The Next Version of MQTT</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/mqtt.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Find out what's in store for the next version of MQTT.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article4.php"><h3>Eclipse SmartHome - Onward!</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2016/september/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/smarthome.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>The Eclipse SmartHome team has a few new tricks up its sleeves. Click to find out what they've been up to.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      
            <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // --> <!--
                        The flexible container has a set width
                        that gets overridden by the media query.
                        Most content tables within can then be
                        given 100% widths.
                     -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                            The content table is the first element
                              that's entirely separate from the structural
                              framework of the email.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse News</h3>
                            <ul>
                              <li><a target="_blank" style="color:#000;"  href="https://mmilinkov.wordpress.com/2016/08/22/progress-update-on-ip-eclipse/">Progress Update on IP @ Eclipse</a></li>
                              <li><a target="_blank" style="color:#000;"  href="https://ianskerrett.wordpress.com/2016/09/01/september-donation-campaign-target-3000/">September Donation Campaign: Target 3000</a></li>
                              <li><a target="_blank" style="color:#000;"  href="https://www.eclipse.org/org/press-release/20160914_ece2016.php">Keynotes, Tracks and Sponsors Announced for the 11th Annual EclipseCon Europe Conference</a></li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/access-control-service-acs-0">Access Control Service (ACS)</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/unide">Unide</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/simopenpass">sim@openPASS</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.locationtech.org/proposals/raster-processing-engine">Raster Processing Engine</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/statet-tooling-r-language">StatET</a></li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.dltk/reviews/5.6-release-review">Dynamic Languages Toolkit 5.6</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.ecp/releases/1.10.0/bugs">EMF Client Platform 1.10</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/PDT/NewIn41">PHP Development Tools 4.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/JGit/New_and_Noteworthy/4.5">JGit 4.5</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/EGit/New_and_Noteworthy/4.5">EGit 4.5</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/SWTBot/Releases#SWTBot_2.5.0">SWTBot 2.5</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egerrit/reviews/1.1.0-neon.1-release-review">EGerrit 1.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.gef/releases/4.1.0-neon.1/plan">Graphical Editing Framework (GEF) 4.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/VIATRA/Releases/NewAndNoteworthy1.4">VIATRA 1.4</a></li>
								<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Linux_Tools_Project/News/NewIn51">Linux Tools 5.1</a></li>
								<li><a target="_blank" style="color:#000;" href="http://rdf4j.org/2016/08/18/rdf4j-2-0-released/">RDF4J 2.0</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.tracecompass/releases/2.1.0/bugs">Eclipse Trace Compass 2.1</a></li>
								<li><a target="_blank" style="color:#000;" href="http://che.eclipse.org/release-notes-eclipse-che-4-7/">Eclipse Che 4.7</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.collections/reviews/8.0.0-release-review">Eclipse Collections 8.0</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.sapphire/releases/9.1/review">Sapphire 9.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/CDT/User/NewIn91">C/C++ Development Tooling (CDT) 9.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.oomph/reviews/1.5.0-release-review">Oomph 1.5</a></li>
								
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
  

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2016/iot"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/iotday400.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://devoxx.us/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/september/images/devoxxus.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p><a target="_blank" style="color:#000;" href="http://www.vogella.com/training/eclipse/eclipsercp_en.html">Eclipse Rich Client Platform (RCP) Training</a><br>
                            Oct 10-14, 2016 | Hamburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://conferences.oreilly.com/oscon/open-source-eu">OSCON London</a><br>
                            Oct 17-20, 2016 | London, UK<br>Save 20% with code: PCECLIPSE on Gold, Silver and Bronze passes.</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/iot">EclipseCon IoT Day 2016</a><br>
                            Oct 25, 2016 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2016/">EclipseCon Europe 2016</a><br>
                            Oct 25-27, 2016 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://www.siriuscon.org/">SiriusCon 2016</a><br>
                            Nov 15, 2016 | Paris, France</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipseconverge.org/na2017/">Eclipse Converge</a><br>
                            Mar 20, 2017 | San Jose, United States</p>
                            <p><a target="_blank" style="color:#000;" href="https://devoxx.us/">Devoxx US</a><br>
                            Mar 21-23, 2017 | San Jose, United States</p>
                            </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

 
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <a target="_blank" href="https://www.eclipsecon.org/europe2016/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/june/images/ece.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a>
                          </td>
                          </tr>
                      
                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Tips and Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://waynebeaton.wordpress.com/2016/03/24/just-drag-and-drop-to-install/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2016/august/images/tipstricks2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Quick Access to Eclipse IDE Features</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>A lot of Eclipse IDE users aren't aware of some very handy features, especially the one-feature-to-rule-them-all: Quick Access. <br><a target="_blank" target="_blank" style="color:#000;"  href="https://waynebeaton.wordpress.com/2016/04/06/quick-access-to-eclipse-ide-features/">Read more</a>.
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

 
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 100%; margin: 0 auto; margin-top: 30px; text-align: center;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
