<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	   <h2>Context</h2>
			<p>The S3P project is a collaborative Research and Development (R&D) project that is building a platform to provide software tools for the design of
			Internet of Things (IoT) systems. The Smart, Safe and Security Software Development and Execution Platform (S3P) has chosen to extend Papyrus to create
			a model-driven development environment for the specification, design, deployment, and monitoring of IoT Systems. This new initiative is called Papyrus for IoT.</p>

			<p>S3P is funded by the French Government program “Nouvelle France Industrielle”.  With a combined budget of 43 million euros, the S3P project consortium includes
			fifteen industrial organizations and seven suppliers of technologies. The CEA List Institute of the CEA Tech, with the support of both PrismTech and MicroEJ,
			has built the first demonstrator of the project using Papyrus.</p>

		<h2>Challenge</h2>

			<p>The Internet of Things (IoT) is a concept and a paradigm that envisions a pervasive presence in the environment of a variety of things (objects) that are able
			to interact with each other and cooperate with other things to create new applications and services and reach common goals. This is achieved through wireless
			and wired connections and unique addressing schemes. For research and development, the primary challenges are:</p>
			<ul>
				<li>Managing heterogeneous applications, development environments, devices and communication technologies.</li>
				<li>Lack of real-time models and design methods describing reliable interworking of heterogeneous systems that is, the interactions among technical, economic, social, and environmental systems.</li>
				<li>Identifying and monitoring critical system elements and detecting critical overall system states in time.</li>
			</ul>


	<h2>Solution: Papyrus for IoT</h2>
			<p>We propose a model-driven development environment as the solution to tackle these challenges. Two core concepts of model-driven engineering, namely abstraction and automation,
			are together, the best solution to the two first challenges. Abstraction facilitates the specification and design of complex IoT systems through the activity of modeling. Automation
			enables managing heterogeneous technologies through automatic model transformations and code generation to specific technological platforms. In addition to these two principles, we
			adopted a common architectural approach for IoT empowerment: the IoT-A architecture reference model which is designed to extrapolate commonalities and define an abstraction layer
			that is common to all IoT-related existing architectures.</p>

			<p>To tackle the third challenge, we propose to use development-time models to supervise a running IoT system. As a consequence, development-time models become models that reflect
			the running system, or in other words, <a target="_blank" href="http://www.springer.com/us/book/9783319089140">Models@Runtime</a>. We now use Models@Runtime to detect critical overall system states and to help make decisions on the adaptation of the running system.</p>

	<h2>Using Papyrus for IoT</h2>
			<p>The IoT model-driven development and supervision environment extends the model-based engineering environment, <a href="https://www.eclipse.org/papyrus/">Papyrus</a>, to specify, design, deploy and monitor an IoT system (Figure 1).
			To illustrate this environment, we use the example of a smart IoT-based home automation system.</p>


	    <p align="center"><a href="/community/eclipse_newsletter/2016/april/images/connected_homes.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2016/april/images/connected_homes_small.jpg"/></a></p>
	    <p align="center"><i>Figure 1. Papyrus based development environment for IoT</i></p>
	    <br/>

	 <h3>1.	Specify and design complex IoT systems using a lightweight IoT-A methodology</h3>
		<p>Papyrus for IoT uses an IoT-A based methodology to guide the IoT system designer during the system development. The methodology is generic and independent from any product. It consists of five steps as shown in Figure 1.</p>

		<p><b>Step 1</b>: Design the purpose and requirements of the system using UML use case diagrams and SysML requirements diagrams.</p>
		<p><b>Step 2</b>: Define the process specification. The use cases of the IoT system are formally described, based on and derived from the purpose and requirements specification.</p>
		<p><b>Step 3</b>: Define the functional architecture of the system based on the IoT domain model.</p>
		<p><b>Step 4</b>: Define the operational platform on which the functional system will be executed.<br>
			The operational view of the system contains information about computing devices (eg. Raspberry PI, Intel Edison, ST Cortex, etc.), sensors, actuators and communication protocols (eg. Wifi, Ethernet, Zigbee, etc).</p>
		<p><b>Step 5</b>: Define deployment plans that include information on the allocation of functional blocks (step 3) to operational ones (step 4).</p>

	  	<a href="/community/eclipse_newsletter/2016/april/images/methodology.png"><img class="img-responsive" src="/community/eclipse_newsletter/2016/april/images/methodology_small.png"/></a>

		<p align="center"><i>Figure 2. IoT-A lightweight methodology steps implemented in Papyrus</i></p>
		<br/>

	<h3>2.	Automatically deploy IoT applications on embedded devices</h3>
		<p>After defining deployment plans, the Papyrus extension for IoT systems will enable automatic code generation to the following solutions.</p>
			<ul>
				<li><a href="http://www.prismtech.com/vortex/vortex-cafe">Prismtech's Vortex</a> which provides a solution based on the Data Distribution Service (DDS) to dynamically deploy microservices into IoT devices, discover new devices and to migrate microservices at runtime.</li>
				<li><a href="http://store.microej.com/">MicroEJ</a>'s operating system: an environment for developing and deploying application software for IoT devices, including an operating system providing the execution environment for microservices</li>
			</ul>


	<h3>3.	Use Models@Runtime to supervise system states</h3>
		<p>Papyrus for IoT offers the ability to use development-time models to supervise running system states. Our solution enables monitoring of micro-services states and starting and stopping them from Papyrus. We are able
		to use Models@Runtime to modify the system’s behavior at runtime in response to changes within the system.</p>

	<h3>Conclusion</h3>
		<p>Papyrus for IoT is a modeling environment that enable to specify, design, and deploy complex IoT systems using an IoT-A lightweight methodology. It also allows you to supervise the
		running system using Models@Runtime technology, which is a great feature!</p>
		<p>Currently, Papyrus for IoT is being developed in the context of S3P R&D project and will be publicly available as an extra Papyrus feature in early 2017.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/april/images/saadia.jpg"
        alt="Saadia Dhouib" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Saadia Dhouib
        <br />
        <a target="_blank" href="http://www-list.cea.fr/en/">CEA LIST</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://fr.linkedin.com/in/saâdia-dhouib-2736b822">LinkedIn</a>
        </li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

