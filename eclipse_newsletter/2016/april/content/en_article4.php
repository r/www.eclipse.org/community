<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/april/images/ericsson.png"/>

    <h2>Context</h2>
        <p>In the telecommunications context, Operation & Maintenance (O&M) refers to the operation and management of telecommunication devices by setting and receiving configuring parameters,
		reading alarms, clearing alarms as well as retrieving performance counters. The models are represented as hierarchies of classes with attributes, relationships, and operations. These
		models can scale up to thousands of elements for complex network devices.</p>

		<p>Basic UML doesn’t provide for the hundreds of metadata properties, sometimes interrelated, required for O&M modeling. These additional metadata properties are modeled using UML
		Profiles and constrained by the Object Constraint Language (OCL), which is applied to all O&M related models. Taken together, these extensions and constraints can be viewed as an O&M Domain Specific Language (DSL).</p>

		<p>The models serve as the input to transformations which generate artifacts to drive the O&M interface. This allows the telecom operator, or a higher level management system, to manipulate
		the system using different protocols. O&M models are created by distributed software engineering teams, often not modeling specialists, collaborating to provide the O&M model for their
		network device. For each device and version, the model is the source code that is tracked through a version control system.</p>

	<h2>Challenge</h2>
		<p><a target="_blank" href="http://www.ericsson.com/">Ericsson</a> has a long tradition of operating and managing telecommunications devices using UML tools. After more than 20 years developing with Model Based Engineering (MBE), it became apparent
		that existing proprietary tools had several disadvantages. It was clear that migrating to an open source tool was the right strategy in order to continue innovation. Our challenge was to migrate existing models, without information loss, from an existing proprietary tool to the open source Papyrus tool.</p>

		<p>The need for rapid development, innovation and customization made migration the obvious choice. Papyrus provides a solid environment for inter-organizational cooperation,
		for customizations for individual firms and for the specific needs of telecommunications and other industries.</p>

	<h2>Solution</h2>
		<p>The Ericsson migration strategy was founded on the goal to create a tool that would perform a lossless transformation of our existing models. Ericsson therefore committed to
		working closely with the Papyrus project to build a new open source import capability, where both model and diagram information would be imported faithfully from the proprietary
		tool in use at the time. A further requirement was to maintain profile-based constraints defined in OCL during the import process. These models would then be fully usable in Papyrus.</p>

		<p>We also needed the advantages of an automated import tool. Importing large models manually is potentially error prone and unsuited for industrial contexts. In addition to reducing
		costly manual errors, the automated tool would make parallel scenarios, running both the old and new tools during a transition easier and more reliable. Having a reliable import
		capability also allowed for a quick assessment of the capabilities of the new tool.</p>

		<p>The import capability was developed in sprints and iteratively tested for the diagram types used in information modeling, such as profile diagrams and class diagrams.
		A range of test and real models were used to train the import capability. These models include both small and extremely large models as well as fragmented ones. We also
		focused on non-functional properties such as usability and performance to ensure that the import capability was as user friendly as possible.</p>

		<p>The model import capability is being used to migrate a considerable number of Ericsson modeling assets and is considered an essential step in migrating users to
		the open source Papyrus tool. This capability is also available to the community as an Eclipse component. It can be installed by going to Help -> Install Papyrus
		Additional Components, and selecting "RSA Model Importer".</p>

	<h2>Benefits</h2>
		<p>Working in an open source environment has helped us to change the way we work with our external suppliers. We now have a peer-to-peer relationship, as opposed
		to the conventional client/vendor relationship. Our experience has shown that this model can be very successful when respect, trust and openness are built among
		the different parties, and when everyone works together towards shared global objectives. Our experiences working in the Papyrus Open Source Modeling project has
		allowed us to assemble a set of best practices that we are now using as part of our processes.</p>

		<p>After five years of involvement in Papyrus development, we believe that Papyrus and open source Eclipse modeling technologies are the primary alternative to
		existing proprietary commercial solutions. This experience allows us to envision the future of MBE with great enthusiasm.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/april/images/Ronan.jpg"
        alt="Ronan Barrett" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Ronan Barrett<br />
        <a target="_blank" href="http://www.ericsson.com/">Ericsson</a>
      </p>
      <ul class="author-link list-inline">
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/april/images/Francis.png"
        alt="Francis Bordeleau" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Francis Bordeleau<br />
        <a target="_blank" href="http://www.ericsson.com/">Ericsson</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/francis-bordeleau-b2aa273">LinkedIn</a>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

