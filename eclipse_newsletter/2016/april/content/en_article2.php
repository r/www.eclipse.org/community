<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

		<h2>The Papyrus Open Source Project</h2>

			<a href="https://eclipse.org/papyrus/"><img align="right" class="img-responsive" src="/community/eclipse_newsletter/2016/april/images/papyrus.jpg"/></a><p>The <a href="https://eclipse.org/papyrus/">Eclipse Papyrus</a> project was created in 2008 by CEA to provide a Domain Specific Language (DSL) platform based on UML. It readily supports UML 2.5 and SysML 1.1. and 1.4,
			and allows end-users, suppliers, and research/academia to customize Papyrus to their specific needs.</p>

			<p>Over the years, several end-user companies have collaborated with CEA and a group of open source suppliers to improve various aspects of Papyrus, with a focus on usability,
			scalability, performance, and support for collaborative modeling. As a result, Papyrus is now developed by a community of experienced users and developers, committers and contributors,
			representing end-user companies, suppliers, and research/academia. And, it has now reached the level of maturity where it can be deployed on an industrial level. Papyrus is currently
			used in many different application domains, including telecommunication, aerospace, defence, automotive, and robotics, to model a range of different development aspects, including
			requirements, systems, software design, network architecture, and information/data. Some examples of industrial projects using Papyrus can be found <a href="https://eclipse.org/papyrus/testimonials.html">here</a>.</p>

		<h2>Why an Industry Consortium?</h2>

			<p>In spite of the progress made both from a technical perspective and a community perspective, key elements were still missing from Papyrus, that prevented it from being
			considered as a first-choice industrial solution. These included:</p>
			<ul>
				<li>Governance and product management to ensure systematic development, long-term viability and long-term evolution.</li>
				<li>Infrastructure to enable development collaboration and co-financing.</li>
				<li>Marketing to promote Papyrus and ensure its broad adoption by industry.</li>
				<li>Establishment of a vibrant community composed of end-users, suppliers, and research/academia to foster innovations and provide the broad spectrum of required capabilities.</li>
			</ul>

			<p>The Papyrus Industry Consortium (IC) was established as the first Polarsys Industry Consortium to address these needs. It is composed of a group of organizations representing end-users,
			suppliers, and research/academia collaborating to develop, maintain, and evolve an industrial-grade open source Model-Based Engineering (MBE) solution. It is targeted for companies developing
			software intensive systems ranging from embedded software development, enterprise software, Internet of Things (IoT) to Cyber-Physical Systems (CPS). The founding members, Adocus, Airbus Helicopters,
			Airbus Defence & Space, Atos, CEA List, Combitech, EclipseSource, Ericsson, Flanders make, Fraunhofer Fokus, OneFact, Saab, and Zeligsoft have already begun to establish a vibrant community around Papyrus.</p>

		<h2>Focus on the business of open source</h2>

			<p>While the role of the Eclipse Papyrus project, like other Eclipse projects, is to focus on the technical aspects, the role of the industry consortium is to focus on the business aspects.
			The stated goals of the Papyrus IC are to:</p>
			<ul>
				<li>Plan and coordinate the development of an industrial-grade Papyrus tool suite to ensure both the availability of key capabilities, and the evolution and long-term availability of a complete MBE solution.</li>
				<li>Capture, consolidate, and manage the requirements of the different consortium members from various application domains, and define development priorities based on these requirements and needs.</li>
				<li>Share MBE knowledge, expertise, and experiences among consortium members to enable broad adoption of the Papyrus tool suite and MBE in general.</li>
				<li>Establish a body of knowledge for MBE methods and processes to help improve development productivity and product quality.</li>
				<li>Promote the use of the Papyrus tool suite as a leading industrial MBE solution.</li>
				<li>Define research priorities to ensure mid-term and long-term evolution of the open source modeling solution.</li>
				<li>Foster collaboration among consortium members to drive strategic research projects.</li>
				<li>Work in collaboration with academic partners and universities to help establish high-quality educational programs in MBE.</li>
				<li>Take a leadership position in standardization initiatives to ensure that the open source solutions are based on open industry standards and leverage
				existing standards developed by leading industry standardization organizations.</li>
			</ul>

		<h2>Membership in the Papyrus IC</h2>

			<p>Any Eclipse Polarsys member can become member of the Papyrus IC. In addition to the annual membership fee, members' commitment include the following:</p>
				<ul>
					<li>Resource allocation: number of days per year that a member organization is committing to different aspects of the consortium activities as required by the
					different committees. The Papyrus IC is composed of four committees, Steering committee, Product Management committee, Architecture committee, and Research/Academia.</li>
					<li>Technical contributions: commitment to make a given level of technical contributions to the development of specific aspects of the Papyrus tool suite.
					Technical contributions can come as a financial contribution or by providing qualified resources to contribute to the development of different aspects.</li>
				</ul>

			<p>All of the details concerning Papyrus IC membership can be found in the <a href="https://www.eclipse.org/org/workinggroups/papyrusic_charter.php">Papyrus IC charter</a>.</p>

		<h2>Recent News</h2>
			<p>Thanks to the commitment and work of members, the Papyrus IC is now officially up-and-running! The Steering Committee, Product Management Committee, and Architecture Committee
			have now been established, and they have each defined their 2016 goals and plan. The Research/Academia committee is under development. During this first year of operation,
			a primary focus will be placed on the packaging of different versions of Papyrus (including Information Modeling, UML, SysML, and UML-RT) and on marketing. We expect that
			commercial Papyrus-based offerings will be made in the next months and that several new members, from different application domains, will join the consortium.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/april/images/Francis.png"
        alt="Francis Bordeleau" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Francis Bordeleau<br />
        <a target="_blank" href="http://www.ericsson.com/">Ericsson</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/francis-bordeleau-b2aa273">LinkedIn</a>
        </li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

