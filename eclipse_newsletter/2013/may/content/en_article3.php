<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

      <p>EMFStore is a repository to store, distribute and collaborate
        on EMF-based entities (a.k.a. data or models). Entities can be
        shared with a server/model repository and distributed to client
        applications. Clients modify the entities in parallel and
        synchronize with the server. Clients can work offline, and
        conflicts in concurrent changes between different clients are
        detected and can be interactively resolved. The server component
        keeps a history of the entities, which can be queried to
        retrieve historical states and changes between different
        versions of the entities.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.1(2).jpg"
        alt="EMFStore" /><br />
      <br />

      <p>All EMFStore features can be used via API to integrate EMFStore
        into an existing application. Furthermore EMFStore integrates
        very well with EMF Client Platform and thereby also supplies
        User Interfaces for viewing and modifying entities.
        Additionally, EMFStore supplies UI for essentially all available
        functions, including a history browser and dialogs, as well as
        other UI contributions to synchronize entities with the server
        component. As a result, all you need to get started with
        EMFStore is your data definition, e.g., an EMF Ecore model.</p>
      <p>The features of EMFStore are described below in more detail. To
        conclude, we will walk through the minimal setup to run an
        EMFStore client and server.</p>

      <h4>Collaboration</h4>
      <p>For many applications, it is important to allow users to
        collaborate on their entities. Multiple users will view and make
        changes to the same entities. This requires synchronization.</p>
      <p>EMFStore provides the following features for collaboration:</p>
      <ul>
        <li>Synchronization of Clients with the Server</li>
        <li>Conflict Detection of Concurrent Changes</li>
        <li>Interactive Merging</li>
        <li>Authentication and Authorization</li>
        <li>Offline Capability</li>
      </ul>
      <p>EMFStore features offline collaboration similar to well-known
        source code versioning systems such as SVN. In contrast to using
        SVN or Git to share your entities that have been serialized to
        files (e.g., XML), you can use EMFStore to share and checkout
        entities as well as commit and update changes to your entities.
        EMFStore understands your entities and their structure and,
        therefore, can support conflict detection and interactive
        merging on a model level as opposed to on a textual level.
        Invalid XML files after merging or dangling links can never
        happen when merging with EMFStore.</p>
      <p>Entities are stored in projects. A project can be shared with
        the server by a client and checked out from the server to other
        clients. Changes on shared or checked-out projects can be
        synchronized with commit and update operations. Commit
        operations will push local changes made by a client to the
        server while update operations will pull changes made by other
        clients from the server to the client. Conflicts can arise
        during synchronization if two clients have changed exactly the
        same entities. These changes can be resolved in a merge UI in
        the client, which ships with the EMFStore client features.</p>
      <p>In addition, EMFStore allows you to assign permissions for
        users to the projects containing your entities. Users are
        authenticated by LDAP and authorized based on permissions.</p>
      <p>Everything from conflict detection over merging to
        authentication and authorization is, of course, extensible and
        customizable.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.2.jpg"
        alt="Commit" /><br />
      <br />

      <h4>Versioning</h4>
      <p>EMFStore ships with a powerful versioning system that provides
        the following features:</p>
      <ul>
        <li>Fine-grained History of Versions</li>
        <li>Differencing/Comparison of Versions</li>
        <li>Branching</li>
        <li>Tagging</li>
      </ul>
      <p>Every commit operation to the EMFStore server results in a new
        version of your entity that is stored. Versioning allows you to
        retrieve any state of your entity that has ever been stored on
        the EMFStore server at any point in time.</p>
      <p>Furthermore, the history of your entity contains a fine-grained
        change log on an attribute change level. EMFStore clients can
        retrieve and display differences if you want to compare
        different states of your entity. The change log also stores
        accounting information about the user, time and log message of a
        version.</p>
      <p>The history of your entity, including branches, is visualized
        by the EMFStore history browser.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.3.jpg"
        alt="History Browser" /><br />
      <br />

      <p>Additionally, EMFStore supports tagging to mark special
        versions and branching to allow concurrent development of your
        entities. With branching, clients can evolve different variants
        of the same entity in parallel and combine them later by
        merging. Changes can be merged both upstream and downstream in
        branches.</p>

      <h4>Extensible Default User Interface</h4>
      <p>To get you and your application started really quickly,
        EMFStore ships with default user interfaces for almost every
        available functionality:</p>
      <ul>
        <li>Navigator and Editor (EMF Client Platform Integration)</li>
        <li>History View</li>
        <li>Synchronization and Merge UI</li>
        <li>Repository Browser (EMF Client Platform Integration)</li>
      </ul>
      <p>With the repository browser (integrated from EMF Client
        Platform), you can configure servers your client can connect to
        and log in to the servers. A navigator view allows you to view
        all your entities in your client’s local workspace and a
        reflective editor allows you to view and modify single entity
        elements — both are featured by EMF Client Platform.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.4.jpg"
        alt="Repository Browser" /><br />
      <br />

      <p>To visualize synchronization with the server, there are default
        dialogs to share and check out entities as well as dialogs for
        committing and updating entities. If a conflict is detected,
        your entities can be interactively merged with a default merge
        dialog.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.5.jpg"
        alt="Merge Conflict" /><br />
      <br />

      <p>Finally, you can view the history of a project or of selected
        entities in a history view. The view also allows you to see
        branches and tags and the changes between versions.</p>
      <p>Of course, all dialogs can be extended, customized and replaced
        as required.</p>

      <h4>Convention over Congifuration</h4>
      <p>EMFStore embraces the convention-over-configuration paradigm.
        From server port to conflict detection, there are default values
        and implementations for every configuration option. This means
        that you have to do zero configuration and implementation to get
        started with EMFStore. You can still configure everything, of
        course, if you decide later that your EMFStore configuration
        should differ from the default.</p>
      <p>As you have seen, the convention-over-configuration paradigm
        even extends to the user interface. No need to provide any kind
        of visualization by yourself. EMFStore features a reflective
        user interface to display and modify your entities and to
        synchronize your UI with the EMFStore server.</p>
      <p>In conclusion, convention-over-configuration for EMFStore means
        that all you need to do to get EMFStore up and running is to
        provide your entity definition, a.k.a. your Ecore model.</p>
      <p>Sounds too good to be true? OK, let’s do it!</p>

      <h4>Running EMFStore</h4>
      <p>
        For evaluation, it is best to start with an Eclipse Modeling
        Edition since it contains all required components of EMF: <a
          href="http://www.eclipse.org/downloads/">http://www.eclipse.org/downloads/</a>
      </p>
      <p>
        To use the user interface to view your entities with the EMF
        Client Platform, please install the EMF Client Platform
        integration hosted on an update site here: <a
          href="http://eclipse.org/emfstore/download.php">http://eclipse.org/emfclient/download.php</a>
      </p>


      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.6.jpg"
        alt="Add Repository" /><br />
      <br />

      <p>Add the update site and select to install the following
        features:</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.7.jpg"
        alt="Features to install" /><br />
      <br />

      <p>
        Installing EMF Client Platform with EMFStore Support will also
        install all core EMFStore features. Of course the EMFStore
        features are also available stand-alone at: <a
          href="http://eclipse.org/emfstore/download.php">http://eclipse.org/emfstore/download.php</a>


      <p>After installation, you need the model plugin and the edit
        plugin for your application to be present in your workspace. To
        get a model, you can chose one of the following possibilities:</p>
      <ol>
        <li>You can use the “Make It Happen” example model, which can be
          retrieved from this repository:
          git://github.com/eclipsesource/makeithappen.git</li>
        <li>You can create the “Make It Happen” example model from
          scratch</li>
        <li>You can create a custom model for your own application; a
          general description on how to do that can be found here:
          http://eclipsesource.com/blogs/tutorials/emf-tutorial/</li>
      </ol>
      <p>No matter which option you chose, there should be at least two
        plugins in your Eclipse workspace: the model and the edit
        plugin.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.8.jpg"
        alt="Package Explorer" /><br />
      <br />

      <p>Now you can start your model with an application by creating a
        run configuration. This run configuration will automatically
        also contain your custom model from your developer workspace. To
        create a run configuration, select the Eclipse menu Run => Run
        Configurations. Create a new run configuration and run the
        application “org.eclipse.emf.ecp.application.e3.application”.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.9(2).jpg"
        alt="Run an application" /><br />
      <br />

      <p>When launching this run configuration it should present you
        with an empty workbench containing your custom model.</p>


      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.10.jpg"
        alt="Model Explorer" /><br />
      <br />

      <p>You can now create a new project in the context menu of the
        empty canvas in the Model Explorer to the left.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.11.jpg"
        alt="New Project" /><br />
      <br />

      <p>You can create entities in the context menu of the newly
        created project.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.12.jpg"
        alt="New Project" /><br />
      <br /> <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.13.jpg"
        alt="New Model Element" /><br />
      <br />

      <p>If you create a task, it will appear in the tree and in a
        reflective editor to the right.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.14.jpg"
        alt="Create a task" /><br />
      <br />

      <p>For a demo, there is a neat feature in the repository browser
        view at the bottom. You can launch a local EMFStore server from
        the context menu of the item “Local EMFStore”. This will launch
        a local EMFStore server from your client that behaves just as a
        remote server would.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.15.jpg"
        alt="Model Repositories" /><br />
      <br />

      <p>Now we can share the local project of the client with the
        server we just launched. In the context menu of the project,
        select Share Project. In the wizard, select Existing Repository
        and then select the “Local EMFStore” then press finish. In the
        login dialog, just press OK since the default demo password
        “super” is already set.</p>

      <p>Once the project is shared you can make changes to the task and
        commit the changes from the context menu of the project.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.17.jpg"
        alt="Commit" /><br />
      <br />

      <p>In the repository browser you can check out another copy of the
        project, simulating a second client.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.18.jpg"
        alt="Checkout" /><br />
      <br />

      <p>The second copy needs to have a different local name.</p>
      <p>Now you change one copy of the project, commit the changes and
        update the other project to pull in the changes.</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.19.jpg"
        alt="Change Copy" /><br />
      <br />

      <p>If both projects are changed in the same element and the same
        attribute, a conflict will arise and can be resolved (Keep My
        Change).</p>

      <img
        src="/community/eclipse_newsletter/2013/may/images/apr_art_3.20(2).jpg"
        alt="Merge Conflicts" /><br />
      <br />

      <p>This concludes a quick walk-through. All functionality in the
        UI is available as API. More detailed tutorials will follow.</p>
      <p>EMFStore will graduate in July 2013 with its 1.0 release. To
        date it is already used by productive products and deployed at
        various customers in various industries.</p>
      <p>
        More information and documentation is available at the <a
          href="http://eclipse.org/emfstore/">EMFStore website</a>.
      </p>


      <script
        src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
        type="text/javascript"></script>
      <script
        src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
        type="text/javascript"></script>
      <script type="text/javascript">
   prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/may/images/mjoegel.jpg"
        alt="Maximilian Koegel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Maximilian Koegel <br />
        <a target="_blank" href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://eclipsesource.com/blogs/author/mkoegel/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/MKoegel">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/102006464246927868861/posts">Google
            +</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>
