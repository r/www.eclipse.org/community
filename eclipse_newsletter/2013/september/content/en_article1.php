<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>
      As Marc Andreessen famously said <a target="_blank"
        href="http://online.wsj.com/article/SB10001424053111903480904576512250915629460.html">Software
        is famously eating the world</a>. It is incorporated into every
      aspect of our lives, used by everyone. Regardless of industry
      software provides an opportunity to organizations of all sizes.
      For the majority of organizations, their ability to compete and
      differentiate is directly tied to the apps that run, sell and
      integrate their business. But software is not a commodity that is
      mined or drilled for, it is instead a commodity invented by
      software developers. Yes, software is a team sport, and any
      finished application or product is touched by many people on its
      way from idea to implementation, but the ultimate creators are
      software engineers. In fact, the majority of modern development
      approaches such as Agile, put software engineers center stage.
      Agile methods empower software engineers to work directly with
      customers, business stakeholders and marketing organizations. <a
        target="_blank"
        href="http://blogs.forrester.com/jeffrey_hammond">Jeffrey
        Hammond</a>, Forrester analyst, describes developers as an,
      “untapped resource for innovation and business opportunity.” But,
      for many managers this idea strikes fear into their hearts.
      Managers think of software development like any key process in
      their business, as one that should be managed and dictated. They
      believe that discipline, control and reporting must be applied to
      the practice of software development. Developer freedom and
      empowerment are easily confused with anarchy and chaos and, for
      most organizations; software delivery projects are already late,
      confusing and, worst of all, under deliver on their promise.
      Historically managers have looked to Application Lifecycle
      Management (ALM) to provide process control, reporting and
      traceability. ALM is described as the application of business
      discipline to the practice of software engineering, the solution
      to application chaos. ALM provides managers with the ability to
      control the application throughout its lifecycle.
    </p>

    <p>For many organizations the perception exists that ALM was driven
      by vendors. These vendors encouraged a single tools strategy,
      standardizing on change management, reporting, version control,
      testing tools and even IDE’s. But as organizations introduced
      standardization initiatives, developers seemed in parallel to be
      adopting new tools. This was not always just to cause trouble, but
      instead in direct response to business need. Standardization is a
      great idea, but it is difficult for any one tool platform to
      innovate at the same frequency as the industry. Disruptions such
      as mobile, cloud and the open web continue to change the fabric of
      development thus making tool selection more difficult. Developers
      who historically would have used one programming language and one
      set of tools now look to multiple development languages, tools and
      techniques. The age of developer populism is upon us – where
      developers are like other craftsman bringing their own tools into
      the workplace. Developer populism, a special form of tech
      populism, describes how practitioners are bringing in technology
      from home and the open source community to better serve them in
      their job. The example often cited are workers bringing their Mac
      into the office even though the company gave them a PC. Developers
      not only bring in different machines, but also the tools for
      building software and the sometimes the software itself.</p>

    <p>And thus is the rub – software innovation is crucial for business
      opportunity and value, but to do it requires empowered, innovative
      software engineers. Management practices encourage process and
      control over empowerment. ALM, the application of management
      discipline to the practice of software engineering is, for many,
      the embodiment of anti-empowerment and choice. But it does not
      need to be that way. Instead of concentrating on one tool to ‘bind
      them all’ ALM vendors’ need to instead focus on enabling the value
      streams of multiple tool adoptions. To do this they need to
      embrace tool variety, while not forgetting the principles and
      practices of ALM. It should not be forgotten that the increased
      use of good business management to software development will help
      everyone. This leads to the creation of a different sort of ALM.
      An ALM that:</p>
    <ul>
      <li><b>Concentrates on federation of development systems of
          record.</b> Instead of forcing each practitioner to use one
        tool. Consider the value stream of software delivery to be
        heterogeneous in nature, with many tools providing their own
        system of record. Each system of record must provide its
        information in a way that allows it to be unifying into one
        definition of the truth about the application and projects.</li>
      <li><b>Adopt open standards for integration. Instead of imposing a
          set of vendor appointed standards.</b> By applying standards
        such as REST and the de-facto standards provided by open source
        tools, the ALM strategy will be better placed for success.</li>
      <li><b>Provides transparency across tools.</b> Instead of focusing
        on one vendor’s data without any regard for the other vendors in
        the development stack. This requires tools to integrate data
        from other tools in their own context. For example bugs may be
        discovered in the testing tools, but then used in backlog report
        for the project management tool and then resolved in the IDE.</li>
      <li><b>Allows for workflow to be delegated to different tools.</b>
        Instead of expecting one tool to manage the whole value chain,
        expect that each tool will come with its own workflow. For
        example, a defect’s state will be managed by each tool that uses
        it, but each tool will be responsible for checking with the
        other tools to ensure that it has not violated the aggregated
        workflow.</li>
      <li><b>Integrate the whole lifecycle. Instead of focusing on just
          development.</b> By not applying a single tool approach it is
        possible to consider the whole value stream, not just the
        development or software development lifecycle (SDLC). Portfolio
        Management and operations can be included. ALM becomes the
        enabler to emerging trends DevOps and Agile Demand management.</li>
    </ul>

    <p>Developer Populism is happening, with or without management’s
      blessing, but that does not mean that you have to lose out on the
      promise of ALM. Traceability, reporting and workflow can still be
      served, but in a different way. By concentrating on integrating
      the development flow organizations can better position ALM as a
      unifying, rather than disruptive, technology that allows software
      developers and their teams to work together in the tools of their
      choosing. By combining ALM and Developer Populism perhaps we are
      on the first step towards making software delivery a truly
      integrated discipline where management and development work
      together to efficiently deliver amazing software.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/september/images/davewest75.jpg"
        alt="Dave West" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Dave West<br />
        <a target="_blank" href="http://www.tasktop.com/">Tasktop</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://tasktop.com/blog/author/dave-westtasktop-com">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/DavidJWest">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/b/107434916948787284891/116762335084488724098/posts">Google +</a></li>
					$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
