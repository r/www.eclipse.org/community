<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    Two years ago I claimed that <a target="_blank"
      href="http://tasktop.com/blog/eclipse/stage-build-review-with-git-gerrit-hudson-and-mylyn">contributing
      to Eclipse is a tedious process</a> and I’m happy to say that this
    no longer holds true. The migration from CVS to Git last year and
    the adoption of Gerrit for code reviews by many Eclipse projects,
    combined with the arrival of contributor license agreements (CLAs),
    has made it incredibly easy to contribute to Eclipse.
  </p>

  <p>Gerrit is great news for contributors since anyone with an
    Eclipse.org account can now propose changes as Git commits. It’s no
    longer necessary to attach patches in Bugzilla; changes can be
    pushed directly to Gerrit. In Gerrit it’s easy to work with the
    changes and, if the CLA has been signed, merging contributed changes
    only takes committers a few clicks.</p>

  <p>
    I have highlighted in previous posts how the <a target="_blank"
      href="http://tasktop.com/blog/eclipse/mylyn-3-8-released-as-part-of-eclipse-juno">Mylyn
      Gerrit Connector integrates code reviews</a> in the IDE. When
    Eclipse.org added CLAs and <a target="_blank"
      href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408640">upgraded
      to Gerrit 2.6</a> (which the Mylyn connector didn’t support at the
    time), I realized how much I rely on the seamless Eclipse
    integration. Thanks to Tomasz Zarna contributed over 20 changes and
    didn’t shy away from using the browser Gerrit 2.6 is now supported
    in the latest Mylyn service release and will soon be available as
    part of Kepler SR1.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle3.1.png"
    alt="change6338" />
  <br />
  <br />

  <p>In addition to supporting the latest Gerrit, Bugzilla and Hudson
    version, Mylyn, and the Gerrit the connector in particular, have
    seen a number of improvements in the last major release. The code
    review editors are now complemented by a Review navigator. The
    navigator view shows all comments in a structured view that
    simplifies keeping track of parallel discussion streams as code
    review progress.</p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle3.2.png"
    alt="new comment" />
  <br />
  <br />

  <p>Global comments are now embedded in the review editor, with support
    for spell checking, and comments can be saved as drafts and edited
    offline. Conveniently, the editor now also supports all patch set
    operations including rebase.</p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle3.3.png"
    alt="patch sets" />
  <br />
  <br />

  <p>
    The Mylyn <a target="_blank"
      href="http://www.eclipse.org/mylyn/new/">New & Noteworthy</a> has
    an overview of all enhancements in the latest release which is <a
      target="_blank" href="http://www.eclipse.org/mylyn/downloads/">available
      here.</a>
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/september/images/steffen75.jpg"
        alt="Steffen Pingel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Steffen Pingel<br />
        <a target="_blank" href="http://www.tasktop.com/">Tasktop</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://tasktop.com/blog/author/steffenpingel">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/steffen_pingel">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
