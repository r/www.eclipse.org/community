<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>


<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>With the arrival of the Kepler release it’s time for a fresh look
    at the how the latest Eclipse tooling makes contributing to Eclipse
    easier and more fun than ever. Read on for a step-by-step guide how
    to setup a workspace for contributing changes to Eclipse using Git,
    Gerrit and Hudson.</p>

  <h2>Registering an Eclipse.org Account</h2>
  <p>Before we can get started we need to create an account on
    Eclipse.org.</p>
  <ol>
    <li><a href="https://dev.eclipse.org/site_login/createaccount.php">Register
        an account on Eclipse.org</a>
    <ul>
      <li>This account is used to access the Eclipse web properties such
        as the Wiki, Bugzilla as well as the code review system Gerrit.
        In order to contribute source code we need to review and sign
        the <a href="http://www.eclipse.org/legal/CLA.php">Eclipse.org
          Contributor License Agreement</a> and setup our account in
        Gerrit.
      </li>
    </ul></li>
    <li>Login to the <a
      href="https://dev.eclipse.org/site_login/?takemeback=https://projects.eclipse.org/user/login/sso">Eclipse
        projects forge</a> and complete the Contributor License
      Agreement
    </li>
    <li>Login to Gerrit and <a
      href="https://git.eclipse.org/r/login/settings/http-password">define
        a username</a></li>
    <li><a href="https://git.eclipse.org/r/login/settings/ssh-keys">Upload
        your public ssh-key</a> to Gerrit for authentication</li>
  </ol>

  <h2>Installing the Tools</h2>
  <p>
    All the tools we need are conveniently available as part of the <a
      href="http://www.eclipse.org/downloads/packages/eclipse-rcp-and-rap-developers/keplerr">Eclipse
      Kepler RCP and RAP Developers package</a>. We can also install the
    tools from their respective p2 repositories into any Eclipse
    instance using Install New Software from the Help menu:
  </p>
  <ul>
    <li><a href="http://eclipse.org/egit/">EGit</a> p2 repository -
      http://download.eclipse.org/egit/updates
    <ul>
      <li>Eclipse Git Team Provider</li>
      <li>Task Focused Interface for Eclipse Git Team Provider</li>
    </ul></li>
    <li><a href="http://eclipse.org/mylyn/">Mylyn</a> p2 repository -
      http://download.eclipse.org/mylyn/latest
    <ul>
      <li>Mylyn Tasks Connector: Bugzilla</li>
      <li>Mylyn Context Connector: Eclipse IDE</li>
      <li>Mylyn Builds Connector: Hudson/Jenkins</li>
      <li>Mylyn Reviews Connector: Gerrit</li>
      <li>Mylyn Version Connector: Git</li>
    </ul></li>
  </ul>

  <p>Some projects release frequently so installing directly from the
    project repositories has the benefit of getting the latest version.
    This tutorial is based on Mylyn 3.9.1 (Kepler SR1) which is
    available from the repository above.</p>

  <h2>Overview</h2>
  <p>Throughout the tutorial we’ll follow the typical steps for
    submitting a contribution to Eclipse.org. We’ll be using the Mylyn
    project as an example but the process is similar for most Eclipse
    projects:</p>
  <ol>
    <li>Querying the Eclipse.org Bugzilla for tasks</li>
    <li>Activating a Bugzilla task</li>
    <li>Committing and pushing a change to Git</li>
    <li>Monitoring the triggered build on Hudson</li>
    <li>Completing the resulting Gerrit code review</li>
  </ol>
  <p>We can complete the entire process from within the Eclipse IDE
    using the Eclipse tools provided by the Eclipse platform, EGit and
    Mylyn.</p>

  <h2>Querying Bugzilla</h2>
  <p>
    As a first step, we will look for a task in Bugzilla to work on. All
    Eclipse project use Bugzilla to track defects and requests for
    enhancements so it’s a good place to start. To make tasks from
    Bugzilla available in Eclipse we open the <i>Task List</i> view
    which is provided by Mylyn and right click to select <i>New > Query</i>
    from the context menu. The Eclipse.org Bugzilla repository is
    already pre-configured so we can select it and advance to the next
    page where we choose <i>Create query using form</i>. On the form
    page we enter a title and select <i>Mylyn Tasks</i> under <i>Product</i>
    and enter <i>helpwanted</i> under <i>Keywords</i>. <br /> <img
      src="/community/eclipse_newsletter/2013/september/images/septarticle4.1.png"
      alt="edit query" /><br />
    <br />


  <p>
    Queries are a personalized view of Bugzilla. They bring tasks we are
    interested in into the IDE and enable us to work with them. The
    query we created adds all tasks for the Mylyn Tasks project to the <i>Task
      List</i> that have been flagged by committers indicating that help
    from the community is desired. For this tutorial, we choose <a
      href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=325673">bug
      325673</a> which is about changing the default summary when
    creating new tasks with Mylyn but we could have picked any open
    task. We double click the task in the task list which opens a rich
    editor where we can review the description, comments, attachments
    and other details.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.2.png"
    alt="edit query" width="600" />
  <br />
  <br />

  <p>
    It’s good practice to state intention on a bug before starting to
    work on it to make others aware of our activity. In order to submit
    tasks we have to enter our account credentials by clicking the <i>Eclipse.org</i>
    link in the header of the Task Editor.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.3.png"
    alt="bugzilla repository settings" />
  <br />
  <br />

  <p>
    We can then refresh the task in the editor by clicking the <i>Synchronize
      Incoming Changes</i> button in the editor toolbar on the top
    right, enter a comment and submit our change.
  </p>

  <h2>Getting Source Code</h2>
  <p>Before we can start working on the task that we selected we need to
    get the source code. The Mylyn Tasks source code is hosted in a Git
    repository and managed by Gerrit.


  <p>
    To connect to the Eclipse.org Gerrit server we go back to the <i>Task
      List</i> and create another query for code reviews. We right click
    and select <i>New > Query</i> again. This time we have to use <i>Add
      Task Repository</i> and select <i>Gerrit Code Review</i> from the
    list. The server drop-down offers <i>Eclipse.org Reviews</i> as a
    template or we can enter <a
      href="https://git.eclipse.org/r/#/q/status:open,n,z">https://git.eclipse.org/r</a>
    as the server location. We also need to uncheck <i>Anonymous</i> and
    enter our Eclipse.org credentials.
  </p>
  <br />
  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.4.png"
    alt="gerrit repository settings" />
  <br />
  <br />

  <p>
    Back in the query dialog we enter a title and leave the default <i>My
      changes</i> selected. The query in the <i>Task List</i> will bring
    in our code reviews. If we haven’t submitted any code reviews, yet,
    it will remain empty initially.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.5.png"
    alt="query parameters" />
  <br />
  <br />

  <p>
    The next step is to import source code from Gerrit. Under <i>File >
      Import</i> we select <i>Projects from Git</i> and select <i>Gerrit</i>
    from the list on the next page. The selection page shows all Git
    repositories hosted on the Eclipse.org Gerrit instance. Since we
    want to contribute a change to Mylyn Tasks we select the <i>mylyn/org.eclipse.mylyn.tasks</i>
    repository.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.6.png"
    alt="source git repository" />
  <br />
  <br />

  <p>
    We can leave the defaults on the next pages and continue through the
    wizard. The repository is automatically cloned and we can select all
    projects to import on the last page. If an error is displayed that
    the SSH key is invalid or missing it needs to be generated first in
    the Eclipse preferences under <i>General > Network Connections >
      SSH2</i> and uploaded to Gerrit as described at the start of the
    tutorial.
  </p>

  <p>
    Once all projects have been imported we can get rid of the API
    baseline errors by opening the Eclipse preferences and selecting the
    <i>Plug-in Development > API Baselines</i> page and setting <i>Missing
      API baseline</i> to <i>Ignore</i>.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.7.png"
    alt="query parameters" />
  <br />
  <br />

  <h2>Working on Tasks</h2>
  <p>Back to our task at hand. We open the task in the task editor and
    use the blue ball button in the top left of the Task Editor to
    activate the task. Activating indicates that we are now working on
    the task.</p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.8.png"
    alt="new task" />
  <br />
  <br />

  <p>
    The features of Mylyn’s task-focused interface are beyond the scope
    of this tutorial. In short, as we edit code and navigate the source
    code Mylyn monitors interactions and builds a degree-of-interest
    model for the active task. We can now focus views such as the <i>Package
      Explorer</i> using the <i>Focus on Active Task</i> button from a
    view’s toolbar. Based on our interactions, focusing filters all
    elements that are not relevant to the task at hand dramatically
    reducing information overload and distraction. We can always go back
    to seeing everything by de-selecting the focus button in the
    toolbar.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.9.png"
    alt="package explorer" />
  <br />
  <br />

  <p>
    Assuming that we have made the necessary code changes for the task
    we are working on and verified our changes locally we are ready to
    commit to the Git repository. Right clicking and selecting <i>Team >
      Commit</i> on a file brings up the commit dialog where we can
    provide a message describing the change. By default this is
    populated with the bug ID, summary and task URL of the active task.
    This automatically enables traceability between the task and change
    in the version control system.
  </p>

  <p>
    The commit dialog has an option to automatically generate a <i>Change-Id</i>
    which is used by Gerrit to track the change. If we want to update
    the commit later Gerrit can relate the changes based on the change
    ID. We can leave the default which is replaced by a generated,
    unique ID as soon we commit.
  </p>

  <p>
    Another important setting in the dialog is the <i>Add Signed-off</i>
    by toolbar button that adds another header to the commit message.
    The <i>Signed-off-by</i> header confirms that we are acting in
    agreement with the <a href="http://www.eclipse.org/legal/CLA.php">terms
      of the Eclipse.org CLA</a> and enables committers to later accept
    our contribution.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.10.png"
    alt="commit changes to git repository" />
  <br />
  <br />

  <p>
    Once we select <i>Commit</i> and <i>Push</i> the change is committed
    to our local clone of the Git repository and then pushed to the
    remote Git repository which in our case is a Gerrit server.
  </p>

  <p>
    When importing repositories from Gerrit the Eclipse Git integration
    creates a configuration that pushes to <i>refs/for/master</i>. This
    is a special location defined by Gerrit that causes every change to
    be staged in a new branch for verification and code reviewed before
    it is merged into the master branch.
  </p>

  <h2>Monitoring Builds</h2>
  <p>The Mylyn Tasks project has configured a job on a Hudson server
    that runs as code reviews are created or updated in Gerrit. Every
    time a new branch is detected a build is automatically triggered to
    verify the change. As soon as the builds completes Hudson comments
    and votes on the code review.</p>

  <p>
    To view Hudson build results in Eclipse we have to add the
    Eclipse.org Hudson server first. Typing <i>Builds</i> into <i>Quick
      Access</i> in the main toolbar opens the Builds view.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.11.png"
    alt="builds" />
  <br />
  <br />

  <p>
    We use the <i>New Builds Server</i> button in the toolbar and select
    Hudson from the list of server types. On the next page we enter the
    URL of the Hudson server <a
      href="https://hudson.eclipse.org/sandbox/">https://hudson.eclipse.org/hudson/</a>
    and <i>Refresh</i>.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.12.png"
    alt="build server properties" />
  <br />
  <br />

  <p>
    The selected build plans are now shown in the <i>Builds</i> view and
    we can monitor the results in the <i>Builds</i> view without
    switching to the browser.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.13.png"
    alt="builds" />
  <br />
  <br />

  <h2>Getting Feedback</h2>

  <p>
    We now go back to the <i>Task List</i> and use the <i>Synchronize
      Changed</i> button in the toolbar to make the code review we just
    created visible in Eclipse. A little popup notification is displayed
    and the review is decorated with a blue arrow indicating that it has
    changes that we haven’t yet looked at. Opening the code review
    displays a rich editor similar to the <i>Task Editor</i> for
    Bugzilla. We can review the changes, called patch sets, and the
    review comments.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.14.png"
    alt="review comments" />
  <br />
  <br />

  <p>When we refresh the code review editor we notice that the review
    has received one vote already. The +1 in the Verified category is
    from Hudson indicating that the all tests passed. Now we need one or
    more people to review and approve the actual code change. Anyone who
    has an Eclipse.org account can participate in code reviews and cast
    their votes but in order to pass the code review a +2 is required.
    On Eclipse.org only committers or the respective project have the
    right to vote +2.</p>

  <p>
    We can explicitly add people in the <i>Code Reviewers</i> section if
    we know their Eclipse.org account ID or we can wait for someone to
    pick up the change. Committers are expected to monitor code reviews
    for their project and should respond within a few days. If nobody
    reacts to the code review it sometimes help to post the URL of the
    code review on the corresponding Bugzilla task explicitly asking for
    a review.
  </p>

  <p>While this workflow is described from the perspective of a
    contributor it’s important to know that committers would follow the
    exact same workflow and post their changes to Gerrit so other
    committers or members from the community can provide feedback before
    a change is merged.</p>

  <h2>Reviewing Code</h2>

  <p>
    The first step to review the code in the IDE is to open the code
    review from the <i>Task List</i>. The content under review is listed
    in the <i>Patch Sets</i> section. Each patch set corresponds to one
    commit on the Gerrit server and is tracked in a separate Git branch.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.15.png"
    alt="patch sets" />
  <br />
  <br />

  <p>
    Double clicking a file from a patch set opens an editor that shows
    the base revision on the left and the proposed changes on the right.
    Comments are added by selecting any line, right clicking and
    selecting <i>Add Comment</i>. Initially, comments are saved as
    drafts and are not visible to others.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.16.png"
    alt="comment" />
  <br />
  <br />

  <p>
    After we have added comments we need to vote on the overall change
    and publish comments to make them visible to everyone. To do that we
    select <i>Publish</i> from the review editor which open a dialog.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.17.png"
    alt="publish comment" />
  <br />
  <br />

  <p>
    The <i>IP Clean</i> flag indicates that all requirements of the
    Eclipse IP process such as a valid CLA and copyright headers in
    changed files have been met. Only committers can set that flag. The
    same applies to the <i>Code Review</i> category. Only project
    committers have permission to vote +2 and submit the review as a
    last step.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/september/images/septarticle4.18.png"
    alt="review merge" />
  <br />
  <br />

  <p>Once a review is submitted by a committer, the most recent patch
    set on the review is merged into the master branch by Gerrit. This
    completes the contribution cycle and the new feature is now part of
    Mylyn.</p>

  <h2>Further Reading</h2>
  <ul>
    <li><a href="http://wiki.eclipse.org/Mylyn/Contributor_Reference">Mylyn
        Contributor Reference</a></li>
  </ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/september/images/steffen75.jpg"
        alt="Steffen Pingel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Steffen Pingel<br />
        <a target="_blank" href="http://www.tasktop.com/">Tasktop</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://tasktop.com/blog/author/steffenpingel">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/steffen_pingel">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
