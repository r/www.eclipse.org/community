<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
 ?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

        <p><a target="_blank" href="http://eclipse.org/kepler/">Eclipse Kepler</a>, the 2013 Eclipse release, is landing on June 26th. This year there are <a target="_blank" href="http://waynebeaton.wordpress.com/2013/06/10/kepler-by-the-numbers/">71 projects, simultaneously shipping 58 million lines of code</a>.</p>
        <p>Eclipse originally shipped in <a target="_blank" href="http://slashdot.org/story/01/11/05/144221/ibm-launches-public-domain-project-eclipse">November 2001</a> as an Open Source tools platform with an emphasis on Java. Since then the platform has grown tremendously. Eclipse is a platform for <a target="_blank" href="http://eclipse.org/birt">report designers</a>, <a target="_blank" href="http://eclipse.org/webtools/">web engineers</a>, <a target="_blank" href="http://eclipse.org/linuxtools/">Linux developers</a>, and more. Eclipse provides <a target="_blank" href="http://eclipse.org/mylyn/">ALM integration solutions</a>, reference implementations for technologies such as <a target="_blank" href="http://eclipse.org/equinox/">OSGi</a> and <a target="_blank" href="http://eclipse.org/eclipselink/">JPA</a> and sets the gold-standard for software development -- shipping software on-time for over 12 years.</p>

        <p>Since 2007, I've been counting down the 10 most New and Noteworthy features of each release. These articles highlight the features that I'm most excited about. Here is my list for 2013.</p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/10/birt-and-nosql-number-10/">10. BIRT &amp; NoSQL</a></h4>

        <p><a target="_blank" href="http://eclipse.org/birt">The Business and Reporting Tools (BIRT)</a> is an open source reporting system that integrates with your Java / JavaEE applications to produce compelling reports. Reports can be generated from a number of data sources and target a variety of runtimes such as web, Swing and SWT/RCP based applications.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/birt.png" width="600" alt="birt"/><br/>

        <p>In Eclipse Kepler, BIRT has added support for NoSQL datasources such as <a target="_blank" href="http://www.mongodb.org/">MongoDB</a> and <a target="_blank" href="http://cassandra.apache.org/">Apache Cassandra</a>. With this new feature you can discover fields in a NoSQL Database, aggregate these fields and produce powerful DataSets. These DataSets can then be used directly in your reports.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/10/birt-and-nosql-number-10/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/11/egit-3-0-top-eclipse-kepler-feature-9/">9. Eclipse Git Integration (EGit) 3.0</a></h4>

        <p>One of the biggest improvements to Eclipse over the past 12 years was the <a target="_blank" href="http://www.slideshare.net/k2moir/migrating-to-git-rethinking-the-commit">migration to Git</a>. To enable this large undertaking, the <a target="_blank" href="http://eclipse.org/egit/">EGit</a> team has stepped up and produced excellent Git. This year the EGit team is shipping version 3.0 with a number of new features, including a 3-way merge tool, rename support, a more powerful reset dialog and the ability to commit-and-push all at once.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/egit.png" width="600" alt="egit"/>

        <p>The underlying <a target="_blank" href="http://eclipse.org/jgit/">Java Git Library (JGit)</a> has also been improved in a number of ways including the ability to pre-compute bitmap indices. This results in <a target="_blank" href="http://www.eclipsecon.org/2013/sessions/scaling-jgit">faster fetch / clone operations than native Git</a>.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/11/egit-3-0-top-eclipse-kepler-feature-9/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/12/mylyn-reviews-with-gerrit-top-eclipse-kepler-feature-8/">8. Mylyn Reviews</a></h4>

        <p><a target="_blank" href="https://code.google.com/p/gerrit/">Gerrit</a> is a code review system designed for use with the Git version control system. It's a highly configurable, web-based, review system. Gerrit is used by Eclipse, LibreOffice, Qt, Android and <a target="_blank" href="https://code.google.com/p/gerrit/wiki/ShowCases">many more</a>. Eclipse Kepler provides tools to integrate with Gerrit, bringing powerful code review tools directly into the IDE.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/gerrit.png" width="600" alt="gerrit"/>

        <p>With the Mylyn Reviews you can easily import change-sets into your IDE and view them in a side-by-side editor. You can comment on the patch and ultimately accept or reject it.</p>

        <p>In addition to Mylyn Reviews, the EGit project also provides tools to directly push your changes to Gerrit for review. This means that you can stay focused on your tasks without leaving your IDE.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/12/mylyn-reviews-with-gerrit-top-eclipse-kepler-feature-8/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/13/linux-tools-2-0-top-eclipse-kepler-feature-7/">7. Linux Tools 2.0</a></h4>

        <p>The <a target="_blank" href="http://www.eclipse.org/linuxtools/">Linux Tools</a> project serves two purposes. First, the project provides frameworks for writing tools relevant to Linux developers; and second, the project provides a place for Linux distributions to collaborate and overcome issues related to packaging of Eclipse technology.</p>

        <p>Linux Tools 2.0 is shipping with Eclipse Kepler and contains a number of improvements. Most notably, the profiling framework has been revised, integrating several popular profiling tools into a single UI.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/linuxtools.png" width="600" alt="linuxtools"/>

        <p>From configuring and launching profilers to analysis of profile data, Linux Tools 2.0 makes this process much easier.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/13/linux-tools-2-0-top-eclipse-kepler-feature-7/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/14/jdt-improvements-top-eclipse-kepler-feature-6/">6. JDT Improvements</a></h4>

        <p>One of the oldest, but most used Plug-ins in Eclipse is the <a target="_blank" href="http://eclipse.org/jdt/">Java Development Tools (JDT)</a>. The JDT was the original 'exemplary' tool that showed just how powerful the Eclipse platform was, and still ships with the majority of the Eclipse packages.</p>

        <p>There are a number of small but powerful improvements to the JDT this year including additional quick-fixes, JUnit improvements, logical debug structures for showing XML DOMs and <i>null-analysis </i>for fields. Null-analysis allows you to annotate fields as 'nullable or non-nullable' and the compiler will warn you about potential Null pointer access.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/jdt.png" width="600" alt="jdt"/>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/14/jdt-improvements-top-eclipse-kepler-feature-6/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/17/m2e-top-eclipse-kepler-feature/">5. Maven and Maven integration with WTP</a></h4>

        <p>The <a target="_blank"  href="http://ianskerrett.wordpress.com/2013/06/12/eclipse-community-survey-results-for-2013/">Eclipse Community Survey</a> indicated that Maven has overtaken Ant as the most used build and release management technology. <a target="_blank" href="http://eclipse.org/m2e/">Maven-to-Eclipse (m2e)</a> reflects this reality by bringing Maven support to Eclipse. This year, a number of new m2e connectors for JavaEE and WTP have be included with Kepler. These connectors, called <a target="_blank" href="http://www.eclipse.org/m2e-wtp/">m2e-wtp</a>, adds Eclipse Maven support for war, ejb, ear and rar projects.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/m2e.png" width="600" alt="m2e"/>

        <p>M2E-WTP even adds to tools to help you migrate from standard Eclipse projects to Maven Projects and it will search for the best Maven plug-in from available Nexus Indexes to help with this migration.<p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/17/m2e-top-eclipse-kepler-feature/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/18/rap-2-x-top-eclipse-kepler-feature-4/">4. RAP 2.x</a></h4>

        <p><a target="_blank" href="http://eclipse.org/rap/">RAP is the Remote Application Platform</a> for Eclipse. RAP is a platform for developing modern UIsÂ  using JavaEE and OSGi, and serve them natively on a variety of different devices through the use of an Open Protocol. An open-source web based client is available as well as several commercial clients for devices such as <a target="_blank" href="http://developer.eclipsesource.com/tabris/">Android and iOS</a>.</p>

        <p>Earlier this year RAP shipped version 2.0, and with Kepler, version 2.1 will land. With the RAP 2.x major release, the client-server protocol was standardized and all communication is now done using a JSON protocol. There is a new API for custom widgets, a new event system, and a new server push API.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/rap.png" width="600" alt="rap"/>

        <p>In addition to the Protocol and API work, the graphics context now supports paths, many widgets support Mnemonics, and RAP now works across multiple browser tabs.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/18/rap-2-x-top-eclipse-kepler-feature-4/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/19/eclipse-platform-improvements-top-eclipse-kepler-feature-3/">3. Platform Improvements</a></h4>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2012/06/27/top-10-eclipse-juno-features/">In 2012, Eclipse released the first major update of the Platform in 8 years</a>. Since then, the platform -- <strong>Eclipse 4</strong> -- has seen a number of improvements. In particular, many of the performance problems that affected Eclipse Juno have been addressed.</p>

        <p>Views and editors can be detached and multiple views can be combined into a single window and separated with sashes, stacks and arbitrary layouts. This is particularly important for those who want to spread their Eclipse windows across multiple displays.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/platform.png" width="600" alt="platform"/>

        <p>Furthermore initial API for Eclipse 4 has been released. Eclipse 4 is the future of the platform, and if you're building applications make sure you checkout many of the Tutorials and other Eclipse 4.x resources available.</p>

        <p>Finally, the Eclipse platform has moved away from a custom build technology to a standard Maven/Tycho build. This is a giant leap forward as it means anybody can get the source code and <a target="_blank" href="http://wiki.eclipse.org/Platform-releng/Platform_Build">build Eclipse</a>.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/19/eclipse-platform-improvements-top-eclipse-kepler-feature-3/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/19/remediation-eclipse-kepler-feature-2/">2. p2 Remediation</a></h4>

        <p>Eclipse has been using <a target="_blank" href="http://eclipse.org/equinox/p2/">p2</a> for installing and managing plug-ins for over 5 years now. The value p2 brought is that it handled dependencies and other integration problems at install time instead of at runtime -- <i>if you can install it, you can run it</i>. Unfortunately this lead to many headaches when trying to install complicated software stacks. The <a target="_blank" href="http://eclipse.org/equinox/p2/">provisioning platform (p2)</a> would often present cryptic error message and users would be left with no way forward.</p>

        <img src="/community/eclipse_newsletter/2013/june/images/p2.png" width="600" alt="p2"/>

        <p>Pascal Rapicault tackled this problem by adding a remediation wizard. The wizard is automatically invoked when a plug-in cannot be installed. The wizard proactively searches for alternate ways to proceed. For example, it may suggest that other plug-ins be upgrade / downgraded to accommodate the tool you are trying to install.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/19/remediation-eclipse-kepler-feature-2/">Read More...</a></p>

      <h4><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/20/orion-3-0-top-eclipse-kepler-feature/">1. Orion 3.0</a></h4>

        <p>In 2001 Eclipse was launched as a generic platform on which software tools could be built. Over the years we have seen an explosion of tools; tools for <a target="_blank" href="http://eclipse.org/cdt/">C/C++ Development</a>, <a target="_blank" href="http://eclipse.org/modeling/">Modeling</a>, <a target="_blank" href="http://projects.eclipse.org/projects/tools.pdt">PHP</a>, <a target="_blank" href="http://eclipse.org/linuxtools/">Linux Development</a>, etc...</p>

        <p>However, when it comes to web development, developers were spread out across a sea of technologies. Notepad for development, Firebug for debugging, Jekyll, Less and other static generation tools, and vi / emacs for system configuration. Eclipse Orion aims to bring web development tools together onto a common platform.</p>

        <p>Orion is a completely new codebase written in JavaScript and targets the browser. There's Git Integration, JavaScript and HTML tool support, internationalization support, an interactive console, and a plug-in model for extensions.</p>


        <img src="/community/eclipse_newsletter/2013/june/images/orion.png" width="600" alt="orion"/>

        <p>Over the past 12 months, Eclipse Orion has graduated and is now shipping version 3.0. With Orion, Eclipse continues to set the standard for software development tools across all platforms. For those interested in trying Orion, navigate to <a target="_blank" href="https://orionhub.org">https://orionhub.org</a>.</p>

        <p><a target="_blank" href="http://eclipsesource.com/blogs/2013/06/20/orion-3-0-top-eclipse-kepler-feature/">Read More...</a></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture" src="/community/eclipse_newsletter/2013/june/images/ian_bull2.PNG" alt ="Ian Bull"/>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ian Bull <br/><a target="_blank" href="http://eclipsesource.com/">EclipseSource</a></p>
          <ul class="author-link">
          <li><a target="_blank" href="http://eclipsesource.com/blogs/author/irbull/">Blog</a></li>
          <li><a target="_blank" href="https://twitter.com/irbull">Twitter</a></li>
          <li><a target="_blank" href="https://plus.google.com/b/107434916948787284891/110293252086584362394/posts">Google +</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>
