<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
 ?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>



      <h4>History</h4>
        <p>Initiatives to address software solution aspects like Workflow, Service Orchestration
           and Document Processing with modeling instead of coding exist for quite a while.
            At the beginning of the millenium, the term Business Process Management (BPM)
            had been established for this approach.</p>
        <p>Technology vendors like IBM, ORACLE or SAP deliver Business Process Management
            Suites (BPMS) as part of their technology platforms IBM Websphere, ORACLE Fusion
            and SAP Netweaver. Basic workflow and orchestration functionality is also provided
            via the Microsoft Workflow Foundation component in the .NET stack. </p>
        <p>The corresponding open source market is still small. jBPM as part of the Red
            Hat/JBoss portfolio exists for quite a while; originally as a very coding-centric
            implementation and only recently with sufficient end user and user interface
            components.One of the founders of jBPM, Tom Bayens had left JBoss due to discrepencies
            on licensing policy and had initiated the project <a target="_blank" href="http://www.activiti.org/">Activiti</a>
            as part of the <a target="_blank" href="http://www.alfresco.com/">Alfresco</a> organization.
            Tom also left Activiti recently and is now focussing on <a target="_blank" href="http://www.column2.com/2013/03/effektif-bpm-in-the-cloud/">Effektif</a>,
            a Cloud offering for BPM. The Berlin-based company <a target="_blank" href="http://www.camunda.com/">Camunda</a>
            is continuing with their own branch of the Activiti code base as <a target="_blank" href="http://www.camunda.com/fox/product/overview/">Camunda
            BPM</a>. Bonitasoft is another open source offering partially using proprietary
            concepts for their delivery.</p>
        <p>All the above indicates the volatility of the open source market for BPM and
            makes it difficult for enterprises to rely on these technologies as part of
            their IT infrastructure. Even being generally open-minded regarding open source
            technology, enterprises will need reliable technology with appropriate Service
            Level Agreements (SLAs).</p>
        <p>With Stardust, a 13-year-old, industry-proven and standards-compliant technology
            is provided under the Eclipse Public License (EPL). Origin of Stardust is the
            CARNOT Process Engine which had been developed by CARNOT AG in Germany between
            2000 and 2006. End of 2006 CARNOT AG was acquired by <a target="_blank" href="http://www.sungard.com">SunGard,
            </a>a leading provider for systems and services in the Financial Services space.
            SunGard has made the CARNOT Process Engine the backbone of their infrastructure
            platform Infinity and rebranded it into Infinity Process Platform. The Infinity
            Process Platform is used in more than 60 SunGard products and in over 1.600
            deployments across the world. The usage profiles goes from document processing
            with hundreds of thousands of documents per day over human-centric workflow
            with thousands of users to low-latency applications with thousands of processes
            per second.</p>
        <p>In 2010, SunGard has contributed the entire Infinity Process Platform code
            base to Eclipse into the Eclipse Process Manager (Stardust) project. With the
            Eclipse Kepler Release, Stardust has matured to Release status. With about 3
            Mio. lines of code, Stardust is one of the biggest code contribution in the
            history of Eclipse ever and a significant part of the 58 Mio. lines of code
            in Kepler. Stardust is part of the <a target="_blank" href="http://www.eclipse.org/soa/">SOA
            Platform top-level project</a>. Collaboration with other subprojects under this
            top-level project has been recently presented at <a target="_blank" href="http://www.slideshare.net/marcgille7/soa-symposium-eclipse-con-2013">EclipseCon
            2013</a>. SunGard is continuing to offer maintenance, support and Cloud/hosting
            services for Stardust.</p>
      <h4>Architecture</h4>
        <p>Stardust supports comprehensive modeling of Business Processes with Activities,
          Data, Sequence Flow, Data Flow, invoked Applications and Workflow Participants
          following the <a target="_blank" href="http://www.omg.org/spec/BPMN/2.0/">Business Process Model
          and Notation (BPMN) 2.0</a> Standard from the <a target="_blank" href="http://www.omg.org">Object
          Management Group (OMG)</a>. Non-interactive Services like Java Classes, Spring
          Beans or REST/Web Services, scripts like Message Transformations or interactive
          UI components e.g. HTML5 pages as well as Data Structures (e.g. Java Classes,
          XSDs) can all be defined and integrated in the Eclipse-based modeling environment
          of Stardust. Standard Eclipse functionality like Refactoring, Reference Search
          or Validation is integrated.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/modeling.gif" width="700" alt="modeling"/><br/>
      <p>Stardust is leaning more and more towards <a target="_blank" href="http://camel.apache.org/">Apache
          Camel </a>to leverage its broad variety of connectors; Camel routs can be used
          directly as Event Sources and Services in Stardust.<br></p>
      <p>Stardust has a powerful Simulation Component which allows to predict runtime
          behavior of processes or workload on systems and human resources. The basis
          for simulation are stochastic properties as incoming process rate or the distribution
          of activity durations. There data can also be derived from existing runtime
          data. The simulation algorithm runs transiently within Eclipse and allows to
          calculate the behavior of the system over years by executing thousands of probability
          experiments per second. The results are displayed graphical and in a tabular
          view in Eclipse. After the simulation run, the evolution of durations, workload
          can be watched &quot;like a video&quot; with pause and position operations.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/simulation.gif" width="700" alt="simulation"/><br/>
      <p>Changes of specific parameters for optimization can be applied. As the resulting
          simulated processes can also be written to an Audit Trail Database and hence
          can be a resource for reporting, &quot;as is&quot; and &quot;what if &quot;comparisons
          can be performed.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/simulation-reporting.gif" width="700" alt="simulation reporting"/><br/>
      <p>Stardust's Runtime Environemnt is a (possibly clustered) EJB or Web Application/Spring
          Container (Application Server) with (relational) Audit Trail Database and Document
          Repository. Hereby, Stardust is integrated with the services of the Application
          Server such as Transaction Management, Custering and Failover and Resource/Connection
          Management to ensure scalable, robust operations. For fast development Stardust
          provides an embedded Runtime Environment in Eclipse based on <a target="_blank" href="http://tomcat.apache.org/">Apache
          Tomcat</a> and <a target="_blank" href="http://db.apache.org/ddlutils/databases/derby.html">Apache
          Derby</a>, which comes ready to use with a Dynamic Web Project Configuration.
          This way Process Models can be deployed directly from Eclipse and runtime functionality
          (Portal, Web/REST Services) can be invoked against the embedded server.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/architecture.gif" width="700" alt="architecture"/><br/>

      <p>The End User Portal is Java Server Faces based but currently transitioned to
          HTML 5. The portal provides different Perspectives for end users, managers and
          administrators. </p>
      <img src="/community/eclipse_newsletter/2013/june/images/portal2.png" width="700" alt="portal"/><br/>
      <p>Non-invasive, REST-based concepts for UI mashups of arbitrary,
          not-necessarily co-located UI components as interactive steps in the process
          flow exist.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/ui-mashup.gif" width="700" alt="ui mashup"/><br/>
      <p>The Stardust Monitoring and Reporting Feature provides a tight
          integration with <a target="_blank" href="http://www.eclipse.org/birt">BIRT</a>; Stardust Model
          and Runtime data are exposed via Open Data Access (ODA) Data Sources and corresponding
          wizards and can be used combined with other Data Sources in BIRT to create and
          retrieve reports.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/reporting.gif" width="700" alt="reporting"/><br/>
      <p>Some elements of the components are essential for mission-critical production
          deployments and only partially or insufficiently available with other open source
          BPMS:</p>
      <ul>
          <li>Scalable, failover architecture with cluster-enabled caching functionality,</li>
          <li>Multi-tenancy with independent logical instances within one physical deployment</li>
          <li>Hot deployment (overwrite and versioning) of process models in 24 x 7 operations</li>
          <li>Parallel management and deployment of different, possibly idenpendent Process
              Models to allow different project and end user teams to work independently
               of each other but still provide holistic access to business process data for
              the management</li>
          <li>Configurability of Runtime Environments for the transition between test,
              User Acceptance Test (UAT) and production environments e.g. to parameterize
              aspects as Web Service/REST endpoints or Mail Server addresses.</li>
      </ul>
    <h4>Getting Access</h4>
      <p>All information regarding Stardust can be obtained from its <a target="_blank" href="http://projects.eclipse.org/projects/soa.stardust">Project
          Page</a> and its <a target="_blank" href="http://www.eclipse.org/stardust">Homepage</a>, e.g.
          a rich <a target="_blank" href="http://wiki.eclipse.org/Stardust">Wiki page</a>, <a target="_blank" href="http://www.eclipse.org/stardust/documentation/training-videos.php">training
          videos</a> and a <a target="_blank" href="http://www.eclipse.org/forums/index.php?t=thread&frm_id=225">discussion
          forum</a>, where a lot of issues and questions have been discussed. All Stardust
          documentation is available <a target="_blank" href="http://help.eclipse.org/kepler/topic/org.eclipse.stardust.docs.dev/html/toc.html?cp=26">online</a>
          with the <a target="_blank" href="http://help.eclipse.org/kepler/index.jsp">Eclipse Online Help</a>.</p>
      <p>Installation and first steps with Stardust as part of Kepler can be obtained
          from the <a target="_blank" href="http://wiki.eclipse.org/Stardust/Project_Plan/Kepler_RC3">Installation
          Guide</a> (for Kepler RC3). The best way to get you hands on Stardust functionality
          after the installation is probably with the <a target="_blank" href="http://help.eclipse.org/kepler/topic/org.eclipse.stardust.docs.dev/html/handbooks/tutorial/support-case/support-case-preface.htm?cp=26_6_0_0">Support
          Case Tutorial</a> which shows you how to create a process model, deploy the
          same into an embedded runtime environment in Eclipse and access the end user
          portals via your browser.</p>
      <p>If you intend to build Stardust locally, you may clone the corresponding <a target="_blank" href="http://git.eclipse.org/c/?q=stardust">Git
          Repositories.</a> Detailed documentation on how to build Stardust locally can
          be found <a target="_blank" href="http://wiki.eclipse.org/Stardust/Source_Code#Build_Tools">here</a>.</p>
      <p>To get in touch with or even become a member of the Stardust team feel free
          and very welcome to contact <a href="mailto:%20stardust-dev@eclipse.org">stardust-dev@eclipse.org</a>.</p>
    <h4>Future</h4>
      <p>Stardust is following the trend of more and more browser-orientation (&quot;development
          on the Web for the Web&quot;). The BPMN 2.0 Browser Modeler as an alternative
          modeling environment to the modeling environment in Eclipse is currently extended
          in short Sprints and follows similar concepts (Extension Points) as Eclipse
          regarding its extensibility. Extensions can be provided in HTML and JavaScript
          agnostic to the server technology in use. Currently components for Rules Managament
          and a Reporting Wizard are added to the Browser Modeler. </p>
      <p>Committers from <a target="_blank" href="http://www.itpearls.com/">itPearls</a> are helping
          with improvements on BPMN 2.0 compatibility.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/browser-modeler.gif" width="700" alt="browser modeler"/><br/>
      <p>To establish a complete, possibly distributed web-based development environment
          the <a target="_blank" href="http://www.slideshare.net/marcgille7/stardust-orion-integration-orion-soa-symposium-eclipse-con-2013">Stardust
          Browser Modeler is integrated with Orion</a>. This way, developers can use Orion
          to create Web UI, business logic, business process models and rules and deploy
          the compound solution into a runtime environment.</p>
      <img src="/community/eclipse_newsletter/2013/june/images/orion.gif" width="700" alt="orion"/><br/>
      <p>&nbsp;</p>
      <p>As well as progressing with the Browser Modeler, we intend to collaborate with
          the <a target="_blank" href="http://projects.eclipse.org/projects/soa.bpmn2-modeler">BPMN2 Modeler
          project </a>to provide a homogenous modeling environment in Eclipse.</p>
      <p>Another subproject in Stardust is creating an end user interface for mobile
          devices with <a target="_blank" href="http://jquerymobile.com/">jQuery Mobile</a> exists in
          <a target="_blank" href="http://git.eclipse.org/c/stardust/org.eclipse.stardust.ui.web.mobile.git/">Git</a>.</p>
      <p>All these activities are open to contributions and contributors and committers are very welcome.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture" src="/community/eclipse_newsletter/2013/june/images/marc-grey.gif" alt ="Marc Gille"/>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Marc Gille <br/><a target="_blank" href="http://www.sungard.com/">SunGard</a></p>
        <ul class="author-link">
          <li><a target="_blank" href="http://twitter.com/marcgille">Twitter</a></li>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

