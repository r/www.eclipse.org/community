<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
 ?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

        <p>Orion is starting to gain traction within the community since the 1.0 graduation release in the Fall of 2012.  At conferences and demo camps, however, there seems to still be a lack of understanding about what Orion is trying to accomplish and technically how it is different from a standard Eclipse IDE.</p>
        <p>In short, Orion is a set of components that together provide the server and client side code that make up an extensible browser based development platform. The components differ from many other Eclipse projects primarily because they’re written in JavaScript, CSS, HTML5 and target modern browsers such as Firefox, Chrome, Safari and IE10.  This is the initial target for Orion, web developers.  The current state of the art for web development tools is not nearly as complete as what the JDT provides for Java developers.  Most use a stand alone editor like Notepad++, Sublime or vi plus command line tools like git to get their day to day development done.  To make Orion compelling that’s the low bar.  None of the existing tools provide a great experience with content-completion or refactoring. Providing JDT like language capabilities for JavaScript and tight integration with Cloud services to deploy applications is the high bar.  So how is Orion put together so that code.anywhere=true; and how is the team progressing towards the high bar?</p>
      <h4>The Orion Server</h4>
        <p>As mentioned, the Orion client is written in JavaScript and runs in the browser. Orion consists of loosely coupled components written in JavaScript, and server-side services exposed via REST-oriented HTTP APIs.  The APIs provide services such as a FileSystem, Search capabilities, Git support, Preferences, and Sites for self hosted development.  Here’s where Orion leverages the existing Eclipse projects for our Java server and exposes the capabilities of JGit, for example, to the client via HTTP requests.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/1-ServerCommunications.png" width="600" alt="server communications"/><br/>
        <p>Since the Orion Client simply relies on these APIs to be available on the server, the team wrote a new server on top of Node.js or more recently a community member wrote one on top of Go.  The client remains the same.  We don’t have Git support (yet) on Node.js so the client differs in what it can support, server to server.</p>

      <h4>The Orion Client</h4>

        <p>The client is really where the major amount of development happens on the project.  The team focuses on writing task and resource focused pages that can easily support exploring and leveraging cross-site workflows.  The pages are designed without building everything you might be able to do in the same browser tab.  This is what sets Orion apart from a traditional IDE experience.  This initial experience with an Orion page might actually seem barren, however knowing the context Orion can get you to the relevant tasks very quickly and you can opt to stay within the existing tab or open a new one.  In all circumstances, Orion just behaves like a web app.  Use the forward and back features of your browser, bookmark the page you’re on, open up new pages in new tabs or don’t. It’s your choice.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/2-OrionEditorPage.png" width="600" alt="orion editor page"/><br/>
        <p>For a concrete example look at this project cloned from GitHub.  Orion can provide destination pages related to this particular file because it’s a Git project.  Go to the Git log for this file, the Repository for this file, open the Orion Shell page in the directory this file is located or start a scoped search beginning in this folder.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/3-RelatedPages.png" width="600" alt="related pages"/><br/>
        <p>These related links and pages are not hard coded.  They are contributions made through the plugin architecture of Orion.</p>

      <h4>The Orion Plugin Architecture</h4>

        <p>Like Eclipse, the Orion development environment is extensible.  Orion provides many pages to deal with editing, navigating, searching, Git tasks, shell access, setting up sites, or dealing with tasks.  These pages contribute to and consume from a JavaScript implementation of OSGi Services API.  The plugins, written in JavaScript, CSS and HTML follow the Lifecycle and Bundle State semantics used in the standard OSGi specification.  This implementation is currently being brought forward at OSGi as a specification for web developers.</p>

        <p>Orion provides a number of extension point service types that plugins can contribute to, in order to customize the client and add functionality. Examples include:</p>
        <ul>
          <li>Adding more commands to the editor toolbar (Format JS)</li>
          <li>Adding more commands to the navigator view (Open with...)</li>
          <li>Adding content assist for new file types (Markdown)</li>
          <li>Adding syntax highlighting rules (Lua)</li>
        </ul>

        <p>Developers write their plugin as an HTML file containing some JavaScript that knows how to connect to the Orion client. Plugins can be hosted on any web server and installed into Orion using their URL.  In other words, Orion plugins are 'separate' HTML pages hosted in non-visible IFrames.  In this way, plugins automatically leverage the security policies in all browsers. Plugins are isolated and secure against direct modification of the hosting page.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/4-OrionPlugins.png" width="600" alt="orion plugins"/><br/>
        <p>Every plugin, then, needs an HTML file that references the Orion plugin.js API to be able to bind to the service framework.  The following are snippets from a plugin that takes the contents of the editor or the selected text and runs it through a common JavaScript library known as JSBeautify.  The file references and loads the Orion API, the JSBeautify code, creates and registers a service provider on the “orion.edit.command” and provides properties which can include a hot key.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/5-PluginSnippets.png" width="600" alt="plugin snippets"/><br/>
        <p>In the screen shot, the plugin manifests itself as the “Format JS” button within the editor toolbar. There are many extension points and the Plugin Developer documentation covers them in detail and provides code samples to help get one started.</p>

      <h4>Can’t I just use it?</h4>

        <p>Yes, the point of creating all these components, servers and plugins is to actually provide a useful set of tools for developers.  Orion’s current feature set is ideally targeted toward languages or sites that do not require compilation or conversion before running.  Orion is one such application, the developers self host Orion in itself for day to day development.  Edit the code, test the site, push changes to git, repeat.</p>

        <p>Our JavaScript tooling capabilities are improving release to release.  There is content assist and JSLint built in.  The Orion editor contains the core functionality developers expect such as find/replace, comment folding, incremental find, file searches, word wrap, variable line height, theming and much more.  In the 3.0 release, the editor also supports a growing list of emacs and vi key bindings, plus auto-save and load.</p>

        <p>It’s trivial to clone a repository from any git source (GitHub, Google, Eclipse) into your Orion workspace and host it on a site to do rapid development.  Developers can choose to work at <a target="_blank" href="http://orionhub.org">http://orionhub.org</a> or download and install a server on a local network. It takes about 2 minutes to have a multi-user server downloaded, and running.</p>
        <img src="/community/eclipse_newsletter/2013/june/images/6-OrionGitPage.png" width="600" alt="plugin snippets"/><br/>
        <p>If you’re doing development for Node.js, Orion also supports a simple ‘npm install orion’, ‘npm start orion’ which gets you an Orion server and client that just needs a node runtime available. This is great for editing files, using npm commands from within the Orion shell, plus launching and debugging Node applications.  Notably, this version of Orion works great on devices such as the Raspberry Pi when you can point the server at your working directory and simply edit the content you’re working on in a browser on your main machine.</p>

      <h4>Consume what you want</h4>

        <p>The Orion team provides many of the components of the platform as individually consumable pieces.  There are a few editors written in JavaScript however Orion’s is under continuous innovation.  The team adapts to the requirements of the community and those using the editor in products. You can embed the editor in a web page, a blog post, or a reveal.js presentation without taking anything more than just the editor.</p>

        <p>There is also a full build of stand alone compare widgets and all the shims that allow the team to implement and utilize the rapidly changing HTML5 specifications and leverage some callback hell optimizations in the Promises A+ implementation.</p>

        <p>Orion has numerous contributors from students, independents, developers at HP, SAP, Pivotal, Mozilla and IBM to name a few.  These contributions are allowing developers to ship products such as Firefox, HP DevAnywhere, JazzHub  Cloud ALM development platform from IBM, a desktop-based code editor called Scripted from Pivotal and even RD Traveler Web and z/OS Cobol solutions on the mainframe.</p>

      <h4>Just keep coding...</h4>

        <p>While interest and participation is growing the team needs to continue to build better JavaScript tooling and begin the work on back-end compilation technologies so that languages such a Java can be supported in the workflows.  Mobile is also an important area so building relationships with other Open Source projects to allow mobile development without requiring local versions of all the tool chains and owning one of every device you need to support. There also needs to be better support for deploying applications to Cloud based services from Orion.  Most of these services have RESTful APIs so there just needs to be additional plugins written to enable this feature and Orion is encouraging some help here.</p>

        <p>Orion 3.0, as part of the Kepler release train, has a lot of great new features so we encourage you to try it out.  There are informational blog posts at <a target="_blank" href="http://planetorion.org/news/">PlanetOrion</a>, lots of technical and getting started guides on the <a target="_blank" href="http://wiki.eclipse.org/Orion">Orion Wiki</a>, and you can follow or post your thoughts to our <a target="_blank" href="https://plus.google.com/u/0/113529759347248251090/posts">CloudCoding Google+</a> or <a target="_blank" href="https://twitter.com/orionhub">@orionhub</a> Twitter accounts.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture" src="/community/eclipse_newsletter/2013/june/images/Ken_Square.png" alt ="Ken Walker"/>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ken Walker <br/><a target="_blank" href="http://www.ibm.com/ca/en/">IBM Canada</a></p>
            <ul class="author-link">
              <li><a target="_blank" href="http://kenwalker.github.io/">Blog</a> </li>
              <li><a target="_blank" href="https://twitter.com/kwalker">Twitter</a></li>
              <li><a target="_blank" href="https://plus.google.com/b/107434916948787284891/106497022660401958933/posts">Google +</a></li>
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>
