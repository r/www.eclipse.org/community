<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>CDT allows for comfortable environment with code highlighting,
    navigation, content assist and static analysis. However, most C/C++
    projects have to be configured to enjoy those features. Most real
    life C/C++ projects heavily depend on code outside of the project
    itself, such as system headers or third party libraries. CDT indexer
    needs to process those to provide accurate index. Failure to set up
    these important settings assuredly will cause the assist tools
    misbehave.</p>
  <p>
    CDT will try to discover include paths and preprocessor symbols
    automatically for supported tool chains. This process is known as <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a> or <b>Autodiscovery</b>. This discovery of symbols
    is twofold. One way, CDT will try to detect built-in compiler
    symbols and include paths running the compiler with special options
    and parse the output. Another method that CDT employs is to analyze
    build output of the regular build with Build Output Parser. Often,
    include paths are supplied to the compiler with -I options, and
    macros with -D options. That relies on verbose build output of your
    build where all these options are actually printed by make.
  </p>
  <p>
    <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a> uses <b>Language Settings Providers</b> to find
    include paths and preprocessor symbols. Language Settings Providers
    can be configured on project properties page "Preprocessor Include
    Paths, Macros, etc.", <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_sd_providers.htm">Providers
      tab</a> for a configuration and on preference page <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_pref_build_scanner_discovery.htm">C/C++
      Preferences: Scanner Discovery</a> for shared providers.
  </p>
  <p>
    If information retrieved by auto-discovery is insufficient or the
    project deviates from a standard one supported by CDT a user can
    inspect discovered entries and enter additional include paths and
    macros manually on the property page "Preprocessor Include Paths,
    Macros, etc.", <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_sd_entries.htm">Entries
      tab</a>.
  </p>
  <p>
    There are other ways to make C/C++ indexer aware of include paths or
    macros. One way is to set up them in MBS via "Paths and Symbols"
    project properties. See <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Ftasks%2Fcdt_t_proj_paths.htm">Including
      paths and symbols in Managed Build System</a>. These entries are
    supplied to the indexer with MBS Language Settings Provider.
  </p>

  <h2>Scanner Discovery</h2>

  <h3>General</h3>
  <p>
    <b>Scanner Discovery</b> (sometimes called <b>Autodiscovery</b>) is
    a method of finding include paths and predefined preprocessor macros
    which the compiler uses during compilation of the code.
  </p>
  <p>Even a simplest C/C++ program just printing "Hello World" needs to
    include system headers. Consider:</p>

  <pre class="prettyprint lang-xtend">
#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;

int main(void) {
  puts("!!!Hello World!!!");
  return EXIT_SUCCESS;
}


  </pre>
  <br />

  <p>
    Indexer needs to be able to find headers <i>stdio.h</i> and <i>stdlib.h</i>
    and parse them to provide accurate index. In this example, the
    headers define prototype of function <i>puts</i> and macro <i>EXIT_SUCCESS</i>.
  </p>
  <p>CDT will try to discover include paths and preprocessor symbols
    automatically for supported tool chains. There are 2 main ways to
    discover those:</p>

  <ol>
    <li><b>Built-in</b> Settings. CDT will try to detect built-in
      compiler symbols and include paths running the compiler with
      special options and parse the output of this special run. Most
      compilers provide such an option to print built-in include paths
      and symbols. Built-in settings are implied and do not get passed
      to compiler during regular compilation.</li>
    <li><b>Build Output Parser (BOP)</b>. Another method that CDT
      employs is to analyze build output of the regular build with Build
      Output Parser. Often, include paths are supplied to the compiler
      with <i>-I</i> options, and macros with <i>-D</i> options and BOP
      will try to find those in the output. That method relies on
      verbose build output of your build where all these options are
      actually printed by make.</li>
  </ol>
  <p>
    CDT uses <b>Language Settings Providers</b> to discover the settings
    (starting with version CDT 8.1). Typically Language settings
    Providers are specialized. One will provide built-in compiler
    settings and another one settings detected by Build output Parser.
    There is also an extension point defined in CDT to allow third party
    Eclipse plugins to contribute custom Language Settings Providers.
  </p>
  <p>The setting entries found by Scanner Discovery can be inspected in
    project properties on "Preprocessor Include Paths, Macros etc."
    page. Each Language Settings Provider can be expanded to show the
    entries it discovered.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.1.png"
    alt="prop entries" width="600" />
  <br />
  <br />

  <p>If information retrieved by auto-discovery is insufficient or the
    project deviates from a standard one supported by CDT a user can
    enter additional include paths and macros manually under "User
    Settings Entries".</p>

  <h3>Project Explorer View</h3>
  <p>
    The <b>Project Explorer View</b> provides a means to view some of
    the settings. The discovered include paths are shown under the
    project when "Includes" node is expanded.
  </p>
  <p>
    Also, a <b>"Wrench" overlay</b> icon is shown for those file/folders
    where distinct entries are discovered which differ from those of
    parent folder. Wrench is not shown on project level.
  </p>
  <p>
    In the example below, <i>Hello.cpp</i> was compiled with added <i>-DHELLO</i>
    compiler option. After BOP discovery "Wrench" overlay is shown for
    this file.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.2.png"
    alt="project explorer" width="600" />
  <br />
  <br />

  <h3>Console View</h3>
  <p>The output of built-in compiler specs detectors can be inspected to
    troubleshoot problems running the command. To do so, enable checkbox
    "Allocate console in the Console View".</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.3.png"
    alt="specs detector options" width="600" />
  <br />
  <br />

  <p>When checkbox "Allocate console in the Console View" is enabled,
    output of each run of the provider will be directed to a special
    console in the Console View.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.4.png"
    alt="sd console view" width="600" />
  <br />
  <br />

  <h2>C/C++ Project properties: Preprocessor Include Paths, Macros, etc.</h2>

  <p>
    Include paths and preprocessor macros for C/C++ indexer and other <b>Language
      Settings</b> are supplied by <b>Language Settings Providers</b>
    using <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a> mechanism.
  </p>
  <p>
    The entries can be inspected and set up on project properties page
    "Preprocessor Include Paths, Macros, etc." - <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_sd_entries.htm">Entries
      tab</a>.
  </p>
  <p>
    Language Settings Providers can be set up and configured for the
    selected configuration on project properties page "Preprocessor
    Include Paths, Macros, etc." - <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_sd_providers.htm">Providers
      tab</a>.
  </p>

  <h2>C/C++ Project properties: Preprocessor Include Paths, Macros, etc.
    - Entries tab</h2>

  <p>
    Include paths and preprocessor macros for C/C++ indexer can be
    inspected and set up on the project property page "Preprocessor
    Include Paths, Macros, etc.", "Entries" tab. This tab presents <b>Language
      Settings Providers</b> employed by <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a> to provide include paths, macros and other <b>Language
      Settings</b>.
  </p>
  <p>Note that the entries could be set also on an individual resource,
    such as file or folder. Open file or folder properties to inspect
    resource-specific entries. The entries on a folder or a project will
    apply to all subfolders and C/C++ files under it - unless overridden
    on a lower level.</p>
  <p>"Entries" tab shows a list of Language Settings Providers for each
    language. A provider node can be expanded to show include paths and
    macros defined by the provider. A user can add custom entries here
    under "User Setting Entries" provider.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.1.png"
    alt="sd console view" width="600" />
  <br />
  <br />

  <h4>Entries tab controls</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Group</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Configuration</b></td>
        <td style="border: 1px solid black;"><a
          href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_all.htm">Click
            here for a description</a></td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Manage Configurations...</b></td>
        <td style="border: 1px solid black;"><a
          href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_all.htm">Click
            here for a description</a></td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Languages</b></td>
        <td style="border: 1px solid black;">Lists all of the languages
          defined by the tool chain. Select a language to see the list
          of applicable providers.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Setting Entries</b></td>
        <td style="border: 1px solid black;">Lists all of the providers
          for the selected language and entries supplied by them. Expand
          a provider node to see the entries.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Show built-in values</b></td>
        <td style="border: 1px solid black;">Disable checkbox "Show
          built-in values" to filter out built-in setting entries in the
          view. Built-in compiler entries need to be provided to C/C++
          indexer but not to the compiler during the build.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Add...</b></td>
        <td style="border: 1px solid black;">Add an entry to the
          provider. Enabled only when the provider supports the
          operation.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Edit...</b></td>
        <td style="border: 1px solid black;">Edit the selected settings
          entry. Enabled only when the provider supports the operation.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Delete</b></td>
        <td style="border: 1px solid black;">Delete the selected
          settings entry. Enabled only when the provider supports the
          operation.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Move Up</b></td>
        <td style="border: 1px solid black;">Moves the selected entry
          higher in the list. Enabled only when the provider supports
          the operation.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Move Down</b></td>
        <td style="border: 1px solid black;">Moves the selected entry
          lower in the list. Enabled only when the provider supports the
          operation.</td>
      </tr>
    </tbody>
  </table>

  <h2>Adding and Editing entries</h2>
  <p>
    Use <b>Add...</b> and <b>Edit...</b> buttons to add or modify the
    entries. Both buttons open similar dialog described here.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.6.png"
    alt="include directory" width="600" />
  <br />
  <br />

  <h4>Add/Edit entries dialog</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Group</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Kind of Setting Entry</b></td>
        <td style="border: 1px solid black;"><img
          src="/community/eclipse_newsletter/2013/october/images/octarticle4.7.png"
          alt="include directory" /><br />A dropbox to select desired
          kind of setting entry. This dropbox is not available in "Edit"
          mode.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Kind of path</b></td>
        <td style="border: 1px solid black;"><img
          src="/community/eclipse_newsletter/2013/october/images/octarticle4.8.png"
          alt="project path" /><br />A dropbox to select desired kind of
          path. This dropbox is shown for path entries only.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Path</b></td>
        <td style="border: 1px solid black;">Enter path value of the
          entry.<br> This field is shown for path entries only.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Open folder button</b></td>
        <td style="border: 1px solid black;">Use folder button to
          navigate folder structure to select existing path. The
          provided dialog varies depending on the kind of path selected
          in the corresponding dropbox.<br />This button is shown for
          path entries only.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Variables</b></td>
        <td style="border: 1px solid black;">Select one of the build or
          environment variables provided by CDT to embed into "Path"
          field.<br />This button is enabled only for File System Paths.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Treat as built-in</b></td>
        <td style="border: 1px solid black;">Enable this checkbox to
          mark the entry as built-in. Built-in compiler entries need to
          be provided to C/C++ indexer but not to the compiler during
          the build.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Contains system headers</b></td>
        <td style="border: 1px solid black;">Enable if the include path
          is a system directory which contains system headers.<br />This
          checkbox is shown for Include Directories only.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Framework folder</b></td>
        <td style="border: 1px solid black;">Mac OS has a unique way of
          specifying include directories for Frameworks. Enable if the
          included directory is Mac Framework directory.<br />This
          checkbox is shown for Include Directories only.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Name</b></td>
        <td style="border: 1px solid black;">Shown for Preprocessor
          Macro only. Specify the macro name here.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Value</b></td>
        <td style="border: 1px solid black;">Shown for Preprocessor
          Macro only. Specify the macro name here.</td>
      </tr>
    </tbody>
  </table>

  <h2>C/C++ Project properties: Preprocessor Include Paths, Macros, etc.
    - Providers tab</h2>

  <p>
    Include paths and preprocessor macros for C/C++ indexer and other <b>Language
      Settings</b> are supplied by <b>Language Settings Providers</b> as
    part of <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a>. Language Settings Providers can be set up and
    configured for the selected configuration in the "Providers" tab.
  </p>
  <p>"Providers" tab is only shown in properties of a project, not in
    properties of a file or a folder.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.9.png"
    alt="preprocessor includes" width="600" />
  <br />
  <br />

  <h4>Providers tab controls</h4>

  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Group</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Configuration</b></td>
        <td style="border: 1px solid black;"><a
          href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_all.htm">Click
            here for a description</a></td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Manage Configurations...</b></td>
        <td style="border: 1px solid black;"><a
          href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_all.htm">Click
            here for a description</a></td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Language</b></td>
        <td style="border: 1px solid black;">Lists all of the providers
          available for the current configuration.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Setting Providers List</b></td>
        <td style="border: 1px solid black;">Select the providers to
          assign to the configuration.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Clear Entries</b></td>
        <td style="border: 1px solid black;">Clears all entries of the
          selected provider. This operation takes effect after pressing
          "Apply" or "OK". Normally this will trigger a rerun if the
          provider supplies built-in compiler settings. Build output
          parsers will regain language settings after build while
          parsing new build output.<br />This operation might be
          unavailable if the provider does not have any language
          settings. Some providers may not have the capability to clear.<br />On
          this property page, this operation is applicable only to
          non-shared providers.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Reset</b></td>
        <td style="border: 1px solid black;">Resets options of the
          selected provider to default values. This operation also
          clears the provider entries. The button can be enabled if some
          options in "Language Settings Provider Options" panel were
          changed.<br />In this dialog, this operation is applicable
          only to non-shared providers.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Move Up</b></td>
        <td style="border: 1px solid black;">Moves the selected provider
          higher in the list.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Move Down</b></td>
        <td style="border: 1px solid black;">Moves the selected provider
          lower in the list.</td>
      </tr>
    </tbody>
  </table>

  <p>
    A user can define some options for configuration providers (or
    "non-shared") on this property page. Note that providers can be
    either shared or defined per configuration. Options of global (or
    "shared") providers can be configured on <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_pref_build_scanner_discovery.htm">C/C++
      Preferences: Scanner Discovery</a>.
  </p>
  <p>There are 2 specific options that are added for non-shared
    providers defined per configuration. They are described in the table
    below.</p>

  <h4>Discovery tab</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Group</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Use global provider
            shared between projects</b></td>
        <td style="border: 1px solid black;">Providers can be global or
          belong to a configuration. Global providers share their
          entries between all configurations which refer to them.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Store entries in project
            settings folder</b></td>
        <td style="border: 1px solid black;">Providers can persist their
          entries in workspace or under project folder. Storing entries
          under project when approprate can make sharing with the other
          team members easier.</td>
      </tr>
    </tbody>
  </table>

  <p>
    The rest of the options is described in <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_pref_build_scanner_discovery.htm">C/C++
      Preferences: Scanner Discovery</a>, under <b>Language Settings
      Providers Options</b>.
  </p>

  <h2>Scanner Discovery Preferences</h2>
  <h3>General</h3>
  <p>
    Use the <b>Discovery Tab</b> on <img
      src="/community/eclipse_newsletter/2013/october/images/octarticle4.12.png" alt="">
    <a
      href="javascript:executeCommand(%22org.eclipse.ui.window.preferences(preferencePageId=org.eclipse.cdt.ui.preferences.BuildSettingProperties)%22)">Build</a>
    preference panel to define global scanner discovery options.
  </p>
  <p>
    In the Discovery tab you can see a list of <b>Language Settings
      Providers</b>. Language Settings Providers supply <b>Language
      Settings</b> such as include paths and preprocessor macros for the
    indexer and other components of CDT.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.10.png"
    alt="preprocessor includes" width="600" />
  <br />
  <br />

  <p>
    A user can define some properties for global (or "shared") providers
    on this preference. Note that providers can be either shared or
    defined per configuration. Properties for non-shared providers can
    be defined in <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_scanner_discovery.htm">Project
      properties: Preprocessor Include Paths, Macros, etc.</a>
  </p>

  <h4>Discovery tab</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Group</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Language Settings
            Providers</b></td>
        <td style="border: 1px solid black;">Lists the available
          language settings providers.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Clear Entries</b></td>
        <td style="border: 1px solid black;">Click to clear all entries
          of the selected provider. This operation takes effect after
          pressing "Apply" or "OK". Normally this will trigger a rerun
          if the provider supplies built-in compiler settings. Build
          output parsers will regain language settings after build while
          parsing new build output. This operation might be unavailable
          if the provider does not have any language settings. Some
          providers may not have the capability to clear.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Reset</b></td>
        <td style="border: 1px solid black;">Click to reset options of
          the selected provider to default values. This operation also
          clears the provider entries. The button can be enabled if some
          options in "Language Settings Provider Options" panel were
          changed.</td>
      </tr>
    </tbody>
  </table>

  <h4>Kinds of Language Settings Providers</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Provider</b></th>
        <th style="border: 1px solid black;"><b>Description</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Build Output Parser
            Provider</b></td>
        <td style="border: 1px solid black;">Build Output Parser
          Provider parses output of build looking for include paths and
          macros passed to the compiler via compiler options. Include
          files, macro files passed the the compiler and some other
          compiler options are normally also recognized.<br />For gcc,
          include paths commonly passed with "-I" option and macros with
          "-D" option. The language settings found by BOP are available
          after the build is finished.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Built-in Compiler
            Settings Provider</b></td>
        <td style="border: 1px solid black;">Built-in Compiler Settings
          Provider launches the compiler with special options that
          instruct the compiler to print built-in settings such as
          include paths to internal system headers and the list of
          internally predefined macros. This provider is usually shared
          between projects in workspace and runs in background once.
          When it is finished the language settings are available to all
          the projects between which it is shared.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Managed Build Language
            Settings Provider</b></td>
        <td style="border: 1px solid black;">MBS Provider supplies
          language settings provided by Managed Build System.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>User Language Settings
            Provider</b></td>
        <td style="border: 1px solid black;">A user has an opportunity
          to enter language settings manually using this provider. User
          Language Settings Provider should be normally the first in the
          list so these settings override settings supplied by other
          providers.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Contributed PathEntry
            Containers</b></td>
        <td style="border: 1px solid black;">This provider is here for
          backward compatibility support of PathEntry Containers from
          older versions of CDT. Using this method is not recommended.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Contributed ScannerInfo
            Entries</b></td>
        <td style="border: 1px solid black;">This provider is here for
          backward compatibility support of ScannerInfo Entries from
          older versions of CDT. Using this method is not recommended.</td>
      </tr>
    </tbody>
  </table>

  <h3>Language Settings Providers Options</h3>
  <p>Language settings providers can have options that affect the
    behavior of a provider. These options are presented in "Language
    Settings Provider Options" panel.</p>

  <h4>Build Output Parser Options</h4>
  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.11.png"
    alt="settings" width="600" />
  <br />
  <br />

  <h4>Build Output Parser Options</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Provider</b></th>
        <th style="border: 1px solid black;"><b>Options</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Compiler command pattern</b></td>
        <td style="border: 1px solid black;">The pattern which is used
          to recognize compiler command in build output.<br />Note that
          the pattern is Java regular expression pattern, it is NOT
          globing pattern used to expand file name in command line.
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Container to keep
            discovered entries</b></td>
        <td style="border: 1px solid black;">The discovered entries can
          be associated with the project, enclosing folder or the file
          itself.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Use heuristics to
            resolve paths</b></td>
        <td style="border: 1px solid black;">The provider will try to
          find the best match for the discovered path in the project or
          workspace trying several heuristics. If disabled, the
          discovered paths will stay as they appear in build output.</td>
      </tr>
    </tbody>
  </table>

  <h4>Built-in Compiler Settings Provider Options</h4>
  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle4.3.png"
    alt="settings" width="600" />
  <br />
  <br />

  <h4>Builtin Compiler Settings Provider Options</h4>
  <table style="border: 1px solid black;">
    <thead>
      <tr>
        <th style="border: 1px solid black;"><b>Provider</b></th>
        <th style="border: 1px solid black;"><b>Options</b></th>
      </tr>
    </thead>
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Command to get compiler
            specs</b></td>
        <td style="border: 1px solid black;">The command to make the
          compiler print built-in settings. Following variables are
          recognized:<br />
          <ul>
            <li>&#36;{COMMAND} - compiler command from the toolchain,
              such as "gcc".</li>
            <li>&#36;{INPUTS} - indicates normally an empty specs file
              required by the compiler. In no such file exists the file
              gets created temporarily in Eclipse workspace plugin area.</li>
            <li>&#36;{EXT} - an extension for the language to discover
              built-in settings. A provider can be associated with
              multiple languages and it will run the command for each
              language.</li>
          </ul>
        </td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Browse</b></td>
        <td style="border: 1px solid black;">Use this button to locate
          the compiler on the file system. The result is put into
          "Command to get compiler specs" input field.</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"><b>Allocate console in the
            Console View</b></td>
        <td style="border: 1px solid black;">Enable to troubleshoot
          problems running the command. When enabled, output of each run
          of the provider will be directed to a special console in the
          Console View. See also <a
          href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
            Discovery</a> - Console View.
        </td>
      </tr>
    </tbody>
  </table>

  <p>
    To see the entries a provider discovered, go to project properties
    (or the properties of the resource of interest), see <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Freference%2Fcdt_u_prop_general_scanner_discovery.htm">Project
      properties: Preprocessor Include Paths, Macros, etc</a>.
  </p>
  <p>A hint: After compiler upgrade re-discover changed settings with
    "Clear Entries" of the appropriate provider.</p>

  <h3>References</h3>
  <p>
    The material provided in this article is also available with Eclipse
    Kepler C/C++ Development User Guide in <a
      href="http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.cdt.doc.user%2Fconcepts%2Fcdt_c_scanner_discovery.htm">Scanner
      Discovery</a> section.
  </p>

  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <p class="author-name">
        Andrew Gvozdev</p>
    </div>
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
