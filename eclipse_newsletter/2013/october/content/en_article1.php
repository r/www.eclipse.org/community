<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>The Eclipse C/C++ Development Tooling, the CDT, has been around for
    a long time and is used by an incredible number of developers, but
    we don’t talk about it that much. That’s why we were excited with
    the opportunity to show off a little of what we’ve been working on
    in this newsletter. To start, here’s 10 thing you probably didn’t
    know about the CDT that you probably should.</p>

  <h2>1. C and C++ isn’t dead yet, far from it</h2>
  <p>If you are a follower of the TIOBE Programming Community Index, you
    would know this already. C and Java have been neck and neck for a
    long time as the two most “popular” languages in our industry. A lot
    of people find it hard to believe that C keeps such a high profile,
    but when you consider that pretty much all operating systems are
    still written in C and that the vast majority of Linux apps are
    still written in C or C++ and embedded devices are almost
    exclusively C and C++, it adds up.</p>

  <p>Even the mobile industry still relies on C and it’s relatives C++
    and Objective-C for apps. Apple’s iOS, of course, uses Objective-C
    as it’s standard language. Android has support for native C/C++
    applications, especially for games. And the new BlackBerry 10 uses
    C++ in combination with the Qt framework for it’s main environment.
    Mobile devices demand good performing apps that are good on power.
    It’s still hard to beat C when trying to get the most out of those
    little CPUs.</p>

  <p>The Internet of Things (IoT) is certainly raising big headlines
    these days, and with good reason. Of course, for us in the embedded
    industry, IoT isn’t really anything that new. A lot of the devices
    we’re talking about as the Things already exist and are programmed
    in C.</p>

  <p>But as the internet grows up, you start to see how you can leverage
    all the data that we’ve been generating on those devices, to analyze
    that data and control those devices from anywhere in the world. It’s
    a very compelling story and I expect some really interesting
    libraries and applications written in C and C++ running on those
    devices. And it will be exciting to see how with Eclipse, we can
    build an IDE that lets you create these systems in multiple
    languages all throughout the stack.</p>

  <h2>2. CDT is 12 years old</h2>
  <p>I often think back to that first CDT summit in the summer of 2001
    where it all started. It was held in a conference room back at QNX’s
    old headquarters in south Kanata, Ontario, Canada. I was there as
    part of the team from Rational and I was yearning to get “down and
    dirty” in IDE business after years of playing in the “ahead of it’s
    time” modeling space. I was pretty sure that a C/C++ IDE for Eclipse
    would be pretty popular and the team from QNX was looking for a
    community to help with the one they had built. It was a perfect
    match.</p>

  <p>12 years later, we’re still going strong. Lots of committers and
    contributors have come and gone, but the mission is still the same:
    build a community driven IDE that helps make the lives of developers
    who work on C/C++ applications better. We have all been proud to
    participate in that mission.</p>

  <h2>3. CDT had 2 million downloads in Juno</h2>
  <p>That’s an incredible number, isn't it? I remember the first time we
    gathered download stats, I was thrilled to see it in the 10s of
    thousands. It was a sign of success. But year after year the number
    grew and grew. Juno SR-1, that SR being by far the most popular of
    the three releases we do a year, almost hit 1 million. Add in the
    half a million each for the June release and SR-2, wow. I’m very
    proud of that number.</p>

  <p>I also follow Stack Overflow and various social media outlets for
    people’s comments and questions about the CDT and I see a lot of
    sentiment that we’re heading in the right direction. A famous game
    engine architect tweeted a few days ago that he was finding that
    Eclipse with CDT is perfectly usable these days. He seems to think
    it’s today’s hardware, however I know that’s because of all the
    performance optimizations we’ve done. There’s no doubting we still
    have usability issues and have trouble handling some of the more
    unstructured projects out there, but we have come a long way.</p>

  <h2>4. CDT has a full C/C++ parser and index database</h2>
  <p>OK, so yeah, this is my baby. After the first CDT summit, our team
    at Rational took on the job of building real C and C++ parsers and
    an index database for the CDT. In our minds, it is very important
    that in order to build good tooling in an IDE, you had to have deep
    knowledge of the user’s code. Source navigation, being able to find
    the definition of a symbol, or to find all references to a function,
    and so on, as well as good context aware content assist are key
    values that a text editor simply isn’t going to give you for C++, at
    least not without a lot of set up and other tools.</p>

  <p>And trust me, while it is valuable, it sure is hard to build. I
    remember senior management laughing a bit at us trying and there
    have been a lot of doubters and a lot of doubts by ourselves that we
    could pull it off, but when you’re editing files, not noticing the
    index running and then have control click (Open Declaration) work in
    the code you just wrote, we got it right.</p>

  <p>It took a lot of hard work and the CDT frankly wasn’t very good in
    this area for the first half of it’s life. But since I rewrote the
    indexer when I joined QNX in 2005 by minimizing the amount of
    parsing it had to do, and maximized the use of heuristics, I’ve been
    pretty happy with it. And as my fellow CDT contributors continue to
    work on improving accuracy while managing the trade off with
    performance, it keeps getting better. And that includes adding in
    new language features as the C++ standard picks up a renewed drive
    towards modernization with C++11 and the upcoming C++14.</p>

  <h2>5. CDT has a static analyzer called Codan</h2>
  <p>The CDT parsers started life to support the source navigation
    features and content assist. But as we realized what we had built, a
    pretty fast and accurate parser, we started to dream bigger.</p>

  <p>Those of you who work on C/C++ projects know it can take quite some
    time for a build to complete. Even incremental builds, which
    minimize compilation, still have to go through a lengthy link phase
    to hook up all the symbol references to create a working executable.
    That’s a long time to wait to see if you have errors in your code,
    and it’s an even longer time to see if you’ve fixed them properly.
    Wouldn’t it be great if we could use the CDT parsers to let you know
    as you type that your code has errors in it. JDT can do this since
    it has a real compiler in it. Are the CDT’s parsers that good.</p>

  <p>Well the answer is yes and no, but it does work in a lot of cases.
    We can pick up on a most syntax errors and have started adding
    checkers to validate the semantics of the code as well. And that
    includes the ever troublesome for C programmers, the accidental
    assignment in a condition, = instead of ==, which is syntactically
    valid, but hardly ever what you want to do and something that almost
    every C programmer has been burned with.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle1.1.png"
    alt="code analysis" />
  <br />
  <br />

  <p>And as we continue our work at improving the accuracy of the
    parsers and kill off the false positives which are inevitable in
    static analysis tools, we think this is a huge reason why users
    would want to use the CDT, and something few other C/C++ IDEs do
    well if at all.</p>

  <h2>6. CDT does refactoring and other cool tricks</h2>
  <p>You thought semantic analysis was hard for C and C++. How about
    refactoring? One of the things that makes refactoring hard for our
    languages is the macro preprocessor that takes your source code and
    converts it into something potentially very different. Given that
    refactoring works at the source level, the preprocessor is it’s
    number one enemy.</p>

  <p>But if you minimize the use of it and keep your code clean, you can
    take advantage of the refactorings we’ve added over the years. Of
    course, you can never have an IDE these days without rename
    refactoring, including in-line rename refactoring just as JDT does.
    But we have others, like extracting constants which gives numbers a
    name, critical for embedded programming. And we have the ability to
    generate code, including generating your traditional getters and
    setters and implementing methods defined in a class.</p>

  <h2>7. CDT has unit test support</h2>
  <p>If you’re a Java programmer, you are very likely familiar with
    JDT’s JUnit support. CDT has something similar in a familiar UI.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle1.2.png"
    alt="run configurations" width="600" />
  <br />
  <br />

  <p>It starts with a launch configuration which supports three popular
    C++ Unit test frameworks: Google Tests, Qt Test, and Boost.Test, and
    it is extensible so we should see others added in the future.
    Generally these launch the application under test and gathers the
    results by watching the programs output. It then formats the results
    and presents it in a Unit Test view.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle1.3.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <h2>8. CDT is very interested in Qt</h2>
  <p>Qt is a C++ framework that is quickly becoming the cross platform
    standard for writing C++ applications for both desktop and mobile
    devices. It has a rich set of APIs and does a lot to simplify C++
    programming, especially with memory management.</p>

  <p>It also offers some added semantics around asynchronous programming
    with the ability to declare methods as slots or signals. You can
    connect a signal emitted from one object to a slot implemented for
    another. You often see this in UI frameworks and network
    programming, but the added syntax that Qt provides, through some
    magic macros and a special preprocessor, makes it simple to work
    with.</p>

  <p>However, that special syntax needs support from your IDE and we’ve
    started adding that to the CDT. We’ll also be adding support for
    Qt’s build system, qmake, which generates makefiles and helps manage
    the special build needed to run it’s preprocessor. Our intention is
    to ensure Eclipse and the CDT is a good alternative IDE for Qt
    developers and allow them to take advantage of the great ecosystem
    Eclipse provides.</p>

  <h2>9. CDT is very interested in LLVM/Clang</h2>
  <p>It’s been one of those long-time dreams of supporting development
    for Apple’s operating systems using the CDT. It’s been hard to get
    off the ground because of the need to support Objective-C. Syntax
    and keyword highlighting is one thing, but writing a parser and
    adding support in the index has been an insurmountable hurdle.</p>

  <p>The LLVM compiler stack and their C/C++/Objective-C compiler Clang
    has opened up possibilities that just weren’t available to us with
    standard gcc environment. First of all, LLVM and friends have a
    BSD-style license. gcc is GPL. For Eclipse projects, that a
    monumental difference. It means we can potentially integrate with
    LLVM technologies without affecting our license.</p>

  <p>And one of those technologies we are keenly interested in is an API
    for the Clang parser. The possibility exists for us to use Clang and
    maybe even it’s backend LLVM as an alternative for CDT’s internal
    parsers and indexer. And that would not only give us support for new
    languages, like Objective-C, but also give us access to Clang’s
    static analysis capability.</p>

  <p>It’s exciting to think of the possibilities, but we all realize
    it’s a huge amount of work and right now, we don’t have the
    community resources to pursue it. But we hope to start with baby
    steps, first learn how to use this new pot of gold, and then see if
    there’s a clean path to bring it in. And we hope that along the way,
    we’ll find others interested in the same idea.</p>

  <h2>10. CDT is at the core of a really interesting Eclipse community</h2>
  <p>One of my earliest EclipseCon memories was a meeting that I got
    invited to to discuss two competing proposals for Fortran support in
    Eclipse. Fortran you say? Once I got to meet and hang out with these
    guys (and get them to work together), I soon learned of another
    community of developers I had forgotten about since leaving
    university, high performance computing. These guys get to play with
    the coolest toys, the big iron, those massively parallel machines
    you hear about in the Top 500 Supercomputers list.</p>

  <p>Well, these guys need an IDE and Eclipse is perfect for them. And
    the cool thing is that they use CDT as a core piece for a lot of
    their tooling. If you ever get a chance, check out the Parallel
    Tools Project, and check out how they’re handling debugging an
    application that spans 100,000 processors. It ain’t easy!</p>

  <p>The other key project that the CDT works with is the Linux Tools
    Project. In a lot of ways, Linux has driven the C/C++ open source
    tools business. Without it, the gcc compiler and the gdb debugger
    would never have been the industry standards they are today.</p>

  <p>Well, there also happens to be a lot of other tools that Linux
    provides. The Linux Tools Project focuses on providing access to
    these tools through Eclipse including Valgrind run-time analysis,
    Gcov code coverage, Gprof system profiler, and LTTng system tracing.</p>

  <p>And, of course, most programs written for Linux are written in C or
    C++ so there is a huge overlap between the communities. So much so,
    that as Linux tools become popular on Windows and Mac, such as we
    saw with the autoconf build tools, we have been able to migrate that
    functionality down into the CDT and made available to the larger
    audience.</p>

  <h2>There’s no end in sight</h2>
  <p>You always wonder how long a project can go on for. After 12+
    years, there is no sign that the CDT is finished. Far from it. As
    long as C and it’s family of high performance, close to the metal
    languages continue to find work in the systems of today, the CDT
    will be there to support the developers who get to play there.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/october/images/dougs75.jpg"
        alt="Doug Schaefer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Doug Schaefer<br />
        <a target="_blank" href="http://www.qnx.com/">BlackBerry/QNX</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://cdtdoug.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/dougschaefer">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/111863048261633151082/posts">Google
            +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
