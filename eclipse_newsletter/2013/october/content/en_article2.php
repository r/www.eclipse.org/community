<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    Multicore hardware is already a reality for industries such as
    telecommunication, automotive, consumer electronics, and aerospace.
    Chips with hundreds, and even thousands of cores are the focus of
    many chip makers (<a target="_blank" href="http://www.tilera.com/">www.tilera.com</a>,
    <a target="_blank" href="http://www.adapteva.com/">www.adapteva.com</a>,
    <a target="_blank" href="http://www.kalray.eu/">www.kalray.eu</a>, <a
      target="_blank" href="http://www.plurality.com/">www.plurality.com</a>,
    etc) and are quickly becoming the most realistic step towards more
    computing performance. With such technology however, software is
    becoming dramatically more complex, and problems exceedingly
    difficult to debug.
  </p>

  <h2>Multicore Visualizer</h2>
  <p>Visualization techniques help developers cope with the complexity
    ensuing from multicore technologies by presenting large numbers of
    items in a graphical summary.</p>

  <p>
    With this in mind, the CDT provides a Multicore Visualizer view
    which displays a graphical representation of the state of the
    application. It allows a user to click- and drag-select groups of
    threads, and apply debugging commands to them directly from the
    Visualizer view, commands such as <i>Resume, Suspend, Step</i>. The
    Multicore Visualizer is designed to scale to large numbers of CPUs
    and cores-per-CPU on both current and future multicore hardware.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.1.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <p>
    In the above screenshots, threads are represented by colored circles
    contained in squares that represent cores. The green and yellow
    colors are used to denote the <i>running</i> and <i>interrupted</i>
    states of execution, respectively, while the color red indicates a
    crash of the application. Execution states, and therefore colors,
    are prioritized in such a way that a crash takes precedence over an
    interrupted state, which in turn takes precedence over a running
    state. This explains why a core is shown red or yellow although some
    of its threads are in the running state.
  </p>

  <p>The Multicore Visualizer is meant to serve as a high-level visual
    overview of the current application, and can be used in tandem with
    the Debug View, which provides more in-depth details. Selections
    made in the Visualizer view are reflected automatically in the Debug
    view, and vice versa.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.2.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <h2>Load Monitoring</h2>

  <p>With multicore hardware, proper usage of computing resources can
    become a challenging problem. Software bugs causing inefficient
    balancing of load, or even starvation of cores, can negate the
    benefits of the hardware. The Multicore Visualizer therefore
    supports system-load monitoring, in the form of graphical
    load-meters. When enabled and space permitting, a load-meter is
    displayed for each core and also one for each CPU. The load-meters
    of the cores display the load of each core of the system being
    debugged, while the CPU load-meters show the average load for all
    cores contained in that CPU.</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.3.png"
    alt="unit test" width="275" />
  <br />
  <br />

  <p>To enable load monitoring, one can use the context-menu of the
    Visualizer view:</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.4.png"
    alt="unit test" width="275" />
  <br />
  <br />

  <p>Using the load-monitoring feature can quickly bring resource
    utilization bugs to the attention of the developer. Enhancements to
    this feature are already being discussed to be able to handle chips
    with thousands of cores, where a graphical load-meter for each core
    simply won’t fit in the very busy display.</p>

  <h2>Filtering</h2>

  <p>With potentially thousands of cores being displayed by the
    Multicore Visualizer, users will need to gradually focus on subsets
    of cores and/or threads that are determined to be problematic. For
    example, using the Multicore Visualizer, a developer could notice
    that a particular subset of cores show regular software crashes; in
    that case, the developer will want to focus on that subset, and
    perform specific debugging operations on those cores, or visualize
    more detailed information about the execution of these cores.</p>

  <p>With that in mind, the Multicore Visualizer provides a filtering
    feature. To filter the view, first select the graphical elements you
    want to keep, then, from the context menu, choose "Filter to
    selection". The selected graphical elements will be shown, along
    with any parent objects; for instance a thread will be shown in its
    core and CPU. The filter stays in place until cleared, using the
    context menu "Clear filter".</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.5.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <h2>Hardware Layout and Communication</h2>

  <p>As the number of cores on multicore chips increases, their location
    with respect to each other will have a great impact on performance.
    For example, it is very often the case that two cores adjacent to
    each other can communicate much faster than cores that are separated
    by one or more other cores; in fact, in many models, the larger the
    number of cores separating two cores, the longer the delay in
    communication.</p>

  <p>Software aimed at such chips will need to take this reality into
    consideration. Consequently, proper debugging tools will need to
    help developers pin-point these communication inefficiencies.</p>

  <p>Currently the Multicore Visualizer provides a generic display of
    multicore chips that does not take a chip’s actual layout into
    consideration. However, we are currently working on providing a
    display that will show chip-specific layouts. Such a display could
    then be enhanced to monitor communication paths and provide insights
    into inefficiencies. Furthermore, such hardware-specific displays
    could also include other components such as memory and caches.</p>

  <h2>Installation</h2>
  <p>
    The Multicore Visualizer is an optional feature of the CDT and must
    be installed manually from the CDT p2 repository
    (http://download.eclipse.org/tools/cdt/releases/kepler) or
    downloaded as part of a <a
      href="http://www.eclipse.org/cdt/downloads.php">CDT archived p2
      repository</a>.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.6.png"
    alt="unit test" />
  <br />
  <br />

  <h2>Step-Into-Selection</h2>

  <p>One of the pain points when debugging non-trivial applications is
    the difficulty of stepping into a method (or function) when multiple
    methods are on the same line. Imagine that the execution of the
    program is currently interrupted at the following line:</p>

  <pre class="prettyprint lang-xtend">
result = subtract( multiply( add( a, b ), 3 ), 5 );  // line 20
</pre>
  <br />

  <p>
    If the user wishes to step into the <i>subtract()</i> method,
    multiple <i>StepInto</i> operations are necessary. Let’s look at the
    details of the required effort.
  </p>

  <p>
    First a <i>StepInto</i> is performed to bring the debugger inside
    the <i>add()</i> method (the first method to execute on line 20).
  </p>

  <pre class="prettyprint lang-xtend">
int add(int arg1, int arg2) {
        return arg1 + arg2;
}
</pre>
  <br />

  <p>
    As this is not the method of interest, the user then performs a <i>StepReturn</i>
    to get back to line 20. A second <i>StepInto</i> is then done to
    bring us within the <i>multiply()</i> method (the next method to
    execute on line 20).
  </p>

  <pre class="prettyprint lang-xtend">
int multiply(int arg1, int arg2) {
        return arg1 * arg2;
}
</pre>
  <br />

  <p>
    Again this is not the method of interest, so another <i>StepReturn</i>
    is done. Finally it is only at the third <i>StepInto</i> that we end
    up in the <i>subtract()</i> method.
  </p>

  <pre class="prettyprint lang-xtend">
int subtract(int arg1, int arg2) {
        return arg1 - arg2;
}
</pre>
  <br />

  <p>
    This adds up to 5 stepping operations, although the user knew from
    the start where the final <i>StepInto</i> should have been done.
  </p>

  <p>
    The traditional way to address this situation is to have the user
    set a breakpoint at the start of the <i>subtract()</i> method and
    resume execution. Beyond the obvious tediousness of having to find
    the implementation of the method, set the breakpoint, and then
    remove that breakpoint once the operation is completed, this
    approach does not lend itself well to method overriding. For
    example, if multiple subclasses have overridden the <i>subtract()</i>
    method, the user would have to determine which one will be called
    before properly setting the breakpoint, or would need to set a
    breakpoint at each overridden method implementation. Unpleasant, to
    say the least.
  </p>

  <p>
    To address this issue, the CDT provides a <b><i>StepIntoSelection</i></b>
    operation.
  </p>

  <pre class="prettyprint lang-xtend">
result = subtract( multiply( add( a, b ), 3 ), 5 );  // line 20
</pre>
  <br />

  <p>
    In the Editor simply set your cursor on the <i>subtract()</i> method
    and select <b>Step into Selection</b> from the Editor context menu
    or the main Run menu. Voila! with this single operation, the
    debugger will arrive directly into the <i>subtract()</i> method.
  </p>

  <p>
    For quicker access the shortcut <b>Ctrl+F5</b> can be used. Or, to
    really make things efficient, use hyperlink navigation by simply
    placing the cursor over the method and using <b>Ctrl+Alt+Click</b>
    to immediately step into it (note that by default <b>Ctrl+Click</b>
    will navigate to the source code, but can be overloaded to also
    perform a <i>StepIntoSelection</i> operation).
  </p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.7.png"
    alt="unit test" />
  <br />
  <br />

  <p>
    As shown above, <i>StepIntoSelection</i> can even be used on a
    method that is on a different line than the one under execution.
  </p>

  <p>
    For those who also develop in Java, fear not, the JDT also provides
    this feature (in fact, the CDT <i>StepIntoSelection</i> was inspired
    by the one provided by the JDT).
  </p>

  <h2>Enhanced Expressions or The Power of the Expressions View</h2>

  <p>For years, the Expressions view has been a nice complement to the
    Variables view and Registers view, allowing users to control what
    information is of interest to them. It allows to specify specific
    variables and registers to be displayed, as well as combinations of
    them, by having the user explicitly define these expressions
    manually.</p>

  <p>
    With the release of CDT 8.2, the Expressions view now accepts <i>Enhanced
      Expressions</i>. An <i>Enhanced Expression</i> is a way of easily
    writing an expression pattern that will automatically expand to a
    larger subset of children expressions. Four types of enhanced
    expressions can be used:
  </p>

  <ul>
    <li>Pattern-matched local variables</li>
    <li>Pattern-matched registers</li>
    <li>Pattern-matched array elements</li>
    <li>Expression groups</li>
  </ul>

  <p>In the next sections, we explain how this support brings the
    Expressions view to a new level of usability, and even makes it
    possible for the Expressions view to optionally replace the use of
    the Variables and Registers views.</p>

  <h2>Pattern-matching syntax</h2>

  <p>
    A pattern-matching syntax based on <a target="_blank"
      href="http://en.wikipedia.org/wiki/Glob_%28programming%29">Glob
      pattern-matching</a> is used to specify enhanced expressions. The
    symbols <i>* ? [ ]</i> are used to create patterns with the expected
    meaning:
  </p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">*</td>
        <td style="border: 1px solid black;">Match any number of any
          character</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">?</td>
        <td style="border: 1px solid black;">Match exactly one of any
          character</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">[ ]</td>
        <td style="border: 1px solid black;">Define a class of
          characters. For example:
          <table>
            <tr>
              <td>[abc]</td>
              <td>Match a, b or c</td>
            </tr>
            <tr>
              <td>[^abc] or [!abc]</td>
              <td>Match any character except a, b or c</td>
            </tr>
            <tr>
              <td>[a-zA-Z]</td>
              <td>Match a through z or A through Z inclusive</td>
            </tr>
            <tr>
              <td>[1-9]</td>
              <td>Match digits 1 through 9 inclusive</td>
            </tr>

          </table>

    </tbody>
  </table>

  <p>
    To indicate that pattern-matching should be performed the user <b>must
      prefix enhanced expressions with the equal (=) symbol</b>. This is
    important because the symbols used for pattern matching also have a
    meaning in C/C++. For example:
  </p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">my*var</td>
        <td style="border: 1px solid black;">means variable my
          multiplied with variable var</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=my*var</td>
        <td style="border: 1px solid black;">means all variables
          starting with the letters my and ending with the letters var,
          with any number of characters in between</td>
      </tr>
    </tbody>
  </table>

  <h2>Pattern-matched local variables</h2>
  <p>The first type of enhanced expression allows the user to specify a
    pattern that will automatically be matched to any existing local
    variable. The resulting set of children expressions will be sorted
    alphabetically. For example:</p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=v&#63;r</td>
        <td style="border: 1px solid black;">Will show all local
          variables starting with the letter v and ending with the
          letter r with a single character in between</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=*x</td>
        <td style="border: 1px solid black;">Will show all local
          variables ending with the letter x</td>
      </tr>
    </tbody>
  </table>

  <p>
    Of particular interest is the pattern =* or *, which can be used to
    have the <b>Expressions view behave like the Variables view</b>
    while also providing sorting:
  </p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=*</td>
        <td style="border: 1px solid black;">Will show all local
          variables of the selected stack frame sorted alphabetically</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">*</td>
        <td style="border: 1px solid black;">Same as =* since no
          confusion is possible in this case</td>
      </tr>
    </tbody>
  </table>

  <h2>Pattern-matched registers</h2>

  <p>As for local variables, the user can specify a pattern that will
    automatically be matched to any existing register. The resulting set
    of children will be sorted alphabetically. Notice the $ prefix to
    indicate matching to registers (this was already the syntax for
    simple register expressions in the Expressions view). For example:</p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=&#36;e?x</td>
        <td style="border: 1px solid black;">Will show all registers
          starting with the letter e and ending with the letter x with a
          single character in between</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=&#36;*x</td>
        <td style="border: 1px solid black;">Will show all registers
          ending with the letter x</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=&#36;st[3-5]</td>
        <td style="border: 1px solid black;">Will show registers
          &#36;st3, &#36;st4, &#36;st5</td>
      </tr>
    </tbody>
  </table>

  <p>
    Of particular interest is the pattern =&#36;* or &#36;*, which can
    be used to have the <b>Expressions view behave like the Registers
      view</b> while also providing sorting:</p>


  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=&#36;*</td>
        <td style="border: 1px solid black;">Will show all registers
          sorted alphabetically</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">&#36;*</td>
        <td style="border: 1px solid black;">Same as =&#36;* since no
          confusion is possible in this case</td>
      </tr>
    </tbody>
  </table>

  <h2>Pattern-matched array elements</h2>

  <p>Another way to use enhanced expressions is to pattern-match array
    elements. For example:</p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">&#61;myarray[30-40]</td>
        <td style="border: 1px solid black;">Will show elements of
          myarray from 30 to 40 inclusive</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">&#61;myarray[1-5,20,30-32]</td>
        <td style="border: 1px solid black;">Will show elements of
          myarray from 1 to 5, as well as element 20, and elements 30 to
          32</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">&#61;myarray?[1-5]</td>
        <td style="border: 1px solid black;">Will show elements from 1
          to 5 of any array starting with the text myarray followed by a
          single character</td>
      </tr>
    </tbody>
  </table>

  <p>As can be seen in the last example, pattern-matching can be used on
    both the name of the array as well as its elements.</p>

  <p>The keen observer will notice that the [] characters are not being
    treated as special characters in the examples above. The CDT has
    intelligent logic that determines when an enhanced expression is
    dealing with an array, in which case the [] characters are
    understood to be array index delimiters, instead of pattern-matching
    symbols.</p>

  <h2>Expression groups</h2>

  <p>Expression groups are two or more enhanced expressions separated by
    semi-colons (and optional spaces). For example:</p>

  <table style="border: 1px solid black;">
    <tbody>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">var1; var2</td>
        <td style="border: 1px solid black;">Will have two children:
          variables var1 and var2</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">=&#36;eax;var1;&#36;rsp</td>
        <td style="border: 1px solid black;">Will have three children:
          register &#36;eax, variable var1, and register &#36;rsp</td>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">var1; =&#36;e*</td>
        <td style="border: 1px solid black;">Will have one
          or more children: variable var1 as well as any and all
          registers starting with the letter e</td>
      </tr>
    </tbody>
  </table>

  <p>Note that the order specified in the expression group definition is
    respected by the generated subset. This means that the user has
    control over where variables and registers will appear in the list
    of children of the expression group.</p>

  <p>This feature allows to quickly define multiple expressions that
    interest the user. Because groups are created from these special
    expressions, they can be collapsed when uninteresting and
    re-expanded later, without having to be re-entered by the user.</p>

  <h2>The power of the Expressions view</h2>
  <p>Let’s have a quick look at a few of the many ways the Expressions
    view can be used, when we factor in Enhanced Expressions.</p>

  <p>For users that deal with a large number of registers, it is easy to
    define enhanced expressions to list only registers of interest. For
    example, in a list of over one hundred registers, a user can focus
    on the xmm registers, as well as the stack pointer by defining the
    expression group: &#36;esp;=&#36;xmm*</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.8.png"
    alt="unit test" />
  <br />
  <br />

  <p>For users that rely heavily on the Variables view but long for a
    greater control of the display ordering, enhanced expressions can be
    used to automatically sort variables alphabetically, and even
    provide fine-grain control over ordering. For example, a user can
    choose to show all variables in alphabetical order while keeping
    variable result at the very top, by using: result;=*</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.9.png"
    alt="unit test" />
  <br />
  <br />

  <p>One of the issues in the traditional use of the Expressions view is
    that defined expressions may not always be applicable to the
    currently selected stack frame. The consequence is that some
    expressions cannot be evaluated and errors keep being shown to the
    user. In the example below, the variable result only exists in stack
    frame main() while arg1 only exists in subtract(), therefore, when
    the user selects different stack frames, errors always appear:</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.10.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <p>Using expression groups, we can avoid this problem by simply
    creating a group for each stack frame. In our example those groups
    can simply be: =result and =arg1 (prefixing each variable with the =
    sign triggers pattern-matching, in this case with a very specific
    pattern: the actual variable name).</p>

  <img
    src="/community/eclipse_newsletter/2013/october/images/octarticle2.11.png"
    alt="unit test" width="600" />
  <br />
  <br />

  <p>As shown above, no errors are reported anymore. Furthermore, thanks
    to the fact that the Expressions view remembers the expansion state
    of elements, as selected by the user, our defined groups will
    automatically collapse or expand, based on the selected stack frame;
    not only is this convenient to avoid showing errors, but it
    completely avoids the evaluation of the expression that is not
    applicable to the stack frame. This solution can also easily apply
    when dealing with variables and registers of different processes or
    cores.</p>

  <p>
    To conclude, let’s take the time to mention that with the <i>Cloning</i>
    feature of the debugging views, it is easy to open multiple
    Expressions views and scroll each of them to have each one display
    different expressions of interest, all at the same time (just use
    the clone icon of the view <img
      src="/community/eclipse_newsletter/2013/october/images/octarticle2.12.png"
      alt="unit test" />).
  </p>

  <h2>(Multicore) Debug Workgroup</h2>

  <p>
    Much effort is being put around improving the CDT’s Debugging
    capabilities. Different features are being added and a variety of
    discussions and proposals are being put forth. To help coordinate
    this joint effort the <i>(Multicore) Debug Workgroup</i> was
    started, and currently counts over 10 regularly participating
    companies. Anyone interested in helping in this effort is welcome to
    join the monthly conference-call where we discuss progress, issues,
    features, tasks, etc. More information can be found at: <a
      href="http://wiki.eclipse.org/CDT/MultiCoreDebugWorkingGroup">http://wiki.eclipse.org/CDT/MultiCoreDebugWorkingGroup</a>
  </p>

  <h2>Conclusion</h2>

  <p>The CDT community is active and motivated. We are always looking
    for new contributions and look to the future with excitement. The
    Debug component evolves at a quick pace, and although we would like
    to, we cannot describe here the many other interesting Multicore
    Debugging features already available in the CDT. To find out more,
    why not look into existing features that may spark your interest:</p>
  <ul>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/FAQ#What_is_the_difference_between_Non-stop_and_All-stop_multi-threaded_debugging.3F">Non-Stop
        debugging</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/NewIn80#Pin_.26_Clone">Pinning
        & Cloning of debugging views</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/NewIn80#Showing_cores_in_Debug_view_labels">Displaying
        of cores in the debug view</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/FAQ#How_do_I_use_multi-process_debugging.3F">Multi-process
        debugging</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/FAQ#How_does_the_multi-select_Resume.2FSuspend_operations_behave.3F">Multi-selection
        for Resume/Suspend operations</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/NewIn81#Show_only_suspended_threads_in_Debug_view">Displaying
        only suspended threads</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/NewIn82#OS_Resources_View">OS
        resources view</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/User/NewIn82#Breakpoint_Filtering">Breakpoint
        filtering</a></li>
  </ul>
  <p>or get familiar with some of our upcoming features:</p>
  <ul>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/MultiCoreDebugWorkingGroup/GlobalBreakpoints">Global
        Breakpoints</a> based on a <a target="_blank"
      href="http://sourceware.org/ml/gdb-patches/2011-06/msg00163.html">GDB
        proposal</a></li>
    <li><a target="_blank"
      href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=341731">Showing
        return values of functions</a></li>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/CDT/MultiCoreDebugWorkingGroup/SynchronizedOperations">Synchronized
        operations</a></li>
    <li><a target="_blank"
      href="https://wiki.eclipse.org/CDT/StandaloneDebugger">Standalone
        debugger</a></li>
    <li><a target="_blank"
      href="https://sourceware.org/ml/gdb/2011-11/msg00013.html">Process/Threads/Cores
        sets</a> as proposed for GDB</li>
  </ul>


  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/october/images/marc75.png"
        alt="Marc Khouzam" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Marc Khouzam<br />
        <a target="_blank" href="http://www.ericsson.com/">Ericsson</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="http://tasktop.com/blog/author/dave-westtasktop-com">Blog</a></li>-->
        <li><a target="_blank" href="https://twitter.com/marckhouzam">Twitter</a></li>
        <!--<li><a target="_blank" href="https://plus.google.com/b/107434916948787284891/116762335084488724098/posts">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
