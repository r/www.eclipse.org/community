<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>What shall I say? I heard it was a great conference. The food was
    good, the band was good, the circus was fun. And the attendees were
    great! So many smart people in one place, so many good
    conversations. The poster shows were appreciated. And the Fortiss
    exhibits were really cool. The overall rating for ECE 2013 was way
    better than the grades i had in my high school degree.</p>

  <p>Oh – before I forget: We also had a great technical program. With
    more than 90 talks about Eclipse and OSGi related technology the
    participants liked most of them! Actually – here is the list of the
    10 best talks. And I have added the links to the video recordings
    (where available), just in case you want to check them out again or
    weren't able to attend the conference at all.</p>

  <h2>Top 10 Talks</h2>

  <ol>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/eclipse-smart-home">Eclipse
        Smart Home</a> - <a target="_blank"
      href="http://www.youtube.com/watch?v=0rbv51feAII">VIDEO</a><br />Kai
      Kreuzer (Deutsche Telekom AG)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/using-gerrit-enhance-your-git">Using
        Gerrit to enhance your Git</a> - <a target="_blank"
      href="http://www.youtube.com/watch?v=Wxx8XndqZ7A">VIDEO</a><br />Shawn
      Pearce (Google)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/flying-sharks-and-m2m">Flying
        sharks and m2m</a><br />Klemens Edler (Lunifera GmbH), Florian
      Pirchner (Lunifera GmbH)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/migration-e4-be-aware-pitfalls-shake-fud-how-migrate-your-eclipse-3-legacy-code-eclipse-4">Migration
        to e4 - be aware of the pitfalls / Shake that FUD; How to
        migrate your Eclipse 3 legacy code to Eclipse 4</a> - <a
      target="_blank" href="http://www.youtube.com/watch?v=RmBj3N7aNN4">VIDEO</a><br />Wim
      Jongman (Remain Software / Industrial-TSI), Philip Wenig
      (OpenChrom), Lars Vogel (vogella GmbH)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/lambda-more-greek-letter">Lambda
        - More Than a Greek Letter</a><br />Sebastian Zarnekow (itemis)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/new-and-noteworthy-java8">New
        and Noteworthy in Java8</a><br />Sebastian Zarnekow (itemis)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/emf-dos-and-don%E2%80%99ts">EMF
        Dos and Don’ts</a> - <a target="_blank"
      href="http://www.youtube.com/watch?v=o0z7d_jcXs0">VIDEO</a><br />Maximilian
      Koegel (EclipseSource Munich), Johannes Faltermeier (EclipseSource
      Munich GmbH)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/custom-swt-and-rap-widgets-shining-nebula">Custom
        SWT and RAP Widgets. Shining in the Nebula.</a><br />Wim Jongman
      (Remain Software / Industrial-TSI)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/eclipse-diagram-editors-endangered-species">Eclipse
        Diagram Editors: An Endangered Species</a><br />Jan Koehnlein
      (itemis)</li>
    <li><a target="_blank"
      href="https://www.eclipsecon.org/europe2013/add-some-spice-your-application-using-emf-parsley-your-ui">Add
        some spice to your application! (using EMF Parsley in your UI)</a>
      - <a target="_blank"
      href="http://www.youtube.com/watch?v=-S8mh5p-ChE">VIDEO</a><br />Lorenzo
      Bettini (Dip. Informatica, Univ. Torino), Francesco Guidieri (RCP
      Vision), Vincenzo Caselli (RCP Vision)</li>
  </ol>

  <p>
    Below you will also find the top 10 most viewed EclipseCon Europe
    video on YouTube. Of course there's more coverage on our <a
      target="_blank" href="http://www.youtube.com/user/EclipseFdn">YouTube
      channel</a> via the <a target="_blank"
      href="http://www.youtube.com/playlist?list=PLy7t4z5SYNaQGJrjB5CFs3tSaulL6yiNU">EclipseCon
      Europe 2013 Playlist</a>.
  </p>

  <p>
    Hope to see you at <a target="_blank"
      href="http://eclipsecon.org/na2014/">EclipseCon 2014</a>,
    EclipseCon France 2014 or the next EclipseCon Europe. Meanwhile, you
    might find other interesting events at <a target="_blank"
      href="http://events.eclipse.org/">http://events.eclipse.org</a>!
  </p>


  <h2>Most views on YouTube</h2>

  <table>
    <tr>
      <td><b>#1</b></td>
      <td><b>#2</b></td>
    </tr>
    <tr>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/PV_djEbXhpE"
          allowfullscreen></iframe></td>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/Wxx8XndqZ7A"
          allowfullscreen></iframe></td>
    </tr>
    <tr>
      <td><b>#3</b></td>
      <td><b>#4</b></td>
    </tr>
    <tr>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/CvKGhNYLTtk"
          allowfullscreen></iframe></td>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/rAFXUxwl2E4"
          allowfullscreen></iframe></td>
    </tr>
    <tr>
      <td><b>#5</b></td>
      <td><b>#6</b></td>
    </tr>
    <tr>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/4S_zvgG_MLw"
          allowfullscreen></iframe></td>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/iCnD2eXMqK4"
          allowfullscreen></iframe></td>
    </tr>
    <tr>
      <td><b>#7</b></td>
      <td><b>#8</b></td>
    </tr>
    <tr>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/Zr1prRkTmQY"
          allowfullscreen></iframe></td>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/k8ldvkWhzTM"
          allowfullscreen></iframe></td>
    </tr>
    <tr>
      <td><b>#9</b></td>
      <td><b>#10</b></td>
    </tr>
    <tr>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/rkjt-snmK94"
          allowfullscreen></iframe></td>
      <td><iframe width="310" height="200"
          src="//www.youtube.com/embed/FQw3eNYTgfI"
          allowfullscreen></iframe></td>
    </tr>
  </table>

  <div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/december/images/ralph75.jpg"
        alt="ralph mueller" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ralph M&#252;ller<br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://ralph-mueller.blog.de/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/ralph_mueller">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/b/107434916948787284891/109870559592385719776/posts">Google
            +</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
