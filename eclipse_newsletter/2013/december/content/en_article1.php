<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>As the Executive Director of the Eclipse Foundation I get to review
    the project proposals for each and every new project joining
    Eclipse. As part of that I get to interact with the new developers
    joining our community. It’s one of the favorite parts of my job.</p>

  <p>2013 has seen a lot of new projects join Eclipse. In this article I
    am going to highlight a few of them that I think are particularly
    interesting. Then I will close off by talking about a few of the
    trends that I see emerging within the Eclipse community.</p>

  <p>Obviously I cannot cover every new project at Eclipse, and I am
    sure that I will be accidentally omitting many very cool projects.
    Use this article as a place to start learning more about what’s
    going on at Eclipse, not as a definitive list.</p>


  <h2>Some Cool Projects</h2>

  <p>
    <a href="http://eclipse.org/sirius/">Sirius</a>. The Eclipse
    community has been very active in modeling for a long time. In the
    past couple of years, the emergence of modeling tools for creating
    domain-specific language has been a hot topic. The <a
      href="http://www.eclipse.org/Xtext/">Xtext</a> project has been at
    the forefront of textual DSL interest and adoption. The new <a
      href="http://eclipse.org/proposals/modeling.sirius/">Sirius</a>
    project is a graphical analog to Xtext in that it allows you to
    create visual DSLs, and tools for them.
  </p>

  <p>
    <a href="http://vertx.io/">Vert.x</a>. Eclipse has had an active
    runtime community for years, but with the exception of <a
      href="http://eclipse.org/jetty/">Jetty</a> is has been almost
    entirely focused on Java and OSGi. Vert.x is a concurrent, polyglot
    application server based on the Java vitrual machine. It is one of
    the most watched Java projects on GitHub. In addition to being a
    very cool project in its own right, Vert.x was also the first
    project hosted by the Eclipse Foundation <a
      href="https://github.com/eclipse/vert.x">on GitHub</a>.
  </p>

  <p>
    <a href="http://www.eclipse.org/efxclipse/index.html">e(fx)clipse</a>.
    The e(fx)clipse project provides the tooling and runtime needed to
    use JavaFX from within Eclipse. Fundamentally, it provides a way to
    modernize the UI of Eclipse RCP desktop applications. It provides
    all of the JDT, PDE, CSS, e4, etc. support required to get JavaFX
    working with Eclipse for both development and deployment.
  </p>

  <h2>The Internet of Things</h2>

  <p>So I will admit it, I have a Raspberry Pi addiction. It all started
    with Benjamin Cabé’s greenhouse demo for the Eclipse M2M project
    that I first saw at FOSDEM 2012. After giving the demo numerous
    times at that conference, I decided that I really wanted to be able
    to run the demo myself. So I bought a Raspberry Pi, an Arduino Uno,
    and the full set of Seeedstudio sensors needed for the demo. And
    then with lots of help from Benjamin I was able to re-create his
    demo, to the point where I think I can do it justice. Of course,
    that got me hooked on playing with these wonderful little devices -
    to the point where I now own six Raspberry Pi’s, three Arduinos, and
    a BeagleBone Black. So the first group of cool projects is based on
    my personal interests in playing with these devices.</p>

  <p>
    <a href="http://www.eclipse.org/proposals/technology.kura/">Kura</a>.
    Based on Java and OSGi, Kura provides a container for M2M
    applications running in service gateways, as well as integrated
    development tools supporting the ability to run M2M applications in
    an emulated environment within the Eclipse IDE, deploy them on a
    target gateway, and finally remotely provision the applications to
    Kura-enabled devices on the field.I like Kura because it has the
    feel of a very mature codebase, with a lot of features for
    management, monitoring and provisioning real systems.
  </p>

  <p>
    <a href="http://eclipse.org/proposals/technology.smarthome/">Smart
      Home</a>. Also based on Java and OSGi, this project provides a
    platform to allow the integration of different systems, protocols or
    standards and that provide a uniform way of user interaction and
    higher level services for home automation. Smart Home has a cool
    visual builder for being able to integrate products from different
    vendors and using different home automation protocols into a single
    system. It basically provides you with all the tools you need to
    build your own residential gateway to manage your personal intranet
    of things.
  </p>

  <p>
    <a href="http://www.eclipse.org/proposals/technology.mosquitto/">Mosquitto</a>.
    The Mosquitto project provides a small server implementation of the
    MQTT and MQTT-SN protocols. It is the server side of <a
      href="http://www.eclipse.org/proposals/technology.paho/">Paho</a>’s
    client-side implementations of MQTT. I think Mosquitto is cool
    mostly because I think it’s great to see Eclipse projects
    implementing the protocols which are going to provide the basic
    plumbing for the Internet of Things. Besides, it runs on my
    Raspberry Pi, and turns it into a hub for my devices to talk to one
    another.
  </p>

  <h2>Trends</h2>

  <p>There are a couple of trends that emerge from 2013.</p>

  <p>First, the message that Eclipse is open to all programming
    languages and platforms is getting out there. We had numerous
    projects come to Eclipse in 2013 that were well outside our
    traditional Java, OSGi and tooling comfort zones.</p>

  <p>Second, Eclipse is becoming the open source center of gravity for
    the Internet of Things. We had ten (10 !) new IoT / M2M projects
    join the Eclipse community in 2013. Many of them were core
    technologies like protocols (CoAP, LWM2M joining our MQTT
    implementations), servers and frameworks (Mosquitto, Ponte, Krikkit)
    and device gateway platforms (Kura, OM2M). There is a wealth of
    technology joining our open community, and given how important this
    emerging domain is, it is great to see.</p>

  <p>And finally, the Eclipse community is reinventing itself as a
    leaner, faster open source community. We have been working hard to
    lower the barriers to contribution to all Eclipse projects, and to
    enable Eclipse projects to leverage popular tools like GitHub.
    Implementing Contributor License Agreements, the Common Build
    Infrastructure, Git, Gerritt, Hudson Instance Per Project (HIPP) are
    part of this. It has never been a better time to bring an open
    source project to the Eclipse community.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/december/images/mikem75.jpg"
        alt="mike milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Mike Milinkovich<br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://mmilinkov.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
        <!--<li><a target="_blank" href="">Google +</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
