<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <p>Eclipse users have a great selection of plugins and tools that can
    help developers extend Eclipse. Eclipse Marketplace makes it easy to
    discover new plugins and Eclipse Marketplace Client makes it simple
    to install directly into Eclipse. If you haven’t used Eclipse
    Marketplace Client, I’d recommend giving it a try from the
    Eclipse>Help>Eclipse Marketplace menu item.</p>
  <p>In 2013, 752 products were successfully installed over 3,800,000
    times from the Eclipse Marketplace Client. The top 10 most popular
    products in 2013 were:</p>

  <ol>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/subversive-svn-team-provider/metrics">Subversive
        - SVN Team Provider</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/subclipse/metrics">Subclipse</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/maven-integration-eclipse-juno-and-newer/metrics">Maven
        Integration for Eclipse (Juno and newer)</a></li>
    <li>Maven Integration for Eclipse WTP (Juno)</li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/eclipse-color-theme/metrics">Eclipse
        Color Theme</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/egit-git-team-provider/metrics">EGit
        - Git Team Provider</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/android-development-tools-eclipse/metrics">Android
        Development Tools for Eclipse</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/pydev-python-ide-eclipse/metrics">PyDev
        - Python IDE for Eclipse</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/jboss-tools-juno/metrics">JBoss
        Tools (Juno)</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/spring-tool-suite-sts-eclipse-juno-42/metrics">Spring
        Tool Suite (STS) for Eclipse Juno (3.8 + 4.2)</a></li>
  </ol>

  <h2>Top 10 NEW Plugins for 2013</h2>
  <p>A number of new plugins and products were introduced to Eclipse
    Marketplace in 2013. Here are the top 10 our users discovered via
    Marketplace. Check them out.</p>

  <ol>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/nodeclipse">Nodeclipse</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/eclipse-pmd">eclipse-pmd</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/node/641101">StartExplorer</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/html-editor-standard">HTML
        Editor</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/node/609100">Jar2UML</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/m2e-dynamic-sources-lookup">m2e
        dynamic sources lookup</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/github-flavored-markdown-viewer-plugin">GitHub
        Flavored Markdown viewer plugin</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/extjs-jsdt-integration">ExtJS
        JSDT Integration</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/ibm-mobile-test-workbench-851-worklight">IBM
        Mobile Test Workbench 8.5.1 for Worklight 6.1</a></li>
    <li><a target="_blank"
      href="http://marketplace.eclipse.org/content/enide-eclipse-nodejs-ide">Enide
        - Eclipse Node.JS IDE</a></li>
  </ol>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/december/images/ians75.PNG"
        alt="ian skerrett" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ian Skerrett<br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://ianskerrett.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/IanSkerrett">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+IanSkerrett">Google +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
