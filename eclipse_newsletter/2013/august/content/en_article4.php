<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>


<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    <a target="_blank" href="http://www.eclipsecon.org/europe2013">EclipseCon
      Europe</a> is the Eclipse Foundation’s primary European event
    designed to create opportunities for the European Eclipse community
    to learn, explore, share, and collaborate on the latest ideas and
    information about Eclipse and its member companies.
  </p>

  <p>EclipseCon is all about community. Contributors, adopters,
    extenders, service providers, consumers, and business and research
    organizations gather to share their expertise and learn from each
    other. EclipseCon delivers networking opportunities that lead to
    synergies in the community, as well as opportunities to give and
    receive help on specific technical issues or to generate business
    opportunities.</p>

  <p>The 2013 keynotes feature an interesting mix: Marty Weiner from
    Pinterest will tell you how they scaled up to serve the fast-growing
    community at Pinterest. Brian Fitzpatrick will talk about
    development practices at Google, and as a third keynote we have Ian
    Robinson from IBM talking about OSGi.</p>

  <p>EclipseCon is not just listening -- it's also about networking and
    exchange. And with the Eclipse circus and the Eclipse band you can
    enjoy two wonderful evenings with good friends.</p>

  <p>
    <a target="_blank"
      href="http://www.eclipsecon.org/europe2013/registration">Register</a>,
    join us, and tell others about it!
  </p>

  <p>I look forward to seeing you in Ludwigsburg from October 29 - 31
    for the eighth year of this great conference!</p>

  <p>
    Date: October 29 - 31, 2013<br> Place: Ludwigsburg, Germany<br>
    Website: <a href="http://eclipsecon.org/europe2013/">eclipsecon.org/europe2013</a>
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/august/images/ralph75.jpg"
        alt="Ralph Mueller" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ralph Mueller <br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://eclipse-membership.blogspot.ca/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/ralph_mueller">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/b/107434916948787284891/109870559592385719776/posts">Google
            +</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
