<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <p>
    The Eclipse <a href="http://www.eclipse.org/legal/CLA.php">Contributor
      License Agreement</a> is now live. This means that contributors
    can execute a CLA, and get theirs on file. Committers will be able
    to use the PMI (project management infrastructure) to look up
    whether a particular contributor <a
      href="https://projects.eclipse.org/user/cla/validate">has a CLA on
      file</a>. So starting immediately, you will be able to refer to a
    CLA rather than asking the “three questions” on a bug.
  </p>

  <p>
    The Eclipse Foundation Contributor’s <a
      href="http://www.eclipse.org/legal/CoO.php">Certificate of Origin</a>
    has also been published, and contributors and committers should
    start using the signed-by option with git.
  </p>

  <p>In order for a contributor to sign their CLA, they need to do the
    following:</p>
  <ol>
    <li>Obtain and Eclipse Foundation userid. Anyone who currently uses
      our Bugzilla or Gerrit systems already has one of those. If they
      don’t, they need to <a
      href="https://dev.eclipse.org/site_login/createaccount.php">register</a>.
    </li>
    <li>Login into the <a href="https://projects.eclipse.org/">projects
        portal</a>, select “My Account”, and then the “Contributor
      License Agreement” tab.
    </li>
  </ol>

  <img
   class="img-responsive" src="/community/eclipse_newsletter/2013/august/images/mikecla.png" alt="" />

  <p>After the Kepler release — Thursday, June 27th — we have started
    automatically enforcing CLAs. This means that every time a
    contribution arrives at one of our git repositories (including
    Gerrit), the author field will be checked to ensure that there is a
    CLA or Committer Agreement on file for the author. If there isn’t,
    the contribution will be rejected.</p>

  <p>Progress!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/august/images/mikem75.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Mike Milinkovich <br />
        <a target="_blank" href="http://eclipse.org">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://mmilinkov.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/b/107434916948787284891/116762335084488724098/posts">Google
            +</a></li>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
