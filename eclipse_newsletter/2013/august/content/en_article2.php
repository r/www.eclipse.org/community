<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>The Eclipse Foundation is going to start allowing its projects to
    host their mainline development on third party forges such as
    GitHub, and (eventually) Bitbucket. This means that an Eclipse
    project will be able to leverage the great development tools
    provided by those vendors. The first project that we are going to
    work with on this is Vert.x, which has been hosted on GitHub since
    its inception. Our intent is to start small, and continually improve
    this program and our support of it. This is an important new option
    for open source projects which have matured to the point where
    vendor-neutral governance, meritocratic processes and proper
    intellectual property management have become important for future
    growth and adoption.</p>

  <p>
    It’s hard to believe that it was almost two years ago when Mikeal
    Rogers penned his “<a target="_blank"
      href="http://www.futurealoof.com/posts/apache-considered-harmful.html">Apache
      Considered Harmful</a>” post, stirring a decent amount of
    controversy at the time. Although I <a target="_blank"
      href="http://mmilinkov.wordpress.com/2011/11/25/foundations-considered-useful/">disagreed
      with many of Mikeal’s points</a>, his key point that open source
    foundations need to change to maintain their relevance resonated
    strongly with us in the Eclipse community. We listened, we’re
    learning, and we’ve been working hard to change our processes and
    infrastructure to stay relevant for open source developers. Here are
    a few examples:
  </p>
  <ol>
    <li><i>We switched to git</i>: Last December 21st we shut off CVS at
      Eclipse, and we have migrated basically all of Eclipse to git.
      (There are a few projects still on Subversion.) Using a modern
      distributed version control system at Eclipse has already helped a
      great deal in increasing contributions and reducing barriers to
      contribution to our projects. Among other things, it has allowed
      us to mirror Eclipse projects on github, which has attracted some
      interest from the development community there.</li>
    <li><i>We adopted Gerrit</i>: Using git has also allowed us to start
      using the Gerrit code review tool at Eclipse, which has been a
      great addition to our contribution workflow.</li>
    <li><i>We implemented the project management infrastructure (PMI)</i>:
      We now have a database-backed system which tracks all of our
      project metadata, such as committer lists, release dates, project
      plans, and the like.</li>
    <li><i>We started using contributor license agreements (CLA)</i>:
      Using CLAs further reduces the barriers to contribution, as we no
      longer have to have a discussion on each contribution about the IP
      rights.</li>
  </ol>
  <p>
    The last two of those items are extremely important because they put
    us in a situation where we can begin to automate workflows, thereby
    reducing the amount of effort by our commmitters and contributors.
    So, for example, last June we have automated the CLA check on all of
    our contributions which flow to us via Gerrit, or directly into our
    git repositories. We will have all of the data in place to determine
    that the author, committer, and signed-by fields on a contribution
    map to a person who is either a committer or who has signed the <a
      href="http://www.eclipse.org/legal/CLA.php">Eclipse Foundation CLA</a>.
  </p>

  <p>And then along came Vert.x.</p>

  <p>
    Early in January 2013, there was a <a target="_blank"
      href="https://groups.google.com/forum/?fromgroups=#!topic/vertx/gnpGSxX7PzI">very
      active discussion</a> about the future of the Vert.x project. The
    project had reached a point in its progress where it was clear that
    it should move to a vendor-neutral home. After quite a bit of
    discussion, the Vert.x community decided that the Eclipse Foundation
    would make a good home. The discussion was particularly interesting
    because the relative merits of moving to Eclipse, Apache, OuterCurve
    and the Software Conservancy were debated in an open and flame-free
    manner. Now that the Eclipse Foundation is open to projects which
    are implemented in any language or technology, without requiring
    linkages to our eponymous IDE, it is a good home for innovative new
    projects like Vert.x.
  </p>

  <p>
    Since its inception, Vert.x has been hosted on GitHub, and is one of
    the <a target="_blank" href="https://github.com/trending?l=java">most
      watched Java projects</a> there. During the process of discussing
    how to migrate Vert.x to the Eclipse Foundation, we had a bit of an
    epiphany: if Eclipse projects could mirror to GitHub, what would
    happen if we simply flipped things around and mirrored projects
    hosted at GitHub on eclipse.org’s git repos? After some discussion,
    we decided that this was a really good idea. Complementing the great
    developer infrastructure at GitHub with the governance and
    meritocratic processes that an open source foundation like Eclipse
    brings to the table will be the best of both worlds for some
    projects.
  </p>

  <p>Projects which choose to take advantage of this will be hosting
    their mainline development remotely, but all of their code will be
    mirrored back to Eclipse’s git repositories. The full Eclipse
    development and IP processes will be applied to these projects.
    Project metadata, plans, committer records and votes will be
    maintained in the Eclipse project management infrastructure (PMI).
    Admin rights to the repos will be owned by the Eclipse Foundation
    webmaster team.</p>

  <p>Initially, most of this will be maintained manually, but over time
    we expect to automate a great deal of it. For example, initially
    checking that a pull request’s author has signed an Eclipse CLA will
    be done manually by the project committer accepting the
    contribution. Over time that will be automated using git commit
    hooks. We will continue to automate these interfaces, so that things
    like committer lists, etc. will be automatically updated based on
    changes in our PMI.</p>

  <p>The last year and a half have been extremely busy at the Eclipse
    Foundation, as we have done a lot of work to modernize our
    infrastructure, and to make it more attractive than ever to become
    part of the Eclipse community. This is part of an on-going effort to
    remain relevant as the expectations of developers and open source
    committers rapidly evolve.</p>

  <p>
    If you are interested in learning more about this, please read our <a
      href="http://www.eclipse.org/org/SocialCodingFAQ.php">FAQ</a>.
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/august/images/mikem75.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Mike Milinkovich <br />
        <a target="_blank" href="http://eclipse.org">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://mmilinkov.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/b/107434916948787284891/116762335084488724098/posts">Google
            +</a></li>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
