<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>
      Eclipse Juno 4.2 has been the first Release Train building on the
      new Eclipse 4 (e4) Application Platform. This raises the question
      of how to migrate existing Eclipse 3.x applications to Eclipse 4
      (e4), e.g 3.7 Indigo. In this tutorial I will review the options
      for developing Eclipse plugins and applications with the new
      platform. Looking forward to your comments and additions. Previous
      parts (<a title="Eclipse 4 (e4) tutorial part 1"
        href="http://eclipsesource.com/blogs/2012/05/10/eclipse-4-final-sprint-part-1-the-e4-application-model/">part
        1</a> and <a title="Eclipse 4 (e4) Tutorial part 2"
        href="http://eclipsesource.com/blogs/2012/06/12/eclipse-4-e4-tutorial-part-2/">part
        2</a>) of this tutorial are available now as a <a
        title="Eclipse 4 Tutorial"
        href="http://eclipsesource.com/eclipse4tutorial">downloadable
        PDF</a>.
    </p>
    <h3>Option 1: Use the Compatibility Layer</h3>
    <p>The compatibility layer enables 3.x applications to run on the
      new Eclipse 4 platform without any code adaptation. In the
      beginning, most existing projects will probably use this option.
      Besides the easy migration, you can still use all existing
      components and frameworks, even if they are not migrated to e4.
      Finally, your application stays backwards compatible, meaning it
      can still be run on 3.7.</p>
    <p>To ease migration, the compatibility layer provides the 3.x
      workbench API and translates all calls into the programming model
      of e4. In the background, it transparently creates an Application
      Model. For example, if the 3.x application registers a 3.x view
      using an extension point, the compatibility layer will create a
      Part for this view. One important criteria for existing
      applications to work well on the compatibility layer is that they
      should not use any internal workbench API. Aside from this, there
      should be no source code changes required. However, you will
      probably need to adapt the product or run configuration of the
      application. Eclipse 4 needs additional plugins to work, and as
      there are no direct dependencies, this will not be automatically
      discovered. These are the plugins you will need to add:</p>
    <p>org.eclipse.equinox.ds : The OSGi plugin enabling declarative
      services</p>
    <p>org.eclipse.equinox.event: The OSGi event broker</p>
    <p>org.eclipse.equinox.util: Required by the first two</p>
    <p>org.eclipse.e4.ui.workbench.addons.swt: Enables features such as
      minimizing and maximizing parts</p>
    <p>An obvious disadvantage of using the compatibility layer is that
      you won’t benefit from the new concepts, such as the application
      model, dependency injection and annotations provided by e4. Some
      other improvements will work though, such as CSS styling.</p>
    <h3>Option 2: A pure Eclipse 4 (e4) Application</h3>
    <p>The second option, primarily interesting for new projects, is to
      build a pure Eclipse 4 (e4) application without any compatibility
      layer. Any existing parts of an application should be completely
      migrated to e4. The major disadvantage of this option is that many
      existing components and frameworks cannot be reused. This affects
      components doing UI contributions such as Views. Examples would be
      the Error Log, the Console View or existing editors. To use them
      in an e4 application they would have to be migrated to e4 as well.
      However, components without any Workbench contributions should
      work in a pure e4 application.</p>
    <h3>Option 3: Compatibility Layer and Eclipse 4 (e4) Plugins</h3>
    <p>For this option, you would rely on the compatibility layer to
      reuse all existing components without any adaptations. New
      components would be developed following the e4 programming model,
      including dependency injection and annotations. There are three
      ways to integrate e4 elements into a compatibility layer
      application.</p>
    <p>
      The first option is to use processors and fragments to add
      elements to the application model created by the compatibility
      layer. However, there are currently timing problems. When
      processors and fragments are being processed, the compatibility
      layer has not yet created the complete application model. (See
      this bug report. (<a
        href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=376486">https://bugs.eclipse.org/bugs/show_bug.cgi?id=376486</a>).
      Therefore, this option might work for handles and views, but
      currently it doesn’t work for editors.
    </p>
    <p>The second option for integrating Eclipse 4 components is to
      create a copy of the application model used by the compatibility
      layer, register it as the application model of your application
      and add new e4 components to it. The relevant model LegacyIDE.xmi
      can be found in in the plugin org.eclipse.ui.workbench.</p>
    <p>The third option is to use the 3.x e4 bridge from the e4 tools
      project, developed by Tom Schindl. The goal of the bridge is to
      ease single sourcing applications on 3.x and e4, which means that
      views and editors can be used in 3.x and e4 in parallel. To enable
      this, the plugin org.eclipse.e4.tools.compat provides wrapper
      classes that implement the interfaces of 3.x. For example, the
      wrapper DIViewPart implements ViewPart. In the wrapper, you
      specify a class (POJO), which implements a view following the e4
      programming model, including dependency injection. Essentially the
      wrapper is just a pointer to an e4 object. It will initialize the
      POJO using dependency injection.</p>
    <p>A 3.x wrapper</p>

    <div class="wp_syntax">
      <table>
        <tbody>
          <tr>
            <td class="code"><pre class="java"
                style="font-family: monospace;">
                <span style="color: #000000; font-weight: bold;">public</span> <span
                  style="color: #000000; font-weight: bold;">class</span> ExampleViewWrapper <span
                  style="color: #000000; font-weight: bold;">extends</span> DIViewPart<span
                  style="color: #009900;">{</span>
   <span style="color: #000000; font-weight: bold;">public</span> Example3xViewWrapper<span
                  style="color: #009900;">(</span><span
                  style="color: #009900;">)</span> <span
                  style="color: #009900;">{</span>
   <span style="color: #000000; font-weight: bold;">super</span><span
                  style="color: #009900;">(</span>ExampleE4View.<span
                  style="color: #000000; font-weight: bold;">class</span><span
                  style="color: #009900;">)</span><span
                  style="color: #339933;">;</span>
   <span style="color: #009900;">}</span>
<span style="color: #009900;">}</span>
              </pre></td>
          </tr>
        </tbody>
      </table>
    </div>

    <p>A e4 view:</p>

    <div class="wp_syntax">
      <table>
        <tbody>
          <tr>
            <td class="code"><pre class="java"
                style="font-family: monospace;">
                <span style="color: #000000; font-weight: bold;">public</span> <span
                  style="color: #000000; font-weight: bold;">class</span> ExampleView <span
                  style="color: #009900;">{</span>
<span style="color: #000000; font-weight: bold;">private</span> <span
                  style="color: #003399;">Label</span> label<span
                  style="color: #339933;">;</span>
   @Inject
   <span style="color: #000000; font-weight: bold;">public</span> ExampleView<span
                  style="color: #009900;">(</span><span
                  style="color: #003399;">Composite</span> parent<span
                  style="color: #009900;">)</span><span
                  style="color: #009900;">{</span>
      label <span style="color: #339933;">=</span> <span
                  style="color: #000000; font-weight: bold;">new</span> <span
                  style="color: #003399;">Label</span><span
                  style="color: #009900;">(</span>parent, SWT.<span
                  style="color: #006633;">NONE</span><span
                  style="color: #009900;">)</span><span
                  style="color: #339933;">;</span>
      label.<span style="color: #006633;">setText</span><span
                  style="color: #009900;">(</span><span
                  style="color: #0000ff;">"Hello World"</span><span
                  style="color: #009900;">)</span><span
                  style="color: #339933;">;</span>
   <span style="color: #009900;">}</span>
<span style="color: #009900;">}</span>
              </pre></td>
          </tr>
        </tbody>
      </table>
    </div>

    <p>
      This approach allows you to develop new parts of the application
      using all the benefits of e4 and as well, reuse all existing
      components. Further, the views developed in this way can be
      integrated into any pure e4 application without any adaptations.
      The approach is described more in detail in <a
        href="http://eclipsesource.com/blogs/tutorials/eclipse-4-e4-tutorial-soft-migration-from-3-x-to-eclipse-4-e4/">this
        tutorial</a>.
    </p>
    <h3>An Eclipse 4 (e4) Application including some 3.x components</h3>
    <p>In this option you would develop an e4 application and reuse some
      3.x components. If they don’t access the workbench API, there
      shouldn’t be any problems. In some cases, even UI components can
      be easily reused or adapted to work with e4. However, this needs
      to be evaluated individually for each component.</p>
    <p>&nbsp;</p>
    <p>In the end, when and how to migrate to e4 is still one of those
      “it depends…” decisions. Probably the most important criteria are
      the number of existing components and the number of reused
      third-party components. If you have additional options for
      migrating or mixing the two technologies, let me know and I will
      gladly add it to this post.</p>
    <p>
      This tutorial and all other parts of the series are available as a
      <a title="Eclipse 4 Tutorial"
        href="http://eclipsesource.com/eclipse4tutorial">downloadable
        PDF</a>.
    </p>
    <p>For more information, contact us:</p>
    <p>
      <a href="https://plus.google.com/107242665623221408066"
        rel="author"> Join me on Google+</a>
    </p>
    <p>Jonas Helming and Maximilian Koegel</p>
    <p>
      <a href="http://eclipsesource.com/munich">EclipseSource Munich</a>
      leads
    </p>
    <p>Author: Jonas Helming</p>
    <p>&nbsp;</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/february/images/jhelming.jpg"
        alt="Jonas Helming" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jonas Helming <br />
        <a href="http://eclipsesource.com/">EclipseSource</a>
      </p>
      <ul class="author-link">
        <li><a href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>
        <li><a href="https://twitter.com/JonasHelming">Twitter</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

