<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <div class="article">
      <div class="titlepage">
        <div>

          <div>
            <h3 class="subtitle">
              <i>Building Eclipse RCP applications based on Eclipse 4 </i>
            </h3>
          </div>
          <div>
            <div class="author">
              <h3 class="author">
                <span class="firstname">Lars</span> <span
                  class="surname">Vogel</span>
              </h3>
            </div>
          </div>
          <div>
            <p class="releaseinfo"></p>
            <p>Version 5.8</p>
            <p class="releaseinfo"></p>
          </div>
          <div>
            <p class="copyright">Copyright © 2009, 2010 , 2011, 2012
              Lars Vogel</p>
          </div>
          <div>
            <p class="pubdate">10.04.2012</p>
          </div>
          <div>
            <div class="revhistory">
              <table>
                <tbody>
                  <tr>
                    <th colspan="4"><b>Revision
                        History</b></th>
                  </tr>
                  <tr>
                    <td>Revision 0.1</td>
                    <td>14.02.2009</td>
                    <td><span class="firstname">Lars<br></span><span
                      class="surname">Vogel<br></span></td>
                    <td>created</td>
                  </tr>
                  <tr>
                    <td>Revision 0.2 - 5.8</td>
                    <td>16.02.2009 - 10.04.2012</td>
                    <td><span class="firstname">Lars<br></span><span
                      class="surname">Vogel<br></span></td>
                    <td>bug fixes and enhancements</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div>
            <div class="abstract">
              <a id="abstract"></a>
              <p class="title">
                <b>Eclipse 4 and the Compatibility Layer</b>
              </p>
              <p>This tutorial gives an overview on how to use the
                Compatibility Layer in Eclipse 4 to run plug-ins based
                on the Eclipse 3.x API.</p>
            </div>
          </div>
        </div>
        <hr>
      </div>
      <div class="toc">
        <p>
          <b>Table of Contents</b>
        </p>
        <dl>
          <dt><span class="section"><a href="#compatibiliylayer">1. Compatibility Layer</a></span></dt>
          <dd><span class="section"><a
            href="#compatibiliylayer_overview">1.1. Running
              Eclipse 3.x plug-ins on top of Eclipse 4</a></span></dd>
          <dd><span class="section"><a
            href="#compatibiliylayer_features">1.2. Using features</a></span></dd>
          <dd><span class="section"><a
            href="#compatibiliylayer_plugins">1.3. Using plug-ins</a></span>
          </dd>
          <dt>
            <span class="section"><a href="#mixedmode_forward">2.
                Wrapping your Eclipse 4 components for Eclipse 3.x</a></span>
          </dt>
          <dt>
            <span class="section"><a href="#mixedmode">3. Mixing Eclipse
                4 and Eclipse 3.x</a></span>
          </dt>
          <dt>
            <span class="section"><a href="#migration">4. Migration to
                Eclipse 4</a></span>
          </dt>
          <dt>
            <span class="section"><a href="#mixedmode_plans">5. Future
                plans for migration</a></span>
          </dt>
          <dt>
            <span class="section"><a href="#questions">6. Questions and
                Discussion</a></span>
          </dt>
          <dt>
            <span class="section"><a href="#resources">7. Links and
                Literature</a></span>
          </dt>
          <dd><span class="section"><a href="#sourcecode">8.1. Source
                    Code</a></span>
          </dd>
        </dl>
      </div>




      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="compatibiliylayer"></a>1.&nbsp;Compatibility
                Layer
              </h2>
            </div>
          </div>
        </div>
        <div class="section">
          <div class="titlepage">
            <div>
              <div>
                <h3 class="title">
                  <a id="compatibiliylayer_overview"></a>1.1.&nbsp;Running
                  Eclipse 3.x plug-ins on top of Eclipse 4
                </h3>
              </div>
            </div>
          </div>
          <p>Eclipse 4 allows you to run Eclipse 3.x RCP applications
            and Eclipse 3.x plug-ins. Your Eclipse RCP application
            should have an existing product configuration file for the
            migration.</p>
          <p>The compatibility layer supports the migration of existing
            Eclipse 3.x plug-ins and applications to Eclipse 4 and to
            start evaluating advantages of Eclipse 4, e.g. the
            application model and CSS based styling.</p>
          <p>
            The compatibility layer converts the relevant extension
            point information into the application model. In the
            <code class="code">org.eclipse.platform</code>
            plug-in you find the
            <code class="filename">LegacyIDE.e4xmi</code>
            file which defines the initial window and some <em
              class="wordasword">model Addons</em>.

          </p>
          <p>
            If Eclipse is started in the compatibility mode, this file
            is read via the
            <code class="code">E4Workbench</code>
            class. This class is part of the
            <code class="code">org.eclipse.e4.workbench.ui.internal</code>
            package.
            <code class="code">E4Workbench</code>
            will then convert the relevant extension points into
            elements of the application model.

          </p>
          <p>
            New programming concepts of Eclipse 4 such as dependency
            injection for <em class="wordasword">Views</em> do not work
            (out of the box) with the compatibility layer.

          </p>
        </div>
        <div class="section">
          <div class="titlepage">
            <div>
              <div>
                <h3 class="title">
                  <a id="compatibiliylayer_features"></a>1.2.&nbsp;Using
                  features
                </h3>
              </div>
            </div>
          </div>
          <p>
            The easiest way to migrate your Eclipse RCP application to
            Eclipse 4.2 is to have a feature based product configuration
            file which includes the standard
            <code class="code">org.eclipse.rcp</code>
            feature.

          </p>
          <p>In Eclipse 4 this feature has been upgraded to include most
            of the necessary additional plug-ins for the compatibility
            mode. If your product is based on this feature you can
            directly start your Eclipse 3.x product and it will work.</p>
          <p>
            You only need to add two additional features to your product
            configuration file, the
            <code class="code">org.eclipse.emf.ecore</code>
            and the
            <code class="code">org.eclipse.emf.common</code>
            feature.

          </p>
        </div>
        <div class="section">
          <div class="titlepage">
            <div>
              <div>
                <h3 class="title">
                  <a id="compatibiliylayer_plugins"></a>1.3.&nbsp;Using
                  plug-ins
                </h3>
              </div>
            </div>
          </div>
          <p>If you product configuration file is based on plug-ins you
            have to add the following plug-ins to it:</p>
          <div class="itemizedlist">
            <ul class="itemizedlist">
              <li class="listitem">
                <p>org.eclipse.e4.ui.workbench.addons.swt</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.equinox.ds</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.equinox.event</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.equinox.util</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.platform</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.ui.forms</p>
              </li>
              <li class="listitem">
                <p>org.eclipse.ui.intro</p>
              </li>
            </ul>
          </div>
          <p></p>
          <p>After this change you should be able to start your existing
            application based on Eclipse 4.</p>
        </div>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="mixedmode_forward"></a>2.&nbsp;Wrapping your
                Eclipse 4 components for Eclipse 3.x
              </h2>
            </div>
          </div>
        </div>
        <p>
          To migrate your Views and Editors to Eclipse 4 you can use the
          <code class="code">org.eclipse.e4.tools.compat</code>
          plug-in from the e4 tooling projects

        </p>
        <p>To use this bridge in Eclipse 4.2 or Eclipse 3.8 just install
          the org.eclipse.e4.tools.e3x.bridge feature into your Eclipse
          IDE.</p>
        <p>Afterwards add the following plug-ins to your MANIFEST.MF</p>
        <div class="itemizedlist">
          <ul class="itemizedlist">
            <li class="listitem">
              <p>org.eclipse.e4.tools.compat</p>
            </li>
            <li class="listitem">
              <p>org.eclipse.e4.core.contexts</p>
            </li>
          </ul>
        </div>
        <p></p>
        <p>You can now develop your Parts as Pojos:</p>
        <p></p>
        <pre class="programlisting">
          <span class="hl-keyword">package</span> example.compat.parts;

<span class="hl-keyword">import</span> javax.annotation.PostConstruct;

<span class="hl-keyword">import</span> org.eclipse.e4.ui.di.Focus;
<span class="hl-keyword">import</span> org.eclipse.jface.viewers.ArrayContentProvider;
<span class="hl-keyword">import</span> org.eclipse.jface.viewers.ColumnLabelProvider;
<span class="hl-keyword">import</span> org.eclipse.jface.viewers.TableViewer;
<span class="hl-keyword">import</span> org.eclipse.jface.viewers.TableViewerColumn;
<span class="hl-keyword">import</span> org.eclipse.swt.SWT;
<span class="hl-keyword">import</span> org.eclipse.swt.widgets.Composite;

<span class="hl-keyword">public</span> <span class="hl-keyword">class</span> Part {
    <span class="hl-keyword">public</span> <span class="hl-keyword">static</span> <span
            class="hl-keyword">final</span> String ID = <span
            class="hl-string" style="color: blue">"example.compat.view"</span>;

    <span class="hl-keyword">private</span> TableViewer viewer;

    <em><span class="hl-annotation" style="color: gray">@PostConstruct</span></em>
    <span class="hl-keyword">public</span> <span class="hl-keyword">void</span> createPartControl(Composite parent) {
        viewer = <span class="hl-keyword">new</span> TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
                | SWT.V_SCROLL);
        viewer.setContentProvider(ArrayContentProvider.getInstance());
        TableViewerColumn column = <span class="hl-keyword">new</span> TableViewerColumn(viewer, SWT.NONE);
        column.getColumn().setWidth(<span class="hl-number">100</span>);
        column.setLabelProvider(<span class="hl-keyword">new</span> ColumnLabelProvider(){
            <em><span class="hl-annotation" style="color: gray">@Override</span></em>
            <span class="hl-keyword">public</span> String getText(Object element) {
                <span class="hl-keyword">return</span> element.toString();
            }
        });

        <em class="hl-comment" style="color: #080">// Provide the input to the ContentProvider</em>
        viewer.setInput(<span class="hl-keyword">new</span> String[] { <span
            class="hl-string" style="color: blue">"One"</span>, <span
            class="hl-string" style="color: blue">"Two"</span>, <span
            class="hl-string" style="color: blue">"Three"</span> });
    }

    <em><span class="hl-annotation" style="color: gray">@Focus</span></em>
    <span class="hl-keyword">public</span> <span class="hl-keyword">void</span> setFocus() {
        viewer.getControl().setFocus();
    }
} </pre>
        <p></p>
        <p>In your plugin.xml you use ViewWrapper to define your Views.

        </p>
        <p></p>
        <pre class="programlisting">
          <span class="hl-keyword">package</span> example.compat;

<span class="hl-keyword">import</span> org.eclipse.e4.tools.compat.parts.DIViewPart;

<span class="hl-keyword">import</span> example.compat.parts.View;

<span class="hl-keyword">public</span> <span class="hl-keyword">class</span> ViewWrapper <span
            class="hl-keyword">extends</span> DIViewPart&lt;View&gt; {

    <span class="hl-keyword">public</span> ViewWrapper() {
        <span class="hl-keyword">super</span>(Part.<span
            class="hl-keyword">class</span>);
    }

} </pre>
        <p></p>
        <p>This way you can use the dependency injection already in your
          Eclipse 3.x plugin, have a better possibility to test your
          user interface components in plain JUnit test (they are just
          POJO) and get ready for for a full Eclipse 4 migration.</p>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="mixedmode"></a>3.&nbsp;Mixing Eclipse 4 and
                Eclipse 3.x
              </h2>
            </div>
          </div>
        </div>
        <p>Mixing Eclipse 4 and Eclipse 3.x. components is currently not
          officially supported in the Eclipse 4.2 release.</p>
        <p>
          You can however define your own
          <code class="filename">LegacyIDE.e4xmi</code>
          file and add your model components to this file. Via the <em
            class="parameter"><code>applicationXMI</code></em> parameter
          in your
          <code class="code">org.eclipse.core.runtime.products</code>
          extension point you point to that file.

        </p>
        <p>
          For this approach you have to copy the standard
          <code class="filename">LegacyIDE.e4xmi</code>
          file and add your model components to it. This file can be
          found in the
          <code class="code">org.eclipse.ui.workbench</code>
          plug-in.

        </p>
        <p>A code example for this approach can be found under the
          following URL:</p>
        <p></p>
        <pre class="programlisting">https:<em class="hl-comment"
            style="color: #080">//github.com/fredrikattebrant/Simple-4x-RCP-app </em>
        </pre>
        <p></p>
        <p>While this approach seems to work, it is currently not
          officially supported in the Eclipse 4.2 release.</p>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="migration"></a>4.&nbsp;Migration to Eclipse 4
              </h2>
            </div>
          </div>
        </div>
        <p>
          If you want to migrate your Eclipse 3.x RCP application to the
          Eclipse 4 programming model, you can't directly reuse existing
          <code class="filename">plugin.xml</code>
          based user interface components, e.g. <em class="wordasword">Views</em>
          or <em class="wordasword">Editors</em> based on the definition
          in
          <code class="filename">plugin.xml</code>
          .

        </p>
        <p>
          Components based on the
          <code class="filename">plugin.xml</code>
          file must be adjusted to avoid inheritance of Eclipse classes
          and to use the programming model based on
          <code class="code">@Inject</code>
          . They also must be contributed to the application model.

        </p>
        <p>
          Components which are not directly based on the
          <code class="filename">plugin.xml</code>
          file must be adjusted if they use Eclipse 3.x singletons, as
          for example
          <code class="code">Platform</code>
          or
          <code class="code">PlatformUI</code>
          , to access Eclipse API.

        </p>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="mixedmode_plans"></a>5.&nbsp;Future plans for
                migration
              </h2>
            </div>
          </div>
        </div>
        <p>The Eclipse 4 team intend to provide support for such mixed
          mode hybrid applications in future Eclipse releases.</p>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="questions"></a>6.&nbsp;Questions and Discussion
              </h2>
            </div>
          </div>
        </div>
        <p>
          Before posting questions, please see the <a class="ulink"
            href="http://www.vogella.com/faq.html" target="_top">
            vogella FAQ</a>. If you have questions or find an error in
          this article please use the <a class="ulink"
            href="http://groups.google.com/group/vogella" target="_top">
            www.vogella.com Google Group</a>. I have created a short
          list <a class="ulink"
            href="http://www.vogella.com/blog/2010/03/09/asking-community-questions/"
            target="_top"> how to create good questions </a> which might
          also help you.

        </p>
      </div>
      <div class="section">
        <div class="titlepage">
          <div>
            <div>
              <h2 class="title">
                <a id="resources"></a>7.&nbsp;Links and Literature
              </h2>
            </div>
          </div>
        </div>


        <div class="section">
          <div class="titlepage">
            <div>
              <div>
                <h3 class="title">
                  <a id="sourcecode"></a>7.1.&nbsp;Source Code
                </h3>
              </div>
            </div>
          </div>
          <p>
            <a class="ulink"
              href="http://www.vogella.com/code/index.html"
              target="_top">Source Code of Examples</a>

          </p>
        </div>
        <p>
          <a class="ulink" href="http://wiki.eclipse.org/E4"
            target="_top"> http://wiki.eclipse.org/E4</a> Eclipse E4 -
          Wiki

        </p>
        <p>
          <a class="ulink"
            href="http://www.vogella.com/articles/EclipseRCP/article.html"
            target="_top"> Eclipse RCP</a>

        </p>
        <p>
          <a class="ulink"
            href="http://www.vogella.com/articles/EclipseEMF/article.html"
            target="_top"> Eclipse EMF</a>

        </p>
        <p>
          <a class="ulink"
            href="http://www.vogella.com/articles/DependencyInjection/article.html"
            target="_top">Dependency Injection </a>

        </p>
      </div>
    </div>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/february/images/larsvogel.png"
        alt="Lars Vogel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Lars Vogel <br />
        <a href="http://www.vogella.com/">vogella</a>
      </p>
      <ul class="author-link">
        <li><a href="http://www.vogella.com/blog/">Blog</a></li>
        <li><a href="https://twitter.com/vogella">Twitter</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

