<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>Sirius allows you to create a model and to represent information in
    a visual way. So, instead of a long article describing how to use
    it, we thought it would be better to simply show you with a few
    videos!</p>

  <h2>Sirius by Cédric Brun</h2>
  <iframe width="600" height="315"
    src="//www.youtube.com/embed/hyDxSmbSi2g"
    allowfullscreen></iframe>
  <br />
  <p>
    Recorded at EclipseCon France 2013 in Toulouse, Cedric Brun, Obeo's
    CTO, is interviewed by Ian Skerrett to<br>present Sirius.
  </p>

  <h2>Principles</h2>
  <iframe width="600" height="315"
    src="//www.youtube.com/embed/SEnfhNl47iA?list=UU7d4apyNIcrOzpLLcLavfZw"
     allowfullscreen></iframe>
  <br />
  <p>Discover a sample modeling workbench created with Sirius, and look
    behind-the-scenes of this modeler.</p>

  <h2>Getting Started</h2>
  <iframe width="600" height="315"
    src="//www.youtube.com/embed/08XIW8hZOyQ?list=UU7d4apyNIcrOzpLLcLavfZw"
     allowfullscreen></iframe>
  <br />
  <p>This video shows the first steps to creating a new modeling
    workbench with Sirius.</p>

  <p>
    If you don't have access to YouTube, the <a target="_blank"
      href="http://www.obeo.fr/videos/sirius/Sirius%20-%20Getting%20Started.mp4">Getting
      Started video</a> is also available in mp4 format.
  </p>

  <h2>Learn more</h2>

  <p>
    To discover Sirius and to get a preliminary version, please visit
    the <a href="http://eclipse.org/sirius/">Eclipse Sirius website</a>.
  </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/freddy75.jpg"
        alt="freddy allilaire" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Freddy Allilaire<br />
        <a target="_blank" href="http://www.obeo.fr/">Obeo</a>
      </p>
      <ul class="author-link">
        <!-- <li><a target="_blank" href="">Blog</a></li> -->
        <li><a target="_blank" href="https://twitter.com/allilaire">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/u/0/+FreddyAllilaire/ ">Google +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
