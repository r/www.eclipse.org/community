<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>Many modeling workbenches have already been implemented with Sirius
    in various fields and use-cases. Here are some noteworthy examples.</p>

  <h2>Thales: a System Engineering modeling workbench</h2>
  <p>One of the most deployed workbench based on Sirius is the tool
    created by Thales to equip its system engineers. The tool implements
    Arcadia, a model-based architecture engineering approach defined by
    Thales.</p>

  <p>For Arcadia, Thales integrated and extended several standards such
    as AADL, DoDAF, NAF, UML or SysML. Specific viewpoints were also
    created and are dedicated to:</p>
  <ul>
    <li>operational analysis</li>
    <li>functional analysis</li>
    <li>logical architecture</li>
    <li>physical architecture</li>
    <li>non functional constraints</li>
    <li>non functional trade-offs</li>
    <li>transition to software or hardware</li>
  </ul>
  <br />
  <p>The tool supports three main objectives:</p>
  <ol>
    <li>Decrease the rework in design and production by performing early
      validation of key architectural aspects;</li>
    <li>Improve the quality of the architecture by helping the engineers
      to take better decisions regarding complex, but necessary,
      architectural trade-offs;</li>
    <li>Capitalize on both product definition and know-how by supporting
      negotiation and compromise between stakeholders.</li>
  </ol>
  <p>The tool provides several editors for these viewpoints and allows
    the system architects to graphically design and analyze a system.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.1-sirius-thales.png"
    width="600" id="sirius-thales" alt="" />
  <br />
  <br />

  <p>System architects can evaluate their candidate’s architecture
    against industrial constraints (cost & schedule, performance,
    safety, maintainability, product policy, etc.). For each type of
    constraint, layers can be applied on the diagrams to graphically
    highlight the violated constraints.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.2-sirius-thales.png"
    width="600" alt="" />

  <p>
    <i>Performance analysis</i>
  </p>

  <p>Initially, the tool was developed to target system and software
    architecture engineering of data processing, signal processing or
    information systems. In the end, Thales successfully applied the
    tool to larger fields, such as electrical power systems or thermal
    systems.</p>

  <p>The tool can be used to work on several interleaved multi-physics
    models of a new airplane:</p>
  <ul>
    <li>Power Model describing electrical generators, loads, converters,
      etc;</li>
    <li>Thermal model describing turbines, compressors, valves, etc;</li>
    <li>Command & Control model describing interfacing units, computing
      unit, data, etc.</li>
  </ul>
  <br />
  <p>Specific viewpoints on these models can help the engineers evaluate
    the solution architecture and detect problems before the IVVQ
    (Integration, Verification, Validation, Qualification) phase for
    many fields:</p>
  <ul>
    <li>electrical power systems</li>
    <li>power and thermal performance depending on flight phase
      consumption</li>
    <li>redundancy rules, failure scenarios and propagation</li>
    <li>reconfiguration issues</li>
    <li>early identification of spatial arrangement constraints
      impacting the architecture</li>
    <li>etc.</li>
  </ul>
  <br />
  <p>In summary, this approach has many advantages. Mainly, it allows
    system architects to avoid exhaustive modeling and, thanks to
    multi-viewpoint tooling, they can adjust the modeling efforts on
    major engineering issues.</p>


  <h2>French Ministry of Defense: Software architecture and design</h2>

  <p>The french ministry of defence has developed a workbench to design
    its new IT applications. The modeler allows software architects to
    specify the interactions between the users and the system, to define
    the services, the entities and the screen flow of the applications.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.3-mindef.png"
    width="600" alt="" />
  <br />

  <p>
    <i><b>Graal Designer:</b> Users/System Interactions and link with
    the related user-stories</i>
  </p>
  <br />

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.4-mindef.png"
    width="600" id="mindefence" alt="" />
  <br />

  <p>
    <i><b>Cinematic Designer:</b> Flow between pages of an application</i>
  </p>

  <p>It is complemented by code generators to automatically produce an
    important part of the JavaEE source code.</p>
  <p>In addition to improving productivity, now, the software
    architecture of the french ministry of defense applications can be
    reproduced easily.</p>

  <p>The benefits of this approach, with a dedicated tool, have been
    successfully demonstrated on a first project that represents 6 000
    days and more than 600 000 of lines of code, 80% of which were
    automatically generated.</p>

  <p>Furthermore, a significant part of the modelers and code generators
    created during this project was contributed to Open Source and are
    used by other companies on their own projects.</p>

  <h2>MAIF: New insurance products definition</h2>
  <p>MAIF, an insurance company, has equipped its experts in P&amp;C
    products (property and casualty) with a graphical tool to define the
    new insurance products and to administrate their life-cycle.</p>

  <p>The tool, an RCP application integrating Sirius, provides graphical
    editors to create, modify, consult, version and validate each
    product. The models created with this workbench are directly
    consumed by the IT system through distributed services
    (progressively replacing legacy ones).</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.5-maif.png"
    width="600" id="maif" alt="" />
  <p>
    <i>Architecture of the Product Catalog</i>
  </p>

  <p>With this solution, MAIF has dramatically reduced its need for
    specific IT developments concerning the delivery of insurance
    products. As a result, the deployment of new products was reduced
    from several weeks to only one week. Their process is not only more
    clear and simple, but it allowed them to also increase their
    productivity and optimise their deployment process.</p>

  <h2>ESA: Modeling satellites on-board applications</h2>
  <p>The European Space Agency (ESA) has defined a generic architecture
    for on-board satellite applications. This software reference
    architecture (SRA) aims to make software development faster in the
    context of a reduced schedule, to allow late definition or changes
    (e.g., adaptation to hardware specificities or changes) and to cope
    with the various system integration strategies.</p>

  <p>The cornerstone principle of the SRA is separation of concerns,
    which is used in order to separate different aspects of software
    design (functional and non-functional concerns, actor's area of
    expertise, etc.) and address them with the best-fit formalisms,
    tools and verification techniques. Other key principles of SRA are
    composability, compositionality, property preservation and
    composition with guarantees.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.6-esa.png"
    width="600" alt="" />
  <p>
    <i><b>Design views:</b> Represent self-contained concerns relevant
      to stakeholders</i>
  </p>

  <p>EMF (Eclipse Modeling Framework) and Sirius were selected to
    develop these views. The resulting editor, initially developed by
    the University of Padova, was later adopted and refined in
    subsequent ESA-funded studies.</p>

  <p>Two strategies have been considered:</p>
  <ul>
    <li>UML with a specific profile</li>
    <li>a DSL (Domain Specific Model)</li>
  </ul>
  <p>Because of the gap between the concepts of the SRA component model
    and those defined by UML, using UML in this context would have
    introduced unnecessary complexity. That's the reason why an Ecore
    metamodel, called SCM, implementing the component model concepts was
    developed. With this approach, the tool natively supports the
    component model of the SRA.</p>

  <p>Then, Sirius was used to implement the 19 types of diagrams and 8
    kinds of tables, which allow the user to edit and visualize the
    components in modeling workbenches integrated with Eclipse.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.7-esa.png"
    width="600" id="esa" alt="" />

  <p>
    <i><b>Processor board diagram:</b> Declaration of the internals of a
      processor board in terms of processors, processor cores, cache and
      memory banks.</i>
  </p>
  <br />

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-4.8-esa.png"
    width="600" alt="" />

  <p>
    <i><b>Hardware diagram:</b> Declaration of the processing units,
      sensors, actuators, devices and the interconnections between them.</i>
  </p>

  <p>These views are complemented by validation rules and specific
    actions (for intensive computations).</p>

  <p>An alternative would have been to directly use GMF, but
    implementing 19 specific diagrams would have consumed most of the
    effort to the detriment of realizing and perfectioning the approach.
    On the contrary, with Sirius, the design views have been implemented
    much faster with better support for extensibility and incremental
    adaptations. Most of all, it enables fast iterations with
    stakeholders in the prototypal phase for delicate points of the
    methodology.</p>



<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/fred75.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Frédéric Madiot<br />
        <a target="_blank" href="http://www.obeo.fr/">Obeo</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://fmadiot.blogspot.fr/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/fmadiot ">Twitter</a></li>
        <!-- <li><a target="_blank" href="">Google +</a></li> -->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/Daniel75.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Daniel Exertier<br />
        <a target="_blank" href="http://www.thalesgroup.com/">Thales</a>
      </p>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
