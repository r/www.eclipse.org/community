<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <br />
  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.1-sirius-logo.png"
    width="300" id="sirius-logo" alt="" />
  <br />
  <br />
  <h2>Overview</h2>
  <p>
    Sirius is a new Eclipse project (<a
      href="http://eclipse.org/sirius/">http://www.eclipse.org/sirius</a>)
    that allows you to easily create your own graphical modeling tools.
    It leverages the Eclipse modeling technologies, including EMF for
    the model management and GMF for the graphical representation. Based
    on a viewpoint approach, Sirius makes it possible to equip teams who
    have to deal with complex architectures on specific domains.
  </p>

  <p>It is particularly adapted for users that have defined a DSL
    (Domain Specific Language) and need graphical representations to
    better elaborate and analyze a system and improve the communication
    with other team members, partners or customers.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.2-sirius-workbench.png"
    width="600" alt="" />

  <p>Figure 1 - A modeling workbench created with Sirius</p>

  <p>All shape characteristics and behaviors can be easily configured
    with a minimal technical knowledge. This description is dynamically
    interpreted to materialize the workbench within the Eclipse IDE. No
    code generation is involved, the specifier of the workbench can
    receive instant feedback while adapting the description. Once
    completed, the modeling workbench can be deployed as a standard
    Eclipse plugin. Thanks to this short feedback loop, a workbench or
    its specialization can be created in a matter of hours.</p>

  <p>
    Sirius is the result of a collaboration between the <a
      target="_blank" href="https://www.thalesgroup.com/en">Thales Group</a>
    and <a target="_blank" href="http://www.obeo.fr/">Obeo</a> initiated
    in 2007. The Thales’ goal was to obtain a generic workbench for
    model-based architecture engineering that could be easily tailored
    to fit the specific needs of each Thales project. The tool had to
    provide ergonomic facilities and support very large models.
  </p>

  <p>
    Originally, the Sirius technology was (and still is) packaged and
    distributed by Obeo as part of the <a target="_blank"
      href="http://www.obeo.fr/pages/obeo-designer/en">Obeo Designer</a>
    product. Sirius was recently released as an Open Source project
    contributed via the Eclipse Foundation. The technology has also been
    deployed in such fields as insurance, administration, civilian
    nuclear, transportation, and communication.
  </p>

  <p>By releasing and developing Sirius publicly, as a new open source
    project, Thales and Obeo are pursuing two objectives:</p>
  <ul>
    <li>Strengthening the Eclipse Modeling ecosystem by enabling the
      creation of rich modeling workbenches at a very low cost.</li>
    <li>Unleashing the potential collaborations with other partners and
      projects in a healthy and commercially-friendly environment.</li>
  </ul>

  <h2>What can you do with Sirius?</h2>
  <p>A modeling workbench created with Sirius provides a set of Eclipse
    editors that allow the users to create, edit and visualize EMF
    models. There are three main types of editors: diagrams, tables and
    trees. These editors are grouped by concern in order to provide
    viewpoints on the models.</p>
  <h3>Diagrams</h3>
  <p>With Sirius, a diagram displays model elements according to rules
    that were defined by the creator of the modeling workbench. These
    rules define which objects will be displayed (based on their type or
    their properties) and how they will be displayed (which shape,
    label, font, color, etc.). The rules also define the graphical
    relationships between the visible objects. A relationship can be
    displayed with an edge between shapes or with the containment of a
    shape within another shape.</p>

  <p>When the diagram is synchronized with the EMF model, it is
    automatically updated to reflect the modifications that concern the
    displayed objects (creations, suppressions and updates). To allow
    the users to modify the model directly from a diagram, the creator
    of the modeler can add a palette providing tools to create new
    objects or relations. He can also specify the behavior of classic
    editing actions such as drag & drop, direct edit, edge reconnection,
    double-click, etc.</p>

  <p>In addition, to help the users master the potential complexity of
    their models, Sirius provides several mechanisms to focus on
    elements of interest:</p>
  <ul>
    <li><b>Conditional styles:</b> change the graphical rendering of
      objects depending on their properties. For example: changing the
      background color to red when an attribute exceeds a threshold,
      varying the size of the shape according to the value of an
      attribute, etc.</li>
    <li><b>Layers and filters:</b> display or hide elements of the
      diagram according to specific conditions.</li>
    <li><b>Validation and quick-fixes:</b> the model can be validated by
      specific rules. The problems are listed on the <i>Problems View</i>
      and the corresponding objects are graphically marked. For some
      problems, quick-fixes can be suggested to correct the model.</li>
  </ul>

  <p>In the example below, you will find a modeler dedicated to system
    processor assembly. The size of the processors (squares) are related
    to their capacity and their color indicates if they are overloaded
    (in red). The connexions between elements also vary according to the
    capacity and the load.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.3-sirius-diagram.png"
    width="600" id="diagram" alt="" />

  <p>Figure 2 - A sample Sirius diagram</p>

  <h3>Tables and matrices</h3>
  <p>Models can also be edited with tables and matrices.</p>

  <p>A table contains objects in the first column and displays the
    features in the other columns. The graphical style of the cells can
    be configured according to rules defined by the creator of the
    modeling workbench.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.4-sirius-table.png"
    width="600" id="table" alt="" />

  <p>Figure 3 - A sample Sirius table</p>

  <p>The cells can be edited to enter new values and tools can be added
    to create or remove objects (lines).</p>

  <p>A matrix contains objects both in the first column and in the first
    line. The cells show the correlations between the two lists of
    objects.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.5-sirius-matrix.png"
    width="600" id="matrix" alt="" />

  <p>Figure 4 - A sample Sirius matrix</p>
  <h3>Trees</h3>

  <p>The third kind of representation provided by Sirius is the tree. It
    displays objects in a hierarchical way.</p>

  <h3>Viewpoints</h3>

  <p>With Sirius, it becomes very easy for the user to obtain many
    custom representations, each one dedicated to a very specific
    concern. To avoid disturbing the user with useless representations,
    Sirius supports the notion of viewpoint. It is an abstraction that
    provides a specification of a system, limited to a particular set of
    problems.</p>

  <p>For example, the same system can be visualized through a view
    showing the exchange between its components, another view focusing
    on the performance issues and another view related to the safety
    analysis.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-1.6-sirius-viewpoint.png"
    width="600" id="viewpoint" alt="" />

  <p>Figure 5 - Sample Sirius viewpoints</p>

  <p>A viewpoint in Sirius provides a set of representations. By
    activating a viewpoint, the user can create and edit the
    corresponding diagrams, tables and trees.</p>

  <h2>Roadmap</h2>

  <p>The Sirius project is based on a fairly large initial contribution
    from Thales group and Obeo.</p>

  <p>This source code was recently migrated to the Eclipse Foundation
    infrastructure, which includes the renaming of all packages to an
    org.eclipse namespace. This first official release (0.9) will
    provide the ability to define Table, Tree and Diagram Editors and
    will be the mark of a complete migration to the Eclipse.org
    infrastructure: from the source code to the build process and the
    documentation. It was presented at EclipseCon Europe 2013 in
    Ludwigsburg at the end of October.</p>

  <p>The next milestones will be the 1.0 release, part of the Eclipse
    Luna simultaneous release in June. This iteration will focus on ease
    of use when getting started, better tooling to assist you when
    defining your modeler (better completion, validation and error
    reporting), scalability and latency.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/cedric75.jpg"
        alt="cedric brun" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Cédric Brun<br />
        <a target="_blank" href="http://www.obeo.fr/">Obeo</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://model-driven-blogging.blogspot.fr/ ">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/bruncedric ">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/110344710078245747120/posts ">Google
            +</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/Stephane75.jpg"
        alt="stephane bonnet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Stéphane Bonnet<br />
        <a target="_blank" href="http://www.thalesgroup.com/">Thales</a>
      </p>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
