<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>Sirius is based on a runtime that provides Eclipse editors for
    models managed with EMF (Eclipse Modeling Framework).</p>

  <p>The screenshot below shows a modeling workbench for EMF models
    based on a DSL (Domain Specific Language) dedicated to the
    specification of composite processors.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.1-sirius-workbench.png"
    width="600" alt="" />
  <br />
  <br />

  <p>This DSL, uses the notions of System, Power Input and Output,
    Processor, Fan, etc to describe the assembly of processors and data
    sources. It is defined by an Ecore model.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.2.png"
    width="600" alt="" />
  <br />
  <br />

  <p>The Sirius runtime uses a description of the modeling workbench
    which defines the complete structure of the viewpoints, the
    corresponding model editors, their behavior and all the edition and
    navigation tools. This description can be seen as a mapping between
    the DSL concepts (the elements defined by the Ecore model) and the
    graphical workbench (the diagrams, the tables, the tools, etc).</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.3.png"
    width="450" alt="" />

  <p>
    <i>The definition of a Sirius modeling workbench</i>
  </p>

  <p>The workbench description is loosely connected to the structure of
    the model thanks to the queries. They are able to define the diagram
    content even in a way that does not match the model structure. As
    long as an EMF model is in memory, being a UML model or a DSL done
    with Xtext, the runtime operates the same.</p>

  <p>With Sirius, a diagram is defined by nodes or containers
    corresponding to Ecore classes and queries which retrieves the
    instances to display.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.4.png"
    width="450" id="viewpoint" alt="" />
  <br />
  <br />

  <p>For each node, the creator of the modeling workbench has to define
    its graphical style (shape, color, label, etc).</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.5.png"
    width="450" id="label" alt="" />
  <br />
  <br />

  <p>The diagram also specifies the edges between nodes and containers.
    Each edge definition contains queries to identify the source and the
    target objects.</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.6.png"
    width="450" id="graphical_style" alt="" />
  <br />
  <br />

  <p>Like nodes and containers, the edge also defines a graphical style
    (color, thickness, line style, form of ends, etc).</p>

  <img
    src="/community/eclipse_newsletter/2013/november/images/article-3.7.png"
    width="450" alt="" />
  <br />
  <br />

  <p>In addition to the model editors, Sirius provides the notion of
    «Modeling Project», an integration with the Project Explore. It
    handles the synchronization of workspace resources, and in-memory
    instances. It also manages the sharing of command stacks and editing
    domains across editors.</p>

  <p>When supporting specific use cases and requirements, there is
    always a need for customization. Sirius is extensible in many ways,
    notably by providing new kinds of representations, new query
    languages and by being able to call Java code to interact with
    Eclipse or any other system. Extensibility is achieved through the
    use of extension points and, when needed, by reusing existing APIs
    available in the underlying components (e.g. using the GMF runtime
    extension points to adapt a graphical modeler).</p>

  <p>Sirius is making heavy use of many Eclipse Modeling projects,
    notably:</p>
  <ul>
    <li>EMF Core as the basics for every model access and introspection,
      but also EMF Transaction and Validation.</li>
    <li>GMF runtime and GMF notation as the basis of the dynamic
      graphical modeling capability.</li>
    <li>Acceleo and MDT-OCL as the foundation of the query languages
      used to define the mapping between the models and the
      representations.</li>
    <li>EEF (Extended Editing Framework), in order to provide rich
      properties editing.</li>
  </ul>

  <p>Sirius can also be integrated with Xtext to provide complementary
    textual editors. With Xtext, models are loaded and saved using their
    textual syntax, enabling concurrent use of diagram, table and
    textual editors.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/november/images/fred75.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Frédéric Madiot<br />
        <a target="_blank" href="http://www.obeo.fr/">Obeo</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://fmadiot.blogspot.fr/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/fmadiot ">Twitter</a></li>
        <!-- <li><a target="_blank" href="">Google +</a></li> -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-3', 'eclipse.org');
  ga('send', 'pageview');

</script>
