<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
 ?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>
<a href="http://www.eclipse.org/xtend/releasenotes_2_4.html">Xtend 2.4</a> comes with a new powerful feature that allows developers to participate in the translation process of Xtend source code to Java code. The <i>Active Annotations</i> are useful in cases where Java requires you to write a lot of boilerplate manually. For instance, many of the good old design patterns fall into this category. With <i>Active Annotations</i> you no longer need to remember how the <a href="http://en.wikipedia.org/wiki/Visitor_pattern">Visitor</a> or the <a href="http://en.wikipedia.org/wiki/Observer_pattern">Observer</a> pattern should be implemented. In Xtend 2.4 you can implement the expansion of such patterns in a library and let the compiler do the heavy lifting for you. To give you an example, lets have a look at the @Data annotation:</p>
<pre class="prettyprint lang-xtend">
@Data class Address {
  String street
  String zip
  String city
}
</pre>
<p>
The <code class="prettyprint lang-xtend">@Data</code> annotation is part of Xtend and implements best practices for immutability for the annotated class. In particular it
<ul>
  <li>marks all fields as final,
  <li>creates a single constructor,
  <li>implements <code class="prettyprint lang-xtend">hashCode()</code> and <code class="prettyprint lang-xtend">equals()</code> using the fields, and
  <li>creates a readable <code class="prettyprint lang-xtend">toString()</code> implementation.
</ul>
<p>
Though these idioms are often very useful, they may not always be exactly what you want. With <i>Active Annotations</i> you are no longer tight to the provided defaults but can roll out your own implementation which better suits your needs. <i>Active Annotations</i> are just library so they integrate easily with your existing development environment.</p>

<p>In this article we want to give you an overview of how <i>Active Annotations</i> work. As always this is best shown by example, so in the following we will develop our own little <code>Observable</code> annotations, which generates getter and setter methods for annotated fields. The setter implementation in addition notifies any listening <code>PropertyChangeListeners</code>.</p>

<h3>Creating The Project</h3>

<p>
An <i>Active Annotation</i> is declared in its own project which will be processed by the compiler before the clients of the annotations are translated. This allows to use the annotation right within your development environment and implement it side by side with the client code. This clear separation allows to avoid any dependencies of the downstream project to the APIs for the annotation processor and solves a chicken-and-egg problem between the processor and the annotated elements. The annotation is usable without any further ado.
</p>

<p>
To get in the required dependencies for running the compiler tests, you should either create a project using M2E (the official Eclipse plug-in for Maven) or create an Eclipse plug-in project. M2E users can use the maven project wizard and select the archetype. You'll have to add the Xtend's maven repository (<code>http://build.eclipse.org/common/xtend/maven/archetype-catalog.xml</code>), like shown in the following screenshot:
</p>
<img src="/community/eclipse_newsletter/2013/march/images/m2e-archetype-wizard.jpg" alt="M2E Archetype Wizard"/>

<h3>Test First</h3>
<p><i>Active Annotations</i> are executed in your IDE, whenever an annotated Xtend element is compiled. This is impressive, since whenever you change an annotation all clients get recompiled on the fly and you get instant feedback.</p>

<p>However, it doesn't allow you to debug your processor implementation nor to unit-test it. In order to do so you should write a test case.</p>

<p>The test will make use of the compiler, so we need to add a dependency to it. If you have choosen the Maven way
add the following dependency to you <code>pom.xml</code> file.</p>

<pre class="prettyprint lang-xml">
&lt;dependency&gt;
  &lt;groupId&gt;org.eclipse.xtend&lt;/groupId&gt;
  &lt;artifactId&gt;org.eclipse.xtend.standalone&lt;/artifactId&gt;
  &lt;version&gt;2.4.0&lt;/version&gt;
  &lt;scope&gt;test&lt;/scope&gt;
&lt;/dependency&gt;
</pre>

<p>
Note that we only need this dependency for testing, which is why we set the scope element to 'test'. Further more we need some test framework, we use Junit in this example.</p>

<p>If you choose to go with an OSGi bundle, add a require-bundle dependency in your Manifest.MF for the bundle <code>org.eclipse.xtend.standalone</code>.</p>

<p>
Let's start with a simple test case:</p>
<pre class="prettyprint lang-xtend">
class ObservableTests {

  extension XtendCompilerTester compilerTester =
      XtendCompilerTester::newXtendCompilerTester(typeof(Observable))

  @Test def void testObservable() {
    '''
      import org.eclipse.example.activeannotations.Observable

      @Observable
      class Person {
        String name
      }
    '''.assertCompilesTo('''
      import java.beans.PropertyChangeSupport;
      import org.eclipse.example.activeannotations.Observable;

      @Observable
      @SupressWarnings("all")
      public class Person {
        private String name;

        public String getName() {
          return this.name;
        }

        public void setName(final String name) {
          String _oldValue = this.name;
          this.name = name;
          _propertyChangeSupport.firePropertyChange(
            "name", _oldValue, name);
        }

        private PropertyChangeSupport _propertyChangeSupport
            = new PropertyChangeSupport(this);

        // method addPropertyChangeListener
        // method removePropertyChangeListener
      }
    ''')
  }
}
</pre>

<p>
We use the <code class="prettyprint lang-xtend">XtendCompilerTester</code> to validate the code which is emitted by the active annotation and the Xtend compiler. Since it is declared as an extension field it is possible to call its method <code class="prettyprint lang-xtend">assertCompilesTo()</code> as if they were defined on instances of <code class="prettyprint lang-xtend">java.lang.String</code>. This idiom makes the test case easy to read and write. Later-on you may want to explore the other methods that are provided by the compiler tester in order to directly execute the code that is produced from a given Xtend snippet.
</p>

<h3>Implementing The Active Annotation</h3>

<p>Next up we want to fix the failing test. For that matter a new Xtend file called 'src/main/java/org/eclipse/example/activeannotations/Observable.xtend' should be created where we declare two elements: the annotation and its processor:</p>
<pre class="prettyprint lang-xtend">
package org.eclipse.example.activeannotations

@Active(typeof(ObservableCompilationParticipant))
annotation Observable {
}

class ObservableCompilationParticipant
      implements TransformationParticipant&lt;MutableClassDeclaration&gt; {

  override doTransform(List&lt;? extends MutableClassDeclaration&gt; classes,
                       extension TransformationContext context) {
    // TODO add getters, setters, and property support
  }
}
</pre>
<p>Active annotations are annotated with <code class="prettyprint lang-xtend">@Active</code>. Therein a single class implementing <code class="prettyprint lang-xtend">TransformationParticipant</code> must be referenced. We put that class right after the annotation and can now start implementing the method <code class="prettyprint lang-xtend">doTransform()</code> which gets called by the compiler during the translation to Java.</p>

<p>All annotated classes per compilation unit are passed into the processor as the first argument <code class="prettyprint lang-xtend">classes</code>. We simply iterate over the classes and therin over the declared fields in order to generate a getter and a setter method.</p>
<!-- I like that the code mirrors the prose text -->
<pre class="prettyprint lang-xtend">
override doTransform(List&lt;? extends MutableClassDeclaration&gt; classes,
                       extension TransformationContext context) {
  for (clazz : classes) {

    for (f : clazz.declaredFields) {
      val fieldName = f.simpleName
      val fieldType = f.type

      // add the getter method
      clazz.addMethod('get' + fieldName.toFirstUpper) [
        returnType = fieldType
        body = ['''return this.&laquo;fieldName&raquo;;''']
      ]

      // add the setter method
      ...

    }

    ...
  }
}
</pre>
<p>To add a getter method we iterate the declared fields and add a method to the class. The <code class="prettyprint lang-xtend">addMethod()</code> operation takes two arguments, the method's name and an initializer block where we can set further properties like the return type, parameters, modifiers, etc. The method body is set with another block that is executed later when the actual Java code is generated. Here, we can generate Java code directly.</p>

<p>As you can see, the translation is two-fold, the coarse grained constructs such as type declarations and its members are created and modified on a structural basis. It is very similar to a mutable variation of the <code class="prettyprint lang-xtend">java.lang.reflect</code> API. On top of that, the finer grained elements such as statements and expressions are generated using Java's concrete syntax in plain text. Xtend's template expression helps to produce beautiful code.</p>

<p>The setter method is created in a similar way. We only want a setter if the field is not final, so we add a guard for that:</p>
<pre class="prettyprint lang-xtend">
...
for (f : clazz.declaredFields) {
  val fieldName = f.simpleName
  val fieldType = f.type

  // add the getter method
  ...

  // add the setter method if field is not final
  if (!field.isFinal) {
    clazz.addMethod('set' + fieldName.toFirstUpper) [
      addParameter(fieldName, fieldtype)
      body = ['''
        &laquo;fieldType&raquo; _oldValue = this.&laquo;fieldName&raquo;;
        this.&laquo;fieldName&raquo; = &laquo;fieldName&raquo;;
        _propertyChangeSupport.firePropertyChange("&laquo;fieldName&raquo;", _oldValue,
            &laquo;fieldName&raquo;);
      ''']
    ]
  }
]
</pre>
<p>Many of the needed properties and methods are declared on the AST types itself, such as <code class="prettyprint lang-xtend">MutableMethodDeclaration</code> and the like. However some are added through extensions coming from <code class="prettyprint lang-xtend">TransformationContext</code>, which is the second argument to <code class="prettyprint lang-xtend">doTransform()</code>. The context exposes a handy API, for instance it allows to add error or warning messages and get some support for tracing as well as useful factories for creating type references. All these blend naturally into the interfaces that are provided on the AST level and allow to implement very advanced processors that validate project specific constraints or implement specific idioms.</p>
<p>Finally after the the getter and setter methods for the fields have been added we need to declare the
  field holding the listeners:</p>
<pre class="prettyprint lang-xtend">
class ObservableCompilationParticipant implements
                                  TransformationParticipant&lt;MutableClassDeclaration&gt; {

  override doTransform(List&lt;? extends MutableClassDeclaration&gt; classes,
                       extension TransformationContext context) {
    for (clazz : classes) {

      for (f : clazz.declaredFields) {
        val fieldName = f.simpleName
        val fieldType = f.type

        clazz.addMethod('get' + fieldName.toFirstUpper) [
          returnType = fieldType
          body = ['''return this.&laquo;fieldName&raquo;;''']
        ]

        clazz.addMethod('set' + fieldName.toFirstUpper) [
          addParameter(fieldName, fieldType)
          body = ['''
            &laquo;fieldType&raquo; _oldValue = this.&laquo;fieldName&raquo;;
            this.&laquo;fieldName&raquo; = &laquo;fieldName&raquo;;
            _propertyChangeSupport.firePropertyChange("&laquo;fieldName&raquo;", _oldValue,
              &laquo;fieldName&raquo;);
          ''']
        ]
      }

      val changeSupportType = typeof(PropertyChangeSupport).newTypeReference
      clazz.addField("_propertyChangeSupport") [
        type = changeSupportType
        initializer = ['''new &laquo;toJavaCode(changeSupportType)&raquo;(this)''']
      ]
      // add method 'addPropertyChangeListener'
      // add method 'removePropertyChangeListener'
    }
  }
}
</pre>
<p>Now you only have to add two additional methods for adding and removing listeners. We leave this as an excercise for the reader. You might also want to check if a getter or setter method has been explicitly implemented already. Users should be able to change the getter or setter semantic by explicitly implementing one or the other.</p>

<h3>Wrap Up</h3>

<p><i>Active Annotations</i> are a powerful new concept which lets you solve a large class of problems that previously had to be solved in cumbersome ways, e.g. by IDE wizards, with code generation or simply by manually writing boilerplate code. It basically <b>is</b> a means of code generation, but its simple integration with your existing project structure and the fast development turnarounds diminish the typical downsides of code generation. <b>Note, that in version 2.4 the <i>Active Annotation</i>-API is provisional, and might be changed in later releases.</b></p>
<script src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js" type="text/javascript"></script>
<script src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js" type="text/javascript"></script>
<script type="text/javascript">
  prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture" src="/community/eclipse_newsletter/2013/march/images/sven-headshot1.jpeg" alt="Sven Efftinge" />

        </div>
        <div class="col-sm-16">
          <p class="author-name">Sven Efftinge <br/><a href="http://www.itemis.com/">itemis</a></p>
        <ul class="author-link">
          <li><a href="http://blog.efftinge.de/">Blog</a></li>
          <li><a href="https://twitter.com/svenefftinge">Twitter</a></li>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

