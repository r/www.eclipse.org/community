<?php
//if name of the file requested is the same as the current file, the script will exit directly.
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}

$settings = array();
$settings['url'] =  "2013/february/";
$settings['title'] = 'February 2013';
$settings['slogan'] = 'Transitioning to Eclipse 4';
$settings['teaser'] = 'Use this area to offer a short teaser of your email\'s content. Text here will show in the preview area of some email clients.';

$settings['articles'] = array();
$settings['articles'][] = array(
		'title' => 'Our Very First Eclipse Newsletter',
		'url' => 'http://eclipse.org/community/eclipse_newsletter/index.php',
		'author' => 'by Roxanne Joncas',
		'teaser' => 'Welcome to the very first issue of the Eclipse Newsletter. We have over 10 000 people signed up for the newsletter so we are very excited about the interest.We plan to publish a monthly newsletter that will hopefully be informative about what is happening in the Eclipse community and a technical resource about Eclipse technology.<br><br>Each newsletter will have a theme that highlights a certain Eclipse technology. This month the theme is <i>Transitioning to Eclipse 4</i>.We want to make this a community newsletter so we’d love to hear what you have to say. Please send me your feedback and suggestions.<br><br>Roxanne<br>Eclipse Newsletter Editor<br>newsletter.editor@eclipse.org',
);

$settings['articles'][] = array(
		'title' => 'Migrating from Eclipse 3.x to Eclipse 4',
		'url' => 'http://www.eclipse.org/community/eclipse_newsletter/2013/february/article1.php',
		'author' => 'by Jonas Helming',
		'teaser' => 'Eclipse Juno 4.2 has been the first Release Train building on the new Eclipse 4 (e4) Application Platform. This raises the question of how to migrate existing Eclipse 3.x applications to Eclipse 4',		
);

$settings['articles'][] = array(
		'title' => 'Eclipse 4 Tutorial – Soft migration from 3.x to Eclipse 4',
		'url' => 'http://www.eclipse.org/community/eclipse_newsletter/2013/february/article2.php',
		'author' => 'by Jonas Helming and Maximilian Koegel',
		'teaser' => 'This tutorial describes how to do a soft migration to the Eclipse 4 (e4) programming model. The basic goal of the tutorial is to enable development using the new concepts such as Dependency Injection and Annotations,',
);

$settings['articles'][] = array(
		'title' => 'Migrating Eclipse 3.x plug-ins and RCP applications to Eclipse 4 - Tutorial',
		'url' => 'http://www.eclipse.org/community/eclipse_newsletter/2013/february/article3.php',
		'author' => 'by Lars Vogel',
		'teaser' => 'This tutorial gives an overview on how to use the Compatibility Layer in Eclipse 4 to run plug-ins based on the Eclipse 3.x API.',
);
$settings['links'] = array();
$settings['links'][] = array(
		'title' => 'Eclipse 4 FAQ',
		'url' => 'http://wiki.eclipse.org/Eclipse4/Eclipse_SDK_4.0_FAQ'		
);
$settings['links'][] = array(
		'title' => 'Eclipse 4.2 Release Notes',
		'url' => 'http://www.eclipse.org/eclipse/development/readme_eclipse_4.2.php'
);
$settings['links'][] = array(
		'title' => 'Eclipse 4 Discussion Forum',
		'url' => 'http://www.eclipse.org/forums/index.php/f/12/'
);
$settings['spotlight'] = array();
$settings['spotlight'][] = array(
		'name' => 'Markus Knauer',
		'image' => 'http://eclipse.org/community/eclipse_newsletter/2013/february/images/user-picture-markus.jpg',
		'position' => 'Senior Eclipse Consultant at <a href="http://www.eclipsesource.com/" target="_blank" style="color:#333 !important;">EclipseSource</a>',
		'content' => array(						
						array(
							'question' => 'How long have you been using Eclipse',
							'answer' => 'More than 10 years. I started using the Java Development Tools (JDT) and got used to CDT soon after.',
						),
						array(
							'question' => '5 Eclipse Plugins you use and think other developers should use',
							'answer' => '<a href="https://marketplace.eclipse.org/content/wireframesketcher" target="_blank">WireframeSketcher</a>, 
								<a href="http://marketplace.eclipse.org/content/eclemma-java-code-coverage" target="_blank">EclEmma</a>, 
								<a href="http://marketplace.eclipse.org/content/checkstyle-plug" target="_blank">Checkstyle</a>, 
								<a href="http://marketplace.eclipse.org/content/findbugs-eclipse-plugin" target="_blank">FindBugs</a>, 
								<a href="http://www.eclipse.org/mat/" target="_blank">Memory Analyzer</a>, 
								<a href="http://www.eclipse.org/egit/" target="_blank">EGit</a> & 
								<a href="http://www.eclipse.org/jdt/" target="_blank">JDT</a>.'
						),
						array(
							'question' => 'Favorite thing to do when not developing software',
							'answer' => 'The obvious... flying small aircrafts.',
						),
				),
		'footer' => '<a href="mailto:newsletter.editor@eclipse.org" class="spotlight">Would you like to be a future spotlight?</a>'
);

$settings['news'] = array();
$settings['news'][] = array(
		'title' => 'Eclipse RAP Project Release 2.0 Now Supports Native Client Platform',
		'date' => 'posted Feb 15, 2013',
		'content' => 'A new major release of the Eclipse Remote Application Platform (RAP) is now available for download.',
		'url' => 'http://eclipse.org/org/press-release/20130215_RAP2.php'
);

$settings['news'][] = array(
		'title' => 'Eclipse Foundation Enables LocationTech',
		'date' => 'posted Feb 5, 2013',
		'content' => 'The Eclipse Foundation has launched a new industry working group to support user driven development of location aware systems. Read the press release to find out more.',
		'url' => 'http://www.eclipse.org/org/press-release/20130205_ef_enables_locationtech.php'
		
);

$settings['news'][] = array(
		'title' => 'Eclipse Foundation Announces Release of Hudson 3.0',
		'date' => 'posted Dec 10, 2013',
		'content' => 'Hudson 3.0 is now available, including new features to simplify installation and management.',
		'url' => 'http://eclipse.org/org/press-release/20130123_hudson3.php'
);
$settings['ads'] = array();
$settings['ads'][] = array(
		'url' => 'http://www.eclipsecon.org/2013/',
		'image' => 'http://www.eclipse.org/community/eclipse_newsletter/template/images/ad-example-1.jpg',
		'alt' => 'Register Now for EclipseCon Boston 2013'	
);
/*$settings['ads'][] = array(
		'url' => 'http://www.eclipsecon.org/2013/',
		'image' => 'http://www.eclipse.org/community/eclipse_newsletter/template/images/ad-example-2.jpg',
		'alt' => 'Get Up and Running Faster with EclipseSource'
);*/

$settings['release'] = array();
$settings['release']['more_url'] = 'http://www.eclipse.org/projects/';
$settings['release']['content'][] = array(
		'title' => '3P - PolarSys Packaging Project2',
		'url' => 'http://polarsys.org/polarsys-packaging-project-starting-blocks'
);
$settings['release']['content'][] = array(
		'title' => 'secRT2',
		'url' => 'http://www.eclipse.org/proposals/secrt/'
);
$settings['release']['content'][] = array(
		'title' => 'UML2 Profiles2',
		'url' => 'http://www.eclipse.org/proposals/modeling.uml2profiles/'
);


$settings['projects'] = array();
$settings['projects']['more_url'] = 'http://www.eclipse.org/projects/';
$settings['projects']['content'][] = array(
		'title' => 'EMF Component',
		'url' => 'http://www.eclipse.org/proposals/modeling.emf-components/'
);
$settings['projects']['content'][] = array(
		'title' => 'e(fx)clipse - JavaFX tooling and runtime for Eclipse',
		'url' => 'http://eclipse.org/proposals/technology.efxclipse/'
);
$settings['projects']['content'][] = array(
		'title' => 'Eclipse Bundle Recipes',
		'url' => 'http://eclipse.org/proposals/rt.ebr/'
);

$settings['events'] = array();
$settings['events'][] = array(
		'title' => 'Eclipse Open Source Executive Summit<br/>March 26, 2013',
		'url' => 'http://eclipseossummit.eventbrite.com/'
);

$settings['events'][] = array(
		'title' => 'ALM Connect Executive Day<br/>March 27, 2013',
		'url' => 'http://almexecutiveday.eventbrite.com/'
);

