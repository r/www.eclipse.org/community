<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    While BIRT offers many ways to connect to Cassandra, including using
    the Cassandra JDBC driver, this post focuses on using a Scripted
    data source to call the Hector Client Java client. A BIRT scripted
    data source allows external Java classed to be called to retrieve
    data for a BIRT report and can be written in Java or JavaScript. The
    examples below will use JavaScript. For this post we used the
    DataStax community edition which is available <a target="_blank"
      href="http://planetcassandra.org/Download/DataStaxCommunityEdition">here</a>,
    and created a keyspace with the name users and a column family named
    User. The User column family contains three string columns for first
    name, last name and age. The script used to load the sample data is
    available in the example download.
  </p>
  <h4>Set Designer Classpath</h4>
  <p>The first thing that you will need to do is set the classpath for
    the designer to access the following set of jars.</p>
  <ul>
    <li>hector-core-version.jar</li>
    <li>hector-object-mapper-version.jar</li>
    <li>slf4j-api-version.jar</li>
    <li>libthrift-version.jar</li>
    <li>apache-cassandra-thrift-version.jar</li>
    <li>guava-rversion.jar</li>
    <li>commons-lang-version.jar</li>
  </ul>

  <p>All of these jars, with the exception of the two Hector jars are
    available in the /install-directory/DataStax
    Community/apache-cassandra/lib directory. To get the Hector jars you
    can download and build the hector source or just download them from
    a maven repository.</p>

  <p>
    The Hector-object-mapper jar file can be downloaded from <a
      target="_blank"
      href="http://search.maven.org/#search|ga|1|hector-object-mapper">here</a>.
  </p>
  <p>
    The Hector core jar file can be downloaded from <a target="_blank"
      href="http://search.maven.org/#search|ga|1|hector-core">here</a>.
  </p>

  <p>One way to setup the classpath is to create a libs directory in
    your Report Project and then copy all of the jars above to this
    folder.</p>

  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_1(2).jpg"
    alt="Libs Directory" />
  <br />
  <br />

  <p>Next Select Window->Preferences. Select the Report
    Design->Classpath preference and click on the Configure project
    specific settings link.</p>

  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_2(2).jpg"
    alt="Configure Settings" />
  <br />
  <br />

  <p>Select the BIRT Project that you will be using Hector with and
    click on ok.</p>

  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_3(2).jpg"
    alt="Select Project" />
  <br />
  <br />

  <p>Select the enable project specific settings checkbox and add the
    jars in the lib folder you created earlier.</p>

  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_4(2).jpg"
    alt="Classpath" />
  <br />
  <br />

  <h4>Creating a Scripted Data Source using Hector</h4>
  <p>You can now create a report that calls the Hector APIs directly. To
    do this first create a new report. Select the data explorer view and
    right click on the data sources node and click on New Data Source.
    Select the Scripted Data Source option and click on finish.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_5(2).jpg"
    alt="Data Source" />
  <br />
  <br />

  <p>Next right click on the Data Sets node and choose the New Data Set
    option. Make sure to select the Scripted Data Source that you just
    created as the data source for this data set.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_6(2).jpg"
    alt="New Data Set" />
  <br />
  <br />

  <p>Click on the Next button and enter each column name and data type
    for the data set.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_7(2).jpg"
    alt="Output Columns" />
  <br />
  <br />

  <p>Click on the Finish button. You can now enter script for the data
    set. To do this first make sure the data set is selected in data
    explorer view and click on the script tab at the bottom of the
    report canvas.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_8(2).jpg"
    alt="Script Tab" />
  <br />
  <br />

  <p>In the script editor you will have many events that could be
    scripted, but in this example all we need is an open script and a
    fetch script. First select open from the script drop down list and
    enter a script similar to the following.</p>

  <pre class="prettyprint lang-xtend">
importPackage(Packages.java.util);

importPackage(Packages.me.prettyprint.cassandra.serializers);

importPackage(Packages.me.prettyprint.cassandra.service);

importPackage(Packages.me.prettyprint.hector.api);

importPackage(Packages.me.prettyprint.hector.api.beans);

importPackage(Packages.me.prettyprint.hector.api.factory);

importPackage(Packages.me.prettyprint.hector.api.query);



var cluster = HFactory.getOrCreateCluster("Test Cluster",
    new CassandraHostConfigurator("localhost:9160"));

var keyspace = HFactory.createKeyspace("users", cluster);

var rangeSlicesQuery = HFactory.createRangeSlicesQuery(keyspace,
    StringSerializer.get(), StringSerializer.get(), StringSerializer.get())

.setColumnFamily("User").setRange(null, null, false, 10).setRowCount(100);

var result = rangeSlicesQuery.execute();

myrows = result.get();

rowsIterator = myrows.iterator();
</pre>
  <br />

  <p>Hector also supports using CQL so you could also use the following
    open script</p>

  <pre class="prettyprint lang-xtend">
importPackage(Packages.java.util);

importPackage(Packages.me.prettyprint.cassandra.serializers);

importPackage(Packages.me.prettyprint.cassandra.service);

importPackage(Packages.me.prettyprint.hector.api);

importPackage(Packages.me.prettyprint.hector.api.beans);

importPackage(Packages.me.prettyprint.hector.api.factory);

importPackage(Packages.me.prettyprint.hector.api.query);

importPackage(Packages.me.prettyprint.cassandra.model);



var cluster = HFactory.getOrCreateCluster("Test Cluster",
    new CassandraHostConfigurator("localhost:9160"));

var keyspace = HFactory.createKeyspace("users", cluster);



var cqlQuery = new CqlQuery(keyspace, StringSerializer.get(),
    StringSerializer.get(), StringSerializer.get());

cqlQuery.setQuery("select * from User");

var resultCQL = cqlQuery.execute();

rowsIterator = resultCQL.get().iterator();

Next add a fetch script like the following.

if (rowsIterator.hasNext()) {

     var myrow = rowsIterator.next();

     var cols = myrow.getColumnSlice().getColumns();

     for( ii=0; ii &lt; cols.size(); ii++ ){

       row[cols.get(ii).getName()] = cols.get(ii).getValue();

     }

        return true;

}else{

       return false;

}
</pre>
  <br />

  <p>In the above fetch the script assumes you have named your scripted
    data set columns the same as the columns in Cassandra. You should
    now be able to preview the data set. Double click on the data set in
    the data explorer view and select preview.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_9(2).jpg"
    alt="Preview Data Set" />
  <br />
  <br />

  <p>You can now use the data set within your report.</p>
  <img
    src="/community/eclipse_newsletter/2013/april/images/article2_10(2).jpg"
    alt="Report Preview" />
  <br />
  <br />

  <h4>Deploying a Report that Uses the Hector API</h4>
  <p>If you are using the BIRT Viewer and deploy a report that calls the
    Hector API, verify that all the jars discussed in the beginning of
    this Post (Set Designer Classpath) are placed in WEB-INF/lib
    directory of the Viewer. If you are running BIRT reports using the
    BIRT APIs verify that the above jars are also in the classpath.</p>

  <p>
    More information on CQL and Hector is available <a target="_blank"
      href="http://hector-client.github.io/hector/build/html/documentation.html">here</a>.
    The example in this post is available on <a target="_blank"
      href="http://www.birt-exchange.org/org/devshare/designing-birt-reports/1539-birt-cassandra-and-hector/">Birt-Exchange</a>.
  </p>


  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
   prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/april/images/jasonweathersby1.jpg"
        alt="Jason Weathersby" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jason Weathersby <br />
        <a target="_blank" href="http://www.actuate.com/home/">Actuate</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="<?php echo $original_url; ?>">Original
            Article</a></li>
        <!--<li><a href="http://nataliaossipova.wordpress.com/">Blog</a> </li>
          <li><a href="https://twitter.com/nossipova">Twitter</a></li>
          <li><a href="http://www.linkedin.com/in/nataliaossipova">LinkedIn</a></li>-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

