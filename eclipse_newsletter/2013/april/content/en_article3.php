<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <div class="article">
    <p>Soon after the MongoDB noSQL-style database system started
      becoming popular, the BIRT community started asking for ways to
      get at that data. The good news for developers using BIRT, is that
      BIRT was designed with data source extensibility in mind. There
      are several ways to connect to MongoDB as a data source within
      BIRT. This post will show how to connect to MongoDB with your
      existing BIRT version, plus several screenshots from the new
      MongoDB ODA driver being added to the next major release of BIRT
      due out this summer.</p>

    <h4>BIRT Scripted Data Source Connection to MongoDB</h4>
    <p>The BIRT Scripted Data Source is like the ‘silver-bullet’ into
      any data source. If you cannot connect to your data with one of
      the built-in BIRT data sources, then the Scripted Data Source may
      be the answer. It uses the server-side RHINO Javascript engine
      allowing you to write a bit of Java or Javascript as a simple
      wrapper to your data. As long as you can get to your data in your
      application, then it’s likely the Scripted Data Source can get
      that data into BIRT.</p>

    <p>To create a connection to MongoDB, you first need to add the
      MongoDB JAR to your project classpath. Now, you just add a bit of
      Javascript to the following methods to make it work.</p>

    <b>Create connection and query in the <u>open</u> method
    </b><br />
    <pre class="prettyprint lang-xtend">
importPackage(Packages.java.lang);
importPackage(Packages.com.mongodb);
importPackage(Packages.java.util);
importPackage(Packages.org.bson);

m = new Mongo( "localhost" , 27017 );
db = m.getDB( "scratch" );
col = db.getCollection("zips");

query = new BasicDBObject();
query.put("state", "AL");
cursor = col.find(query);
</pre>
    <br /> <b>The <u>fetch</u> method will get called repeatedly while
      you return true
    </b><br />
    <pre class="prettyprint lang-xtend">
if( cursor.hasNext() ){
var jsonStr = new String(cursor.next());
row["city"] = jsonStr.split(",")[1].split(":")[1].replace("\"", "");
row["zip"] = jsonStr.split(",")[2].split(":")[1].replace("\"", "");
row["state"] = jsonStr.split(",")[6].split(":")[1].replace("\"", "").replace("}","");
return true;
}else{
return false;
}
</pre>
    <br /> <b>The <u>close</u> method is used to discard objects
    </b>
    <pre class="prettyprint lang-xtend">
cursor.close();
</pre>
    <br />
    <h4>Community Created BIRT Plug-Ins for MongoDB</h4>
    <p>Part of BIRT’s extensibility allows for external plug-ins to be
      dropped into existing versions of BIRT. There have been several
      community developed BIRT plug-ins to access MongoDB data created
      over the years. At the time of this post, a quick search in the
      BIRT Exchange DevShare shows 3 different MongoDB ODA Driver
      plug-ins to choose from.</p>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_1.jpg"
      alt="Different Plug-ins" /><br />
    <br />

    <h4>New MongoDB ODA Driver for BIRT</h4>
    <p>The next major release of BIRT will include a new data driver for
      accessing MongoDB. This means, instead of creating your own
      connection to MongoDB in a scripted data set, you can now simply
      select MongoDB from a list of available choices.</p>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_2.jpg"
      alt="Select MongoDB" /><br />
    <br />

    <p>Next, fill out the database credentials and connection
      information. This information can be hard-coded in each specific
      field or the connection information can be dynamically generated
      at runtime using BIRT’s Property Binding feature.</p>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_3.jpg"
      alt="Database Credentials" /><br />
    <br />

    <p>Once you have created a connection, you can now create a new Data
      Set to query for specific data. BIRT supports the MongoDB
      operations that return data. This includes the Query commands, the
      Aggregate command, MapReduce, and select Database Commands that
      return data.</p>

    <h4>Creating a MongoDB Data Set in BIRT</h4>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_4.jpg"
      alt="Creating DataSet" /><br />
    <br />

    <p>The new MongoDB Data Set dialog allows you to choose the data
      Collection and fields you want returned from the query. Java
      Objects can be flatted out to bring in just the data elements, or
      the entire object can be brought back for further processing.
      Select Finish and we can now preview the results of the MongoDB
      query.</p>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_5.jpg"
      alt="Preview Results" /><br />
    <br />

    <h4>Using a MongoDB MapReduce Command in BIRT</h4>
    <img src="/community/eclipse_newsletter/2013/april/images/article3_6.jpg" alt="MapReduce Command" /><br />
    <br />

    <h4>Using a MongoDB Aggregate Command in BIRT</h4>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_7.jpg"
      alt="Aggregate Command" /><br />
    <br />

    <h4>Using a MongoDB Database Command in BIRT</h4>
    <img
      src="/community/eclipse_newsletter/2013/april/images/article3_8.jpg"
      alt="Database Command" /><br />
    <br />

    <p>BIRT supports a subset of the MongoDB Database Commands that
      return data. The following is a list of the supported database
      commands from within BIRT.


    <p>buildInfo, collStats, connPoolStats, count, cursorInfo, dataSize,
      dbStats, distinct, eval with nolock: true geoNear, geoSearch,
      getLastError, getLog, getPrevError, group, isMaster, isdbgrid,
      listCommands, listDatabases, listShards, ping,
      printShardingStatus, replSetGetStatus, serverStatus</p>

    <h4>Summary</h4>
    <p>There are several ways to connect to MongoDB no matter what
      version you are using. The new MongoDB ODA driver will make it’s
      first appearance in BIRT version 4.3 Milestone 7 due out soon.</p>




    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
      type="text/javascript"></script>
    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
      type="text/javascript"></script>
    <script type="text/javascript">
   prettyPrint();
</script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
      src="/community/eclipse_newsletter/2013/april/images/virgildodson1.jpg"
      alt="Virgil Dodson" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
      Virgil Dodson <br />
      <a target="_blank" href="http://www.actuate.com/home/">Actuate</a>
    </p>
    <ul class="author-link">
      <li><a target="_blank" href="<?php echo $original_url; ?>">Original
          Article</a></li>
      <!--<li><a href="http://koehnlein.blogspot.ca/">Blog</a> </li>
					<li><a href="https://twitter.com/jankoehnlein">Twitter</a></li>
					<li><a href="https://plus.google.com/116635061773096754601">Google+</a></li>-->
    </ul>
        </div>
      </div>
    </div>
  </div>
</div>
