<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		//include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
	include_once($_SERVER['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/assets/newsletter-parser.php");
	# Begin: page-specific settings.  Change these.
	$pageTitle 		= $title;
	$pageKeywords	= "eclipse, newsletter," . $title;
	$pageAuthor		= "Christopher Guindon";
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	
	<div id="fullcolumn">
		<div id="newsletter-container">
				$body
		</div>
		
	</div>

EOHTML;

	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/eclipse_reset.css" media="screen" />');
	
	if(!empty($css)){
		$App->AddExtraHtmlHeader('<style>' . $css . '</style>');
	}
	//$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/eclipse_reset.css" media="screen" />');
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>