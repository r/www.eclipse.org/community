<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    In previous releases, the <a target="_blank"
      href="http://www.eclipse.org/ecf/">ECF project</a> has provided
    implementations of both <a target="_blank"
      href="http://www.osgi.org/Main/HomePage">OSGi</a> Remote Services
    and <a target="_blank"
      href="http://www.osgi.org/Specifications/HomePage">OSGi Remote
      Services Admin</a>. For ECF's Kepler release, the project
    committers have now fully tested both of these implementations
    against the OSGi Release 5 Test Compatibility Kit (TCK)...and we are
    happy to report that our <a target="_blank"
      href="http://www.eclipse.org/ecf/NewAndNoteworthy.html">Kepler
      release</a> implementation is passing all tests.
  </p>

  <h3>Standards Compliance</h3>
  <p>TCK testing was done to assure the community that our
    implementation of RS/RSA is fully compatible with these two
    enterprise specifications (Chapters 100 and 122, repectively). Why
    is standards compliance helpful to our community? The primary reason
    is that compatibility allows SOA developers to develop and use high
    level remote services that are independent of the underlying network
    transport. Not only does this prevent commercial lock-in, but
    developers and commercial adopters using ECF's implementation of
    OSGi Remote Services can also be assured that they can:</p>
  <ol>
    <li>Develop/test their proprietary remote services on one
      implementation of RS/RSA, and if necessary...deploy/run on others</li>
    <li>If desired, use other compliant RS/RSA implementations
      alongside/with ECF's implementation</li>
    <li>Create remote services that are interoperable with
      existing/legacy services</li>
    <li>Extend or enhance ECF's open source implementation without
      restrictions</li>
  </ol>

  <h3>What are OSGi Remote Services?</h3>
  <p>
    OSGi Remote Services specifies a very simple idea: Start with a <b>local</b>
    OSGi service, and <b>export</b> it for access outside the hosting
    process. There are a number of excellent articles about defining and
    using local OSGi services...for example <a target="_blank"
      href="http://www.vogella.com/articles/OSGiServices/article.html">OSGi
      Services - Tutorial</a> by Lars Vogel and <a target="_blank"
      href="http://www.knopflerfish.org/osgi_service_tutorial.html">OSGi
      Services Tutorial</a> by Knopflerfish...so we will not describe
    OSGi services in detail here. However, for the remainder of this
    article, we will go through a hands-on example of how to create,
    publish, and use an example OSGi Remote Service. It should be said,
    however, that an important aspect of the OSGi Remote Services
    specification is it's tight integration with the OSGi service
    registry, meaning that any/all of the mechanisms described in the <a
      target="_blank"
      href="http://www.vogella.com/articles/OSGiServices/article.html">tutorial</a>
    by Lars Vogel for example, Declarative Services...also work with
    Remote Services.
  </p>

  <h3>The Service Interface</h3>
  <p>OSGi Services are defined by 1 or more service interfaces. For
    example, here is a very simple interface to retrieve the current
    time</p>

  <pre class="prettyprint lang-xtend">
public interface ITimeService {
  /**
   * Get current time.
   *
   * @return Long current time in milliseconds since Jan 1, 1970. Will not
   *         return <code>null</code>.
   */
  public Long getCurrentTime();

}
</pre>
  <br />

  <p>
    For the consumer/user of the <b>ITimeService</b>, access to this
    interface class is <b>all that is needed</b> to use this remote
    service. For example, here is a declarative services (DS) component
    that uses the <b>ITimeService</b> to call getCurrentTime() and print
    out the result to stdout:
  </p>

  <pre class="prettyprint lang-xtend">
import com.mycorp.examples.timeservice.ITimeService;

public class TimeServiceComponent {

  void bindTimeService(ITimeService timeService) {
    System.out.println("Discovered ITimeService via DS");
    // Call the service and print out result!
    System.out.println("Current time is: " + timeService.getCurrentTime());
  }
}
</pre>
  <br />

  <p>Note that</p>
  <ol>
    <li>The bindTimeService method is called automatically by DS</li>
    <li>This implementation simply calls getCurrentTime and shows the
      result</li>
  </ol>
  <p>
    And for the <b>ITimeService</b> consumer...that's it for code.
    Everything about the remote service discovery, proxy creation,
    connection setup, parameter and result marshaling...anything having
    to do with the actual <b>transport-level</b> or <b>distribution</b>
    activities that make the remote call, are hidden from the consumer
    of this remote service. The <b>ITimeService</b> interface specifies
    the semantic <b>contract</b> between the <b>ITimeService</b> host
    and the consumer.
  </p>

  <h2>Service Implementation</h2>
  <p>
    Here is a trivial implementation of the <b>ITimeService</b> host
    (aka server)
  </p>

  <pre class="prettyprint lang-xtend">
import com.mycorp.examples.timeservice.ITimeService;

public class TimeServiceImpl implements ITimeService {

  public Long getCurrentTime() {
    System.out.println("TimeServiceImpl.  Received call to getCurrentTime()");
    return new Long(System.currentTimeMillis());
  }
}
</pre>
  <br />
  <p>As per the OSGi Remote Services specification, the way that this
    service implementation is exported/published is to register it as an
    OSGi service, and add the specification of standard service
    properties. For example, in the code example, here is how this
    remote service is exported</p>

  <pre class="prettyprint lang-xtend">
1: Dictionary&lt;String , Object&gt; props = new Hashtable&lt;String , Object&gt;();
2: props.put("service.exported.interfaces", "*");
3: props.put("service.exported.configs","ecf.generic.server");
4: props.put("ecf.generic.server.id","ecftcp://localhost:3288/server");
5: context.registerService(ITimeService.class, new TimeServiceImpl(), props);


  </pre>
  <br />

  <p>What's being done here is that</p>
  <ol>
    <li>Three service properties are put into a new Hashtable
    <ol>
      <li><b>service.imported.interfaces</b> set to '*' (line 2). <b>service.imported.interfaces</b>
        is a standard service property that is required to be set for
        OSGi services that are to be exported by a remote services
        implementation.</li>
      <li><b>service.exported.configs</b> set to <b>ecf.generic.server</b>
        (line 3). <b>service.exported.configs</b> is a standard optional
        service property that is used to specify the desired remote
        service distribution implementation. In the case of the example,
        <b>ecf.generic.server</b> identifies the ECF Generic Provider,
        which is one of several distribution providers that exist in
        ECF.</li>
      <li><b>ecf.generic.server.id</b> set to <b>ecftcp://localhost:3288/server</b>
        (line 4). <b>ecf.generic.server.id</b> is a service property
        specific to the <b>ecf.generic.server</b> distribution
        provider...to specify the generic server's identity.</li>
    </ol></li>
    <li>A new TimeServiceImpl() instance is created and then registered
      and exported via the OSGi service registry (line 5). This is the
      call that actually results in the exporting of this as a remote
      service. If ECF remote services is available (installed and
      started), the synchronous registerService call will result in
      ECF's implementation publishing the remote service metadata
      necessary for consumers to discover and use this remote service.
      As per the specification, this is done during the call to
      registerService by ECF's implementation, so that when the
      registerService call returns, the <b>ITimeService</b> is
      accessible for remote access by consumers as described above. Note
      that service registration can also be done via declarative
      services, or by any other means that results in registration via
      the OSGi service registry. It's not required that
      BundleContext.registerService be called directly.
    </li>
  </ol>

  <h3>Running the Example in Eclipse</h3>
  <ol>
    <li><a target="_blank"
      href="http://www.eclipse.org/ecf/downloads.php">Install</a> ECF
      SDK into Eclipse 3.8 or newer</li>
    <li>Retrieve the source for examples by cloning the <a
      target="_blank"
      href="http://git.eclipse.org/c/ecf/org.eclipse.ecf.git">ECF
        Eclipse Foundation Git repo</a>. The following example projects
      should be imported into the local workspace

    <ul>
      <li>com.mycorp.examples.timeservice</li>
      <li>com.mycorp.examples.timeservice.consumer</li>
      <li>com.mycorp.examples.timeservice.consumer.ds</li>
      <li>com.mycorp.examples.timeservice.consumer.filediscovery</li>
      <li>com.mycorp.examples.timeservice.host</li>
    </ul></li>
  </ol>
  <p>
    Once these projects are imported into the workspace, you can examine
    the source code for the above. The <b>ITimeService</b> <i><b>interface is
        declared in the</b> com.mycorp.examples.timeservice <b>bundle.
      The DS consumer</b></i> <b>('TimeServiceComponent
    </b> class) is in <b>com.mycorp.examples.timeservice.consumer.ds</b>,
    and the DS component metadata is in <b>OSGI-INF/timeservicecomponent.xml</b>.
    The <b>TimeServiceImpl</b> class and the remote service registration
    code are in the <b>com.mycorp.examples.timeservice.host</b> bundle.
    The <b>com.mycorp.examples.timeservice.host.Activator.start</b>
    method has the remote service host registration.
  </p>

  <h3>Launching the Time Service Host</h3>
  <p>
    Run/Debug the <b>TimeServiceHost</b> launch configuration to start
    the service host. In the Console window, you should see output
    similar to the following:
  </p>

  <pre class="prettyprint lang-xtend">
MyTimeService host registered
with registration={com.mycorp.examples.timeservice.ITimeService}=
{ecf.generic.server.id=ecftcp://localhost:3288/server,
service.exported.configs=ecf.generic.server, service.exported.interfaces=*, service.id=32}
osgi> Service Exported by RemoteServiceAdmin.
EndpointDescription Properties={ecf.endpoint.id.ns=org.eclipse.ecf.core.identity.StringID,
ecf.generic.server.id=ecftcp://localhost:3288/server,
endpoint.framework.uuid=403cb17a-8bf1-0012-10cc-84fedd6ed32e,
endpoint.id=ecftcp://localhost:3288/server,
endpoint.package.version.com.mycorp.examples.timeservice=1.0.0, endpoint.service.id=1,
objectClass=[Ljava.lang.String;@78d25faa,
remote.configs.supported=[Ljava.lang.String;@57f221b6, remote.intents.supported=
[Ljava.lang.String;@598360d5, service.id=32, service.imported=true,
service.imported.configs=[Ljava.lang.String;@57f221b6}
</pre>
  <br />
  <p>
    This means that the service host is running...with the
    id=ecftcp://localhost:3288/server...and that the <b>ITimeService</b>
    has been exported. It also reports the <b>EndpointDescription</b>...this
    is the OSGi Remote Services metadata that are published for
    discovery as specified by the <a target="_blank"
      href="http://www.osgi.org/Specifications/HomePage">OSGi Remote
      Service Admin</a> specification.
  </p>

  <h3>Launching the TimeService Consumer</h3>
  <p>
    Run/Debug the <b>TimeServiceConsumerDS</b> launch configuration (in
    com.mmycorp.examples.timeservice.consumer.filediscovery/launch) to
    start the service consumer. The consumer should start, and the
    Console window should show the OSGi prompt only...i.e.


  <pre class="prettyprint lang-xtend">
osgi>
</pre>
  <br />
  <p>At the OSGi prompt, to initiate the discovery of the remote service
    type</p>


  <p>
    osgi> <b>start
      com.mycorp.examples.timeservice.consumer.filediscovery</b>
  </p>

  <p>as a result, you should then see</p>

  <pre class="prettyprint lang-xtend">
osgi> start com.mycorp.examples.timeservice.consumer.filediscovery
osgi> Discovered ITimeService via DS
Current time is: 1374360266319
</pre>
  <br />
  <p>With the time value returned from the ITimeService.getCurrentTime()
    remote call. If you bring the host console back up, you should see
    output at the bottom like this</p>

  <pre class="prettyprint lang-xtend">
TimeServiceImpl.  Received call to getCurrentTime()
</pre>
  <br />

  <p>This indicates that the host did indeed get the consumer's remote
    call to getCurrentTime().</p>

  <h3>Implementation Details</h3>
  <p>A fair amount happened when the
    com.mycorp.examples.timeservice.consumer.filediscovery bundle was
    started. Briefly, what happened was this:</p>
  <ol>
    <li>The ECF Remote Services Admin implementation detected that the
      filediscovery bundle had an endpoint description file (<a
      target="_blank"
      href="http://wiki.eclipse.org/File-based_Discovery_with_the_Endpoint_Description_Extender_Format">EDEF
        file</a>) and read that file (in the <b>timeserviceendpointdescription.xml</b>
      file)...as per the OSGi Remote Service Admin specification.
    </li>
    <li>The reading and parsing of the EDEF file resulted in the import
      of the remote service described by the EDEF meta-data, which also
      resulted in the creation of an <b>ITimeService</b> proxy, which
      was registered in the consumer's service registry.
    </li>
    <li>The registration of the <b>ITimeService</b> proxy resulted in
      the consumer's <b>TimeServiceComponent.bindTimeService</b> method
      to be called by DS, and the implementation of this method on the
      consumer made the call to <b>timeService.getCurrentTime()</b>.
    </li>
    <li>The call to the <b>timeService.getCurrentTime()</b> went through
      the ECF Generic Provider to the <b>TimeServiceImpl</b> code on the
      service host, and when that method completed the return value was
      sent back to the consumer and printed out to the display.
    </li>
  </ol>
  <p>
    Note to the consumer, the <b>ITimeService</b> instance bound to the
    <b>TimeServiceComponent</b> looks just like a local instance of an <b>ITimeService</b>,
    but it's actually a proxy constructed dynamically during import by
    the ECF implementation of Remote Service Admin.
  </p>

  <p>
    Feel free to stop and restart the consumer and/or host...and if
    desired you may set breakpoints to debug into the example code at
    desired locations during this process. In the example source code
    there is also some tracing that's enabled that reports to stdout
    when the host exports the service, and when the client imports the
    service. The ability to listen to events created by the Remote
    Service Admin management agent is as specified by the <a
      target="_blank"
      href="http://wiki.eclipse.org/Remote_Services_Admin">Remote
      Service Admin</a> chapter 122.
  </p>

  <p>Note that ECF's implementation of OSGi Remote Services can/does run
    on multiple OSGi R5 Framework implementations...and has been tested
    explicitly on both Equinox and Felix.</p>

  <h3>ECF's Provider Architecture and Remote Services Customization</h3>
  <p>
    One of the unique aspects of ECF's implementation of RS/RSA is that
    it is based upon an <a target="_blank"
      href="http://wiki.eclipse.org/OSGi_4.2_Remote_Services_and_ECF">open
      provider architecture</a>. This allows adopters to customize,
    configure, extend, or even replace the major parts of this process.
    For example, with remote service discovery, it's often desirable to
    have dynamic, network-based discovery of remote services...so that
    consumers will simply be notified via the network when a remote
    service becomes available. ECF has multiple implementations of
    discovery providers, and any one of these can be used without
    modification of the remote service code. Here is a list of the
    discovery providers that ECF committers have created, and are
    already developed and available from ECF for use.
  </p>
  <ol>
    <li><a target="_blank"
      href="http://wiki.eclipse.org/File-based_Discovery_with_the_Endpoint_Description_Extender_Format">EDEF
        xml-file-based discovery</a> (used in example above)</li>
    <li><a target="_blank" href="http://zookeeper.apache.org/">Apache
        Zookeeper</a>. A popular server-based service discovery system
      that's part of the Apache Hadoop project.</li>
    <li><a target="_blank"
      href="http://en.wikipedia.org/wiki/DNS-SD#Apple.27s_multicast_DNS.2FDNS-SD">Zeroconf</a>.
      A multicast-based protocol originally created by Apple for printer
      and other device discovery.</li>
    <li><a target="_blank"
      href="http://en.wikipedia.org/wiki/Service_Location_Protocol">Service-Location
        Protocol (SLP)</a>. An IETF service discovery standard.</li>
    <li><a target="_blank"
      href="http://en.wikipedia.org/wiki/DNS-SD#DNS-based_service_discovery">DNS-Service
        Discovery (DNS-SD)</a>. A discovery protocol based upon dynamic
      DNS.</li>
  </ol>
  <p>
    As well, ECF has a full <a target="_blank"
      href="http://download.eclipse.org/rt/ecf/3.6.1/javadoc/org/eclipse/ecf/discovery/package-frame.html">Discovery
      API</a>...allowing anyone to implement a new provider for remote
    service discovery.
  </p>

  <p>
    The other major subsystem used by ECF's OSGi Remote Service
    implementation is known as the distribution subsystem. The
    distribution subsystem is used to export the remote service, provide
    the protocol for <br />consumer&lt;-&gt;host communication when making the
    remote call, marshal/unmarshall (serialize/deserialize) method
    parameters and return values on both host and consumer, and
    dynamically create proxies on the consumer when discovered. ECF has
    a <b>Remote Services API</b> that allows different providers to be
    used for the distribution mechanism for OSGi Remote Services. As
    with the Discovery API, the ECF Remote Service API is open and
    implementable by others, allowing new providers to be used that are
    based upon existing technologies (commercial and/or open source), or
    based upon new technologies. Here are the remote service providers
    that ECF currently distributes.
  </p>
  <ol>
    <li>ECF Generic</li>
    <li>r-OSGi</li>
    <li>Java Messaging Service (JMS)</li>
    <li>RESTlet</li>
    <li>Other REST-based Providers</li>
    <li>XMPP</li>
    <li>JavaGroups</li>
  </ol>
  <p>And as with the Discovery API, other distribution providers may
    easily be created and used, simply by implementing the ECF Remote
    Service API.</p>

  <h3>Conclusion</h3>
  <p>ECF committers have created a flexible and extensible open
    implementation of the OSGi R5 Remote Services and Remote Service
    Admin specifications. With the ECF Kepler Release, we have now shown
    the implementation to be fully compatible with the R5 spec, through
    full testing with the OSGi RS/RSA Test Compatibility Kit.</p>

  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
    </script>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/july/images/slewis2.jpg"
        alt="Scott Lewis" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Scott Lewis <br />
        <a target="_blank" href="http://www.composent.com">Composent.com</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://eclipseecf.blogspot.ca/">Blog</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-2', 'eclipse.org');
  ga('send', 'pageview');

  </script>
