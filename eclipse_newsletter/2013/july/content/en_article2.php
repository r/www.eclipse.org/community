<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <p>The Eclipse RAP project has set out to realize the idea of ​“single
    sourcing”, i.e. to develop software for different target platforms
    from a common code base. The increasing popularity of mobile devices
    presents new challenges to this approach. In Version 2, the RAP
    project is open for interchangeable clients and thus accounts for
    this development.</p>

  <h2>RAP is now the “Remote Application Platform”</h2>

  <p>
    The <a target="_blank" href="http://eclipse.org/rap/">RAP project</a>,
    which originated more than 6 years ago, was designed to make RCP
    applications available on the Web. The name “Rich Ajax Platform” of
    the project was deliberately modeled after the “Rich Client
    Platform”. The term “single sourcing” was quickly established for
    the development of applications for desktop and browser from a
    common code base.
  </p>
  <p>Although the desktop has since lost in significance, it is still
    indispensable in many areas of Enterprise IT. In addition, many
    projects today are faced with the task of having to also offer
    mobile access to business processes. With the fragmentation of the
    target platforms in the mobile sector, the subject of single
    sourcing is all the more interesting. Should it not be possible to
    transfer the proven function principle of RAP to new client
    platforms?</p>
  <p>RAP applications run on a server while the user interface is
    displayed and operated from a client connected via HTTP. Based on
    this separation we developed the vision of making the browser
    interchangeable against any other client. To put this idea into
    practice, the HTTP communication in RAP 2.0 was completely converted
    to an open JSON format that allows the connection of any client.</p>
  <p>Therefore the project made a transition from a pure Web framework
    to a universal platform for “Remote Applications”, i.e. applications
    that are operated locally but are running on a server. This claim is
    also reflected in the changed project name “Remote Application
    Platform”.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Multi-Platform.png"
    alt="Multi Platform" />
  <br />

  <p>Options for single sourcing with RAP</p>

  <h2>More than RCP in a Browser</h2>

  <p>RAP offers a remote-enabled implementation of the Eclipse widget
    toolkit (SWT) at the core and a customized version of the JFace
    library on top of it. With this API it is possible to single source
    complex applications. Even Eclipse Workbench based applications can
    be single-sourced with the help of few more additional components
    that are also part of the project. Although it is common practice to
    bring classical RCP applications to the Web with RAP – it is not the
    optimal solution for every project. Especially for mobile UIs, it’s
    obvious that RCP does not provide a suitable UI framework.</p>
  <p>The strengths of RCP are on the one hand the high flexibility of
    the user interface, which can be rearranged by the user, and on the
    other hand, the ability to develop the UI components independently
    of one another and to combine them freely. These capabilities are
    valuable for projects that develop a platform themselves. There are,
    however, many business applications that require more static user
    interfaces and thus can hardly benefit from the modularization
    concepts of the platform. In this case, the question arises as to
    whether the higher complexity and resource consumption of the
    Workbench is justified.</p>
  <p>
    Since version 1.5, RAP is no longer bound to the programming model
    of RCP. Applications can be freely put together from SWT widgets.
    The new <i>AbstractEntryPoint</i> base class reduces the
    implementation to the pure UI code. The example below shows a
    complete implementation. This base class forms the preferred entry
    point for applications in RAP 2. Entry points, themes, and other
    elements of the application are registered programmatically in an <i>ApplicationConfiguration</i>,
    instead of using extensions, as it is common in RCP.
  </p>

  <pre class="prettyprint lang-xtend">
public class SimpleSnippet extends AbstractEntryPoint {

  @Override
  public void createContents(Composite parent) {
    parent.setLayout(new GridLayout(2, false));
    Label label = new Label(parent, SWT.NONE);
    label.setText("Hello");
    Button button = new Button(parent, SWT.PUSH);
    button.setText("World");
  }

}
</pre>
  <br />
  <h2>Single sourcing with mobile Clients</h2>
  <p>It now possible to develop RAP clients for any desired platform
    with the new JSON protocol. The only prerequisite is that the
    respective platform provides a network connection and can display
    the widgets of the application. The programming language is
    unimportant, as long as a JSON Parser and an HTTP library are
    available. Clients for specialized embedded platforms are also
    conceivable. Such clients have to support only a small subset of the
    widgets depending on the application.</p>
  <p>
    Under the brand name “<a target="_blank"
      href="http://developer.eclipsesource.com/tabris/">Tabris</a>”,
    EclipseSource offers RAP-Clients for Android and iOS, which rely on
    the native widgets of the particular system and thus offer the
    look-and-feel and performance of native applications. With these
    clients it is possible to develop native mobile apps that access a
    RAP server. Tabris offers a special API to use the native navigation
    concept of the respective mobile platform in RAP applications.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/InputForm-Browser-Android.png"
    width="600" alt="Input Form Android Browser" />
  <br />
  <p>An input form in the web browser and with Tabris on Android and iOS</p>

  <p>A desktop user interface will rarely be directly transferable to a
    mobile phone. Layout, navigation concept and several details have to
    be adapted to the target platform. Such platform differences are not
    uncommon in single sourcing. With the underlying OSGi platform, it
    is easy to outsource platform-specific parts in services or
    fragments. Applications that are designed in terms of single
    sourcing mostly achieve high code reuse despite these differences.</p>
  <p>More important than the reuse of each individual class is, however,
    the common programming environment and common concepts for all
    target platforms, e.g. for the data binding of the UI. Existing
    Eclipse knowledge, trusted tools and infrastructure can equally be
    reused.</p>

  <h3>New Client-API in RAP 2</h3>
  <p>In order to differentiate between various clients in the program
    code and to allow access to platform-specific functions such as GPS
    or barcode scanner, RAP 2 provides a new API.</p>
  <p>
    The method <i>RWT.getClient()</i> returns an instance of the
    interface <i>Client</i> that represents the currently connected
    client. For the standard web client, it is always a <i>WebClient</i>.
    Platform-specific functions can be supplied by the client
    implementation as <i>ClientService</i>. A service locator method in
    the client then returns an instance of the requested service.
  </p>
  <p>The mobile Tabris clients offer, among other services, reading the
    geo-location and taking pictures with the built-in camera. The Web
    client supplied with RAP, on the other hand, provides services to
    access the browser history or perform JavaScript code in the
    browser, as shown in this example:</p>

  <pre class="prettyprint lang-xtend">
JavaScriptExecutor executor = RWT.getClient().getService(JavaScriptExecutor.class);
executor.execute("alert('Hello World!')");

</pre>
  <br />
  <h2>Remote-API for Custom Widgets</h2>
  <p>The development of custom widgets is also becoming easier with the
    new RAP protocol. Like all widgets in RAP, custom widgets are
    composed of two parts that are synchronized via the protocol: the
    server part that serves as a programming interface and the client
    part that is responsible for the representation.</p>
  <p>Custom Widgets for RAP 2 can now directly access the object on the
    opposite side via a RemoteObject. With this simple API, that
    corresponds directly with the operations of the RAP protocol,
    properties of the remote object can be changed, methods can be
    invoked, and event notifications can be received. This allows custom
    widgets to take synchronization into their own hands without having
    to register an adapter as in the RAP 1.x Framework.</p>
  <p>
    Also, the web client offers a public JavaScript API for the first
    time, which also contains a RemoteObject for communication with the
    opposite side. The integration of the <a target="_blank"
      href="https://github.com/eclipsesource/rap-ckeditor">CKEditor</a>
    can be used as an example of a custom widget that already uses the
    new API.
  </p>

  <h3>New Features in RAP 2.0 and 2.1 (Kepler)</h3>
  <p>As every year, the RAP project is also part of the 2013 joint
    Eclipse release (Kepler). RAP version 2.1 has been released with
    Kepler in June 2013.</p>
  <p>
    With RAP 2.1 it is now possible to open a single RAP application
    simultaneously in <b>multiple browser tabs</b>. The RAP server now
    autonomously differentiates various instances within a HTTP session,
    and treats them separately. For this purpose, the new UISession,
    which represents a separate instance of the UI in RAP 2, was
    introduced to supplement the <i>HttpSession</i> from the
    Servlet-API. Application developers can use the UISession to save
    EntryPoint specific data. User-related data should instead be
    retained between instances in the HttpSession.
  </p>
  <p>
    In the past RAP applications had to be accessed with <b>entrypoint
      URLs</b> of the form <i>http://hostname/rap?startup=myentrypoint</i>.
    Both the servlet path “<i>/rap</i>”, and also the “<i>startup</i>”
    parameter had been predefined in this pattern. Custom URLs were only
    possible with a branding. Since 2.0, each entry point must be
    assigned a fixed servlet-path. This replaces the old URL pattern
    with simple URLs of the form <i>http://hostname/myentrypoint</i>.
  </p>
  <p>
    The <b>Server-Push</b> mechanism of RAP was further developed again.
    In 1.x, this function was known as “UICallBack”, in 2.x, there is a
    simpler API. Any desired components of an application can now start
    and stop a <i>ServerPushSession</i> independently of one another and
    so indicate their interest in push notifications.
  </p>
  <p>
    For many business applications, a sophisticated <b>keyboard control</b>
    is essential. In RAP 2.1, the support for shortcuts has been
    expanded again. Menu items can now be equipped with an accelerator,
    i.e. a combination of keys that is immediately available as a
    shortcut without adding additional listeners. Even mnemonics are now
    supported in the Web client. To distinguish the keyboard shortcuts
    from those of the browser, other modifier keys must be used, e.g.
    Alt+Ctrl+A instead of Alt+A.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Shortcuts.png"
    alt="Shortcuts" />
  <br />

  <p>Shortcuts and Mnemonics in RAP 2.1</p>

  <p>
    The GraphicsContext is now able to <b>draw arbitrary path-object</b>
    including Bezier curves through the support of the class path.
  </p>
  <p>
    Finally, the <b>support of actual browsers</b> has continued. In
    Internet Explorer 10, RAP can now revert to the CSS3 support for
    rounded corners and gradients. Such effects had to be implemented
    with SVG or VML in older Microsoft browsers. The touch operation on
    mobile browsers has also been improved.
  </p>
  <p>
    A complete list of modifications in <a
      href="http://eclipse.org/rap/noteworthy/2.0/">RAP 2.0</a> and <a
      href="http://eclipse.org/rap/noteworthy/2.1/">2.1</a> can be found
    in the “New and Noteworthy” sides on the releases.
  </p>

  <h3>Migration of applications on RAP 2.x</h3>
  <p>
    With the development of RAP 2.0, some API modifications that are not
    backward compatible were unavoidable. Thus, e.g. the package <i>org.eclipse.rwt</i>
    had to be renamed <i>org.eclipse.rap.rwt</i>. The conversion of
    existing 1.x RAP applications therefore requires some adjustments
    (often as simple as a recalculation of imported packages). Since the
    modifications are limited to the RWT-API – i.e. APIs of SWT, JFace,
    etc., are not affected, the transition for most applications should
    be without problems. To make the migration as simple as possible,
    outdated APIs, where technically possible, are still supported. All
    necessary changes are described in a <a
      href="http://eclipse.org/rap/noteworthy/2.0/migration-guide/">migration
      guide</a> on the RAP project site.
  </p>


  <h2>New in the RAP Incubator</h2>
  <p>The RAP Incubator is a separate project, in which additional
    components for RAP are developed. Two recent developments will be
    briefly introduced here:</p>

  <h3>Auto-Suggest</h3>
  <p>Automatic suggestions and completion for input fields are state of
    the art for modern applicatinos. In SWT, the Combo widget is often
    used for suggesting terms, but with its static list of proposals it
    is not a fully satisfactory substitute. A new Incubator component
    called DropDown should solve the autosuggest requirement for RAP.
    This component basically consists of a DropDown widget that can
    display a drop-down list below a text field and some client-side
    scripts to add the auto-suggest behavior. A matching JFace viewer
    uses this DropDown to enhance a Text widget with the auto-suggestion
    function and provide data from the specified content provider.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/AutoSuggest.png"
    alt="AutoSuggest" />
  <br />
  <p>AutoSuggest from the RAP Incubator</p>

  <h3>ClientScripting</h3>
  <p>This component makes an implementation of the SWT interface
    listener available which allows processing of events directly on the
    client. This can be used, for example, to immediately validate
    inputs and display the results without delay.</p>
  <p>Such a ClientListener is equipped with JavaScript code, which is
    then automatically transferred and registered on the client. The
    JavaScript API is closely based on SWT, so that existing listener
    code can easily be converted to JavaScript. The following example
    shows the JavaScript code of a ClientListener for a text field that
    validates input with the Modify event and highlights the background
    in case of invalid input.</p>

  <pre class="prettyprint lang-xtend">
var handleEvent = function(event) {
  var text = event.widget.getText();
  if(text.match(/^[0-9]*$/) === null) {
    event.widget.setBackground([255, 255, 128]);
    event.widget.setToolTip("Digits only!");
  } else {
    event.widget.setBackground(null);
    event.widget.setToolTip(null);
  }
};
</pre>
  <br />
  <p>The ClientScripting module will be expanded in the coming release
    and integrated into the core of RAP.</p>
  <p>
    Links to the source code and builds of the Incubator components can
    be found on the <a href="http://eclipse.org/rap/incubator/">RAP
      Incubator page</a>.
  </p>

  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
    </script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/july/images/RalfSternberg2.jpeg"
        alt="Ralf Sternberg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ralph Sternberg <br />
        <a target="_blank" href="http://eclipsesource.com/en/home/">Eclipse
          Source</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://eclipsesource.com/blogs/author/rsternberg/">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/ralfstx">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/102329659608792609904">Google +</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-2', 'eclipse.org');
  ga('send', 'pageview');

  </script>
