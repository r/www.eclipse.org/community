<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

  <p>
    <a href="http://eclipse.org/scout/">Eclipse Scout</a> is a framework
    to build business applications based on the Eclipse platform. So
    far, Scout client applications included support for Swing or SWT to
    build desktop applications, and Eclipse RAP for web applications.
  </p>

  <p>New with the Kepler release, Scout has added support for creating
    mobile applications that run on tablets and smart phones. Thanks to
    the clean separation of the Scout application model from a specific
    UI technology Scout applications may now be deployed on mobile
    phones, as web applications or to the desktop with a single code
    base. This feature greatly reduces maintenance costs for larger
    applications that need to be available on multiple frontends.</p>

  <p>Below, a mid-sized Scout business application (20'000 Java classes)
    is shown. This application takes advantage of Scout's mobile support
    and can be deployed to desktop and mobile frontends simultaneously.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Eclipsescout_businessapplication_kepler.png"
    alt="scout kepler app" />
  <br />

  <p>The rendering of the UI is adapted to the different target devices
    at runtime. For this, a device transformer component takes care of
    adapting UI elements to a specific frontend. Examples being context
    menus that are replaced with touch buttons for mobile devices and
    tables that are mapped to lists with finger scrolling support.</p>

  <p>For the application shown above, less than 20 Java classes are
    specifically built for the mobile client. These classes are mainly
    responsible for the application's home screen and the telephony
    integration on mobile phones. In addition, 200 minor frontend
    specific adaptations are included in the code base of the product.
    This results in over 98% of the Java code beeing reused across the
    different frontends.</p>

  <p>Being independent of specific UI technologies is clearly one of
    Scout's major assets. As an additionaly benefits, it reduces the
    long term risks of "marrying" critical business applications to the
    "wrong" UI technology to almost zero. In fact, the default setup of
    Scout applications enforces the separation of business code from UI
    technology codes.</p>

  <p>The scope of the Scout framework is best shown using a layered
    setup typical for enterprise IT landscapes. Many well established
    open source frameworks exist to deal with data access, persistence
    and the implementation of backend systems up to the business service
    layer.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Scout_integration.jpg"
    alt="Scout_integration" />
  <br />


  <h3>The Hello World</h3>

  <p>A "Hello World" program is a traditional way to begin any new
    framwork or programming language. The special aspect of the Scout
    "Hello World" is it's client server architecture including clients
    that runs on the desktop, in a web browser, and on mobile devices.</p>

  <p>In the text below we will first introduce the "Hello World" from
    the user perspective before we go through the installation process
    and dive into the actual implementation.</p>

  <h4>What the User sees</h4>

  <p>When the user starts a client desktop application he/she gets
    either a Swing or SWT based application showing a "hello world!"
    message in the main frame of the application. To run the Hello World
    as a web application in a browser, the user can also type the URL to
    the application into the address bar of a web browser. As shown
    below, the user has the choice between three different client
    applications.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Helloworld_running.png"
    width="600" alt="hello world" />
  <br />

  <p>To demonstrate the new mobile Support shipped with the Kepler
    relase of Scout, the user can also type the application's URL into a
    mobile browser. Depending on the frontend device used, the user is
    redirected to specific links for mobile phones or tablets.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Helloworld_running_mobile.png"
    alt="hello world mobile" />
  <br />

  <p>When we compare the Hello World web application with the mobile
    version shown above the slight differences between the two frontends
    become apparent. On the mobile device the label of the message field
    is rendered on top of the text and instead of a Tools button that
    provides access to the applications menu tree we just have a logout
    button on the mobiles home form.</p>

  <h4>Installing Scout</h4>

  <p>
    Installing the Eclipse for Scout Developers package is no different
    from installing any other Eclipse package. The Scout package is
    available on <a href="http://eclipse.org/downloads/">eclipse.org/downloads</a>
    and the only prerequisite for the installation is a working JDK
    version 6 or 7.
  </p>

  <p>
    In case you prefer to go through detailed installation instructions,
    have a look at <a target="_blank"
      href="http://tools.bsiag.com/scoutbook/3.9/2013-07-11_15-05-49/html/bookap2.html#x96-86000B">these
      pages</a>.
  </p>

  <h4>The Implementation</h4>

  <p>Begin the implementation of the Hello World application with
    starting your Eclipse IDE executable and providing an empty
    workspace directory.</p>

  <p>
    Once Eclipse has started, click away the welcome screen to switch to
    the workbench of the Scout SDK and you see the Scout perspective
    with its <b>Scout Explorer</b> shown in the upper left of the
    Eclipse IDE.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_new_projectmenu.png"
    alt="new project menu" />
  <br />

  <p>
    As shown above, use context menu <i>New Scout Project</i>... to
    launch the new project wizard.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_new_project.png"
    alt="new project" />
  <br />

  <p>
    In the new project wizard enter <b>org.eclipsescout.helloworld</b>
    into the field Application Name and click on the wizard's Finish
    button.
  </p>

  <p>After the Scout SDK has created the initial Scout application in
    the background the application model is presented as a tree in the
    Scout Explorer view.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_new_formfield.png"
    alt="new form field" />
  <br />

  <p>
    And, under the main project node <i>org.eclipsescout.helloworld</i>:
  </p>

  <ul>
    <li>Navigate to the orange client node <i>org.eclipsescout.helloworld.client</i></li>
    <li>Expand its sub folder <i>Forms</i></li>
    <li>Expand the <i>DesktopForm</i> node
    </li>
    <li>Click on the <b>MainBox</b> node
    </li>
  </ul>

  <p>
    What you have done now is selecting the container of all form fields
    of the desktop form which in turn is the dialog shown in the main
    application window of the <i>Hello World</i> application.
  </p>

  <p>
    We now add the first form field by selecting the context menu <b>New
      Form Field</b>... on the <i>MainBox</i> node. This will open the
    new form field wizard shown below.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_groupbox_1.png"
    alt="groupbox 1" />
  <br />

  <p>
    In the first wizard step we select a <b>GroupBox component</b> and
    click on the Next button.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_groupbox_2.png"
    alt="groupbox 2" />
  <br />

  <p>
    Then we set the Class Name field to <b>DesktopBox</b> and click the
    <i>Finish</i> button. This will add a group box field that later
    holds the message field. Please note the only reason for adding the
    group box field here is a better layout for the <i>Hello World</i>
    application. We could also have added the message field directly
    into the <i>MainBox</i>.
  </p>

  <p>
    On the newly created desktop box field we now want to add the
    message field. As shown below, we now use the context menu <b>New
      Form Field</b>... on the <i>DesktopBox</i> node.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_new_messagefield.png"
    alt="message field" />
  <br />

  <p>
    In the first wizard step we select a <b>StringField</b> component
    and click on the <i>Next</i> button.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_message_1.png"
    alt="message 1" />
  <br />

  <p>
    To provide the lable for this string field we add <b>Message</b>
    into the <i>Name</i> field as shown below.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_message_2.png"
    alt="message 2" />
  <br />

  <p>
    From the drop-down list presented by the wizard select the entry <b>New
      translated text...</b> with a double click. This starts the new
    text entry wizard shown below.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_message_3.png"
    alt="message 3" />
  <br />

  <p>In this step, the Scout SDK helps the developer to work with
    translated texts, rather than typing in string literals directly
    into the source code of the Scout application. The texts entered
    into the langauge tabs will be copied by the Scout SDK into Java
    properties files and the application code then only contains the
    text keys.</p>

  <p>
    As we only have a <i>default</i> language tab to fill in, the
    suggested values for the fields <i>Key Name</i> and <i>default</i>
    are fine and we can close the dialog with the <i>Ok</i> button.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_message_4.png"
    alt="message 4" />
  <br />

  <p>
    Having filled in the label text the class name field is
    automatically filled in and we can close the wizard with the <i>Finish</i>
    button. With this step we have completed the implementation of the
    Scout client part of the <i>Hello World</i> application.
  </p>

  <p>As a next step, we will now implement the business logic on the
    Scout server that will provide the content to the message field that
    is displayed in the Scout client to the user.</p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_load_service.png"
    alt="load service" />
  <br />

  <p>
    To implement the server logic of the <i>Hello World</i> application,
    we first navigate to the <i>load</i> method of the <b>DesktopService</b>
    under the blue server node as shown above.
  </p>

  <p>This follows the Scout standard pattern where each client form has
    an associated server service that is responsible for loading form
    content and storing user changes using the application's persistence
    mechanism.</p>

  <p>
    In the <i>Hello World</i> application the desktop service is only
    used to load an initial content into the message field of the
    desktop form. With a double click on the load method shown in the
    Scout Explorer the associated source code is loaded into the Java
    editor. In the Java code we only need to provide the Scout code to
    fill in the content to the message field.
  </p>

  <pre class="prettyprint lang-xtend">

formData.getMessage().setValue("hello world!");

</pre>
  <br />

  <p>
    Using the <i>formData</i> input parameter, we can access the desktop
    forms message field with method <b>getMessage</b> and use <b>setValue</b>
    to fill in the desired content. This leads to the final
    implementation of the <i>load</i> method as shown below:
  </p>

  <pre class="prettyprint lang-xtend">
@Override
 public DesktopFormData load(DesktopFormData formData) throws ProcessingException {
   formData.getMessage().setValue("hello world!");
   return formData;
 }
 </pre>
  <br />
  <p>That's it. Both the transportation of the form data object between
    the Scout server and client application, and the necessary mapping
    of its content to the form fields in the client application are
    performed by the framework. This does not require any work by the
    Scout developer.</p>

  <h3>Running the Application</h3>

  <p>
    To launch the <i>Hello World</i> application from within the Scout
    SDK first select the main project node <b>org.eclipsescout.helloworld</b>
    in the Scout Explorer as shown below.


  <p>
    The <i>Scout Object Property</i> view of the main project node
    contains a <i>Product Launchers</i> section. This section contains a
    launcher box for each development product of the <i>Hello World</i>
    application.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_start_server.png"
    alt="start server" />
  <br />

  <p>
    As shown in the screenshot above, we first start the <i>Hello World</i>
    server application with a click on the green <i>Start Product</i>
    icon in the <b>Server</b> box.
  </p>

  <img
    src="/community/eclipse_newsletter/2013/july/images/Sdk_start_client_swt.png"
    alt="start server" />
  <br />

  <p>
    Then we can start the client products in turn. In the screenshot
    above the SWT client is started using the start icon in the <b>SWT</b>
    box.
  </p>

  <p>
    To access the necessary target URLs for web and mobile clients of
    the <i>Hello World</i> application click on the link <b>open
      config.ini</b> provided in the <i>RAP</i> launcher box. This will
    open the config.ini file that contains the URL in the form of
    comments.
  </p>
  <ul>
    <li><a target="_blank" href="http://localhost:8082/mobile">http://localhost:8082/mobile</a></li>
    <li><a target="_blank" href="http://localhost:8082/tablet">http://localhost:8082/tablet</a></li>
    <li><a target="_blank" href="http://localhost:8082/web">http://localhost:8082/web</a></li>
    <li><a target="_blank" href="http://localhost:8082/">http://localhost:8082/</a>
      (for automatic dispatching)</li>
  </ul>

  <h3>Next Steps</h3>
  <p>In case you have been intrigued by what you have seen from Scout so
    far you might consider the following steps:</p>

  <p>
    <b>Do more tutorials.</b> In addition to the Hello World tutorial
    presented above, a significant amount of additional <a
      target="_blank" href="http://wiki.eclipse.org/Scout/Tutorial/3.9">tutorials</a>
    and <a target="_blank"
      href="http://wiki.eclipse.org/Scout/HowTo/3.9">how-tos</a> is
    available on the <a target="_blank"
      href="http://wiki.eclipse.org/Scout">Scout wiki</a>. Depending on
    your time and interest, this will keep you occupied an addtional
    hour - or several days. In case you run into difficulties or have
    general questions regarding Scout we are happy to answer your
    questions in the <a
      href="http://www.eclipse.org/forums/index.php?t=thread&frm_id=174">Scout
      forum</a>. Please bear in mind that July and August is summer
    vacation time and we are less responsive during these months.
  </p>

  <p>
    <b>Browse the Book.</b> A more comprehensive introduction is
    available in the <a target="_blank"
      href="http://wiki.eclipse.org/Scout/Book/3.9">Scout book</a>, also
    new with the Kepler release. So far, the book provides a good
    introduction and a general overview of the possibilities of Scout
    applications. For the next releases we plan to add more material to
    the book that cover various aspects of Scout and the development of
    Scout applications in greater detail. The Scout book is free and
    available in <a target="_blank"
      href="http://tools.bsiag.com/scoutbook/3.9/2013-07-11_15-05-49/html/book.html">HTML</a>
    format for online browsing. For offline usage the book is availabe
    in <a target="_blank"
      href="http://tools.bsiag.com/scoutbook/3.9/2013-07-11_15-05-49/pdf/book.pdf">PDF</a>,
    <a target="_blank"
      href="http://tools.bsiag.com/scoutbook/3.9/2013-07-11_15-05-49/epub/">EPUB</a>
    and <a target="_blank"
      href="http://tools.bsiag.com/scoutbook/3.9/2013-07-11_15-05-49/html/book.zip">ZIP</a>
    (zipped HTML) formats.
  </p>

  <p>
    <b>Follow Scout.</b> For more recent updates, check out the <a
      target="_blank" href="http://www.bsiag.com/scout/">Scout blog</a>
    or the <a target="_blank" href="https://twitter.com/EclipseScout">@EclipseScout</a>
    Twitter account. If you happen to live near Zurich also have a look
    at the local Eclipse Community page on <a target="_blank"
      href="https://plus.google.com/communities/103558137660089260183">Google+</a>.
    This is the place where we usually announce public Eclipse events
    that frequently include Scout presentations.
  </p>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
    </script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2013/july/images/mzi3.png"
        alt="Matthias Zimmermann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Matthias Zimmermann <br />
        <a target="_blank" href="http://eclipse.org/scout/">Eclipse
          Scout</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://www.bsiag.com/scout/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/EclipseScout">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/communities/103558137660089260183">Google
            +</a></li>
          <?php echo $og; ?>
        </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-2', 'eclipse.org');
  ga('send', 'pageview');

  </script>
