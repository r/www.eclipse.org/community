<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <p>
    The main goal of the EclipseLink 2.5 release was the support of the
    JPA 2.1 specification, as EclipseLink 2.5 was the reference
    implementation for JPA 2.1. For a list of JPA 2.1 features look <a
      target="_blank"
      href="http://wiki.eclipse.org/EclipseLink/Release/2.5/JPA21">here</a>,
    or <a target="_blank"
      href="http://en.wikibooks.org/wiki/Java_Persistence/What_is_new_in_JPA_2.1%3F">here</a>.
  </p>
  <p>Most of the features that went into the release were to support JPA
    2.1 features, so there was not a lot of development time for other
    features. However, I was still able to sneak in a few cool new
    performance features. The features are not well documented yet, so I
    thought I would outline them here.</p>

  <h2>Indexing Foreign Keys</h2>
  <p>The first feature is auto indexing of foreign keys. Most people
    incorrectly assume that databases index foreign keys by default.
    Well, they don't. Primary keys are auto indexed, but foreign keys
    are not. This means any query based on the foreign key will be doing
    full table scans. This is any OneToMany, ManyToMany or
    ElementCollection relationship, as well as many OneToOne
    relationships, and most queries on any relationship involving joins
    or object comparisons. This can be a major perform issue, and you
    should always index your foreign keys fields.</p>
  <p>EclipseLink 2.5 makes indexing foreign key fields easy with a new
    persistence unit property:</p>

  <pre class="prettyprint lang-xtend">
"eclipselink.ddl-generation.index-foreign-keys"="true"
</pre>
  <br />
  <p>
    This will have EclipseLink create an index for all mapped foreign
    keys if EclipseLink is used to generate the persistence unit's DDL.
    Note that DDL generation is now standard in JPA 2.1, so to enable <a
      target="_blank"
      href="http://wiki.eclipse.org/EclipseLink/Release/2.5/JPA21#DDL_generation">DDL
      generation</a> in EclipseLink 2.5 you can now use:
  </p>

  <pre class="prettyprint lang-xtend">
"javax.persistence.schema-generation.database.action"="create"
</pre>
  <br />
  <p>EclipseLink 2.5 and JPA 2.1 also support several new DDL generation
    features, including allowing user scripts to be executed. See, DDL
    generation for more information.</p>

  <h2>Query Cache Invalidation</h2>
  <p>
    EclipseLink has always supported a query cache. Unlike the object
    cache, the query cache is not enabled by default, but must be
    enabled through the query hint <i>"eclipselink.query-results-cache"</i>.
    The main issue with the query cache, is that the results of queries
    can change when objects are modified, so the query cache could
    become out of date. Previously the query cache did support
    time-to-live and daily invalidation through the query hint <i>"eclipselink.query-results-cache.expiry"</i>,
    but would not be kept in synch with changes as they were made.
  </p>
  <p>
    In EclipseLink 2.5 automatic invalidation of the query cache was
    added. So if you had a query <i>"Select e from Employee e"</i> and
    had enabled query caching, every execution of this query would hit
    the cache and avoid accessing the database. Then if you inserted a
    new Employee, in EclipseLink 2.5 the query cache for all queries for
    Employee will automatically get invalidated. The next query will
    access the database, and get the correct result, and update the
    cache so all subsequent queries will once again obtain cache hits.
    Since the query cache is now kept in synch, the new persistence unit
    property <i>"eclipselink.cache.query-results"="true"</i> was added
    to enable the query cache on all named queries. If, for some reason,
    you want to allow stale data in your query cache, you can disable
    invalidation using the <i>QueryResultsCachePolicy.setInvalidateOnChange()
      API.</i>
  </p>
  <p>
    Query cache invalidation is also integrated with cache coordination,
    so even if you modify an Employee on another server in your cluster,
    the query cache will still be invalidated. The query cache
    invalidation is also integrated with EclipseLink's support for
    Oracle Database Change Notification. If you have other applications
    accessing your database, you can keep the EclipseLink cache in synch
    with an Oracle database using the persistence unit property <i>"eclipselink.cache.database-event-listener"="DCN"</i>.
    This support was added in EclipseLink 2.4, but in EclipseLink 2.5 it
    will also invalidate the query cache.
  </p>


  <h2>Tuners</h2>
  <p>
    EclipseLink 2.5 added an API to make it easier to provide tuning
    configuration for a persistence unit. The <i>SessionTuner</i> API
    allows a set of tuning properties to be configured in one place, and
    provides deployment time access to the EclipseLink Session and
    persistence unit properties. This makes it easy to have a
    development, debug, and production configuration of your persistence
    unit, or provide different configurations for different hardware.
    The SessionTuner is set through the persistence unit property <i>"eclipselink.tuning"</i>.
  </p>

  <h2>Concurrent Processing</h2>
  <p>The most interesting performance feature provided in EclipseLink
    2.5 is still in a somewhat experimental stage. The feature allows
    for a session to make use of concurrent processing.</p>
  <p>
    There is no public API to configure it as of yet, but if you are
    interested in experimenting it is easy to set through a <i>SessionCustomizer</i>
    or <i>SessionTuner</i>.
  </p>

  <pre class="prettyprint lang-xtend">
public class MyCustomizer implements SessionCustomizer {
  public void customize(Session session) {
    ((AbstractSession)session).setIsConcurrent(true);
  }
}
</pre>
  <br />

  <p>Currently this enables two main features, one is the concurrent
    processing of result sets. The other is the concurrent loading of
    load groups.</p>
  <p>In any JPA object query there are three parts. The first is the
    execution of the query, the second is the fetching of the data, and
    the third is the building of the objects. Normally the query is
    executed, all of the data is fetched, then the objects are built
    from the data. With concurrency enabled two threads will be used
    instead, one to fetch the data, and one to build the objects. This
    allows two things to be done at the same time, allowing less overall
    time (but the same amount of CPU). This can provide a benefit if you
    have a multi-CPU machine, or even if you don't, it allows the client
    to be doing processing at the same time as the database machine.</p>
  <p>
    The second feature allows all of the relationships for all of the
    resulting objects to be queried and built concurrently (only when
    using a shared cache). So, if you queried 32 Employees and also
    wanted each Employee's address, the address queries could all be
    executed and built concurrently, resulting in significant less
    response time. This requires the usage of a <i>LoadGroup</i> to be
    set on the query. <i>LoadGroup</i> defines a new API <i>setIsConcurrent()</i>
    to allow concurrency to be enabled (this defaults to true when a
    session is set to be concurrent).
  </p>
  <p>
    A <i>LoadGroup</i> can be configured on a query using the query hint
    <i>"eclipselink.load-group"</i>, <i>"eclipselink.load-group.attribute"</i>,
    or through the JPA 2.1 <i>EntityGraph</i> query hint <i>"javax.persistence.loadgraph"</i>.
  </p>
  <p>Note that for concurrency to improve your application's performance
    you need to have spare CPU time. So, to benefit the most you need
    multiple-CPUs. Also, concurrency will not help you scale an
    application server that is already under load from multiple client
    requests. Concurrency does not use less CPU time, it just allows for
    the CPUs to be used more efficiently to improve response times.</p>


  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
    type="text/javascript"></script>
  <script
    src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
    type="text/javascript"></script>
  <script type="text/javascript">
  prettyPrint();
    </script>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <p class="author-name">
        James Sutherland <br />
        <a target="_blank" href="http://www.oracle.com/index.html">Oracle</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://java-persistence-performance.blogspot.ca/">Blog</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/102096746046640528569/posts">Google
            +</a></li>
          <?php echo $og; ?>
        </ul>
    </div>
  </div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34967275-2', 'eclipse.org');
  ga('send', 'pageview');

    </script>
