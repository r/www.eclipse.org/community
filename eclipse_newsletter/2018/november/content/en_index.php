<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2018.11.29</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/stephanie.jpg" />
                            <h3>Editor's Note</h3>
                            <p>Welcome to the quarterly edition of the Jakarta EE newsletter where you&rsquo;ll find articles and updates on the topic of Cloud Native Java.</p>
							<p>Over the last couple of months, critical progress has been made in the world of Jakarta EE. From the announcement of the new Eclipse Foundation Specification Process (EFSP) to the Eclipse GlassFish 5.1.0-RC1 release, Jakarta EE is making waves among the Java community!</p>
							<p>Keeping that in mind, this month we are excited to bring you a fully loaded Jakarta EE newsletter that has a mix of technical content, community news, and a tutorial!</p>
								<p> &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/whilewaitingjakartaee.php">While Waiting for Jakarta EE</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jnosql_jakartaee.php">JNoSQL and Jakarta EE</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartavoices.php">Jakarta EE Community Voices: CDI is the Future</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/birdseyejavaee8.php">Java EE 8 - A Bird's Eye View</a><br>   
                           			&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartaworkflow.php">Tutorial: Imixs-Workflow and Jakarta EE</a><br>  
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/glassfish.php">Out-of-Box GlassFish and Payara Clustering</a><br>   
									&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jsonjakartaee.php">JSON Support in Jakarta EE</a><br></p>   
							<p>We hope you enjoy!</p>
                          	<p>P.S, if you haven't already, please take the  <a href="http://bit.ly/2KFeoLb" target="_blank">Eclipse Foundation brand survey</a>! The last day for feedback is December 14th, 2018! We value your opinions and appreciate your support!</p>
                            <p>Stephanie Swart<br>
                            <a style="color:#000;" target="_blank" href="https://twitter.com/stephaniejswart">@stephaniejswart</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/whilewaitingjakartaee.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/whilewaitingjakartaee.php"><h3>While Waiting for Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jnosql_jakartaee.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jnosql_jakartaee.php"><h3>JNoSQL and Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartavoices.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartavoices.php"><h3>Jakarta EE Community Voices: CDI is the Future</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/birdseyejavaee8.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/7.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/birdseyejavaee8.php"><h3>Java EE 8 - A Bird's Eye View</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartaworkflow.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jakartaworkflow.php"><h3>Tutorial: Imixs-Workflow and Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/glassfish.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/glassfish.php"><h3>Out-of-Box GlassFish &amp; Payara Clustering</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
       <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="100%" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="100%"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="48%"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jsonjakartaee.php"><img
                            src="https://www.eclipse.org/community/eclipse_newsletter/2018/november/images/3.png"
                            width="100%" class="flexibleImage"
                            style="max-width: 100%;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/november/jsonjakartaee.php"><h3>JSON Support in Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="48%"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href=""><h3></h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

         
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-passage">Eclipse Passage</a> provides a complete solution to control the usage of licensed functionality and to manage licensing-related data for OSGi-based and Eclipse-based products. </li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-oscar-open-software-composition-analysis">Eclipse OSCAR</a> provides a complete software composition analysis solution, focused on compliance and security, that can be installed on cloud, local server, or workstation environment.</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.interceptors/reviews/1.2.3-release-review">Eclipse Project for Interceptors 1.2.3</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jakartaee-stable/reviews/1.0-release-review">Eclipse Project for Stable Jakarta EE APIs 1.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jstl/reviews/1.2.3-release-review">Eclipse Project for JSTL 1.2.3</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jaxb/reviews/2.3.2-release-review">Eclipse Project for JAXB 2.3.2</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.reddeer/reviews/2.4.0-release-review">Eclipse RedDeer 2.4.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jms/reviews/2.0.2-release-review">Eclipse Project for JMS 2.0.2</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.ejb/reviews/3.2.3-release-review">Eclipse Project for EJB 3.2.3</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jpa/reviews/2.2.2-release-review">Eclipse Project for JPA 2.2.2</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/november/images/new_vecm.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.siriuscon.org/">SiriusCon 2018</a><br>
                           December 4 - 5, 2018 | Virtual Conference</p>
                           <p><a target="_blank" style="color:#000;" href="https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2018/">KubeCon + CloudNativeCon North America 2018</a><br>
                           December 10 - 13, 2018 | Seattle, Washington</p>
                           <p><a target="_blank" style="color:#000;" href="http://www.euroforum.de/software-eng/">Future of Automotive Software</a><br>
                           December 10, 2018 | Munich, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->