<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>It is more than a year since Oracle announced the transfer of Java&trade; EE to Eclipse Foundation at JavaOne 2017. Since then, a lot has happened:</p>
	<ul>
		<li>Java&trade; EE 8 API and implementation projects have been set up under <a href="https://projects.eclipse.org/projects/ee4j" target="_blank">EE4J</a>.</li>
		<li>The Eclipse GlassFish 5.1 <a href="https://wiki.eclipse.org/Eclipse_GlassFish_5.1_Release_Plan" target="_blank">release</a> is approaching.</li>
		<li>A brand new <a href="https://jakarta.ee/" target="_blank">Jakarta EE</a> specification process is right around the corner.</li>
		<li>Community shows <a href="https://docs.google.com/document/d/1y-Vs4d9Iotw0HqsiTxG5UCm7ua0w35vJZkGVmS_hFrw/edit#heading=h.4lyopu2gbqrg" target="_blank">involvement</a> regarding the technical direction of Jakarta EE.</li>
		<li>The Jakarta EE NoSQL specification <a href="https://projects.eclipse.org/proposals/jakarta-ee-nosql" target="_blank">project proposal</a> has been created.</li>
	</ul>
	<p>This is all very good, excellent actually! When you think about the size of it all, it is actually quite an achievement. We are talking about <strong>7.7 million lines of code</strong>! More than <strong>60.000 files</strong> and a total of <strong>38 new projects</strong> that have been set up at the Eclipse Foundation.</p>
	<p>But, as everyone knows, developers are impatient and eager to try out everything new, so there are still a couple of questions that I always get when talking about Jakarta EE:</p>
	<ul>
		<li>When can I start developing Jakarta EE applications?</li>
		<li>How does <a href="https://microprofile.io/" target="_blank">Eclipse MicroProfile</a> fit in this picture?</li>
	</ul>
	<p>The answer to the first question is: "not yet". Until the Jakarta EE specification process is finalized, the technologies are still Java&trade; EE. </p>
	<p>The answer to the second question differs a little depending on who you ask, but usually is something along the lines of "I am pretty sure that some of the MicroProfile specs will be integrated into Jakarta EE when they have proven to be useful".</p>
	<p>So, what should an eager developer do in the meantime? Switch to <strong>Spring Boot</strong> ...ouch ...or ...<strong>JavaScript</strong> ...squeal...?"</p>
	<p><strong>NO</strong>, here is what you should do: Use the power of <strong>Java&trade; EE 8</strong> and combine it with <strong>Eclipse MicroProfile</strong>.</p>	
	<p>Many of the application server vendors have added MicroProfile features to their Java&trade; EE 8 compliant or certified application servers. Examples are <a href="https://openliberty.io/" target="_blank">Open Liberty</a>, <a href="http://wildfly.org/" target="_blank">WildFly</a>, <a href="https://info.payara.fish/" target="_blank">Payara</a> and <a href="http://tomee.apache.org/" target="_blank">Apache TomEE</a>. See the respective vendor's documentation for which versions they have included.</p>		
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/dependancy.png"></p>
	<p align="center"><em>Java EE 8 with Eclipse MicroProfile 2.1</em></p>
	<p>I have put together a simple application called <a href="https://github.com/ivargrimstad/jakartaee-duke" target="_blank">Jakarta EE Duke</a> to demonstrate how to do this. The application uses the <strong>@ConfigProperty</strong> annotation from <a href="https://microprofile.io/project/eclipse/microprofile-config" target="_blank">MicroProfile Config</a> to configure a message as well as the new <strong>@Email</strong> annotation from <a href="https://beanvalidation.org/2.0/" target="_blank">Bean Validation 2.0</a>, which came with Java&trade; EE 8 to validate input.</p>
	<p>While this example is extremely simple, it does indicate how you can combine the full power of Java&trade; EE 8 with the lightweight APIs of MicroProfile to implement cloud-native microservices using Java&trade; technology.</p>
	<p>One last tip: Make sure to join the <a href="https://accounts.eclipse.org/mailing-list/jakarta.ee-community" target="_blank">Jakarta EE Community Mailing list</a> to always stay up-to-date on the latest development of Jakarta EE.</p>
	
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/ivar.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Ivar Grimstad<br>
            <a target="_blank" href="https://www.cybercom.com/">Principal Consultant at Cybercom Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ivar_grimstad">Twitter</a><br>
          <a href="http://www.agilejava.eu/2018/11/14/while-waiting-for-jakarta-ee/" target="_blank">Original Article</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>