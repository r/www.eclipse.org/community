<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/chart.png"></p>
	<p>Ensuring trouble-proof 24/7 service delivery is among of the most discussed areas in cloud hosting for the last few years, and the very obvious and commonly used solution here is building a clustered infrastructure for your project.</p>
	<p>Intending to help our customers to deal with such a non-trivial task and save time for other project-related activities, today we are glad to present a special high-availability solution, designed to facilitate the Java EE, going forward Jakarta EE, application hosting, - embedded <strong>Auto-Clustering</strong> for GlassFish, going forward <strong>Eclipse GlassFish</strong>, and <strong>Payara</strong> application servers.</p>
	<p>The main advantage of this solution is in the automatic interconnection of multiple application server instances upon the application topology change, which implements the commonly used clustering configuration.</p>
	<p>So, the article below describes how the Glassfish and Payara auto-clustering works, as well as infrastructure topology specifics and the way you can get the appropriate development and production environments up and running inside Jelastic PaaS.</p>
	
<h3>How the Auto-Clustering for GlassFish and Payara Works</h3>	
	<p>In the most general sense, any "clusterized solution" can be defined as a set of interconnected instances that run the same stack and operate the same data. In other words, this means that the corresponding server should be <a href="https://docs.jelastic.com/app-server-scaling" target="_blank">horizontally scaled</a> and share user sessions.</p>
	<p>Starting with the Jelastic 5.5.3 version, a new Auto-Clustering feature is introduced allowing to enable clusterization of the GlassFish and Payara instances directly within the topology wizard:</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/topology.png"></p>
	<p>Choose either <em>GlassFish</em> or <em>Payara</em> application server on the <strong>Java</strong> tab of the wizard. Then, in the central part, locate and enable the appropriate <strong><em>Auto-Clustering</em></strong> switcher. Configure the remaining settings up to your need (consider <a href="https://docs.jelastic.com/horizontal-scaling" target="_blank">horizontal scaling</a> to get a reliable solution from the start).</p>
	<p style="border:2px; border-style:solid; border-color:#000; padding: 1em;">Tip: The <strong><em>Auto-Clustering</em></strong> feature is also available for some other software templates (e.g. <em>MySQL, MariaDB,</em> and <em>Couchbase</em>).</p>
	<p>Based on your environment purpose, you may consider not to use <em>Auto-Clustering</em> (for example during development). In such a way a regular standalone server(s) will be created without configuring a cluster.</p>
	<p>For production, clustering is virtually a mandatory option to ensure your application high-availability and smooth/uninterrupted experience for clients. The usage of the <em>Auto-Clustering</em> by Jelastic is the simplest way to implement a reliable topology for your services without a necessity to manually configure anything. Herewith, the following adjustments take place:</p>
	<ul>
		<li><em>for 2+ GlassFish (Payara) instances</em>, environment topology is complemented with a load balancer (LB), intended to handle the incoming requests and distribute them across the workers</li>
		<li>An extra Domain Administration Server (<a href="https://docs.oracle.com/cd/E19159-01/819-3680/abfbb/index.html"><strong>DAS</strong></a>) node is automatically added - a dedicated instance to perform centralized control of cluster nodes and to configure interaction between them via SSH. Its integration implies a number of specifics:
		<ul>
			<li>administration server is linked to all workers within the application server layer with the <em>DAS</em> alias hostname, which can be used by workers for further interaction</li>
			<li>to enable proper nodes connectivity and control, the system automatically generates an SSH keypair for <em>DAS</em> node and places it within a <a href="https://docs.jelastic.com/docker-volumes" target="_blank">volume</a>, mounted to all the rest of cluster instances</li>
		</ul>
		</li>
	</ul>
	<br>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/cluster.png"></p>
	
<h3>Session Replication Implementation</h3>	
	<p>To ensure high availability of your cluster, the Jelastic PaaS automatically configures session replication across the worker nodes. This way, all user session data, that is stored during its processing, is distributed across all application server instances from the node that has actually handled the request. </p>
	<p>Together with automatically configured sticky sessions mechanism on the <a href="https://docs.jelastic.com/shared-load-balancer" target="_blank">load balancer</a> layer, session replication ensures hosting of the increased reliability and improves failover capabilities of your application within such GlassFish or Payara cluster. Herewith, depending on a used stack, the implemented replication mechanism will slightly differ - let's consider each approach in more details.</p>

<h3>GlassFish Session Replication with GMS</h3>
	<p>Within the GlassFish cluster, session replication is powered by the Group Management Service (<a href="https://docs.oracle.com/cd/E19879-01/821-0182/gjfnl/index.html" target="_blank">GMS</a>) - a built-in application server component that ensures failover protection, in-memory replication, transaction and timer services for cluster instances.</p>
	<p>GMS uses <a href="https://docs.oracle.com/cd/E26576_01/doc.312/e24934/clusters.htm#GSHAG485" target="_blank">TCP without multicast</a> to detect cluster instances. When a new node is joining a GlassFish cluster, the system re-detects all running workers and DAS node - such <a href="https://docs.oracle.com/cd/E26576_01/doc.312/e24934/clusters.htm#CHDIGFCG" target="_blank">auto discovery</a> mechanism is applied by means of the <strong>GMS_DISCOVERY_URI_LIST</strong> property being set to the <strong><em>generate</em></strong> value.</p>

<h3>Payara Session Replication with Hazelcast</h3>
	<p>Session replication inside the Payara cluster is based on Hazelcast, which has an extra benefit of being JCache compliant and provides the embedded Web and EJB sessions' persistence. This in-memory data grid is automatically enabled at all Payara instances to discover your environment cluster members by <a href="" target="_blank">TCP without multicast</a>. </p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/config.png"></p>
	<p>To manage Hazelcast settings, access the Administration Console and refer to the <a href="https://payara.gitbooks.io/payara-server/content/v/162/documentation/extended-documentation/hazelcast.html#4-configuring-hazelcast" target="_blank">Hazelcast Configuration</a> page.</p>

<h3>Deploy Example Application for HA Testing</h3>
	<p>Now, let's check the high availability of such automatically composed cluster with the example of scaled GlassFish server. To make sure of its fault tolerance, we'll deploy a dedicated testing application, which enables to add some custom session data and to view the detailed information on a server this session is handled by. This way, stopping particular cluster instances allows ascertaining that the already running user sessions will continue being processed even in case the corresponding server fails. So, let's see it in practice.</p>
	<p>1. Click Open in browser next to your environment to access the application server start page.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/access.png"></p>
	<p>Within the opened page, select the <strong>go to the Administration Console</strong> reference and log in with credentials, delivered to you via email upon the environment creation.</p>
	<p>2. Switch to the <strong>Applications</strong> section and upload <a href="https://github.com/jelastic-jps/glassfish/blob/master/glassfish-cluster/test-app/clusterjsp.ear?raw=true" target="_blank">clusterjsp.ear</a> application to the <strong>Packaged File to Be Uploaded to the Server</strong> location.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/server.png"></p>
	<p>3. Check to have the <strong>Availability</strong> enabled and set up <strong><em>cluster1</em></strong> as the application target, then click <strong>OK</strong> to proceed.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/targets.png"></p>
	<p>4. Now, open environment in browser and append <strong><em>/clusterjsp</em></strong> to the URL.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/hapjsp.png"></p>
	<p>Provide any custom <em>Name</em> and <em>Value </em>for your own session attribute and click on <strong>Add Session Data</strong>.</p>
	<p>5. Switch back to the admin panel and navigate to the <strong>Clusters &gt; cluster1 &gt; Instances </strong>tab. Here, select and <strong>Stop </strong>the instance your session is running on (its hostname is circled in the image above).</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/clusters2.png"></p>
	<p>6. Return to our application and <strong>Reload Page </strong>with the appropriate button.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/hajsp.png"></p>
	<p>As you can see, despite the session being handled by another instance, our custom attribute is still output.</p>
	
	<div style="border:2px; border-style:solid; border-color:#000; padding: 1em;"><p><strong>Tip:</strong> All replication settings are available at the <strong>Configurations &gt; cluster1-config &gt; Availability Service</strong> section of the server admin panel. Here, you can see the following replication modes being enabled by default:</p>
		<ul>
			<li><em>Web Container Availability</em></li>
			<li><em>EJB Container Availability</em></li>
		</ul>
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/container.png"></p>
	</div>

<h3>Cloning Cluster for A/B Testing</h3>	
	<p>When releasing a new application version or just applying some essential adjustments, it&rsquo;s a good practice to check how the newly implemented changes could affect the service work and your users&rsquo; appeal. The Jelastic PaaS allows you to accomplish such testing &lsquo;on-fly&rsquo; (i.e. without service downtime and implicitly for your customers) with the <strong>Clone Environment</strong> option.</p>	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/clone.png"></p>
	<p>As a result, a ready-to-work cluster copy will be created, with all the required modifications being already applied. To be more precise, this means that a cloned DAS node operates with the appropriate cloned workers, which are already listed within its admin panel, and all applications from the original environment are deployed to the cloned one as well. Thus, the only thing that remains for you to do is to recheck your app's code &amp; custom server configurations for the hardcoded IPs/domains and fix them accordingly, if are any. </p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/clonecluster.png"></p>
	<p>This way, you can apply the implied changes to your environment copy without affecting the actual production one.</p>
	<p>Subsequently, you can also evaluate the productivity and effectiveness of the modified application version comparing to the current, original one, i.e. to perform so-called <em>A/B Testing</em>. At Jelastic PaaS, this can be implemented with a special supplementary <a href="https://docs.jelastic.com/traffic-distributor" target="_blank">Traffic Distributor</a> add-on.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/traffic.png"></p>
	<p>Being placed in front of a pair of environments with the <em>Sticky Sessions</em> mode chosen, it provides smart routing of the incoming requests according to the stated backends weight. For more details on a proper TD configuration in this case, refer to the dedicated <a href="https://docs.jelastic.com/ab-testing" target="_blank">A/B Testing</a> guideline.</p>

<h3>...and a Few Useful Tips</h3>
	<p>When your GlassFish or Payara cluster is set up and you&rsquo;ve ensured everything works as intended, you could also consider the hints below to get the maximum efficiency of its running inside the Jelastic Cloud with the extensive platform functionality:</p>
		<ul>
			<li>For optimized resource consumption, set <a href="https://docs.jelastic.com/automatic-horizontal-scaling" target="_blank">auto-scaling triggers</a> within your environment settings so that nodes will be automatically added/removed within a cluster depending on the incoming load.</li>
		</ul>
		<ul>
			<li>For connection with any <a href="https://docs.jelastic.com/software-stacks-versions#databases" target="_blank">database software stack</a>, the cluster requires the appropriate libraries being integrated to its Administration Server - the most popular ones are available by default at all newly created GF/Payara nodes. And if operating with legacy instances, make sure the <em>/opt/glassfish/glassfish/domains/domain1/lib</em> DAS directory contains the appropriate files (otherwise - just upload them to the mentioned location manually).</li>
		</ul>

<h3>Conclusion</h3>
	<p>We hope the described <a href="https://github.com/eclipse-ee4j/glassfish" target="_blank">GlassFish</a> &amp; <a href="https://www.payara.fish/" target="_blank">Payara</a> cluster implementation details were clear enough for you to decide this solution is the one you need. Give it a try with creating your own cluster at one of <a href="https://jelastic.cloud/" target="_blank">Jelastic Cloud Platforms</a> during a free trial period.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/RS.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Ruslan Synytsky<br>
            <a target="_blank" href="https://jelastic.com/">Jelastic CEO</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/Jelastic">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>