<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Before CodeOne and EclipseCon this October, the Jakarta EE Steering Committee issued a <a href="https://docs.google.com/document/d/1y-Vs4d9Iotw0HqsiTxG5UCm7ua0w35vJZkGVmS_hFrw/edit?usp=sharing" target="_blank">call to action</a> for the community to share their individual visions for the future of Jakarta EE.  The community did not disappoint.</p>
	<p>Over 70 responses were given to 4 questions by 27 Jakarta EE visionaries totaling over 6000 words of tersely written quotes.  As representing this potent vision is more an act of decompression than distilling, we'll extract their vision in a series of focused but hydrated articles we can all digest.</p>
	<p>Hands down the loudest and most detailed answers revolved around pushing CDI to the far corners of Jakarta EE, platform- wide, as the single and only component model for all specifications.  An overwhelming 19 of the 27 voices shared their vision for how CDI should rule the Jakarta EE world.  <a href="https://twitter.com/l33tj4v4" target="_blank">Steve Millidge</a> of Payara put it well stating, "All the specifications need work to coalesce around CDI as the baseline bean model this will drive out complexity and duplication making the Jakarta EE platform light-weight and internally consistent."</p>
	<p>Specific areas of the platform that could change to adopt or leverage CDI included:</p>
		<ul>
			<li>JMS allowing messages to be consumed by components other than EJB Message-Driven Beans. "The work to create a CDI based JMS listener started in JMS 2 but never finished", notes <a href="https://twitter.com/reza_rahman" target="_blank">Reza Rahman</a>.</li>
			<li>JAX-RS committers "want to get rid of the ancient JAX-RS DI mechanics and replace it with CDI" according to Markus Karg and echoed by <a href="https://twitter.com/DaschnerS" target="_blank">Sebastian Dashner</a>, <a href="https://twitter.com/emilyfhjiang" target="_blank">Emily Jiang</a> and several other project members. <a href="https://twitter.com/spericas" target="_blank">Santiago Pericas-Geertsen</a> notes CDI beans can currently leverage JAX-RS, but "the combination of two injection frameworks creates some ugly edge cases that cannot be easily resolved" such as which should handle constructor injection.</li>
			<li>"JCA is an incredibly powerful API for connecting to many different enterprise systems" states Steve Millidge, indicating realigning it on CDI can enable better connectivity to systems such as Apache Kafka or Cloud Messaging systems.  Noted in other answers, though JCA was greatly improved in Java EE 7 it is tied to MDBs which have no clearly defined lifecycle and require EJB.</li>
			<li>"EJB and CDI are redundant in many areas, it would be great to eventually build out the missing and necessary parts of the EJB spec into CDI, so that EJB could be phased out" answered <a href="https://twitter.com/javajuneau" target="_blank">Josh Juneau</a>. An aggressive sentiment echoed by a number of community voices.</li>
		</ul>
	<p>While the love for CDI is clear, <a href="https://twitter.com/struberg" target="_blank">Mark Struberg</a> and spec lead <a href="https://twitter.com/antoine_sd" target="_blank">Antoine Sabot-Durand</a> caution that CDI should not become the next EJB and that CDI's SPI should be used to make those integrations.  Sabot-Durand adds his vision for CDI evolution involves flushing out that SPI and "could also focus on more asynchronous support and see how CDI events could be enhanced to be more powerful."</p>
	<p>Passion for pushing powerful asynchronous support into CDI was shared very notably by <a href="https://twitter.com/clementplop" target="_blank">Clement Escoffier</a> of Eclipse Vert.x, who stated despite being a "CDI newcomer or even a CDI noob" he believes CDI can embrace reactive and indicates his commitment to helping it get there.  It will be a bit of work, but "without challenges, life would be boring", said Escoffier.</p>
	<p>Laird Nelson shared some radical ideas for CDI itself, indicating that CDI could be the definitive API for bootstrapping servers in a way that allows developers control of public static void main, including "standardized propagation of command line arguments into the CDI environment."  Similar command-line argument ideas have floated around the Eclipse MicroProfile Config project, which is a good something in this space is bound to take flight.</p>
<h3>From Application Framework to Server Framework</h3>
	<p>One thing is clear.  For all these specifications to align with CDI, implementers will be forced to recast their code into CDI extensions using the SPI.  CDI will transition from the API used by developers to the API used to build servers, making it the SystemD and SysV of Jakarta EE, forcing it to solve similar issues such as extension start order.</p>
	<p>Will we see CDI expand from DI framework to kernel?  Very possibly.</p>
	<p>If CDI becomes our future server-building framework, it will be because of the vision of all 27 community voices that pointed to CDI in 2018 and as the first act of the Jakarta EE community, declared it king.</p>
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/david.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 David Blevins<br>
            <a target="_blank" href="https://www.tomitribe.com">Founder, CEO at Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/dblevins">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>