<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Jakarta EE takes JSON support by Java EE 8 to the next level. JavaScript Object Notation (JSON) is an open format that uses mostly human-readable text to transmit data as attribute/value pairs and arrays of such types in a serializable form.</p>
<h3>JSON Processing</h3>
	<p>JSON Processing (JSON-P) is a Java API to process (e.g. parse, generate, transform and query) JSON messages. It produces and consumes JSON text in a streaming fashion (similar to StAX API for XML) and allows to build a Java object model for JSON documents using API classes that offer strong typing (similar to DOM API for XML).</p>
	<p>The JSON Processing Standard has been part of the Java Enterprise platform since Java EE 7. In addition to the reference implementation within the Glassfish project, the standard has since been implemented by several other projects like Apache Johnzon or Genson. Many commercial products based on Java EE use either the Glassfish SI (Specification Implementation) directly, because apart from the JSON API itself, it has no dependencies on other parts of Java EE, Jakarta EE or Glassfish. So, it can be easily used on a desktop or in a "serverless" environment. That's why the JSR as an alternative to Jackson is also supported by several popular Java frameworks like Spring. While Jackson itself does not directly implement the JSON-P standard yet, there are both Jackson types for JSR 353, which practically anticipated both JSON standards in Java EE 8, as well as a JSR 353 implementation based on Jackson.</p>
	<p>Here's a simple example on how to create a JSON string using JsonObjectBuilder:</p>
<pre style='color:#000000;background:#ffffff;'>
public static void main (String[] args) {

    JsonObject json = Json.createObjectBuilder()
      .add("name", "Falco")
        .add("age", BigDecimal.valueOf(3))
      .add("bitable", Boolean.FALSE).build();    
     String result = json.toString();
     System.out.println(result);    
    }
}
</pre>
	<p>The resulting JSON string:</p>
<pre style='color:#000000;background:#ffffff;'>
{
    "name": "Falco",
    "age": 3,
    "bitable": false
}
</pre>
	<p>For dynamic JSON content which may change regularly or when dealing with very large JSON documents, using the JSON Streaming API is usually the more flexible option.</p>
	<p>Here's a very basic example on using the JSON Streaming API:</p>
<pre style='color:#000000;background:#ffffff;'>
public static void main (String[] args) {
   final String result = 
     "{\"name\":\"Falco\",\"age\":3,\"bitable\":false}";
   final JsonParser parser = Json.createParser(new StringReader(result));
        while (parser.hasNext()) {
            final Event event = parser.next();
            switch (event) {
                case KEY_NAME:
                    String key = parser.getString();
                    System.out.println(key);
                    break;
                case VALUE_STRING:
                    String string = parser.getString();
                    System.out.println(string);
                    break;
                case VALUE_NUMBER:
                    BigDecimal number = parser.getBigDecimal();
                    System.out.println(number);
                    break;
                case VALUE_TRUE:
                    System.out.println(true);
                    break;
                case VALUE_FALSE:
                    System.out.println(false);
                    break;
            }
        }
        parser.close();
    }
}
 
Output: 
name
Falco
age
3
bitable
false
</pre>

<h3>New Features</h3>
	<p>The new features added with JSON Processing 1.1 are:</p>
		<ul>
			<li>JSON Pointer</li>
			<li>JSON Patch</li>
			<li>JSON Merge Patch</li>
			<li>Java 8 Support</li>
			<li>JSON Big Data</li>
		</ul>

<h3>JSON Pointer</h3>
	<p>JSON Pointer provides a syntax to identify certain elements of a JSON document, e.g. "/phone/mobile" or "/users/0", JSON Pointer offers a similar user experience as common REST URLs making it is a good match to use with REST APIs.</p>
	<p>A JSON Pointer example:</p>
<pre style='color:#000000;background:#ffffff;'>
[
  {   
    "name": "Jason Voorhees",
    "profession": "Maniac killer",
    "age": 45
  },
  {
    "name": "Jason Bourne",
    "profession": "Maniac killer",
    "age": 35
  }
]
 
JsonArray jasons = . . .;
JsonPointer pointer = Json.createPointer("/1/profession");
JsonValue profession = pointer.getValue(jasons);
p.replace(jasons, Json.createValue("Super agent"));
</pre>

<h3>JSON Patch</h3>
	<p>JSON Patch allows modification of a JSON document with one of the following operations:</p>
	<ul>
		<li>Add</li>
		<li>Remove</li>
		<li>Replace</li>
		<li>Move</li>
		<li>Copy</li>
		<li>Test</li>
	</ul>
	<p>Most of them correspond to HTML operations e.g. REMOVE can be seen as an equivalent to HTTP DELETE.</p>
	<p>A JSON patch operation is atomic and the patch should only be applied if all operations are safe and easy to use. The TEST operation may provide additional validation to ensure that pre- or post-conditions for the patch are met. If the test fails, the entire patch is discarded. So TEST can viewed a bit like a unit test.</p>
	<p>JSON Patch Before:</p>
<pre style='color:#000000;background:#ffffff;'>
[
  {   
    "name": "Jason Voorhees",
    "profession": "Maniac killer",
    "age": 45
  },
  {
    "name": "Jason Bourne",
    "profession": "Maniac killer",
    "age": 35
  },
  {
    "name": "James Bond",
    "profession": "Agent 007",
    "age": 40
  }
]
</pre>
	<p>Applying JSON Patch:</p>
<pre style='color:#000000;background:#ffffff;'>
[
  { "op": "replace",
    "path": "/1/profession",
    "value": "Super agent"},
   { "op": "remove",
    "path": "/2"}
]
 
JsonArray agents = ...;
JsonArray patch  = ...;
JsonPatch jsonPatch = Json.createPatch(patch);
JsonArray result = jsonPatch.apply(agents);
JsonPatchBuilder builder = Json.createPatchBuilder();
JsonArray result = builder.replace("/1/profession", "Super agent")
.remove("/2").apply(agents);
</pre>
	<p>JSON Patch After:</p>
<pre style='color:#000000;background:#ffffff;'>
[
  {   
    "name": "Jason Voorhees",
    "profession": "Maniac killer",
    "age": 45
  },
  {
    "name": "Jason Bourne",
    "profession": "Super agent",
    "age": 35
  }
]
</pre>
	<p>The patch itself is also a JSON document and can be combined with the HTTP <em>PATCH</em> operation and a special media type "<strong>application/json-patch+json</strong>". Making JSON Patch also a great companion to REST APIs and Microservices.</p>
	<p>The "<strong>path</strong>" expression of a JSON Patch indicates the use of a JSON Pointer. And shows how the two standards work hand-in-hand.</p>

<h3>JSON Merge Patch</h3>
	<p>JSON Merge Patch rounds up the newly supported standards for the modification of JSON documents in JSON Processing. JSON Merge Patch is intended primarily for use with the HTTP PATCH method to describe a number of changes to the destination resource in JSON format.</p>
	<p>JSON Processing supports two ways to create a JSON Merge Patch:</p>
		<ol>
			<li>A new <em>JsonMergePatch</em> based on an existing JSON Merge Patch.</li>
			<li>A new <em>JsonMergePatch</em> from the delta of two JsonValue objects.</li>
		</ol>
<h3>Other Features</h3>
	<p>Furthermore, JSON Processing 1.1 adds Java 8 support like Lambdas and Streams. As well "Big JSON" features allowing to parse very large JSON documents more efficiently. For this purpose, two methods were added to JsonParser:</p>
		<ul>
			<li><em>skipArray <br>
			Skips all structures until the next <strong>END_ARRAY</strong> position</em></li>
			<li><em>skipObject <br>
			Skips all characters until the next <strong>END_OBJECT</strong> position</em></li>
		</ul>

<h3>Future Plans</h3>
	<p>For new versions of the JSON Processing spec in upcoming releases of Jakarta EE, potentially JSON Processing 2.0 there are several ideas, improved support for Java primitive types for building JSON values or other Java types, for example Date/Time values. Our JsonObjectBuilder example could then look somewhere like:</p>
<pre style='color:#000000;background:#ffffff;'>
    JsonObject json = Json.createObjectBuilder()
      .add("name", "Falco")
      <strong>.add("dob", LocalDate.of(1957, 2, 19))</strong>
      .add("bitable", Boolean.FALSE).build();    
</pre>
	<p>The resulting JSON string still has to comply with the JSON standard which is not aware of date, time or other complex data types, but the API should add some convenience taking those types as arguments for creating JSON values.</p>
	<p>See <a href="https://github.com/eclipse-ee4j/jsonp/milestone/8" target="_blank">https://github.com/eclipse-ee4j/jsonp/milestone/8</a> for a list of JSON-P 2.x feature candidates.</p>

<h3>JSON Binding</h3>
	<p>JSON Processing 1.1 provides the basis for the new standard JSON Binding 1.0, which was first introduced with Java EE 8. JSON Processing provides generic low-level access and the basis for type-safe mapping and binding of Java objects to JSON documents. JSON-B is similar to JAXB for XML documents.</p>
	<p>JSON-B is a standard binding framework for converting Java objects to and from JSON documents. It defines a standard mapping algorithm for transforming existing Java classes into JSON, while allowing developers to customize the mapping process through the use of Java annotations.</p>
	<p>JSON-B is consistent with JAXB (Java API for XML Binding) and other Java EE and SE APIs where it makes sense and is possible. Use of the Builder Pattern is similar to the underlying JSON-P standard. With a few deviations. Mostly the use of Java SE 8, while JSON-P version 1.0 was still based on Java 7. The central Json class in JSON-P is a static facade, whereas Jsonb is an interface created by the static factory call <em>JsonbBuilder.create().</em> A shortcut for <em>JsonbBuilder.newBuilder().build().</em> As Java 8 introduced static methods to interfaces, <em>JsonbBuilder</em> is also an interface.</p>
	<p>Here's a simple example. We model a dog in a domain class <em>Dog</em>:</p>
<pre style='color:#000000;background:#ffffff;'>
public class Dog {
    public String name;
    public int age;
    public boolean bitable;
}
</pre>
	<p>Calling the JSON-B API:</p>
<pre style='color:#000000;background:#ffffff;'>
public static void main(String[] args) {
// Create a dog instance
Dog dog = new Main.Dog();
dog.name = "Falco";
dog.age = 3;
dog.bitable = false;
 
// Create Jsonb and serialize
Jsonb jsonb = JsonbBuilder.create();
String result = jsonb.toJson(dog);
 
System.out.println(result);
 
// Deserialize back
dog = jsonb.fromJson("{\"name\":\"Falco\",\"age\":3,\"bitable\":false}", Dog.class);
}
</pre>
	<p>The JSON string is identical to our initial JSON Processing example.</p>
	<p>Collections like lists can also be serialized and deserialized:</p>
<pre style='color:#000000;background:#ffffff;'>
// List of dogs
List&lt;Dog&gt; dogs = new ArrayList&lt;&gt;();
dogs.add(falco);
dogs.add(cassidy);
 
// Create Jsonb and serialize
Jsonb jsonb = JsonbBuilder.create();
String result = jsonb.toJson(dogs);
 
// Deserialize back
dogs = jsonb.fromJson(result, new ArrayList&lt;Dog&gt;(){}.getClass().getGenericSuperclass());
</pre>
	<p>JSON Binding support all relevant data types of the JDK. Both traditional java.util.Calendar or Date and the new types like Duration or LocalDateTime introduced with Java 8.</p>
	<p>Default mapping and behaviour can be overridden via <em>JsonbConfig</em>.</p>
	<p>Or by using annotations. Here's an example:</p>
<pre style='color:#000000;background:#ffffff;'>
public class Person {
    @JsonbProperty("person-name")
    private String name;
 
    private String profession;
}
</pre>
	<p>The <em>@JsonProperty</em> annotation has a slight similarity with the @Column Annotation of JPA entities. The JSON result looks like this:</p>
<pre style='color:#000000;background:#ffffff;'>
{
    "person-name": "Jason Bourne",
    "profession": "Super Agent"
}
</pre>
	<p>JSON-B tries to use familiar elements and practices, therefore a JPA entity could even combine the two annotations if there is a use case for both. Other Java EE standards like Bean Validation work with both as well.</p>

<h3>Summary</h3>
	<p>The JSON format became a first-class citizen with Java EE 8. REST APIs and similar services often prefer JSON over heavier formats like XML, to save data and bandwidth in the cloud, where transmitting more information can often be more expensive when billing based is based on data access. New security protocols such as JWT (JSON Web Token) also use the JSON format. While this is currently explored by Eclipse MicroProfile, it is possible, that JWT support also becomes part of a future Jakarta EE JSON specification or an update to existing ones. With synergies from other Jakarta EE standards like Enterprise Security.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/werner.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Werner Keil<br>
            <a target="_blank" href="http://www.catmedia.us/">Creative Arts & Technologies</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/wernerkeil">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>