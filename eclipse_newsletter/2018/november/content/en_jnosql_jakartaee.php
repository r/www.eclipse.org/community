<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The Enterprise Java platform has been evolving steadily since 1999 when it was first introduced as Java 2, Enterprise Edition. Today, Enterprise Java is being standardized under the Eclipse Foundation with the new brand, Jakarta EE.  Jakarta EE picks up where Java EE 8 left off, but the roadmap going forward will be focused on modern innovations such as microservices, modularity, and, now, NoSQL databases.</p>
	<p>The <a href= "http://www.jnosql.org/" target="_blank">JNoSQL project</a>, of which I'm a part, is excited to announce that it will be the first new standardization project to be adopted by Jakarta EE providing a robust and vendor agnostic API that will allow any Jakarta EE implementation to seamlessly integrate with most NoSQL databases.</p>
<h3>What is NoSQL?</h3>
	<p>The NoSQL databases are a new approach to data persistence that goes beyond relational database. NoSQL databases offer both performance and scalability that is often better suited to today's dynamic and large-scale data storage and access needs. A particularly good article to get you started with NoSQL databases is available <a href="https://dzone.com/refcardz/nosql-and-data-scalability-20?chapter=1" target="_blank">here</a>.</p>
	<p>NoSQL databases have grown quickly in popularity and have been used extensively in a number of industries including finance.  According to the <a href="https://db-engines.com/en/ranking" target="_blank">DB-Engine</a> ranking, four of the ten databases listed are NoSQL.  Today there are more than <a href="http://nosql-database.org/" target="_blank">two hundred and twenty-five NoSQL databases</a> to choose from.</p>
<h3>Java EE and NoSQL</h3>	
	<p>The rapid adoption of NoSQL combined with the vast assortment of implementations has driven a desire to create a set of standardized APIs.  In the Java world, this was initially proposed in an <a href="https://javaee.github.io/javaee-spec/download/JavaEE9.pdf" target="_blank">effort</a> by Oracle to define a NoSQL API for Java EE 9.  The justification for the definition of a new API, separate form JDBC, and JPA was the following.</p>
	<ul>
		<li>JPA was not designed with NoSQL in mind.</li>
		<li>A single set of APIs or annotations isn't adequate for all database types.</li>
		<li>JPA over NoSQL implies the inconsistent use of annotations.</li>
		<li>The diversity in the NoSQL world matters.</li>
	</ul>
	<p>Unfortunately, the Oracle proposal for Java EE 9 was not completed when Java EE was donated to the Eclipse Foundation.</p>
<h3>Jakarta EE and NoSQL</h3>
	<p>To bring the NoSQL innovation to the Jakarta EE platform, the Eclipse Foundation's JNoSQL project will be the foundation of a new standardized Java API for NoSQL databases. The goals for the JNoSQL project is to be the foundation for the  definition a Java-based NoSQL API standard that enables Jakarta EE platforms to:</p>
	<ul>
	<li>Support the four NoSQL types (key-value, column family, document and Graph)</li>
	<li>Implement a Java NoSQL  communication API (analogous to JDBC) for each storage category  - the Graph it will use <a href="http://tinkerpop.apache.org/" target="_blank">Apache Tinkerpop</a></li>
	<li>Provide a Mapping API (analogous to JPA) for each supported type, with a shared set of annotations.</li>
	<li>Define a standard set of annotations that are useful across both communication and mapping APIs.</li>
	</ul>
	<p>To learn more about Eclipse JNoSQL, <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/april/" target="_blank">click the link of this article</a>.</p>
	
<h3>Wrapping Up</h3>
	<p>The adoption of JNoSQL as the foundation of the first new standard added to the Jakarta EE platform demonstrates the Eclipse Foundation's commitment to the modernization of Enterprise Java.  The JNoSQL project looks forward to contributing to the Jakarta EE platform and working with the new Jakarta EE Working Group to integrate and standardize JNoSQL as an API for NoSQL databases.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/otavio.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Ot&aacute;vio Gon&ccedil;alves de Santana<br>
            <a target="_blank" href="https://www.tomitribe.com">Software Engineer at Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/otaviojava">Twitter</a><br>
          <a href="https://www.tomitribe.com/blog/jnosql-and-jakarta-ee/" target="_blank">Original Article</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>