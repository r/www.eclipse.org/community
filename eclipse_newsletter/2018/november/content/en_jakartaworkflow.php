<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>In this short tutorial, I would like to show you how you can run a human-centric workflow engine on Jakarta EE. Jakarta EE is the successor of Java EE and like the previous version, Jakarta EE offers you a full stack Java platform for enterprise applications. The Open Source Workflow Engine <a href="http://www.imixs.org/" target="_blank">Imixs-Workflow</a> is based on this platform from the early beginning of Java EE.</p>
	<p>Imixs-Workflow provides you with a powerful, scalable and transactional workflow engine for Java Enterprise Applications. You can embed the workflow engine in your Jakarta EE project or run the engine as a Microservice based on Jakarta EE.</p>
	<p>The idea of the project is to move most of the usual business logic into a model. As a result, you can change and optimize your application in a model-driven way. The project supports the <strong>Business Process Modeling Notation (BPMN 2.0)</strong>. BPMN enables you to describe your business process from different perspectives. You can describe the organizational aspects just to give people an understanding of your process. And you can as well model the technical details to execute your process with the Imixs-Workflow engine.</p>

<h3>Create your Workflow Model</h3>
	<p>Before we start we create a Workflow Model describing our business process. You create an Imixs-Workflow model with the Eclipse-based modeling tool Imixs-BPMN. In Imixs-Workflow the process status is described with the BPMN element 'Task'. The status change between the Task elements is defined by BPMN element 'Event'.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/graph.png"></p>
	<p>For each task element, you can define the responsibilities and access control list (ACL). When you later start the workflow, each process instance will be automatically assigned to the roles defined by your model.</p>
	<p>You can download an example model from the <a href="https://github.com/imixs/imixs-microservice" target="_blank">Imixs-Microservice project on Github</a>.</p>
	
<h3>The Workflow Engine</h3>
	<p>Imixs-Workflow acts as a service within the Jakarta EE component model. This means you can run the engine on any Jakarta EE compliant application server. Currently, you can choose from the following servers:</p>
	<ul>
		<li><a href="http://wildfly.org/" target="_blank">JBoss/Wildfly</a></li>
		<li><a href="http://www.payara.org/" target="_blank">Payara</a></li>
		<li><a href="https://openliberty.io/" target="_blank">Open Liberty</a></li>
		<li><a href="http://tomee.apache.org/" target="_blank">Apache TomEE</a></li>
	</ul>
	<p>You can integrate the Imixs-Workflow engine into your project just by adding the corresponding maven dependency.</p>
<pre style='color:#000000;background:#ffffff;'>
&lt;dependency&gt;
  &lt;groupId&gt;org.imixs.workflow&lt;/groupId&gt;
  &lt;artifactId&gt;imixs-workflow-engine&lt;/artifactId&gt;
  &lt;version&gt;${org.imixs.workflow.version}&lt;/version&gt;
&lt;/dependency&gt;
&lt;dependency&gt;
  &lt;groupId&gt;org.imixs.workflow&lt;/groupId&gt;
  &lt;artifactId&gt;imixs-workflow-jax-rs&lt;/artifactId&gt;
  &lt;version&gt;${org.imixs.workflow.version}&lt;/version&gt;
&lt;/dependency&gt;
</pre>
	<p>Take a look at the <a href="https://github.com/imixs/imixs-workflow/releases" target="_blank">release list</a> to find out the latest version.</p>
	<p>The component imixs-workflow-jax-rs provides a Rest API. With this API you can run Imixs-Workflow as a microservice. The Rest API is also used to upload your workflow model into the workflow engine. You can read more about the Rest API of Imixs-Workflow <a href="https://imixs.org/doc/restapi/index.html" target="_blank">here</a>.</p>

<h3>Lucene Search</h3>
	<p>A workflow engine typically manages unstructured data in the form of documents. The full-text search of Lucene is used to search this data. You need to add the lucene search engine as well with the following maven dependencies:</p>
<pre style='color:#000000;background:#ffffff;'>
&lt;dependency&gt;
  &lt;groupId&gt;org.apache.lucene&lt;/groupId&gt;
  &lt;artifactId&gt;lucene-core&lt;/artifactId&gt;
  &lt;version&gt;7.5.0&lt;/version&gt;
&lt;/dependency&gt;
&lt;dependency&gt;
  &lt;groupId&gt;org.apache.lucene&lt;/groupId&gt;
  &lt;artifactId&gt;lucene-analyzers-common&lt;/artifactId&gt;
  &lt;version&gt;7.5.0&lt;/version&gt;
&lt;/dependency&gt;
&lt;dependency&gt;
  &lt;groupId&gt;org.apache.lucene&lt;/groupId&gt;
  &lt;artifactId&gt;lucene-queryparser&lt;/artifactId&gt;
  &lt;version&gt;7.5.0&lt;/version&gt;
&lt;/dependency&gt;
</pre>

<h3>The Deployment</h3>
	<p>Now its time to deploy your application. Jakarta EE offers you a full Java Enterprise stack so there is no need to add any additional libraries. For the deployment of Imixs-Workflow, there are two points that are significant to a workflow engine: persistence and security.</p>

<h3>Persistence</h3>
	<p>In Jakarta EE data can easily be stored into a database using the Java Persistence API (JPA). For this purpose, it is necessary to specify a data source. This can be done within your Application Server. Any standard SQL database can be used here.</p>
	<p>For example, in the Wildfly Application Server you can define a data-source from the web admin interface:</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/email.png"></p>
	<p>To link this data source with your application, a JPA descriptor named 'persistence.xml' need to be added into your application:</p>

<pre style='color:#000000;background:#ffffff;'>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;persistence version="1.0" xmlns="http://java.sun.com/xml/ns/persistence"&gt;
	&lt;persistence-unit name="org.imixs.workflow.jpa" transaction-type="JTA"&gt;	
		&lt;provider&gt;org.eclipse.persistence.jpa.PersistenceProvider&lt;/provider&gt;	
		&lt;jta-data-source&gt;jdbc/imixs-microservice&lt;/jta-data-source&gt;
		&lt;jar-file&gt;lib/imixs-workflow-engine-${org.imixs.workflow.version}.jar&lt;/jar-file&gt;
		&lt;properties&gt;
			&lt;!-- target-database Auto MySQL PostgreSQL  --&gt;
			&lt;property name="eclipselink.target-database" value="Auto" /&gt;
			&lt;property name="eclipselink.ddl-generation" value="create-tables" /&gt;
			&lt;property name="eclipselink.deploy-on-startup" value="true" /&gt;
		&lt;/properties&gt;				
	&lt;/persistence-unit&gt;
&lt;/persistence&gt;</pre>
	<p>This file is simply placed in the META-INF/ folder of your application or microservice. Note that in this example I use <a href="https://www.eclipse.org/eclipselink/" target="_blank">EclipseLink</a> as a JPA provider which need to be part of your server setup.</p>
	
<h3>Security</h3>
	<p>Security is the second notable aspect of Imixs-Workflow. Users need to be authenticated to access the workflow engine. On the other hand, Imixs-Workflow protects your business data in a perfect way. Like the persistence also the security is part of Jakarta EE and can be fully managed in your application server. This means you can use different authentication modules like Database-Realms, LDAP-Realms or simple File-Realms.</p>
	<p>The following example shows the definition of a file-realm in the Wildfly Application Server using again the web admin interface:</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/wildfly.png"></p>
	<p>The security realm is typically mapped by a deployment descriptor like the jboss-web.xml file:</p>
<pre style='color:#000000;background:#ffffff;'>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;jboss-web&gt;
   &lt;security-domain&gt;imixsrealm&lt;/security-domain&gt;
   &lt;context-root&gt;imixs-microservice&lt;/context-root&gt;
&lt;/jboss-web&gt;
</pre>
	<p>This descriptor is placed in the WEB-INF/ folder. You can find a complete project setup in the Imixs-Microservice project on Github. You can use this project as a template for your own application.</p>
	
<h3>Docker Support</h3>
	<p>The Imixs-Microservice project on Github provides a docker image. This makes it easy to run the Imixs-Microservice out of the box in a Docker container. You can use this image for your own project. A Docker container can also be used as a productive tool for development. To build the Docker container run:</p>

<pre style='color:#000000;background:#ffffff;'>
$ mvn clean install -Pdocker
</pre>
	<p>To start the Imixs-Microservice as a docker container you only need to define a container stack description with Docker Compose. <a href="https://docs.docker.com/compose/overview/" target="_blank">Docker Compose</a> is a tool for defining and running a stack of multiple Docker containers. The following docker compose file includes a Postgres database service and the Jakarta EE Application server Wildfly:</p>
<pre>
version: '3.1'
services:
  db:
    image: postgres:9.6.1
    environment:
      POSTGRES_PASSWORD: adminadmin
      POSTGRES_DB: workflow
  app:
    image: imixs/imixs-microservice
    environment:
      WILDFLY_PASS: adminadmin
      DEBUG: "true"
      POSTGRES_USER: "postgres"
      POSTGRES_PASSWORD: "adminadmin"
      POSTGRES_CONNECTION: "jdbc:postgresql://db/workflow"
    ports:
      - "8080:8080"
      - "9990:9990"
</pre>
	<p>With a single command, you create and start all the services with your own configuration defined in the docker-compose.yml file:</p>
<pre>
$ docker-compose up
</pre>

<h3>Testing the Workflow Engine</h3>
	<p>After you have set up your project and application server you can deploy and test your application.  As mentioned earlier, Imixs-Workflow includes a Rest API. Via this API you can transfer the model you created earlier. The following example shows how to upload the model with the <em>'curl'</em> command:</p>
<pre>
curl --user admin:adminadmin --request POST -Tticket.bpmn http://localhost:8080/imixs-microservice/model/bpmn
</pre>
	<p>The curl command includes the userId/password to authenticate against the workflow engine with the security realm we defined earlier in our application server.</p>
	<p>You can verify the status of your service by the following URL:</p>
<pre>
http://localhost:8080/imixs-microservice/model
</pre>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/workflow.png"></p>

<h3>Starting a New Process Instance</h3>
	<p>With the Imixs-Rest API, you can now start a new process instance based on your model definition. This can be done with a JSON request. See the following curl example:</p>
<pre>
curl --user admin:adminadmin -H "Content-Type: application/json" -H "Accept: application/json" -d \
       '{"item":[ \
                 {"name":"type","value":{"@type":"xs:string","$":"workitem"}}, \
                 {"name":"$modelversion","value":{"@type":"xs:string","$":"1.0.1"}}, \
                 {"name":"$taskid","value":{"@type":"xs:int","$":"1000"}}, \
                 {"name":"$eventid","value":{"@type":"xs:int","$":"10"}}, \
                 {"name":"txtname","value":{"@type":"xs:string","$":"test-json"}}\
         ]}' \
         http://localhost:8080/imixs-microservice/workflow/workitem.json
</pre>
	<p>This will create a new process instance with some business data (item 'txtname'='test-json'). The Imixs-Workflow engine will now add automatically processing information and returns a unique identifier. <a href="https://imixs.org/doc/quickstart/workitem.html" target="_blank">See more about "How to manage business data" on the project home</a>.</p>
	<p>You can test the newly created running process instances also by a URL:</p>
<pre>
http://localhost:8080/imixs-microservice/workflow/tasklist/creator/admin
</pre>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/november/images/collection.png"></p>
	
<h3>Conclusion</h3>
	<p>With this brief insight, I have shown how you can develop and operate a highly scalable workflow engine with Jakarta EE. The advantage of this platform is that many parts for a business application are already available and do not need to be installed manually. This simplifies the development of business applications and offers much more possibilities to run and operate modern applications.</p>
	<p>On the other hand, the Imixs-Workflow engine offers a model-driven approach to keep business logic flexible. This opens up a multitude of possibilities for realizing complex business processes in an easy and flexible way.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/ralph.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Ralph Soika<br>
            <a target="_blank" href="www.imixs.com">Imixs GmbH</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://ralph.blog.imixs.com">Blog</a><br>
          <a href="https://blog.imixs.org/2018/10/21/tutorial-imixs-workflow-and-jakarta-ee/" target="_blank">Original Article</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>