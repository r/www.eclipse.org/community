<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "Java Tools and Runtimes";
  $pageKeywords = "eclipse newsletter, java, jakarta ee, microprofile, openj9, vert.x, jnosql, eclipse collections";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "Read five great articles written by technical experts that feature Java Tools and Runtimes like, Eclipse MicroProfile and Eclipse Vert.x.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "Devoxx US";
  $sponsorLink = "https://devoxx.us/";
  $sponsorImage = "/community/eclipse_newsletter/assets/public/images/devoxxus_small.png";

  // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;