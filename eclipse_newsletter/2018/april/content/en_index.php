<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2018.05.04</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxannenew.png" />
                            <h3>Editor's Note</h3>
                            <p>I'm happy to share with you this great issue of the Eclipse Newsletter. This month, we have five articles written by technical experts that feature Java Tools and Runtimes.</p>
                            <p>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/microprofile.php">Building Your Next Microservice With Eclipse MicroProfile</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/vertx.php">Scalable Open APIs with Eclipse Vert.x</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/collections.php">Optimization Strategies with Eclipse Collections</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/openj9.php">Eclipse OpenJ9; not just any Java Virtual Machine</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/jnosql.php">Eclipse JNoSQL: One API to many NoSQL databases</a></p>
							<p>You may have heard the great news: leading software vendors collaborated to move Java EE technologies to the Eclipse Foundation where they will evolve under the Jakarta EE brand.</p> 
							<p>The Jakarta EE logo and website have also been announced! &#9973; View the logo below and visit the &#10140; <a style="color:#000;" target="_blank" href="https://jakarta.ee/">jakarta.ee</a> website, the new home of Cloud Native Java.</p>
							<p>Finally, be sure to join us at EclipseCon France in Toulouse, on June 13-14, to learn more about Jakarta EE and many other great Eclipse projects. <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/france2018/conference/schedule/session/2018-06-13">Check out the program</a>!</p>
                                                    
                            <p>May the fourth be with you!</p>
                            
                            <p>Roxanne Joncas <br>
                            <a style="color:#000;" target="_blank" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
              <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a target="_blank" href="https://www.jakarta.ee"><img class="img-reactive"
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/jakartaee_newsletter.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

	
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/microprofile.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/microprofile.php"><h3>Building Your Next Microservice With Eclipse MicroProfile</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/vertx.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/vertx.php"><h3>Scalable Open APIs with Eclipse Vert.x</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/collections.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/collections.php"><h3>Optimization Strategies with Eclipse Collections</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/openj9.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/openj9.php"><h3>Eclipse OpenJ9; not just any Java Virtual Machine</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
    
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/jnosql.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/april/jnosql.php"><h3>Eclipse JNoSQL: One API to many NoSQL databases</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                        <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                           
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
    
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2018/conference/schedule/session/2018-06-13">EclipseCon France - Program Announced</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/20180404_iotchallenge_winners2018.php">Winners of Open IoT Challenge 4.0 Demonstrate How Open Source Accelerates Innovation</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/downloads/packages/release/oxygen/3a">Eclipse Oxygen 3A has been released!</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://blog.benjamin-cabe.com/2018/04/17/key-trends-iot-developer-survey-2018">IoT Developer Survey 2018 | Results are in!</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://jakarta.ee/news/2018/04/24/jakarta-ee-community-survey/">Java Developers Survey results now released</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2018/04/cloud-native-java-jakarta-ee">Cloud Native Java has a new home: Jakarta EE</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipse-microprofile-intro-java-community-143818.html">Eclipse MicroProfile: What it has to offer to the Java Community</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2018/04/eclipse-photon">Eclipse Photo Nears Release</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2018/04/jakarta-ee-working-group">Jakarta EE Working Group Established</a></li>
								<li><a target="_blank" style="color:#000;" href="https://goo.gl/forms/5JVT6ztLXIEX841A2">Survey: How Eclipse Contributors Respond to Bug Reports</a></li>
								<li><a target="_blank" style="color:#000;" href="https://globenewswire.com/news-release/2018/04/10/1467810/0/en/Lightbend-Joins-Eclipse-Foundation-to-Support-Jakarta-EE-Modernization-for-Cloud-Native-Java.html">Lightbend Joins Eclipse Foundation to Support Jakarta EE Modernization for Cloud-Native Java</a></li>
							</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-fog05">Eclipse fog05</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-jakarta-ee-tck">Eclipse Jakarta EE TCK</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-ejb">Eclipse Project for EJB</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-interceptors">Eclipse Project for Interceptors</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-jacc">Eclipse Project for JACC</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-jaspic">Eclipse Project for JASPIC</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-jca">Eclipse Project for JCA</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-jsp">Eclipse Project for JSP</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-project-servlet">Eclipse Project for Servlet</a></li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.4diac/reviews/1.9.0-release-review">Eclipse 4diac 1.9</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.ease/reviews/0.6.0-release-review">Eclipse Advanced Scripting Environment (EASE) 0.6</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.app4mc/reviews/0.9.0-release-review">Eclipse APP4MC 0.9</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/aspectj/doc/released/README-190.html">Eclipse AspectJ 1.9</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.corrosion/reviews/0.1-release-review">Eclipse Corrosion: Eclipse IDE for Rust 0.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.gendoc/reviews/0.7.0-release-review">Eclipse Gendoc 0.7</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jnosql/reviews/0.0.5-release-review">Eclipse JNoSQL 0.0.5</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.kura/reviews/3.2.0-release-review">Eclipse Kura 3.2</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4j/reviews/0.4.0-release-review">Eclipse LSP4J 0.4</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.mosquitto/reviews/1.5-release-review">Eclipse Mosquitto 1.5</a></li>
								<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Nebula/Releases/2.0.0/NaN">Eclipse Nebula - Supplemental Widgets for SWT 2.0</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.orion/reviews/18.0-release-review">Eclipse Orion 18.0</a></li>
								<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/PDT/NewIn53">Eclipse PHP Developer Tools 5.3</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/eclipse/news/4.7.3a/">Eclipse Project 4.7.3a</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.rdf4j/reviews/2.3-release-review">Eclipse RDF4J 2.3</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.reddeer/reviews/2.1.0-release-review">Eclipse RedDeer 2.1</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.unide/reviews/0.2.0-release-review">Eclipse Unide 0.2</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/locationtech.geomesa/reviews/2.0.0-release-review">LocationTech GeoMesa 2.0</a></li>
                             </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/jan_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.devoxx.co.uk/">Devoxx UK</a><br>
                            May 9-11, 2018 | London, UK</p>
                            <p><a target="_blank" style="color:#000;" href="http://www.dlr.de/ts/en/desktopdefault.aspx/tabid-3930/20125_read-50294/">SUMO User Conference 2018 - Simulating Autonomous and Intermodal Transport Systems</a><br>
                            May 14-16, 2018 | Berlin, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://2018.foss4g-na.org/">FOSS4G NA</a><br>
                            May 14-17, 2018 | St. Louis, MO, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2018/">Eclipse IoT Day Santa Clara</a><br>
                            May 14, 2018 | Santa Clara, CA, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://tmt.knect365.com/iot-world/">IoT World 2018</a><br>
                            May 14-17, 2018 | Santa Clara, CA, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2018/">EclipseCon France 2018</a><br>
                            Jun 13-14, 2018 | Toulouse, France</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2018/">EclipseCon Europe 2018</a><br>
                            Oct 23-25, 2018 | Ludwigsburg, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
