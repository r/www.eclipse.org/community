<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The HTTP/2 protocol is slowly but surely eating market share of the public web and if you are browsing a website proxied by a CDN (like the <a target="_blank" href="http://vertx.io">https://vertx.io</a> website) with a modern browser, there is a high chance that your browser and the web server are talking with HTTP/2.</p>

<p>HTTP/1 was designed more than 20 years ago to render very simple web pages. Since then, the complexity of web pages has exploded, and web browsers have had to develop a variety of techniques to work around HTTP/1’s limitations; managing impressive performance that keeps load times reasonable, even for complex pages.</p>

<p>HTTP/2 was introduced a few years ago as a better transport for HTTP requests and responses. It does not actually redefine how client and server interact, but instead tries to optimize the data exchange in the most efficient possible fashion.</p>

<p>Web APIs can also benefit from the improvements provided by HTTP/2, whether it is a publicly hosted API endpoint or private interactions between microservices in the same data center.</p>

<h2>A multiplexed protocol</h2>

<p>If you need to know a single thing about HTTP/2, it is certainly that HTTP/2 is a multiplexed protocol.</p> 

<p>An HTTP/1 connection can only transport one request / response at a time. Of course, connections can be reused to avoid the creation cost of the connection, but when the client needs to achieve simultaneously several requests to the same server, it has no choice to open several connections; one for each request.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/serveroneconnection.png"></p>

<p>In addition, the client has to wait until it receives the response, with the connection remaining unused during the service time of the server. While it can be acceptable in many situations, it is certainly a very poor usage of the connection which is ultimately a resource cost for the client, the server, and the intermediaries.</p>

<p>HTTP/2 is a multiplexed protocol: multiplexed means that a single connection can transport multiple requests and responses simultaneously between the client and the server.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/servermulticonnection.png"></p>

<p>Let’s have look at the benefits of using HTTP/2.</p>

<h2>Better capacity planning</h2>

<p>The most important configuration aspect of non-multiplexed clients is to get the number of connections for the client right. A too low value creates unnecessary waiting for the client which leads to higher response times.</p>

<p>Conversely, HTTP/2 uses a single connection and the number of requests is actually defined by the server: the server tells the client how many requests it can handle simultaneously; this magic number is called the connection concurrency.</p>

<h2>Better concurrency control</h2>
<p>The server defining the concurrency gives the unique opportunity for the server to size the value according to a variety of criteria, such as the client QoS or the actual server resources.</p>

<p>The concurrency value is initially sent by the server when the connection is created and this value can be updated whenever the server needs to, e.g when the server has resources scaling down it can notify the client with a lower concurrency.</p>

<h2>Better latency</h2>
<p>The connection pool size has a direct impact on application performance and QoS: when the value is too low it creates unnecessary waiting on the client which results in higher service time from the perspective of the client.</p>

<p>With HTTP/2 server controlled concurrency, the client always uses optimal concurrency according to the server’s capacity. When the client is an intermediary like an API gateway (a glorified HTTP reverse proxy) this results in lower response times at the edge.</p>

<h2>What is Eclipse Vert.x ?</h2>

<p>Eclipse Vert.x is a toolkit to build distributed reactive systems on the top of the Java Virtual Machine using an asynchronous and non-blocking development model. As a toolkit, Vert.x can be used in many contexts: in a standalone application or embedded in a Spring application.</p> 

<p>Vert.x and its ecosystem are just jar files used as any other library: just place them in your classpath and you are done.</p>

<h2>HTTP/2 with Eclipse Vert.x</h2>

<p>Vert.x supports HTTP/2 for server and client since the version 3.3 with a variety of options:</p>

    <ul>
        <li>The h2 protocol:HTTP/2 over TLS sockets</li>
        <li>The h2c with prior knowledge protocol: HTTP/2 over plain sockets</li>
        <li>The h2c upgrade protocol: HTTP/1.1 request upgraded to the HTTP/2 protocol</li>
    </ul>
    
<p>Creating an HTTP/2 server with Vert.x is very simple</p>

<pre style="background:#fff;color:#000"><span style="color:#ff7800">public</span> <span style="color:#ff7800">class</span> <span style="color:#3b5bb5">Http2Server</span> {

 <span style="color:#ff7800">public</span> <span style="color:#ff7800">static</span> <span style="color:#ff7800">void</span> <span style="color:#3b5bb5">main</span>(<span style="color:#ff7800">String</span>[] args) {

   <span style="color:#8c868f">// Create a Vert.x instance</span>
   <span style="color:#ff7800">Vertx</span> vertx <span style="color:#ff7800">=</span> <span style="color:#ff7800">Vertx</span><span style="color:#ff7800">.</span>vertx();

   <span style="color:#8c868f">// Generate the certificate for https</span>
   <span style="color:#ff7800">SelfSignedCertificate</span> cert <span style="color:#ff7800">=</span> <span style="color:#ff7800">SelfSignedCertificate</span><span style="color:#ff7800">.</span>create();

   <span style="color:#8c868f">// Start an HTTP/2 server that prints hello world!</span>
   vertx<span style="color:#ff7800">.</span>createHttpServer(<span style="color:#ff7800">new</span> <span style="color:#ff7800">HttpServerOptions</span>()
       .setSsl(<span style="color:#3b5bb5">true</span>)
       .setUseAlpn(<span style="color:#3b5bb5">true</span>)
       .setKeyCertOptions(cert<span style="color:#ff7800">.</span>keyCertOptions())
       .setPort(<span style="color:#3b5bb5">8443</span>))
       .requestHandler(request <span style="color:#ff7800">-</span><span style="color:#ff7800">></span> {
         request<span style="color:#ff7800">.</span>response()<span style="color:#ff7800">.</span>end(<span style="color:#409b1c">"Hello World"</span>);
       })<span style="color:#ff7800">.</span>listen();
  }
}
</pre>

<p>We chose to use the h2 protocol for our server and we need to configure the TLS layer accordingly:</p>

    <ul>
        <li>The SSL is set along with the usage of ALPN - ALPN is a TLS extension used by the client and the server to negotiate the usage of h2</li>
        <li>A self-signed certificate is generated on the fly with the <code>SelfSignedCertificate</code> class and set on options for the server</li>
    </ul>

<h2>Contract First APIs Development</h2>

<p>When you need to develop a Web API and want high productivity you might want to consider OpenAPI.</p>

<p>OpenAPI is an open standard that empowers developers to formally document web APIs.  It provides a document and schema-centric workflow for creating Web APIs. You can adopt the <i>Contract-First approach</i>, enabling teams to deeply connect design and implementation phases of your Web APIs development.</p>

<p>You can document every aspect of your endpoints: the request parameters, the request body, the different response bodies, the security requirements and so on.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/contractfirstapproach.png"></p>

<p>With this approach, you create an OpenAPI contract during the design phase and you reuse it during the implementation phase using specific OpenAPI tooling. You can even go beyond that and manage every part of the lifecycle of your product.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/april/images/productlife.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/productlife_sm.png"></a></p>

<p>For more info give a look at <a target="_blank" href="https://swagger.io/blog/benefits-of-openapi-api-development/">OpenAPI API Development</a>.</p>

<h2>Vert.x meets OpenAPI</h2>

<p>In this article, we will implement an API for managing users based on a simple OpenAPI contract. The document is in YAML format and is very human readable; you can create such documents easily with a tool, for this example we used <a target="_blank" href="https://www.apicur.io">Apicur.io Studio</a>.</p>

<pre style="background:#fff;color:#000"><span style="color:#ff7800">--</span><span style="color:#ff7800">-</span>
openapi<span style="color:#ff7800">:</span> <span style="color:#3b5bb5">3.0</span><span style="color:#3b5bb5">.0</span>
info<span style="color:#ff7800">:</span>
 title<span style="color:#ff7800">:</span> <span style="color:#ff7800">Users</span> <span style="color:#3b5bb5">API</span>
 description<span style="color:#ff7800">:</span> <span style="color:#ff7800">An</span> <span style="color:#3b5bb5">API</span> <span style="color:#ff7800">for</span> managing users
 version<span style="color:#ff7800">:</span> <span style="color:#3b5bb5">1.0</span><span style="color:#3b5bb5">.0</span>
paths<span style="color:#ff7800">:</span>
 <span style="color:#ff7800">/</span>users<span style="color:#ff7800">:</span>
   get<span style="color:#ff7800">:</span>
     summary<span style="color:#ff7800">:</span> <span style="color:#ff7800">All</span> users
     operationId<span style="color:#ff7800">:</span> listUsers
     description<span style="color:#ff7800">:</span> <span style="color:#ff7800">List</span> all users
     responses<span style="color:#ff7800">:</span>
       <span style="color:#3b5bb5">200</span><span style="color:#ff7800">:</span>
         description<span style="color:#ff7800">:</span> <span style="color:#ff7800">Returns</span> the list of all users
         content<span style="color:#ff7800">:</span>
           application<span style="color:#ff7800">/</span>json<span style="color:#ff7800">:</span>
             schema<span style="color:#ff7800">:</span>
               type<span style="color:#ff7800">:</span> array
               items<span style="color:#ff7800">:</span>
                 $ref<span style="color:#ff7800">:</span> <span style="color:#409b1c">'#/components/schemas/User'</span>
   post<span style="color:#ff7800">:</span>
     summary<span style="color:#ff7800">:</span> <span style="color:#ff7800">Creates</span> a user
     operationId<span style="color:#ff7800">:</span> addUser
     requestBody<span style="color:#ff7800">:</span>
       content<span style="color:#ff7800">:</span>
         application<span style="color:#ff7800">/</span>json<span style="color:#ff7800">:</span>
           schema<span style="color:#ff7800">:</span>
             $ref<span style="color:#ff7800">:</span> <span style="color:#409b1c">'#/components/schemas/User'</span>
       required<span style="color:#ff7800">:</span> <span style="color:#3b5bb5">true</span>
     responses<span style="color:#ff7800">:</span>
       <span style="color:#3b5bb5">200</span><span style="color:#ff7800">:</span>
         description<span style="color:#ff7800">:</span> <span style="color:#ff7800">A</span> user was created successfully
         content<span style="color:#ff7800">:</span>
           application<span style="color:#ff7800">/</span>json<span style="color:#ff7800">:</span>
             schema<span style="color:#ff7800">:</span>
               type<span style="color:#ff7800">:</span> string
components<span style="color:#ff7800">:</span>
 schemas<span style="color:#ff7800">:</span>
   <span style="color:#ff7800">User</span><span style="color:#ff7800">:</span>
     required<span style="color:#ff7800">:</span>
     <span style="color:#ff7800">-</span> firstName
     properties<span style="color:#ff7800">:</span>
       firstName<span style="color:#ff7800">:</span>
         type<span style="color:#ff7800">:</span> string
       lastName<span style="color:#ff7800">:</span>
         type<span style="color:#ff7800">:</span> string
</pre>


<p>Now let’s see how we can implement a web service for this API with Vert.x.</p>

<p>Since version 3.5, Vert.x provides the <a target="_blank" href="https://vertx.io/docs/#web">vertx-web-api-contract</a> component that creates a <a target="_blank" href="https://vertx.io/docs/vertx-web/java/#_basic_vert_x_web_concepts">Vert.x Web Router</a>. The main entry point is the router factory that performs two important things:</p>

<p>First, the router factory ensures that every request to the API conforms to the OpenAPI contract such as parsing, validating the HTTP request body, path, and query. This part is transparent and happens before the API is called.</p>

<p>Then the router factory maps the OpenAPI endpoint definitions to the service implementation we create: Vert.x Web API Contracts allow us to mount operations with handlers, which are simple Java lambdas that will react to an event you can handle in your code.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/openapi_vertx.png"></p>

<p>This factory also helps you to mount the right auth handlers, to mount the right failure handlers for validation and so on.</p>

<p>The contract defines two operations for an API exposed on the /users URL</p>
	<ul>
		<li>GET : list all users in JSON format - the <code>listUsers</code> operation</li>
		<li>POST : inserts a new user in JSON format and returns the user identifier - the addUser operation</li>
	</ul>
<p>In Vert.x, you map each operation to a handler that processes the request and produces a response.</p>

<h2>The listUsers operation</h2>
<p>For the sake of simplicity, we implemented the users as a single in-memory list of users, obviously a real application would instead use a persistent storage.</p>

<pre style="background:#fff;color:#000"><span style="color:#ff7800">void</span> listUsers(<span style="color:#ff7800">RoutingContext</span> routingContext) {
  <span style="color:#8c868f">// Returns the list of users encoded to a JSON array</span>
  routingContext
     .response()
     .setStatusCode(<span style="color:#3b5bb5">200</span>)
     .end(users<span style="color:#ff7800">.</span>encode());
}
</pre>

<p>The implementation uses the <code>RoutingContext</code> which is provided by Vert.x Web, among other things this context provides the current HTTP response:</p>

<p>Upon a successful GET request, <code>listUsers</code> returns a JSON encoded array of a schema named User; representing all of the users in the system.</p> 

<p><a target="_blank" href="https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.0.md#schemaObject">Schemas</a> are effectively data types, and allow us to describe the format of inputs and outputs in our APIs. In this instance, a reference ($ref) is used, allowing us to reuse our Schema many times without repetition.</p>

<p>If you know JSON Schema, then many of the properties will be familiar, allowing for the definition of types, formats, lengths, patterns, and myriad other elements.</p>

<h2>The addUser operation</h2>

<p>For the addUser implementation, we need to use the parameters parsed by the API that gives us the JSON object representing the user.</p>

<pre style="background:#fff;color:#000"><span style="color:#ff7800">void</span> addUser(<span style="color:#ff7800">RoutingContext</span> routingContext) {

  <span style="color:#8c868f">// Get the parsed parameters</span>
  <span style="color:#ff7800">RequestParameters</span> params <span style="color:#ff7800">=</span> routingContext<span style="color:#ff7800">.</span>get(<span style="color:#409b1c">"parsedParameters"</span>);

  <span style="color:#8c868f">// We get an user JSON object validated by Vert.x Open API validator</span>
  <span style="color:#ff7800">JsonObject</span> user <span style="color:#ff7800">=</span> params<span style="color:#ff7800">.</span>body()<span style="color:#ff7800">.</span>getJsonObject();

  <span style="color:#8c868f">// Generate a user id</span>
  <span style="color:#ff7800">String</span> userId <span style="color:#ff7800">=</span> <span style="color:#409b1c">""</span> <span style="color:#ff7800">+</span> users<span style="color:#ff7800">.</span>size();

  <span style="color:#8c868f">// Add the user to the users list</span>
  users<span style="color:#ff7800">.</span>add(user);

  <span style="color:#8c868f">// Send the user id as JSON response</span>
  routingContext
     .response()
     .setStatusCode(<span style="color:#3b5bb5">200</span>)
     .end(userId);
}
</pre>

<p>The <code>addUser</code> operation accepts a JSON encoded user, as defined in our <code>User</code> schema, returning a string containing the newly created user’s unique ID upon success.</p>

<p>We retrieve all of the relevant data from the RoutingContext, including the body, which is conveniently available as a <code>JsonObject</code>, and store it our users list.</p>

<p>Handily, much of the input validation can be performed on our behalf by Vert.x, which ensures the incoming object conforms to our OpenAPI definition. This means that, for example, all required fields will be present and of the correct type when our handler is executed. The validator will validate request body but also take care of deserializing and validating the query, path, cookie and header parameters, and Vert.x Web Open API does the heaving lifting for you, providing you the guarantee that the request you receive conforms to the defined contract.</p>

<h2>Final steps</h2>

<p>Now we have all the parts of our small application, we can wire them together.</p>


<pre style="background:#fff;color:#000"><span style="color:#8c868f">// Start creating the router factory from openapi.yaml</span>
<span style="color:#ff7800">OpenAPI3RouterFactory</span><span style="color:#ff7800">.</span>create(vertx, <span style="color:#409b1c">"openapi.yaml"</span>, ar <span style="color:#ff7800">-</span><span style="color:#ff7800">></span> {

  <span style="color:#8c868f">// The router factory instantiation could fail</span>
  <span style="color:#ff7800">if</span> (ar<span style="color:#ff7800">.</span>succeeded()) {
    <span style="color:#ff7800">OpenAPI3RouterFactory</span> factory <span style="color:#ff7800">=</span> ar<span style="color:#ff7800">.</span>result();

    <span style="color:#8c868f">// Now you can use the factory to mount map endpoints to Vert.x handlers</span>
    factory<span style="color:#ff7800">.</span>addHandlerByOperationId(<span style="color:#409b1c">"listUsers"</span>, this<span style="color:#ff7800">:</span><span style="color:#ff7800">:</span>listUsers);
    factory<span style="color:#ff7800">.</span>addHandlerByOperationId(<span style="color:#409b1c">"addUser"</span>, this<span style="color:#ff7800">:</span><span style="color:#ff7800">:</span>addUser);

    <span style="color:#8c868f">// Build the router with factory.getRouter()</span>
    <span style="color:#ff7800">Router</span> router <span style="color:#ff7800">=</span> factory<span style="color:#ff7800">.</span>getRouter();

    <span style="color:#8c868f">// Generate the certificate for https</span>
    <span style="color:#ff7800">SelfSignedCertificate</span> cert <span style="color:#ff7800">=</span> <span style="color:#ff7800">SelfSignedCertificate</span><span style="color:#ff7800">.</span>create();

    <span style="color:#8c868f">// Start the HTTP/2 server with the OpenAPI router</span>
    vertx<span style="color:#ff7800">.</span>createHttpServer(<span style="color:#ff7800">new</span> <span style="color:#ff7800">HttpServerOptions</span>()
       .setSsl(<span style="color:#3b5bb5">true</span>)
       .setUseAlpn(<span style="color:#3b5bb5">true</span>)
       .setKeyCertOptions(cert<span style="color:#ff7800">.</span>keyCertOptions())
       .setPort(<span style="color:#3b5bb5">8443</span>))
       .requestHandler(router<span style="color:#ff7800">:</span><span style="color:#ff7800">:</span>accept)
       .listen();
  }
});
</pre>

<p>This code is executed during the start of the application in several steps:</p>

    <ul>
        <li>The first step is the creation of the Vert.x Web router from the openapi.yaml file</li>
        <li>After the router is created, we map the <code>listUser</code> and <code>addUser</code> methods to the corresponding API operations</li>
        <li>Finally the HTTP/2 server is started</li>
    </ul>

<h2>Conclusion</h2>

<p>In this article, we have seen how OpenAPI’s Contract First approach can be effectively implemented with Vert.x and deliver a highly scalable implementation. If you are interested in HTTP/2 performance you can watch this <a target="_blank" href="https://www.youtube.com/watch?v=2pkjavkBsjE">presentation</a> that explains more in-depth the technology. The entire source code for this article is shared on <a target="_blank" href="https://github.com/vietj/scalable-open-api-with-vertx">GitHub</a>.</p>

<p>For further details about Vert.x and how you can build your next application with it, check <a target="_blank" href="http://vertx.io">http://vertx.io</a></p>

<p>See you soon on our <a target="_blank" href="https://gitter.im/eclipse-vertx/vertx-users">Gitter channel</a>!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/april/images/julien.jpeg"
        alt="Julien Viet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Julien Viet<br />
            <a target="_blank" href="https://www.redhat.com/">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/julienviet">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/vietj">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
     <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/april/images/francesco.jpg"
        alt="Francesco Guardiani" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           	Francesco Guardiani<br />
            <a target="_blank" href="https://www.credimi.com/">Credimi S.p.a</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/SlinkyGuardiani">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/slinkydeveloper">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>