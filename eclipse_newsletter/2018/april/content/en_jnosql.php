<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/eclipse_jnosql.png"></p>
   
<p>Eclipse JNoSQL is a Java framework that streamlines the integration of Java applications with NoSQL databases. It defines a set of APIs and provides a standard implementation for most NoSQL databases. This clearly helps to achieve very low coupling with the underlying NoSQL technologies used in applications.
The project has two layers:</p>

<p>The project has two layers:</p>
<ol>
<li><b>Communication Layer:</b> A set of APIs that defines communication with NoSQL databases. Compared with traditional the RDBMS world, they are like the JDBC API. It contains four modules, one for each NoSQL database type: Key-Value, Column Family, Document, and Graph.</li> 
<li><b>Mapping Layer:</b> These APIs help developers to integrate their Java application with the NoSQL database. This layer is annotation-driven and uses technologies like CDI and Bean Validation, making it simple for developers to use. In the traditional RDBMS world, this layer can be compared to the Java Persistence API or object-relational mapping frameworks such as Hibernate.</li> 
</ol>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/jnosql_map.png"></p>

<h3>One Mapping API, multiples databases</h3>
 
<p>Eclipse NoSQL has one API for each NoSQL database type. However, it uses the same annotations to map Java objects. Therefore, with just these annotations that look like JPA, there is support for more than twenty NoSQL databases.</p>

<pre>
@Entity
public class God {
 
    @Id
    private String id;
    @Column
    private String name;
    @Column
    private String power;
 //... 
}
</pre>

<p>Another example can be found in <a target="_blank" href="https://dzone.com/articles/eclipse-jnosql-a-quick-overview-with-redis-cassand">an article</a> that demonstrates the same annotated entity used across different NoSQL databases: Redis, Cassandra, Couchbase, and Neo4J. The approach is "stick to the API": the developer can replace Redis with Hazelcast, as both implement the Key-Value API, thus avoiding vendor lock-in with one of these databases.</p>

<p>Vendor lock-in is one of the things any Java project needs to consider when choosing NoSQL databases. If there's a need for a switch, other considerations include: time spent on the change, the learning curve of a new API to use with this database, the code that will be lost, the persistence layer that needs to be replaced, etc. Eclipse JNoSQL avoids most of these issues through the Communication APIs. It also has template classes that apply the design pattern 'template method’ to databases operations. And the Repository interface allows Java developers to create and extend interfaces, with implementation automatically provided by Eclipse JNoSQL: support method queries built by developers will automatically be implemented for them.</p>

<pre>
public interface GodRepository extends Repository&lt;God, String&gt; {
 
    Optional&lt;God&gt; findByName(String name);
 
}
 
 
GodRepository repository = ...;
God diana = God.builder().withId("diana").withName("Diana").withPower("hunt").builder();
repository.save(diana);
Optional<God> idResult = repository.findById("diana");
Optional<God> nameResult = repository.findByName("Diana");
</pre>

<h3>Beyond JPA</h3>

<p>JPA is a good API for object-relationship mapping and it's already a standard in the Java world defined in JSRs. It would be great to use the same API for both SQL and NoSQL, but there are behaviors in NoSQL that SQL does not cover, such as time to live and asynchronous operations. JPA was simply not made to handle those features.</p>

<pre>
ColumnTemplateAsync templateAsync = …;
ColumnTemplate template = …;
God diana = God.builder().withId("diana").withName("Diana").withPower("hunt").builder();
Consumer&lt;God&gt; callback = g -> System.out.println("Insert completed to: " + g);
templateAsync.insert(diana, callback);
Duration ttl = Duration.ofSeconds(1);
template.insert(diana, Duration.ofSeconds(1));
</pre>

<h3>A Fluent API</h3>

<p>Eclipse JNoSQL is a fluent API that makes it easier for Java developers create queries that either retrieve or delete information in a Document type, for example.</p> 


<pre>
DocumentTemplate template = //;//a template to document nosql operations
God diana = God.builder().withId("diana").withName("Diana").withPower("hunt").builder();
template.insert(diana);//insert an entity
DocumentQuery query = select().from(God.class).where("name").eq("Diana").build();//select god where name equals “Diana”
List&lt;God&gt; gods = template.select(query);//execute query
DocumentDeleteQuery delete = delete().from("god").where("name").eq("Diana").build();//delete query
template.delete(delete);
</pre>

<h3>Let's not reinvent the wheel: Graph</h3>
<p>The Communication Layer defines three new APIs: Key-Value, Document and Column Family. It does not have new Graph API, because a very good one already exists. <a target="_blank" href="http://tinkerpop.apache.org/">Apache TinkerPop</a> is a graph computing framework for both graph databases (OLTP) and graph analytic systems (OLAP). Using <a target="_blank" href="http://tinkerpop.apache.org/">Apache TinkerPop</a> as Communication API for Graph databases, the <a target="_blank" href="https://dzone.com/articles/have-a-fun-moment-with-graph-and-java">Mapping API has a tight integration</a> with it.</p>

<h3>Particular behavior matters in NoSQL database</h3>

<p>Particular behavior matters. Even within the same type, each NoSQL database has a unique feature that is a considerable factor when choosing a database over another. This ‘’feature’’ might make it easier to develop, make it more scaleable or consistent from a configuration standpoint, have the desired consistency level or search engine, etc. Some examples are  Cassandra and its Cassandra Query Language and consistency level, OrientDB with live queries, ArangoDB and its Arango Query Language, Couchbase with N1QL - the list goes on. Each NoSQL has a specific behavior and this behavior matters, so JNoSQL is extensible enough to capture this substantiality different feature elements.</p>

<pre>
public interface PersonRepository extends CouchbaseRepository<Person, String> {

        @N1QL("select * from Person")
        List&lt;Person&gt; findAll();

        @N1QL("select * from Person where name = $name")
        List&lt;Person&gt; findByName(@Param("name") String name);
}

Person person = ...
CassandraTemplate template = ...
ConsistencyLevel level = ConsistencyLevel.THREE;
template.save(person, level);
</pre>


<h2>A Standard, Easy-to-Use Extension API</h2>

<p>The Eclipse JNoSQL API has a simple, user-friendly interface that makes it <a target="_blank" href="https://frostillic.us/blog/posts/CB3FED16EF19D48B85258206005915D7">easy to implement to a new database</a>. As mentioned in the previous section, NoSQL database behavior matters — that why the JNoSQL API is so extensible.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/april/images/tweet.png"></p>

<p>Having a simple and extensible interface is important, but having an easy implementation process is just as important. That’s why in JNoSQL we are building a Technology Compatibility Kit, called the TCK. TCK is a suite of tests that nominally checks a particular (alleged) implementation of a NoSQL type API, provided by Eclipse JNoSQL. NoSQL vendors don't need to create two drivers, because the communication layer works very well with an adapter to the mapping layer, including its specific behavior.</p>


<h2>Conclusion</h2>

<p>Eclipse JNoSQL is an excellent tool to use when Java EE developers want to integrate with NoSQL databases. This is especially true because it already supports more than twenty NoSQL databases, such as Cassandra, Redis, Neo4J, Couchbase, ArangoDB, Riak, and MongoDB.  The unique and fluent API to support this persistence technology, with both synchronous and asynchronous calls through either a template class or just an interface, managed by CDI, plus extensions that make particular features available by the NoSQL provider: this is a formula that makes Eclipse JNoSQL a tool for polyglot persistence, big data on the NoSQL world.</p>

<p>Find out more information and get involved!</p>
    <ul> 
        <li>Website: <a target="_blank" href="http://www.jnosql.org/">http://www.jnosql.org/</a></li>
        <li>Twitter: <a target="_blank" href="https://twitter.com/jnosql">https://twitter.com/jnosql</a></li>
        <li>GitHub Repo: <a target="_blank" href="https://github.com/eclipse?q=Jnosql">https://github.com/eclipse?q=Jnosql</a></li>
        <li>Mailing List: <a target="_blank" href="https://accounts.eclipse.org/mailing-list/jnosql-dev">https://accounts.eclipse.org/mailing-list/jnosql-dev</a></li>
    </ul>
    
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/april/images/otavio.jpg"
        alt="Otavio Gonçalves de Santana" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            	Ot&aacute;vio Gonçalves de Santana<br />
            <a target="_blank" href="http://www.tomitribe.com/">Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/otaviojava">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/mynetwork/invite-sent/otaviojava/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>