<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App ();
require_once ('_variables.php');

// Begin: page-specific settings. Change these.
$pageTitle = "What can Reactive Streams offer EE4J?";
$pageKeywords = "reactive streams, ee4j, enterprise java, java ee, jakarta ee";
$pageAuthor = "Christopher Guindon";
$pageDescription = "Discover the specific parts of EE4J where Reactive Streams would be useful.";

// Uncomment and set $original_url if you know the original url of this article.
    $original_url = "https://developer.lightbend.com/blog/2018-02-06-reactive-streams-ee4j/index.html";
    $og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

// Place your html content in a file called content/en_article1.php
$script_name = $App->getScriptName();

require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");