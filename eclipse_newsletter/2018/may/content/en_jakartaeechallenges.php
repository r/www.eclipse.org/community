<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>Do you know that many famous people were born at the end of April? I’ll give you some samples. April 21 is the birthday of Queen Elizabeth II; Vladimir Lenin was born on April 22,  William Shakespeare on April 21, and Adolf Hitler on April 20. Maybe I shouldn’t include Hitler here, but he had a huge influence on 20th century history. Why do I mention this? Because on April 24, 2018, Jakarta EE was born. Using mathematical induction, we can predict that this newborn child will have a great influence on the Java world.</p>

<p>What’s the progress so far? There will be 39 projects under the umbrella <a target="_blank" href="https://projects.eclipse.org/projects/ee4j">EE4J project</a>. 26 of them are already created. Others are in different states, but all them have been submitted for creation review. I made a table with a list of all of the projects, but it is quite big, so I included it in <a target="_blank" href="https://dmitrykornilov.net/2018/05/09/jakarta-ee-projects-summary/">another article</a>.</p>

<p>There are approximately 110 repositories to transfer, and about half of them have already been transferred.</p>

<p>Transferring projects may sound like an easy and straightforward process, but it’s not. It requires a lot of effort to transfer a project accurately. To help you understand, I’ll describe the steps to do it, and the challenges faced during this process. This should answer your “Why is it taking so long” questions that I often see in Twitter and on mailing lists.</p>

<p><b>1. Internal license check</b></p>

<p>We check all source files license headers to make sure they are correct. If not, we fix them. For old files it can be difficult to find an original file in a third party project.</p>

<p><b>2. Internal third party analysis</b></p>

<p>We run a third-party dependency analysis to identify which the dependencies should be replaced with their latest versions to fix major bugs and security issues. If these newer versions are not backward compatible with the older versions, then this requires some adoption work.</p>

<p><b>3. Proposals creation</b></p>

<p>In parallel with the steps above, we create project proposals in the Eclipse Foundation. The first challenge that we faced here is project granularity. We cannot (and don’t want) to create a project for every single repository. The Project Management Committee (PMC) decided to separate API and implementation projects and combine all satellite subprojects in the main project.</p> 
<p>Another challenge was how to name newly created projects. Names must not violate any required legal requirements and trademarks. It took quite a while to agree to use the “Eclipse Project for XXX” pattern for API projects (which typically used a name based on the Java trademark) and the “Eclipse XXX” pattern for implementation projects.</p>
<p>The third challenge is to find contributors and leads. No one wants to contribute to old projects such as Enterprise Management.</p>

<p><b>4. Initial Contribution</b></p>

<p>When the proposal is created, approved, and has passed all required reviews, the project is created by the Eclipse Foundation. This allows us to start making an initial contribution and work with the Eclipse IP team to approve it. We submit one CQ (Contribution Questionnaire) for the initial contribution and one CQ for each project’s third party dependency. The Eclipse IP team often needs some clarification and may request changes. But if the license and third party work in steps 1 and 2 was done correctly, this step usually runs smoothly.</p>

<p><b>5. Issues Transfer</b></p>

<p>We created a special script to transfer issues. Our goal was to preserve issue numbers while doing the transfer to simplify the future work of updating web sites and issue links. The script took 8 hours just to transfer all of the Jersey issues.</p>

<p><b>6. Build Environment</b></p>

<p>We have to prepare a new build environment on Eclipse Foundation infrastructure. To help you understand the amount of work, the Compatibility Technology Kit (CTS) runs on a master machine with 50 slaves. CTS also requires a mail server and a JWSDP server, which are configured in a separate machine. To build GlassFish, we use 50 servers, and each server has 2 to 4 executors. So there are up to 200 executors in total. This configuration supports processing of up to 4 simultaneous pull requests. This is fine for normal development but not enough to process the intensive changes that usually happen at release time.</p>
<p>For each EE4J project, we need to create Jenkins jobs to build the project, run unit tests, run TCK tests, and make nightly builds and Maven central releases. The Eclipse build infrastructure is different than what we use in Oracle, so all these jobs cannot be simply copied, they have to be reworked.</p>

<p><b>7. Old projects shut down</b></p>

<p>Java EE projects which have been transferred to the Eclipse Foundation have to be properly shut down. This requires updating project GitHub repositories and web sites with a message that this project has been transferred to the Eclipse Foundation. The message includes links to the new repository, issues tracker and project page. We are maintaining active Java EE communication channels, but we may shut them down in the future.</p>
<p>There were dozens of smaller challenges I didn’t put in the list. It was more difficult at the beginning. With experience, we know what to do and it goes faster. But we still have many challenges. One of them is transferring the CTS suite. It has never been open sourced, it’s full of third party dependencies, it’s in an SVN repository, and it’s huge. Maybe I should write another article about it. Another challenge is updating CI/CD jobs to use the new cloud-based build system recently introduced by the Eclipse Foundation.</p>

<p>I’d like to use this opportunity to ask the community to be more active and to contribute. Jakarta EE is a part of the open source foundation, and it belongs to the community. Your contributions are needed!</p>

<ul>
<li>Subscribe to Jakarta EE community mailing list: <a href="https://accounts.eclipse.org/mailing-list/jakarta.ee-community">jakarta.ee-community@eclipse.org</a></li>
<li>Contribute to EE4J projects: <a target="_blank" href="https://github.com/eclipse-ee4j">https://github.com/eclipse-ee4j</a></li>
<li>Jakarta EE on Twitter: <a target="_blank" href="https://twitter.com/jakartaee">@JakartaEE</a></li>
<li>Jakarta EE web site: <a target="_blank" href="https://jakarta.ee">jakarta.ee</a></li>
</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/dmitry.jpg"
        alt="Dmitry Kornilov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Dmitry Kornilov<br />
            <a target="_blank" href="https://www.oracle.com/">Oracle</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/m0mus">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://dmitrykornilov.net/author/dmitrykornilov/">Blog</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>