<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>First of all, this is an opinion piece from someone that has been in the IT field for almost 30 years and in many capacities, as a developer, services consultant, technical seller and technical marketer. I have seen the IT and software market evolve since before there was open source software and Java. And for many of us that have been around this long, no matter what new technologies pop up, it seems that these are always attempting to solve the same problems (encapsulation, reusability, availability, distributed systems, data management, etc.) we have been trying to tackle since the creation of the first computer using a new approach or technique, e.g. hierarchical, relational, noSQL.</p>

<p>I remember when I attended my first Java workshop (run by Sun Microsystems) back in the 90’s. Besides the catchy marketing slogan "Write Once, Run Everywhere", as a developer, I recall leaving with a sense of awe that with this new language, I no longer needed to worry about allocating and deallocating memory and portability was guaranteed across hardware providers. Just these two capabilities were going to save me tons of development time! Then came Java Enterprise Edition (JPE→J2EE→Java EE) that provided a set of APIs that delivered enterprise-grade capabilities that many organizations found extremely useful to develop production applications that, to this day, still run many businesses across the globe. Java continues to be one of the <a target="_blank" href="https://www.tiobe.com/tiobe-index/">top languages</a> in use for development nowadays. However, we now live in different times, where cloud, containers, microservices, miniservices, API management, serverless, reactive systems have become a necessity to be able to compete and succeed in this Digital Economy that requires hyper-agility in the development, delivery, and maintenance of applications. There exists an abundance of runtimes and frameworks that are adept to microservices and cloud. For example, Node.js has become pretty <a target="_blank" href="http://redmonk.com/sogrady/2018/03/07/language-rankings-1-18/">popular</a> in microservices development and Java EE is no longer the only JVM-based framework, Spring and Eclipse Vert.x are two other ones that come to mind. The days of using a single programming language to implement applications are long gone. In fact, in a recent Red Hat customers <a target="_blank" href="https://developers.redhat.com/blog/2017/12/05/state-microservices-survey-2017-eight-trends-need-know/">survey</a>, 87% of the respondents indicated that they were using or considering multiple technologies for developing microservices. Likewise in a 2018 Eclipse Foundation Jakarta EE Developers <a target="_blank" href="https://jakarta.ee/news/2018/04/24/jakarta-ee-community-survey/">survey</a>, 68% of the respondents indicated that more than 60% of their applications used multiple languages within their implementation.</p>

<p>As valuable and productive as Java EE has been and still is to organizations and developers worldwide, it is a fact that Java EE, as a standard, has fallen behind in advances related to cloud, containers, and microservices. It is because of this that the community decided to "take the bull by the horns" in 2016 and created <a target="_blank" href="http://microprofile.io">MicroProfile</a>, a community-driven open source specification - that now resides with the Eclipse Foundation - focused on optimizing enterprise Java for microservices. Many naysayers have been proclaiming “Java EE dead” for a few of years now, and although there’s some truth to this, there are some major changes coming down for Java EE in the form of a recently announced Eclipse project called <a target="_blank" href="https://jakarta.ee">Jakarta EE</a>.</p>

<p>Jakarta EE, which serves as the new home of cloud-native Java, has been seeded with Java EE from Oracle, and plans to first ensure the integrity of the transfer by releasing a Java EE 8 compliant Glassfish 5.1 server by Q3CY2018 and a Jakarta EE 8 compliant Glassfish 5.1 by Q4CY2018 using a new certification process. Other 2018 deliverables are Java EE 8 specifications, RIs, TCKs contributed, processes for existing and new specs, compatibility process, etc. Presently, Jakarta EE sub-projects are being organized under the Eclipse Foundation and as a next step, Jakarta EE will start its quick evolution into cloud, containers, microservices, serverless, and reactive technologies. Responding to the 2018, Eclipse Foundation Jakarta EE Developers <a target="_blank" href="https://jakarta.ee/news/2018/04/24/jakarta-ee-community-survey/">survey</a>, Jakarta EE plans to:</p>
	<ul>
        <li>Be supported by a vibrant developer community</li>
        <li>Enhance support for microservices architecture</li>
        <li>Move to Cloud Native Java</li>
        <li>Innovate Faster: Become more nimble</li>
        <li>Provide production quality reference implementations</li>
	</ul>
	
Also, Jakarta EE will modernize its ecosystem with:
    <ul>
        <li>New open specification process to replace JCP</li>
        <li>New governance structure</li>
        <li>New, more open means to contribute</li>
    </ul>
    
<p>One key element that will accelerate the evolution of Jakarta EE is its close alignment to Eclipse MicroProfile. Eclipse MicroProfile 1.3 already includes enterprise specifications for Configuration, Fault Tolerance, Metrics, JWT propagation, Open API, Open Tracing, Health Check and Rest Client. Because of the high synergy between MicroProfile and Jakarta EE, the latter could get a quick jumpstart by adopting these MicroProfile specifications. There have been talks within both communities about increasing the alignment of these two open source projects. It is too early to say how these two will end up but some possible scenarios are:</p>

	<ul>
        	<li>Eclipse MicroProfile is moved under the EE4J umbrella initiative and governed by the <a target="_blank" href="https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php">Jakarta EE Working Group</a></li>
        	<li>Eclipse MicroProfile is moved under the <a target="_blank" href="https://projects.eclipse.org/projects/ee4j/charter">EE4J</a> umbrella initiative and continues with its own governance processes</li>
        	<li>Things stay as they are now, both under the Eclipse Foundation as separate projects and each with their own governance processes</li>
	</ul>

<p>In any case, Eclipse MicroProfile could continue to serve as a fast-paced incubation project where new ideas can be quickly put in the hands of developers to be experimented with and improved. As these MicroProfile APIs are used in the market and solidified per community and users’ feedback, they become good candidates to be adopted by Jakarta EE. It is because of this that I think that in two years time (or maybe even earlier), Jakarta EE will contain full-fledged specifications targeted to microservices architectures, containers, cloud, API management, serverless, reactive systems, and service meshes.</p>

<p>But Jakarta EE supporting cloud-native Java is not the only goal. There are thousands of organizations around the globe that trust Java EE for their productions workloads. In a recent Red Hat customers <a target="_blank" href="https://developers.redhat.com/blog/2017/12/05/state-microservices-survey-2017-eight-trends-need-know/">survey</a>, the top three reasons why Red Hat Middleware customers were using or considering to use Java EE for microservices were:</p>
	<ol>
        	<li>Java EE is a standard</li>
        	<li>No need to re-train workforce</li>
        	<li>We trust Java EE to run production because it’s well established and enterprise-grade</li>
	</ol>
	
<p>Moreover, in a 2018 Eclipse Foundation Jakarta EE Developers <a target="_blank" href="https://jakarta.ee/news/2018/04/24/jakarta-ee-community-survey/">survey</a>, respondents indicated that the most important reasons that their organizations selected Java EE were:</p>
	<ol>
       	<li>Stability</li>
       	<li>Specifications</li>
       	<li>Availability of developers</li>
       	<li>Multiple vendors with compatible implementations</li>
	</ol>
	
<p>So, there’s a clear continued thirst in the market for a community-driven open source specification that gives organizations the freedom of choice when it comes to choosing an implementation and allows them to leverage their developers’ expertise or easily find that skill in the job market.</p>

<p>Furthermore, there are many organizations that do not need to implement microservices. Not everyone needs to be an Uber or Netflix. On the contrary, most companies are not like Uber and Netflix and they should not be aiming to be like them. In many cases, Java EE workloads will continue to run production workloads for years to come. And some firms, because of the nature of their business, cannot do "live testing" in production, e.g. canary releases, blue-green deployments, A/B testing; it’s ok if your movie fails to play or your cab ride doesn’t show up, but there are no do-overs for a heart being delivered late to a transplant patient, or an airplane navigation system bug. However, there are clear benefits of adopting agile methodologies/frameworks for development, like containers, cloud, CI/CD, DevOps, etc. because all these support becoming digital. In fact, according to a 2016 Bain &amp; Company and Red Hat Digital Transformation <a target="_blank" href="http://www.bain.com/publications/articles/for-traditional-enterprises-path-to-digital-role-of-containers.aspx">Survey</a>, digitally mature companies are 8 times more likely to gain market share.</p>

<p>So as Jakarta EE evolves enterprise Java, it must also preserve capabilities that organizations have learned to trust through their years of using Java EE. What will this look like under Jakarta EE? Here are a few considerations that the community are currently discussing:</p>
	<ul>
        <li>The existing full profile could be marked as “stable” or “proposed optional” so that the community may focus on new functionality that’s more adept to cloud, containers, microservices, internet/web-scale, highly distributed.</li>
        <li>Get rid of the concept of profiles and go with a composability of APIs model, i.e. an application assembler approach (similar to <a target="_blank" href="http://wildfly-swarm.io">WildFly Swarm</a>, recently renamed <a target="_blank" href="https://thorntail.io">Thorntail</a>) that would create a running application that has only the Jakarta APIs it needs to run and nothing else</li>
        <li>The need to have a minimum core profile within Jakarta EE that other profiles can build upon</li>
        <li>How many profiles to define? Possibly: core (Servlet or CDI or both), web, microservices, full, and custom</li>
        <li>Have a single legacy full profile (for backward compatibility), plus a new full profile that would include cloud-native enterprise Java specifications (legacy-free profile), plus possibly a handful of other sub-profiles</li>
        <li>Integration or inclusion of service meshes</li>
        <li>A combination of some of the above</li>
	</ul>
	
<p>What is clear is that Jakarta EE will need to preserve key existing capabilities from Java EE for a few years to give existing Java EE customers a clear path to evolve into Jakarta EE as it matures. Likewise, organizations currently invested in Java EE will be able to slowly take advantage of new cloud-native capabilities of Jakarta EE while still using key features of Java EE. They should also have ample time to migrate from Java EE capabilities marked as “proposed optional” to new Jakarta EE features.</p>

<p>When it comes to microservices, Spring Boot has become quite popular. Spring Boot and Spring are based on Java and are a competing technology to Jakarta EE. Spring Boot adopted the "fat jar" concept from Dropwizard, and Pivotal, the vendor that owns Spring Boot, also has marketed the term "cloud-native", originally coined by Netflix, which is now widely used in the market. Although cloud-native application development existed even before containers and microservices became popular, these have greatly influenced and changed what cloud-native application development is. The concept of fat jars is being replaced by layered container images, which are proving to be more efficient and accelerating, even further, the delivery of a cloud-native application. But I digress. When it comes to runtimes, organizations wanting to develop microservices have been mostly heading in the direction of Node.js and Spring Boot. Although some app servers are very well suited for microservices, a myth of Java EE being slow and a resource hog has unfortunately been spread in the market and has indiscriminately smeared all app servers. But this myth will soon have no legs to stand on anymore. With Jakarta EE, which will have cloud-native enterprise Java capabilities, organizations will have a third option for microservices and cloud-native application development. Organizations that have trusted Java EE to run their production workloads will have a clear path forward into cloud-native application development with Jakarta EE.</p>

<p>Choices of frameworks and languages are good for developers, who are now used to using the right tool for the right task. Pivotal, the owners of Spring, participates in the Jakarta EE Working Group, together with IBM, Red Hat, Oracle, Microsoft, Fujitsu, SAP, Lightbend, among others. So, what does this mean for Spring’s future? There are many possible scenarios for how Jakarta EE and Spring will evolve:</p>
    <ul>
        <li>Pivotal, through collaboration, helps evolve Jakarta EE into the community-driven cloud-native enterprise Java specification it aims to be, resulting in a convergence of capabilities into a single specification</li>
        <li>Jakarta EE does not gain market traction leaving Spring to become the only enterprise Java specification for cloud-native enterprise Java</li>
        <li>Jakarta EE gains market traction displacing Spring</li>
        <li>Jakarta EE gains market share to co-exist with Spring</li>
    </ul>
    
<p>No matter what happens in two years time, I think all developers have already won by having all these vendors, users groups, open source community members, and corporations come together under Jakarta EE and joining forces to develop a cloud-native enterprise Java specification that will serve everyone.</p>

<p>Jakarta EE is a great new dawn for enterprise Java.</p>
   
   
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/cesar.jpg"
        alt="Cesar Saavedra" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Cesar Saavedra<br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/cesar_saavedr">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://github.com/cealsair">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="linkedin.com/in/saavedracesar">LinkedIn</a></li>
            <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>