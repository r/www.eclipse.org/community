<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<h2>A platform is greater than the sum of its parts</h2>
<p>Last year I talked about the exciting news that <a target="_blank" href="https://developer.ibm.com/wasdev/blog/2017/08/18/open-future-java-ee/">Java EE was moving to the Eclipse Foundation</a> for the ongoing development of the platform technologies. There’s an important word in there – "platform".</p>

<p>One of the strengths of Java EE – and a significant factor in its ongoing success – is its notion of platform. Enterprise applications require composable technologies which can be used independently but which also can integrate and cooperate in a well-defined manner when used together. Effective platforms enable these diverse technologies to work together. The platform is greater than just the sum of its parts.</p>

<p>And that’s where <a target="_blank" href="http://www.jakarta.ee/">Jakarta EE</a> comes in. Jakarta EE is the new name for the overall platform comprised initially of the Java EE technologies that are being contributed to the <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation</a>. In the early days of e-business, IBM helped create J2EE and the Java EE platform this grew into, to establish Java as a technology for enterprises. So I’m delighted that IBM is a strategic member of the <a target="_blank" href="https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php">new Jakarta EE working group</a>, continuing to drive the evolution and adoption of Jakarta EE as the platform for cloud-native Java applications.</p>  
   
<h2>Java EE is leaving home</h2>
   <img align="right" class="img-responsive" src="/community/eclipse_newsletter/2018/may/images/IanRobinson-WagonHatchback-Small.jpg">
<p>And as the great and underrated Brian May Queen song says, “Leavin’ home ain’t easy.” That song’s now an ear worm as I write this blog, and it’s true in so many different situations. Sending my teenage daughter off to university was so much harder than just putting all her stuff into the car. But it’s been good for her, and I’m learning to enjoy it, too.</p>

<p>The IBM team knows a thing or two about sending our kids into the great wide open. Last year we moved both our <a target="_blank" href="https://developer.ibm.com/dwblog/2017/websphere-liberty-java-open-source/">WebSphere Liberty development and JVM development</a> into the open. <a target="_blank" href="https://openliberty.io/">Open Liberty</a> is a Java EE runtime today, and I’m looking forward to being able to call it a Jakarta EE runtime in the future.</p>

<p>I know that there will be a few bumps along the way as Java EE leaves its old home – for example choosing the new platform name created passionate debate as you can see in <a target="_blank" href="http://www.tomitribe.com/blog/2018/02/java-ee-to-jakarta-ee/">David Blevins’ post</a>. But by making this a real community effort at the Eclipse Foundation, we’ve had a great deal of success in a short amount of time already. A significant amount of the Java EE reference implementations are now re-homed in projects under the top-level <a target="_blank" href="https://projects.eclipse.org/projects/ee4j">Eclipse Enterprise for Java (EE4J) project</a>. Each reference implementation has its own set of git repositories under the <a target="_blank" href="https://github.com/eclipse-ee4j">eclipse-ee4j github org</a>. Test Compatibility Kits (TCKs) and the platform Compatibility Test Suite (CTS) are still in the pipeline, and we have a new lightweight specification process to finalize. There’s still a lot to do, but progress towards full community ownership of enterprise Java has been impressive. And the IBM team is looking forward to seeing rapid innovation in Jakarta EE now that it’s left home and moved out into the community at Eclipse.</p>


<h2>Where next for Jakarta EE</h2>
<p>The Eclipse Foundation conducted an <a target="_blank" href="https://www.jakarta.ee/documents/insights/2018-jakarta-ee-developer-survey.pdf">Enterprise Java Developer Survey</a> this March. Over 1,800 Java developers responded, and recommended three areas of focus:</p>
	<ul>
        <li>	Better support for microservices</li>
        	<li>Native integration with Kubernetes and Docker</li>
        <li>	Faster pace of innovation</li>
	</ul>

<p>Happily, the survey findings confirmed what we’ve been focusing on at IBM for a while now. We got a head start on these areas when we, along with other members of the community, became founding members of the <a target="_blank" href="http://microprofile.io/">Eclipse MicroProfile community</a> whose goal is to accelerate and extend enterprise Java for microservices. We’ve already seen four releases of Eclipse Microprofile in its 18-month history. I see this as an encouraging example of how the Eclipse Foundation can support a “faster pace of innovation” with the will of a community. The rapid adoption of MicroProfile by many Java EE implementations, including Open Liberty, has made it one of top six Java frameworks for building cloud-native applications.</p>

<p>At IBM, we use Liberty as the Java application environment for our <a target="_blank" href="https://www.ibm.com/cloud/private">IBM Cloud platform</a>. We integrate Liberty’s support for MicroProfile capabilities like Health Check, Metrics and Fault Tolerance with the IBM Cloud platform’s underlying Kubernetes and Istio subsystems. I hope and expect to see the EE-centric technologies we’ve built in MicroProfile inform the future direction of Jakarta EE.</p>

   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/may/images/jakartaee_logo.png"></p>


<h2>Get involved at the Eclipse Foundation</h2>
<p>There’s been a whole lot of new activity at the Eclipse Foundation over the last year – including Jakarta EE, MicroProfile and the OpenJ9 JVM. And I’m proud of IBM’s contributions to all these open source projects.</p>

<p>One common purpose of all of these projects is to enable the widest community participation. So, thanks for reading this! And now... help us build tomorrow’s cloud-native Java platform:</p>
    <ul>
    		<li>Join the <a target="_blank" href="http://www.jakarta.ee/">Jakarta EE community</a> to help us build tomorrow’s enterprise Java platform.</li>
    		<li>Make a difference by becoming a contributor or committer to one of the <a target="_blank" href="https://projects.eclipse.org/projects/ee4j">EE4J projects</a>.</li>
    		<li>Help determine our future direction by joining the <a target="_blank" href="https://www.eclipse.org/org/workinggroups/jakarta_ee_charter.php">Jakarta EE working group</a>.</li>
    </ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/ianr.jpeg"
        alt="Ian Robinson" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Ian Robinson<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ian__robinson">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/drianrobinson/">LinkedIn</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>