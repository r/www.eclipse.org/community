<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>As was discussed in the last post, <a target="_blank" href="https://www.tomitribe.com/blog/2018/05/jakarta-ee-into-the-fourth-epoch/">Jakarta EE: Into the Fourth Epoch</a>, enterprise Java is undergoing the biggest change in the 20-year history of the platform. Although a big part of this is the brand change from Java EE to Jakarta EE as well as a new technical direction (i.e. cloud-native and microservices) the most important change will be the move from a proprietary Java EE to fully open source Jakarta EE platform.</p>

<p>The change in custodianship, from Oracle to the Eclipse Foundation, is fundamentally the most important change to the entire platform. To understand why, it’s important to understand the difference in motivations and mission between for-profit, publicly-traded organizations like Oracle and not-for-profit consortiums like the Eclipse Foundation.</p>
   
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/may/images/cage.jpg"></p>

<h2>The Limitations of a Commercial, For-Profit Custodian</h2>
<p>During the <a target="_blank" href="https://www.tomitribe.com/blog/2018/05/jakarta-ee-into-the-fourth-epoch/">Second and Third Epochs</a> (1999 – 2017) of enterprise Java, Java EE was wholly owned and guided by first Sun Microsystems and then Oracle. Both companies are fundamentally different organizations compared to the Eclipse Foundation because they are profit-driven, publicly traded businesses, while the Eclipse Foundation is a not-for-profit consortium. The first responsibility of any publicly traded corporation, such as Sun Microsystems and Oracle, is to the stockholders that own the company. Every aspect of the publicly traded company is focused on strategies to increase profits.</p>

<p>In retrospect, it seems likely that the profit-centric nature of Sun Microsystems and Oracle was, to a large degree, incompatible with supporting an open, ubiquitous, multi-vendor platform. Simply put, the cost of maintaining, promoting, and advancing the most popular enterprise platform in the world far outweighs the dollar-for-dollar benefits that can be derived from that platform for a single vendor.</p>

<p>Oracle, and Sun Microsystems before it, were essentially creating a market in which open source and their competition could flourish. The idea that a high-tide raises all boats would appear to offset the costs, but if Java EE was so profitable for Oracle why did it donate the platform to an open source consortium? Would it not have made more sense to continue capitalizing on the platform? A profit-driven, publicly traded company only voluntarily abandons a business when there is a poor return-on-investment, ROI. In other words, Oracle must have believed that it could no longer make a profit on the Java EE platform – if it ever did. The fact that Sun Microsystems and Oracle were able to maintain Java EE for so long is a testimony to their tenacity.</p>

<h2>The Advantage of an Open, Not-for-Profit Custodian</h2>
<p>Much of what we consider progress in enterprise computing takes place in the open source community. By its very nature, the emphasis of open source is not the creation of profits, but the creation of value. The two are not the same. A business can generate a lot of profit while providing value, but if it produces value without also producing profit it’s not going to last very long.</p>

<p>As a not-for-profit consortium, the Eclipse Foundation has a fundamentally different orientation. While it was initially formed through the donations of IBM, it is maintained by an ecosystem of open source developers and a plethora of commercial sponsors, led by its Strategic Members. The commercial sponsors (e.g. Strategic Members) provide the funding in terms of pure cash through membership dues, but they also direct their paid open source developers to maintain and advance the projects. At the same time, the open source community, at large, provides volunteer developers for no cost. In this way, the burden of maintaining and advancing the platform is spread across for-profit and dedicated volunteers both of whom benefit from the involvement of the other.</p>

<p>It’s difficult to imagine a popular, widely used, and extensive open source technology that has not benefited from the contributions of commercial sponsors. Linux, Apache, and Mozilla all benefit from the money and or labor contributed by large publicly traded companies like IBM, Oracle, and Google as well as many smaller companies. And while these sponsors provide a lot of the money and talent, the projects themselves would not thrive if not for the contributions of thousands of volunteer open source developers who report bugs, make fixes, and develop new features (see <a target="_blank" href="https://www.tomitribe.com/blog/2018/03/open-source-heroes/">Open Source Heroes</a>).</p>

<p>With the costs and benefits spread across commercial vendors and open source developers, the Jakarta EE platform should be able to innovate more quickly because it is less constrained by for-profit, short-term motives. Open source by itself, without the support of commercial beneficiaries, however, is hard-pressed to make progress due to a lack of resources. Without the contributions of thousands of volunteer open source developers, there would be no community on which commercial vendors can build and sell products and services. It’s a very symbiotic relationship and when one breaks down then the entire project, if substantial, is unlikely to thrive.</p>

<h2>The Need for an Open Source Custodian</h2>
<p>While the commercial motivations of sponsors will sometimes come into conflict with the not-for-profit, long-term motivations of open source developers, overall a stable balance is achieved. Vendors will not support the development of solutions that their customers do not need, and open source will not develop software that doesn’t provide real, long-term value. This arrangement is symbiotic, but it’s not perfect or self-governing. Someone has to maintain the peace and balance.</p>

<p>This is where the open source organizations like the Linux Foundation, Apache Foundation, Mozilla, and the Eclipse Foundation comes into play. These open source foundations have the hard job of balancing the needs of its commercial sponsors with those of the open source contributors. Large open source projects simply cannot survive without both of them. It is the open source foundations that have the job of keeping both contingents happy and that can be a difficult, but rewarding balancing act.</p>

<h2>The Eclipse Foundation as a Custodian for Jakarta EE</h2>
<p>The Eclipse Foundation, which is the new home of Java EE under the Jakarta EE brand, is well suited for custodianship. It has proven, for over 14 years, capable of not only managing the open source development of some of the most common tools in Java development but in maintaining a solid relationship with commercial sponsors (i.e. Strategic Members). It has the infrastructure and know-how to balance the sometimes differing concerns of commercial vendors with each other and the larger open source development community. But, like any organization, it’s not perfect and is likely to make mistakes. The Eclipse Foundation is entering new territory having to wrestle with a new set of intellectual property concerns, as well as, developing an open specification process. Operating under the premise of “business as usual” simply will not work. The Eclipse Foundation will need to change, in some cases fundamentally, how it governs Jakarta EE. If not, then the commitment of sponsors and the larger open source community to the Jakarta EE platform will vanish.</p>

<h2>Optimistic for the Future</h2>
<p>The next couple of years will be extremely challenging for the Java community as we establish policies, processes, and norms for managing the Jakarta EE platform. There are likely to be conflicts among sponsors, the Eclipse Foundation leadership, and the larger community over the years, but I’m optimistic that we can work together to overcome the challenges.</p>

<p><i>Special thanks to Paul White (<a target="_blank" href="https://twitter.com/EclipseEE4J">@EclipseEE4J</a>), Mark Little (<a target="_blank" href="https://twitter.com/nmcl">@nmcl</a>), and Mark Struberg (<a target="_blank" href="https://twitter.com/struberg">@struberg</a>) for reviewing an early draft and providing invaluable feedback before publishing.</i></p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/richard.jpg"
        alt="Richard Monson-Haefel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Richard Monson-Haefel<br />
            <a target="_blank" href="https://www.tomitribe.com/">Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/rmonson">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.tomitribe.com/blog/author/rmonson/">Blog</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>