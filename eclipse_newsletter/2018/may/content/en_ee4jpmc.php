<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>We asked the Project Management Committee Lead of the EE4J, Ivar Grimstad, to answer a few questions about EE4J and Jakarta EE. We hope this short interview provides you with a quick insight into the motivation behind EE4J and Jakarta EE before reading the rest of the articles in this month’s newsletter.</p>

<p><b>1. What is EE4J?</b><br>
<a target="_blank" href="https://projects.eclipse.org/projects/ee4j/charter">Eclipse Enterprise for Java (EE4J)</a> is the top-level project at the Eclipse Foundation for all the projects that will form the base for Jakarta EE. The EE4J projects are a mix of API projects and open source implementations of those APIs. It also contains projects for the TCKs (Technology Compatibility Kit).</p>

<p><b>2. What is Jakarta EE?</b><br>
<a target="_blank" href="https://jakarta.ee/">Jakarta EE</a> is the name of the platform governed by the Jakarta EE Working Group. The first version will be Jakarta EE 8 which will be based on the Java EE 8 technologies transferred from Oracle to the Eclipse Foundation.</p>

<p><b>3. What is the difference between EE4J and Jakarta EE? Are there two top-level projects?</b><br>
Jakarta EE is the brand name for the platform, not a top-level project. It  is based on technologies produced by the EE4J projects. The Jakarta EE platform may also include projects outside of EE4J.</p>

<p><b>4. What is the role of the PMC?</b><br>
The EE4J Project Management Committee (PMC) is responsible for maintaining the overall vision for the top level project. It will set the standards and requirements for releases and help the projects communicate and cooperate.</p>

<p><b>5. What does it mean for Java EE to move from proprietary to open?</b><br>
I am not sure I agree with characterizing Java EE as proprietary. Java EE has been a community-driven standard for enterprise software developed through the Java Community Process (JCP). Contributions have come from commercial organizations, individuals, and non-profit organizations. The main difference going forward will be in the licensing and flow of intellectual property. Rather than having Expert Groups and Specification Leads, as we are used to from the JCP, the projects will be run as traditional open source projects. This means that there will not be a single specification lead (Company) that owns the intellectual property.</p>

<p><b>6. How do you see Java EE evolving at the Eclipse Foundation as Jakarta EE?</b><br>
I expect to see Jakarta EE evolve faster at the Eclipse Foundation with more frequent releases and shorter time-to-market for newer technologies and innovation to enable standardized, portable, cloud-native Java applications.</p>

<p><b>7. How do you feel about the move of Java EE teachnologies to the Eclipse Foundation?</b><br>
The massive amount of traffic on the mailing lists shows how engaged this community is. It just continues to amaze me how many individuals and companies that are so eager to contribute that they can barely wait. The folks at Oracle and the Eclipse Foundation are doing an excellent job of getting the transfer done and bootstrapping all the projects.</p>

 <div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/ivar.jpeg"
        alt="Ivar Grimstad" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Ivar Grimstad<br />
            <a target="_blank" href="https://www.cybercom.com/">Cybercom</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ivar_grimstad">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ivargrimstad">GitHub</a></li>
          <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>