<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2018.05.31</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxannenew.png" />
                            <h3>Editor's Note</h3>
                            <p>This month's newsletter brings exciting news. Starting now, we will dedicate four issues of the Eclipse Newsletter to <a style="color:#000;" target="_blank" href="https://jakarta.ee/">Jakarta EE</a> each year. They will be spread out quarterly and the remainder of the newsletters will cover various topics as usual. The idea is to keep you updated with the fast pace and various changes happening around Cloud Native Java.</p>
                            <p>To start, we have eight great pieces to inform you about various parts of Jakarta EE. It starts with what Jakarta EE is and what has happened since the announcement last September (that Java EE technologies were coming to the Eclipse Foundation), to technical articles about Java EE 8 and Reactive Streams.<br>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeeprogress.php">Jakarta EE Progress to Date</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeechallenges.php">Jakarta EE Challenges</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/ee4jpmc.php">5 minutes with the PMC Lead of EE4J</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/noturningback.php">Jakarta EE - No Turning Back</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaenterprise-newhome.php">Jakarta EE – The new home for enterprise Java</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/proprietarytoopen.php">Proprietary to Open: A Sea Change for Jakarta EE</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaee8.php">Java EE 8: the baseline of Jakarta EE</a><br>
								&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/reactivestreams-ee4j.php">What can Reactive Streams offer EE4J?</a></p>
							<p>Be sure to join us next week at EclipseCon France in Toulouse, on June 13-14, to learn more about Jakarta EE and many other great Eclipse projects. <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/france2018/conference/schedule/session/2018-06-13">Check out the program</a>!</p>   
                            <p>Can't make it to EclipseCon France? <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/europe2018/cfp">Propose a talk</a> now for EclipseCon Europe, Oct 23-25 in Ludwigsburg, Germany.</p>   
                            <p>Finally, the Eclipse Foundation has updated its <a style="color:#000;" target="_blank" href="https://www.eclipse.org/legal/privacy.php">privacy policy</a>. We encourage you to check it out. As always, you can unsubscribe from the newsletter at any time, and if you do so we will permanently remove your personal information from our subscription list.</p>       
                            <p>Happy reading!</p>
                            
                            <p>Roxanne Joncas <br>
                            <a style="color:#000;" target="_blank" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

              <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a target="_blank" href="https://www.jakarta.ee"><img class="img-reactive"
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/april/images/jakartaee_newsletter.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

	
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeeprogress.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeeprogress.php"><h3>Jakarta EE Progress to Date</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>What is Jakarta EE? Why has it moved to the Eclipse Foundation? What has been the progress to date?</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeechallenges.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/jakartaeechallenges.php"><h3>Jakarta EE Challenges</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>What are the current and future challenges of Jakarta EE?</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/ee4jpmc.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/ee4jpmc.php"><h3>5 minutes with the PMC Lead of EE4J</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>EE4J PMC Lead, Ivar Grimstad gave us 5 minutes of his time to tell us about EE4J and Jakarta EE.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/noturningback.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/noturningback.php"><h3>Jakarta EE - No Turning Back</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Read this great opinion piece from someone who's been in the IT field for almost 30 years!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
         <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaenterprise-newhome.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaenterprise-newhome.php"><h3>Jakarta EE – The new home for enterprise Java</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Jakarta EE is the new name for the overall platform comprised initially of the Java EE technologies that are being contributed to the Eclipse Foundation.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/proprietarytoopen.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/proprietarytoopen.php"><h3>Proprietary to Open: A Sea Change for Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Java is undergoing the biggest change in the 20-year history of the platform.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
               <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaee8.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/7.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/javaee8.php"><h3>Java EE 8: the baseline of Jakarta EE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Java EE 8 and GlassFish 5 will be the baseline for the initial Jakarta EE version. Take a deeper look at what is new and noteworthy in Java EE 8.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/reactivestreams-ee4j.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/8.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/may/reactivestreams-ee4j.php"><h3>What can Reactive Streams offer EE4J?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Discover the specific parts of EE4J where Reactive Streams would be useful.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
        <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a target="_blank" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/vecm-schedule.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://blogs.eclipse.org/post/thabang-mashologu/updates-our-privacy-policy">Updates to our Privacy Policy</a></li>
                            		<li><a target="_blank" style="color:#000;" href="http://www.eclipsecon.org/europe2018/cfp">EclipseCon Europe 2018 | Call for Papers Open</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://eclipse.org/org/workinggroups/osbea_wg_charter.php">New Proposed Charter for OS.bea Working Group</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.youtube.com/watch?v=nvzqdQmw9gY&feature=youtu.be">Eclipse Common Build Infrastructure: Upcoming Changes</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2018/05/latest-ee4j-activities">Latest Roundup of EE4J Activities for Jakarta EE</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoworld.com/article/3269210/java/java-roadmap-eclipses-jakarta-ee-enterprise-java-takes-shape.html">Java roadmap: Eclipse’s Jakarta EE enterprise Java takes shape</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipse-photon-m7-144442.html">What’s new in Eclipse Photon M7?</a></li>
								<li><a target="_blank" style="color:#000;" href="https://medium.com/@nikhilnanivadekar/eclipse-collections-a-community-story-dc63d0226fc2">Eclipse Collections: A Community Story</a></li>
								<li><a target="_blank" style="color:#000;" href="https://dzone.com/articles/elasticsearch-a-powerful-and-easy-search-engine-di-2">Eclipse JNoSQL: Connecting Elasticsearch Directly to your Java EE Application</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipse-microprofile-intro-java-community-143818.html">Eclipse MicroProfile: What it has to offer to the Java community</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.de/eclipse-iot-developer-survey-2018-70439">Eclipse IoT Developer Survey 2018: Die wichtigsten Trends der IoT-Entwickler</a></li>
							</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-jakarta-ee-platform">Eclipse Jakarta EE Platform</a>: produces the Jakarta EE platform specification, which is an umbrella specification that aggregates all other Jakarta EE specifications.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-metro">Eclipse Metro</a>: is a high-performance, extensible, easy-to-use web service stack. It is a one-stop shop for all your web service needs, from the simplest hello world web service to reliable, secured, and transacted web service that involves .NET services.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-orb">Eclipse ORB</a>: is a runtime component that can be used for distributed computing using IIOP communication.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-tahu">Eclipse Tahu</a>: addresses the existence of legacy SCADA/DCS/ICS protocols and infrastructures and provides a much-needed definition of how best to apply MQTT into these existing industrial operational environments.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-volttron">Eclipse VOLTTRON</a>: provides secure, extensible, and modular integration middleware for the Internet of Things, that supports the development of a wide range of loosely coupled, but tightly integrated connected applications.</li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
								
								<li><a target="_blank" style="color:#000;" href="https://medium.com/oracledevs/eclipse-collections-9-2-released-aa88b56ecacc">Eclipse Collections 9.2</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.dirigible/reviews/3.2-release-review">Eclipse Dirigible 3.2</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.efxclipse/reviews/3.3.0-release-review">Eclipse e(fx)clipse 3.3</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/hono/release-notes/">Eclipse Hono 0.6</a></li>
								<li><a target="_blank" style="color:#000;" href="https://mosquitto.org/blog/2018/05/press-release/">Eclipse Mosquitto 1.5</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.openj9/reviews/0.9.0-release-review">Eclipse OpenJ9 0.9</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.uomo/reviews/0.7.0-release-review">Eclipse UOMo 0.7</a></li>
								<li><a target="_blank" style="color:#000;" href="https://blogs.itemis.com/en/announcement-xtext-2-14-rc1-is-now-available">Eclipse Xtext 2.14</a></li>
                             </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/may/images/may_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2018/">EclipseCon France 2018</a><br>
                            Jun 13-14, 2018 | Toulouse, France</p>
                            <p><a target="_blank" style="color:#000;" href="https://jnation.pt/">JNation 2018</a><br>
                            Jun 19, 2018 | Coimbra, Portugal</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_2018/Eindhoven">Eclipse DemoCamp Eindhoven</a><br>
                            Jul 4, 2018 | Eindhoven, Netherlands</p>
                             <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2018/">EclipseCon Europe 2018</a><br>
                            Oct 23-25, 2018 | Ludwigsburg, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
