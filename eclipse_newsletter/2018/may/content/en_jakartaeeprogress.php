<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>Last September <a target="_blank" href="https://blogs.oracle.com/theaquarium/opening-up-ee-update">Oracle announced</a>, with the support of IBM and Red Hat, that Java EE was going to move to the Eclipse Foundation. Since then Fujitsu, Payara and Tomitribe have all joined the initiative with strategic-level commitments. The scale of this migration is huge, and if you’re interested in understanding the complexity of the undertaking, I highly recommend you read <a target="_blank" href="https://mmilinkov.wordpress.com/2018/03/23/on-complexity-and-good-intentions/">On Complexity and Good Intentions</a>.</p> 

<p>Roughly eight months later, let’s see how we are doing compared to the goals set out in Oracle’s original announcement.</p>

<p> &rarr; <i><b>Relicense Oracle-led Java EE technologies, and related GlassFish technologies, to the Foundation. This would include RIs, TCKs, and associated project documentation.</i></b></p>
<p>This has been the major accomplishment to date. The Eclipse Enterprise for Java (EE4J) top-level project has been created, and thirty-nine projects established. We don’t yet have all of the source code moved over, but you can follow the steady progress on the <a target="_blank" href="https://www.eclipse.org/ee4j/status.php">project status page</a>. All of the projects are using the same license: Eclipse Public License 2.0 plus (Secondary) GNU General Public License, version 2 with the GNU Classpath Exception.</p>

<p> &rarr; <i><b>Demonstrate the ability to build a compatible implementation, using Foundation sources, that passes existing Java EE 8 TCKs.</i></b></p>
<p>This is what the project community is working towards next, and it is going to be a Very Big Deal. Keep in mind that to accomplish this goal, we are going to have to complete the creation of 39 projects, get them building on eclipse.org infrastructure, run the Java EE 8 CTS on those builds, and then get all of the projects together to ship on the same day. There is an enormous amount of work and learning to be done before this release becomes a reality.</p>

<p> &rarr; <i><b>Define a branding strategy for the platform within the Foundation, including a new name for Java EE to be determined. We intend to enable use of existing javax package names and component specification names for existing JSRs to provide continuity.</i></b></p>

<p>In the first quarter of 2018, the Eclipse Foundation worked with the community to establish a <a target="_blank" href="https://mmilinkov.wordpress.com/2018/02/26/and-the-name-is/">new name</a> and a logo for the future of this technology. So <a target="_blank" href="https://jakarta.ee/">Jakarta EE</a> was born, and we’re very excited about the new name and the logo. Folks who have been around the Java world for a long time will recognize the name “Jakarta,” as the Apache Software Foundation’s had a long-lived incubator under that name. With <a target="_blank" href="https://www.tomitribe.com/blog/2018/02/java-ee-to-jakarta-ee/">their kind permission</a> what was old is new again. We’re really excited about once again seeing Java innovation happening under the Jakarta banner.</p>

   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/may/images/jakartaee_logo.png"></p>


<p> &rarr; <i><b>Define a process by which existing specifications can evolve, and new specifications can be included in the platform.</i></b></p>

<p>The first step in creating this process was to establish the <a target="_blank" href="https://mmilinkov.wordpress.com/2018/02/05/introducing-the-ee-next-working-group/">Jakarta EE Working Group</a>, which will be the governing body for this community moving forward. This was completed back in March, and the various committees are hard at work.</p>

<p>Discussions to create the new Jakarta EE Specification Process are underway, and early results are promising. This is going to be a very large job, but one which is essential to the future of this technology.</p>

<p> &rarr; <i><b>Recruit and enable developers and other community members, as well as vendors, to sponsor platform technologies, and bring the platform forward within the foundation. This would include potential incorporation of Eclipse MicroProfile technologies into the platform.</i></b></p>

<p>So far so good. Since the original announcement with Oracle, IBM and Red Hat back in September, here is the list of companies that have joined the initiative:</p>
<p><b>Strategic Members</b>: Fujitsu, Payara, and Tomitribe<br>
<b>Participating Members</b>: DocDuko, Genuitec, IncQueryLabs, Lightbend, London Java Community, Microsoft, Pivotal, RCPVision, SAP, UseOpen, Vaadin, and Webtide</p>

<p>More importantly, we’ve seen a lot of developers from the Java EE community jump in and participate in the new projects as they’ve been set up. It has been great to welcome so many new people to the Eclipse community.</p>

<p>Eclipse MicroProfile has not joined the Jakarta EE initiative. But those conversations cannot even really start until the new specification process is available for that community to review.</p> 

<p> &rarr; <i><b>Begin doing the above as soon as possible after completion of Java EE 8 to facilitate a rapid transition.</i></b></p>

<p>We’ve all been busy! It’s hard to say if the transition has been rapid enough, but everyone involved has been working hard and making progress. The process has not been and will not be perfect. With so many moving parts, people, and technologies, perfection is the enemy of good. But we’ve been very excited by the process we’re making. In particular, we have been excited by the passion and energy of the Java EE community as they have embraced the future with Jakarta EE.</p> 
 
 <div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/mike.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Mike Milinkovich<br />
            <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://mmilinkov.wordpress.com/">Blog</a></li>
          <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>