<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><i>Java EE 8 and its reference implementation GlassFish 5 will be the baseline for the initial Jakarta EE version (a.k.a. Jakarta EE 8 and Eclipse Glassfish 5.x). Therefore, it is worth to take a deeper look at what is new and noteworthy in Java EE 8 compared to its predecessors.</i></p> 

<h2>Java EE 8 advertising mottos</h2> 
<p>One of the main goals of the JEE 8 specification is to consequently continue what started in JEE 6 and JEE 7, in particular supporting the two mottos “Modern Web Development” and “Ease of Development”.</p>

<p>Topics like HTTP/2 including push instead of pull, advanced JSON support and a reactive programming model are as well on the list of new features as advanced support of new Java 8 lambda and stream APIs or CDI enhancements like asynchronous events.</p>  

<h2>Brave new Web World</h2> 
<p>First of all, it is worth mentioning that the JSF managed bean facility is deprecated since JSF version 2.3 (finally!). So please, please use CDI managed beans only in the future: <i>“As of version 2.3 of this specification, use of the managed bean facility as specified in this section is strongly discouraged. A better and more cohesively integrated solution for solving the same problem is to use Contexts and Dependency Injection (CDI), as specified in JSR-365.”</i></p>

<p>Since the new <a target="_blank" href="https://www.jcp.org/en/jsr/detail?id=372">version 2.3</a>, JSF offers a tag for WebSocket support. Combined with an injectable push context on the server side, it is quite easy to implement the WebSocket protocol inside a JSF application (listing 01 and 02).</p>
   
<pre style="background:#fff;color:#000"><span style="color:#1c02ff">&lt;!<span style="color:#888"><span style="color:#888">DOCTYPE</span> html</span>></span>
<span style="color:#1c02ff">&lt;<span style="font-weight:700">html</span> <span style="font-style:italic">xmlns</span>=<span style="color:#036a07">"http://www.w3.org/1999/xhtml"</span>
    <span style="font-style:italic">xmlns:f</span>=<span style="color:#036a07">"http://xmlns.jcp.org/jsf/core"</span>
    <span style="font-style:italic">xmlns:h</span>=<span style="color:#036a07">"http://xmlns.jcp.org/jsf/html"</span>></span>
    <span style="color:#1c02ff">&lt;<span style="font-weight:700">h:head</span>></span>
        Websocket Example
    <span style="color:#1c02ff">&lt;/<span style="font-weight:700">h:head</span>></span>
    <span style="color:#1c02ff">&lt;<span style="font-weight:700">h:body</span>></span>
        <span style="color:#1c02ff">&lt;<span style="font-weight:700">f:websocket</span> <span style="font-style:italic">channel</span>=”<span style="font-style:italic">helloWorldChannel</span><span style="color:#036a07">" 
            onmessage="</span><span style="font-style:italic">function</span>(<span style="font-style:italic">message</span>){alert(message)}<span style="color:#036a07">" />
    &lt;/h:body>
&lt;/html>
</span></span></pre>
<br/>

<p><b>Listing 01: JSF view with f:websocket tag</b></p>

<pre style="background:#fff;color:#000">@<span style="color:#00f;font-weight:700">Named</span>
@<span style="color:#00f;font-weight:700">RequestScoped</span>
<span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">class</span> <span style="text-decoration:underline">SomeBean</span> {
 
    <span style="color:#06f;font-style:italic">// WebSocket push channel with name “helloWorldChannel”</span>
    <span style="color:#00f;font-weight:700">@Inject</span> <span style="color:#00f;font-weight:700">@Push</span>(<span style="color:#c5060b;font-weight:700">channel</span> <span style="color:#00f;font-weight:700">=</span> “helloWorldChannel””
    <span style="color:#00f;font-weight:700">private</span> <span style="color:#00f;font-weight:700">PushContext</span> channel;
 
    <span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">void</span> send(){
        channel<span style="color:#00f;font-weight:700">.</span>send(<span style="color:#036a07">"Hello, push world!"</span>);
    }
)
</pre>
<br/>

<p><b>Listing 02: CDI bean with PushContext</b></p>

<p>In addition, JSF 2.3 offers some convenient producers to allow easy annotation-based access to JSF artifacts like application, session, request or header.</p> 

<pre style="background:#fff;color:#000">Inject
@SessionMap
private Map&lt;String, Object> SessionMap;
</pre>  

<p>Also interesting is a new JSF tag for class level bean validation <code>&lt;f:validateWholeBean&gt;</code> that in my personal opinion is long overdue. With the help of this tag not only attributes of the CDI backed bean can be validated via BeanValidation but the bean as a whole. This allows checks like the equality of password and password repeat in a very easy manner.</p> 

<h2>RESTful API Design</h2> 
<p>The second big block of changes and enhancements addresses the field of RESTful API design.</p> 

<p>Even though the new <a target="_blank" href="https://jcp.org/en/jsr/detail?id=370">JAX-RS version 2.1</a> is only a minor release, it offers a lot of new features, first and foremost to support non-blocking I/O and asynchronous request handling.</p> 

<p>The JAX-RS client is now able to support the Java 8 CompletionStage. This feature really helps to prevent the problem of nested callback handlers (a.k.a. <i>pyramid of doom</i>). Listing 03 shows a simple example taken from the original JAX-RS 2.1 specification. The example shows a client sending two asynchronous REST calls whose results will be reunited with the help of a <i>CompletionStage</i>.</p> 

<pre style="background:#fff;color:#000"><span style="color:#06f;font-style:italic">// async REST call no 1 (via RxInvoker using CompletionStage)</span>
 CompletionStage&lt;<span style="color:#00f;font-weight:700">String</span>> cs1 <span style="color:#00f;font-weight:700">=</span> client.<span style="color:#3c4c72;font-weight:700">target</span>(<span style="color:#036a07">"http://my-domain-one.com/api"</span>)
     .<span style="color:#3c4c72;font-weight:700">request</span>()
     .rx()
     .<span style="color:#3c4c72;font-weight:700">get</span>(<span style="color:#00f;font-weight:700">String</span>.class);

<span style="color:#06f;font-style:italic">// async REST call no 2 (via RxInvoker using CompletionStage)</span>
 CompletionStage&lt;<span style="color:#00f;font-weight:700">String</span>> cs2 <span style="color:#00f;font-weight:700">=</span> client.<span style="color:#3c4c72;font-weight:700">target</span>(<span style="color:#036a07">"http://my-domain-two.com/api"</span>)
     .<span style="color:#3c4c72;font-weight:700">request</span>()
     .rx()
     .<span style="color:#3c4c72;font-weight:700">get</span>(<span style="color:#00f;font-weight:700">String</span>.class);

 <span style="color:#06f;font-style:italic">// combining result of async REST call no 1 and no 2</span>
 CompletionStage&lt;<span style="color:#6d79de;font-weight:700">List</span>><span style="color:#00f;font-weight:700">String</span>>> <span style="color:#3c4c72;font-weight:700">list</span> <span style="color:#00f;font-weight:700">=</span> cs1.thenCombine(cs2, <span style="color:#3c4c72;font-weight:700">list</span><span style="color:#00f;font-weight:700">:</span><span style="color:#00f;font-weight:700">:</span>of); 
</pre>
<br/>

<p><b>Listing 03: reactive client via RxInvoker</b></p>

<p>Also new and noteworthy in JAX-RS 2.1 is the support of the <a target="_blank" href="https://www.w3.org/TR/eventsource/">Server-Sent Event specification</a>. The new API allows to push DOM events from server to a specific client or – via broadcasting - to a group of registered clients. While listing 04 demonstrates how to establish an SSE connection via JAX-RS client, listing 05 shows the corresponding JAX-RS server that pushes events to all registered clients via broadcast mechanism.</p> 

<pre style="background:#fff;color:#000"><span style="color:#318495">WebTarget</span> <span style="font-style:italic">target</span> <span style="color:#00f;font-weight:700">=</span> client.target(<span style="color:#036a07">"http://..."</span>);
 try (<span style="color:#318495">SseEventSource</span> <span style="font-style:italic">source</span> <span style="color:#00f;font-weight:700">=</span> SseEventSource.target(<span style="color:#318495">target</span>).build()) {
     source.register(<span style="color:#318495">System.out</span><span style="color:#00f;font-weight:700">:</span><span style="color:#00f;font-weight:700">:</span><span style="color:#318495">println</span>);
     source.open();
     Thread.sleep(<span style="color:#0000cd">5000</span>); <span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> <span style="color:#318495">Consume</span> <span style="color:#318495">events</span> <span style="color:#00f;font-weight:700">for</span> <span style="color:#318495">just</span> <span style="color:#0000cd">5000</span> <span style="color:#318495">ms</span>
 } catch (<span style="color:#318495">InterruptedException</span> <span style="color:#318495">e</span>) {
    <span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> <span style="color:#318495">do</span> <span style="color:#318495">some</span> <span style="color:#318495">intelligent</span> <span style="color:#318495">exception</span> <span style="color:#318495">handling</span> 
        <span style="color:#00f;font-weight:700">...</span>
 }
</pre>
<br/>

<p><b>Listing 04: JAX-RS SSE client</b></p>

<pre style="background:#fff;color:#000">@<span style="color:#00f;font-weight:700">Singleton</span>
<span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">class</span> <span style="text-decoration:underline">SseResource</span> {

    <span style="color:#00f;font-weight:700">@Context</span>
    <span style="color:#00f;font-weight:700">private</span> <span style="color:#00f;font-weight:700">Sse</span> sse;

    <span style="color:#00f;font-weight:700">private</span> <span style="color:#00f;font-weight:700">SseBroadcaster</span> sseBroadcaster;

    <span style="color:#00f;font-weight:700">@PostConstruct</span>
    <span style="color:#00f;font-weight:700">public</span> <span style="color:#0000a2;font-weight:700">init</span>() {
        <span style="color:#318495">this</span><span style="color:#00f;font-weight:700">.</span>sseBroadcaster <span style="color:#00f;font-weight:700">=</span> sse<span style="color:#00f;font-weight:700">.</span>newBroadcaster();
    }
    
    <span style="color:#00f;font-weight:700">@GET</span>
    <span style="color:#00f;font-weight:700">@Path</span>(<span style="color:#036a07">"register"</span>)
    <span style="color:#00f;font-weight:700">@Produces</span>(<span style="color:#00f;font-weight:700">MediaType</span><span style="color:#c5060b;font-weight:700"><span style="color:#00f;font-weight:700">.</span>SERVER_SENT_EVENTS</span>)
    <span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">void</span> <span style="color:#0000a2;font-weight:700">register</span>(@<span style="color:#00f;font-weight:700">Context</span> <span style="color:#00f;font-weight:700">SseEventSink</span> <span style="font-style:italic">eventSink</span>) {
        eventSink<span style="color:#00f;font-weight:700">.</span>send(sse<span style="color:#00f;font-weight:700">.</span>newEvent(<span style="color:#036a07">"welcome!"</span>));
        sseBroadcaster<span style="color:#00f;font-weight:700">.</span>register(eventSink);
    }
  
    <span style="color:#00f;font-weight:700">@POST</span>
    <span style="color:#00f;font-weight:700">@Path</span>(<span style="color:#036a07">"broadcast"</span>)
    <span style="color:#00f;font-weight:700">@Consumes</span>(<span style="color:#00f;font-weight:700">MediaType</span><span style="color:#c5060b;font-weight:700"><span style="color:#00f;font-weight:700">.</span>MULTIPART_FORM_DATA</span>)
    <span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">void</span> <span style="color:#0000a2;font-weight:700">broadcast</span>(@<span style="color:#00f;font-weight:700">FormParam</span>("<span style="font-style:italic">event</span>") String event) {
       sseBroadcaster<span style="color:#00f;font-weight:700">.</span>broadcast(sse<span style="color:#00f;font-weight:700">.</span>newEvent(event));
    } 
}
</pre>
<br/>

<p><b>Listing 05: JAX-RS SSE broadcast server</b></p>

<p>With JSON-B 1.0 (<a target="_blank" href="https://jcp.org/en/jsr/detail?id=367">JSON Binding API</a>) a missing piece finds its way into the Java Enterprise standard. With the help of the JSON-B API Java objects can be automatically transformed into JSON objects and vice versa. This is a real step forward, because it means that there is no need for the (mis)usage of JAX-B for this task anymore.</p>

<p>To adapt the default mapping behavior various annotations can be used. So, for example, individual fields can be hidden via <i>@JsonbTransient</i> and default attribute names can be overridden via <i>@JsonProperty</i>. These two examples are only a few of many customizing possibilities.</p> 

<p>But JSON-B is not the only API indicating the growing importance of REST and JSON for the Java Enterprise standard. The JSON-P API also got some really interesting new features. Besides some minor issues, like the introduction of a <i>JsonCollectors</i> helper class and the alignment with the Java 8 Stream APIs, the added support for JSON pointer and JSON patch are the most outstanding enhancements of <a target="_blank" href="https://www.jcp.org/en/jsr/detail?id=374">JSON-P 1.1</a>.</p>

<p>JSON pointer defines a string expression that identifies a specific value or part within a JSON object. So, it is kind of similar to XPointer (for XML documents). In addition, JSON pointer can be used for JSON manipulation. Listing 06 shows an example where an element is added to an array – named “hobbies” - of a JSON object first and an existing element of the same array is replaced afterwards.</p>

<pre style="background:#fff;color:#000"><span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> original json object 
JsonObject jsonObject <span style="color:#00f;font-weight:700">=</span> (JsonObject) <span style="color:#c5060b;font-weight:700">JsonUtil</span>.<span style="color:#c5060b;font-weight:700">toJson</span>(
“{
    ‘name’: ‘lars’, 
    ‘hobbies’: [
        ‘swimming’, 
        ‘reading’
    ]
}”

<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> extract hobbies <span style="color:#00f;font-weight:700">from</span> JSON structure 
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> <span style="color:#00f;font-weight:700">and</span> insert “biking” <span style="color:#00f;font-weight:700">as</span> new first element 
JsonPointer pointer <span style="color:#00f;font-weight:700">=</span> <span style="color:#c5060b;font-weight:700">Json</span>.<span style="color:#c5060b;font-weight:700">createPointer</span>(“<span style="color:#00f;font-weight:700">/</span>hobbies<span style="color:#00f;font-weight:700">/</span><span style="color:#0000cd">0</span>”); 
<span style="color:#c5060b;font-weight:700">pointer</span>.<span style="color:#c5060b;font-weight:700">add</span>(jsonObject, <span style="color:#c5060b;font-weight:700">Json</span>.<span style="color:#c5060b;font-weight:700">createValue</span>(“biking”)); 

<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> replace 3rd element (reading) with new value (running)
pointer <span style="color:#00f;font-weight:700">=</span> <span style="color:#c5060b;font-weight:700">Json</span>.<span style="color:#c5060b;font-weight:700">createPointer</span>(“<span style="color:#00f;font-weight:700">/</span>hobbies<span style="color:#00f;font-weight:700">/</span><span style="color:#0000cd">2</span>”);
JsonObject newJsonObject <span style="color:#00f;font-weight:700">=</span> <span style="color:#c5060b;font-weight:700">pointer</span>.<span style="color:#c5060b;font-weight:700">replace</span>(jsonObject, <span style="color:#c5060b;font-weight:700">Json</span>.<span style="color:#c5060b;font-weight:700">createValue</span>(“running”)); 

<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> manipulated json object 
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> {
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>     ‘name’: ‘lars’, 
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>     ‘hobbies’: [
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>         ‘biking’,
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>         ‘swimming’, 
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>         ‘running’
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span>     ] 
<span style="color:#00f;font-weight:700">/</span><span style="color:#00f;font-weight:700">/</span> }
</pre>
<br/>

<p><b>Listing 06: JSON manipulation via JSON pointer</b></p>

<p>The usage of JSON pointer for JSON manipulation can become kind of complex when there are more than one or two operations – add, remove, replace, move copy or test - to fulfill. In a case like this, it would make sense to use JSON patch instead. Listing 07 shows an example where the value of one JSON attribute is copied into another attribute (“name” to “nickname”) and in addition a JSON array is manipulated.</p> 

<pre style="background:#fff;color:#000">// original json object 
JsonObject targetObject = (JsonObject) JsonUtil.toJson(
    “ {                               “ +
    “   ’name': 'lars,                “ +
    “   ’nickname ': 'mobileLarson',  “ +
    “   ’hobbies': [                  “ +
    “        ’swimming', ’running'    “ +
    “   ]                             “ +
    “ }                               “ ); 

// patch operations to apply  
JsonArray patch = (JsonAray) JsonUtil.toJson(
    “ [                                                            “ +
    “    { ’op': 'copy', 'path': '/nickname', 'from': '/name' },   “ +
    “    { ’op': 'remove', 'path': '/hobbies/1},                   “ +
    “    { ’op': 'add', 'path': '/hobbies/-', 'value': 'cycling' } “ +
    “]                                                             “ ); 
JsonPatch patch = new JsonPatch(patch)

// apply patch will result in: 
// {
//    ‘name’ : ‘lars’
//    ‘nickname’ : ‘lars’
//    ‘hobies’ : [
//       ‘swimming’, ‘cycling‘
//    ]
// }
JsonStructure result = patch.apply(target)
</pre>
<br/>

<p><b>Listing 07: JSON manipulation via JSON patch</b></p>

<h2>Old friends refurbished</h2> 
<p>In addition to the so far described enhancements, there are two more APIs that got a kind of face-lift: <a target="_blank" href="http://cdi-spec.org">CDI 2.0</a> and <a target="_blank" href="http://beanvalidation.org/specification/">Bean Validation 2.0</a>:</p> 

<p>CDI 2.0 is now aligned with Java 8. Features like streams, lambdas or repeating qualifiers can be used in a transparent way. In addition, the order of CDI observer handling at runtime can be forced via @Priority annotation, that is part of Commons Annotations for the Java Platform (JSR 250). Next to these rather minor enhancements, the CDI specification has been split into three parts:</p> 
	<ul>
        <li>Core CDI</li>
        <li>CDI in Java SE</li>
        <li>CDI in Java EE</li>
	</ul>
	
<p>With the help of CDI in Java SE it is now possible to bootstrap a CDI container in a Java SE application in a standardized way and to use all CDI core features, like injection, qualifiers, producers, scopes, stereotypes, observers and so on (listing 08).</p> 

<pre style="background:#fff;color:#000"><span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">static</span> <span style="color:#3c4c72;font-weight:700">void</span> main(<span style="color:#00f;font-weight:700">String</span>... args) {
    SeContainerInitializer containerInit <span style="color:#00f;font-weight:700">=</span> SeContainerInitializer.newInstance();
    SeContainer container <span style="color:#00f;font-weight:700">=</span> containerInit.<span style="color:#3c4c72;font-weight:700">initialize</span>();
    <span style="color:#06f;font-style:italic">// retrieve a bean and do work with it</span>
    MyWorkerBean myBean <span style="color:#00f;font-weight:700">=</span> container.select(MyWorkerBean.class).<span style="color:#3c4c72;font-weight:700">get</span>();
    myBean.doSomeWork();
    <span style="color:#06f;font-style:italic">// when done</span>
    container.<span style="color:#3c4c72;font-weight:700">close</span>();
}
</pre>
<br/>

<p><b>Listing 08: CDI bootstrapping in Java SE</b></p>

<p>The introduction of asynchronous events is another important enhancement of the current CDI specification. CDI events now offer an additional method <i>fireAsync(…)</i> that returns a <i>CompletionStage&lt;T&gt;</i>. To be able to handle the event in an asynchronous way, the corresponding observer must be qualified as @ObservesAsync (listing 09).</p> 

<pre style="background:#fff;color:#000">@<span style="color:#00f;font-weight:700">ApplicationScoped</span> 
<span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">class</span> <span style="text-decoration:underline">AsyncEventFireBean</span> { 

        <span style="color:#00f;font-weight:700">@Inject</span>  
        <span style="color:#00f;font-weight:700">Event&lt;<span style="color:#00f;font-weight:700">SomePayload</span>></span> event;  

        <span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">void</span> <span style="color:#0000a2;font-weight:700">someAsyncBusinessTriggerMethod</span>() {   
            <span style="color:#06f;font-style:italic">// fire (and forget) asynchronous event </span>
            event<span style="color:#00f;font-weight:700">.</span>fireAsync(<span style="color:#00f;font-weight:700">new</span> <span style="color:#00f;font-weight:700">SomePayload</span>());  
            <span style="color:#06f;font-style:italic">// do some additional work in main thread</span>
            .<span style="color:#00f;font-weight:700">.</span>.
        } 
    }
</pre>
<br/>

<p><b>Listing 09: asynchronous CDI event “fire &amp; forget”</b></p>

<pre style="background:#fff;color:#000">@<span style="color:#00f;font-weight:700">ApplicationScoped</span> 
<span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">class</span> <span style="text-decoration:underline">MyAsyncObserverBean</span> { 

    <span style="color:#00f;font-weight:700">public</span> <span style="color:#00f;font-weight:700">void</span> <span style="color:#0000a2;font-weight:700">callMe</span>(@<span style="color:#00f;font-weight:700">ObservesAsync</span> <span style="color:#00f;font-weight:700">SomePayload</span> <span style="font-style:italic">payload</span>) {  
       <span style="color:#06f;font-style:italic">// BTW: this is another thread </span>
    } 
}
</pre>
<br/>

<p><b>Listing 10: asynchronous CDI observer</b></p>

<p>As mentioned As mentioned before not only the CDI API but also the BeanValidation API got some face-lift. As most of the Java EE related APIs also BeanValidation supports new Java 8 types (LocalTime and Optional) and features (lambdas, repeating qualifiers, type annotations). In addition, there is a wide range of new BeanValidation constraints available (e.g. @NotBlank, @NotEmpty, @Email, @Past, @Future, @Negative, @Positive, …).</p> 

<h2>Conclusion</h2> 
<p>Even if Java EE 8 is not the most innovative and revolutionary release of the Java Enterprise Edition history, it is still an important milestone on a suitable path into the future. Thanks to the consequent focus on the two main mottos “Modern Web Technologies” and “Ease of Development” and the corresponding clearing up and alignment of a lot of APIs, the current edition is one of the most stable JEE releases ever and therefore a perfect baseline for the future: <a target="_blank" href="https://jakarta.ee/">Jakarta EE</a>.</p> 

   
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/may/images/lars.png"
        alt="Lars Rowekamp" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Lars R&ouml;wekamp<br />
            <a target="_blank" href="www.openknowledge.de">open knowledge GmbH</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mobileLarson">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.openknowledge.de/blog">Blog</a></li>
            <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>