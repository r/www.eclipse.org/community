<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The Internet of Things (IoT) is all about connectivity and scalability. A lot of great technology exists on the cloud side of the IoT which enables fast prototyping, yet scales as the amount of data and number of devices increases. However, the things (embedded, connected sensor devices such as the <a target="_blank" href="http://xdk.io/">Bosch Cross Domain Development Kit - XDK)</a> in IoT remain elusive to a lot of developers.</p> 

<p>There are many things to learn and consider when building IoT solutions. For example, embedded IoT devices require a very different technology stack compared to the cloud. Bare-metal devices powered by ARM Cortex-M class microcontrollers are most commonly programmed in C which is far less convenient, productive and easy to learn when compared to the programming languages we use in the cloud. Finally, different programming style and often complex APIs present a barrier to developers wanting to build embedded IoT solutions.</p>

<p>Existing technologies that aim at making embedded devices easier to program, e.g. Arduino, work well for prototyping. However, when trying to scale these prototypes towards real-world deployment, let alone to production levels, such platforms fall short. The level of control over timing, memory, and behavior that such existing technologies provide, often do not fit the requirements of commercial IoT deployments.</p>

<p><a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-mita">Eclipse Mita</a> is a new project in the <a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a> family which aims to remove the entry barrier to embedded IoT development and to close the gap between cloud developer experience and the embedded IoT one. Mita is a new programming language designed to feel like modern programming languages in the vein of TypeScript, Kotlin, and Go.</p> 

<p><b>Eclipse Mita:</b></p>
    <ul>
        <li>Combines imperative programming with model-driven configuration.</li> 
        <li>Scales from prototypes to commercial IoT deployments by transpiling to C code.</li> 
	</ul>
	
<p>This way developers can inspect, learn, and build on Mita results, thus enabling trust, control and efficiency. Because it was built using Eclipse Xtext, Mita offers a great developer experience sporting auto-complete, syntax highlighting, and a lot of static validation.</p>

<p>With Mita we target resource-constraint devices, often powered by ARM Cortex-M class microcontrollers, limited in RAM, program size and energy available. To leverage the debugging tools, efficient compilers and existing platforms built for such devices, we transpile Mita to C code. Device vendors ship drivers and other libraries which we leverage in the generated C code. Mita is event-based, e.g. events triggered by sensors, connectivity, or time. This development style fits well to the IoT where devices spend most of their time in power-saving modes, wake up due to some event, process data, send it out, and go back to sleep.</p>

<h2>Model driven configuration</h2>

<p>At the core of Mita are platforms which describe the devices Mita generated C code can run on. Each platform (e.g. the XDK110 platform for the <a target="_blank" href="http://xdk.io/">Bosch XDK</a>) provides two key concerns:</p>
	<ol>
        <li>How code looks like: there are several implementation details which differ across platforms, e.g. how the system starts up, how timers are used, how events are handled or how the Makefile should look like.</li>
        <li>System resources: All devices sport different sets of connectivity, sensors, general-purpose IO and hardware interconnectivity (e.g. I2C or SPI). In addition, the SDKs and libraries that ship with the device provide additional resources, such as application level protocols. Platforms describe all those resources, their configuration items, runtime readable state (called modalities, thing sensor data) and communication channels with the outside world, called signals.
	</ol>
	
<h2>Example</h2>
<p>Let's have a look at an example and use the <a target="_blank" href="http://xdk.io/">Bosch XDK</a> to build a shock detector connected using MQTT over WLAN:</p>

<pre style="background:#fff;color:#000"><span style="color:#8c868f">// Mita has packages which are the main unit of isolation</span>
<span style="color:#ff7800">package</span> <span style="color:#ff7800">main</span>;
 
<span style="color:#8c868f">// Every Mita program must import a platform, here we use the XDK</span>
<span style="color:#ff7800">import</span> <span style="color:#ff7800">platforms.xdk110</span>;
 
<span style="color:#8c868f">// System resources are configured using the setup keyword.</span>
<span style="color:#8c868f">// Here, we configure the WLAN connectivity of the XDK.</span>
setup devnet <span style="color:#ff7800">:</span> <span style="color:#3b5bb5">WLAN</span> {
    ssid <span style="color:#ff7800">=</span> <span style="color:#409b1c">"MyWifiName"</span>;
    psk <span style="color:#ff7800">=</span> <span style="color:#409b1c">"supersecretkey"</span>;
}
 
<span style="color:#8c868f">// Software resources are resources nonetheless and thus are set up</span>
<span style="color:#8c868f">// using the setup keyword. Notice how we refer to the devnet WLAN setup</span>
<span style="color:#8c868f">// as means of transport, and instantiate a signal to the /events topic.</span>
setup backend <span style="color:#ff7800">:</span> <span style="color:#3b5bb5">MQTT</span> {
    transport <span style="color:#ff7800">=</span> devnet;
    url <span style="color:#ff7800">=</span> <span style="color:#409b1c">"mqtt://iot.eclipse.org:1883"</span>;
    clientId <span style="color:#ff7800">=</span> <span style="color:#409b1c">"shockDetector42"</span>;
   
    var events <span style="color:#ff7800">=</span> topic(name<span style="color:#ff7800">=</span><span style="color:#409b1c">"/events"</span>);
}
 
<span style="color:#8c868f">// Functions use the fn or function keyword. If the return type were omitted</span>
<span style="color:#8c868f">// the Mita compiler would infer it automatically. Also, notice the type</span>
<span style="color:#8c868f">// parameter of the array.</span>
fn mean(x <span style="color:#ff7800">:</span> array<span style="color:#ff7800">&lt;</span>uint32<span style="color:#ff7800">></span>) <span style="color:#ff7800">:</span> uint32 {
    .<span style="color:#ff7800">.</span>.
}
 
<span style="color:#8c868f">// Variables can be immutable (let) or mutable (var).</span>
<span style="color:#8c868f">// For arrayPosition we do not have to explicitely give a type as our</span>
<span style="color:#8c868f">// type inference infers it from the initialization.</span>
let acceleration <span style="color:#ff7800">=</span> <span style="color:#ff7800">new</span> array&lt;uint32>(size<span style="color:#ff7800">=</span><span style="color:#3b5bb5">10</span>);
var arrayPosition <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">0</span>;
 
<span style="color:#8c868f">// The every keyword handles events. Here we use time as an event source and</span>
<span style="color:#8c868f">// run at a regular interval.</span>
every <span style="color:#3b5bb5">10</span> milliseconds {
    <span style="color:#8c868f">// Sensor data (and other modalities) are available due to the platform import above.</span>
    <span style="color:#8c868f">// One can use some resources even if they were not configured beforehand.</span>
    acceleration[arrayPosition] <span style="color:#ff7800">=</span> accelerometer<span style="color:#ff7800">.</span>magnitude<span style="color:#ff7800">.</span>read();
    arrayPosition <span style="color:#ff7800">=</span> (arrayPosition <span style="color:#ff7800">+</span> <span style="color:#3b5bb5">1</span>) <span style="color:#ff7800">%</span> acceleration<span style="color:#ff7800">.</span>length();
 
    <span style="color:#8c868f">// The mean() function can be called using the familiar OO-style notation.</span>
    <span style="color:#8c868f">// The expression on the left side of the dot becomes the first argument</span>
    <span style="color:#8c868f">// of the function call.</span>
    <span style="color:#ff7800">if</span>(acceleration<span style="color:#ff7800">.</span>mean() <span style="color:#ff7800">></span> <span style="color:#3b5bb5">5000</span>) {
        <span style="color:#8c868f">// Writing to the signal instance we've created in the signal block</span>
        <span style="color:#8c868f">// above sends out the MQTT message (backend is an MQTT resource</span>
        <span style="color:#8c868f">// after all). Using backticks we can use string interpolation.</span>
        <span style="color:#8c868f">// Here we construct a JSON string inline to the function call.</span>
        backend<span style="color:#ff7800">.</span>events<span style="color:#ff7800">.</span>write(`{ <span style="color:#409b1c">"type"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"shock"</span>, <span style="color:#409b1c">"mag"</span><span style="color:#ff7800">:</span> ${acceleration<span style="color:#ff7800">.</span>mean()} }`);
    }
}
 
<span style="color:#8c868f">// Events are described and offered by the platforms.</span>
<span style="color:#8c868f">// This pressed event exists because the xdk110 platform imported</span>
<span style="color:#8c868f">// above declares it.</span>
every button_one<span style="color:#ff7800">.</span>pressed {
    <span style="color:#ff7800">for</span>(var i <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">0</span>; i <span style="color:#ff7800">&lt;</span> acceleration<span style="color:#ff7800">.</span>length(); i<span style="color:#ff7800">++</span>) {
        acceleration[i] <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">0</span>;
    }
    arrayPosition <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">0</span>;
}

</pre>

<h2>Conclusion</h2>
<p>Eclipse Mita is a new programming language for the embedded IoT. It aims to close the gap between cloud and embedded development, bringing both communities closer together. We support high-level features by combining a model-driven with an imperative language approach, transpiling to C. This affords control over our devices and scales from prototype to production.</p>
<p>The next items on the roadmap after the initial contribution are supporting more platforms, improving the type system and extending the event handling.</p>

<p>If you have any questions, or would like to help out building Mita, please get in touch on the <a target="_blank" href="https://accounts.eclipse.org/mailing-list/mita-dev">mailing list</a>.</p>
		  
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/march/images/christian.jpeg"
        alt="Christian Weichel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Christian Weichel<br />
            <a target="_blank" href="https://www.bosch-connectivity.com/">Bosch Connected Devices and Solutions</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/csweichel">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/christian-weichel-740b4224/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>