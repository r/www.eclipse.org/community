<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
 
<p>The Eclipse PHP Development Tools 5.0 or <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/june/article2.php">PDT 5.0</a> release was a big success, but we aren’t done yet and are still working on our product. The team released two minor versions and with Oxygen.3 will deliver the third. Each one contains new features and improvements as part of over 100 resolved issues on Bugzilla!</p>

<h2>5.1 – Synchronized Project</h2>
<p><a target="_blank" href="https://www.eclipse.org/downloads/packages/eclipse-php-developers/oxygen3rc3">Eclipse for PHP Developers</a> package currently contains <a target="_blank" href="https://projects.eclipse.org/projects/tools.tm">RSE</a> plugins. They allow work on remote FTP or SSH in a similar way as on local disk in Eclipse. Unfortunately, it introduces problems:</p>
    <ol>
        <li>Developers don’t have their work on a disk</li>
        <li>Project indexer cannot work without performance degradation</li>
        <li>IDE is a hostage of network performance</li>
        <li>And again performance…</li>
    </ol>

<p>This problem can be resolved with Synchronized Projects. This new module using popular <a target="_blank" href="https://www.eclipse.org/ptp/">Eclipse PTP</a> framework to extend standard local PHP project with synchronization capabilities. At the start, developers are able to download project files from a remote server using project creation wizards. Later, the plugin monitor file changes and allows for files to be sent directly to the server after being saved in the editor or on demand, it depends on the user’s preferences. Read more about PTP synchronized projects <a target="_blank" href="http://help.eclipse.org/oxygen/topic/org.eclipse.ptp.doc.user/html/sync.html?cp=80_6">here</a>.</p>

 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image1.png"></p>
 
<h2>5.2 – Composer autoload-dev and enhanced editor</h2>
<p>Most of 5.2 changes were introduced “under the hood”. We improved:</p> 
    <ul>
        <li>performance,</li> 
        <li>code assist precision</li> 
        <li>code navigation in the editor (for example “goto” label navigation).</li>
	</ul>
<p>PDT adopters receive new APIs to modify PHP validation results. For example, the popular <a target="_blank" href="https://github.com/pulse00/Doctrine-Eclipse-Plugin">Doctrine plugin</a> marks correct usage of imported Annotations:</p>

<div class="row">
  	<div class="col-md-12"><p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image4.png"></p></div>
    <div class="col-md-12"><p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image2.png"></p></div>
</div>
 
<div class="row">
    <div class="col-md-12"><p align="center"><b>Before</b></p></div>
  	<div class="col-md-12"><p align="center"><b>After with external plugin</b></p></div>
</div>

 <p>Another interesting change is support for composer.json <a target="_blank" href="https://getcomposer.org/doc/04-schema.md#autoload-dev">autoload-dev</a>. Users can modify this important section via the easy to use graphical editor in the same way they would for the standard autoload. Based on this, PDT is able to build correct <a target="_blank" href="http://help.eclipse.org/oxygen/topic/org.eclipse.php.help/html/concepts/build_paths.html">build path</a>.</p>
 
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image6.png"></p>
 
<h2>PDT 5.3 – Upcoming Oxygen.3 release</h2>
<p>Will be soon available and introduces many new features.</p>

<h3>PHP 7.2</h3>
<p>We now officially support the most recent stable PHP release! From IDE point of view we introduced support for two main syntax changes:</p>
	<ol>
        <li><a target="_blank" href="https://wiki.php.net/rfc/object-typehint">Object type hint</a></li>
        <li><a target="_blank" href="https://wiki.php.net/rfc/list_reference_assignment">List reference assignment</a></li>
 	</ol>
 
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image5.png"></p>
 
<h3>PHPUnit</h3>
<p>PHPUnit support was introduced in 5.0, but this release deliver a lot of great changes.</p>

<p>1. After tests launch, developers might be surprised by the changes in main PHPUnit view. It contains consistent and colored labels for all tests and its parents. The view has been enriched, similar to JDT JUnit view, by execution time.</p> 
 
  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/march/images/image3.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image3_sm.png"></a></p>
  
  <p>It’s also now possible to run concrete tests from any level of the tree or re-run all failed tests with a simple mouse click!</p>
 
  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image7.png"></p>
  
  <p>2. Another enhancement is the new Object Diff view. It introduces usable diff presentation for failed <i>assertEquals</i> calls.</p>  

  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image9.png"></p>
  
  <p>3. On top of that, we added support for PHPUnit 7 and introduce native PHPUnit output in the Eclipse workspace console view. </p>
  
  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image8.png"></p>
  
  <p>I’d like to give special thanks to <a target="_blank" href="https://github.com/mlocati">Michele Locati</a> who patiently reported and tested changes for this module.</p>

<h3>PHP Code Formatter improvements</h3>
<p>Another important feature in this release is support for custom PHPDoc-like tags. In previous versions, tags were interpreted as standard comments. Due to this, the formatter would sometimes break them. In the latest release, the formatter leaves the tags untouched! This means that Doctrine and PHPUnit annotations are safe. :)</p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/march/images/image10.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/image10_sm.png"></a></p>
  
<h2>And more…</h2>
<p>As mentioned before, these are only some of the improvements introduced in the past three releases. More details are available on our New & Noteworthy pages for <a target="_blank" href="https://wiki.eclipse.org/PDT/NewIn51">5.1</a>, <a target="_blank" href="https://wiki.eclipse.org/PDT/NewIn52">5.2</a>, <a target="_blank" href="https://wiki.eclipse.org/PDT/NewIn53">5.3</a> and corresponding issues list on <a target="_blank" href="https://projects.eclipse.org/projects/tools.pdt/">PMI</a>.</p>

<h2>How to get it</h2>
<p>Like most eclipse projects we support many ways to install. For example, developers who haven’t used Eclipse PHP yet can download all-in-one Eclipse for PHP Developers package from the <a target="_blank" href="https://www.eclipse.org/downloads/eclipse-packages/">Eclipse download</a> page, directly from <a target="_blank" href="https://eclipse.org/pdt/#download">PDT webpage</a>, or even using <a target="_blank" href="https://www.eclipse.org/downloads/eclipse-packages/?show_instructions=TRUE#page-download">Eclipse Installer</a>.</p>

<p>For those who prefer to extend existing Eclipse installation we strongly recommend <a target="_blank" href="https://marketplace.eclipse.org/content/php-development-tools">Eclipse Marketplace</a>, but of course, a standalone p2 repository is also <a target="_blank" href="http://download.eclipse.org/tools/pdt/updates/oxygen">available</a>.</p>

<h2>Contact us</h2>
<p>We are a community driven project and greatly appreciate any help you’d like to offer. Please report bug or feature request on <a target="_blank" href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=pdt">Bugzilla</a> or ask on our <a target="_blank" href="https://dev.eclipse.org/mailman/listinfo/pdt-dev">developer mailing list</a>. We will answer with pleasure!</p>

<p>Happy PHPing!</p>
  

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/march/images/davidp.jpg"
        alt="Dawid Pakuła" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Dawid Paku&#322;a<br />
            <a target="_blank" href="https://w3des.net/pl">w3des.net</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/zulusx">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://about.me/zulusx">Website</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>