<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>It's been "almost" a year since our last <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/may/article4.php">article</a> for the Eclipse Newsletter, and a lot has happened in the jdt.ls project since. In this article, we'll cover some of the highlights of the past releases and the impact the project had on the Java ecosystem.</p>

<h1 id="improvementsallovertheplace">Improvements all over the place!</h1>

<p>Over the past 10 months, jdt.ls received a fair share of bug fixes and some notable improvements, including, among other things:</p>

<ul>
<li>smaller distribution size,</li>

<li>faster start times,</li>

<li>improved diagnostics performance,</li>

<li>new Java 9 support,</li>

<li>improvements to the "organize imports" refactoring (with favorite imports, imports order, execute on save),</li>

<li>code actions galore (known as quick-fixes in Eclipse IDE land),</li>

<li>the ability to disable autobuilds, or trigger incremental or full builds on demand,</li>

<li>proper test classpath isolation (i.e. test classes don't leak into main code anymore),</li>

<li>improved symbol (a.k.a. Type) search,</li>

<li>new multi-root support,</li>

<li>partial rename refactoring support (still missing file rename)</li>

<li>support for 3rd party extensions (debugger, decompilers, new commands),</li>
</ul>

<p>For some of those fixes and improvements, jdt.ls leverages the latest milestone developments from the Eclipse Photon SDK.</p>

<h1 id="jdtlsintegratedeverywhere">jdt.ls integrated everywhere</h1>

<p>While <a target="_blank" href="https://marketplace.visualstudio.com/items?itemName=redhat.java">vscode-java</a> is the reference implementation for jdt.ls clients, others have jumped on the jdt.ls train, like the <a target="_blank" href="https://github.com/atom/ide-java">ide-java extension</a> for the Atom editor:</p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/march/images/atom_extension.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/atom_extension_sm.png" alt="Atom with ide-java extension"></a></p>

<p>or <a target="_blank" href="https://github.com/Valloric/ycmd">YCMD</a>, providing autocompletion in <a target="_blank" href="https://www.vim.org/">VIM</a>:</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/nG8n105B5ls" frameborder="0" allowfullscreen></iframe>
</div>


<h1 id="aflourishingecosystem">A flourishing ecosystem</h1>

<p>As we're seeing jdt.ls being used more and more across the LSP client landscape, we've inevitably received many requests to allow 3rd party extensions to integrate deeply with jdt.ls. </p>

<p>Because the server is based on the OSGi framework powering the Eclipse IDE, we could open the gates to allow external extensions, fairly easily. During the <code>initialize</code> request, clients must provide the <code>bundles</code> parameter, which lists the full path of all OSGi bundles needed to be started by jdt.ls :</p>

<pre style="background:#fff;color:#000"><span style="color:#ff7800">interface</span> <span style="color:#3b5bb5">InitializationOptions</span> {
        <span style="color:#8c868f">/**
         * a list of Java LS extensions
         */</span>
        bundles<span style="color:#ff7800">?</span><span style="color:#ff7800">:</span> string[];
        <span style="color:#8c868f">/**
         * a list of workspace folders
         */</span>
        workspaceFolders<span style="color:#ff7800">?</span><span style="color:#ff7800">:</span> <span style="color:#ff7800">WorkspaceFolder</span>[];
        <span style="color:#8c868f">/**
         * Java LS configuration settings
         */</span>
        settings<span style="color:#ff7800">?</span><span style="color:#ff7800">:</span> <span style="color:#ff7800">JavaConfigurationSettings</span>[];
}
</pre>

<p>In order to contribute commands, bundles need to provide a extension point <code>org.eclipse.jdt.ls.core.delegateCommandHandler</code> in their plugin.xml, with a list of command ids, like :</p>

<pre style="background:#fff;color:#000">&lt;plugin>
   &lt;extension point<span style="color:#ff7800">=</span><span style="color:#409b1c">"org.eclipse.jdt.ls.core.delegateCommandHandler"</span>>
      &lt;delegateCommandHandler class<span style="color:#ff7800">=</span><span style="color:#409b1c">"com.microsoft.java.debug.plugin.internal.JavaDebugDelegateCommandHandler"</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.startDebugSession"</span><span style="color:#ff7800">/</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.resolveClasspath"</span><span style="color:#ff7800">/</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.resolveMainClass"</span><span style="color:#ff7800">/</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.buildWorkspace"</span><span style="color:#ff7800">/</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.fetchUsageData"</span><span style="color:#ff7800">/</span>>
            &lt;command id<span style="color:#ff7800">=</span><span style="color:#409b1c">"vscode.java.updateDebugSettings"</span><span style="color:#ff7800">/</span>>
       &lt;<span style="color:#ff7800">/</span>delegateCommandHandler>
   &lt;<span style="color:#ff7800">/</span>extension>
&lt;<span style="color:#ff7800">/</span>plugin>
</pre>

<p>This mechanism allowed the Microsoft team to implement a complete <a target="_blank" href="https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-debug">Java debugger extension for VS Code</a>:</p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/march/images/vscode_debugger.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/vscode_debugger_sm.png" alt="VS Code Java debugger in action"></a></p>

<p>... and a <a target="_blank" href="https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-java-test">Java Test runner</a></p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/march/images/vscode_test.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/march/images/vscode_test_sm.png" alt="VS Code Java Test Runner"></a></p>

<p><a target="_blank" href="https://www.eclipse.org/che/">Eclipse Che</a>, currently in the process of replacing its own Java language server with jdt.ls, should reach feature parity with its old server by providing custom jdt.ls extensions, for instance dependency visualization:</p>


<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PQ5S5awPiVE" frameborder="0" allowfullscreen></iframe>
</div>
<br/>

<p><a target="_blank" href="https://github.com/dgileadi">David Gileadi</a> added support for <a target="_blank" href="https://github.com/dgileadi/dg.jdt.ls.decompiler">external decompilers implementations</a> by implementing the <code>org.eclipse.jdt.ls.core.contentProvider</code> <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls/blob/master/org.eclipse.jdt.ls.core/schema/org.eclipse.jdt.ls.core.contentProvider.exsd">extension point</a>. A <a target="_blank" href="https://marketplace.visualstudio.com/items?itemName=dgileadi.java-decompiler">VSCode extension</a> is available, but the bundles can be embedded in any client.</p>

<p>Finally, <a target="_blank" href="https://twitter.com/GorkemErcan">Gorkem Ercan</a>, fearless leader of the jdt.ls project, can satisfy his dogfooding needs by developing jdt.ls with jdt.ls in VS Code: his <a target="_blank" href="https://github.com/gorkem/jdt.ls.pde/releases">vscode-pde</a> extension allows the development of Eclipse Plugins by loading a target platform file, referenced in a <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls/blob/master/javaConfig.json">javaConfig.json</a> file.
The <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls/blob/master/org.eclipse.jdt.ls.core/schema/org.eclipse.jdt.ls.core.importers.exsd">org.eclipse.jdt.ls.core.importers</a> extension point allows 3rd party adopters to expand the types of projects imported by jdt.ls beyond simply Maven, Gradle and Eclipse projects. </p>

<h1 id="whatsnext">What's next?</h1>

<p>The jdt.ls team tries to keep a 2-weeks release cadence, making each release incrementally superior to the last. We're collaborating with the LSP4J and the Microsoft teams to keep improving the Language Server Protocol, so, in the near future, we'll hopefully be able to provide :</p>

<ul>
<li>full renaming support (including moving files),</li>
<li>more refactorings / code actions</li>
<li><code>textDocument/onTypeFormatting</code> support, to automatically format your code after typing <code>;</code> or when going to the next line,</li>
<li>provide a progress report API, allowing clients to track background activity</li>
<li>provide a notification API to broadcast classpath changes</li>
<li>Java 10 support</li>
<li>squash more bugs</li>
</ul>

<p>All contributions are welcome, so feel free to chime in the <a target="_blank" href="https://mattermost.eclipse.org/eclipse/channels/eclipsejdtls">eclipse.jdt.ls channel on Mattermost</a>, or open bug reports (and submit pull requests!) to <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls">https://github.com/eclipse/eclipse.jdt.ls</a></p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/may/images/fred.jpeg"
        alt="Fred Bricon" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Fred Bricon<br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fbricon">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/fbricon">GitHub</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>