<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>As 2018 begins I would like to share a few thoughts on where I think the Eclipse community is heading. I am looking forward to an incredibly busy year for myself, the staff of the Eclipse Foundation, and our community, because this is going to be a year of tremendous growth and opportunity. I will try to give a brief overview of what I see as some of the exciting things that are going on at the Eclipse Foundation. I am sure that I will miss some, so apologies in advance!</p>

<p>Of course, the big news of the past few months was the <a target="_blank" href="https://blogs.oracle.com/theaquarium/opening-up-ee-update">announcement by Oracle</a> that Java EE is going to be moving to the Eclipse Foundation. This represents the largest single contribution to the Eclipse community since -- well, the original Eclipse IDE project in 2001. It is approximately 35 new projects, hundreds of new committers and contributors, and millions of lines of code. It is and was an incredible endorsement of the Eclipse Foundation's mission as the leading organization for individuals and companies to collaborate on commercial-friendly open source software. Since the announcement in September, we have created the new Eclipse Enterprise for Java (EE4J) top-level project, and source code is starting to move into the projects. During 2018 our collective mission will be to create a functioning and successful community around this code, pick a new brand to replace Java EE going forward, ship a release compatible with Java EE 8, open source the Java EE TCKs, and establish a new specification process to shape the future of cloud-native Java. I feel out of breath just thinking of it.</p>

<p>But in addition to this Java EE work, it is clear that the Eclipse Foundation is now playing a pivotal role in the future of the Java ecosystem. Projects such as <a target="_blank" href="http://microprofile.io/">Eclipse MicroProfile</a> (microservices for Java), <a target="_blank" href="https://www.eclipse.org/openj9/">Eclipse OpenJ9</a> (Java virtual machine), <a target="_blank" href="https://projects.eclipse.org/projects/technology.deeplearning4j">Eclipse DeepLearning4J</a> (machine learning), <a target="_blank" href="https://www.eclipse.org/collections/">Eclipse Collections</a> (highly scalable collections), <a target="_blank" href="https://projects.eclipse.org/projects/technology.jnosql">Eclipse JNoSQL</a> (NoSQL for Java EE) and <a target="_blank" href="http://vertx.io/">Eclipse Vert.x</a> (reactive apps for Java) are leading the next generation of Java innovation.</p>

<p>From the Eclipse Science community comes <a target="_blank" href="https://projects.eclipse.org/projects/science.xacc">Eclipse XACC</a>, which I believe is the world's first community-led open source project in the new field of quantum computing. Originating from Oak Ridge National Laboratory, XACC is working to integrate quantum processors with the high-performance computing environments that are the backbone of modern scientific computing. It will be exciting to see XACC ship its first release in 2018, and to support its desire to create an open collaboration to shape the next generation of computing hardware and programming paradigms.</p>

<p>The <a target="_blank" href="http://iot.eclipse.org/">Eclipse IoT</a> community has been a significant growth area within the Eclipse community over the past couple of years. In 2017 Eclipse IoT grew to over 25 projects, and is attracting a substantial developer and corporate community. It is also a terrifically ambitious group, with a <a target="_blank" href="https://iot.eclipse.org/resources/white-papers/Eclipse%20IoT%20White%20Paper%20-%20The%20Three%20Software%20Stacks%20Required%20for%20IoT%20Architectures.pdf">vision of providing technology stacks</a> that span the smallest of constrained devices, through device gateways, to cloud-scale data collection and management runtimes. Late in 2017 the group published a <a target="_blank" href="https://iot.eclipse.org/resources/white-papers/Eclipse%20IoT%20White%20Paper%20-%20Open%20Source%20Software%20for%20Industry%204.0.pdf">white paper</a> on the role that open source will play in Industry 4.0, or industrial IoT. This white paper is important because in many ways it sets out the vision for the group, which has been primarily focused on industrial IoT. In 2018 the primary goal for Eclipse IoT is to start shipping these stacks rather than simply projects. In other words, to create cross-project collaborations that provide IoT adopters with more complete solutions rather than individual building blocks. This will go a long way to paving the way for broad industry adoption of these open source IoT technologies.</p>

<p>Finally, a word on developer tools -- the Eclipse Foundation's original franchise. I noticed recently that according to at least <a target="_blank" href="http://pypl.github.io/IDE.html">one source</a>, the Eclipse IDE is maintaining its position as the #1 IDE in the world, and grew its market share substantially last year. The goal for this year is to continue this trend. In addition, the <a target="_blank" href="https://www.eclipse.org/che/">Eclipse Che</a> cloud IDE continues to grow its community and adoption. As more and more developers work on cloud-native applications, the appeal of a cloud IDE that works where they do is going to grow. Che is well positioned to be the leader in this space and is the only community-led cloud IDE.</p>

 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/topide2018.png"/></p>
<p align="center"><small>Source: http://pypl.github.io/IDE.html</small></p>

<p>Anyone who has seen me speak over the past couple of years has likely heard me express the idea that the "community is the capacity." The Eclipse Foundation is a 30 person organization that supports a community of hundreds of projects, hundreds of members, thousands of committers and contributors, and millions of users. Whenever I take a moment to reflect on what we accomplish together it is breathtaking. The breadth of the technology that we collectively produce is vast, and our community spans the globe. Equally exciting, engagement continues to grow with a variety of industries notably automotive, power, transportation, etc. interested in leveraging the Eclipse Foundation as the place for open, commercial collaboration.</p>
  
<p>I am incredibly optimistic that 2018 is going to be one of the most exciting years we've ever had, so please get involved!</p>
   
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/mike.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Mike Milinkovich<br />
            <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://mmilinkov.wordpress.com/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>