<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2018.01.25</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxannenew.png" />
                            <h3>Editor's Note</h3>
                            <p>Happy New Year!</p>
                            <p>We're planning ahead for this year and already announced the <a style="color:#000;" target="_blank" href="https://medium.com/@roxanne.eclipse/eclipse-newsletter-editorial-calendar-4dc5d1d64f81">upcoming Eclipse Newsletter themes</a> for every month in 2018 -- every month except March. If there's a topic you'd like to read about or or if you have feedback about the newsletter, <a style="color:#000;" target="_blank" href="https://www.surveymonkey.de/r/marchnewsletter3">tell us here</a>!</p>
                            <p>This month, the newsletter gives you a little taste of what you can expect in 2018. Keep reading to learn more about Java EE's move to the Eclipse Foundation, IoT trends, DeepLearning4J (Go-Bot included), Language Server Plugins: aCute and Eclipse Corrosion, and Eclipse Che.</p> 
                            <p>Into IoT? The Eclipse IoT working group, along with the IEEE IoT Initiative, the Open Mobile Alliance, and the AGILE-IoT, has launched the fourth annual IoT Developer Survey. Please take 5-8 minutes to <a style="color:#000;" target="_blank" href="https://www.surveymonkey.de/r/eclipseorg">complete the survey</a>.</p>
                            
                            <p>Finally, plan to join us in Toulouse this summer for <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/france2018/">EclipseCon France 2018</a>. We are accepting talk proposals until March 19. <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/france2018/cfp">Propose a talk</a>.</p>                            
                            <p>Thank you for reading!</p>
                            
                            <p>Roxanne Joncas <br>
                            <a style="color:#000;" target="_blank" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article1.php"><h3>Eclipse Community Directions for 2018</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>As 2018 begins, Mike Milinkovich, Eclipse Foundation Executive Director, would like to share a few thoughts on where he thinks the Eclipse community is heading.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article2.php"><h3>IoT Trends for 2018</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p><br>Ian Skerrett wrote up IoT trends he thinks we will see in the IoT industry and Eclipse IoT community this year.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article3.php"><h3>Eclipse Che 2018: Community, Extensibility and Kubernetes</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>What has the Eclipse Che community has achieved in 2017? What exciting updates can you expect in 2018? Find out!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article4.php"><h3>Building a Go playing bot with Eclipse Deeplearning4J</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Learn all about the ancient chinese game Go (and play it) in this article! Then learn how to build a bot to play against using Deeplearning4J.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2018/january/article5.php"><h3>The Rise of Language Server Plugins</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>The Language Server Protocol (LSP) was big last year. It's now time for the rise of LSP plugins!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

    
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Java IDE Tips &amp; Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">

                            <p>Here is this month's tip in action &#10145;<br>
                            <p style="color:grey">Some devs will never give up the println statements as a way to debug. Use the 'trace' autocomplete to automatically print out a tracing statement!</p> 
                            <p><i><small>Click on the gif to view the original tweet and gif.</small></i></p>
                            
                            <p>Want more tips? Follow <a target="_blank" style="color:#000;" href="https://twitter.com/eclipsejavaide">@EclipseJavaIDE</a> on Twitter.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://twitter.com/EclipseJavaIDE/status/956074785305440256"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/javaidejan.gif"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
       <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
        <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a target="_blank" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/vmeetupschedulewinter2018.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/iotdevsurvey2018.php">Launching IoT Developer Survey 2018</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2018/">Eclipse IoT Day Santa Clara | Call for Papers</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2018/cfp">EclipseCon France | Call for Papers</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="https://www.infoworld.com/article/3250629/java/eclipses-enterprise-java-roadmap-more-services-coming.html">Eclipse's Enterprise Java roadmap: more services coming</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/java-ee-migration-ee4j-check-140777.html">The great code migration from Oracle to EE4J: Grizzly is shaping up nicely</a></li>
								<li><a target="_blank" style="color:#000;" href="https://dzone.com/articles/eclipse-jnosql-a-solution-to-java-in-nosql-databas">Eclipse JNoSQL 0.0.4, a Solution to Java in NoSQL Databases</a></li>
								<li><a target="_blank" style="color:#000;" href="https://medium.com/@donraab/eclipse-collections-9-1-released-24c413d200ec">Eclipse Collections 9.1 Released</a></li>
								<li><a target="_blank" style="color:#000;" href="https://adtmag.com/articles/2017/12/12/buildship-release.aspx">Buildship 2.2 Release Supports Java 9, Runtime Classpath Separation and More</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2017/12/first-nine-projects-for-ee4j">The First Nine Projects Proposed for EE4J</a></li>
							</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://www.polarsys.org/proposals/polarsys-libimslibrary-modular-integrated-simulation">PolarSys LibIMS:</a> a library for Modular Integrated Simulation.</li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-ozark">Eclipse Ozark</a>: provides an implementation for Model View Controller API (MVC 1.0).</li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-rasterframes">Eclipse RasterFrames</a>: a Scala library (with planned Python bindings) built on top of Apache Spark SQL and GeoTrellis.</li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust">Eclipse Corrosion: the Eclipse IDE for Rust</a>: provides development tools for Rust and Cargo based Rust applications in the Eclipse IDE</li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.app4mc/releases/0.8.3/bugs">Eclipse APP4MC 0.8.3</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/CDT/User/NewIn94">Eclipse C/C++ Development Tooling (CDT) 9.4</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://medium.com/@donraab/eclipse-collections-9-1-released-24c413d200ec">Eclipse Collections 9.1</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.dirigible/releases/3.1">Eclipse Dirigible 3.1</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.edapt/releases/1.3.0">Eclipse Edapt 1.3</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egit/releases/4.10.0">Eclipse EGit 4.10</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jgit/releases/4.10.0">Eclipse JGit 4.10</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jnosql/releases/0.0.4">Eclipse JNoSQL 0.04</a></li>                  	
                             	<li><a target="_blank" style="color:#000;" href="http://microprofile.io/blog/2018/01/eclipse-microprofile-1.3-available">Eclipse MicroProfile 1.3</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.openj9/releases/0.8">Eclipse OpenJ9 0.8</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.pdt/releases/5.2">Eclipse PHP Development Tools 5.2</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.linuxtools/releases/6.2.0">Eclipse Linux Tools 6.2</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/rt.rap/releases/3.4.0">Eclipse Remote Application Platform 3.4</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.titan/releases/6.3.0">Eclipse Titan 6.3</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/science.triquetrum/releases/0.2.1">Eclipse Triquetrum 0.2.1</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.unide/releases/0.1.0">Eclipse Unide 0.1</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://www.polarsys.org/projects/polarsys.capella/releases/1.2.0">Capella 1.2</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://www.polarsys.org/projects/polarsys.kitalpha/releases/1.2.0">Kitalpha 1.2</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/locationtech.spatial4j/releases/0.7">LocationTech Spatial4j 0.7</a></li>
                             </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/january/images/jan_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.fosdem.org/2018">FOSDEM 2018</a><br>
                            Feb 3-4, 2018 | Brussels, Belgium</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipse.org/che/checonf18/index.html">CheConf 2018</a><br>
                            Feb 21, 2018 | Virtual Conference</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.devoxx.fr/">DEVOXX France 2018</a><br>
                            Apr 18-20, 2018 | Paris, France</p>
                            <p><a target="_blank" style="color:#000;" href="http://2018.foss4g-na.org/">FOSS4G NA</a><br>
                            May 14-17, 2018 | St. Louis, MO, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2018/">Eclipse IoT Day Santa Clara</a><br>
                            May 14, 2018 | Santa Clara, CA, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://tmt.knect365.com/iot-world/">IoT World 2018</a><br>
                            May 14-17, 2018 | Santa Clara, CA, USA</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2018/">EclipseCon France 2018</a><br>
                            Jun 13-14, 2018 | Toulouse, France</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
