<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>When beginning to learn a new language, wouldn't it be better if you weren't also forced to learn a new IDE? Language-specific IDEs moves one's focus away from learning the nuances of a language and towards learning the basics of a new environment. Being able to work in a familiar environment and have familiar paths to basic functions drastically simplifies the learning process. Luckily with the rise in the popularity of Language Servers and the developments made within the Eclipse IDE,  creating plugins which add language support has become quick and easy allowing users to stay within their favorite IDE to work with new languages.</p>

<p>The IDE development community has come a long way since <a target="_blank" href="https://developers.redhat.com/blog/2016/06/27/a-common-interface-for-building-developer-tools/">the June of 2016 announcement</a> of working together to develop the Language Server Protocol (LSP). Over 50 Language Servers are now being maintained and almost all major IDEs and code editors support their integration.</p>

<p>However, the LSP is not yet a complete experience. It continues to grow, but users are still required to exit their editor to command line interfaces to do what is either too domain specific for the protocol to cover or too reliant on user input to be currently supported. This is a hurdle that Eclipse is great at jumping and is demonstrated within Eclipse's <a target="_blank" href="https://marketplace.eclipse.org/content/acute-c-edition-eclipse-ide-experimental">aCute</a> and <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust">Eclipse Corrosion</a> which offer full IDE experiences for C# and Rust respectively.

<p>2018 will be a large year for LSP based plugins as it holds the release of aCute into Photon Simultaneous Release, the initial release of Corrosion to the Eclipse Marketplace, and most likely the beginning of other language support plugins for both new and older languages.</p>


<h2>What are Language Servers</h2>

<p>Language servers, in short, are an abstraction of language-specific support out of the IDE into a separate process that can be used by any editor. The Language Servers communicate with the editors using <a target="_blank" href="https://microsoft.github.io/language-server-protocol/howItWorks">The Language Server Protocol (LSP)</a>, a standardization of communication ensuring that all servers can be used by all editors without much extra effort required. This means that once a Language Server is built, users can use any LSP-supporting editor to develop in that language. Requests are sent from the editor to the server for various tasks including autocomplete, variable references, and document formatting and the responses from the server are applied or displayed in the editor for the user to see. To read more about Language Servers, there was a <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/may/">full Eclipse Newsletter on them back in May 2017</a>.</p>

<p align="center">
	<a target="_blank" href="https://microsoft.github.io/language-server-protocol/howItWorks">
		<img class="img-responsive" src="images/language-server-sequence.png" alt="Language Server sequence diagram">
	</a>
</p>

<p>The Language Server Protocol drastically cuts down the cost to support a language in all editors and allows language developers to access a wider user base. For new languages, it is a clearly more advantageous to build and maintain a Language Server instead of a new IDE or attempting to maintain support in multiple IDEs individually.</p>

<h3>How Language Servers Change the IDE Landscape</h3>

<p>If you are about to choose an IDE to develop Java projects and in your research, you see that the leading IDEs all have the same features and reviews, but one has the capability of supporting 50+ other languages with the same level of completeness, which would you choose? The LSP has changed up the playing field for IDE market share for just this reason. IDEs that do not support the LSP will have a difficult time keeping up with supporting new languages, and for those who do support the LSP, language support is no longer a competitive advantage. For these IDEs, the main way to stay ahead will be with how the UI displays the information received from the Language Server and what beyond the protocol they are capable of doing.</p>

<p>Eclipse has the power to take the lead here. Built to develop plugins easily, Eclipse is able to fill in the gaps left by the protocol in a simple and user-friendly way.</p>

<h3>The Eclipse IDE Advantage</h3>

<p>For Eclipse IDE users, the way to create new, export, build, run, and perform tests on projects is made simple through wizards, UI commands, and shortcuts. When working with a new language it is expected by the user, no matter the IDE, that the location of these basic functions are found where they are expected. Currently, these are all features that are not supported by the LSP meaning that implementing a Language Server only results in an editor and not yet a full IDE experience. It requires extra work to develop a plugin that offers full development support for a language and out of all the LSP-supporting IDEs out there, Eclipse makes this the easiest.</p>

<p>With Eclipse's design, developing plugins to add features with a native feel is simplistic and fast. Creating wizards and commands to implement the above features are processes that are well documented with many easy to find tutorials and examples online. <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust">Corrosion</a> is a great example of how to integrate a command-line interface, in this case, the <code>cargo</code> command which offers build, run, and test support for Rust projects, into the Eclipse IDE in a native way.</p>

<p align="center">
<img class="img-responsive" src="images/wizards.png" alt="further language features integrated with wizards">
</p>

<p>Other LSP-supporting IDEs, lack in this category as they do not have the infrastructure to make adding these features a valid option for Language Server developers looking to quickly put out plugins across different IDEs. But with Eclipse, the value of these features outweigh the minimal time required for development.</p>

<h3>What we can expect this year</h3>
<a target="_blank" href="https://pypl.github.io/IDE.html?country="><img class="img-responsive" src="images/growth.png" alt="Eclipse IDE growth over the past year" align="right"></a>

<p>From the user's perspective, the ability to stay within one program to create, work on, run, and export a project may place Eclipse above other IDEs when deciding which to primarily use. With new language adoption happening at a faster rate, and the need for full IDE support for all these languages being just as apparent with larger languages, I would expect to see the Eclipse IDE become a leading host for new language IDEs in 2018 and continue the growth in popularity it has seen this past year.</p>


<h2>Language Server Plugins in 2018</h2>

<h3>aCute - C# and .NET Core</h3>
<p>Back in August of this year, aCute; the C# and .NET Core plugin for Eclipse IDE, joined the Eclipse Tools Project ecosystem and since then has enjoyed a steadily increasing ranking and multiple improvements brought forward by users. Learn more about aCute by reading <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/august/article3.php">the Eclipse Newsletter article about aCute</a>.</p>

<p>Within 2018, aCute plans to be a part of the Photon Simultaneous Release. This will bring with it multiple improvements including minimizing the dependencies required by the Language Server, making set up easier and more accessible, along with adding new features including editor support for .csproj files and C# debug support. This will mark a large step for Eclipse LS-plugins as it will be the first to use the new Debug Protocol, which is not currently part of the LSP but works in the same way, abstracting the debug logic out of the IDE and into the <a target="_blank" href="https://github.com/OmniSharp/omnisharp-node-client">Omnisharp Language Server</a>, which is used for aCute.</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/DwN6e_VcdD8" frameborder="0" allowfullscreen></iframe>
</div>

<h3>Eclipse Corrosion - Rust and Cargo</h3>

<p>Rust is the 5th most discussed repository on GitHub and is growing in popularity quickly. That being said, with RustDT no longer being developed, there is currently no Eclipse plugin being supported for Rust. With the help of the Rust Language Server and Eclipse's plugin-centric design, full IDE support has been brought to the Eclipse marketplace once again. <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust">Corrosion</a>, which is short for reduction-oxidation; the reaction that causes rust, supports creating, editing, running and packaging Rust projects.</p>

<p>Corrosion shows the simplicity of bringing full IDE support to a language in Eclipse as it went from repository creation to the Eclipse Marketplace in under 2 weeks; much faster than could have been done in other IDEs. In its current state, Corrosion hosts all the features expected from a LS-plugin including error reporting, hover, content assists, formatting and more all supplied from the <a target="_blank" href="https://github.com/rust-lang-nursery/rls">Rust Language Server (RLS)</a>, and has integration with various operations of the <code>cargo</code> command-line (New, Build, Run, Package) as typical Eclipse IDE wizards and workflows.</p>

<p>2018 will be a big year for Corrosion. With it now being on the Eclipse Marketplace, more exposure and more users will continue to improve its quality with feedback and feature requests and hopefully more contributors joining Team Corrosion and assist in making Eclipse the go-to location for Rust Development. It is also planned for Corrosion to join aCute as an Eclipse Tools Project within the near future.</p>

<a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust"><img class="img-responsive" src="images/redOx-features.gif" alt="A display of Corrosion's features" align="center"></a>

<h3>The Next Big Thing</h3>
<p>aCute and Corrosion are just two of the Language Server based plugins in the Eclipse Marketplace and within 2018 I expect there to be multiple more to join, both for existing languages and new ones to come. If you are part of a team developing a new language this year, then I would highly suggest building a Language Server. If you are currently working on a Language Server and want to give your users a full IDE experience, I hope I have inspired you to take a look at developing an Eclipse plugin. Plus if you are a C# or Rust developer then download <a target="_blank" href="https://marketplace.eclipse.org/content/acute-c-edition-eclipse-ide-experimental">aCute</a> and <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-corrosion-eclipse-ide-rust">Corrosion</a> and see how they compare to your current workflow and feel free to submit issues, pull requests, or comments.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/august/images/lucas.jpg"
        alt="Lucas Bullen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Lucas Bullen<br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/lucasbullen/?ppe=1">LinkedIn</a></li>
           <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.twitter.com/thelucasbullen">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>