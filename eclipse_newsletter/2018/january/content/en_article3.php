<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <p>Happy New Year!</p> 
<p>At this time of year, we look back at what the <a target="_blank" href="https://www.eclipse.org/che/">Eclipse Che</a> community has achieved in 2017 and look forward to a host of exciting updates in 2018.</p>
<p>2017 was another big year for Che - where 2016 brought a huge increase in awareness of the Che project, 2017 was focused much more on usage. In fact, through 2017 we saw publicly accessible Che workspaces used for >7,000,000 hours! We know there are also large installs that are behind firewalls and make them invisible to our anonymous usage sampling. What’s perhaps even more exciting is that we average over 100,000 active sessions on hosted Che workspaces each month. That’s not too bad for a young project and it’s very exciting to see this kind of real user activity.</p>

<p>The Che team also did a fair bit of traveling in 2017 - trying to get out a meet new and existing users around the world. Apart from the independently organized meetups (virtual and physical), the Che team was also at a lot of big shows like EclipseCon, KubeCon, JavaOne, Devoxx, and others. Personally, these shows are one of the best parts of my job as project lead - I love hearing what people have to say. At KubeCon, for example, we were showing an early beta of Che working on native Kubernetes (Red Hat has been working on enabling Che to run in OpenShift so much of the work falls out from that effort). Although there were a lot of rough edges, attendees were still blown away - one woman commented that “THIS is how Kubernetes development should be!” A sentiment echoed by most of the folks who stopped by the booth. We heard a similar comment from a development manager at a major multi-national bank who said that they Che was “so much easier than having all my developers try and follow our antiquated wiki.”</p>
<p>But enough looking back - let’s talk about what’s coming!</p>
   
<h2>Che 6: “Enterprise Eclipse Che” - January</h2>
<p>The focus of the upcoming Che 6 release is enterprise capabilities. Eclipse Che is a workspace server, but until now it has been focusing on individuals and ISVs. Enterprises who wanted to leverage the power of portable and shareable developers workspace for their teams were required to setup complex Che farms or buy Codenvy proprietary licenses.</p>
<p>The big news for the community is that all the enterprise capabilities from Codenvy are being open sourced onto Eclipse Che directly! In some cases, we’ve even reworked the components to make them more reliable and scalable.</p>
<p>This makes Eclipse Che the best solution for dev teams and enterprises who are ready to develop in a more efficient, secure and continuous way.</p>
<p>Let’s look at some specific examples...</p>
   
   <h3>User Management with Keycloak</h3>
<p>Eclipse Che relies on <a target="_blank" href="http://www.keycloak.org/">Keycloak</a> to manage users. Authentication for users can support either OAuth or SAML and you can easily integrate Eclipse Che with your LDAP or Active Directory server.</p>
   
    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/chelogin.png"/></p>
<p align="center"><small>Login into multi-user Eclipse Che</small></p>

<h3>Multi-User and Multi-Tenancy</h3>
<p>Eclipse Che 6 allows multiple users to log into the Che server, allowing a single Che server to orchestrate hundreds of workspaces for developers.</p>
<p>Further, Che includes the concept of an “organization” which allows an installation to be subdivided and users associated with one of several organizations - useful for large companies that need more granular control of their groups.</p>

<h3>Team workspaces & organization</h3>
<p>Eclipse Che provides a built-in permissions system which allows fined grained control to define “who” is allowed to do “what”. Any user or administrator can control the resources managed in Che and allow certain actions. For example, as the owner of a workspace, you can grant access to others to see and/or use your workspace. Administrators also have the ability to regroup developers by teams or organizations and allocate resources. To make the collaboration easier between developers, email notifications can be enabled.</p>

<h3>OpenShift, Kubernetes and Docker Infrastructure Support</h3>
<p>Che 6 adds support for Openshift in addition to Docker. OpenShift brings increased security, TLS support, embedded reverse proxy and distributed volumes. Additionally, native Kubernetes (which has been frequently requested by the community) is going to be added in the first quarter of this year.</p>

    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/technologies.png"/></p>

<h3>Service Provider Infrastructure</h3>
<p>A new abstraction layer has been introduced into Che’s architecture to make it easier for community members to add support for other workspace orchestration engines (or even process forking if that’s your thing). This work is one of the most important underlying improvements in Che 6 and is why we’re able to bring Che to OpenShift, Docker and soon on Kubernetes.</p> 

<h3>New UI</h3>
<p>We heard a lot of developers, who just don't want to touch the mouse when they are coding. In the new IDE UI our target is simple: remove any sort of distraction and be much more minimalistic. Keep only in the UI the primary actions, focus the user to the code, provide him the largest editor area as possible and let him interact with any IDE actions by using only his keyboard leveraging shortcuts.</p>

    <p align="center"><a href="/community/eclipse_newsletter/2018/january/images/cheui.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/cheui_sm.png"/></a></p>
<p align="center"><small>The Che UI can show project explorer, terminal, commands, PR table, editor and other helpers…</small></p>


    <p align="center"><a href="/community/eclipse_newsletter/2018/january/images/codeonly.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/codeonly_sm.png"/></a></p>
<p align="center"><small>...or can show only the code for a focused coding session.</small></p>

<h3>Other Improvements</h3>
<p>Eclipse Che 6 is a major release with almost 450 community pull requests, so you can expect a lot of improvements and new features. For example:</p>
	<ul>
        <li>The debugger is richer and now allows you to browse all the threads of your code as well as setup conditions and suspend policies on your breakpoints.</li>
        <li>Git status is shown through color highlighting in the project explorer (file level) and editor (line level).</li>
        <li>Workspace startup time (shorter) and resource consumption (less) are both improved.</li>
	</ul>
	
<h2>Roadmap for 2018: Extensibility and Ecosystem Development</h2>

<p>While working on Che 6 a part of the team has started working on extensibility improvements that will be a big focus for us in 2018. Our goal is to significantly improve the mechanisms for extending Che in order to more rapidly develop the community and ecosystem around Che.</p>

<p>From its birth, Eclipse Che was built as an extensible platform that could be completely white labeled. The current approach has worked well for large companies like SAP and Software AG who use it to provide a set of developer tools based on Che for their customers.</p>

<p>Unfortunately, some of the technology and approaches we’ve used have been too complex and monolithic for more casual contributors who also wanted to extend Che.</p>

<p>Breaking up the Che monolith is going to be a major target in 2018. Eclipse Che’s contributors will be able to create their Che plugin directly from Che. They will be able to publish their plugins on a marketplace, and Che’s users will be able to install and customize Che with the plugins they want.</p>
 
<p>In order to achieve that, we are going to introduce a few new extensibility mechanisms:</p>

<p>A <b>new JS extension points</b> will allow anyone to extend the IDE using the language of their choice (typescript, javascript, etc…). While the base of the IDE will remain in GWT, you’ll not need to touch this part - so your plugin can be written entirely in your preferred language. This will make plugins faster to develop, build and run.</p>

<p><b>Server-side extensibility</b> will be added so that making server changes will not require rebuilding the whole Che assembly, but only your plugin.</p>

<p>A <b>packaging solution for the plugins</b> will allow you to package and post your plugin for community consumption. For example, you may write a server-side plugin in a language other than Java and that may require your plugin to provide the dependencies it needs to run. In this situation, your plugin can bring its own container which will have everything necessary to provide the capabilities you need and Che will run that container as a sidecar of the other workspace containers.</p>
 
<p>All these capabilities will work with a plugin marketplace and the nascent <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-lsphub">Eclipse LSP marketplace</a>.</p>

<p>There are many other things we expect to get to this year, a little sampling of those is:</p>
	<ul>
        <li>Unification of Factories and CheFiles into one standard that can achieve both goals</li>
        <li>Reorganization of the Che “agents” to allow many of them to run as container sidecars which keeps your application’s container unchanged between your developer environment to the production</li>
        <li>Editor updates to make it faster, cleaner and more powerful</li>
        <li>Istio support to help Kubernetes developers get comfortable with this powerful technology</li>
        <li>Serverless development - this is an area we want to embrace in Che but are early on in our thinking...it’s possible this will land closer to the end of 2018</li>
	</ul>
<p>As you can, although 2017 was a busy and exciting year we fully expect 2018 to be even bigger.</p>
<p>We are also running our second CheConf on February 21st - it’s a virtual conference that you can attend from anywhere to learn about the latest updates on Che and how real organizations are using Che to increase the efficiency of their development teams and customers. You can register today at: <a target="_blank" href="https://www.eclipse.org/che/checonf18/">https://www.eclipse.org/che/checonf18/</a></p>

   
<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/stevan.jpeg"
        alt="Stevan Le Meur" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Stevan Le Meur<br />
            <a target="_blank" href="https://www.redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/stevanlm">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.codenvy.com/@slemeur?gi=4cc452ac08a4">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/august/images/brad.png"
        alt="Brad Micklea" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Brad Micklea <br />
        <a target="_blank" href="https://www.redhat.com">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://che.eclipse.org/author/brad/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/bradmicklea">Twitter</a></li>
      </ul>
        </div>
      </div>
     </div>  
   </div>
</div>