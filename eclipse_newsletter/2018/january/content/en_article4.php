<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>


  <p> As long as there have been computers, programmers have been interested in
  <em>artificial intelligence</em> (or "AI"): implementing human-like behavior on
  a computer.  Games have long been a popular subject for AI researchers.
  During the personal computer era, AIs have overtaken humans at checkers,
  backgammon, chess, and almost all classic board games. But the ancient strategy
  game Go remained stubbornly out of reach for computers for decades.  Then in
  2016, Google DeepMind's AlphaGo AI challenged 14-time world champion Lee Sedol
  and won four out of five games. The next revision of AlphaGo was completely out
  of reach for human players: it won 60 straight games, taking down just about
  every notable Go player in the process. </p>

  <p>AlphaGo's breakthrough was enhancing classical AI algorithms with machine
  learning. More specifically, it used modern techniques known as <em>deep learning</em>
  -- algorithms that can organize raw data into useful layers of abstraction.
  These techniques are not limited to games at all. You will also find deep
  learning in applications for identifying images, understanding speech,
  translating natural languages, and guiding robots. </p>

  <p><a target="_blank" href="https://deeplearning4j.org">Eclipse Deeplearning4J (DL4J)</a> is a powerful, general-purpose deep learning framework for the JVM with which you can build many
  interesting applications. In this article you will learn how to design a deep learning
  model for the game of Go that can predict the next move in any given board situation.
  This model is powered by records of Go games played by professional players. In the end,
  we will embed this model into a full-blown application that you can play against yourself
  in your browser!</p>

  <h2>Overview</h2>

  <p>At the end of this article you will know quite a bit about the following topics:</p>

  <ul>
		<li>Understanding the fundamentals of how you can use machine learning, and deep learning
      in particular, for an interesting problem domain like Go.</li>
    <li>Playing the game of Go at a beginner level.</li>
    <li>Getting a glimpse at what neural networks are and how deep networks can be used for
      predicting Go moves.</li>
    <li>Understanding what it takes to build and deploy a Go bot.</li>
    <li>Building a deep learning model for Go move prediction with Eclipse DL4J.</li>
    <li>Running a deep learning bot with docker that you can play against.</li>
  </ul>

  <p>Many of these topics we can barely scratch the surface of in this article. There's a
  whole lot more to explore and learn in deep learning and AI. However, many of the techniques that
  run the strongest Go AIs out there carry over
  to other applications -- and truly understanding them can be your gateway into more advanced
  AI topics.</p>

  <p>If we've caught your attention and you want to learn more about this fascinating topic,
  go ahead and check out our new book <a target="_blank" href="https://www.manning.com/books/deep-learning-and-the-game-of-go"><em>Deep Learning and the Game of Go</em></a> (Manning).</p>

  <p><em>Readers of this newsletter get a 40% discount off of all formats when using the following code: "smpumperla40".</em> Code for the book and other useful material is freely available
  in the following <a target="_blank" href="https://github.com/maxpumperla/deep_learning_and_the_game_of_go">GitHub repository</a>. The book itself is written with Python and does not use Eclipse DL4J, but in this article we tackle some aspects covered in detail in the book with Java and DL4J.  </p>

  <a target="_blank" href="https://github.com/maxpumperla/deep_learning_and_the_game_of_go">

      <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/dl_go_cover.jpg" alt="Deep Learning and the Game of Go Book"></p>
      <p align="center"><small>Deep Learning and the Game of Go (Manning)</small></p>
  </a>

  <h2>Machine learning and deep learning</h2>

  <p>Consider the task of identifying a photo of a friend. This is effortless for
  most people, even if the photo is badly lit, your friend got a haircut or is
  wearing a new shirt. But suppose you wanted to program a computer to do the
  same thing. Where would you even begin? This is the kind of problem that
  machine learning can solve.</p>

  <h3>Traditional programming versus machine learning</h3>

  <p>Traditionally, computer programming is about applying clear rules to structured
  data. A human developer programs a computer to execute a set of instructions on
  data and outputs the desired result. Think of a tax form: every box has a
  well-defined meaning, and there are detailed rules about how to make various
  calculations from them. Depending on where you live, these rules may be
  extremely complicated. It's easy for people to make a mistake here, but this is
  exactly the kind of task that computer programs excel at.</p>

  <p>In contrast to the traditional programming paradigm, <em>machine learning</em> is a
  family of techniques for inferring a program or algorithm from example data,
  rather than implementing it directly. So, with machine learning, we still feed
  our computer data, but instead of imposing instructions and expecting output,
  <em>we provide the expected output and let the machine find an algorithm by
  itself</em>.<p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/january/images/annotated_traditional_paradigm.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/annotated_traditional_paradigm_sm.png" alt="Traditional programming paradigm"></a></p>
    <p align="center"><small>An illustration of the standard programming paradigm that most software developers are familiar with. The developer identifies the algorithm and implements the code; the users supply the data.</small></p>

  <p>To build a computer program that can identify who's in a photo, we can apply an
  algorithm that analyzes a large collection of images of your friends and
  generates a function that matches them. If we do this correctly, the generated
  function will also match new photos that we've never seen before. Of course, it
  will have no knowledge of its purpose; all it can do is identify things that
  are similar to the original images we fed it.</p>

  <p>In this situation, we call the images we provide the machine <em>training data</em>
  and the names of the people on the picture <em>labels</em>. Once we have <em>trained</em> an
  algorithm for our purpose, we can use it to <em>predict</em> labels on new data to
  test it. Figure &lt;&lt;figure-ml-schema&gt;&gt; displays this example alongside a schema
  of the machine learning paradigm.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/january/images/annotated_ml_paradigm.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/annotated_ml_paradigm_sm.png" alt="machine learning paradigm"></a></p>
    <p align="center"><small>An illustration of the machine learning paradigm. During development, we generate an algorithm from a data set and then incorporate that into our final application.</small></p>

  <p>Machine learning comes in when rules aren't clear; it can solve problems of the
  "I'll know it when I see it" variety. Instead of programming the function
  directly, we provide data that indicates what the function should do and then
  methodically generate a function that matches our data.</p>

  <p>In practice, you usually combine machine learning with traditional programming
  to build a useful application. For our face detection app, we have to instruct
  the computer on how to find, load, and transform the example images before we
  can apply a machine learning algorithm. Beyond that, we might use hand-rolled
  heuristics to separate headshots from photos of sunsets and latte art; then we
  can apply machine learning to put names to faces. Often a mixture of
  traditional programming techniques and advanced machine learning algorithms
  will be superior to either one alone.</p>

  <h3>Deep learning</h3>

  <p>This article is made up of sentences. The sentences are made of words, the words
  are made of letters, the letters are made up of lines and curve, and
  ultimately those lines and curves are made up of tiny tinted pixels. When
  teaching a child to read, we start with the smallest parts and work our way up:
  first letters, then words, then sentences, and finally complete books.
  (Normally children learn to recognize lines and curves on their own.) This kind
  of hierarchy is the natural way for people to learn complex concepts. At each
  level, we ignore some detail and the concepts become more abstract.</p>

  <p><em>Deep learning</em> applies the same idea to machine learning. Deep learning is a
  subfield of machine learning that uses a specific family of models: sequences
  of simple functions chained together. These chains of functions are known as
  <em>neural networks</em> because they were loosely inspired by the structure of
  natural brains.  The core idea of deep learning is that these sequences of
  functions can analyze a complex concept as a hierarchy of simpler ones. The first
  layer of a deep model can learn to take raw data and organize it in basic ways
  -- like grouping dots into lines. Each successive layer organizes the previous
  layer into more advanced and abstract concepts.</p>

  <p>The amazing thing about deep learning is that you do not need to know what the
  intermediate concepts are in advance. If you select a model with enough layers,
  and provide enough training data, the training process will gradually organize
  the raw data into increasingly high-level concepts. But how does the training
  algorithm know what concepts to use? It doesn't really; it just organizes the
  input in any way that helps it match the training examples better. So there is
  no guarantee this representation matches the way humans would think about the
  data.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/january/images/dl_pipeline.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/dl_pipeline_sm.jpg" alt="Deep learning"></a></p>
   <p align="center"><small>Deep learning and representation learning.</small></p>

  <h2>A lightning introducing to the Game of Go</h2>

  <p>The rules of Go are famously simple. In short, two players alternate
  placing black and white stones on a board, starting with the black player. The
  goal is to surround as much of the board as possible with your own stones. Although
  the rules are simple, Go strategy has endless depth, and we don't even attempt to
  cover it here.</p>

  <h3>The board</h3>

  <p>A Go board is a square grid. Stones go on the intersections, not inside the
  squares. The standard board is 19×19, but sometimes players use a smaller board
  for a quick game. The most popular smaller options are 9×9 and 13×13 boards.</p>

  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/board_with_stones.png" alt="Board with stones"></p>
    <p align="center"><small>A standard 19 × 19 Go board. The intersections marked with the
      dots are the star points: they are solely for players' reference. Stones go
      on the intersections.</small></p>

  <h3>Placing and capturing stones</h3>

  <p>One player plays with black stones and the other plays with white stones. The
  two players alternate placing stones on the board, starting with the black
  player.  Stones don't move once they are on the board, although they can be
  captured and removed entirely. To capture your opponent's stones, you must
  completely surround them with your own. Here's how that works.</p>

  <p>Stones of the same color that are touching are considered connected together. For
  the purposes of connection, we only consider straight up, down, left, or right;
  diagonals don't count. Any empty point touching a connected group is called a
  <em>liberty</em> of that group. Every group needs at least one liberty to stay on the
  board. So you can capture your opponent's stones by filling their liberties.</p>

  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/liberties.png" alt="Liberties"></p>
    <p align="center"><small>The three black stones are connected. They have four liberties on the
      points marked with squares. White can capture the black stones by placing white
      stones on all the liberties.</small></p>

  <p>When you place a stone in the last liberty of an opponent's group, that group
  is captured and removed from the board. The newly empty points are then
  available for either player to play on (so long as the move is legal). On the
  flip side, you may not play a stone that would have zero liberties, unless you
  are completing a capture.</p>

  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/two_eyes.png" alt="Two eyes"></p>
    <p align="center"><small>The white stones on the left can never be captured: black can play
      at neither A nor B. A black stone there would have no liberties and is, therefore,
      an illegal play. On the other hand, black can play at C to capture five white
      stones.</small></p>

  <p>There's an interesting consequence of the capturing rules. If a group of stones
  has two completely separate internal liberties, it can never be captured. See
  the figure above: black can't play at A, because that black
  stone would have no liberties. Nor can black play at B. So black has no way to
  fill the last two liberties of the white group. These internal liberties are
  called <em>eyes</em>. In contrast, black can play at C to capture five white stones.
  That white group has only one eye and is doomed to get captured at some point.</p>

  <p>Although it's not explicitly part of the rules, the idea that a group with two
  eyes can't be captured is the most basic part of Go strategy. In fact, this is
  the only strategy we will specifically code into our bot's logic. All the more
  advanced Go strategies will be inferred through machine learning.</p>

  <h3>Ending the game and counting</h3>

  <p>Either player may pass any turn instead of placing a stone. When both players
  pass consecutive turns, the game is over. Before scoring, the players identify
  any <em>dead stones</em>: stones that have no chance of making two eyes or connecting
  up to friendly stones. Dead stones are treated exactly the same as captures
  when scoring the game. If there's a disagreement, the players can resolve it by
  resuming play. But this is very rare: if the status of any group is unclear,
  players will usually try to resolve it before passing.</p>

  <p>The goal of the game is to surround a larger section of the board than your
  opponent. There are two ways to add up the score, but they nearly always give
  the same result.</p>

  <p>The most common counting method is <em>territory scoring</em>. In this case, you get
  one point for every point on the board that is completely surrounded by your
  own stones, plus one point for every opponent's stone that you captured. The
  player with more points in the winner.</p>

  <p>Here's a full 9x9 game with explanations that illustrate all these concepts in a
    concrete example. Use the arrows below the Go board to navigate through the moves.</p>

  <div style="text-align: center; width: 100%">
  <div style="display: inline-block;">
    <div style="float: left;">
      <div id="board">
      </div>

      <div class="commentary" style="height:80px; width:570px;">
      <div id="lastMove">
      </div>
      <div id="comments">
      </div>
      </div>

      <p class="controls">
      <a href="#" onclick="move(-5); return false;"><i class="fa fa-backward"></i></a>
      <a href="#" onclick="move(-1); return false;"><i class="fa fa-step-backward"></i></a>
      <strong id="move">1</strong> / <strong id="moves">1</strong>
      <a href="#" onclick="move(1); return false;"><i class="fa fa-step-forward"></i></a>
      <a href="#" onclick="move(5); return false;"><i class="fa fa-forward"></i></a>
      </p>
    </div>

  </div>
  </div>

  <script type="text/javascript" src="/community/eclipse_newsletter/2018/january/dist/jgoboard-latest.js"></script>
  <script type="text/javascript" src="/community/eclipse_newsletter/2018/january/large/board.js"></script>
  <script type="text/javascript" src="/community/eclipse_newsletter/2018/january/medium/board.js"></script>
  <script type="text/javascript">

  var gameRecord = [
  ["G7"],
  ["C7"],
  ["C3"],
  ["G3", "The players start in opposite corners, a typical opening."],
  ["H4", "Black attempts to make some territory on the right side."],
  ["G4", "White responds by strengthening the G4 stone."],
  ["H3"],
  ["H2"],
  ["G5"],
  ["D4", "This move has a loose connection to the G4 stones, and also puts pressure on the black C3 stone."],
  ["D3"],
  ["E2", "This low move helps make room for white to make eyes on the bottom, but it leaves a gap for black to push through."],
  ["C4"],
  ["B5"],
  ["E3"],
  ["F2", "White's G4 group is fairly safe now, but the D4 stone is in trouble."],
  ["D5", "Black captures the white stone. White can't escape by playing at E4."],
  ["E7", "White abandons the D4 stone (for now) and tries to make territory on the top."],
  ["F8", "Blacks wants to expand its territory on the top, while limiting white's."],
  ["E8"],
  ["E9"],
  ["D9"],
  ["F9"],
  ["C8", "The two players peacefully split up the top."],
  ["B4"],
  ["F7"],
  ["G8"],
  ["F5"],
  ["G6"],
  ["J3", "Note that the H4 stones have only two liberties now. This makes the cutting point at h4 very dangerous for black."],
  ["B6", "Instead of defending the cutting point on the right, black sacrifices the H4 stones in order to capture the B5 stone."],
  ["h4", "White completes the trade. The black stones can't escape by playing at J4: white will just capture at J5."],
  ["F4", "Black cuts the white chain in two."],
  ["F3", "Although the D4 stone was left for dead many turns ago, now black is forced to spend a move capturing it. This lets white get a big move elsewhere. Even dead stones are still worth something."],
  ["E4"],
  ["H6"],
  ["F6", "Black captures the F5 stone. White can't get any more liberties by playing at E5."],
  ["B1", "This tricky-looking move makes a big reduction in black's territory. This shape is called a <i>monkey jump</i> (or <i>saru-suberi</i> in Japanese)."],
  ["B2"],
  ["B7"],
  ["C6"],
  ["D7"],
  ["A7"],
  ["A8"],
  ["A6"],
  ["H7", "If you rewind to move 24, it looked like black would make 10 or more points on the right side; white has taken almost all of it away."],
  ["C1"],
  ["E6"],
  ["E5"],
  ["D2"],
  ["C2"],
  ["D1"],
  ["H8"],
  ["J8"],
  ["H9"],
  ["J7"],
  ["D6"],
  ["J9"],
  ["pass"],
  ["pass", "Black has 12 points of territory plus 4 captures. White has 17 points of territory plus 3 captures. So white is already ahead on the board, and wins comfortably after adding komi."],
  ];

  var whiteTerritory = [
      'A9', 'B9', 'C9', 'B8', 'D8',
      'J6', 'J5', 'H4', 'J4',
      'H3',
      'G2', 'J2',
      'E1', 'F1', 'G1', 'H1', 'J1',
  ];
  var blackTerritory = [
      'G9',
      'A5', 'B5', 'C5', 'F5',
      'A4', 'D4',
      'A3', 'B3',
      'A2',
      'A1', 'B1',
  ];


  var BOARD_SIZE = 9;
  var jrecord = new JGO.Record(BOARD_SIZE);
  var jboard = jrecord.jboard;
  var jsetup = new JGO.Setup(jboard, JGO.BOARD.largeWalnut);
  var moveIdx = -1;
  var colnames = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N'];
  var lastMove = null;

  function stringToCoords(move_string) {
      var colStr = move_string.substring(0, 1);
      var rowStr = move_string.substring(1);
      var col = colnames.indexOf(colStr);
      var row = BOARD_SIZE - parseInt(rowStr, 10);
      return new JGO.Coordinate(col, row);
  }

  function move(moveDiff) {
      if (moveDiff > 0) {
          var newMoveIdx = Math.min(gameRecord.length - 1, moveIdx + moveDiff);
          while (moveIdx < newMoveIdx) {
              nextMove();
          }
      } else if (moveDiff < 0) {
          var newMoveIdx = Math.max(moveIdx + moveDiff, -1);
          resetGame();
          move(newMoveIdx - moveIdx);
      }
      document.getElementById('move').innerHTML = (moveIdx + 1).toString();
      document.getElementById('moves').innerHTML = gameRecord.length.toString();
  }

  function resetGame() {
      jrecord.jboard.clear();
      jrecord.root = jrecord.current = null;
      jrecord.info = {};
      moveIdx = -1;
      document.getElementById('lastMove').innerHTML = '';
      document.getElementById('comments').innerHTML = '';
  }

  function nextMove() {
      if (moveIdx == gameRecord.length - 1) {
          return;
      }
      moveIdx += 1;
      var player = moveIdx % 2 == 0 ? JGO.BLACK : JGO.WHITE;
      var nextTurn = gameRecord[moveIdx];
      var nextMove = nextTurn[0];
      var comments = nextTurn[1] || '';
      if (nextMove == 'pass') {
          doPass(player);
      } else {
          doStone(player, stringToCoords(nextMove));
      }

      var playerName = player == JGO.BLACK ? 'Black' : 'White';
      var moveText = nextMove == 'pass' ? 'passes' : nextMove;
      document.getElementById('lastMove').innerHTML = playerName + ' ' + moveText;
      document.getElementById('comments').innerHTML = comments;

      if (moveIdx == gameRecord.length - 1) {
          // Show territory.
          var node = jrecord.current;
          for (var i = 0; i < blackTerritory.length; ++i) {
              node.setMark(stringToCoords(blackTerritory[i]), JGO.MARK.SQUARE);
          }
          for (var i = 0; i < whiteTerritory.length; ++i) {
              node.setMark(stringToCoords(whiteTerritory[i]), JGO.MARK.TRIANGLE);
          }
      }
  }

  function doPass(player) {
      // Clear last move mark.
      var node = jrecord.current;
      if (lastMove) {
          node.setMark(lastMove, JGO.MARK.NONE);
      }
      lastMove = null;
  }

  function doStone(player, coords) {
      var play = jboard.playMove(coords, player, false /* ko */);
      if (play.success) {
          var node = jrecord.createNode(true);
          node.info.captures[player] += play.captures.length; // tally captures
          node.setType(coords, player); // play stone
          node.setType(play.captures, JGO.CLEAR); // clear opponent's stones
          if (lastMove) {
              node.setMark(lastMove, JGO.MARK.NONE); // clear previous mark
          }
          node.setMark(coords, JGO.MARK.CIRCLE); // mark move
          lastMove = coords;
      }
  }

  jsetup.create('board', function(canvas) {
      document.body.onkeydown = function(e) {
          if(e.keyCode == 37) move(-1);
          else if(e.keyCode == 39) move(1);
      };
  });
  </script>
  <script type="text/javascript">JGO.auto.init(document, JGO);</script>

  <p>If you want to play a game yourself, you can do this right now. Below is a playable
    demo of a 5x5 Go bot that we built and deployed for you.<p>

  <div style="text-align: center; width: 100%">
    <div style="display: inline-block;">
      <iframe   src="https://www.badukai.com/demos/static/play_mcts_55.html" height="700" width="700" style="border:2px solid grey; background-color: #f8f8f8;"></iframe>
    </div>
  </div>

  <h2>Deep learning and the game of Go</h2>

  <h3>What machine learning can do for you in Go</h3>

  <p>Whether you're programming a computer to play Go or tic-tac-toe, most board
  game AIs share a similar overall structure. Depending on the game, the best solutions
  may involve game-specific logic, machine learning, or both. Let's have a look at a few
  tasks you can hope to achieve when attempting to solve board games with a computer:</p>

  <ul>
    <li>Selecting moves in the early game: many systems use an <em>opening book</em>, that is
      a database of opening sequences taken from expert human games.</li>
    <li>Searching game states: from the current board state, look ahead and try to evaluate
      what the outcome might be. Humans mostly use intuition for that, but computers are much
      stronger at brute forcing computation-heavy solutions. A smart application of these first
      two points essentially beat Chess world champion Garry Kasparov back in 1997.</li>
    <li>Reducing the number of moves to consider: In Go there are around 250 valid moves per
      turn. This means looking ahead just four moves requires evaluating nearly 4 billion
      positions. Employing smart heuristics to limit which moves to consider and which to discard immediately might help immensely.</li>
    <li>Evaluating game states: If you could perfectly judge how likely a board situation is
      to win in the end, you'd have a winning strategy: pick the move that maximizes the likelihood
      of winning the game. Of course, this is a very difficult problem and we can be happy to find good
      approximations of these values.
  </ul>

  <p>In Go, rules-based approaches to move selection turn out to be mediocre at this
  task: it's extremely difficult to write out rules that reliably identify the
  most important area of the board. But deep learning is perfectly suited to the
  problem -- we can apply it to train a computer to imitate a
  human Go player. Searching and evaluating game states are tasks that deep learning
  algorithms can be particularly strong at and for the remainder of this article we will
  focus on <em>predicting expert Go moves</em>.</p>

  <h3>Building blocks for a deep learning Go bot</h3>

  <p>We start with a large collection of game records between strong human players;
  online gaming servers are a great resource here. Then we replay all the games
  on a computer, extracting each board position and the following move. That's
  our training set. With a suitably deep neural network, it's possible to predict
  the human move with better than 50% accuracy. You can build a bot that just
  plays the predicted human move, and it's already a credible opponent. Schematically,
  the application we're trying to build looks as follows:</p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/january/images/overview_diagram.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2018/january/images/overview_diagram_sm.jpg" alt="Overview diagram deep learning bot"></a></p>
   <p align="center"><small>How to predict the next move in a game of Go using deep learning</small></p>

  <p>On a high level, to build a deep-learning-Go-move-prediction application, you need
    to address the following tasks:</p>

  <ol>
    <li><b>Downloading and processing Go data:</b> You can download expert Go data from
      various Go servers, like <a target="_blank" href="https://u-go.net/gamerecords/">KGS</a>. Most of
      the time this data comes in a specific, text-based format called <em>Smart Go Format
      (SGF)</em>. You need to read game information from such files, for instance how many
      moves there are, what the current board state or next move is.</li>
    <li><b>Encoding Go data:</b> Next, the Go board information has to be encoded in a
      machine-readable way, so that we can feed it into a neural network. The last figure
      illustrates a simple example of such an encoder, in which black stones are assigned a 1,
      white stones a -1 and empty points a 0. There are many more sophisticated ways to encode
      Go games, but this is a good first attempt.</li>
    <li><b>Building and training a deep learning model with Go data:</b> We then need to
      build a neural network into which we can feed the encoded Go data. Specifically, we
      will build a network, to which we show the current board situation and let it predict
      the next move. This prediction can be compared to the actual outcome (the next move
      from data). From this comparison, the network will infer how to adapt its parameters
      to get better at this task -- no explicit programming needed.  </li>
    <li><b>Serving the trained model:</b> Finally, once the model has been built and trained,
      we need to expose it somewhere for humans or other computers to play against. For humans,
      a graphical interface is convenient. For bots, you will have to comply with an exchange
      format like the Go Text Protocol (GTP). At the end of this article, you will run a Go bot
      served over HTTP with docker. </li>
  </ol>

  <p>Explaining all these points in  detail in an article is a bit too much, but at least we
  can show you how to do the third step, namely setting up and training a neural network with DL4J.</p>

  <h3>Building and training a deep learning model for Go with Eclipse Deeplearning4J</h3>

  <p><a target="_blank" href="https://deeplearning4j.org">Deeplearning4J</a> is an open-source deep learning framework
  at the core of a suite of other <a target="_blank" href="https://github.com/deeplearning4j">powerful machine learning tools for the JVM</a>. Perhaps most
  notably <a target="_blank" href="https://nd4j.org">ND4J</a> provides fast and versatile n-dimensional arrays, which
  can be used as the foundation for all sorts of numerical computing on the JVM.<p>

  <p>To build a deep neural network with Deeplearning4J for Go move prediction, we proceed in five steps: loading data, processing data, building a model, training the model and evaluating it - a typical procedure for machine learning applications. We've published the code for this section in the following <p><a target="_blank" href="https://github.com/maxpumperla/eclipse_dl4j_go_move_prediction">GitHub repo</a>, with instructions to run it with maven or docker.</p>

  <h4>Loading data</h4>

  <p>As a first step you need to load data into ND4J arrays. Features and labels for this example have
  been <a
  href="https://github.com/maxpumperla/eclipse_dl4j_go_move_prediction/tree/master/src/main/resources">
  created and stored</a> for you already, in two separate files. You can load each with a single line of code:</p>

  <pre style="background:#fff;color:#000">        INDArray features <span style="color:#ff7800">=</span> Nd4j.createFromNpyFile(<span style="color:#ff7800">new</span> <span style="color:#3b5bb5">ClassPathResource</span>(<span style="color:#409b1c">"features_3000.npy"</span>).getFile());
        INDArray labels <span style="color:#ff7800">=</span> Nd4j.createFromNpyFile(<span style="color:#ff7800">new</span> <span style="color:#3b5bb5">ClassPathResource</span>(<span style="color:#409b1c">"labels_3000.npy"</span>).getFile());

</pre>

  <h4>Processing data</h4>

  <p>Next, you build a so-called <em>DataSet</em> from features and labels, a core ND4J abstraction. Using this DataSet, you can split data into 90% training and 10% test data randomly:</p>

<pre style="background:#fff;color:#000">        DataSet allData <span style="color:#ff7800">=</span> <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">DataSet</span>(features, labels);
        SplitTestAndTrain testAndTrain <span style="color:#ff7800">=</span> allData.splitTestAndTrain(<span style="color:#3b5bb5">0.9</span>);
        DataSet trainingData <span style="color:#ff7800">=</span> testAndTrain.getTrain();
        DataSet testData <span style="color:#ff7800">=</span> testAndTrain.getTest();
</pre>

  <h4>Building a model</h4>

  <p>With data processed and ready to go, you can now turn to building the actual model that we'll
  feed the data into. In DL4J you do this by creating a model configuration, a so-called
  <em>MultiLayerConfiguration</em>. This configuration is built up modularly using a <em>builder
  pattern</em>, adding properties of the network one by one. Essentially, a neural network
  configuration consists of general properties for setup and learning and a list of <em>layers</em>. When we feed data into the network, data passes through the network sequentially, layer by layer, until we reach an output or <em>prediction</em>. The following code sets up a neural network
  tailored towards Go move prediction:</p>

<pre style="background:#fff;color:#000">    <span style="color:#ff7800">int</span> size <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">19</span>;
        <span style="color:#ff7800">int</span> featurePlanes <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">11</span>;
        <span style="color:#ff7800">int</span> boardSize <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">19</span> <span style="color:#ff7800">*</span> <span style="color:#3b5bb5">19</span>;
        <span style="color:#ff7800">int</span> randomSeed <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">1337</span>;

        MultiLayerConfiguration conf <span style="color:#ff7800">=</span> <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">NeuralNetConfiguration.Builder</span>()
                .seed(randomSeed)
                .learningRate(.<span style="color:#3b5bb5">1</span>)
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.ADAGRAD)
                .list()
                .layer(<span style="color:#3b5bb5">0</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">ConvolutionLayer.Builder</span>(<span style="color:#3b5bb5">3</span>, <span style="color:#3b5bb5">3</span>)
                        .nIn(featurePlanes).stride(<span style="color:#3b5bb5">1</span>, <span style="color:#3b5bb5">1</span>).nOut(<span style="color:#3b5bb5">50</span>).activation(Activation.RELU).build())
                .layer(<span style="color:#3b5bb5">1</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">SubsamplingLayer.Builder</span>(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(<span style="color:#3b5bb5">2</span>, <span style="color:#3b5bb5">2</span>).stride(<span style="color:#3b5bb5">1</span>, <span style="color:#3b5bb5">1</span>).build())
                .layer(<span style="color:#3b5bb5">2</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">ConvolutionLayer.Builder</span>(<span style="color:#3b5bb5">3</span>, <span style="color:#3b5bb5">3</span>)
                        .stride(<span style="color:#3b5bb5">1</span>, <span style="color:#3b5bb5">1</span>).nOut(<span style="color:#3b5bb5">20</span>).activation(Activation.RELU).build())
                .layer(<span style="color:#3b5bb5">3</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">SubsamplingLayer.Builder</span>(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(<span style="color:#3b5bb5">2</span>, <span style="color:#3b5bb5">2</span>).stride(<span style="color:#3b5bb5">1</span>, <span style="color:#3b5bb5">1</span>).build())
                .layer(<span style="color:#3b5bb5">4</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">DenseLayer.Builder</span>().activation(Activation.RELU)
                        .nOut(<span style="color:#3b5bb5">500</span>).build())
                .layer(<span style="color:#3b5bb5">5</span>, <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">OutputLayer.Builder</span>(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(boardSize).activation(Activation.SOFTMAX).build())
                .setInputType(InputType.convolutional(size, size, featurePlanes))
                .backprop(<span style="color:#3b5bb5">true</span>).pretrain(<span style="color:#3b5bb5">false</span>).build();

</pre>

  <p>We can't go into details about the individual parts of this model, but note that on a high level it consists of six layers, two convolutional layers, two subsampling layers, a dense layer and an output layer. These layers map our input data to a vector of length 19 times 19, corresponding to the number of possible moves on a 19 by 19 Go board. Convolutional and subsampling layers are often used together and are very good at detecting features in <em>spatial data</em>, such as images, videos, or Go boards.</p>

  <h4>Training the model</h4>

  <p>To train our model, you first need to create a <em>MultiLayerNetwork</em> from the above configuration, initialize it and then fit or train it on the training data:</p>

<pre style="background:#fff;color:#000">        MultiLayerNetwork model <span style="color:#ff7800">=</span> <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">MultiLayerNetwork</span>(conf);
        model.init();
        model.setListeners(<span style="color:#ff7800">new</span> <span style="color:#3b5bb5">ScoreIterationListener</span>(<span style="color:#3b5bb5">10</span>));
        model.fit(trainingData);

</pre>

  <h4>Evaluating the model</h4>

  <p>The last step is to evaluate your model on test data and check the results:</p>

<pre style="background:#fff;color:#000">        Evaluation eval <span style="color:#ff7800">=</span> <span style="color:#ff7800">new</span> <span style="color:#3b5bb5">Evaluation</span>(<span style="color:#3b5bb5">19</span> <span style="color:#ff7800">*</span> <span style="color:#3b5bb5">19</span>);
        INDArray output <span style="color:#ff7800">=</span> model.output(testData.getFeatureMatrix());
        eval.<span style="color:#3b5bb5">eval</span>(testData.getLabels(), output);
        log<span style="color:#3b5bb5">.info</span>(eval.stats());
</pre>

  <p>If you run this yourself, don't be surprised by the relatively poor results of this little
  experiment. We only use 3000 moves to train the network for a complex game that
  has about 200 moves per game. So the data we're using is way too small to expect this network
  to learn to accurately predict next moves. However, the network we chose, trained on a data set
  consisting of a few hundred thousand games worth of moves, will play Go at an intermediate amateur
  level. Not bad for just a few lines of code.</p>

  <h3>Running an end-to-end deep learning bot</h3>

  <p>Finally, if you want to run an end-to-end go bot locally on your machine, you can do this quite
  easily with docker. When pulling the following image from Docker Hub, you have access to all
  the bots we currently have built for the book:</p>

<pre style="background:#fff;color:#000">
docker pull maxpumperla/dlgo
docker run maxpumperla/dlgo
</pre>

  <p>Running these two lines will start up a Python web server that serves several Go bots. For
  instance, you can play against a 9x9 bot trained with so-called <em>policy gradients</em>, a
  more advanced deep learning technique, by navigating to the following site in your browser:</p>

  127.0.0.1:5000/static/play_pg_99.html

  <h2>Conclusion</h2>

  <p>We've covered a lot of ground in this article and hope we have sparked your interest in both the
  field of deep learning and the game of Go. If you want to learn more, check out the following
  resources:

  <ul>
    <li><a target="_blank" href="https://www.amazon.com/Deep-Learning-Practitioners-Josh-Patterson/dp/1491914254/"><b>Deep Learning: A Practitioner's Approach:</b></a> This book shows you how to get started with deep
    learning in a very pragmatic way, giving you an in-depth introduction to Eclipse DL4J and its
    ecosystem. If you know Java, but are new to deep learning, this might be a very interesting read.</li>
    <li><a target="_blank" href="https://www.manning.com/books/deep-learning-and-the-game-of-go"><b>Deep Learning and the Game of Go:</b></a> In this book, you learn all about deep learning as it can be applied in the
    running example of mastering computer Go. This article contains an excerpt of the first two chapters that are <a target="_blank" href="https://livebook.manning.com/#!/book/deep-learning-and-the-game-of-go/chapter-1">freely available</a>. The online version of the book is packed with interactive tutorials and demos,
    a few of which you've seen here. Notably, apart from knowing Python at an intermediate level,
    it does not make any assumptions about your knowledge of machine learning. So, if you're new to
    this and want an introduction to deep learning by example, this book might be for you (make sure
    to use "smpumperla40" to save 40%).</li>
  </ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/max.jpg"
        alt="Max Pumperla" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Max Pumperla<br />
            <a target="_blank" href="https://skymind.ai/">Skymind</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/maxpumperla">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/maxpumperla">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>