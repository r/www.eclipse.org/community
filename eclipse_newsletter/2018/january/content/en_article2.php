<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>Last year I made the bold move to write-up the <a target="_blank" href="https://ianskerrett.wordpress.com/2016/12/19/iot-trends-to-watch-in-2017/">2017 trends</a> I thought would be important for the IoT industry and <a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a> community. It seemed like a useful exercise so I thought a 2018 version is appropriate.</p>

<h2>1. Edge/Fog/IoT Gateway Computing</h2> 
<p>Edge/Fog/IoT Gateway computing will continue to gain traction. It is not feasible that all IoT devices will be communicating directly with the cloud. The amount of data, network availability and latency, security are some of the primary drivers for pushing IoT to compute toward the edges of the network. For example, an IoT solution for an oil rig needs to work regardless of connectivity to the cloud.</p>

<p>Companies like <a target="_blank" href="https://www.theregister.co.uk/2017/10/11/dell_iot_tech_tech_investment/">Dell</a>, who happen to see hardware for gateways are announcing significant investment in edge computing. <a target="_blank" href="https://softwareengineeringdaily.com/2017/02/03/the-end-of-cloud-computing-with-peter-levine/">Silicon Valley VCs</a> are now saying edge computing is the next big thing and <i>potentially the end of cloud computing</i>. FWIW, I don’t see cloud computing ending anytime soon.</p>

<p>I think we will continue to see Eclipse Kura and Eclipse ioFog being embraced for edge computing. In 2018, I hope we will see <a target="_blank" href="https://www.eclipse.org/kura/">Eclipse Kura</a> running <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-deeplearning4j">Eclipse DeepLearning4J</a> providing a great IoT machine learning platform at the edge.</p>

<h2>2. Digital Twins</h2> 
<p>Digital Twins will become a thing. I expect to see the IoT industry talk more about the term ‘<a target="_blank" href="https://en.wikipedia.org/wiki/Digital_twin">digital twin</a>‘. Most of the major IoT vendors claim support for digital twins: <a target="_blank" href="https://www.ge.com/digital/industrial-internet/digital-twin">GE</a>, <a target="_blank" href="https://www.ibm.com/internet-of-things/spotlight/digital-twin">IBM</a>, <a target="_blank" href="https://www.bosch-iot-suite.com/things/">Bosch</a>, <a target="_blank" href="http://docs.aws.amazon.com/iot/latest/developerguide/iot-thing-shadows.html">Amazon</a>, <a target="_blank" href="https://enterprise.microsoft.com/en-us/trends/microsoft-is-redefining-digital-twins-in-discrete-manufacturing/">Microsoft</a>. It will be interesting to see if the IoT industry will agree to a common definition for digital twin and even some common standards. Some common standards would enable a more robust vendor ecosystem for digital twins.</p>

<p>For Eclipse IoT, the <a target="_blank" href="https://www.eclipse.org/ditto/">Eclipse Ditto</a> project is a new project that provides a framework for managing and creating digital twins. The code based is from the Bosch implementation of their digital twin solution. You can check out their first <a target="_blank" href="https://www.eclipse.org/ditto/2017-12-18-milestone-announcement-010-M1.html">milestone release</a>.</p>

<h2>3. Eclipse IoT Integration</h2> The Eclipse IoT open source projects are maturing to the point where many are providing stable production ready functionality. We are seeing more and more interest in the community to focus on integration between the projects. I see this as a very positive sign and expect to see more cross-project collaborations in 2018. Is possible the Eclipse IoT WG organizes the first Eclipse IoT release train in 2018?</p>

<h2>4. More Open IoT Testbeds</h2> 
<p>In 2017, the IoT WG launched two <a target="_blank" href="https://iot.eclipse.org/testbeds/">open IoT Testbeds</a>: 1) Asset Tracking and 2) Industry 4 Production Performance Management. These testbeds have successfully demonstrated how open source and commercial solutions can be integrated to solve a real IoT solution. In 2018, I hope we see more end-user organizations proposing and leading testbeds that show how a community of vendors can solve their IoT requirements.</p>

<h2>5. Technology to watch in 2018</h2> It is difficult to ignore the hype around blockchain. However, it has not been clear how that blockchain technology scales up and down to meet the requirements of IoT. <a target="_blank" href="https://iota.org/">IOTA</a>’s tangle technology seems to have an approach that may work for IoT. <a target="_blank" href="https://venturebeat.com/2017/12/19/robert-bosch-venture-capital-bets-on-iota-as-it-invests-in-the-future-of-iot/">Robert Bosch Venture Capital</a> announcement that they have purchased IOTA tokens to invest in the future of IoT seems pretty significant.</p>

<p>Low-power Wide-Area Network (LPWAN) technology also seems to be ready for wide adoption. There appear to be a lot of momentum around LoRaWAN. <a target="_blank" href="https://www.thethingsnetwork.org/">The Things Network</a> community continues to drive forward an open source community for LoRaWAN. With more roots in the cellular industry, <a target="_blank" href="https://www.amihotechnology.com/lora-vs-nbiot/">NB-IoT appears to be the alternative to LoRaWAN</a>. Wide adoption of both technologies will be a positive influence on enabling more IoT use cases.</p>

<p>2018 will be another exciting year for IoT. My bold prediction is that 2018 is going to be the year that companies will start talking about their IoT success stories. Enjoy!</p>



<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/ian.jpg"
        alt="Ian Skerrett" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Ian Skerrett<br />
            <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ianskerrett">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://ianskerrett.wordpress.com">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>