<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Eclipse Community Directions for 2018";
  $pageKeywords = "eclipse community, trends 2018, ee4j, java ee, deeplearning, vert.x";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "As 2018 begins, Mike Milinkovich would like to share a few thoughts on where I think the Eclipse community is heading.";

  // Uncomment and set $original_url if you know the original url of this article.
  $original_url = "https://mmilinkov.wordpress.com/2018/01/22/eclipse-community-directions-for-2018/";
  $og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");