<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Most of the Java EE projects have already <a target="_blank" href="http://blog.supol.cz/?p=5">transitioned to the Eclipse Foundation</a> under the <a target="_blank" href="https://jakarta.ee/">Jakarta EE</a> umbrella of projects. One of those projects is Eclipse Jersey, which is a RESTful Web services framework and reference implementation of the <a target="_blank" href="https://jcp.org/en/jsr/detail?id=370">JAX-RS Specification</a>. We <a target="_blank" href="http://blog.supol.cz/?p=36">announced</a> the initial contribution of Jersey a few weeks ago. Since then, we saw an increase of community interest in Jersey. We registered a number of filed ideas for enhancements, discovered issues, and received pull requests from both individuals and industry. All of these are very welcome.</p>

<p>Jersey is used in <a target="_blank" href="https://projects.eclipse.org/projects/ee4j.glassfish">Eclipse GlassFish</a> and Weblogic application servers, and it is often understood to be an "enterprise" framework that needs full application server with CDI, <a target="_blank" href="https://projects.eclipse.org/projects/ee4j.ejb">EJB</a>, and bean validation support, or at least a <a target="_blank" href="https://projects.eclipse.org/projects/ee4j.servlet">servlet</a> container, such as Tomcat. Sure, Jersey can leverage integration with other Java EE/<a target="_blank" href="https://projects.eclipse.org/projects/ee4j">Jakarta EE projects</a>, but there is more. Jersey comes with an ability to deploy JAX-RS application in a Java SE environment; it provides a container extension module that enables support for using a variety of frameworks that only understand the HTTP protocol. This is useful in the microservices world. Using Jersey, you can create a simple micro application that is capable of running inside a Docker container with just a JDK installed!</p>

<p>Let’s create an application that accepts queries and provides answers. First, we define the maven dependencies. Note the last hk2 dependency which is needed since Jersey 2.26:</p>

<pre style='color:#000000;background:#ffffff;'>&lt;dependency>
  &lt;groupId>org.glassfish.jersey.core&lt;/groupId>
  &lt;artifactId>jersey-common&lt;/artifactId>
&lt;/dependency>
&lt;dependency>
  &lt;groupId>org.glassfish.jersey.core&lt;/groupId>
  &lt;artifactId>jersey-client&lt;/artifactId>
&lt;/dependency>
&lt;dependency>
  &lt;groupId>org.glassfish.jersey.core&lt;/groupId>
  &lt;artifactId>jersey-server&lt;/artifactId>
&lt;/dependency>
&lt;dependency>
  &lt;groupId>org.glassfish.jersey.inject&lt;/groupId>
  &lt;artifactId>jersey-hk2&lt;/artifactId>
&lt;/dependency>
</pre>

<p>For simplicity, we can use the HTTP server from the JDK so as not to bring other dependencies, such as the widely used <a target="_blank" href="https://projects.eclipse.org/projects/ee4j.grizzly">Eclipse Grizzly</a>. (Other <a target="_blank" href="https://jersey.github.io/documentation/latest/deployment.html#deployment.javase">deployment options</a> are described in the <a target="_blank" href="https://jersey.github.io/documentation/latest/index.html">Jersey User Guide</a>, which has yet to be transitioned to the Eclipse Foundation):</p>

<pre style='color:#000000;background:#ffffff;'>&lt;dependency>
  &lt;groupId>org.glassfish.jersey.containers&lt;/groupId>
  &lt;artifactId>jersey-container-jdk-http&lt;/artifactId>
&lt;/dependency>
</pre>

<p>We would like to send popular JSON, so we add a dependency for it:</p>

<pre style='color:#000000;background:#ffffff;'>&lt;dependency>
  &lt;groupId>org.glassfish.jersey.media&lt;/groupId>
  &lt;artifactId>jersey-media-json-binding&lt;/artifactId>
&lt;/dependency>
</pre>

<p>We define the entity to be sent as an answer and the resource that sends responses for the <a target="_blank" href="http://hitchhikers.wikia.com/wiki/Ultimate_Question">ultimate question</a> requests:</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> Answer {
  <span style='color:#7f0055; font-weight:bold; '>public</span> Answer(<span style='color:#7f0055; font-weight:bold; '>String</span> answer) {
    <span style='color:#7f0055; font-weight:bold; '>this</span>.answer = answer;
  }
  <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>String</span> answer;
}

@Path("/question")
<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> QuestionResource {
  <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>static</span> <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> RESPONSE = <span style='color:#2a00ff; '>"42"</span>;

  @GET
  @Path(<span style='color:#2a00ff; '>"ultimate"</span>)
  @Produces(MediaType.APPLICATION_JSON)
  <span style='color:#7f0055; font-weight:bold; '>public</span> Answer getResponse() {
    <span style='color:#7f0055; font-weight:bold; '>return</span> <span style='color:#7f0055; font-weight:bold; '>new</span> Answer(RESPONSE);
  }
}
</pre>

<p>We start the JDK HTTP Server on port 8080 with the <code>QuestionResource</code> registered. We also want the entity to be wired as JSON, so we use <code>JsonBindingFeature</code>:</p>

<pre style='color:#000000;background:#ffffff;'>URI base_uri = UriBuilder.fromUri("http:<span style='color:#3f7f59; '>//localhost/").port(8080).build();</span>
ResourceConfig config = new ResourceConfig(QuestionResource.class, JsonBindingFeature.class);
JdkHttpServerFactory.createHttpServer(base_uri, config);
</pre>

<p>At this point, the Jersey application is up. We can use cURL, or even better, the JAX-RS Client (implemented by Jersey of course – and we have defined the maven <code>org.glassfish.jersey.core:jersey-client</code> dependency for it already):</p>

<pre style='color:#000000;background:#ffffff;'>System.out.println(ClientBuilder.newClient().target(BASE_URI).path("question/ultimate").request().get(String.class));
</pre>

<p>And we should see the JSON response:</p>

<pre style='color:#000000;background:#ffffff;'>{<span style='color:#2a00ff; '>"answer"</span>:<span style='color:#2a00ff; '>"42"</span>}
</pre>

<p>The ability to run the JAX-RS application in the Java SE environment is a new feature in the planned <a target="_blank" href="https://projects.eclipse.org/projects/ee4j.jaxrs">Eclipse JAX-RS</a> 2.2 release.</p>

<p>JAX-RS is evolving fast. There are already plans for 3.0, which Jersey needs to reflect. Also there are <a target="_blank" href="https://projects.eclipse.org/proposals/eclipse-microprofile">Eclipse MicroProfile</a> features that Jersey should either implement or integrate with. And JDK 9/10/11 need to be fully supported. There is a lot of work for the future and we are looking forward to hearing from the community. Your <a target="_blank" href="https://github.com/eclipse-ee4j/jersey">feature proposals, bug reports, and pull requests</a> are very much appreciated!</p>



<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/august/images/jan.jpg"
        alt="Jan Supol" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jan Supol<br />
            <a target="_blank" href="https://www.oracle.com/">Oracle</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.supol.cz/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/jansupol">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>