<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>While REST and Microservices are common terms these days, few people know JAX-RS, or do have a rather blurry vision of it. In fact, JAX-RS started a decade ago to provide a standard API for more sophisticated support in writing RESTful web service than the Servlet API. Back then, "fat" application servers were state of the art, still having CORBA, EJBs and SOAP onboard. REST was the new kid on the block, and JAX-RS made it pretty simple to get it right, without getting into vendor lock-in, and without simply doing XML-over-HTTP. JAX-RS started with native support for automatic encoding and decoding headers, parameters, and even better, XML payloads. Yes, there had been a time before JSON, and thanks to JAX-RS's perfect JAXB integration, it was a painless no-brainer to turn XML representations into Java parameters, and back. But the best of all was the complete separation of business logic and technological aspects, all greatly served by the concept of providers, part of a highly extensible SPI layer. BTW, while CDI still was in the making, JAX-RS already provided annotation-based context and dependency injection, and automatic detection of custom providers.</p>

<p>Times have changed. Today application servers feel clumsy compared to Microservices, with their strange deployment model, slow boot times, and complex configuration. Generation Y is used to instantaneous REPL workstyle, expects no boilerplates and slow turnaround cycles. Nobody cares for XML anymore, as it clogs our gigabit pipes with masses of unnecessary whitespace and tags. With feeble-powered mobile devices there came the rise of JSON. Furthermore, modern programmers like to find support for reactive streams. And a whole industry works on their next revolution, this time accepting IT protocols like MQTT, AMQP but also certainly REST to be used in myriads of microdevices – with no room for a full server. All that are massive challenges to the grownup ex-Java EE member API. But the master driver behind Java EE lost pace. And then stuck. There was no progress for a very long time.</p>

<h2>And then came Jakarta EE</h2>

<p>Under the auspices of the Eclipse Foundation, a lot did change. Besides Oracle now others could actively participate in really everything – not only contribute code, but also commit, manage, and have a say in rather every aspect of JAX-RS. Thanks to GitHub, collaborating was easy, and really fair. No more waiting for Oracle. The community picked up leadership immediately, so JAX-RS became one of the first active projects of Jakarta EE, and is still today. Volunteers, like long-term JAX-RS EG member Markus Karg and MVC API project lead Christian Kaltepoth, chimed in with organizational improvements and first fully-externally developed features additions. They work alongside people from Oracle, Payara, IBM, Red Hat, Apache, and many more, on the next, better, greater JAX-RS. Together they provide the heart of Java Microservices: A standard for REST in the Java ecosystem.</p>
 
<p>Updated to meet current needs and trends, like supporting pure Java SE deployments without application servers, CDI, Flow, and much more, JAX-RS will not just be the layer between Servlets and business logic. It will become the centerpiece driving your applications. And it will be made for the community by the community. Innovation, made at the Eclipse Foundation.</p>

<p>If you like to join the JAX-RS team: Contributions are welcome! Pick an issue from our <a target="_blank" href="https://github.com/eclipse-ee4j/jaxrs-api/issues">GitHub tracker</a>, discuss it with us, send us PRs. We are glad for every helping hand!</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/august/images/markus.jpeg"
        alt="Markus Karg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Markus Karg<br />
            <a target="_blank" href=""></a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://headcrashing.wordpress.com/author/fertigmampf/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/mkarg">GitHub</a></li>          
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>