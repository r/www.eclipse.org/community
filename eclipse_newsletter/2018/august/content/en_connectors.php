<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The Connector Architecture is probably one of the least known specifications in Jakarta EE, and as such, not as widely used as the others. However, this technology, when implemented properly, allows you to connect your application server to just about anything, and do so in a portable manner. In this article, I’ll show you how to get started with a simple connector, introduce some examples that you can take and modify for your own needs to get started, and finally wrap up with some example connectors to inspire you.</p>

<h2>Packaging a connector</h2>
<p>In this article, we’ll create 4 artifacts in order to deploy and use our connector. These are:</p>
    <ul>
        <li>An API jar which will contain the interfaces for our connector. Any applications that use our connector will need to have these interfaces on the classpath.</li>
        <li>A jar with the implementation of our resource adapter.</li>
        <li>A rar file which will contain the implementation jar, along with a ra.xml deployment descriptor file.</li>
        <li>An application which will send messages to and receive messages from the system we’re integrating with via the connector.</li>
   </ul>
   
   <p>The sample code for this article is in the GitHub repository here: <a target="_blank" href="https://github.com/tomitribe/sample-slack-connector">https://github.com/tomitribe/sample-slack-connector</a>. In this example, we’ll create a connector to communicate with the popular messaging tool, Slack. The connector will use the allbegray <a target="_blank" href="https://github.com/allbegray/slack-api">Slack API</a>. This API provides a real-time connection to Slack to both post messages and be notified when others post messages. Connections through this library will appear as ’bots’ in Slack.</p>
   
<h2>Creating the base connector classes</h2>
<p>Let’s start by creating the base class for the ResourceAdapter in the implementation jar. This class should implement <code>javax.resource.spi.ResourceAdapter</code>, and be annotated with <code>@javax.resource.spi.Connector</code>. This annotation provides basic metadata for the adapter, such as the display name and a brief description.</p>
<p>There are five methods on the resource adapter. The <code>start()</code> and <code>stop()</code> methods are called by the application server when the adapter starts and stops. The <code>endpointActivation()</code> and <code>endpointDeactivation()</code> methods are called when message-driven beans (MDBs) associated with the adapter are started and stopped. We’ll focus on outbound communication to start with, so we’ll leave the implementation of these blank. Finally, there’s the <code>getXAResources() method</code>. For simplicity, we won’t implement transaction handling with this adapter, so this can return an empty array.</p>
   
<pre style='color:#000000;background:#ffffff;'>@Connector(description = "Sample Resource Adapter", displayName = "Sample Resource Adapter", eisType = "Sample Resource Adapter", version = "1.0")
<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackResourceAdapter <span style='color:#7f0055; font-weight:bold; '>implements</span> ResourceAdapter {

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> start(<span style='color:#7f0055; font-weight:bold; '>final</span> BootstrapContext bootstrapContext) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceAdapterInternalException {
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> stop() {
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> endpointActivation(<span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpointFactory messageEndpointFactory, <span style='color:#7f0055; font-weight:bold; '>final</span> ActivationSpec activationSpec)
            <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> endpointDeactivation(<span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpointFactory messageEndpointFactory, <span style='color:#7f0055; font-weight:bold; '>final</span> ActivationSpec activationSpec) {
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> XAResource[] getXAResources(<span style='color:#7f0055; font-weight:bold; '>final</span> ActivationSpec[] activationSpecs) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        <span style='color:#7f0055; font-weight:bold; '>return</span> <span style='color:#7f0055; font-weight:bold; '>new</span> XAResource[0];
    }
}
</pre>


<p>The Slack API requires a token (which you can obtain by adding a bot in Slack’s admin interface – see the “testing it out section” later). We can provide the token to the resource adapter as a configuration setting. This can be done by adding a field, annotated with @javax.resource.spi.ConfigProperty.</p>
<p>We can fill out the <code>start()</code> method to connect to Slack through the albergray API, and the stop method to disconnect. Finally, add a <code>sendMessage()</code> method to send a message through to Slack.</p>

<pre style='color:#000000;background:#ffffff;'>@ConfigProperty
    <span style='color:#7f0055; font-weight:bold; '>private</span> String token;

    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackRealTimeMessagingClient slackRealTimeMessagingClient;
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackWebApiClient webApiClient;
    <span style='color:#7f0055; font-weight:bold; '>private</span> String user;

    <span style='color:#7f0055; font-weight:bold; '>public</span> String getToken() {
        <span style='color:#7f0055; font-weight:bold; '>return</span> token;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> void setToken(<span style='color:#7f0055; font-weight:bold; '>final</span> String token) {
        <span style='color:#7f0055; font-weight:bold; '>this</span>.token = token;
    }


    <span style='color:#7f0055; font-weight:bold; '>public</span> void start(<span style='color:#7f0055; font-weight:bold; '>final</span> BootstrapContext bootstrapContext) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceAdapterInternalException {
        webApiClient = SlackClientFactory.createWebApiClient(token);
        slackRealTimeMessagingClient = SlackClientFactory.createSlackRealTimeMessagingClient(token);

        <span style='color:#7f0055; font-weight:bold; '>final</span> Authentication authentication = webApiClient.auth();
        user = authentication.getUser();

        webApiClient.setPresenceUser(Presence.AUTO);

        slackRealTimeMessagingClient.connect();
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> void stop() {
        webApiClient.setPresenceUser(Presence.AWAY);
    }

<span style='color:#7f0055; font-weight:bold; '>public</span> void sendMessage(<span style='color:#7f0055; font-weight:bold; '>final</span> String channel, <span style='color:#7f0055; font-weight:bold; '>final</span> String message) {
        ChatPostMessageMethod postMessage = <span style='color:#7f0055; font-weight:bold; '>new</span> ChatPostMessageMethod(channel, message);
        postMessage.setUsername(user);
        webApiClient.postMessage(postMessage);
    }
</pre>  

<h2>Outbound communication</h2>
<p>Next, want to do add the necessary code to enable an application to send a message through the adapter. This is done by obtaining a connection from a connection factory, and connections are pooled by the application server. If you have sent a message using JMS before, then you’ll be familiar with this pattern.</p> 
<p>For this part, we’ll need to create two interfaces for our connector’s custom API jar – the ConnectionFactory and the Connection. We also need four classes for the implementation jar: implementations of the ConnectionFactory and Connection, and also implementations of ManagedConnectionFactory and ManagedConnection.</p> 

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/slack_schema.png"></p>

<p>The flow works as shown in the diagram below. The ConnectionFactory is injected into the application and requests a connection through the createConnection() method. This delegates to the ConnectionManager, which is provided by the application server, and is responsible for managing connection pooling. The ConnectionManager delegates to the ManagedConnectionFactory to create and match connections.</p><br/>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/august/images/flowchart.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/flowchart_sm.png"></a></p>

<p>The resource adapter is responsible for providing the Connection, Connection Factory, Managed Connection and Managed Connection Factory classes, while the application server provides the connection manager and is responsible for pooling connections. The application that interacts with resource adapter will use the Connection Factory and Connection classes, via an interface. The ConnectionFactory can either be looked up from JNDI or injected using the @Resource annotation on a field.</p>

<p>These classes are fairly boilerplate and are shown below. You can use the barebones project here to get started rather than write these classes from scratch.</p>
<p>The <code>ManagedConnectionFactory</code> should be annotated with <code>@javax.resource.spi.ConnectionDefinition</code>, which specifies the Connection and ConnectionFactory API and implementation classes.</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>interface</span> SlackConnection {
    <span style='color:#7f0055; font-weight:bold; '>void</span> sendMessage(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel, <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> message);
    <span style='color:#7f0055; font-weight:bold; '>void</span> close();
}

<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>interface</span> SlackConnectionFactory <span style='color:#7f0055; font-weight:bold; '>extends</span> Serializable, Referenceable {
    SlackConnection getConnection() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException;
}

<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackConnectionImpl <span style='color:#7f0055; font-weight:bold; '>implements</span> SlackConnection {
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackManagedConnection mc;
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackManagedConnectionFactory mcf;

    <span style='color:#7f0055; font-weight:bold; '>public</span> SlackConnectionImpl(<span style='color:#7f0055; font-weight:bold; '>final</span> SlackManagedConnection mc, <span style='color:#7f0055; font-weight:bold; '>final</span> SlackManagedConnectionFactory mcf) {
        <span style='color:#7f0055; font-weight:bold; '>this</span>.mc = mc;
        <span style='color:#7f0055; font-weight:bold; '>this</span>.mcf = mcf;
    }

    <b><span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> sendMessage(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel, <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> message) {
        mc.sendMessage(channel, message);</b>
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> close() {
        mc.closeHandle(<span style='color:#7f0055; font-weight:bold; '>this</span>);
    }
}

<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackConnectionFactoryImpl <span style='color:#7f0055; font-weight:bold; '>implements</span> SlackConnectionFactory {
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>static</span> <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>long</span> serialVersionUID = 1L;
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>final</span> Logger log = Logger.getLogger(SlackConnectionFactoryImpl.class.getName());
    <span style='color:#7f0055; font-weight:bold; '>private</span> Reference reference;
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackManagedConnectionFactory mcf;
    <span style='color:#7f0055; font-weight:bold; '>private</span> ConnectionManager connectionManager;

    <span style='color:#7f0055; font-weight:bold; '>public</span> SlackConnectionFactoryImpl(<span style='color:#7f0055; font-weight:bold; '>final</span> SlackManagedConnectionFactory mcf, <span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionManager cxManager) {
        <span style='color:#7f0055; font-weight:bold; '>this</span>.mcf = mcf;
        <span style='color:#7f0055; font-weight:bold; '>this</span>.connectionManager = cxManager;
    }

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> SlackConnection getConnection() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"getConnection()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> (SlackConnection) connectionManager.allocateConnection(mcf, <span style='color:#7f0055; font-weight:bold; '>null</span>);
    }

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> Reference getReference() <span style='color:#7f0055; font-weight:bold; '>throws</span> NamingException {
        log.finest(<span style='color:#2a00ff; '>"getReference()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> reference;
    }

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> setReference(Reference reference) {
        log.finest(<span style='color:#2a00ff; '>"setReference()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>this</span>.reference = reference;
    }
}

<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackManagedConnection <span style='color:#7f0055; font-weight:bold; '>implements</span> ManagedConnection {

    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>final</span> Logger log = Logger.getLogger(SlackManagedConnection.class.getName());
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> logwriter;
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackManagedConnectionFactory mcf;
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>List</span>&lt;ConnectionEventListener> listeners;
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackConnectionImpl connection;

    <span style='color:#7f0055; font-weight:bold; '>public</span> SlackManagedConnection(<span style='color:#7f0055; font-weight:bold; '>final</span> SlackManagedConnectionFactory mcf) {
        <span style='color:#7f0055; font-weight:bold; '>this</span>.mcf = mcf;
        <span style='color:#7f0055; font-weight:bold; '>this</span>.logwriter = <span style='color:#7f0055; font-weight:bold; '>null</span>;
        <span style='color:#7f0055; font-weight:bold; '>this</span>.listeners = Collections.synchronizedList(<span style='color:#7f0055; font-weight:bold; '>new</span> ArrayList&lt;ConnectionEventListener>(1));
        <span style='color:#7f0055; font-weight:bold; '>this</span>.connection = <span style='color:#7f0055; font-weight:bold; '>null</span>;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>Object</span> getConnection(<span style='color:#7f0055; font-weight:bold; '>final</span> Subject subject, <span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionRequestInfo cxRequestInfo) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"getConnection()"</span>);
        connection = <span style='color:#7f0055; font-weight:bold; '>new</span> SlackConnectionImpl(<span style='color:#7f0055; font-weight:bold; '>this</span>, mcf);
        <span style='color:#7f0055; font-weight:bold; '>return</span> connection;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> associateConnection(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>Object</span> connection) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"associateConnection()"</span>);

        <span style='color:#7f0055; font-weight:bold; '>if</span> (connection == <span style='color:#7f0055; font-weight:bold; '>null</span>)
            <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> ResourceException(<span style='color:#2a00ff; '>"Null connection handle"</span>);

        <span style='color:#7f0055; font-weight:bold; '>if</span> (!(connection <span style='color:#7f0055; font-weight:bold; '>instanceof</span> SlackConnectionImpl))
            <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> ResourceException(<span style='color:#2a00ff; '>"Wrong connection handle"</span>);

        <span style='color:#7f0055; font-weight:bold; '>this</span>.connection = (SlackConnectionImpl) connection;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> cleanup() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"cleanup()"</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> destroy() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"destroy()"</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> addConnectionEventListener(<span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionEventListener listener) {
        log.finest(<span style='color:#2a00ff; '>"addConnectionEventListener()"</span>);

        <span style='color:#7f0055; font-weight:bold; '>if</span> (listener == <span style='color:#7f0055; font-weight:bold; '>null</span>) {
            <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> <span style='color:#7f0055; font-weight:bold; '>IllegalArgumentException</span>(<span style='color:#2a00ff; '>"Listener is null"</span>);
        }

        listeners.add(listener);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> removeConnectionEventListener(<span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionEventListener listener) {
        log.finest(<span style='color:#2a00ff; '>"removeConnectionEventListener()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>if</span> (listener == <span style='color:#7f0055; font-weight:bold; '>null</span>) {
            <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> <span style='color:#7f0055; font-weight:bold; '>IllegalArgumentException</span>(<span style='color:#2a00ff; '>"Listener is null"</span>);
        }

        listeners.remove(listener);
    }

    <span style='color:#7f0055; font-weight:bold; '>void</span> closeHandle(<span style='color:#7f0055; font-weight:bold; '>final</span> SlackConnection handle) {
        ConnectionEvent event = <span style='color:#7f0055; font-weight:bold; '>new</span> ConnectionEvent(<span style='color:#7f0055; font-weight:bold; '>this</span>, ConnectionEvent.CONNECTION_CLOSED);
        event.setConnectionHandle(handle);
        <span style='color:#7f0055; font-weight:bold; '>for</span> (ConnectionEventListener cel : listeners) {
            cel.connectionClosed(event);
        }
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> getLogWriter() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"getLogWriter()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> logwriter;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> setLogWriter(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> out) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"setLogWriter()"</span>);
        logwriter = out;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> LocalTransaction getLocalTransaction() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> NotSupportedException(<span style='color:#2a00ff; '>"getLocalTransaction() not supported"</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> XAResource getXAResource() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> NotSupportedException(<span style='color:#2a00ff; '>"getXAResource() not supported"</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> ManagedConnectionMetaData getMetaData() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"getMetaData()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> <span style='color:#7f0055; font-weight:bold; '>new</span> SlackManagedConnectionMetaData();
    }

    <b><span style='color:#7f0055; font-weight:bold; '>void</span> sendMessage(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel, <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> message) {
        log.finest(<span style='color:#2a00ff; '>"sendMessage()"</span>);

        <span style='color:#7f0055; font-weight:bold; '>final</span> SlackResourceAdapter resourceAdapter = (SlackResourceAdapter) mcf.getResourceAdapter();
        resourceAdapter.sendMessage(channel, message);
    }</b>
}

@ConnectionDefinition(connectionFactory = SlackConnectionFactory.class,
        connectionFactoryImpl = SlackConnectionFactoryImpl.class,
        connection = SlackConnection.class,
        connectionImpl = SlackConnectionImpl.class)
<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackManagedConnectionFactory <span style='color:#7f0055; font-weight:bold; '>implements</span> ManagedConnectionFactory, ResourceAdapterAssociation {

    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>static</span> <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>long</span> serialVersionUID = 1L;
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>final</span>  Logger log = Logger.getLogger(SlackManagedConnectionFactory.class.getName());
    <span style='color:#7f0055; font-weight:bold; '>private</span> ResourceAdapter ra;
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> logwriter;

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>Object</span> createConnectionFactory(<span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionManager cxManager) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"createConnectionFactory()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> <span style='color:#7f0055; font-weight:bold; '>new</span> SlackConnectionFactoryImpl(<span style='color:#7f0055; font-weight:bold; '>this</span>, cxManager);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>Object</span> createConnectionFactory() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> ResourceException(<span style='color:#2a00ff; '>"This resource adapter doesn't support non-managed environments"</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> ManagedConnection createManagedConnection(<span style='color:#7f0055; font-weight:bold; '>final</span> Subject subject, <span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionRequestInfo cxRequestInfo) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"createManagedConnection()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> <span style='color:#7f0055; font-weight:bold; '>new</span> SlackManagedConnection(<span style='color:#7f0055; font-weight:bold; '>this</span>);
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> ManagedConnection matchManagedConnections(<span style='color:#7f0055; font-weight:bold; '>final</span> Set connectionSet, <span style='color:#7f0055; font-weight:bold; '>final</span> Subject subject, <span style='color:#7f0055; font-weight:bold; '>final</span> ConnectionRequestInfo cxRequestInfo) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"matchManagedConnections()"</span>);
        ManagedConnection result = <span style='color:#7f0055; font-weight:bold; '>null</span>;
        Iterator it = connectionSet.iterator();
        <span style='color:#7f0055; font-weight:bold; '>while</span> (result == <span style='color:#7f0055; font-weight:bold; '>null</span> &amp;&amp; it.hasNext()) {
            ManagedConnection mc = (ManagedConnection) it.next();
            <span style='color:#7f0055; font-weight:bold; '>if</span> (mc <span style='color:#7f0055; font-weight:bold; '>instanceof</span> SlackManagedConnection) {
                result = mc;
            }

        }
        <span style='color:#7f0055; font-weight:bold; '>return</span> result;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> getLogWriter() <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"getLogWriter()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> logwriter;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> setLogWriter(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>PrintWriter</span> out) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        log.finest(<span style='color:#2a00ff; '>"setLogWriter()"</span>);
        logwriter = out;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> ResourceAdapter getResourceAdapter() {
        log.finest(<span style='color:#2a00ff; '>"getResourceAdapter()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>return</span> ra;
    }

    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> setResourceAdapter(ResourceAdapter ra) {
        log.finest(<span style='color:#2a00ff; '>"setResourceAdapter()"</span>);
        <span style='color:#7f0055; font-weight:bold; '>this</span>.ra = ra;
    }
}
</pre>

<p>See the code highlighted in bold – this is the sendMessage() method on the connection which delegates to the ManagedConnection, which in turn calls the send() method we added to the resource adapter earlier.</p>

<h2>Testing it out</h2>
<p>To test the resource, we’ll need to package it. To do this, the implementation jar will need to be packaged inside a RAR file, along with a ra.xml deployment descriptor in META-INF. The ra.xml tells the application server which  classes in the resource adapter to use.</p>

<pre style='color:#000000;background:#ffffff;'>&lt;connector xmlns="http:<span style='color:#3f7f59; '>//xmlns.jcp.org/xml/ns/javaee"</span>
           xmlns:xsi="http:<span style='color:#3f7f59; '>//</span><span style='color:#3f3fbf; '>www.w3.org/2001/XMLSchema-instance</span><span style='color:#3f7f59; '>"</span>
           xsi:schemaLocation="http:<span style='color:#3f7f59; '>//xmlns.jcp.org/xml/ns/javaee </span><span style='color:#3f3fbf; '>http://xmlns.jcp.org/xml/ns/javaee/connector_1_7.xsd</span><span style='color:#3f7f59; '>"</span>
           version="1.7">
    &lt;description>Chatterbox Slack Connector&lt;/description>
    &lt;display-name>Chatterbox Slack Connector&lt;/display-name>
    &lt;eis-type>Slack Connector&lt;/eis-type>
    &lt;resourceadapter-version>1.0&lt;/resourceadapter-version>
    &lt;license>
        &lt;license-required>false&lt;/license-required>
    &lt;/license>
    &lt;resourceadapter>
        &lt;resourceadapter-class>org.tomitribe.chatterbox.slack.adapter.SlackResourceAdapter&lt;/resourceadapter-class>
        &lt;config-property>
            &lt;config-property-name>token&lt;/config-property-name>
            &lt;config-property-type>String&lt;/config-property-type>
        &lt;/config-property>
        &lt;outbound-resourceadapter>
            &lt;connection-definition>
                &lt;managedconnectionfactory-class>org.tomitribe.chatterbox.slack.adapter.out.SlackManagedConnectionFactory&lt;/managedconnectionfactory-class>
                &lt;connectionfactory-interface>org.tomitribe.chatterbox.slack.api.SlackConnectionFactory&lt;/connectionfactory-interface>
                &lt;connectionfactory-impl-class>org.tomitribe.chatterbox.slack.adapter.out.SlackConnectionFactoryImpl&lt;/connectionfactory-impl-class>
                &lt;connection-interface>org.tomitribe.chatterbox.slack.api.SlackConnection&lt;/connection-interface>
                &lt;connection-impl-class>org.tomitribe.chatterbox.slack.adapter.out.SlackConnectionImpl&lt;/connection-impl-class>
            &lt;/connection-definition>
            &lt;transaction-support>NoTransaction&lt;/transaction-support>
            &lt;reauthentication-support>false&lt;/reauthentication-support>
        &lt;/outbound-resourceadapter>
    &lt;/resourceadapter>
&lt;/connector>
</pre>

<p>The API jar will need to be added to the classpath for both the resource adapter and the application. I’m using Apache TomEE, so I place the API jar in the lib directory, and the RAR file in the apps directory, and I enable the apps directory by uncommenting the following option in tomee.xml:</p>

<pre style='color:#000000;background:#ffffff;'>&lt;!-- &lt;Deployments dir="apps" /> -->
</pre>

<p>The final thing you’ll want to do before you start the application is provide your slack token to the connector. The process for this varies from server to server, but TomEE it is provided via a system property. The name of the system property follows the name of the rar file with “RA” on the end, then a dot, and then the name of the property. So using the adapter exactly as it is in the example project, I need to set the following system property:</p>

<pre style='color:#000000;background:#ffffff;'>slack-connector-rar-0.1-SNAPSHOTRA.token=XXXXXXXXXX
</pre>

<p>You can obtain the token from Slack’s admin interface under: Customize Slack &rarr; Configure Apps &rarr; Custom Integrations &rarr; Bots</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/august/images/bots.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/bots_sm.png"></a></p>

<p>In order to send an outbound message, the application will need to obtain a connection to slack from a connection factory. To do this, the <code>ConnectionFactory</code> can either be injected into a managed component or looked up in JNDI. The Connection to Slack can be obtained using the <code>getConnection()</code> method.</p>

<pre style='color:#000000;background:#ffffff;'>@Singleton
@Lock(LockType.READ)
@Path("sender")
<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> Sender {
    <span style='color:#7f0055; font-weight:bold; '>private</span> <span style='color:#7f0055; font-weight:bold; '>final</span> Logger log = Logger.getLogger(Sender.class.getName());

    @Resource
    <span style='color:#7f0055; font-weight:bold; '>private</span> SlackConnectionFactory cf;

    @Path(<span style='color:#2a00ff; '>"{channel}"</span>)
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> sendMessage(@PathParam(<span style='color:#2a00ff; '>"channel"</span>) <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel, <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> message) {
        <span style='color:#7f0055; font-weight:bold; '>try</span> {
            <span style='color:#7f0055; font-weight:bold; '>final</span> SlackConnection connection = cf.getConnection();
            connection.sendMessage(channel, message);
            connection.close();
        } <span style='color:#7f0055; font-weight:bold; '>catch</span> (ResourceException e) {
            log.severe(<span style='color:#2a00ff; '>"Unexpected error sending message to Slack: "</span> + e.getMessage());
        }
    }
}
</pre>

<h2>Inbound communication</h2>
<p>Inbound communication, is also possible, with messages arriving in Slack calling a Message Driven Bean inside the application. Fortunately, there is less boilerplate code with inbound communication than there is with outbound.</p>
<p>We’ll start by adding a simple interface in the interface jar that our MDBs must implement in order to receive messages.</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>interface</span> InboundListener {
    <span style='color:#7f0055; font-weight:bold; '>void</span> messageReceived(<span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel, <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> message);
}
</pre>

<p>The implementation jar needs an ActivationSpec class adding to it. ActivationSpec provides the means to configure individual MDBs. In this example, the ActivationSpec is minimal and the adapter will broadcast all messages to all MDBs, but it would be possible to extend this to specify which Slack channel a particular MDB is interested in, for example.</p>

<pre style='color:#000000;background:#ffffff;'>@Activation(messageListeners = InboundListener.class)
<span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>class</span> SlackActivationSpec <span style='color:#7f0055; font-weight:bold; '>implements</span> ActivationSpec {

    <span style='color:#7f0055; font-weight:bold; '>private</span> ResourceAdapter resourceAdapter;

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> validate() <span style='color:#7f0055; font-weight:bold; '>throws</span> InvalidPropertyException {
    }

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> ResourceAdapter getResourceAdapter() {
        <span style='color:#7f0055; font-weight:bold; '>return</span> resourceAdapter;
    }

    @Override
    <span style='color:#7f0055; font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; '>void</span> setResourceAdapter(<span style='color:#7f0055; font-weight:bold; '>final</span> ResourceAdapter ra) <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
        <span style='color:#7f0055; font-weight:bold; '>this</span>.resourceAdapter = ra;
    }
}
</pre>

<p>You may remember the core of the resource adapter tracks MDBs registered with it through the <code>endpointActivation()</code> and <code>endpointDeactivation()</code> methods.</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>public</span> void endpointActivation(<span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpointFactory messageEndpointFactory, <span style='color:#7f0055; font-weight:bold; '>final</span> ActivationSpec activationSpec)
        <span style='color:#7f0055; font-weight:bold; '>throws</span> ResourceException {
    <span style='color:#7f0055; font-weight:bold; '>final</span> SlackActivationSpec slackActivationSpec = (SlackActivationSpec) activationSpec;

    <span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpoint messageEndpoint = messageEndpointFactory.createEndpoint(<span style='color:#7f0055; font-weight:bold; '>null</span>);
    targets.put(slackActivationSpec, messageEndpoint);
}

<span style='color:#7f0055; font-weight:bold; '>public</span> void endpointDeactivation(<span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpointFactory messageEndpointFactory, <span style='color:#7f0055; font-weight:bold; '>final</span> ActivationSpec activationSpec) {
    <span style='color:#7f0055; font-weight:bold; '>final</span> SlackActivationSpec telnetActivationSpec = (SlackActivationSpec) activationSpec;

    <span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpoint endpoint = targets.get(telnetActivationSpec);
    <span style='color:#7f0055; font-weight:bold; '>if</span> (endpoint == <span style='color:#7f0055; font-weight:bold; '>null</span>) {
        <span style='color:#7f0055; font-weight:bold; '>throw</span> <span style='color:#7f0055; font-weight:bold; '>new</span> <span style='color:#7f0055; font-weight:bold; '>IllegalStateException</span>(<span style='color:#2a00ff; '>"No Endpoint to undeploy for ActivationSpec "</span> + activationSpec);
    }

    endpoint.release();
}
</pre>

<p>We can make the resource adapter class receive messages from Slack itself by adding the following before the <code>slackRealTimeMessagingClient.connect()</code> call:</p>

<pre style='color:#000000;background:#ffffff;'>slackRealTimeMessagingClient.addListener(Event.MESSAGE, this);
</pre>

<p>This will require the resource adapter to implement the Slack API’s EventListener class, which defines an <code>onMessage()</code> method. In the implementation of this method, we can push the message out to all the registered MDBs. There is one requirement, which is <code>beforeDelivery()</code> must be called before the call to the <code>messageReceived()</code> method on the MDB, and <code>afterDelivery()</code> must be called afterwards.
</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>public</span> void onMessage(<span style='color:#7f0055; font-weight:bold; '>final</span> JsonNode jsonNode) {
    <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> text = jsonNode.get(<span style='color:#2a00ff; '>"text"</span>).textValue();
    <span style='color:#7f0055; font-weight:bold; '>final</span> <span style='color:#7f0055; font-weight:bold; '>String</span> channel = jsonNode.get(<span style='color:#2a00ff; '>"channel"</span>).textValue();

    <span style='color:#7f0055; font-weight:bold; '>for</span> (<span style='color:#7f0055; font-weight:bold; '>final</span> MessageEndpoint endpoint : targets.values()) {
        <span style='color:#7f0055; font-weight:bold; '>boolean</span> beforeDelivery = <span style='color:#7f0055; font-weight:bold; '>false</span>;

        <span style='color:#7f0055; font-weight:bold; '>try</span> {
            endpoint.beforeDelivery(messageReceivedMethod);
            beforeDelivery = <span style='color:#7f0055; font-weight:bold; '>true</span>;

            ((InboundListener) endpoint).messageReceived(channel, text);
        } <span style='color:#7f0055; font-weight:bold; '>catch</span> (<span style='color:#7f0055; font-weight:bold; '>Throwable</span> t) {
            log.severe(<span style='color:#2a00ff; '>"Unable to deliver message to endpoint. Cause: "</span> + t.getMessage());
        } <span style='color:#7f0055; font-weight:bold; '>finally</span> {
            <span style='color:#7f0055; font-weight:bold; '>if</span> (beforeDelivery) {
                <span style='color:#7f0055; font-weight:bold; '>try</span> {
                    endpoint.afterDelivery();
                } <span style='color:#7f0055; font-weight:bold; '>catch</span> (<span style='color:#7f0055; font-weight:bold; '>Throwable</span> t) {
                    log.severe(<span style='color:#2a00ff; '>"Unable to call afterDelivery(). Cause: "</span> + t.getMessage());
                }
            }
        }
    }
}
</pre>

<h2>Taking it further</h2>
<p>The use the interface on the MDB is a little inflexible. It is possible to get the class name of the MDB, in the <code>endpointActivation()</code> method by using this code:</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#7f0055; font-weight:bold; '>final</span> EndpointTarget target = new EndpointTarget(messageEndpoint);
<span style='color:#7f0055; font-weight:bold; '>final</span> Class&lt;?> endpointClass = slackActivationSpec.getBeanClass() != null ? slackActivationSpec
    .getBeanClass() : messageEndpointFactory.getEndpointClass();
</pre>

<p>With this, the resource adapter could reflectively look at annotations on the methods, and use those to decide which method it is going to call. Using the Slack example, imagine multiple methods on the MDB, each with a @Channel annotation specifying which channel they are interested in. You can see some examples of connectors using annotations in this way in the chatterbox project – see the <a target="_blank" href="https://tomitribe.io/projects/chatterbox">Chatterbox</a> and <a target="_blank" href="https://tomitribe.io/projects/crest">CREST</a> projects for examples.</p>

<h2>Other features</h2>
<p>This simple introduction has shown how to get started with creating a resource adapter, but has not covered some of the more advanced features of resource adapters, including security, transactions and work management. You can learn more about these in the <a target="_blank" href="http://download.oracle.com/otndocs/jcp/connector_architecture-1_7-mrel-spec/">specification documents</a> or take a look at the Sheldon connector available here: <a target="_blank" href="https://tomitribe.io/projects/sheldon">https://tomitribe.io/projects/sheldon</a>. The sheldon connector makes use of the security aspects on the spec, as well as the work manager to allow a user to SSH directly into the application server and issue commands which are executed as MDBs – this could be a neat way to provide a command line interface for management functions for your next application.</p>



<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/august/images/jonathan.jpeg"
        alt="Jonathan Gallimore" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jonathan Gallimore<br />
            <a target="_blank" href="https://www.tomitribe.com/">Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jongallimore">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/jgallimore">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>