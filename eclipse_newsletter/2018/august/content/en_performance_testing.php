<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <p><i><small>This article is originally posted at the AdoptOpenJDK blog site and is modified slightly to mention some application/Microprofile testing plans.</small></i></p>
   
   <p>Before I dabble in the juicy world of computer architectures and measuring and understanding performance implications, let me premise this entire post with a quick introduction to myself.</p>

<p>I am not a performance analyst, nor am I a low-level software developer trying to optimize algorithms to squeeze out the last ounce of performance on particular hardware.</p>

<p>While I am fortunate to work with people who have those skills, I myself am a 'happy-go-lucky' / high-level software developer.  My focus in the last few years has been developing my skills as a verification expert. I have a particular interest in finding solutions that help testing software easier and more effective. One flavour of software verification is performance testing.</p>

<p>While I am fairly new to performance benchmarking, I am experienced in streamlining processes and tools to reduce the friction around common tasks.  If we want to empower developers to be able to benchmark and test the performance impact that their changes, we need to create tools and workflows that are dead easy. I personally need it to be dead easy! "<a target="_blank" href="https://www.youtube.com/watch?v=IuRLGdGnqSU">Not only am I the hair club president, I am also a client</a>". I want to be able to easily run performance benchmarks and at some level understand the results of those benchmarks. This seems like a good time to segue to the recent open-sourcing of tools that help in that effort, <a target="_blank" href="https://github.com/AdoptOpenJDK/openjdk-test-tools#perfnext">PerfNext</a>/<a target="_blank" href="https://github.com/AdoptOpenJDK/openjdk-test-tools#test-result-summary-service-trss">TRSS</a> and <a target="_blank" href="https://github.com/AdoptOpenJDK/bumblebench">Bumblebench</a>… a topic of my presentation entitled "<a target="_blank" href="https://www.eclipsecon.org/europe2018/sessions/java-performance-testing-everyone">Performance Testing for Everyone</a>" at the upcoming <a target="_blank" href="https://www.eclipsecon.org/europe2018">EclipseCon Europe</a> conference.</p> 

<p>Existing benchmarks that you may wish to run are not standardized. They take very different inputs. PerfNext offers a easy to use GUI to configure and vary inputs which you “tell it about” via a standardized config file. Once you have set the inputs to the values you like, or keep the default settings, PerfNext then generates a script for the particular platform on which you wish to run. This can be sent directly to a Jenkins server to be run once, or checked into a git repo (as we are doing in the openjdk-tests repo at AdoptOpenJDK), to be picked up and deployed as part of the continuous integration pipelines. Once the benchmarks are run, they produce very different types of non-standardized outputs. TRSS parses the variety of different results to present them in a graphical way. Adding new benchmarks is a matter of adding new input config files for PerfNext and a new Parser for TRSS.</p>  

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/folders.png"></p>

<p>For writing your own benchmarks, there are some useful tools. The de facto standard for Java developers is <a target="_blank" href="http://openjdk.java.net/projects/code-tools/jmh/">jmh</a>. An additional, newly available option is <a target="_blank" href="https://github.com/AdoptOpenJDK/bumblebench">bumblebench</a>. Each have their benefits and reasons why they are useful. They offer developers a way to easily write microbenchmarks which can then be launched from PerfNext, or added directly into the CI test jobs at AdoptOpenJDK, or at any other project. Perhaps more importantly, microbenchmarks offer a very quick way for developers to verify changes locally have not caused performance regressions. If a developer has made String related changes, they could run the string_set of microbenches on their laptop with and without their changes before pushing changes forward.</p>

<p>If our work at the AdoptOpenJDK project proceeds as planned, I will be demonstrating the Test Results Summary Service (TRSS) and the PerfNext tool in October. TRSS offers some compelling visual representations of test results such as graphical representations of daily builds of different benchmarks (as shown in a diagram below), and a comparison view. This comparison view is most interesting, despite it being perhaps the most simple.  We do not just run performance tests at the project. In fact, we have a great variety of tests (including regression, system and application tests) described in more detail in a previous blog post called "<a target="_blank" href="https://blog.adoptopenjdk.net/2017/12/testing-java-help-count-ways">Testing Java: Help Me Count the Ways</a>". Most test-related questions I field start with “how does that compare to…?" What is the axis around which to compare? There are many and they are layered. Take any test, such as some of the <a target="_blank" href="http://microprofile.io/">Eclipse MicroProfile</a> tests currently included in our "external tests" builds, and run those tests, but vary OpenJDK version or OpenJDK implementation. If the test is a Jakarta EE compliance test (some of those <a target="_blank" href="https://github.com/eclipse/microprofile-fault-tolerance/blob/master/tck/running_the_tck.asciidoc">fault tolerance APIs</a> or <a target="_blank" href="https://github.com/eclipse/microprofile-metrics/blob/master/tck/running_the_tck.asciidoc">metrics APIs</a> TCKs we <a target="_blank" href="https://github.com/AdoptOpenJDK/openjdk-tests/blob/master/thirdparty_containers/README.md">plan to add</a> into our builds), compare against the <a target="_blank" href="https://javaee.github.io/glassfish/">GlassFish</a> reference implementation, or across projects. And what to compare? The tests results themselves, but perhaps also the execution time or other implicit output. Essentially, the tools we are creating, should allow for many lines of enquiry and comparison.</p>

<p>But returning back to the current story, a wonderful opportunity presented itself. We have the great fortune at the AdoptOpenJDK project to work with many different teams, groups and sponsors. <a target="_blank" href="https://www.packet.net/">Packet</a>, a cloud provider of bare-metal servers, is one of our sponsors who donates machine time to the project allowing us to provide pre-built and tested OpenJDK binaries from <a target="_blank" href="https://github.com/AdoptOpenJDK/openjdk-build">build scripts</a> and infrastructure. They are very supportive of open-source projects, and recently offered us some time on one of their new <a target="_blank" href="https://www.acceleratewithoptane.com/access">Intel® Optane™ SSD servers</a> (with their Skylake microarchitecture).</p>

<p>Packet and AdoptOpenJDK share the mutual goal of understanding how these machines affect Java™ Virtual Machine (JVM) performance. Admittedly, I attempted to parse all of the information found in the <a target="_blank" href="https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-optimization-manual.pdf">Intel® 64 and IA-32 Architectures Optimization Manual</a>, but needed some help. Skylake improves on the Haswell and Broadwell predecessors. Luckily, Vijay Sundaresan, WAS and Runtimes Performance Architect, took the time to summarize some features of the Skylake architecture. He outlined those features having the greatest impact on JVM performance and therefore are of great interest to JVM developers. Among the improvements he listed :</p>

    <ul>
        <li>Skylake’s 1.5X memory bandwidth, higher memory capacity at a lower cost per GB than DRAM and better memory resiliency</li>
        <li>Skylake cache memory hierarchy is quite different to Broadwell, with one of the bigger changes being that it stopped being inclusive</li>
        <li>Skylake also added AVX-512 (512 bytes vector operations) which is a 2X improvement over AVX-256 (256 bytes vector operations)</li>
    </ul>

<p>Knowing of those particular improvements and how a JVM implementation leverages them, we hoped to see a 10-20% improvement in per-core performance. This would be in keeping with the <a target="_blank" href="https://www.spec.org/jbb2015/results/jbb2015.html">Intel® published SPECjbb®2015 benchmark</a>** (the de facto standard Java<sup>™</sup> Server Benchmark) scores showing improvements in that range.</p>

<p>We were not disappointed. We decided to run variants of the ODM benchmark. This benchmark runs a Rules engine typically used for automating complex business decision automation, think analytics (compliance auditing for Banking or Insurance industries as a use case example). Ultimately, the benchmark processes input files. In one variant, a small set of 5 rules, in the other a much larger set of 300 rules was used. The measurement tracks how many times a rule can be processed per second, in other words, it measures the throughput of the Rules engine with different kinds of rules as inputs. This benchmark does a lot of String/Date/Integer heavy processing and comparison as those are common datatypes in the input files. Based on an average of the benchmark runs that were run on the Packet machine, we saw a healthy improvement of 13% and 20% in the 2 scenarios used.</p>


<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/august/images/odmresults.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/odmresults_sm.png"></a></p>
<p align="center">Summary of ODM results</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/august/images/odm_graph.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/odm_graph_sm.png"></a></p>
<p align="center">ODM results from PerfNext/TRSS graph view</p>

<p>We additionally ran some of our other tests used to verify AdoptOpenJDK builds on this machine to compare the execution times… We selected a variety of OpenJDK implementations (hotspot and openj9), and versions (openjdk8, openjdk9, and openjdk10), and are presenting a cross-section of them in the table below. While some of the functional and regression tests were flat or saw modest gains, we saw impressive improvements in our load/system tests. For background, some of these system tests create hundreds or thousands of threads, and loop through the particular tests thousands of times. In the case of the sanity group of system tests, we went from a typical 1 hr execution time to 20 minutes, while the extended set of system tests saw an average 2.25 hr execution time drop to 34 minutes.</p>

<p>To put the system test example in perspective, and looking at our daily builds at AdoptOpenJDK, on the x86-64_linux platform, we have typically 3 OpenJDK versions x 2 OpenJDK implementations, plus a couple of other special builds under test, so 8 test runs x 3.25 hrs = 26 daily execution hours on our current machines. If we switched over to the Intel® Optane™ machine on Packet, would drop to 7.2 daily execution hours. A tremendous savings, allowing us to free up machine time for other types of testing, or increase the amount of system and load testing we do per build.</p>

<p>The implication? For applications that behave like those system tests, (those that create lots of threads and iterate many times across sets of methods, including many GUI-based applications or servers that maintain a 1:1 thread to client ratio), there may be a compelling story to shift.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/august/images/systemresults.png"></p>
<p align="center">System and functional test results and average execution times</p>

<p>Having this opportunity from Packet, has provided us a great impetus to forge into "open performance testing" story for OpenJDK implementations and some of our next steps at AdoptOpenJDK. We have started to develop tools to improve our ability to run and analyze results. We have begun to streamline and automate performance benchmarks into our CI pipelines. We have options for bare-metal machines, which gives us isolation and therefore confidence that results are not contaminated by other services sharing machine resources. Thanks to Beverly, Piyush, Lan and Awsaf for getting some of this initial testing going at AdoptOpenJDK. While there is a lot more to do, I look forward to seeing how it will evolve and grow into a compelling story for the OpenJDK community.</p>

<p>Special thanks to Vijay, for taking the time to share with me some of his thoughtful insights and great knowledge! He mentioned with respect to Intel Skylake, there are MANY other opportunities to explore and leverage including some of its memory technologies for Java<sup>™</sup> heap object optimization, and some of the newer instructions for improved GC pause times. We encourage more opportunities to experiment and investigate, and invite any and all collaborators to join us. It is an exciting time for OpenJDK implementations, <strong>innovation happens in the open</strong>, with the help of great collaborators, wonderful partners and sponsors!</p>

<p><small>** SPECjbb®2015 is a registered trademark of the Standard Performance Evaluation Corporation (SPEC).</small></p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/august/images/shelley.jpg"
        alt="Shelley Lambert" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Shelley Lambert<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ShelleyMLambert">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://8thdaytesting.com/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/shelley-lambert-6120961/">LinkedIn</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/smlambert/">GitHub</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>