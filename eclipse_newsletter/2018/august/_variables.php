<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "Exploring Jakarta EE Technologies";
  $pageKeywords = "eclipse, newsletter, jakarta EE, jaxrs, jersey, glassfish";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "This second issue of the Jakarta EE Newsletter focuses on the technical side of things.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "Devoxx US";
  $sponsorLink = "https://devoxx.us/";
  $sponsorImage = "/community/eclipse_newsletter/assets/public/images/devoxxus_small.png";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;