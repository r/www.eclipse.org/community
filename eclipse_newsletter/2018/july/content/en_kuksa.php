<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>The smartphone has conquered almost every person's daily life. In the morning, it wakes you up with exotic sounds, presents the current news, and later on gives motivational assistance as a personal trainer during sports in accordance with your vital signs. In the evening, it suggests interesting events in the local area before calculating the cheapest way home.</p>

<p>The fact that the smartphone assists in such different situations is partly due to the fact that it is full of sensors and can access any services through its connection to the internet. Consequently, smartphone data can be accessed by a large number of developers in order to develop interesting and innovative applications.</p>

<p>The car is yet another companion of many people's daily life. Just like the smartphone, the modern car is full of sensors. However, a continuous connection to the internet and a possibility to openly extend vehicle functions or even add new vehicle applications is missing. While OEMs and supplier yet cope with such challenges proprietarily, a common infrastructure or at least standardized interfaces would significantly ease development activities and strengthen collaboration and innovation in terms of IoT, Cloud, and 5G technologies.</p>

<p>Additionally, such an environment would ease a large number of interesting applications. For example, when buying a vehicle, the decision of whether to buy or not to buy a navigation system and a cruise control technology could be flexibly postponed to a certain point in time, for instance, when a long holiday has been scheduled that certainly makes such technologies very worthy.</p>

<p>Furthermore, the unpleasant situation of a breakdown could be significantly improved, since an alarmed rescue service would already have access to a detailed list of the problems encountered and consequently could pack all the appropriate tools and spare parts this specific rescue would require. In accordance, traffic lights could be switched to the ambulance's route convenience, in order to provide the most interruption-free ambulance route that is possible.</p>

<p>The features of the 5G technology enable ubiquitous connection of vehicles to the internet and thus allow the scenarios described above. What is missing is a platform that enables the vehicles to be equipped with such new functionalities.</p>

<p>Eclipse Kuksa addresses exactly this point. It provides a development ecosystem for building the cloud and vehicle infrastructure for Car2X scenarios. Eclipse Kuksa is licensed under EPL 2.0 that allows the development of an extensible open infrastructure that is standardized between vehicle manufacturers.</p>

<h2>Structure of the Kuksa Ecosystem</h2>

<p>The development of an infrastructure for Car2X scenarios comprises three areas as shown the Figure below: An in-vehicle platform, a platform in the cloud back-end and an IDE for the development of new functionalities.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/kuksa_arch.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/kuksa_arch_sm.png"></a></p>

<h3>In-Vehicle Platform</h3>

<p>In order to cope with the various scenarios of a networked vehicle infrastructure, not only a sophisticated network and cloud architecture is required, but in particular a new type of hardware component in the vehicle.</p>

<p>On the one hand, this hardware component is intended to ensure bi-directional communication with the outside world across ECUs and, on the other hand, it also contains a wide variety of technologies to be able to carry out updates, upgrades, maintenance, or diagnostic work, and data exchange of any kind in a secure, error-free, authenticated and verified manner.</p>

<p>A reliable basis for such a component is Automotive Grade Linux (AGL), which has proven to be most suitable for the intended development activities compared to other platforms.</p>

<p>First implementations have already shown that the lightweight adapted AGL image can comprise a standardized Kuksa layer that grants access to required functions of the automotive domain. A key part of the implementations is developed by a separate security group, in which existing technologies are checked for security gaps and suitable methods are selected to protect vehicles from all kinds of attacks from the network.
Furthermore, the Kuksa vehicle component provides a runtime environment for verified applications.</p>

<p>Accordingly, new and individual functions can be managed similar to a smartphone but under stricter conditions such that new market potentials in the networked automotive sector can be accessed. One of the main incorporated methodologies is the secure access to telemetry data and vehicle functions. With the help of centralized and decentralized data acquisition of vehicle data and states, it is intended to support autonomous driving, and enable globally optimized traffic. For a future-proof infrastructure, it is crucial that the ecosystem basis and the associated protocols and communication technologies are used independently by manufacturers.</p>

<h3>IDE</h3>

<p>The IDE provided under Eclipse Kuksa not only support the development of applications for the vehicle component, but also the creation of applications for the cloud. Users should be able to choose between two different workspaces and technology stacks that contain the preconfigured and embedded APIs as well as software libraries of the respective applications to be developed. This allows the car to be equipped with new functions and new services to be deployed in the cloud.</p>

<p>Kuksa offers various APIs for implementing vehicle applications, a project template for cloud services, and wizards for easily providing vehicle applications in the App Store via the IDE. The extensive provision of the various APIs and libraries in the IDE enables accessing existing communication interfaces for the secure data transmission, storage, management, and authentication without having to take separate measurements for processing or interpreting the data.</p>

<p>Kuksa also supports the simplified deployment of new applications for both the cloud and vehicle components. This is provided by a pre-configured Eclipse Che stack, to which only the <i>address</i> of a target platform must be specified. Configuration, <i>building</i> and <i>deployment</i> can be done at the <i>push of a button</i> without further configuration or processing. Depending on the application, different development tools (e.g. <i>Logging, Debugging, Tracing,...</i>) can be included. Of course, <i>syntax highlighting</i>, <i>code completion</i>, and other necessary IDE functions are supported. For instance, the in-vehicle Eclipse Kuksa Che stack for AGL development activities features including Yocto based SDKs in order to support target specific programming shown in the screenshot below. After compiling and building software, specifying a target IP allows also the deployment process.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/Kuksa_IDE.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/Kuksa_IDE_sm.png"></a></p>

<p>In order to make new applications applicable to a greater amount of vehicles, applications need to be centrally checked, managed, and organized with regard to various in-vehicle derivatives and variants in such a way that only vehicle-appropriate applications are accessible. Similar to a Smartphone App Store, it has to be possible to add new functions and applications to their vehicle or perform updates or upgrades. Therefore, standardized interfaces of the in-vehicle and cloud platforms are required and they must offer the most diverse and yet simple infrastructure for vehicle owners. Authentication methods, security concepts, variant management, and suitable data transmission technologies in combination with the publicly accessible ecosystem form mandatory components as well as the difference to existing solutions.</p>

<h3>Cloud back-end</h3>

<p>The cloud back-end is the counterpart to the services provided by the in-vehicle platform. It offers basic services regarding connectivity, authentication, authorization, device update and data management. These services are realized by open source <a target="_blank" href="https://iot.eclipse.org/">Eclipse IoT</a> technologies, which are tailored to the requirements of a connected vehicle platform. An abstract overview of the cloud components is given in the Figure below.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/Kuksa_Cloud.png"></p>


<p>Central functionalities rely on a unified and secure communication between the back-end and the in-vehicle platform. In this regard, the connectivity for a large number of vehicles is realized using <a target="_blank" href="https://www.eclipse.org/hono/">Eclipse Hono</a>, which receives telemetry as well as event data and allows to transmit information to the vehicles. Open communication protocols such as AMQP and MQTT are accompanied by vehicle identity management.</p>

<p>The large amount of collected data can either be processed by big data analysis applications via streaming or persisted using database management systems. In addition, the state on the individual vehicles is managed using <a target="_blank" href="https://www.eclipse.org/ditto/">Eclipse Ditto</a>, a digital twin implementation, which allows the synchronization of their physical and virtual representations.</p>

<p>A device management component takes care of provisioning software updates to the vehicle, both in terms of the core system as well as adding or modifying vehicle functionality through apps. The former comprises so-called rollout management, which controls the distribution of the software to a large number of vehicles. This component is represented by <a target="_blank" href="https://www.eclipse.org/hawkbit/">Eclipse hawkBit</a>. Applications augmenting the functionality of the vehicles are provisioned by the vehicle manufacturers or external developers via an app store.</p>

<p>A set of core services provide the access to the vehicles managed within the cloud back-end. In this regard, an authentication and authorization component ensures that malicious access by third-party services is prohibited. This involves all major components of the back-end infrastructure.</p>

<h2>Next steps</h2>

<p><a target="_blank" href="https://projects.eclipse.org/projects/iot.kuksa">Eclipse Kuksa</a> is currently in incubation status and just recently got its initial contribution committed by the beginning of June (2018). Since then, committers are working on CQs in order to add appropriate third-party technologies and provide consistent implementations to be used and adapted conveniently. Documentations and tutorials are also planned and will include tutorials on how to build adapted AGL-Kuksa images for a Raspberry PI3 or other HW platforms supported by Yocto, transfer data to a cloud instance, and implement cloud or in-vehicle applications using the provided Eclipse Che Kuksa stack. With its origins in the ITEA3 research project <a target="_blank" href="https://itea3.org/project/appstacle.html">APPSTACLE</a>, some related documents, deliverables, and planned activities can also be found at the <a target="_blank" href="https://itea3.org/project/appstacle.html">research project’s ITEA website</a>.</p>

<p>In addition, various technologies such as new authentication or security methods as well as use case examples will be added to the corresponding repositories. For instance, a software rollout OBD scenario (shown in the following picture) is currently being developed.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/Kuksa_RollOut.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/Kuksa_RollOut_sm.png"></a></p>

<p>The scenario uses Eclipse hawkbit, a Kuksa-Application store, and various AGL-based layers and services, e.g. an AGL service implementing the W3C standard, or a download manager AGL service. A variety of device and application management components are also incorporated in order to cover the complete application provisioning process right up to the vehicle.</p>

<p>Finally, the APP4MC-Rover is also continuously being evolved with Eclipse Kuksa technologies. For example, an Eclipse Che instance has been extended with Yocto support to build and deploy AGL services to the Rover over-the-air. The comprehensive rover implementations and documentation are intended to be migrated to Eclipse Kuksa from <a target="_blank" href="https://github.com/app4mc-rover/rover-docs">GitHub Rover-Docs</a> and <a target="_blank" href="https://github.com/app4mc-rover/rover-services">GitHub Rover-Services</a>.</p> 

<p>Stay tuned <a target="_blank" href="https://www.eclipse.org/kuksa/">Eclipse Kuksa</a> for Automotive IoT &amp; Cloud Technologies!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/robert.jpg"
        alt="Robert Hoettger" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Robert Hoettger<br />
            <a target="_blank" href="www.fh-dortmund.de">Dortmund University of Applied Sciences and Arts</a>
          </p>
          <ul class="author-link list-inline">
          	<li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/rohoet">GitHub</a></li>
          	<li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/RobertHoert">Twitter</a></li>
          	<li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/rohoe/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/joerg.jpg"
        alt="Joerg Tessmer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Joerg Tessmer<br />
            <a target="_blank" href="https://www.bosch.com">Robert Bosch GmbH</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/joerg-tessmer-a3b72b83/">LinkedIn</a></li>
          </ul>
        </div>
      </div>
     </div>
	</div>
   <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/empty.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Tobias Rawald<br />
            <a target="_blank" href="https://www.bosch-si.com/corporate/home/homepage.html">Bosch Software Innovations GmbH</a>
          </p>
        </div>
      </div>
     </div>  
     <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/empty.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Joannes Kristan<br />
            <a target="_blank" href="https://www.bosch-si.com/corporate/home/homepage.html">Bosch Software Innovations GmbH</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
</div>