<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>The Arduino C++ extension for the Eclipse C/C++ IDE has been on the Eclipse Marketplace for a couple of years. It has been fairly popular consistently placing in the top 100 entries. For the Eclipse Photon release, we decided it was ready for prime time and it is now a full member of the Eclipse C/C++ IDE. <a target="_blank" href="http://eclipse.org/download">Download and install the IDE</a> and you’re ready to go.</p>

<p>This article will walk through how to use the Eclipse C/C++ IDE to work with your Arduino boards. If you have used previous versions, this will mostly be a refresher. There is one major improvement on how you view the output of your board. If you haven’t used it and want to use the professional features of the IDE that you don’t get in the official Arduino IDE, then you’re in for a treat.</p>


<h2>Arduino Downloads Manager</h2>
<p>The IDE does not ship with the toolchains and SDKs needed to build Arduino sketches. You can use the Arduino Downloads Manager available in the Help menu.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/download_manager.png"></p>

 <p>With the Packages tab selected, click Add to see the list of available packages. An Arduino package supports a collection of boards. Hold your mouse over the platform name to see the list of boards supported by this package. In my case, I have an Arduino Uno which is supported by the Arduino AVR Boards package. Check it and click OK to install which can take a few minutes.</p>

<p>If you don’t see a package that matches your board, there are others available. <a target="_blank" href="https://github.com/arduino/arduino/wiki/unofficial-list-of-3rd-party-boards-support-urls">A list is maintained on the Arduino wiki</a>. You can copy the URL to the package index JSON file and add it on the C/C++ Arduino preference page.</p>
 
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/url.png"></p>
 
<p>Once added, if you open the list of Packages again, you will see the packages offered from that site.</p>

<h2>New Arduino Project</h2>
<p>Use the New C/C++ Project wizard to create a new Arduino C++ Sketch project. This creates a simple project with a single source file populated with the familiar setup and loop functions.</p>


 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/newproject.png"></p>
 
 <h2>New Arduino Launch Target</h2>
<p>Before we can build and launch the project, we need to tell the IDE about the board we’re using. For this, we use the Eclipse Launch Bar which is now a regular part of the Eclipse C/C++ IDE. It is composed of three buttons and three selectors. The selectors determine the launch mode, what to launch, and where to launch it or the launch target. And it does read as a sentence. The three buttons on the left are for Build, Launch, and Stop.</p>
 
  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/run.png"></p>
 
 <p>Click on the third selector and select New Launch Target in the drop-down. Select Arduino as the target type then enter a name, the serial port, and the board type. There may be additional parameters to set based on the board type. The Uno is pretty simple. Also, you can ignore the Programmer setting for now since we’re using the standard flash writer to program the device.
 </p>
 
   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/new_target.png"></p>
 

<h2>Terminal</h2>
<p>New for Eclipse Photon is an adapter to allow the Terminal to work with Arduino boards. Click on the Terminal toolbar button to create a new Terminal. Select Arduino Serial Monitor for the terminal type, select the board and set the communication parameters you’d like to use for the Terminal.</p>

   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/launch_terminal.png"></p>
 
<p>You can drag the terminal over to the side so that you can have the Console and Terminal open at the same time. You’ll be using these two views extensively while working with your board.</p>

<h2>Build</h2>
<p>To test out our setup, we’ll enter in a standard Hello World program that sends a message out every 200 ms with an incrementing counter.</p>

   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/helloworld_code.png"></p>
 
<p>Now hit the Build button in the Launch Bar and watch the output from the build on the Console. At the end, you’ll see a message showing how much program store and initial RAM your program uses.</p>

<h2>Launch</h2>
<p>Finally, hit the Launch button in the Launch Bar. If you had a previous program printing the Terminal, it’ll stop and control of the serial port handed over to the flash writer program. The download only takes a few seconds and you’ll then see the Terminal start up and the output from your new program appear.</p>
 
   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/helloworld.png"></p>
 
<h2>Conclusion</h2>
<p>Once you have your tools set up, the world of Arduino is available in your Eclipse C/C++ IDE. You can check out the Arduino Download Manager again and click on the Libraries tab to download one of the hundreds of libraries available from the community to help program the electronics project of your dreams. And while you’re at it, you get to use the professional features of the Eclipse C/C++ IDE such as content assist and source navigation to help you take advantage of the power of this little platform. Enjoy!</p>
 
 
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/doug.jpg"
        alt="Doug Schaefer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Doug Schaefer<br />
            <a target="_blank" href="https://blackberry.qnx.com/en">QNX</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://cdtdoug.ca/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/dougschaefer">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>