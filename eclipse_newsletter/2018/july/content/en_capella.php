<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Designing complex and critical systems, and more generally architectures that are subject to multiple functional and non-functional constraints, is an activity which requires a level of rigor that can only be provided by formalized and tooled modeling approaches.</p>

<p>The Arcadia methodology and the corresponding open source tool <a target="_blank" href="https://www.google.com/url?q=https://www.polarsys.org/capella/&sa=D&ust=1531742049169000&usg=AFQjCNFjar1vpGGxneay31htz6oBGoudIA">Capella</a> have been developed with these challenges in mind. With Arcadia as its DNA, the Capella workbench provides methodological guidance, including an engineering workflow with clear modeling objectives, and an advanced set of productivity tools. Successfully deployed across various industrial domains, Capella is a field-proven solution which helps manage systems complexity, primarily during the development phases, but also during the production and operations phases.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/phases.png"></p>

<p>As a way of extending its adoption beyond Thales, Capella was made open source in 2014 as part of PolarSys, an Eclipse Foundation Working Group that is dedicated to embedded systems development.</p>

<p>Among the standards from which Arcadia and Capella are inspired, the SysML language is definitely prominent. To the extent that Arcadia/Capella can be considered a SysML-like solution to design the architecture of complex systems using models:</p>

    <ul>
    		<li>SysML is a standard and a general-purpose language for systems modeling covering a large spectrum of applications.</li> 
		<li>Arcadia/Capella primarily focuses on the design of systems architectures (functional analysis, identification of subsystems, definition and justification of interfaces, early evaluation).</li>
    </ul> 


<p>The Arcadia/Capella solution capitalizes on the SysML concepts that it simultaneously simplifies and enriches. As a consequence, the Arcadia method is supported by various kinds of diagrams (Architecture, Dataflow, Functional chains, Sequence, Tree, Mode and States, Classes and Interfaces) which are largely inspired by UML and SysML diagrams (Block Definition, Internal Block, Activity, Sequence, State Machine, Use Case, Requirement , Parametric).</p>

<p>For example, when the Internal Block Diagram in SysML captures the internal structure of a block in terms of properties and connectors between properties, Arcadia/Capella offers a twin diagram, the Architecture Diagram, which describes the assembly of components in terms of internal breakdown and connections.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/sysml_internal_block.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/sysml_internal_block_sm.png"></a></p>
<p align="center"><i>SysML Internal Block Diagram</i></p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/arcadia_capella_architecture.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/arcadia_capella_architecture_sm.png"></a></p>
<p align="center"><i>Arcadia/Capella Architecture Diagram</i></p>

<p>The purpose of Arcadia and Capella is to have systems engineers embrace the cultural change of MBSE, rather than having modeling “experts” owning the model on behalf of systems engineers. The main differences between SysML and Capella/Arcadia are motivated by the commitment to reach this goal and result from the integration of the actual practices and concerns of systems engineers who do not necessarily have software engineering background.</p>

<p>For example, functional analysis is a classical technique broadly used by systems engineers. Arcadia and Capella provide methodological guidance and engineering helpers to support this technique that has been mostly left out of SysML V1. While the mapping of Capella functions to SysML activities is the most natural one in terms of semantics, there are a few fundamental differences between them.</p>  

<p>In particular, the strong encapsulation mechanism of SysML favors the reuse of activity definitions in multiple contexts but also enforces a top-down approach and imposes strong constraints on what can be represented in one single diagram.</p> 

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/functional_analysis.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/functional_analysis_sm.png"></a></p>
<p align="center"><i>Several diagrams required to perform a functional analysis</i></p>

<p>To facilitate the implementation of flexible workflows (top-down and bottom-up), Arcadia/Capella relies on a different approach in order to:</p>
    <ul>
        <li>help manage the complexity of functional trees by relieving engineers from the tedious task of maintaining the consistency of dependencies between several levels of decomposition</li>
        <li>allow the immediate production of simplified views of the functional analysis</li>
        <li>enable the easy combination between top-down and bottom-up workflows</li>
    </ul>
    
<p>The two diagrams below are an example of two views automatically computed from the same diagram. Ports are displayed on non-leaf functions but still belong to children functions.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/leaf_and_nonleaf_functions.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/leaf_and_nonleaf_functions_sm.png"></a></p>
<p align="center"><i>Diagram displaying leaf and non-leaf functions</i></p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/hidden_leaf_function.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/hidden_leaf_function_sm.png"></a></p>
<p align="center"><i>The same diagram hiding leaf functions</i></p>

<p>Many other equivalences and differences between SysML and Arcadia/Capella are explained in more details on this article: <a target="_blank" href="https://www.polarsys.org/capella/arcadia_capella_sysml_tool.html">https://www.polarsys.org/capella/arcadia_capella_sysml_tool.html</a></p>

<p>A webinar (45 minutes) is dedicated to this topic:</p>

<div class="embed-responsive embed-responsive-16by9">
<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qFfj8K3uj3Y" frameborder="0" allowfullscreen></iframe>
</div>
<br>

<p>For more in-depth information about Capella, its release, the upcoming events including monthly webinars, etc.: join the discussion by following the <a target="_blank" href="https://twitter.com/capella_arcadia">Capella Twitter account</a>, the <a target="_blank" href="https://www.youtube.com/channel/UCfgwbb2h10V3tgJ59sbGBnQ">Capella Youtube Channel</a> and the <a target="_blank" href="https://www.linkedin.com/groups/8605600">Capella LinkedIn Group</a>.</p>
<p>Feel also free to check the <a target="_blank" href="http://www.polarsys.org/capella/">Capella website</a>!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/stephane.jpeg"
        alt="Stephane Bonnet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Stéphane Bonnet<br />
            <a target="_blank" href="https://www.thalesgroup.com/en">Thales</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/stephane-bonnet-946703/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
      <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/samuel.jpg"
        alt="Samuel Rochet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Samuel Rochet<br />
            <a target="_blank" href="http://obeo.fr/">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/samuel_rochet">Twitter</a></li>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>