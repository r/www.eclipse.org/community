<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<img align="left" class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/papyrus_small.png">

<p>The generic Papyrus UML has not been designed and streamlined for novice UML users, students, and software engineers who only use a basic subset of UML concepts. Such users can be easily scared by the number of menu items and commands representing all the existing UML concepts.s a result, they often lose interest in Papyrus before even getting to know the potential of the Papyrus platform and its ecosystem. Based on the success of Papyrus-IM, the Papyrus IC has decided to invest in the development of a new version of Papyrus, called Papyrus UML Light, targeted to these users. Papyrus UML Light will be a standalone, simplified and streamlined modeling tool, built on the Papyrus platform, that aims at providing an optimized experience for the targeted users. It is based on the subset of the most commonly used UML concepts and provides a set of customization (palette tools, new child menus, property sheets, and wizards for creating models for the most common UML concepts) to simplify the overall modeling environment and improve the user experience. The set of selected UML concepts is based on the OMG Certified UML Professional 2TM (OCUP 2 TM) Foundation Level.</p> 

<p><a target="_blank" href="https://www.eclipse.org/papyrus/">Eclipse Papyrus</a> is known as an open-source UML modeling tool, but it’s also a platform for creating domain- and methodology-specific UML-based domain-specific modeling tools. That’s why, the Papyrus UML modeling tool is not streamlined for any particular methodology, user group, or domain, but is rather a pure implementation of the UML metamodel, exposing all its concepts and features without particular guidance. The generic and pure implementation of the UML metamodel in the Papyrus UML platform is on purpose. Papyrus UML is not meant to be optimized for a particular user group or application domain. The goal is for it to act as a basis for users/toolsmiths creating domain- or methodology-specific, streamlined modeling tools based on the Papyrus platform.</p>
 
<p>The development of Papyrus UML Light leverages the success of the development of Papyrus for Information Modeling (<a target="_blank" href="https://wiki.eclipse.org/Papyrus_for_Information_Modeling">Papyrus-IM</a>) by EclipseSource and CEA, funded by the Papyrus Industry Consortium (<a target="_blank" href="https://www.polarsys.org/papyrus-ic">Papyrus IC</a>). Indeed, these initiatives demonstrate how the Papyrus platform can be customized and simplified to fit the needs of a specific domain. In this case, Papyrus is reduced to only a subset of UML concepts (around class diagrams) required for information modeling. It includes customization of the available palette tools, new child menus, property sheets, and wizards for creating models, to provide an optimized experience for information modelers. Since its development, Papyrus-IM has been mainly used as a simplified version of Papyrus for information modeling.</p>
 
<p>The development of Papyrus UML Light will also include a user documentation helping novice users (new to UML and/or Papyrus) during their learning phases. This documentation will be available publically via the web inside the Eclipse Foundation’s infrastructure (wiki and/or help pages) and will also be shipped inside the Papyrus UML Light tool.</p>
 
<p>We strongly believe that this simplified UML tool could then be recommended to new users and be used for teaching and training.</p>
 
<p>The goal is to release the initial version of Papyrus UML Light at <a target="_blank" href="https://www.eclipsecon.org/europe2018/">EclipseCon Europe 2018</a>, October 23-25. If you’re planning to attend the conference, find us to talk about it!</p>
   
   

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/philip.jpeg"
        alt="Philip Langer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Philip Langer<br />
            <a target="_blank" href="https://eclipsesource.com/">EclipseSource</a>
          </p>
          <ul class="author-link list-inline">
		 <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/planger">GitHub</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/philchip23">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
     <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/francis.jpg"
        alt="Francis Bordeleau" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Francis Bordeleau<br />
            <a target="_blank" href="http://cmind.io/">Cmind Inc</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/francis-bordeleau-b2aa273/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>