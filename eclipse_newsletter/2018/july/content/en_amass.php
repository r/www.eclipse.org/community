<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Embedded systems have significantly increased in technical complexity towards open, interconnected cyber-physical systems (CPSs), exacerbating the problem of ensuring dependability properties such as safety and security. CPSs integrate software applications interacting with the physical world at increasingly higher levels of autonomy, which will allow them to drive on our roads, fly over our heads, move alongside us in our daily lives, and work independently in our factories soon. In industrial domains such as aerospace, space, automotive, industrial automation, and railway, CPSs are subject to rigorous assurance processes as a way to ensure they do not pose undue risk to people or property. The dependability of the systems must also often be certified by an independent "certification authority" as a formal recognition.</p>

<p>Present-day assurance processes, as prescribed by international standards such as IEC 61508 and ISO 26262 for functional safety, provide guidance on how to evaluate and mitigate risks. Complying with these standards typically results in cumbersome, time-consuming, and paper-intensive processes to provide convincing and valid justifications of system dependability, which may preclude CPSs deployment, particularly for SMEs.</p>
 
<p>This article presents the toolchain developed by the partners of the AMASS project, integrated under the umbrella of the <a target="_blank" href="https://www.polarsys.org">PolarSys</a> <a target="_blank" href="https://www.polarsys.org/opencert">OpenCert project</a> to increase the efficiency of assurance and certification activities and to lower certification costs in face of rapidly changing product features and market needs.</p>

<p>The <b>OpenCert ecosystem</b> is an advanced integrated solution for assurance and certification of CPSs that will allow different stakeholders (engineers, assessors, tool vendors, etc.) to more easily and better perform their work. Starting from the results of the <a target="_blank" href="http://opencoss-project.eu/">OPENCOSS project</a> and its resulting OpenCert solution, it leverages several Eclipse and PolarSys projects including <a target="_blank" href="https://www.eclipse.org/papyrus/">Eclipse Papyrus</a> and its <a target="_blank" href="https://www.polarsys.org/projects/polarsys.chess">CHESS extension</a>, <a target="_blank" href="https://www.eclipse.org/epf/">EPF Composer</a> (Eclipse Process Framework), <a target="_blank" href="https://projects.eclipse.org/projects/modeling.capra">Eclipse Capra</a>, and <a target="_blank" href="http://www.eclipse.org/cdo/">Eclipse CDO</a>.</p>

<h2>What does the PolarSys OpenCert platform provide? Let’s look at a typical usage scenario</h2>
<p>To deal with the technical CPS complexity and the associated labor-intensive activities for assurance and certification of these systems, the tools use model-based approaches and incremental techniques.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/opencert_graph.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/opencert_graph_sm.png"></a></p>

<p>A typical usage scenario consists of five macro-phases:</p>
<p><b>Assurance Project Management</b>: This functionality, provided by OpenCert, includes project lifecycle aspects such as the creation of assurance projects and any project baseline information that may be shared by the different functional modules. This module manages a "project repository", which can be accessed by the other tool modules and can include information reused from previous projects. It also includes Capra for management of traceability between assurance assets.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/assurance_assets.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/assurance_assets_sm.png"></a></p>
 
<p><b>Process Definition</b>: This feature supports the management of knowledge about standards (e.g. IEC 61508, DO-178C, ISO 26262, EN 50128/50126/50129, and IEC 62443, among others), regulations, and interpretations, in a form that can be stored, retrieved, categorized, associated, searched, and browsed. It also supports the definition of development and assurance processes as well as the tailoring of particular project plans for assurance. In addition, these tools allow engineers to track where they are with respect to their duties to conform to standards, helping them to see the effective progress of the work and the level of compliance. OpenCert and EPF Composer provide support for this functionality.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/opencert_composer.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/opencert_composer_sm.png"></a></p>

<p><b>System Design and Analysis</b>: This feature manages system architecture specification by decomposing a system into components. It also includes mechanisms to support compositional assurance, contract-based system specification, and architectural pattern management. It allows engineers to browse information about the architecture of the system and how the entities of the architecture can be related to an assurance case. Most of the functionality is supported by Papyrus and CHESS extensions.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/opencert_example.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/opencert_example_sm.png"></a></p>

<p><b>Assurance Case Management</b>: This feature manages argumentation information in a modular fashion. It also includes mechanisms to support assurance pattern management. Assurance cases are a structured form of an argument that specifies a convincing justification that a system is adequately dependable for a given application in a given environment. Assurance cases are modeled as connections between claims, sub-claims, and their evidence. The OpenCert editor for argumentation is the mains basis of this feature. Assurance case fragments can be generated from system models in CHESS and from process models in EPF Composer.</p>
 
 <p align="center"><a href="/community/eclipse_newsletter/2018/july/images/arg_diagram.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/arg_diagram_sm.png"></a></p>
 
<p><b>Evidence Management</b>: This feature deals with the specification of the actual artifacts that are used as evidence in an assurance project. The artifacts can have specific properties (e.g. the result of a test case) and can be stored in external data tools (e.g. DOORS for a requirement). All these aspects are managed throughout an artifact’s lifecycle, which can include changes to an artifact and evaluations (e.g. about the completeness of a document). The OpenCert evidence editor supports this functionality.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/july/images/resource_set.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/july/images/resource_set_sm.png"></a></p>

<p>The toolchain is further integrated with external tools for systems and software engineering, e.g. <a target="_blank" href="https://www.ibm.com/us-en/marketplace/rational-rhapsody">IBM Rhapsody</a> for system modeling, <a target="_blank" href="https://www.reusecompany.com/requirements-quality-suite">Requirements Quality Suite</a> for requirements engineering, and <a target="_blank" href="https://www.all4tec.com/safety-architect">Safety Architect</a> and <a target="_blank" href="https://www.all4tec.com/cyber-architect">Cyber Architect</a> for system dependability analysis. In the context of the AMASS project, the above features are being applied and validated in 11 case studies dealing with novel characteristics of aircrafts, cars, control systems, satellites, and trains.</p>

<h2>Conclusion</h2>
<p>The main benefits of the PolarSys OpenCert ecosystem platform can be outlined as follows:</p>
	<ul>     	
        <li><b>Reduced Initial &amp; Rework Costs</b>: guidance for compliance with standards and regulations, and for reuse of assurance and certification assets, help for engineers to more efficiently execute assurance projects.</li>
        <li><b>Better Coping with Risks</b>: deployment of safety and security analyses, and of cost-effective and transparent assurance and certification processes, improvement in risk management.</li>
        <li><b>Harmonized compliance</b>: the OpenCert ecosystem helps engineers create a transparent view of the process and product quality against a set of harmonized requirements derived from standards and regulations.</li>
        	<li><b>Reduced compliance management and (re) certification costs</b>: through the use of existing knowledge, quantitative methods, and modular reuse techniques, the OpenCert ecosystem reduces these costs.</li>
	</ul>	

<p>In summary, the <b>OpenCert ecosystem</b> is an advanced integrated solution for assurance and certification of CPSs that will allow different stakeholders (engineers, assessors, tool vendors, etc.) to more easily and better perform their work.</p> 

<p>More information about the ecosystem and the underlying approach can be found at <a target="_blank" href="https://www.amass-ecsel.eu/">https://www.amass-ecsel.eu/</a>.  Videos demonstrating the latest features of the OpenCert platform can be found at <a target="_blank" href="https://amass-ecsel.eu/content/demos">https://amass-ecsel.eu/content/demos</a>. The project consortium already provides prototypes that can be used for testing purpose and is eager to help early adopters of the technology.</p>

<h2>More details about AMASS</h2>
<p>As the result of various European collaborative projects involving both industry and academia, including <a target="_blank" href="http://www.chess-project.org/">CHESS</a>, <a target="_blank" href="http://www.opencoss-project.eu/">OPENCOSS</a>, <a target="_blank" href="http://www.safecer.eu/">SafeCer</a>, and now <a target="_blank" href="https://www.amass-ecsel.eu/">AMASS</a>, a large consortium that is creating and consolidating an ecosystem of open tools and a community for assurance and certification of CPSs.</p>

<p>AMASS has received funding from the Electronic Component Systems for European Leadership Joint Undertaking under grant agreement No 692474. This Joint Undertaking receives support from the European Union’s Horizon 2020 research and innovation programme and Spain, Czech Republic, Germany, Sweden, Italy, United Kingdom and France.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/huascarespinoza.jpg"
        alt="Huascar Espinoza " />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Huáscar Espinoza <br />
            <a target="_blank" href="http://www-list.cea.fr/en/">CEA-List</a>
          </p>
        </div>
      </div>
     </div>  
         <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/joseluisdelavara.png"
        alt="Jose Luis de la Vara" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jose Luis de la Vara<br />
            <a target="_blank" href="https://www.uc3m.es/Home">Universidad Carlos III de Madrid</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
     <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/garazijuez.jpeg"
        alt="Garazi Juez" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Garazi Juez<br />
            <a target="_blank" href="https://www.tecnalia.com/en/">TECNALIA Research & Innovation</a>
          </p>
        </div>
      </div>
     </div>  
         <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/cristinamartinez.jpg"
        alt="Cristina Martinez" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Cristina Martinez<br />
            <a target="_blank" href="https://www.tecnalia.com/en/">TECNALIA Research &amp; Innovation</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
     <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/barbaragallina.jpg"
        alt="Barbara Gallina" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Barbara Gallina<br />
            <a target="_blank" href="http://www.mdh.se/">Mälardalens Högskola</a>
          </p>
        </div>
      </div>
     </div>  
         <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/stefanopuri.jpg"
        alt="Stefano Puri" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Stefano Puri<br />
            <a target="_blank" href="http://www.intecs.it/">Intecs</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
     <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/silviamazzini.jpg"
        alt="Silvia Mazzini" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Silvia Mazzini<br />
            <a target="_blank" href="http://www.intecs.it/">Intecs</a>
          </p>
        </div>
      </div>
     </div>  
         <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/july/images/gaelb.jpg"
        alt="Gael Blondelle" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Gaël Blondelle<br />
            <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
</div>