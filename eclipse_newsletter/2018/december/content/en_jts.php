<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Early in the development of the <strong><a href="https://projects.eclipse.org/projects/locationtech.jts">JTS Topology Suite</a></strong>, it became apparent there was a need to easily create and view geometric data, as well as run JTS geometry algorithms and inspect the results. This led to the development of the <strong>JTS TestBuilder</strong> application, a graphical user interface for interacting with JTS in a visual way. (A secondary motive was to explore how well the at-that-time new Swing and Java2D APIs supported rendering geometry. This experience led to the development of the JUMP Unified Mapping Platform - but that's another story.) After many years of development, the TestBuilder has become a full-featured multitool for working with geometry. </p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder.PNG"></p>
<p align="center"><em>The JTS TestBuilder user interface</em></p>
	<p>The TestBuilder is designed around the concept of Test Cases. A test case has one or two input geometries (called A and B). Many different kinds of operations can be performed on inputs, producing either geometric or scalar results. Test cases can be created by entering input geometry, loading a file in JTS TestRunner XML format, or by copying existing cases. </p>
<h3>Geometry Support</h3>
	<p>Geometry can be created by manual drawing, using the <strong>Rectangle, Polygon, LineString and Point</strong> tools. Drawing tools support placing individual vertices by clicking or dragging to create "stream digitized" linework. Geometry can be entered by pasting textual data in the formats supported by JTS (such as WKT, WKB Hex, GML and GeoJSON). Data files can be loaded in any of the text formats or the venerable but still-common ESRI shapefiles. Spatial editing tools allow modifying geometry linework and vertices. Input geometry is visible in the spatial view, and also as WKT in the text views. The <strong>Inspector</strong> view shows a detailed view of the hierarchical structure of geometry, including components, line segments, and vertices, along with indices and metrics of counts, area, and length.</p>
	<p>Information about geometry structure and topology can be graphically depicted using styling options. Styles include showing vertices, labeling line segments with their positional index, and displaying segment direction arrows. To aid in distinguishing polygonal components polygon fill can be varied using a few different strategies. A useful aid for visualization of very small differences in geometry linework is the <strong>RevealTopology</strong> mode, which "stretches" nearby vertices and line segments. The display can be saved as an image, which is useful for illustrating technical discussions and issue reports. </p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder-RevealTopology-off.PNG"></p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder-RevealTopology-on.PNG"></p>
	<p align="center"><em>Reveal Topology mode in action</em></p>
<h3>Predicates and Validation</h3>
	<p>Two of the most commonly-used functions of the JTS API are computing spatial relationships (binary predicates) and performing validation of geometry topology. In the TestBuilder these are supported by dedicated user interface panels. The panels allow executing predicates and validation and present the results in an easily-understandable visual layout. In the case of validation, the nature and location of errors are indicated by graphic markers displayed on the geometry view.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder-predicates.PNG"></p>
	<p align="center"><em>Spatial Predicate display panel</em><p> 
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder-validation.PNG"></p>
	<p align="center"><em>Spatial Predicate display panel</em><p>
<h3>Geometry Functions</h3>
	<p>TestBuilder provides a wide variety of geometric functions which can be run on test cases. Every Geometry method is available, and many of the API classes as well. Functions are either unary or binary in terms of their geometry inputs and area categorized as Geometry-valued and Scalar-valued. Geometric results are displayed in the spatial view and as WKT. They can easily be copied to new test cases, to support executing chains of operations. Scalar results are displayed as text. Recently added to the Testbuilder are meta-functional controls, which allow executing a function on each component of an input geometry and executing functions over a sequence of parameter values. This supports performance and stress testing of geometry functionality. To allow performance evaluation, execution time and memory usage are displayed when a function is executed, and a session log is kept.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/TestBuilder-Geometry-function.PNG"></p>
	<p align="center"><em>Executing a buffer function</em><p>
<h3>Function Extensibility</h3>	
	<p>Functions are implemented as Java class static methods and can be loaded dynamically from the TestBuilder command line. This allows using the TestBuilder as a test harness for JTS spatial functionality during development, and in fact for any Java spatial API (with conversions to JTS Geometry classes provided). The combination of geometry-valued functions and the TestBuilder visualization capability is quite versatile and provides a useful "drop-in" visual debugging tool for Java spatial development.</p>
<h3>Future Directions</h3>
	<p>The TestBuilder continues to evolve to provide more capability for geometric manipulation and visualization. Ideas for future development include multiple layer support, more kinds of styling and symbolization, and better options for displaying custom graphical elements such as labels and titles</p>
	<p>The TestBuilder has played an essential role in the development of JTS, and it may be worth adding to your toolbelt as well.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/december/images/martin_davis.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Martin Davis<br>
            <a target="_blank" href="https://www.vividsolutions.com/">Senior Geospatial Architect at Vivid Solutions</a>
            <br>
          </p>
          <p>Martin Davis has been an open source geospatial developer for over 15 years, primarily in Java. He has a particular interest in geometric algorithms, and is the project lead of the widely-used JTS Topology Suite library.</p>
           <?php // echo $og; ?>
        </div>
      </div>
     </div>  
   </div>
</div>