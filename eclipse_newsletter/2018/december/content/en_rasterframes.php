<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h3>Background</h3>
	<p>Human beings have a millennia-long history of organizing information in tabular form. Typically, rows represent independent events or observations, and columns represent measurements from the observations. The forms have evolved, from hand-written agricultural records and transaction ledgers, to the advent of spreadsheets on the personal computer, and on to the creation of the DataFrame data structure as found in <a href="https://www.rdocumentation.org/packages/base/versions/3.5.1/topics/data.frame">R Data Frames</a> and <a href="https://pandas.pydata.org/">Python Pandas</a>. The table-oriented data structure remains a common and critical component of organizing data across industries, and is the mental model employed by many data scientists across diverse forms of modeling and analysis.</p>
	<p>Today, DataFrames are the lingua franca of data science. The evolution of the tabular form has continued with Apache Spark SQL, which brings DataFrames to the big data distributed compute space. Through several novel innovations, Spark SQL enables interactive and batch-oriented cluster computing without having to be versed in the highly specialized skills typically required for high-performance computing. As suggested by the name, these DataFrames are manipulatable via standard SQL, as well as the more general-purpose programming languages Python, R, Java, and Scala.</p>
	<p>RasterFrames®, an incubating Eclipse Foundation LocationTech project, brings together Earth-observing (EO) data analysis, big data computing, and DataFrame-based data science. The recent explosion of EO data from public and private satellite operators presents both a huge opportunity as well as a challenge to the data analysis community. It is Big Data in the truest sense, and its footprint is rapidly getting bigger. According to a World Bank document on assets for post-disaster situation awareness <a href="#tagcontent">[^1]</a>:</p>
	<p style="color:gray; padding-left:20px"><em>Of the 1,738 operational satellites currently orbiting the earth (as of 9/[20]17), 596 are earth observation satellites and 477 of these are non-military assets (ie available to civil society including commercial entities and governments for earth observation, according to the Union of Concerned Scientists). This number is expected to increase significantly over the next ten years. The 200 or so planned remote sensing satellites have a value of over 27 billion USD (Forecast International). This estimate does not include the burgeoning fleets of smallsats as well as micro, nano and even smaller satellites... All this enthusiasm has, not unexpectedly, led to a veritable fire-hose of remotely sensed data which is becoming difficult to navigate even for seasoned experts.</em></p>
	<p>RasterFrames provides a DataFrame-centric view over arbitrary EO data, enabling spatiotemporal queries, map algebra raster operations, and compatibility with the ecosystem of Spark ML algorithms. By using DataFrames as the core cognitive and compute data model, it is able to deliver these features in a form that is accessible to general analysts while handling the rapidly growing data footprint.</p>
<h3>Architecture</h3>
	<p>RasterFrames takes the Spark SQL DataFrame and extends it to support standard EO operations. It does this with the help of several other LocationTech projects: <a href="https://geotrellis.io/">GeoTrellis</a>, <a href="https://www.geomesa.org/">GeoMesa</a>, <a href="https://github.com/locationtech/jts">JTS</a>, and <a href="https://github.com/locationtech/sfcurve">SFCurve</a>.
	<p align="center"><img class="img-responsive" style="max-width:60%" src="/community/eclipse_newsletter/2018/december/images/stack.jpg"></p>
	<p>RasterFrames introduces a new native data type called <code>tile</code> to Spark SQL. Each tile cell contains a 2-D matrix of "cell" (pixel) values, along with information on how to numerically interpret those cells. A "RasterFrame" is a Spark DataFrame with one or more columns of type <code>tile</code>. A <code>tile</code> column typically represents a single frequency band of sensor data, such as "blue" or "near infrared", discretized into regular-sized chunks, but can also be quality assurance information, land classification assignments, or any other discretized geo-spatiotemporal data. It also includes support for working with vector data, such as <a href="https://en.wikipedia.org/wiki/GeoJSON">GeoJSON</a>. Along with <code>tile</code> columns there is typically a <code>geometry</code> column (bounds or extent/envelope) specifying the location of the data, the map projection of that geometry (<code>crs</code>), and a <code>timestamp</code> column representing the acquisition time. These columns can all be used in the <code>WHERE</code> clause when querying a catalog of imagery.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/december/images/anatomy.png"></p>
	<p>Raster data can be read from a number of sources. Through the flexible Spark SQL DataSource API, RasterFrames can be constructed from collections of (preferably Cloud Optimized) GeoTIFFs, <a href="https://geotrellis.readthedocs.io/en/latest/guide/core-concepts.html#layouts-and-tile-layers">GeoTrellis Layers</a>, and from an experimental catalog of Landsat 8 and MODIS data sets on the <a href="https://registry.opendata.aws/modis/">Amazon Web Services (AWS) Public Data Set (PDS)</a>. We are also experimenting with support for the evolving <a href="https://github.com/radiantearth/stac-spec">Spatiotemporal Asset Catalog (STAC)</a> specification.</p>
	<p align="center"><img class="img-responsive" style="max-width:60%" src="/community/eclipse_newsletter/2018/december/images/datasources.png"></p>
<h3>Using RasterFrames</h3>
	<p>The following example shows some of the general operations available in RasterFrames. As an imagery target we will use the <a href="https://lpdaac.usgs.gov/dataset_discovery/modis/modis_products_table/mcd43a4_v006">MODIS Nadir BRDF-Adjusted Surface Reflectance Data Product</a> from NASA, which is available as GeoTIFFs through AWS PDS. The example extracts data using the RasterFrames MODIS catalog data source and manipulates it via SQL (as noted above, Python, Java, and Scala are also options). We will compute the monthly global average of a <a href="https://en.wikipedia.org/wiki/Normalized_difference_vegetation_index">vegetation index</a> in 2017 and see how it varies over the year.</p>
	<p style="color:gray; padding-left:20px"><em>Note: RasterFrames version 0.8.0-RC1 was used in this example.</em></p>
	<p>The first step is to load the MODIS catalog data source into a table and take a look a the schema:</p>
<pre>
CREATE TEMPORARY VIEW modis USING `aws-pds-modis`;
DESCRIBE modis;
-- +----------------+------------------+
-- |col_name        |data_type         |
-- +----------------+------------------+
-- |product_id      |string            |
-- |acquisition_date|timestamp         |
-- |granule_id      |string            |
-- |gid             |string            |
-- |assets          |map&lt;string,string&gt;|
-- +----------------+------------------+
</pre>
	<p>The assets column contains a dictionary mapping band names to URIs holding the location of each GeoTIFF. To determine what bands are available in the catalog we execute the following:</p>
<pre>
<code style="color:red">SELECT DISTINCT</code> explode(map_keys(assets)) <code style="color:red">as</code> asset_keys
<code style="color:red">FROM</code> modis
<code style="color:red">ORDER BY</code> asset_keys
-- +-------------+
-- |   asset_keys|
-- +-------------+
-- |          B01|
-- |        B01qa|
-- |          B02|
-- |        B02qa|
-- |          ...|
-- +-------------+
</pre>
	<p>From the <a href="https://vip.arizona.edu/documents/MODIS/MODIS_VI_UsersGuide_June_2015_C6.pdf">data product's user manual</a> we find that <code>B01</code> and <code>B02</code> assets map to the red and NIR bands. From there we create a query reading the red and NIR band data on the (arbitrarily chosen) 15th of each month in 2017. This will give us 12 global coverages from which we will compute our statistics.</p>
<pre>
CREATE TEMPORARY VIEW red_nir_tiles_monthly_2017 <code style="color:red">AS</code>
<code style="color:red">SELECT</code> granule_id, month(acquisition_date) <code style="color:red">as</code> month, 
       f_read_tiles(assets['B01'], assets['B02']) <code style="color:red">as</code> (crs, extent, red, nir)
<code style="color:red">FROM</code> modis
<code style="color:red">WHERE</code> year(acquisition_date) <code style="color:red">=</code> <code style="color:blue">2017</code> <code style="color:red">AND</code> day(acquisition_date) <code style="color:red">=</code> <code style="color:blue">15</code>;
DESCRIBE red_nir_tiles_monthly_2017;
-- +--------+-------------------------------------------------------+
-- |col_name|data_type                                              |
-- +--------+-------------------------------------------------------+
-- |month   |int                                                    |
-- |crs     |struct&lt;crsProj4:string&gt;                  		    |
-- |extent  |struct&lt;xmin:double,ymin:double,xmax:double,ymax:double&gt;|
-- |red     |tile                                                   |
-- |nir     |tile                                                   |
-- +--------+-------------------------------------------------------+
</pre>
	<p>Computing the <a href="https://en.wikipedia.org/wiki/Normalized_difference_vegetation_index">normalized difference vegetation index</a> (NDVI) is a very common operation in EO analysis, and is calculated simply as the normalized difference of the Red and NIR bands from a surface reflectance data product:</p>
	<p align="center"><img class="img-responsive" style="max-width:50%" src="/community/eclipse_newsletter/2018/december/images/ndvi.png"></p>
	<p>Since a normalized difference is such a common operation in EO analysis, RasterFrames includes the function <code>rf_normalizedDifference</code> to compute it. For this example we first compute NDVI and then collect the aggregate statistics (via <code>rf_aggStats</code>) on a per-month basis:</p>
<pre>
<code style="color:red">SELECT</code> month, ndvi_stats.<code style="color:red">* FROM</code> (
    <code style="color:red">SELECT</code> month, rf_aggStats(rf_normalizedDifference(nir, red)) as ndvi_stats
    <code style="color:red">FROM</code> red_nir_tiles_monthly_2017
    <code style="color:red">GROUP BY</code> month
    <code style="color:red">ORDER BY</code> month
)
-- +-----+---------+-----------+----+---+-------------------+-------------------+
-- |month|dataCells|noDataCells|min |max|mean               |variance           |
-- +-----+---------+-----------+----+---+-------------------+-------------------+
-- |1    |530105436|1261254564 |-1.0|1.0|0.2183160845625243 |0.12890947667168529|
-- |2    |600524070|1213875930 |-1.0|1.0|0.19716944997032773|0.12721311415470044|
-- |3    |615820993|1198579007 |-1.0|1.0|0.20199490649244112|0.12937148422536485|
-- |4    |629659958|1138660042 |-1.0|1.0|0.24221570883272578|0.13896602048542867|
-- |5    |635920714|1074799286 |-1.0|1.0|0.2949905328774185 |0.15610215565162433|
-- |6    |617826616|1092893384 |-1.0|1.0|0.3430829566130305 |0.1768520590388395 |
-- |7    |604436464|1106283536 |-1.0|1.0|0.37280247950373924|0.19128743861294956|
-- |8    |598500660|1169819340 |-1.0|1.0|0.3620362092401007 |0.19355049617163744|
-- |9    |623849120|1190550880 |-1.0|1.0|0.30889448330373964|0.1727205792001795 |
-- |10   |613413505|1200986495 |-1.0|1.0|0.2771316767269186 |0.15228789046594618|
-- |11   |546679019|1244680981 |-1.0|1.0|0.2510236308996954 |0.1348450714383397 |
-- |12   |520511978|1201728022 |-1.0|1.0|0.237714687195611  |0.13236712076432644|
-- +-----+---------+-----------+----+---+-------------------+-------------------+
</pre>
	<p>Plotting the resultant mean value and standard deviation bands shows the following:</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2018/december/images/ndvi-2017.png"></p>
	<p>(While the curve is interesting, interpreting it is beyond the scope of this article.)</p>
	<p>This is but a simple example of what one can do with RasterFrames. In <a href="http://rasterframes.io/">the documentation</a> we provide other examples, including supervised and unsupervised machine learning, as well as options for rendering results as new imagery layers.</p>
<h3>Scalability</h3>
	<p>As stated in the introduction, a primary benefit of RasterFrames is its ability to scale with additional compute hardware. This means that not only can we attempt analyses that extend beyond the bounds of a single computer, but that we can also trade money for time. The example above was run 6 times using different sized Elastic Map Reduce (EMR) clusters on AWS. A modest node size (AWS "m4.large") was used, each composed of 4 virtual cores, 8 GB RAM, and 32 GB HDD.</p>
	<table class="table">
  <thead>
    <tr>
      <th scope="col">Nodes</th>
      <th scope="col">Cores</th>
      <th scope="col">Memory (GB)</th>
      <th scope="col">Execution Time (min)</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>4</td>
      <td>8</td>
      <td>80</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>12</td>
      <td>24</td>
      <td>27</td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>20</td>
      <td>40</td>
      <td>18</td>
    </tr>
     <tr>
      <th scope="row">7</th>
      <td>28</td>
      <td>56</td>
      <td>12</td>
    </tr>
     <tr>
      <th scope="row">11</th>
      <td>44</td>
      <td>88</td>
      <td>9</td>
    </tr>
     <tr>
      <th scope="row">15</th>
      <td>60</td>
      <td>120</td>
      <td>8</td>
    </tr>
  </tbody>
</table>
	
	
	
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2018/december/images/compute-time.png"></p>
	<p>In this particular job we achieved significant scalability up until about 11 nodes. Cursory analysis indicated that the limiting factor in this analysis was network saturation. Analyses with greater computational requirements (say, K-means clustering), would be better able to make use of even more hardware.</p>
	<p>At the time of writing, all 6 jobs cost less than the price of a cup of coffee, making it very much within the reach of any business user.</p>
<h3>Conclusion</h3>
	<p>As more and more data becomes available in every industry, solutions have emerged to connect the traditional, tabular model of data to computational engines powerful enough to process it at volume. DataFrames and Spark SQL are general purpose tools that provide scalable data analysis through distributed computing. Data science and machine learning are consistently becoming more mainstream, and there is an opportunity for specialized technologies to further abstract common constructs within each problem space. For the EO realm, RasterFrames adds another layer of abstraction that can empower a broader set of users to process EO data in an intuitive way. By combining the technological advancements of its fellow 
Tech projects with DataFrames and Spark SQL, RasterFrames unlocks more natural, scalable machine learning for data analysts and scientists operating under a flood of new data while attempting to address global issues such as deforestation, climate change, and food shortages. As an open-source project and a proud member of Eclipse LocationTech, RasterFrames strives to have a positive global impact.</p>
<h3>Learn More</h3>
	<p>To try out RasterFrames we recommend starting out with the preconfigured Docker image containing Jupyter Notebooks. A library of examples in both Python and Scala are included. Additional examples and details on doing development with RasterFrames on the RasterFrames website rasterframes.io.</p>
		<ul>
			<li>Documentation: <a href="http://rasterframes.io/">rasterframes.io</a></li>
			<li>Source Code: <a href="https://github.com/locationtech/rasterframes">GitHub</a></li>
			<li>Community Chat: <a href="https://gitter.im/s22s/raster-frames">Gitter</a></li>
			<li>Jupyter Details: <a href="https://github.com/locationtech/rasterframes/tree/develop/deployment">Jupyter Notebooks</a></li>
		</ul>
	<p><a id="tagcontent">[^1]</a>: Demystifying Satellite Assets for Post-Disaster Situation Awareness. World Bank via OpenDRI.org. Accessed November 28, 2018.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/december/images/simon.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Simeon H.K. Fitch<br>
            <a target="_blank" href="https://astraea.earth/">VP of R&amp;D at Astraea, Inc</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/metasim">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
   <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/december/images/beng.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 D. Benjamin Guseman<br>
            <a target="_blank" href="https://astraea.earth/">Senior Software Engineer Astraea, Inc</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/ben-guseman-8b7b5476/">LinkedIn</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>