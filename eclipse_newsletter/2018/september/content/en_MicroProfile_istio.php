<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   	<ol>
   		<li><a href="#mpnutshell">MicroProfile in a nutshell</a></li>
   		<li><a href="#istionutshell">Istio in a nutshell</a></li>
   		<li><a href="#mpistio">MicroProfile meets Istio</a></li>
   		<li><a href="#configistio">MicroProfile Config in Istio</a></li>
   		<li><a href="#healthcheck">MicroProfile Health Check in Istio</a></li>
   		<li><a href="#liveness">Liveness</a></li>
   		<li><a href="#readiness">Readiness</a></li>
   		<li><a href="#servicemesh">Service Mesh usage of Liveness and Readiness</a></li>
   		<li><a href="#metrics">MicroProfile Metrics in Istio</a></li>
   		<li><a href="#openapi">MicroProfile Open API in Istio</a></li>
   		<li><a href="#opentracing">MicroProfile Open Tracing in Istio</a></li>
   		<li><a href="#mpjwt">MicroProfile JWT in Istio</a></li>
   		<li><a href="#restclient">MicroProfile Rest Client in Istio</a></li>
   		<li><a href="#faulttolerance">MicroProfile Fault Tolerance in Istio</a></li>
   		<li><a href="#timeout">Timeout</a></li>
   		<li><a href="#retry">Retry</a></li>
   		<li><a href="#bulkhead">Bulkhead</a></li>
   		<li><a href="#cirbreaker">Circuit Breaker</a></li>
   		<li><a href="#fallback">Fallback</a></li>
   		<li><a href="#currenteco">Current Ecosystem</a></li>
   		<li><a href="#futureeco">Future Ecosystem in my view</a></li>
   		<li><a href="#mpistioeco">MicroProfile and Istio ecosystem in action</a></li>
   		<li><a href="#ref">References</a></li>
   	</ol>
   	
<h2 id="mpnutshell">MicroProfile in a nutshell</h2>
		<p>MicroProfile is a fast-growing open community. It is a warm and friendly platform for developers to come together to evolve programming model for cloud-native microservices. Since it was established in June 2016, it has released 6 overall releases and 16 individual specification releases in less than 2 years. Below is the latest status of each individual specifications:</p>
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th scope="col">Specification</th>
		      <th scope="col">Latest Verison</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-config/">MicroProfile Config</a></th>
		      <td>1.3</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-fault-tolerance/">MicroProfile Fault Tolerance</a></th>
		      <td>1.1</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-health/">MicroProfile Health Check</a></th>
		      <td>1.0</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-metrics/">MicroProfile Metrics</a></th>
		      <td>1.1</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-open-api/">MicroProfile Open API</a></th>
		      <td>1.0</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-opentracing">MicroProfile Open Tracing</a></th>
		      <td>1.1</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-jwt">MicroProfile JWT</a></th>
		      <td>1.1</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-rest-client/">MicroProfile Rest Client</a></th>
		      <td>1.1</td>
		    </tr>
		  </tbody>
		</table>
		<p>The following specifications are in progress:</p>
		<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th scope="col">Specification</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-reactive-streams/">MicroProfile Reactive Streams Operators</a></th>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-reactive-messaging">MicroProfile Reactive Messaging</a></th>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-lra">MicroProfile LRA (Long Running Action)</a></th>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://github.com/eclipse/microprofile-concurrency">MicroProfile Concurrency</a></th>
		    </tr>
		  </tbody>
		</table>

	<p>The cloud-native microservices created using MicroProfile can be deployed anywhere freely, including a service mesh architecture, e.g. Istio. In this article, we explore how microservice using MicroProfile is functioning in Istio platform. Let's look at the Istio in a nutshell.</p>

<h2 id="istionutshell">Istio in a nutshell</h2>
	<p>Cloud-native microservices are well-suited to be deployed to a cloud infrastructure. When there are many microservices, the communication among the microservices will need to be orchestrated. The orchestration is managed by so-called service mesh, which is a dedicated infrastructure layer to make service to service communication fast, safe and reliable. It also provides discovery, load balancing, failure recovery, metrics and monitoring. It may also include A/B testing, canary releases, etc.</p>
	<p><a href="https://istio.io/"><strong>Istio</strong></a> is the most popular service mesh, designed to connect, manage and secure microservices. It is an open source project with an active community, which started from IBM, Google and Lyft. Istio 1.0 was released at the end of July, 2018.</p>
	<p>Istio provides the following core functionalities:</p>
<ul>
	<li>Traffic management:
		<ul>
			<li>Automatic load balancing for HTTP, gRPC, WebSocket, and TCP traffic.</li>
			<li>Fine-grained control of traffic behaviour with rich routing rules, retries, failovers, and fault injection.</li>
			<li>A pluggable policy layer and configuration API supporting access controls, rate limits and quotas.</li>
		</ul>
	</li>	
	<li>Observability
		<ul>
			<li>Automatic metrics, logs, and traces for all traffic within a cluster, including cluster ingress and egress.</li>
		</ul>
	</li>
	<li>Security
		<ul>
			<li>Secure service-to-service communication in a cluster with strong identity-based authentication and authorization.</li>
		</ul>
	</li>	
</ul>

<p>Istio is platform-independent and designed to run in a variety of environments, such as Kubernetes, Mesos, etc. This article focus on Istio under Kubernetes.</p>
	
<p>Istio consists of a data plane and a control plane (see diagram below for Istio Architecture, taken from <a href="https://istio.io/" target="_blank">istio.io</a>).</p> <br>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/control_plane.png"></p>

<h2 id="mpistio">MicroProfile meets Istio</h2>
	<p>As mentioned in the previous section, MicroProfile offers</p>
		<ul>
			<li>Config</li>
			<li>Fault Tolerance: Retry, Circuit Breaker, Bulkhead, Timeout, Fallback</li>
			<li>Health Check</li>
			<li>Metrics</li>
			<li>Open API</li>
			<li>Open Tracing</li>
			<li>Rest Client</li>
			<li>JWT</li>
		</ul>
	<p>Istio is capable of doing:</p>
		<ul>
			<li>Fault Tolerance: Retry, Circuit Breaker, Limits on concurrent connections or requests, Timeouts</li>
			<li>Metrics</li>
			<li>Open Tracing</li>
			<li>Fault Injection</li>
		</ul>
	<p>At a quick glimpse, there are some overlapping. Let's look at each individual MicroProfile specification and investigate how they can be used in Istio.</p>

<h2 id="configistio">MicroProfile Config in Istio</h2>
	<p>MicroProfile Config provides a solution to externalise the configuration. The default config sources include environment variables, system properties and <code>microprofile-config.properties</code> file on the classpath.</p>
	<p>The properties defined in Kubernetes config map can be transformed to environment variables via envFrom capability, as highlighted below.</p>

<pre style='color:#000000;background:#ffffff;'>
kind: ConfigMap
apiVersion: v1
metadata:
 name: <strong>example-config</strong>
 namespace: default
data:
 EXAMPLE_PROPERTY_1: hello
 EXAMPLE_PROPERTY_2: world

 
# Use envFrom to load ConfigMaps into environment variables

apiVersion: apps/v1beta2
kind: Deployment
metadata:
 name: mydeployment
 labels:
   app: servicea
spec:
 replicas: 1
 selector:
   matchLabels:
     app: servicea
 template:
   metadata:
     labels:
       app: servicea
   spec:
     containers:
       - name: app
         image: microprofile/servicea/latest
         imagePullPolicy: Always
         ports:
           - containerPort: 80
         <strong>envFrom:</strong>
         - configMapRef:
             name: <strong>example-config</strong>
</pre>

<p>The config properties specified in the config map are automatically injectable to the microservices via MicroProfile Config APIs.</p>

<pre style='color:#000000;background:#ffffff;'>
@ApplicationScoped
public class Demo {
 
   @Inject
   @ConfigProperty(name="example.property.1") String myProp1;
   @Inject
   @ConfigProperty(name="example.property.2") String myProp2;
   public void echo() {
       System.out.println(myProp1 + myProp2);
   }
}
</pre>
<p>Note: You might have noticed that in the above Istio config rules, configmap defines the properties <code>EXAMPLE_PROPERTY_1</code> <code>EXAMPLE_PROPERTY_2</code>. However, the above snippet looks for the properties <code>example.property.1 example.property.2</code>. Why? How could this work? <p>

<p>Environment variable names used by the utilities in the Shell and Utilities volume of IEEE Std 1003.1-2001 consist solely of uppercase letters, digits, and the '_' (underscore) from the characters defined in Portable Character Set and do not begin with a digit. Other characters may be permitted by an implementation; applications shall tolerate the presence of such names. Some shells might not support anything besides letters, numbers, and underscores, e.g. Ubuntu.</p>

<p>MicroProfile Config 1.3 onwards directly maps any non-alphanumberic characters (e.g. ".", which is invalid environment variable in some OS) to `_`. In this example, in the <code>configmap</code>, the property names are <code>EXAMPLE_PROPERTY_1 EXAMPLE_PROPERTY_2</code>, which is the mapping name of <code>example.property.1</code> and <code>example.property.2.</code></p>

<h2 id="healthcheck">MicroProfile Health Check in Istio</h2>
	<p>In Service Mesh architecture, each pod has a lifecycle, which is often in Kubernetes cluser. Kubernetes needs to know when to kill a pod and Istio needs to know when to route requests to a pod. In summary, knowing each pod's health status is necessary. A pod's health status is measured using Liveness and Readiness.</p>

<h3 id="liveness">Liveness</h3>
	<p>Many microservices run for long periods of time and they eventually might transit to a broken state. Therefore, it cannot recover except being restarted. This is called liveness lifecycle.</p>
<h3 id="readiness">Readiness</h3>
	<p>Sometimes, microservices are temporarily unable to serve traffic. For example, a microservice might need to load large data or configuration files during startup.</p>
	<p>MicroProfile Health Check denotes whether the microservice is live or ready. It exposes an endpoint <code>/health</code>. Invoking the endpoint returns either UP (healthy) or DOWN (unhealthy).</p>		
<h3 id="servicemesh">Health Check of microservices in Istio</h3>
	<p>Service Mesh e.g. Istio can utilise the readiness and liveness status from the underline component such as Kubernetes. Kubernetes provides liveness probes or readiness probes to detect and remedy such situations. Kubernetes can check the pods frequently. If the pod is not live, it will destroy the pod and start a new one. If the application is not ready, Kubernetes doesn't want to kill the application, but not to send its requests either.</p>
	<p>Microservices in Istio can utilise the endpoint exposed by MicroProfile Health for its liveness probe, so that Kubernetes can control whether to destroy the pod or not. Below is the configuration for a liveness prob. Any return code greater than or equal to 200 and less than 400 indicates success. Any other code indicates failure, which will cause the pod to be destroyed.</p>

<pre style='color:#000000;background:#ffffff;'>
livenessProbe:
         exec:
           command:
           - curl
           - -f
           - http://localhost:8080/health
         initialDelaySeconds: 10
         periodSeconds: 10
</pre>

<h2 id="metrics">MicroProfile Metrics in Istio</h2>
	<p>MicroProfile Metrics is to provide a unified way to export telemetry to management agents and APIs that microservice developers can use to add their telemetry data. For an instance, the following metrics will hold the number of systems in the inventory.</p>

<pre style='color:#000000;background:#ffffff;'>
 @Gauge(unit = MetricUnits.NONE, name = "inventorySizeGuage", absolute = true,
    description = "Number of systems in the inventory")
  public int getTotal() {
    return invList.getSystems().size();
  }
</pre>
	<p>MicroProfile metrics will be able to provide application-specific metrics over and above what Istio manages to get. It is complementary to Istio telemetry.</p>

<h2 id="openapi">MicroProfile Open API in Istio</h2>
	<p>In service mesh, it is important to view the capability of each service so that the service can be discovered. MicroProfile Open API comes to the rescue with the aim to provide a set of Java interfaces and programming models which allow Java developers to natively produce OpenAPI v3 documents for their JAX-RS applications.</p>

<pre style='color:#000000;background:#ffffff;'>
@GET
    @Path("/{hostname}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponses(
        value = {
            @APIResponse(
                responseCode = "404", 
                description = "Missing description",
                content = @Content(mediaType = "text/plain")),
            @APIResponse(
                responseCode = "200",
                description = "JVM system properties of a particular host.",
                content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Properties.class))) })
    @Operation(
        summary = "Get JVM system properties for particular host",
        description = "Retrieves and returns the JVM system properties from the system "
        + "service running on the particular host.")
    public Response getPropertiesForHost(
        @Parameter(
            description = "The host for whom to retrieve the JVM system properties for.",
            required = true, 
            example = "foo", 
            schema = @Schema(type = SchemaType.STRING)) 
        @PathParam("hostname") String hostname) {
        // Get properties for host
        Properties props = manager.get(hostname);
        if (props == null) {
            return Response.status(Response.Status.NOT_FOUND)
                            .entity("ERROR: Unknown hostname or the system service may "
                                + "not be running on " + hostname)
                            .build();
        }
 
        //Add to inventory to host
        manager.add(hostname, props);
        return Response.ok(props).build();
    }
</pre>
	<p>The APIs can be viewed via the endpoint of <code>/openapi/ui</code></p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/system_properties.png"></p>
	<p>This specification offers a great addition to Istio as DevOps can use this to find out the details about each JAX-RS endpoint.</p>

<h2 id="opentracing">MicroProfile Open Tracing in Istio</h2>
	<p>In a service mesh architecture, an essential need is to trace the service invocations. A complete chain from the client to the final service will help visualise service invocation hops. If there is a problem, this can be used to identify the faulty service.</p>
	<p>MicroProfile Open Tracing helps to achieve this goal. This specification defines behaviours and an API for accessing an OpenTracing compliant Tracer object within JAX-RS microservices. All incoming and outgoing requests will have OpenTracing spans automatically created. It works with the tracer implementations including Zipkin or Jaeger. Istio provides Jaeger and also works with Zipkin.</p>
	<p>Istio requires microservices to propagate the following 7 headers, which is automatically propagated by MicroProfile Open Tracing, which saves microservice developers from writing the boilerplate code.</p>
		<ul>
			<li><code>x-request-id</code></li>
			<li><code>x-b3-traceid</code></li>
			<li><code>x-b3-spanid</code></li>
			<li><code>x-b3-parentspanid</code></li>
			<li><code>x-b3-sampled</code></li>
			<li><code>x-b3-flags</code></li>
			<li><code>x-ot-span-context</code></li>		
		</ul>

<h2 id="mpjwt">MicroProfile JWT in Istio</h2>
	<p>Securing the service to service communication is essential requirement in service mesh architecture. MicroProfile JWT defines a means to secure service to service communication, strongly related to RESTful Security. One of the main strategies to propagate the security state from clients to services, services to services involves the use of security tokens. It uses <a href="http://openid.net/connect/">OpenID Connect</a> based <a href="https://tools.ietf.org/html/rfc7519">JSON Web Tokens (JWT)</a> for role-based access control (RBAC) of microservice endpoints. The MicroProfile JWT token is used to authenticate and authorise the user roles on @RolesAllowed, @PermitAll, @DenyAll defined in JSR-250.</p>
<pre>
// The JWT of the current caller. Since this is a request scoped resource, the 
// JWT will be injected for each JAX-RS request. 
@Inject 
private JsonWebToken jwtPrincipal; 

@GET 
@RolesAllowed({ "admin", "user" }) 
@Path("/username") 
public Response getJwtUsername() { 
return Response.ok(this.jwtPrincipal.getName()).build(); 
} 
</pre>
	<p><a href="https://istio.io/docs/concepts/security/">Istio security</a> provides two types of authentications:</p>
		<ul>
		<li>Transport authentication (service-to-service authentication): verifies the direct client making the connection using <a href="https://en.wikipedia.org/wiki/Mutual_authentication">mutual TLS</a> as a full stack solution for transport authentication. This can be used without any microservice code change.</li>
		<li>Origin authentication (end-user authentication): verifies the origin client making the request as an end-user or device. It only supports JWT origin authentication. Istio can add extra authentication and intercept with MicroProfile JWT authentication. The Origin authentication can be used if microservices have no security embedded.</li>
		</ul>

<h2 id="restclient">MicroProfile Rest Client in Istio</h2>
	<p>In Istio, service to service communication is often via JAX-RS. One issue with JAX-RS is its lack of type safe client. In order to fix this issue, MicroProfile Rest Client defines a type safe client programming model and also provide a better validation for misconfigurated JAX-RS clients.</p>
	
<pre style='color:#000000;background:#ffffff;'>
@Dependent
@RegisterRestClient
@Path("/properties")
public interface SystemClient {
end::annotations[]
 
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Properties getProperties() throws UnknownUrlException, ProcessingException;
}
</pre>

	<p>The above code snippet automatically builds and generates a client implementation based on what is defined in the <code>SystemClient</code> interface, which automatically setting up the client and connecting with the remote service.</p>
	<p>When the getProperties() method is invoked, the SystemClient instance sends a GET request to the <code> &lt;baseUrl&gt;/properties</code> endpoint.

<pre style='color:#000000;background:#ffffff;'>
@ApplicationScoped
public class InventoryManager {
  @Inject
  @RestClient
  private SystemClient defaultRestClient;
 
  public Properties get(String hostname) {
    try {
        return defaultRestClient.getProperties();
      } catch (UnknownUrlException e) {
        System.err.println("The given URL is unreachable.");
      } catch (ProcessingException ex) {
        handleProcessingException(ex);
      }
      return null;
  }
}
</pre>
	<p><code>@Inject</code> and <code>@RestClient</code> annotations inject an instance of the <code>SystemClient</code> called <code>defaultRestClient</code> to the <code>InventoryManager</code> class , which is type-safe client.</p>
	<p>This specification is used by the microservice and does not surface anything beyond. Therefore, it has no direct interaction nor conflict with Istio.</p>

<h2 id="faulttolerance">MicroProfile Fault Tolerance in Istio</h2>
	<p>Building a resilient microservice is key for microservices design. <a href="https://github.com/eclipse/microprofile-fault-tolerance">Eclipse MicroProfile Fault Tolerance</a> provides a simple and flexible solution to build a Fault Tolerance microservice, which is easy to use and configurable. It offers the following Fault Tolerance policies:</p>
		<ol>
			<li><strong>Timeout:</strong> Define a duration for timeout.</li>
			<li><strong>Retry:</strong> Define criteria on when to retry.</li>
			<li><strong>Bulkhead:</strong> isolate failures in part of the system while the rest part of the system can still function.</li>
			<li><strong>CircuitBreaker:</strong> offer a way of fail fast by automatically failing execution to prevent the system overloading and indefinite wait or timeout by the clients.</li>
			<li><strong>Fallback:</strong> provide an alternative solution for a failed execution.</li>
		</ol>
	<p>The main design is to separate execution logic from execution. The execution can be configured with fault tolerance policies.</p>
	<p>Istio also defines a set of opt-in failure recovery features, including:</p>
		<ol>
			<li>Timeouts</li>
			<li>Bounded retries with timeout budgets and variable jitter between retries</li>
			<li>Limits on number of concurrent connections and requests to upstream services</li>
			<li>Fine-grained circuit breakers (passive health checks) - applied per instance in the load balancing pool</li>
		</ol>
	<p>Istio's failure recovery is via Envoy proxy to mediate outbound traffic e.g. duplicating requests etc. However, it cannot manipulate any secure calls, e.g. https requests.</p>
	<p>Let's compare MicroProfile Fault Tolerance with Istio failure handling.</p>
		<table class="table">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">Categories</th>
		      <th scope="col">MicroProfile Fault Tolerance</th>
		      <th scope="col">Istio Fault Handling</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row">Http calls</th>
		      <td>Yes</td>
		      <td>Yes</td>
		    </tr>
		    <tr>
		      <th scope="row">Https calls</th>
		      <td>Yes</td>
		      <td>No</td>
		    </tr>
			<tr>
		      <th scope="row">Retry</th>
		      <td>Yes</td>
		      <td>Yes</td>
		    </tr>
		    <tr>
		      <th scope="row">Timeout</th>
		      <td>Yes</td>
		      <td>Yes</td>
		    </tr>
		    <tr>
		      <th scope="row">Circuit Breaker*</th>
		      <td>Yes</td>
		      <td>Yes</td>
		    </tr>
		    <tr>
		      <th scope="row">Semaphore bulkhead</th>
		      <td>Yes</td>
		      <td>Yes</td>
		    </tr>		    
			<tr>
		      <th scope="row">Thread-pool bulkhead</th>
		      <td>Yes</td>
		      <td>No</td>
		    </tr>
		    <tr>
		      <th scope="row">Fallback</th>
		      <td>Yes</td>
		      <td>No</td>
		    </tr>
		  </tbody>
		</table>
	<p>*MicroProfile Fault Tolerance Circuit Breaker is owned by the clients, which has no sharing among different clients, while Istio Circuit Breaker is owned by the backend, which means multiple connections can contribute towards the same Circuit Breaker.</p>				
	<p>Let's compare the failure handling in more details by investigating each individual policy.</p>

<h2 id="timeout">Timeout</h2>
	<p>For Timeout, MicroProfile Fault Tolerance uses @Timeout annotation to specify the timeout period.</p>

<pre style='color:#000000;background:#ffffff;'>
@Timeout(400) // timeout is 400ms
public void callService() {
    //calling ratings
}
</pre>

	<p>Istio uses the following configure rule to specify the timeout period.</p>

<pre style='color:#000000;background:#ffffff;'>
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
    - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
    timeout: 10s
</pre>
	
	<p>If both Istio Timeout and MicroProfile Fault Tolerance Timeout is specified, the most restrictive of the two will be triggered when failures occur.</p>

<h2 id="retry">Retry</h2>
	<p>How to deal with unstable services? Retry is the obvious choice to increase the success. MicroProfile uses @Retry to specify the retry.</p>

<pre style='color:#000000;background:#ffffff;'>
/**
* The configured the max retries is 90 but the max duration is 1000ms.
* Once the duration is reached, no more retries should be performed,
* even through it has not reached the max retries.
*/
@Retry(maxRetries = 90, maxDuration= 1000)
public void callService() {
    //calling rating;
}
/**
* There should be 0-800ms (jitter is -400ms - 400ms) delays
* between each invocation.
* there should be at least 4 retries but no more than 10 retries.
*/
@Retry(delay = 400, maxDuration= 3200, jitter= 400, maxRetries = 10)
public Connection serviceA() {
    return connectionService();
}
/**
* Sets retry condition, which means Retry will be performed on
* IOException.
*/
@Timeout(400)
@Retry(retryOn = {IOException.class})
public void callRating() {
    //call ratings;
}
</pre>

 <p>Istio uses the following config rule to specify Retry.</p>
 
<pre style='color:#000000;background:#ffffff;'>
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
    - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
    retries:
      attempts: 3
      perTryTimeout: 2s
</pre>

	<p>The above config rule can be simply mapped to MicroProfile Fault Tolerance Retry with <code>@Retry(maxRetries=3</code>, <code>delay=2</code>, <code>delayUnit=ChronoUnit.SECONDS)</code></p>
	<p>When MicroProfile Fault Tolerance Retry and Istio Retry are specified, the microservice will eventually multiply the number the retries. For an instance, if MicroProfile Fault Tolerance specifies 3 retries and Istio specifies 3 retries, the maximum retries will be 9 (3x3), as each outgoing request are duplicated 3 times. Don't panic. Read on. A solution is provided by MicroProfile Fault Tolerance.</p>

<h2 id="bulkhead">Bulkhead</h2>
	<p>MicroProfile Bulkhead provides two different categories of bulkhead:</p>
		<ul>
			<li>Thread Isolation
				<ul>
					<li>Use thread-pool with a fixed number of threads and a waiting queue by using the annotation of <code>Asynchronous</code> and <code>Bulkhead</code></li>
				</ul>
			</li>
		</ul>	
<pre style='color:#000000;background:#ffffff;'>
// maximum 5 concurrent requests allowed, maximum 8 requests allowed in the waiting queue
@Asynchronous
@Bulkhead(value = 5, waitingTaskQueue = 8)
public Future &lt;Connection&gt; serviceA() {
Connection conn = null;
conn = connectionService();
return CompletableFuture.completedFuture(conn);
}
</pre>

	<ul>
		<li>Semaphore Isolation
			<ul>
				<li>Limit the number of concurrent requests</li>
			</ul>
		</li>
	</ul>

<pre style='color:#000000;background:#ffffff;'>
@Bulkhead(5) // maximum 5 concurrent requests allowed
public Connection serviceA() {
       Connection conn = null;
       conn = connectionService();
       return conn;
}
</pre>

	<p>Istio can use Circuit Breaker config rules to configure the connection pool, so that it can limits the concurrent number of requests. The following rule indicate that if you exceed more than one connection and request concurrently, you should see some failures when the Istio-proxy opens the circuit for further requests and connections.</p>

<pre style='color:#000000;background:#ffffff;'>
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: httpbin
spec:
  host: httpbin
  trafficPolicy:
    connectionPool:
      tcp:
        maxConnections: 1
      http:
        http1MaxPendingRequests: 1
        maxRequestsPerConnection: 1
</pre>

<h2 id="cirbreaker">Circuit Breaker</h2>
	<p>Circuit Breaker is an important pattern for creating resilient microservices. It can be used to prevent repeatable timeouts by instantly rejecting the requests. MicroProfile Fault Tolerance uses @CircuitBreaker to control the client calls.</p>

<pre style='color:#000000;background:#ffffff;'>
@CircuitBreaker(successThreshold = 10, requestVolumeThreshold = 4, failureRatio=0.75, delay = 1000)
public Connection serviceA() {
       Connection conn = null;
       conn = connectionService();
       return conn;
}
</pre>

	<p>The above code-snippet means the method serviceA applies the <span style="color:red">CircuitBreaker</span> policy. For the last 4 invocations, if 75% failed (i.e. 3 out of the 4 invocations failed) then open the circuit. The circuit will stay open for 1000ms and then back to half open. After 10 consecutive successful invocations, the circuit will be back to close again. When a circuit is open, A <span style="color:red">CircuitBreakerOpenException</span> will be thrown instead of actually invoking the method.</p>
	<p>Istio uses Circuit Breaker rules to limit the impact of failures, latency spikes, and other undesirable effects of network issues.</p>
	<p>The following rule sets a connection pool size of 100 connections and 1000 concurrent HTTP2 requests, with no more than 10 req/connection to "reviews" service. In addition, it configures upstream hosts to be scanned every 5 mins, such that any host that fails 7 consecutive times will be ejected for 15 minutes.</p>

<pre style='color:#000000;background:#ffffff;'>
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews-cb-policy
spec:
  host: reviews.prod.svc.cluster.local
  trafficPolicy:
    connectionPool:
      tcp:
        maxConnections: 100
      http:
        http2MaxRequests: 1000
        maxRequestsPerConnection: 10
    outlierDetection:
      consecutiveErrors: 7
      interval: 5m
      baseEjectionTime: 15m
</pre>
	
	<p>As you see, Istio CircuitBreaker cover both some aspect of bulkhead and circuit breaker. The above configuration can be translated to the following MicroProfile Fault Tolerance</p>
	<p><code>@Bulkhead(1000)<br>
@CircuitBreaker(requestVolumeThreshold=7, failureRatio=1.0, delay=15, delayUnit=ChronoUnit.MINUTES)</code></p>
	<p>The Circuit Breaker between MicroProfile and Istio are different. In MicroProfile Fault Tolerance, the policy is placed on the clients, as the policy controls outbound requests. Istio places the policy on the destination, so multiple clients could contribute towards the same Circuit Breaker.</p>

<h2 id="fallback">Fallback</h2>
	<p>MicroProfile Fault Tolerance or Istio Fault Tolerance is here to increase the success chance. However, in realty, they cannot guarantee 100% success rate. You still need to come up with a contingency plan. What if my request fails? Fallback in MicroProfile Fault Tolerance comes to rescue.</p>
	<p>Istio Fault Tolerance does not provide any fallback capabilities. It does make sense as only application developers can decide the contingency plan, which requires business knowledge.</p>
	<p>MicroProfile Fault Tolerance offers a great fallback capability via @Fallback annotation.</p>
	<p>In the following code snippet, when the method fails and retry reaches its maximum retry, the fallback operation will be performed. In this example, it just return a string. You might choose to call a different backup service.</p>

<pre style='color:#000000;background:#ffffff;'>
@Retry(maxRetries = 2)
@Fallback(fallbackMethod= "fallbackForServiceB")
public String serviceB() {
	 counterForInvokingServiceB++;
	 return nameService();
}
private String fallbackForServiceB() {
return "myFallback";
}
</pre>

<h2 id="currenteco">Current Ecosystem</h2>
	<p>When you read up to here, you might wonder:<p>
	<p><strong>Q: Can I use MicroProfile Fault Tolerance fallback together with Istio Fault handling?</strong> <br>
	   A: Yes, you can by just using @Fallback annotation. <br>
		This is a simple ecosystem. Let's go further.</p>
	<p><strong>Q: What if I want to use MicroProfile Fault Tolerance in dev and testing but when it is deployed to Istio, I want to use Istio fault handling? </strong><br>
	   A: Thanks to the configuration <span style="color:red">MP_Fault_Tolerance_NonFallback_Enabled</span> provided by MicroProfile Fault Tolerance, you can configure this property in configmap with the value of false, which will disable MicroProfile Fault Tolerance capabilities except Fallback.</p>

<pre style='color:#000000;background:#ffffff;'>
apiVersion: v1
kind: ConfigMap
metadata:
	name: servicea-config
	data:
MP_Fault_Tolerance_NonFallback_Enabled: "false"
</pre>
	<p>This ecosystem is still basic as it directly disables MicroProfile Fault Tolerance except fallback. Microservice developers fault handling knowledge is completely thrown away. DevOps has to create Istio config rules from scratch.</p>

<h2 id="futureeco">Future Ecosystem in my view</h2>
	<p>Producing a correct Istio config rules can be daunting. If we can use MicroProfile Fault Tolerance annotation as the input to Istio fault handling rule creation, it will be possible to generate the corresponding Istio config rules. In this way, developers' knowledge about timeout or retries will be reflected in the configure rules.</p>
	<p>However, for https requests, Istio cannot intercept the request to add fault handling capability. The corresponding Istio config rules can still be generated but MicroProfile Fault Tolerance will not be disabled. DevOps can modify the parameters in the rules, which will automatically take effect in the microservices as all MicroProfile Fault Tolerance annotation parameters are configurable. The ecosystem can be summarised in the following table.</p>
	<p>As mentioned earlier, for https requests, if Istio fault handling is to be used, MicroProfile Fault Tolerance except fallback can be disabled via the following configuration. The corresponding properties can be set in configmap as explained in the previous section, which will disable the relevant Fault Tolerance capabilities.</p>
<div class="table-responsive">
	<table class="table table-bordered">
	  <thead>
	    <tr>
	      <th scope="col"></th>
	      <th scope="col"></th>
	      <th scope="col"></th>
	      <th colspan="2">Istio Circuit Breaker</th>
	    </tr>
	     <tr>
	      <th scope="col"></th>
	      <th scope="col">Istio Retry</th>
	      <th scope="col">Istio Timeout</th>
	      <th scope="col">Concurrent request handling</th>
	      <th scope="col">Failure handling</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="row">MicroProfile Retries</th>
	      <td>Set <code>Retry/enabled=false</code> in configmap</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	    </tr>
	    <tr>
	      <th scope="row">MicroProfile Timeout</th>
	      <td>&#x2714;</td>
	      <td>Set <code>Timeout/enabled=false</code> in configmap</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	    </tr>
	    <tr>
	      <th scope="row">MicroProfile Circuit Breaker</th>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
		  <td>Set <code>CircuitBreaker/enabled=false</code> in configmap</td>
	    </tr>
	     <tr>
	      <th scope="row">MicroProfile Bulkhead</th>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	      <td><code>Bulkhead/enabled=false</code></td>
	      <td>&#x2714;</td>
	    </tr>
	     <tr>
	      <th scope="row">MicroProfile Fallback</th>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	      <td>&#x2714;</td>
	    </tr>
	  </tbody>
	</table>
</div>	
	
	<p>For Https requests, MicroProfile Fault Tolerance will handle the fault tolerance capabilities since Istio cannot inject the fault handling.</p>
	<p>The plan is to generate Istio config rules and then disable MicroProfile Fault Tolerance if Istio can handle the situation.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/http_requests.JPG"></p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/https_requests.JPG"></p>
	<p>This section is what I am thinking and would love to hear more feedback from the public.</p>
	
<h2 id="mpistioeco">MicroProfile and Istio ecosystem in action</h2>
	<p>MicroProfile sets up a sample github <a href="https://github.com/eclipse/microprofile-service-mesh/" target="_blank">repository</a> to explore the ecosystem with particular focus on MicroProfile Fault Tolerance. There are also two microservices set up to demonstrate the ecosystem, <a href="https://github.com/eclipse/microprofile-service-mesh-service-a/" target="_blank">servicea</a> and <a href="https://github.com/eclipse/microprofile-service-mesh-service-b" target="_blank">serviceb</a>. The two microservices will demonstrate all MicroProfile specifications. If you are interested in this exercise, please join in the <a href="https://gitter.im/eclipse/microprofile-service-mesh" target="_blank">gitter room</a> and join the weekly call, where the details can be found the <a href="https://calendar.google.com/calendar/embed?src=gbnbc373ga40n0tvbl88nkc3r4%40group.calendar.google.com&ctz=GMT" target="_blank">MicroProfile Calendar</a>.
	<p>In summary, MicroProfile is seen as the programming model for developing microservices for Istio service mesh.</p>

<h2 id="ref">References</h2>
	<ol>
		<li>MicroProfile website: <a href="http://microprofile.io/" target="_blank">http://microprofile.io/"</a></li>
		<li>Istio: https://istio.io/</li>
		<li>MicroProfile service mesh repo: <a href="https://github.com/eclipse/microprofile-service-mesh" target="_blank">https://github.com/eclipse/microprofile-service-mesh</a></li>
		<li>MicroProfile service mesh samples: <br>
			<a href="https://github.com/eclipse/microprofile-service-mesh-service-a/" target="_blank">https://github.com/eclipse/microprofile-service-mesh-service-a/</a><br>
			<a href="https://github.com/eclipse/microprofile-service-mesh-service-b/" target="_blank">https://github.com/eclipse/microprofile-service-mesh-service-b/</a></li>
		<li>Open Liberty guides on MicroProfile: <a href="https://openliberty.io/guides/" target="_blank">https://openliberty.io/guides/</a></li>
		<li>MicroProfile Fault Tolerance article:<br>
		<a href="https://www.eclipse.org/community/eclipse_newsletter/2017/september/article4.php" target="_blank">https://www.eclipse.org/community/eclipse_newsletter/2017/september/article4.php</a></li>
		<li>MicroProfile service mesh experience: <a href="https://medium.com/@pilhuhn/working-on-microprofile-service-mesh-istio-and-kiali-26d6c01b45cc" target="_blank">https://medium.com/@pilhuhn/working-on-microprofile-service-mesh-istio-and-kiali-26d6c01b45cc</a></li>
	</ol>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/september/images/emily.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Emily Jiang<br>
            <a target="_blank" href="https://www.ibm.com/ca-en/">IBM</a>
          </p>
          <p>Emily Jiang is Liberty Architect for MicroProfile and CDI in IBM. Based at IBM's Hursley laboratory in the UK, she has worked on the WebSphere Application Server since 2006 and is heavily involved in Java EE implementation in WebSphere Application Server releases. She is a key member of MicroProfile and CDI Expert Group, and leads the specification of MicroProfile Config and Fault Tolerance. Emily is also Config JSR co-spec lead. She regularly speaks at conferences, such as JAX, Voxxed, Devoxx US, Devoxx UK, Devoxx France, DevNexus, EclipseCon and CodeOne, etc.</p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/emilyfhjiang">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/emily-jiang-60803812/">Linked In</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>