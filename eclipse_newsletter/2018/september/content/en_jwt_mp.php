<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>For a while, companies used to store user contextual information in HTTP Sessions. It has worked for years, but in a clustering architecture, it proved to be expensive, unreliable, and painful to scale.  However, with microservices and REST, which are stateless, HTTP Session state is not used eliminating the problem of sharing session state. The question is: How and where to save security context?</p>
	<p>In this post, I'll explain Eclipse MicroProfile, JSON Web Tokens (JWT), and how they can be used to implement stateless security. I'll also talks about the extensibility and flexibility of MicroProfile with claims.</p>

<h2>What is Eclipse MicroProfile?</h2>
	<p>Eclipse MicroProfile defines itself as<p>
		<ul style="list-style: none;">
			<li>The MicroProfile is a baseline platform definition that optimizes Enterprise Java for a microservices architecture and delivers application portability across multiple MicroProfile runtimes.  -- <a href="https://MicroProfile.io/faq" target="_blank">MicroProfile FAQ</a></li>
		</ul>
	<p>Java for Enterprise applications are usually built on two options: Spring Framework and Java EE. Java EE created a set of specifications defined first by Sun Microsystems and then by Oracle through the Java Community Process. Specifications were meant to facilitate vendor agnostic development and deployment. During the last 5 years, the Java EE platform has become stable and mature resulting in less frequent releases. Java EE has also expanded over the years, requiring vendors to maintain or implement the large set of specifications.</p>
	<p>While Java EE was slowing down, web services continue to evolve leading to the creation of new technologies such as JSON, HTTP 2, RESTful web services and microservices architecture. With its slower release cycle, Java EE failed to keep up with changes in the industry.  Aware of the skills and investment that both enterprises and vendors put into Java EE, a group of vendors, supported by the active Java Community, decided to create MicroProfile, an optimized platform for microservices architecture.</p>
	<p>MicroProfile was created in 2016 and quickly joined the Eclipse foundation. Since then, there have been 5 releases of the MicroProfile platform with the addition of many specifications to addressing the needs and feedback of users (See Figure 1). MicroProfile JWT, one of the most recent specifications, addresses the authentication and authorization of microservices using JWT.  MicroProfile as per June 2018 release, is composed of the following specification.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/chart.jpg"></p>
	<p>Figure 1: MicroProfile 1.4 release content.</p>
	
<h2>What is JWT?</h2>
	<p>JWT stands for JSON Web Token. It's a JSON-based text format for exchanging information between parties. JWT is an open standard specified under RFC 7519. The information contained in the JWT is called claims and the JWT is usually digitally signed (i.e. JSON Web Signature) so that the information can be verified and trusted. Optionally, it's also possible to encrypt the claims (i.e. JSON Web Encryption) so it's not in clear text within the JWT.</p>
	<p>JWT is widely used because it is simple and efficient. One use cases is exchanging authentication and authorization information between parties so that a remote server can identify the caller, verify the caller's identity by checking the signature, and implement Role Based Access Control based on roles included in the JWT call. Standards such as OpenID Connect and OAuth 2 use JWT to represent their own tokens.</p>
	<p>A JWT is composed of:</p>
		<ul>
			<li><strong>Header</strong>: the header contains metadata such as the type of algorithm used to sign the token (HS256 for HMAC for instance, RS256 for RSA, ES256 for Elliptic Curves), the type of the token (OpenID Connect, OAuth2, MicroProfile JWT), etc</li>
			<li><strong>Claims</strong>: the claims is basically all the information you want to store in the token. Some are required depending on the type of token.</li>
			<li><strong>Signature</strong>: the signature, although optional, it is highly recommended.  If you want to trust something coming from the outside, you need the signature. It allows you to know the content hasn't been changed by the caller or in between (man in the middle).  I don't recommend using MicroProfile JWT without a digital signature.</li>
		</ul>
	<p>In its serialized version, a JWT token looks like: </p>
	<p><strong>base64(header).base64(claims).base64(signature)</strong></p>
	<p>As an example:</p>
<pre style='color:#000000;background:#ffffff;'>
<span style="color:green">eyJraWQiOiJteS1yc2Eta2V5IiwiY3R5IjoianNvbiIsInR5cCI6IkpXVCIsImFsZyI6IlJTMjU2In0.</span><span style="color:blue">eyJzdWIiOiJhbGV4IiwidG9rZW4tdHlwZSI6ImFjY2Vzcy10b2tlbiIsImlzcyI6Ii9vYXV0aDIvdG9rZW4iLCJncm91cHMiOlsiY3JlYXRlIiwidXBkYXRlIiwiZGVsZXRlIl0sIm5iZiI6MTUzMzIyODUxOSwiZXhwIjoxNTMzMjI4ODE5LCJpYXQiOjE1MzMyMjg1MTksImVtYWlsIjoiYWxleEBzdXBlcmJpei5jb20iLCJqdGkiOiI0ODAxODQ4MDFmNzgyOGNhIiwidXNlcm5hbWUiOiJhbGV4In0.</span><span style="color:purple">PZWPE-bXNzKbO6aoEqWxE....apj8sxtIBP1rgFIU8ZQ</span>
</pre>
	<p>In it's JSON format, a JWT token looks like this for the header</p>
<pre style='color:#000000;background:#ffffff;'>
<span style="color:green">{
  "kid": "my-rsa-key",
  "cty": "json",
  "typ": "JWT",
  "alg": "RS256"
}</span>
</pre>
	<p>And like this for the payload</p>
<pre style='color:#000000;background:#ffffff;'>
<span style="color:blue">{
  "sub": "alex",
  "token-type": "access-token",
  "iss": "/oauth2/token",
  "groups": [
    "create",
    "update",
    "delete"
  ],
  "nbf": 1533228519,
  "exp": 1533228819,
  "iat": 1533228519,
  "email": "alex@superbiz.io",
  "jti": "480184801f7828ca",
  "username": "alex"
}</span>
</pre>

<h2>Implementing stateless security with MicroProfile JWT?</h2>
	<p>MicroProfile JWT is a specification aiming at defining a simple and common way to rely on JWT to implement authentication and authorization for microservices. Implementing security usually involves 3 main steps:</p>
		<ul>
			<li>Authenticate the caller: identify the caller by reading a standard claim (e.g. username) in the JWT and validate the signature of the JWT</li>
			<li>Authorize the caller: by using the roles of the group listed in the claim, enforce access control in the application</li>
			<li>Propagate caller context: pass the JWT token in subsequent calls so other microservices involved can also service the request</li>
		</ul>
	<p>MicroProfile JWT defines a minimum set of claims such as: it is usable as an authentication token and it is usable as an authorization token containing high level roles. MicroProfile JWT also supports extensibility through the defined <a href="http://www.iana.org/assignments/jwt/jwt.xhtml" target="_blank">IANA JWT Assignments</a> (pre-defined set of claims) or any custom claims. In addition to that, MicroProfile JWT also defines two new claims: 'upn' to identify the subject and 'groups' to hold all the roles to be mapped in the application.</p>
	<p>MicroProfile JWT completely fulfills the requirements of microservices architecture and solves the HTTP Session clustering issue by pushing the state on the caller side as opposed to maintaining the state on the server. On the server side, it becomes trivial to pass the security context (aka the JWT) either as a header, a cookie or in the payload.</p>
	<p>Figure 2, shows the big picture of how it looks like all together.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/september/images/issue_jwt.png"></p>
	<p>Figure 2: typical architecture of a system using JWT</p>
	<p>If you want to know about more MicroProfile JWT, visit <br>
	<a href="https://www.eclipse.org/community/eclipse_newsletter/2017/september/article2.php" target="_blank">https://www.eclipse.org/community/eclipse_newsletter/2017/september/article2.php</a></p>

<h2>The Pros and Cons to using JWT</h2>
	<p>There are some downsides to using JWT to store session context including:</p>
		<ul>
			<li><strong>Base64 encoding isn't encryption</strong>: Because base64 is not encryption everyone in the middle or the caller itself can, at any point, read the content of the JWT. If you want to protect your data, you also need to encrypt part or the whole JWT using JSON Web Encryption another standard like JSON Web Signature but for encryption rather than signing.</li>
			<li><strong>Larger Payload</strong>:even in its most minimal form, a serialized JWT is way bigger than a regular cookie or SessionID. Even though nowadays bandwidth is less of an issue, it's something to keep in mind. The more you put in the JWT, the more you have to send with each and every communication on the wire.</li>
			<li><strong>Token Expiration</strong>keep in mind that the token can be valid but may contain outdated information. Make sure to properly configure the expiration policy of your token so that a stale token can be detected and refreshed.</li>
		</ul>
	<p>On the other hand, there are a lot of benefits to using JWT</p>
		<ul>
			<li><strong>JSON</strong>: because it's JSON based, it's simple and lightweight to deal with and there are plenty of libraries for it</li>
			<li><strong>Speed and Reliability</strong>: it does not require a third party call which in the context of a distributed system (microservices for instance) can be slow and create a single point of failure (either LDAP, Database, or your identity provider).</li>
			<li><strong>Secure</strong>:  it is secure if you are at least using JSON Web Signature and possibly JSON Web Encryption</li>
			<li><strong>Claims</strong>: it easy for parties to agree on a common set of claims for performing authentication, authorization and more</li>
		</ul>
	<p>MicroProfile JWT defines a common integration with Java EE allowing efficient use of JWT in Java EE development. For instance, it defines a standard javax.security.Principal interface to access the main claims and it integrates with <a href="http://cdi-spec.org/">Context and Dependency Injection</a> (CDI) and has the ability to inject all or a specific claim.</p>

<h2>How to extend user context with custom claims?</h2>
	<p>When using JWT for authentication and authorization, it's important to be able to add custom claims to the JWT, otherwise, you will end up having to maintain state on the server side with the additional information, or do an extra call passing in the JWT.  If you are looking at an identity provider to authenticate the user and deliver JWT, you should be seriously considering support for extensible custom claims.</p>
	<p>Basically, the idea is to customize the JWT with user information such as email, language, and subscription details, so you can then use the trustable information in the JWT during processing. You can for instance decide to route gold subscriptions to a dedicated set of servers.</p>
	<p>To conclude, MicroProfile JWT makes use of most of JWT standard to simplify authentication and authorization in the context of microservices. It provides a common set of claims and a default behavior for JAX-RS and CDI so you can focus on the implementation of your business logic.</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/september/images/Jean.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jean-Louis Monteiro<br>
            <a target="_blank" href="https://www.tomitribe.com/">Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jlouismonteiro">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>