<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2018.09.28</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/stephanie.jpg" />
                            <h3>Editor's Note</h3>
                            <p>Welcome to the Eclipse MicroProfile Edition of the newsletter! <br>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/september/MicroProfile_istio.php">MicroProfile, the microservice programming model made for Istio</a><br>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/september/reactive_mp.php">How to Write Reactive Applications with MicroProfile</a><br>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/september/jwt_mp.php">Stateless security with MicroProfile JWT</a><br>
                            		&nbsp;&#10140; <a style="color:#000;" target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2018/september/mp_jakartaee.php">What's next for MicroProfile and Jakarta EE?</a><br></p>   
                            <p>Eclipse MicroProfile is not your typical open source project, it's a community dedicated to optimizing enterprise Java for microservice based architectures. Being a little over two years old, MicroProfile continues to deliver more value with each of its releases. The dynamic and growing Eclipse MicroProfile community looks to rapidly innovate, with future releases expected to update existing APIs and add new ones.</p>
							<p>In this month's issue, I am happy to showcase four great articles by some of Eclipse MicroProfile's dedicated community members! In the first article, explore how microservices using MicroProfile function in the Istio platform. Next, learn how MicroProfile adopts reactive programming and why support for it is needed. In the third article, examine how Eclipse MicroProfile and JSON Web Tokens (JWT) can be used to implement stateless security. Finally, see one of the most difficult, hot-button questions around Jakarta EE and MicroProfile get answered! It's all right here in this month's edition of the Eclipse Newsletter!</p>
							<p>I'd like to thank the Eclipse MicroProfile community for contributing these articles. It is always a great pleasure to work with each author and the community as a whole.</p> 
                            <p>Enjoy!</p>
                            <p>Stephanie Swart<br>
                            <a style="color:#000;" target="_blank" href="https://twitter.com/stephaniejswart">@stephaniejswart</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/MicroProfile_istio.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/one.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/MicroProfile_istio.php"><h3>MicroProfile, the microservice programming model made for Istio</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/reactive_mp.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/three.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/reactive_mp.php"><h3>How to Write Reactive Applications with MicroProfile</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/jwt_mp.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/two.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/jwt_mp.php"><h3>Stateless security with MicroProfile JWT</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/mp_jakartaee.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/four.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/september/mp_jakartaee.php"><h3>What's next for MicroProfile and Jakarta EE?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://mmilinkov.wordpress.com/2018/09/26/k8s-at-the-edge/?utm_source=K8&utm_medium=blog&utm_campaign=newwg&utm_term=www">K8s at the Edge - Some Context on the New Kubernetes IoT Working Group</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://blogs.eclipse.org/post/tanja-obradovic/welcoming-glassfish-eclipse-foundation">Welcoming GlassFish to the Eclipse Foundation</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://blogs.eclipse.org/post/thabang-mashologu/87-billion-shared-investment-sizing-economic-value-eclipse-community">An $8.7 Billion Shared Investment: Sizing the Economic Value of Eclipse Community Collaboration</a></li>
                            		<li><a target="_blank" style="color:#000;" href="http://www.globenewswire.com/news-release/2018/09/11/1569175/0/en/Mizuho-International-Joins-Eclipse-Foundation.html">Mizuho International Joins Eclipse Foundation</a></li>
                               		<li><a target="_blank" style="color:#000;" href="https://blog.benjamin-cabe.com/2018/09/04/how-many-lines-of-open-source-code-are-hosted-at-the-eclipse-foundation">How many lines of code are hosted at the Eclipse Foundation?</a></li>
                           
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="https://www.sdxcentral.com/articles/news/cncf-eclipse-foundation-push-kubernetes-to-the-edge/2018/09/">CNCF, Eclipse Foundation Push Kubernetes to the Edge</a></li>
								<li><a target="_blank" style="color:#000;" href="https://siliconangle.com/2018/09/26/new-working-group-created-bring-kubernetes-iot-edge-networks/">New working group aims to bring Kubernetes to IoT edge networks</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.tomitribe.com/blog/jnosql-and-jakarta-ee/">JNoSQL and Jakarta EE</a></li>
								<li><a target="_blank" style="color:#000;" href="http://www.fujitsu.com/global/products/software/middleware/application-infrastructure/interstage/solutions/ai/apserver/20180918-joined-eclipse-foundation-as-strategic-member.html">Fujitsu has joined the Eclipse Foundation as a Strategic Member</a></li>
							</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/agile-uml">Eclipse Agile UML</a>: is a toolset that supports the agile specification of systems in UML and OCL and the automated generation of code from these specifications. </li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-krazo">Eclipse Krazo</a>: provides an implementation for Model View Controller API (MVC 1.0), starting from the specification defined by Java Community Process JSR-371. </li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.xsd">Eclipse MDT XSD 2.15.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egit">Eclipse EGit 5.1.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.papyrus">Eclipse Papyrus 4.1.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.ocl">Eclipse OCL 6.5.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mmt.qvt-oml">Eclipse QVT Operational 3.9.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.emf.emf">Eclipse EMF 2.15.0</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.etrice">Eclipse eTrice 2.0.0</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2018/september/images/sep_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://jaxlondon.com/">JAX London</a><br>
                            Oct 8-11, 2018 | London, UK</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2018/">EclipseCon Europe 2018</a><br>
                            Oct 23-25, 2018 | Ludwigsburg, Germany</p>
                             <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2018/iot-day">Eclipse IoT Day at EclipseCon Europe 2018</a><br>
                            Oct 23, 2018 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://jax.de/en/">W-JAX 2018</a><br>
                            Nov 5-9, 2018 | Munich, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://devoxx.be/">DEVOXX BE</a><br>
                            Nov 12-16, 2018 | Antwerp, Belgium</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.lfasiallc.com/events/kubecon-cloudnativecon-china-2018/">KubeCon + CloudNativeCon</a><br>
                            Nov 13-15, 2018 | Shanghai, China</p>
                            <p><a target="_blank" style="color:#000;" href="https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2018/">KubeCon + CloudNativeCon North America 2018</a><br>
                           December 10 - 13, 2018 | Seattle, Washington</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->