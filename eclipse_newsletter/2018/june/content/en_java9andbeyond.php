<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
<p>The world around Java is changing at a fast pace. Java now has bi-yearly releases, this started March 2018. Java 10 was released in March 2018 and Java 11 is expected in September 2018. Not so far back, in September 2017, we had the Java 9 release. Quarterly releases of the Eclipse IDE in March, June, September, and December will help provide a release with a new version of Java without much delay. The latest release, Eclipse Photon is now available and supports Java 9 and Java 10. It has added compiler, UI and launching support for Java 9 and Java 10.</p>

<h2>Java 9 features</h2>
<p><strong>1. Private interface methods</strong> – Java 8 allowed default methods in Interface and Java 9 allows private scoped and static methods in interfaces. This will help programmers to keep reusable codes in private interface methods.</p>
   
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/InterfaceMethods.png"></p>

<p><strong>2. Try-With Resources enhancements </strong>– Java 8 mandated the variable to be created for the try-with resources block. Java 9 allows final and effectively final variables to be placed in try-with-resource blocks.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/TryWithResources.PNG"></p>

<p><strong>3. @SafeVarargs Annotation for private instance methods</strong> – Java 7 allowed <code>@SafeVarargs</code> annotation for final and static methods and the Constructors. Java 9 allows <code>@SafeVarargs</code> annotation for private instance methods also. It can not be used for public instance methods.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/SafeVarargs.PNG"></p>

<p><strong>4. Collection Factory Methods</strong> – Java 9 has added simple ways to create immutable Collections without null values by providing a set of <code>&lt;Collection&gt;.of</code> methods. The created collection should not have null values and should not be modified later to avoid runtime exceptions.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Collection_Factory_Method.png"></p>

<p><strong>5. Stream improvements</strong> – Java 9 added the new APIs for Stream.</p>

    <ul>
        <li>Stream improvements–Java 9 added the new APIs for Stream.  Stream iterate – A terminating condition can be added to the iterate method.</li>
        <li>Stream takeWhile -  takeWhile is another way to add a terminating condition to iterate method.</li>
        <li>Stream dropWhile - dropWhile drops a subset of elements from the Stream till it matches the specified condition.</li>
        <li>Stream ofNullable - ofNullable method returns a sequential Stream with single value only if the Stream is not null. This method helps the programmers to avoid adding the null checks to the Stream.</li>
    </ul>
    
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/StreamImprovements.PNG"></p>


<p><strong>6. Option --release</strong> – Java 9 released <strong>–release</strong> option. In Eclipse IDE, if you have Java 9 in your build path you can still use the –release option along with compliance level 1.6 or more for the workspace or a project. Without this option, when Java 1.8 was on build path and the compliance level is set as 1.7, the code base still gets compiled against the APIs available with the Java 1.8 JRE found on the path. This is available only if build path has a JRE 9 or above and the compliance level used is 1.6 or more. This also means Eclipse does not need EE descriptor file and the default access rules for JRE libraries are not required anymore. Bot of these things can be achieved by the <strong>–release</strong> option.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/java9_release_option_disabled_project_preference_page.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/java9_release_option_workspace_preference_page.png"></p>

<p><strong>7. Modules </strong> – Project Jigsaw introduced the Java 9 Platform Module System.
Module is a uniquely named reusable group of related packages, as well as resources and a module descriptor (module-info.java)<br>
All JDK Modules starts with "jdk.*". All Java SE Specifications Modules starts with "java.*". Java 9 Module System has a <strong>"java.base"</strong> Module which is an independent module.</p>
<p>There are many UI features supported in Eclipse to help the Java programmers use Java Modules.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/NewProject.PNG"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/CreateModuleInfo.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Configuremodule-info.png"></p>

<p>In the Eclipse IDE, a modular Java Project can be created using the New Java Project Menu which has by default Create module-info.java option set to true. Users can specify the module name for the Java Project. They can also create/update module-info.java file later using Configure Menu available.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/SearchModuleScope.png"></p>

<p>Java search now includes a new search scope – Module.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Is_modular.png"></p>

<p>Java Build Path allows the addition of Projects and jars to Modulepath or Classpath.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/IsNotModular.png"></p>

<p>Is modular node is changed to Is not modular if the Build Path Project library is moved to Classpath.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Is_modular_Contents.PNG"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Is_modular_Details.png"></p>

<p>Encapsulation properties of Modulepath elements can be modified using Is modular node. The modules can be limited by adding the modules explicitly, the --add-reads and --add-exports ca be added to the packages in a module. It can be used to patch a module.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix1.png"></p>

<p>Eclipse IDE provides a quick fix in module-info.java to import class and add requires to module-info.java.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix5.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix4.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix6.png"></p>

<p>If a buildpath entry specified in Classpath is used in module-info.java, the Eclipse IDE will suggest moving of the classpath entry to modulepath.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix2.png"></p>

<p>Eclipse IDE provides a quick fix in a class to add requires to module-info.java.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/QuickFix3.png"></p>

<p>Eclipse workspace provides a quick fix in import to add requires to module-info.java.</p>

<p><strong>8. Launching with Java 9 and beyond</strong> – As the Java Build Path page supports Module Path and Class Path, Launch Configuration Dialog for Java 9 and beyond has added this support via the <strong>Dependencies</strong> tab.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Dependencies1.PNG"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Dependencies2.PNG"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Dependencies3.PNG"></p>

<p><strong>9. Overriding dependencies while launching for Java 9 and beyond</strong> – Override Dependency dialog allows a user to override the encapsulation rules like patching, adding exports, adding reads, limiting modules for launching.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Dependencies4.PNG"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Dependencies5.PNG"></p>

<p>The dialog shows the dependencies derived from the build path and gives the option to override them.</p>

<p>Here's an example of mixing non-modular code in the named module:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/OverrideTestDependency.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/TestsrcPE.png"></p>

<p>A Junit 5 test file in the test folder resides out of the module scope and needs to patch the module and --add-reads for the module to ALL-UNNAMED.</p>           

<h2>Java 10 features</h2>
<p><strong>1. Local variable type inference support for Java 10</strong> – Java 9 extends type inference to declarations of local variables with initializers.</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/varTestJava10.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/varcompletion.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/hover.png"></p>
  
<p>Eclipse IDE supports the autocompletion of <strong>var</strong>, it shows the inferred type on the hover.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/suggestion.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/suggestion2.png"></p>

<p>Eclipse IDE suggests the usage of var or the inferred type to be used instead.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/Lambdavar.PNG"></p>

<p>Eclipse IDE also suggests changing the compliance to 10 if the var that is used in a Project has a compliance level lower than 10.</p>

<p><strong>2. Time-based release versioning</strong> - As Java will have two releases a year with specified release versions, the Eclipse IDE allows to add future Java versions with a warning.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/FutureJDK.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/FutureJDKdefault.png"></p>

<p>A Java program can also be launched with future versions of Java.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/AlternateJRE.png"></p>

<h2>Conclusion</h2>
<p>This article covers some of the important features of Java 9 and Java 10 supported by Eclipse JDT. Some more details can be found at:</p>
	<ul>
        <li><a target="_blank" href="https://www.eclipse.org/eclipse/news/4.8/jdt.html">Eclipse Photon JDT New and Noteworthy</a></li>
        <li><a target="_blank" href="https://wiki.eclipse.org/Java9/Examples">Java 9 Examples</a></li>
        <li><a target="_blank" href="https://wiki.eclipse.org/Java10/Examples">Java 10 Examples</a></li>
	</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/sarika.png"
        alt="Sarika Sinha" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Sarika Sinha<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/SarikaSinha1">Twitter</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>