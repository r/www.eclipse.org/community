<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The <a target="_blank" href="https://www.eclipse.org/viatra/">Eclipse VIATRA</a> project provides a model query and transformation framework that helps to move information back and forth between various documents and models in the most efficient way. The framework supports the <b>reactive programming</b> paradigm by event-driven transformations that can happen on-the-fly as the models change.</p>

<p align="center"><a target="_blank" href="https://www.eclipse.org/viatra/"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/viatralogo.png"></a></p>

<p>While trivial transformations (for instance copying pieces of data from one document to another) are easy to solve with any programming language, the task quickly becomes too challenging for those who need to enforce complex <b>design rules</b> or produce <b>live reports</b>.</p>
 
<p>VIATRA is a long-running project that incorporates the results of over 15 years of active research and development to solve this issue. The framework offers an efficient <b>Java runtime library</b> that is easy to embed, e.g. with limited dependencies. In addition to that, dedicated domain-specific <b>query and transformation languages</b> are available, motivated by the concepts of <b>graph patterns</b> and <b>graph transformations</b>.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/pattern_code.png"></p>
<p align="center"><i>Listing 1: A query over state machines expressed as a graph pattern</i></p>

<p>To illustrate the capabilities of VIATRA, a complete model case study called <b>CPS demonstrator</b> was created. The showcase includes model queries, transformations in multiple styles (batch, live, target incremental, etc.) and a model generator. Furthermore, the ease of embedding VIATRA is demonstrated by creating a dedicated modeling application that integrates all these features.</p>

<p>Query and transformation performance depends on the domain complexity and the size of the models. VIATRA was already successfully applied to complex domains such as UML or AutoSAR and handled models up to millions of model elements. To achieve this kind of <b>scalability</b>, it is possible to configure the execution in detail, including the selection of the algorithm used by the model queries.</p>

<p>The queries and transformation available in the CPS demonstrator are also used to benchmark the capabilities of the framework. The <a target="_blank" href="https://github.com/viatra/viatra-cps-benchmark">benchmark code is available</a> and is executed regularly to detect performance regressions during development.</p>

<p>To learn more about the VIATRA framework, we suggest having a look at the <a target="_blank" href="https://www.eclipse.org/viatra/documentation/">comprehensive documentation</a> of the project, especially the <a target="_blank" href="https://www.eclipse.org/viatra/documentation/tutorial.html">getting started tutorial</a>.</p>

<h2>What’s new in VIATRA 2.0?</h2>
<p>Eclipse Photon will include version 2.0 of the VIATRA framework. The main goals of the new release were to both simplify and modernize the existing code of the project.</p>
<p>One aspect of this modernization was to require Java 8 as a minimum dependency, allowing features such as lazily calculated query results via a stream-friendly API. Naturally, it was also seen to that everything works with Java 9 and 10 as well.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/initialize_matcher.png"></p>
<p align="center"><i>Listing 2: Processing the results of the query using the new Stream API</i></p>

<p>The query language was also enhanced with a few new features that makes pattern definitions simpler, e.g. by reducing the need for trivial patterns like ones enumerating all instances of a given class. In addition to these changes, version 2.0 includes fixes and performance optimizations to ensure VIATRA works as expected in as many environments and workloads as possible.</p>

<p>Of course the development of VIATRA will not stop after the release. In fact, the planning of VIATRA 2.1 has already begun. We plan to maintain our cadence of two new feature releases per year after Photon, so VIATRA 2.1 is planned for Eclipse 2018-12. With regards to planned features, work has begun on a graphical query editor using Sirius. This editor will provide an alternate way to create and review model queries while maintaining compatibility with the existing runtime.</p>

<h2>Using VIATRA beyond EMF</h2>
<p>Although VIATRA was originally designed for the needs of model-driven engineering tools based on the Eclipse Modeling Framework (EMF), the VIATRA core itself can be applied to other modeling or engineering tools. Version 2.0 introduces new API enhancements that make it easier to develop such model access connectors in additional, heterogeneous technological spaces.</p>
 
<p>Based on these capabilities, several open source and commercial adaptations have been developed, including the <a target="_blank" href="https://szabta89.github.io/projects/inca.html">INCA tool</a> that analyses software code created in IDEs including <a target="_blank" href="http://mbeddr.com/">mbeddr</a>. Recently, VIATRA 2.0 was also successfully integrated with MagicDraw and Teamwork Cloud, a popular commercial UML/SysML modeling and collaboration platform. Some aspects of this integration have been released as open source (<a target="_blank" href="http://github.com/viatra/v4md">V4MD</a>), while others empower commercial products aimed at scalable cloud-based modeling (<a target="_blank" href="http://iq4md.incquerylabs.com">IncQuery for MagicDraw and the IncQuery Server</a>). To learn more about these developments, check out our <a target="_blank" href="https://www.eclipsecon.org/france2018/session/lessons-learned-building-eclipse-based-add-ons-commercial-modeling-tools">EclipseCon France 2018</a> presentation.</p> 

<h2>Links</h2>
    <ul>
        <li><a target="_blank" href="https://www.eclipse.org/viatra/documentation/tutorial.html">Tutorial</a></li>
        <li><a target="_blank" href="https://www.eclipse.org/viatra/documentation/releases.html#viatra-20">Changelog</a></li>
        <li><a target="_blank" href="https://projects.eclipse.org/projects/modeling.viatra/releases/2.0.0/bugs">List of issues fixed in VIATRA 2.0</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/france2018/session/lessons-learned-building-eclipse-based-add-ons-commercial-modeling-tools">EclipseCon France 2018 Talk</a> - including slides</li>
    </ul>
    
<div class="embed-responsive embed-responsive-16by9">    
<iframe width="560" height="315" src="https://www.youtube.com/embed/SkCrPbtxrsg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/istvan.jpg"
        alt="Istvan Rath" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            István Rath<br />
            <a target="_blank" href="https://incquerylabs.com/">IncQuery Labs</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/istvanrath">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/istvanrath/">LinkedIn</a></li>
          </ul>
        </div>
      </div>
     </div>  
     <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/zoltan.jpg"
        alt="Zoltan Ujhelyi" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Zoltán Ujhelyi<br />
            <a target="_blank" href="https://incquerylabs.com/">IncQuery Labs</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/stampiehun">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/ujhelyiz/">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
     <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
			<img class="img-responsive"
       		 src="/community/eclipse_newsletter/2018/june/images/gabor.jpeg"
       		 alt="Gabor Bergmann" />
        </div>
        		<div class="col-sm-16">
          <p class="author-name">
            Gábor Bergmann<br />
            <a target="_blank" href="https://incquerylabs.com/">IncQuery Labs</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.github.com/bergmanngabor">GitHub</a></li>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>