<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>With Eclipse Photon, the Eclipse ecosystem has consolidated its adherence to decoupled development tools, relying on Language Server Protocol and others. The examples of Eclipse projects interacting with the Language Server Protocol are numerous: LSP4J, LSP4E, aCute (C# edition in Eclipse IDE), Corrosion (Rust edition in Eclipse IDE), JDT.LS (Java edition in VSCode and Theia), Xtext, Ceylon, Che, Theia…</p>
<p>We believe that this trend and the rich noteworthy new features of <a target="_blank" href="https://projects.eclipse.org/projects/technology.lsp4e">Eclipse LSP4E</a> make it worth an interesting article. Enjoy ;)</p>

<h2>Support for Debug Adapter Protocol</h2>
<p>LSP4E now supports the <a target="_blank" href="https://code.visualstudio.com/docs/extensions/example-debuggers">Debug Adapter Protocol</a> created for VS Code. Similar to the language server protocol, this now means tool developers may reuse debug adapters developed for the debug protocol and have them automatically integrated with Eclipse debugger functionality.</p> 

<p align="center"><a target="_blank"href="/community/eclipse_newsletter/2018/june/images/lsp_at_eclipse.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/lsp_at_eclipse_sm.jpg"></a></p>

<h2>Debug Protocol Overview</h2>
<p>The debug protocol provides a generic interface based on JSON between the Client (IDE or editor) and the Server (the debugger or an adapter to the debugger).</p>
<p>The protocol consists of:</p>
    <ul>
        <li>Requests e.g. GoToRequest, SetBreakpointRequest, VariablesRequest</li>
        <li>Responses e.g. GoToResponse, SetBreakpointResponse, VariablesResponse</li>
        <li>Events e.g. StoppedEvent, BreakpointEvent, ThreadEvent</li>
	</ul>

<p>For more details see the <a target="_blank" href="https://github.com/Microsoft/vscode-debugadapter-node/blob/master/debugProtocol.json">JSON</a> or <a target="_blank" href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j.debug/src/main/java/org/eclipse/lsp4j/debug/DebugProtocol.xtend">XTEND</a> version of the protocol.</p>

<h2>Integrating a Debug Adapter</h2>
<p>LSP4E provides a Debug Adapter launch configuration which allows tool developers to quickly launch their debug extension and test out how it would work in the IDE. Once experiments have been made a launch configuration for the specific debug adapter can be created.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/launch_python.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/launch_python_sm.png"></a></p>

<p>To try it out, install the Debug Adapter client for Eclipse IDE (Incubation) from the <a target="_blank" href="https://projects.eclipse.org/projects/technology.lsp4e/downloads"></a>update site into your Eclipse installation. You will also need a debug adapter. You can get one from VS Code (it your ~/.vscode/extensions directory).</p>

<p>Create a <i>Debug Adapter Launcher</i> debug configuration.</p>
Fill in the <i>Command</i> and command line <i>Arguments</i> for the debug adapter.
<p style="word-wrap: break-word;">For example, the Command is likely to be the path to node and the argument to the extension's JavaScript. In the screenshot above, this is <code>HOME/.vscode/extensions/ms-python.python-VERSION/out/clint.debugger.Main.js</code> for using Python, with HOME and VERSION adapted to your machine.</p>
<p>Fill in the <i>Launch Parameters (Json)</i> with the bit of JSON that your specific launch adapter understands.</p>

<p>To create your own Eclipse plug-in that uses LSP4E’s Debug implementation you have two choices:</p>
	<ol>	
		<li>Contribute to the <a target="_blank" href="http://help.eclipse.org/photon/topic/org.eclipse.platform.doc.isv/reference/extension-points/org_eclipse_debug_core_launchConfigurationTypes.html">launch configuration</a> extension point, using the LSP4E’s <a target="_blank" href="http://git.eclipse.org/c/lsp4e/lsp4e.git/tree/org.eclipse.lsp4e.debug/src/org/eclipse/lsp4e/debug/launcher/DSPLaunchDelegate.java">DSPLaunchDelegate</a> as the delegate. This requires saving into the launch configuration command, arguments and json launch parameters using the launch configuration keys defined in the <a target="_blank" href="http://git.eclipse.org/c/lsp4e/lsp4e.git/tree/org.eclipse.lsp4e.debug/src/org/eclipse/lsp4e/debug/DSPPlugin.java">DSPPlugin</a>.</li>
		<li>Contribute to the launch configuration extension point using a custom <a target="_blank" href="http://help.eclipse.org/photon/topic/org.eclipse.platform.doc.isv/reference/api/org/eclipse/debug/core/model/LaunchConfigurationDelegate.html">launch delegate</a>. Within the launch delegate, create a <a target="_blank" href="http://git.eclipse.org/c/lsp4e/lsp4e.git/tree/org.eclipse.lsp4e.debug/src/org/eclipse/lsp4e/debug/debugmodel/DSPDebugTarget.java">DSPDebugTarget</a> and call initialize() on it to start the processing.</li>
	</ol>
<p>The advantage to option 1 is more reuse of some tricky code to launch and trace a debug adapter. The advantage of option 2 is DSPDebugTarget receives a Map and therefore clients of DSPDebugTarget do not have to use/store JSON in their launch configuration.</p>

<h2>Using a Debug Adapter</h2>
<p>From the end user perspective, nothing changes. They don’t have to do anything different to work with debugger extensions - just debug as normal. Today the debug protocol supports all essential debugging features, but does not support some advanced features of Eclipse debugger. We welcome contributors who want to work drive this forward with the community, see below for how to get involved!</p>

<h2>Summary</h2>
<p>The debug protocol implementation in LSP4E enables Eclipse IDE to quickly unlock support for new debuggers and debugger features. It also provides a way to support custom debuggers written in different programming languages. Tool developers can develop debug features in an agile and more efficient way for their users and keep up with the pace of fast-changing technology.</p> 

<h2>Support for latest version of the Language Server Protocol</h2>
<p>As usual, LSP4E also comes with support for the latest version of the protocol by adopting the latest version of LSP4J API. As such, compared to previous 0.4.0 version, it includes support for interesting new operations and options. The most notable are supports for:</p>
	<ul>
        <li>Dynamic command registrations</li>
        <li>Support for external or HTTP links in completion and hover</li>
        <li>Better support for CodeLens. With Eclipse Platform now shipping good support for “code mining”, LSP4E can now render CodeLens in a very nice way as code annotations. OCaml Language Server gives type inference as CodeLens, rendered in Eclipse IDE</li>
    </ul>    
            <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/let_example.png"></p>
    <ul>    
<li><b>Multi-root workspaces</b>. Last year, language-server based architectures were not able to <b>scale</b> at the size of typical Eclipse IDE workspace with dozens of distinct projects, as LSP spec suffered from a multiplicity issue: each server was expected to be used by a single project. The LSP specification has evolved to support this, and LSP4E immediately made the necessary adaptations to adopt this new important feature.</li>
	</ul>
 
<h2>More control and more logs for users and developers</h2>
<p>In order to better enable complex language servers whose lifecycle is very dynamic, LSP4E has added the capability for LS integrators to refine when their LS is enabled for a given file, using a typical enabledWhen expression on the extension point.</p>
<p>LSP4E also now offers end users the possibility to disable a language server for a given file-type. It can be convenient for cases where the user has multiple extensions installed for a given file, one being based on language-server and finds that the one coming from language server is of inferior quality compared to the other one. Then, the user can disable the Language Server for those files in one click.</p>
    
    
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/ls_html.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/ls_html_sm.png"></a></p>

<p>And finally, to help Language Server and LSP4E developers and integrators in troubleshooting their integrations, some easy to trigger logging capabilities were added in LSP4E. A dedicated preference page allows to control them trivially.
</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/logs.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/logs_sm.png"></a></p>

<h2>Possible integration with Docker</h2>
<p>During the last release, LSP4E developers could also investigate possible integration with Docker-based language servers, and validate that with minimal effort, it’s possible to use a Docker image as a language server, including images developed for Eclipse Che. See the following demo:</p>

<iframe class="tscplayer_inline embeddedObject" name="tsc_player" scrolling="no" frameborder="0" type="text/html" style="overflow:hidden;" src="https://www.screencast.com/users/mistria/folders/Default/media/ab3a1d91-3f36-4ba1-85bb-657b22c3db3f/embed" height="400" width="700" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

<p>Some next steps may happen during the summer on this topic. The project mailing-list is the best place to keep in touch.</p>

<h2>LSP4E is your project too ;)</h2>
<p>As any Eclipse.org project, LSP4E is a really open project. It’s everyone’s project. So if you’d like to learn more about it, or to simply be aware of the main discussions, or get involved in its continuous development, here are the entry-points:</p>
	<ul>
        <li>The project mailing-list <a target="_blank" href="https://dev.eclipse.org/mailman/listinfo/lsp4e-dev">lsp4e-dev</a>.</li>
        <li>The bugtracker component is LSP4E, if you want to <a target="_blank" href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Technology&list_id=17598116&product=lsp4e&query_format=advanced&resolution=---">see open bugs</a> (where your help is welcome) or <a target="_blank" href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=lsp4e">report a new bug/enhancement proposal</a>.</li>
        <li>LSP4E <a target="_blank" href="http://git.eclipse.org/c/lsp4e/lsp4e.git/tree/CONTRIBUTING.md">contribution guide</a> and <a target="_blank" href="http://git.eclipse.org/c/lsp4e/lsp4e.git">Git repo</a>. LSP4E uses the <a target="_blank" href="https://git.eclipse.org/r/#/q/project:lsp4e/lsp4e">awesome Gerrit code-review system</a> to handle code contributions and reviews.</li>
	</ul>
	
<p>It’s also important to acknowledge to work of many contributors to this release: Angelo Zerr, Alex Boyko, Elliotte Harold, Jonah Graham, Kris De Volder, Lucas Bullen, Lucia Jelinkova, Markus Ofterdinger, Martin Lippert, Mickael Istria, Philip Alldredge, Rastislav Wagner and Remy Suen.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
 		<img class="img-responsive" src="/community/eclipse_newsletter/2016/november/images/jonahg.jpg" alt="Jonah Graham" />
            </div>
            <div class="col-sm-16">
              <p class="author-name">
                 Jonah Graham<br />
                <a target="_blank" href="https://kichwacoders.com/">Kichwa Coders</a>
              </p>
              <ul class="list-inline">
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://twitter.com/JonahGrahamKC">Twitter</a></li>
                <li><a class="btn btn-sm btn-warning" target="_blank" href="https://www.linkedin.com/in/jonahgraham">LinkedIn</a></li>
                <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div> 
         <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
       <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/may/images/mickael.jpeg"
          alt="Mickael Istria" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Mickael Istria<br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/mickaelistria">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mickaelistria">Twitter</a></li>
            <?php //echo $og; ?>
      </ul> 
      </div>
      </div>
    </div>
   </div>
</div>