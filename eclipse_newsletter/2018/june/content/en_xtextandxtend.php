<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2>The journey of Tina Toolsmith</h2>
<p>Tina Toolsmith is one of those typical developers. She loves to code in the office, on the train, during a long flight or even while sitting on her sunny terrace during her vacation. Tina takes great pleasure in using the latest programming languages, frameworks, and IDEs for manufacturing innovative solutions. Essential parts of her toolbox are Xtext and Xtend which she uses to tailor domain-specific languages for a wide variety of usage scenarios. However, the last two releases of <a target="_blank" href="https://www.eclipse.org/Xtext/">Xtext</a> and <a target="_blank" href="https://www.eclipse.org/xtend/">Xtend</a> have been difficult for Tina, since they were mainly concerned with bug fixes and internal performance improvements and offered only a limited amount of cutting-edge features. The good news for Tina is that the 2.14 release comes with a whole bunch of new features and support for the latest framework versions Xtext and Xtend are build upon. So join Tina's journey to explore the new Xtext and Xtend features.</p>

<h2>An Evolved Foundation</h2>
<p>Since Tina is a classic early adopter she is always using the latest Java version. Therefore, Tina is really excited about the 2.14 release since it comes with the ability to compile with and against Java 9 &amp; 10. Of course, she is aware of the prerequisites and restrictions:</p>
    <ul>
        <li>Currently, there is no native support for Java 9/10</li>
        <li>A current Maven version (has been tested with 3.5)</li>
        <li>Gradle must be at least version 4.7</li>
        <li>Java 9 requires Tycho 1.1 or newer and Java 10 requires Tycho 1.2</li>
        <li>Java 9 with at least Oxygen.1a and Java 10 with at least Oxygen.3a (As an early adapter, Tina, of course, goes for Eclipse Photon that works for both)</li>
        <li>Java 10 requires <code>org.objectweb.asm</code> library in version 6.1.1</li>
    </ul>
<p>After realizing that fulfilling all the prerequisites is a piece of cake for her, she discovers another great feature. Xtext 2.14 ships with support for JUnit 5 which itself comes with a lot of <a target="_blank" href="https://junit.org/junit5/docs/snapshot/release-notes/index.html">new features</a>. Tina is very delighted to see that the project wizard was updated accordingly. Moreover, the projects created by the wizard only depend on the JUnit 5 artifact (<code>org.junit.jupiter:junit-jupiter-api:5.0.0</code>). Thereby, all dependencies are configured by the JUnit library container which lowers the coupling to concrete JUnit versions.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_wizard_junit5.gif"></p>

<p>Before continuing her journey, she reminds herself to remember this <a target="_blank" href="https://blogs.itemis.com/en/xtext-2.14-adds-support-for-junit-5">blog post which offers interesting details on the JUnit 5 integration</a>.</p>

<h2>Exploring the Grammar Enhancements</h2>
<p>After updating the technical foundation, Tina is excited to see what Xtext 2.14 offers for the foundation of every Xtext language - the grammar. She is very happy to see that Xtext 2.14 comes with a <code>@Deprecated</code> annotation which Tina missed from the beginning.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_annotation_deprecated.png"></p>

<p>In addition, she is grateful for being able to customize the severity of the message displayed when referring to a deprecated rule through the Xtext preference page.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_annotation_preference.png"></p>

<p>Tina is even more delighted to see that with the <code>@Final</code> annotation it is now possible to prevent certain rules from being overwritten.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_annotation_final.png"></p>

<p style="word-wrap: break-word;">The third new grammar feature that attracts Tina’s attention is the brand-new <code>@Exported</code> annotation. If Tina wanted to export features to the index in the past, she had to manually come up with an <code>IDefaultResourceDescriptionStrategy</code> implementation. Now using the @Exported annotation will generate the basic infrastructure to publish custom objects to the index. All Tina has to keep in mind is that only those grammar rules annotated with <code>@Exported</code> will be available on the index.</p>

<h2>Examining the Eclipse Integration</h2>
<p>Having tested the new grammar features Tina is thrilled to see what enhancements were made regarding the Eclipse integration.</p>

<p>At first sight, she cannot believe what she is seeing. After two releases of only a few new features, she finally found something that makes her early adoapter heart beat faster. Xtext 2.14 is one of the first to integrate the newly introduced Code Mining feature. The API allows Tina to add additional semantic information into the text, which is not part of the text itself. Tina thoughts start to run. There are so many situations in which she can leverage Code Mining.</p>
    <ul>
         <li>Show the inferred return type of a function based on its content.</li>
         <li>Display parameter names in the context of a function call.</li>
         <li>Present the number of incoming or outgoing references of a method.</li>
         <li>...</li>
    </ul>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_domainmodel-codemining.gif"></p>

<p style="word-wrap: break-word;">Tina only has to add the <code>org.eclipse.xtext.xtext.generator.ui.codemining.CodeMiningFragment</code> to her generator workflow and all the required dependencies and a stub implementation will be added to her Xtext project. Tina also realizes that there is a <a target="_blank" href="https://blogs.itemis.com/en/code-mining-support-in-xtext">blog post providing more details on how to leverage code mining</a>. But before she jumps right in to get her hands dirty, she wants to continue her journey of exploring all the new Xtext features.</p>

<p>While inspecting the next feature Tina realizes that she took the right decision. Xtext 2.14 ships with support for specifying "New Project" and "New File" wizards without the need to write tons of SWT code. Tina only has to add two simple modules to her generator workflow and the infrastructure encapsulating the UI implementation for creating Eclipse projects and files for her specific DSL is generated.</p>

<pre style="background:#fff;color:#000">language <span style="color:#ff7800">=</span> StandardLanguage {
    <span style="color:#3b5bb5">name</span> <span style="color:#ff7800">=</span> <span style="color:#409b1c">"org.xtext.example.mydsl.MyDsl"</span>
    fileExtensions <span style="color:#ff7800">=</span> <span style="color:#409b1c">"mydsl"</span>
        
    fileWizard <span style="color:#ff7800">=</span> {
        generate <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">true</span>
    }
    projectWizard <span style="color:#ff7800">=</span> {
        generate <span style="color:#ff7800">=</span> <span style="color:#3b5bb5">true</span>
    }
}
</pre>

<p>Instead of fiddling around with SWT widgets, the user interface for "New Project" and "New File" wizard can be defined using a concise API. Moreover, the API allows Tina to specify the structure of the projects and the content of the files with so-called templates. Based on the descriptions seamlessly integrated wizards will be provided in the runtime. The wizard to create projects, for example, allows selecting from the templates specified by Tina.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_new-project-wizard-template-selection.png"></p>

<p>The wizard to create files is more simple in its basic form and skips the template selection if there is - as in most cases - only one file template.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_new-file-wizard.png"></p>

<p>At this point, Tina realizes that the number of new features in Xtext 2.14 is overwhelming. So before she continues to inspect the other cool new stuff, she notes the blog posts for the <a target="_blank" href="https://blogs.itemis.com/en/generating-a-new-project-wizard-with-xtext-2.14">"New Project" wizard</a> and the <a target="_blank" href="https://blogs.itemis.com/en/new-file-wizard-easy-to-generate-with-xtext-2.14">"New File" wizard</a> which provide more detailed information on the matter.</p>

<p>Continuing her exploration of new Xtext features she discovers the Create Action Quickfix. Tina knows from a lot of situations like the one displayed underneath. Grammar rules that can be consumed without object instantiation usually require a <i>Create Action</i>. For example, a grammar rule that only contains an optional assignment will be marked with a warning message by the Xtext editor. Tina is really glad that with Xtext 2.14 the warning message is enhanced by a quick fix that automatically inserts the proper <i>Create Action</i>.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_create-action.gif"></p>

<p>Although Tina has witnessed a lot of large new features, she still has an eye for the detail. While experimenting with the new Xtext 2.14 she realizes that the Xtext Build console finally got a proper icon.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_console-icon.png"></p>

<h2>Accelerating Tina’s journey</h2>
<p>Tina as a passionate Xtext user implements domain-specific language workbenches at an industrial scale. Therefore, Tina is euphoric about the performance improvements incorporated in Xtext 2.14. One pain point she experienced in the past was caused by the access to <code>IOutputConfigurationfrom</code> the builder participant. The problem that depends on the number of files produced by code generators is now fixed by cache implementation within the <code>BuilderParticipant</code>.</p>
<p>Further, the improvements for <code>QueuedBuiledData</code> regarding build Timenaes for a large number of resources will increase performance in Tinas industry scale projects. Finally, Tina is looking forward to the performance improvements made in the area of semantic highlighting. She is keen to see the positive effects when editing large Xtend files. Since Tinae is thinking about Xtend, she decides to move on to the improvements made in release 2.14.</p>

<h2>What's new in Xtend - The journey continues</h2>
<p>After going through all the new features and functions in Xtext, Tina is keen about the improvements included in Xtend 2.14. Before she starts exploring the features in detail she notes with satisfaction that Xtend also supports Java 9 &amp; 10. Having secured that Xtend is built on the same solid foundation as Xtext, Tina takes a closer look at the Xtend compiler feature.</p>

<p>He quickly realizes that Xtend now supports Multi-Catch support. Multi-Catch support allows the handling of multiple Exception classes in a single catch block.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_0_xtend-multi-catch-new.png"></p>

<p>Given that Tina is an early adopter she founds herself thinking: "Been there. Done that." However, she has to admit that applying the same concept to the type guard expressions of switch statements is exactly the kind of syntactical sugar she loves about Xtend.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_0_xtend-multi-type-guard-new.png"></p>

<p>Tina is a huge fan of the concise and semantically rich Xtend language. Especially avoiding unnecessary modifiers, such as "public" or "final" is a feature she uses extensively. However, every once in a while she falls back to old routines and explicitly specifies unnecessary modifiers. Therefore, Tina is really grateful as she realizes that Xtend 2.14 now marks unnecessary modifiers and even better, offers a quick-fix to remove them.</p>

<p align="center"><a href="/community/eclipse_newsletter/2018/june/images/2_14_unnecessary_modifier.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/2_14_unnecessary_modifier_sm.png"></a></p>

<p>Once again, Tina is delighted to see that the severity for the unnecessary modifier markers can be customized through a preference page. She quickly writes down the link to this <a target="_blank" href="https://blogs.itemis.com/en/xtend-2-14-unnecessary-modifiers-validation">blog post that describes the unnecessary modifier feature</a> in more detail.</p>

<h2>One journey ends, but many more will follow!</h2>
<p>Having explored all the interesting new features of Xtext and Xtend 2.14 Tina is excited to quickly use them all. Before she leaves her laptop to grab another coffee and plan the next steps for leveraging the new features she realizes two changes in the Xtext team. With <a target="_blank" href="https://blogs.itemis.com/en/becoming-xtext-co-project-lead">Christian Dietrich becoming co-project</a> lead after more than 10 years of supporting and enhancing Xtext the project envisions a prosper and feature-rich future. Additionally, Arne Deutsch who added more than 60 pull requests in the last 12 month was <a target="_blank" href="https://projects.eclipse.org/projects/modeling.tmf.xtext/elections/election-arne-deutsch-committer-eclipse-xtext">elected as new committer</a> by the Xtext team. Witnessing the on-going support and commitment of all Xtext contributors reaffirms Tinas wish to experience more journeys like the one she just finished.</p>

<h2>Start your own journey</h2>
<p>If you find yourself thinking I would love to take a trip like Tina did, feel free to explore all the details of the Xtext 2.14 release starting from the <a target="_blank" href="https://www.eclipse.org/Xtext/releasenotes.html">Xtext</a> or <a target="_blank" href="https://www.eclipse.org/xtend/releasenotes.html">Xtend</a> release notes. In case you are more of a guided tour person, you should watch the <a target="_blank" href="https://www.youtube.com/watch?v=Isxverh09e4">webinar held by the Xtext committers Karsten Thoms and Holger Schill</a>. Alternatively, if you want to add a social component to your journey and you happen to be around in Bonn on July 9th join the <a target="_blank" href="https://wiki.eclipse.org/Eclipse_DemoCamps_2018/Bonn">Eclipse DemoCamp</a> which will also have a focus on the Xtext 2.14 release.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/march/images/hendrik.jpg"
        alt="Hendrik Bunder" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Hendrik Bünder<br />
            <a target="_blank" href="https://www.itemis.com/de/">itemis AG</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blogs.itemis.com/author/hendrik-b%C3%BCnder">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/hendrik-b%C3%BCnder-b17b4b81/">LinkedIn</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>