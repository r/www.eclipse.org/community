<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p><a target="_blank" href="https://www.eclipse.org/sirius/">Eclipse Sirius</a> is a framework to easily and quickly create a graphical modeling workbench dedicated to your domain specific language.</p>

This year at <a target="_blank" href="https://www.obeo.fr/en">Obeo</a>, we started working on two aspects: <b>prepare the future of Sirius</b> &amp; provide <b>new features for the upcoming 6.0 release</b> which is part of the Photon release train.

<h2>Ready for Photon? Sirius 6.0 is there for you!</h2>

<p>We have added several new features:</p>

<ul>
	<li><b>Support for background color on diagrams</b>: it is now possible to dynamically compute the color of the diagram according to the state of the model. This is a small feature, but it continues to expand the visual customization capabilities available to specifiers.</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/diagram_background.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/diagram_background_sm.png"></a></p>

<ul>
	<li><b>New "magic" edge creation tool</b>: All diagrams will now benefit for free from a new smart edge creation tool. Thanks to this end users no longer have to chase for the appropriate tool in the palette (which can contain many entries): just click on the source and target of the edge to create and Sirius will automatically detect which tools can be applied to them. If there is only one, the edge is created immediately. If several tools are possible as is the case of the animation, a menu opens to choose among the candidate tools.</li>
</ul>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/runtime_sirius_demo2.gif"></p>

<ul>
	<li><b>Quick navigation to service method implementation</b>: Real-world modelers often need to call into Java code to perform complex operations on models or call into Sirius and Eclipse APIs. This is very straightforward to do in Sirius with the notion of Java services that can be transparently invoked from AQL expressions. It is now possible to navigate from the expressions in the Sirius specification which invoke a service method directly into the corresponding Java code with a single keystroke.</li>
</ul>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/runtime_sirius_demo.gif"></p>

<ul>
	<li><b>Integration with ELK for improved diagram layouts</b>: Sirius has always proposed an automatic layout algorithm. Specifiers can now leverage the high-quality layout algorithms provided by the Eclipse ELK project. You can choose any of the algorithms proposed by ELK and tweak all their configuration parameters in the Sirius specification. The end users will transparently get a nicer layout when using the existing “Arrange All” action. This is experimental in Sirius 6.0. Give us feedback on which aspects to focus on for the future.</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/runtime_sirius.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/runtime_sirius_sm.png"></a></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/override_options.png"></p>

<h2>What's next?</h2>

<p>We are working on what would be the future of modeling tools. We already gathered some feedback from the community. You expect that modeling in the future would be: <b><i>fast, simple, easy, beautiful, cloud &amp; collaborative.</i></b></p>

<p>We believe that the concept of IDE is evolving. In the future, we will have more accessible tools and better tool integration. In the end, we think that tools should become IDE agnostic.  We will have more and more tools dedicated to specific domains. These tools would be available more broadly, for various kind of users on any kind of platforms. We need frameworks to ease the creation of such specific tools dedicated to one usage. That’s why we believe in frameworks such as Sirius.</p>

<p>Sirius is a really nice framework to create dedicated desktop workbenches based today on the Eclipse platform. Our purpose is to bring the spirit of Sirius to the cloud: easily develop modeling workbenches but rendered in a browser and integrable in any web application.</p>

<p>So where are we today? We are going step by step and work on different aspects.</p>

<ol>
	<li><b>Introduce web technologies in existing Eclipse views</b>: Our first step is to introduce web technologies in existing Eclipse views. This approach is used to provide a brand new feature in Sirius 6.0 called Workflow which allows specifiers to guide users through the usage of their workbench. On the left of the following screenshot, we see a Sirius configuration file which defines a workflow with different actions using this new DSL. To the end-user, these are rendered as web elements inside the Eclipse view. Note that this new feature is still experimental in 6.0, and will be improved for version 6.1 this fall.</li>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/webcomponents.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/webcomponents_sm.png"></a></p>

	<li><b>Make Sirius independent from Eclipse platform</b>: The idea is to make the Sirius code base more modular, isolating the core business concerns from the current Eclipse-based technology stack: the Eclipse UI, GMF, the workspace, and even the Eclipse Runtime itself. We will work on this progressively over several versions, making sure Sirius always keeps working even in the classical Eclipse-based context. The end goal is to get a set of core components that can be reused both in Eclipse and, more to the point, inside a web server exposing its services to any web client through a well-defined protocol.</li>

<li><b><a target="_blank" href="https://youtu.be/Ua3-93O3TRs">Render Sirius diagrams in a browser</a></b>: Based on a classical Sirius configuration, we can render in a browser the graphical elements of a diagram. As usual, the specifier can work on the look and feel of his modeler iteratively. This prototype is based on Sprotty. <a target="_blank" href="https://projects.eclipse.org/proposals/sprotty">Sprotty</a> is a new project proposed to the Eclipse Foundation by TypeFox. It is a small, lightweight, open source &amp; well architectured JavaScript graphical library providing rendering in SVG & well integrated with Eclipse ELK which provides auto-layout.</li>

<li><b>Sirius integrated with Cloud IDEs</b>: Sirius in a near future will be IDE-agnostic. Then you will be able to integrate Sirius based workbenches in any web application or any IDE. Have a look at the prototypes we already have of <a target="_blank" href="https://youtu.be/CB22Uqz8hLg">Sirius integrated in Eclipse Theia</a> and of <a target="_blank" href="https://youtu.be/B6aCqywKpyY">Sirius integrated in Eclipse Che</a>.</li>
</ol>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/sirius_components.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/sirius_components_sm.png"></a></p>

<p>To sum up, we will keep working on the existing Sirius project and we will reintegrate the new web-related features and components. This summer we will continue our work on the modularization of the architecture. And for the 6.1 this fall, we will contribute a first version of web based diagrams based on the Graphical Server Protocol.</p> 

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/diagram_workflow.png"></p>


<p>At Obeo, we’re taking a community-first approach to influence the development of the next generation of modeling tools. Please <a target="_blank" href="https://www.obeo.fr/en/contact">tell us</a> what you want! We have lots of ideas for the future of Sirius. But what we need is to know what YOU need. So please Speak Up!</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/OQRFUyBt1r4" frameborder="0" allowfullscreen></iframe>
</div>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/melanie.jpg"
        alt="Melanie Bats" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            M&eacute;lanie Bats<br />
            <a target="_blank" href="https://www.obeo.fr/en">Obeo</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/melaniebats">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/mbats">GitHub</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>