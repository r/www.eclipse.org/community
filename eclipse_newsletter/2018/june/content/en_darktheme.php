<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>A few years ago, in 2014, Eclipse IDE announced that it will ship a default dark theme. In 2014, many of restrictions existed in the CSS and SWT styling of the Eclipse workspace and the result was not very usable. But the Eclipse platform team wanted to lay the foundation for improvements without the requirement to install external components to test and to drive these improvements. The releases following the initial release constantly improved the capabilities of the underlying CSS engine and the SWT toolkit to enhance the user experience.</p>

<p>With Eclipse Photon, we are shipping the best support for the dark theme so far. Continue to read to learn about the improvements in the platform and in the second part, how plug-in developers can enable their Eclipse contributions for the dark theme.<p>

<h2>Try it out</h2>
<p>Someone once said, that all the cool kids are using a dark theme these days. If you want to join this club, simply type "Dark" into the Quick Access box and press enter. This brings you to the correct preference page. Select Dark, press Apply and Close (and restart your IDE to fully apply the theme).</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/preferences.png"></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/appearance.png"></p>

<h2>Selected Dark theme improvements in 4.8</h2>
<p>Sometimes the small things do matter.  Eclipse 4.8 continues to improve the default text editor. The Eclipse default dark theme now includes styling for the text editor's range indicator and uses transparency for the expand and collapse buttons.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/transparency.png"></p>

<p>To improve readability in the dark theme, bold style usage has been reduced in the Java editor and some colors that were too close to each other have been altered.</p> 

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/colors.png"></p>

<p>Lots of icons have been adjusted to look better in both the light as well as the dark theme.</p>

<p>For example, the block selection, word wrap and show whitespace icons have been adjusted.</p>

<p>Before:</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/toolbarbefore.png"></p>

<p>After:</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/toolbarafter.png"></p>

<p>Popup dialogs, for example the platform's update notification popup, now use a dark background and a light foreground color in the dark theme.</p>


<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/update.png"></p>

<p>The same work has been done for the Javadoc popup which makes the dialog much easier to read in the dark theme. Support for other languages will most likely pick this up for the next release, e.g., the PHP team is working on this in <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=534520">https://bugs.eclipse.org/bugs/show_bug.cgi?id=534520</a>.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/override.png"></p>

<p>Links now consistently use a light blue color in the dark theme. One example where this was very visible is the PDE's manifest editor.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/changes2.png"></p>
<p align="center"><a target="_blank" href="https://www.eclipse.org/eclipse/news/4.8/platform.php#tree-table-scaling">Improved Tree and Table widget scaling at high DPI on Windows</a></p>

<p>The colors of links in code element information control now take the color settings of the <b>Hyperlink text color</b> and the <b>Active hyperlink text color</b> from the <b>Colors &amp; Fonts</b> preference page into account. The readability in the dark theme has been improved a lot by this.</p>

<p>Before:</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/testclass.png"></p>

<p>After:</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/testclass_after.png"></p>

<p>Also multiple extensions like the Gradle tooling for Eclipse have improved their styling.</p> 
<p>For example, this is how the Gradle preference page looked before Photon.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/gradle_before.png"></p>

<p>This page is nicely styled in the Photon release.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/gradle.png"></p>

<p>The Gradle tooling initially designed a relatively complex solution to support the dark theme. The final solution is really simple (see <a target="_blank" href="https://github.com/eclipse/buildship/pull/688">https://github.com/eclipse/buildship/pull/688</a>) and the next chapter is designed to help other plug-in developers to apply the same solution for their plug-ins.</p>

<h2>Guide for plug-in developers to contribute to the default dark theme</h2>
<p>Plug-in developers can easily support the dark theme as any plug-in can contribute to the default dark theme.</p> 

<p>To contribute a CSS file to the dark theme, you can write an extension in your plugin.xml similar to the following. The refid="org.eclipse.e4.ui.css.theme.e4_dark" points to the platform dark theme and tells the CSS engine to extend it.</p>
 
<pre style="background:#fff;color:#000"><span style="color:#3a4a64">&lt;<span style="color:#3a4a64">extension</span>
        <span style="color:#3a4a64">point</span>=<span style="color:#409b1c">"org.eclipse.e4.ui.css.swt.theme"</span>></span>
    <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">stylesheet</span>
            <span style="color:#3a4a64">uri</span>=<span style="color:#409b1c">"css/your_dark_extensions.css"</span>></span>
        <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">themeid</span>
            <span style="color:#3a4a64">refid</span>=<span style="color:#409b1c">"org.eclipse.e4.ui.css.theme.e4_dark"</span>></span>
        <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">themeid</span>></span>
    <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">stylesheet</span>></span>
   <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">extension</span>></span>
</pre>

<p>If you have a custom control, for example MyControl, you can style it for example with the following entry in the "your_dark_extensions.css" file</p>

<pre style="background:#fff;color:#000">MyControl {
 <span style="color:#3b5bb5">background-color</span>:<span style="color:#3b5bb5">#515658</span>;
 <span style="color:#3b5bb5">color</span>:<span style="color:#3b5bb5">#eeeeee</span>;
}
</pre>

<p>If MyControl is a subclass of Control, the existing CSS handler for Control and the properties background-color and color will call its setForeground and setBackground method on instances of this widget.</p>

<p>As for colors and icons, plug-in developers should avoid hard-coded colors and use png icons with a transparent background to look good in the light as well as in the dark theme.</p>
<p>One way to allow styling of  colors is to use preferences or even use the colors and fonts extension point for the platform as described in the  following blog post: <a target="_blank" href="http://blog.eclipse-tips.com/2008/08/adding-color-and-font-preferences.html">http://blog.eclipse-tips.com/2008/08/adding-color-and-font-preferences.html</a></p>

<p>Styling preference via CSS is very simple. Use the IEclipsePreferences#your-preference-node to select the preference node you want to set. Only replace "." with "-" for your preference node. You cannot use “.” as these have a predefined meaning in CSS. The Eclipse CSS engine will convert the underscores to the correct preference node. If styling colors and fonts from the platform extension point, you must also add a pseudo-select (in the following example :org-eclipse-jdt-ui) to avoid overridden CSS preference settings from other plug-ins.</p>

<p>Here is an example from JDT UI for styling its colors, which will contribute to the org.eclipse.ui.workbench preference node.</p>

<p style="word-wrap: break-word;">IEclipsePreferences#org-eclipse-ui-workbench:org-eclipse-jdt-ui { /* pseudo attribute added to allow contributions without replacing this node, see Bug 466075 */</p>
<pre style="background:#fff;color:#000">preferences:
     'org.eclipse.jdt.ui.ColoredLabels.inherited<span style="color:#ff7800">=</span><span style="color:#3b5bb5">143</span>,<span style="color:#3b5bb5">143</span>,<span style="color:#3b5bb5">191</span>'
     'org.eclipse.jdt.ui.Javadoc.backgroundColor<span style="color:#ff7800">=</span><span style="color:#3b5bb5">52</span>,<span style="color:#3b5bb5">57</span>,<span style="color:#3b5bb5">61</span>'
     'org.eclipse.jdt.ui.Javadoc.foregroundColor<span style="color:#ff7800">=</span><span style="color:#3b5bb5">238</span>,<span style="color:#3b5bb5">238</span>,<span style="color:#3b5bb5">238</span>'
}
</pre>

<h2>Available tools for CSS work</h2>
<p>To analyze or to test CSS snippets you can use the CSS spy and the CSS scratch pad. Install the spies via the following update site: <a target="_blank" href="http://download.eclipse.org/e4/snapshots/org.eclipse.e4.tools/latest/">http://download.eclipse.org/e4/snapshots/org.eclipse.e4.tools/latest/</a></p>

<p>The CSS spy allows the selection of a UI component and show (and sometimes) change their properties.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/highlightelements.png"></p>

<p>To test CSS snippets use CSS scratchpad, also available on the above update site. For example, open the “CSS Scratchpad” view via the Quick Access and use the following:</p>

<pre style="background:#fff;color:#000">CTabFolder Composite {
  <span style="color:#3b5bb5">background-color</span>: <span style="color:#900;font-style:italic">pink</span>;
}

CTabFolder CTabItem {
  <span style="color:#3b5bb5">background-color</span>: <span style="color:#3b5bb5">lime</span>;
  <span style="color:#3b5bb5">color</span>: <span style="color:#3b5bb5">green</span>;
}

CTabFolder CTabItem:selected {
  <span style="color:#3b5bb5">background-color</span>: <span style="color:#3b5bb5">blue</span>;
  <span style="color:#3b5bb5">color</span>: <span style="color:#3b5bb5">white</span>;
}
</pre>

<p>This will style the running IDE similar to the following.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/highlight.png"></p>

<p>Last but not least, it allows the preference spy to trace changes in the preferences to find the correct preference node and value to style it.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/june/images/changes.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/changes_sm.png"></a></p>

<p>For more information on the spys and the CSS styling see <a target="_blank" href="http://www.vogella.com/tutorials/EclipsePlatformDevelopment/article.html#eclipse_ide_spies">http://www.vogella.com/tutorials/EclipsePlatformDevelopment/article.html#eclipse_ide_spies</a> and <a target="_blank" href="http://www.vogella.com/tutorials/Eclipse4CSS/article.html">http://www.vogella.com/tutorials/Eclipse4CSS/article.html</a>.</p>


<h2>Future work</h2>
<p>Of course, the platform team continues to work on the dark theme. Most interesting for Windows users is the work in <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=508634">Bug 508634</a> which would allow users to set the background and foreground color for (read-only) combo-box which are frequently used in the Eclipse IDE. But again lots of small annoyances will be addressed for the next release, which will arrive only 3 months later after the Photon release.</p>

<p>Good news for the dark theme is that several platform developers, including the author of this article, have switched to the dark theme for their daily work. This also means that the remaining issues with the dark theme are more easily discovered and faster fixed.</p>

<h2>How can you help as a user?</h2>
<p>Tools like Git, Gradle, and Maven for Eclipse already provide good support for the dark theme and we expect that this support will grow in the future, especially now that plug-in developers have a small guide how to support it. Patches to support the dark theme are also relatively simple (plugin.xml registration of the CSS file and a corresponding CSS file).</p>

<p>So if you are a dark theme user, please open bugs for the components which do not yet support the dark theme. Or even better provide a snippet which you validated via the CSS spy or CSS scratch pad so that the plug-in developers can apply it. Also plug-ins which are still using old gifs (which do not support real transparency), will most likely be happy about contributions of svg version of their icons, so that they can generate png and HDPI png files from them.</p>

<h2>Conclusion</h2>
<p>So hopefully you enjoy the improved dark theme in Eclipse Photon. Like I said earlier, all the cool kids are using a dark theme these days. ;-)</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
         <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/lars.png"
        alt="Lars Vogel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Lars Vogel<br />
            <a target="_blank" href="http://www.vogella.com/">Vogella</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/vogella">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.vogella.com/people/larsvogel.html">About</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/vogella">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>