<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>A good <code>code editor</code> for a <code>given language</code> (Java, JSON, XML, etc) must provide several features like:</p>
    <ul>
        <li><code>completion</code>, <code>hyperlink</code>, <code>hover</code>, <code>validation</code></li>
        <li>and <code>syntax highlighting</code></li>
	</ul>
<p>For several years now, each code editor has implemented those features for each language. Today the strategy for a code editor is to share those features with other code editors by supporting <a target="_blank" href="https://langserver.org/">Language Server Protocol (LSP)</a>: completion, hyperlink, hover, and validation for a given language are managed with a language server. The code editor is the client which consumes the language server.</p>

<p><a target="_blank" href="https://atom.io/">Atom</a>, Eclipse IDE (with <a target="_blank" href="https://projects.eclipse.org/projects/technology.lsp4e">Eclipse LSP4E</a>), <a target="_blank" href="https://www.sublimetext.com/">Sublime Text</a>, <a target="_blank" href="https://code.visualstudio.com/">VSCode</a> are code editor samples which support LSP.</p>
<p>But what about <code>syntax highlighting</code> (as LSP doesn't support it), how to share 
<code>syntax highlighting</code> between code editor?</p>

<h2>Syntax Highlighting - TextMate</h2>

<p>The <a target="_blank" href="https://macromates.com/">TextMate MacOS editor</a> uses <a target="_blank" href="https://manual.macromates.com/en/language_grammars">TextMate grammar</a> to support <code>syntax highlighting</code> in her editor. A TextMate grammar is a simple file which describes a language grammar with advanced RegExp.</p>
<p>A lot of code editors have the capability to consume a TextMate grammar to support <code>syntax highlighting</code> like Atom, Sublime Text,and VSCode.</p>

<h2>What is Eclipse TM4E?</h2>
<p><a target="_blank" href="https://projects.eclipse.org/projects/technology.tm4e">Eclipse TM4E</a> is the TextMate support for Eclipse IDE, it gives the capability to:</p>
    <ul>   
        <li>Consume a TextMate grammar to support <code>syntax highlighting</code> in any Eclipse editor.</li>
        <li>Consume a language configuration to support other features like <code>matching bracket</code>, <code>auto close</code>, <code>on enter support</code>.</li>
	</ul> 
	
<h2>TM4E - TextMate grammar</h2>
<p>Once you have found a TextMate grammar file for your language, you can consume it inside Eclipse IDE with TM4E in two ways:</p>
     <ul>
        <li>user preferences</li>
        <li>plugin development</li>
     </ul>
<p>Imagine you want to have <code>syntax highlighting</code> for Shell Script of your test.sh file:</p>

<pre style="background:#fff;color:#000"><span style="color:#8c868f">#!/bin/csh</span>
<span style="color:#3b5bb5">echo</span> compiling...
cc -c foo.c
cc -c bar.c
cc -c qux.c
cc -o myprog foo.o bar.o qux.o
<span style="color:#3b5bb5">echo</span> <span style="color:#ff7800">done</span>.
</pre>

<p>You can download <a target="_blank" href="https://raw.githubusercontent.com/textmate/shellscript.tmbundle/master/Syntaxes/Shell-Unix-Bash.tmLanguage">Shell-Unix-Bash.tmLanguage</a></p>

<h3>User preferences</h3>
<p>Here steps to follow to consume the shell TextMate grammar:</p>
	<ul>
		<li>Register content type of <code>*.sh</code> files:</li>
	</ul>
	
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/content_type.png"></p>
	
	<ul>
		<li>Register TextMate grammar of <code>*.sh</code> files:</li>
	</ul>
	
<p align="center"><a href="/community/eclipse_newsletter/2018/june/images/grammar_file.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/grammar_file_sm.png"></a></p>

<p>Now you can open your sh file with Generic Text Editor and you should see:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/test_sh.png"></p>

<h2>Plugin development</h2>
<ul>
<li>Defines a content type for your file:</li>
</ul>

<pre style="background:#fff;color:#000"><span style="color:#3a4a64">&lt;<span style="color:#3a4a64">extension</span>
     <span style="color:#3a4a64">point</span>=<span style="color:#409b1c">"org.eclipse.core.contenttype.contentTypes"</span>></span>
  <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">content</span><span style="color:#3a4a64">-type</span>
        <span style="color:#3a4a64">base-type</span>=<span style="color:#409b1c">"org.eclipse.core.runtime.text"</span>
        <span style="color:#3a4a64">file-extensions</span>=<span style="color:#409b1c">"shell"</span>
        <span style="color:#3a4a64">id</span>=<span style="color:#409b1c">"com.youcompany.youapp.shell"</span>
        <span style="color:#3a4a64">name</span>=<span style="color:#409b1c">"Shell"</span>
        <span style="color:#3a4a64">priority</span>=<span style="color:#409b1c">"normal"</span>></span>
  <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">content</span><span style="color:#3a4a64">-type</span>></span>
<span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">extension</span>></span> 
</pre>

	<ul>
		<li>Register your TextMate grammar and bind it with your content type:</li>
	</ul>

<pre style="background:#fff;color:#000"><span style="color:#3a4a64">&lt;<span style="color:#3a4a64">extension</span>
      <span style="color:#3a4a64">point</span>=<span style="color:#409b1c">"org.eclipse.tm4e.registry.grammars"</span>></span>
   <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">grammar</span>
        <span style="color:#3a4a64">scopeName</span>=<span style="color:#409b1c">"source.shell"</span>
        <span style="color:#3a4a64">path</span>=<span style="color:#409b1c">"./syntaxes/Shell-Unix-Bash.tmLanguage"</span> ></span>
   <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">grammar</span>></span>      
   <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">scopeNameContentTypeBinding</span>
         <span style="color:#3a4a64">contentTypeId</span>=<span style="color:#409b1c">"com.youcompany.youapp.shell"</span>
         <span style="color:#3a4a64">scopeName</span>=<span style="color:#409b1c">"source.shell"</span>></span>
   <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">scopeNameContentTypeBinding</span>></span>
<span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">extension</span>></span>
</pre>

<p>Now you can open your sh file with Generic Text Editor and you should see:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/test_sh.png"></p>

<h2>TM4E - Language configuration</h2>
<p>VSCode <a target="_blank" href="https://code.visualstudio.com/docs/extensionAPI/extension-points#_contributeslanguages">Language configuration</a> gives the capability to configure matching bracket character pair, etc for a language. TM4E is able to consume this language configuration to bind it with a content type. Today it supports only auto close and on enter support.</p>

<p>Here a sample of language configuration for JSON:</p>

<pre style="background:#fff;color:#000">{
    <span style="color:#409b1c">"comments"</span>: {
        <span style="color:#409b1c">"lineComment"</span>: <span style="color:#409b1c">"//"</span>,
        <span style="color:#409b1c">"blockComment"</span>: [ <span style="color:#409b1c">"/*"</span>, <span style="color:#409b1c">"*/"</span> ]
    },
    <span style="color:#409b1c">"brackets"</span>: [
        [<span style="color:#409b1c">"{"</span>, <span style="color:#409b1c">"}"</span>],
        [<span style="color:#409b1c">"["</span>, <span style="color:#409b1c">"]"</span>]
    ],
    <span style="color:#409b1c">"autoClosingPairs"</span>: [
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"{"</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">"}"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>] },
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"["</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">"]"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>] },
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"("</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">")"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>] },
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"'"</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">"'"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>] },
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"<span style="color:#3b5bb5">\"</span>"</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">"<span style="color:#3b5bb5">\"</span>"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>, <span style="color:#409b1c">"comment"</span>] },
        { <span style="color:#409b1c">"open"</span>: <span style="color:#409b1c">"`"</span>, <span style="color:#409b1c">"close"</span>: <span style="color:#409b1c">"`"</span>, <span style="color:#409b1c">"notIn"</span>: [<span style="color:#409b1c">"string"</span>, <span style="color:#409b1c">"comment"</span>] }
    ]
}
</pre>

<p>To bind the JSON content type with the language configuration, you can use the following extension point:</p>

<pre style="background:#fff;color:#000"><span style="color:#3a4a64">&lt;<span style="color:#3a4a64">extension</span>
      <span style="color:#3a4a64">point</span>=<span style="color:#409b1c">"org.eclipse.tm4e.languageconfiguration.languageConfigurations"</span>></span>
   <span style="color:#3a4a64">&lt;<span style="color:#3a4a64">languageConfiguration</span>
         <span style="color:#3a4a64">contentTypeId</span>=<span style="color:#409b1c">"content-type.of.json"</span>
         <span style="color:#3a4a64">path</span>=<span style="color:#409b1c">"language-configurations/json.language-configuration.json"</span>></span>
   <span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">languageConfiguration</span>></span>
<span style="color:#3a4a64">&lt;/<span style="color:#3a4a64">extension</span>></span>
</pre>


<p>Here are two demos that show you the benefit with language configuration:</p>
<p>1. Without language configuration:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/a_json.gif"></p>


<p>2. With language configuration:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/june/images/a_json2.gif"></p>


<h2>Conclusion</h2>
<p>Configuring syntax coloration for any language in Eclipse IDE is now possible with TM4E which consumes a simple TextMate grammar file. More and more projects are using the TM4E/LSP4E combo like <a target="_blank" href="https://github.com/eclipse/corrosion">Eclipse Corrosion</a> and <a target="_blank" href="https://github.com/mickaelistria/eclipse-bluesky">Eclipse BlueSky</a>.</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/june/images/angelo.jpeg"
        alt="Angelo Zerr" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Angelo Zerr<br />
            <a target="_blank" href="https://www.sodifrance.fr/">Sodifrance</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/angelozerr">GitHub</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/angelozerr">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>