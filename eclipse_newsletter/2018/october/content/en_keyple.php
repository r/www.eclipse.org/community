<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
		<p>The Eclipse Keyple project aims at providing generic libraries for simplifying the development of contactless applications based on the Calypso standard, and for facilitating integration with the secure elements typically involved in a secure contactless solution.</p>
	<h3>What is Calypso?</h3>
		<p>Calypso is a set of specifications describing a fast and secure off-line contactless transaction, between a portable object and a terminal. It has been created for public transport ticketing at the end of the 1990s by some European public transport operators or authority (RATP and SNCF in Paris and all France, STIB in Brussels, OTLIS in Lisbon, ACTV in Venice, Konstanz in Germany), in order to create an open, interoperable and secure standard independent from industrials to ensure a real competition</p>
		<p>Today Calypso represents 20% of the world market of contactless smart ticketing in 25 countries and 125 cities. It brings to his user a guarantee of security and interoperability which relies on a total compliance with existing standards (ISO 14443, ISO 7816-4, GlobalPlatform). The Calypso standard is managed by the Calypso Networks Association (CNA) , which is led by transports operators and public authorities in order to ensure openness and independence from industrials in confront of other proprietary schemes.</p>
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/tap.png"></p>
	<h3>Keyple makes contactless ticketing accessible</h3>
		<p>The goal of Keyple is to allow developers to easily implement fast and secure off-line contactless transactions (using NFC cards, mobile phones, ...) based on the Calypso standard.</p>
		<p>More specifically, Keyple is a set of open source libraries that will initially be available in Java and C++, designed on the same mutual Object-Oriented Model compatible with any terminal architecture: mobile, embedded or server and Interoperable with any smart card reader solution: standard or proprietary, local or remote.</p>
		<p>To fully understand how Keyple works, it is important to discern two main components of contactless ticketing technology:</p>
			<ul>
				<li><strong>Secure Elements (SE) readers:</strong> Readers are situated at the entrance and exit of events, venues and transport sites. For example, a smart reader could be a terminal, a portable scanning laser gun, or a swipe tablet area that is embedded into a door, vehicle or gate. Code is written for a terminal to set the parameters for allowing cards or apps to transmit ticketing information data. Sometimes in a distributed architecture system design, the code for the reader is not on the terminal, but in a cloud environment, so the reader sends the data to cloud-based architecture.  </li>
				<li><strong>Ticketing application:</strong> This is behind-the-scenes code that is able to take the data from the smart reader terminal and, in milliseconds real-time, analyze the balance of the ticket, confirm the permissions for entry, and update the data on the ticket (for example, to confirm that the ticket holder can enter the gate or vehicle, and then to deduct the cost of the journey and calculate the new balance).</li>
			</ul>
		<p>According to this scheme, Keyple defines two layers:</p>
		<ul>
			<li><strong>SE readers</strong> are integrated through plugins implementing the SE Proxy API which manages the communications with a contactless secure element through ant type of contactless reader (local, remote, standard, proprietary...)</li>
			<li><strong>Ticketing applications</strong> relies on a high-level Calypso processing API to manage Calypso commands &#38; security features.  This API uses the SE Proxy API to communicate with the reader</li>
		</ul>
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/keyplechart.png"></p>
		<p><em>Other modules can be implemented at the same level than the Calypso Processing API to manage other technology than Calypso (Mifare, Felica, contactless memory cards...)</em></p>
		
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/october/images/pierre.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
          	Pierre Terr&eacute;e<br>
            <a target="_blank" href="www.calypsonet-asso.org">Project Manager and Ticketing Expert 
            <br>Calypso Networks Association</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/calypsonet_asso">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>