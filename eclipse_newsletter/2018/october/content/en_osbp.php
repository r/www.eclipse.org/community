<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Coding is an activity that is full of tedious repetition. Tools to speed up certain tasks are available but seldom do they offer more than piecemeal automation for isolated steps. In contrast, the software factory <a href=" https://www.eclipse.org/osbp/index.html" target="_blank">Eclipse OSBP</a> provides nearly full automation, eliminating much of the manual work that makes traditional software engineering costly both in time and effort and error-prone.</p>
	<p>OSBP creates platform-independent applications that run in all prevalent environments, feed from most data sources and deliver to practically all devices. The factory persists the model of the app in domain-specific languages (DSL), not source code. This allows designers and, of course, software developers to make changes easily at any time. Human-made modifications in one programming layer are adjusted by OSBP's robot hand in all other related layers. Developers could even replace any underlying framework with a more suitable one without losing the know-how and structure of the app.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/timeeffort.png"></p>
	<p>Unlike most tools, OSBP does not come with a trade-off between automation and flexibility. The factory offers extensive low-code/no-code as well as full code facilities that support app designers, developers - possibly even end-users. At the same time, developers are free to code for the same project and profit from the robotic inter-layer adjustments. They can easily jump back and forth between layers, choosing the level of automation that seems most efficient for their ends. Still, most of the actual human work done in the factory will be modeling, not coding.</p>
<h3>Install your own factory</h3>
	<p>The best way to check out the capabilities of OSBP is to give it a brief try. The factory's flexibility ensures that you can start any project right away and make changes easily later on. Here is where you can <a href="https://www.eclipse.org/osbp/download.html" target="_blank">download</a> it. You can install OSBP and set your target platform within minutes (assuming that you have Java and the Eclipse IDE also installed). There is a short <a href="https://www.youtube.com/watch?time_continue=163&v=19P1m23Ug3I" target="_blank">video</a> to guide you through the process, or a <a href="http://download.osbee.org/documentation/index.php/OS.bee_Software_Factory_Installation_Neon" target="_blank">documentation</a> if you prefer it. The result will be your own OSBP workspace in the Eclipse IDE.</p>
<h3>Watch an app unfold</h3>
	<p>OSBP has a <a href="https://www.youtube.com/watch?time_continue=1&v=Ucd7MLlKpQY" target="_blank">wizard</a> that creates a first, full application, along with some mock data to feed it ("My1stApp"). Once started, you can watch the robot adding layer upon layer in the package explorer. Follow the <a href="https://download.osbee.org/documentation/index.php/My_first_app" target="_blank">instructions</a>, launch the app and watch the result in your browser. After the log-in (the factory has furnished the app with an authentication feature), you will find a grid and a dialog. In your browser, you can edit the mock data as you please, checking whether the app is running correctly.</p>
	<p>Now that it is possible to see and virtually touch the application, ideas of what could be done differently tend to emerge. For making changes, close your browser, return to the workspace and start modeling.</p>
<h3>Making changes swiftly</h3>
	<p>Let's take an example: Someone looks at the list of attributes in the app and decides that it should be complemented with new attributes, related to the names. Under traditional circumstances, few developers would be enthusiastic about such a request at this stage, as it effectively means to them: start over an important part of the implementation process, redesign the ERM, the UI, link both, etc. In the factory, you would swiftly peruse the package explorer, find the entity model and <a href="https://www.youtube.com/watch?time_continue=59&v=hdML_HZV0qY" target="_blank">add</a> the supplementary attributes to the entity "person", in this case, "street", "zip code" and "country". What you have changed is, in fact, the domain model, written in self-explanatory DSL.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/dsl.png"></p>
	<p>Then you leave the rest to the factory. It will automatically adapt the dependent domain models and rewrite the source code plus the database structure. You will find that you have made the complementation quickly and with very little effort.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/attributes.jpg"></p>
<h3>Content Assist</h3>
	<p>The factory has defined DSLs for all domains that a typical business app is made of, 26 in total. Examples are the user interface, authorization, reports, database models, data import, etc. The modeling is facilitated by Content Assist, a functionality of the Eclipse IDE that has been integrated to support the work with OSBP. Click CTRL+SPACE, then a drop-down list appears showing context-dependent input choices. Not only does this speed up the modeling, but it also makes it extremely easy to familiarize yourself with the "factory slang".</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/menu.png"></p>
<h3>Apex of automation: Create an app from a CSV-file</h3>	
	<p>Once you have familiarized yourself with OSBP, you can easily mass-produce customized applications with very little effort. This, however, is not yet the pinnacle of automation. Here it comes: OSBP allows you to drag-and-drop a CSV file into the Eclipse workspace, start the machine and let it build an application that reflects the input data automatically.</p>
	<p>More precisely, OSBP extracts the data structure from the first line of the CSV file and creates an entity model, containing the columns of the CSV file as attributes, and continues with all the other steps to build the full app. There are a few basic <a href="https://download.osbee.org/documentation/index.php/App_up_in_5_minutes#AppUpIn5">preparations</a> that must be made first: the preferences of the OSBP application configuration need to be set, regarding the project name, the DSL model paths, the data source, and the delimiter used when extracting the data from the file. With these settings, it's working like magic. Here is an example of a screenshot of a completed application that is automatically generated using the data from a CSV file.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/csv.jpg"></p>
<h3>Refine in iterative loops</h3>
	<p>Traditionally, software engineering has been broken down into sharply defined stages, in which the product design is determined before the largest chunks of software are being coded. Arrangements and time-budgets may vary, but they all have one thing in common:  there is a point-of-no-return, after which changes cannot be incorporated without extraordinary cost. Not so in this software factory.</p>
	<p>OSBP allows for a new kind of co-operation between users and developers, as it makes it very easy to produce the first prototype and refine it in an iterative e.g. scrum-driven process. In fact, the <a href="https://wiki.eclipse.org/OSbea" target="_blank">OS.bea Working Group</a> is exploring the potentials of this approach (and of the factory in general.) What makes OS.bea a unique group is that, unlike any other one at Eclipse, it explicitly addresses both sides, developers, app-designers and end-users. Faster and flexible development, a simplified process and closer involved users - OSBP truly opens up unprecedented opportunities.</p>



<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/october/images/christophe.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
          	Christophe Loetz<br>
            <a target="_blank" href="https://www.osbee.org/home/">Compex Systemhaus GmbH</a>
            </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/osbeeorg">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>