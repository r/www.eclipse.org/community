<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Eclipse VOLTTRON&trade; is an open-source distributed control and sensing platform (see figure 1) for connecting devices, managing data, and executing actions. To date, most of the 'applications' developed using VOLTTRON have been for buildings, the electricity power grid, and the integration of buildings with the grid to support the deployment of energy efficiency and grid services. VOLTTRON provides an agent-based programming paradigm to ease application development and minimize the lines of code that need to be written by domain experts (such as buildings engineers).  The VOLTTRON platform has four primary roles;</p>
		<ul>
			<li>A reference platform for researchers to quickly develop control applications for transactive energy</li>
			<li>A reference platform with flexible data-storage support for energy-analytics applications,  either in academia or in commercial enterprises</li>
			<li>A platform from which commercial enterprises can develop  products  without  license  issues  and  easily integrate them into their product line</li>
			<li>An accelerator to drive industry adoption of transactive energy and advanced building energy analytics</li>
		</ul>
	<p>VOLTTRON provides an environment for agent execution and serves as a single point of contact for interfacing with devices [rooftop units (RTUs), building systems, meters, etc.], external resources (weather, utility transactive signals, etc.), and platform services such as data archival and retrieval.  It provides a collection of utility and helper classes which simplifies agent development.</p>	
	<p>VOLTTRON connects devices and external signals from the power grid to agents implemented in the platform or in the cloud. The platform plug-in provides additional capabilities for large-scale, real-world deployments.  These enhancements include agent mobility, signing, and verification of agents, and resource management.  The plug-in allows actors to sign agent packages as an acknowledgment that the code is useful,  reliable, and beneficial. If the code is modified either in situ or in transit to a remote platform, it will fail verification and not be allowed to run. This mechanism is especially important for an app-store use case in which the deploying entity may not have first-hand experience with the code and must trust the entities who have signed it.  The resource management allows platforms to ensure agents do not consume more resources that are available on the hardware and exhaust the system. The agent mobility service allows agents to move from platform to platform (either as directed by an administrator or self-directed).</p>	
	<p>The VOLTTRON platform communicates to devices and building controllers through the driver framework. Drivers periodically read data off devices (such as a building automation system controller) and publish data to the message bus via a topic for each device; it also provides a means to send control commands from agents to controllers. The driver framework simplifies  the creation of new drivers by handling platform interaction and allows developers to focus on methods for communicating with devices. Modbus and BACnet implementations are included with VOLTTRON. VOLTTRON incorporates several open-source projects to build a flexible and powerful platform (http://github.com/ VOLTTRON). The key services and applications provided by VOLTTRON include the following:</p>
	<br/>
	<h4><em>Figure 1</em></h4>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/volttronchart.png"></p>
		<ul>
			<li style="margin-bottom: 10px">The <em>Actuator/Scheduler</em> agent allows other applications on the platform to schedule times to interact with devices. This Scheduler agent ensures that only one agent can control a device at a given time and allows the user to set the relative priority of each application.</li>
			<li style="margin-bottom: 10px"><em>Drivers</em>. Devices controlled by the platform communicate with drivers. The drivers abstract device-specific protocols from the rest of the platform by publishing device data to and taking commands from the message bus. VOLTTRON includes drivers for Modbus, BACnet, DNP3, SEP2.0, ChargepointEV, and other contributions from the community.</li>
			<li style="margin-bottom: 10px"><em>Historians</em>. Data being published to the message bus by drivers and applications can be sent to storage (database, file, web service, etc.) through use of the Historian framework. Much like drivers, the framework provides a simple interface for adding additional solutions. Current implementations include: SQLite, MySQL, MongoDB, CrateDB, InfluxDB, and others.</li>
			<li style="margin-bottom: 10px">A <em>management interface</em>. A web-based user interface allows the administration of VOLTTRON nodes (and the agent/applications) running on the VOLTTRON nodes on one or more networks.</li>
			<li style="margin-bottom: 10px">A <em>message bus</em>. All agents and services can publish and subscribe to topics on the message bus. The message bus provides a single and uniform interface that abstracts the details of devices and agents from each other. Agents and components running in the platform produce and consume messages and/or events. The agents decide how agents produce events and how they process received events.</li>
			<li style="margin-bottom: 10px"><em>Remote procedure calls</em>. VOLTTRON agents can interact via remote procedure calls. This is especially useful when interacting with service agents, such as an actuator or historian, and simplifies the code required to use those services.</li>
			<li style="margin-bottom: 10px"><em>VOLTTRON Interconnect Protocol</em> (VIP). This protocol is designed to provide secure communications within and between VOLTTRON platforms. It allows for attribution of messages and restriction of access. It also makes it easier to address messages to agents on other platforms.</li>
			<li style="margin-bottom: 10px"><em>Multinode communication</em>. The message bus enables encrypted multi-node communication allowing agents on separate VOLTTRON instances to work together.</li>
			<li style="margin-bottom: 10px"><em>Supervisory service</em>.  Several subsystems are included that VOLTTRON Central uses to give an administrator situational awareness of the health and status of devices in their deployment.  These systems can also trigger emails and other automatic notifications.</li>
			<li style="margin-bottom: 10px"><em>Proxy agents</em>.  A proxy agent is an agent that runs on the platform with the sole purpose of interfacing with a service that does not natively participate in the VOLTTRON message bus.</li>
			<li style="margin-bottom: 10px"><em>Simulation integration</em>. VOLTTRON has facilities for working with simulation frameworks. This allows algorithms to be developed, tested and demonstrated on simulated buildings to ease adoption onto actual installations</li>
		</ul>
	<p>VOLTTRON provides security against unauthorized access to system data and unauthorized exercise of control functions. It isolates applications running on the platform from each other (if needed) and enforces resource utilization limits on the applications to ensure the stability of the computational platform. The software platform uses well-established and widely accepted security mechanisms,  including elliptic-curve encryption, authentication, and authorization.    When a peer (agent) authenticates to a platform, the peer is proving its identification. In other words, authentication is the process of finding a peer to an identity.  Authorization is the processing of granting permissions or capabilities to peers based on their identity.  VOLTTRON agents use authorization to selectively limit which peers can call which methods based on the peer's granted permissions.  The software platform's authorization gives agent authors and platform owners fine control over who can use their agents and how their agents can be used.  Additionally, communications with other VOLTTRON platforms use authentication and authorization functions to ensure that only legitimate transactions are performed.  Access to the system through local management interfaces is also protected by similar security measures</p>
	<p>The hardware requirements of the VOLTTRON platform depend on the intended role for that instance. The platform software itself consumes little resources, but the applications deployed into it and the services provided determine where this instance should run.  An instance collecting data from a handful of devices could comfortably run on a single-board computer (such as RaspberryPi or BeagleBone,  etc.).  However, an instance supporting applications that analyze data from multiple buildings to aggregate grid services or optimize energy use across a campus could require the resources of a server.  VOLTTRON's only requirement is that it runs in a Linux environment with needed prerequisites such as Python.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/october/images/george.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            George Hernandez<br>
            <a target="_blank" href="https://www.pnnl.gov/">Pacific Northwest National Laboratory</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/EclipseVolttron">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>