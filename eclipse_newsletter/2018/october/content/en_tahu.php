<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<h3>Background</h3>
		<p>MQTT was originally designed as a message transport for real-time SCADA (Supervisory Control and Data Acquisition) systems. The MQTT message transport specification does not specify the topic namespace to use nor does it define the Payload representation of the data being published and/or subscribed to. In addition to this, since the original use case for MQTT was targeting real-time SCADA, there are mechanisms defined to provide the state of an MQTT session such that SCADA/Control HMI application can monitor the current state of any MQTT device in the infrastructure. As with the topic namespace and payload the way state information is implemented and managed within the MQTT infrastructure is not defined. All of this was intentional within the original MQTT specification to provide maximum flexibility across any solution sector that might choose to use MQTT infrastructures.</p>
		<p>But at some point, for industrial MQTT based solutions to be interoperable within a given market sector, the topic namespace, payload representation, and session state must be defined. The intent and purpose of the Sparkplug specification is to define an MQTT topic namespace, payload, and session state management that can be applied generically to the overall IIoT market sector, but specifically meets the requirements of real-time SCADA/DSC (Distributed Control System)/ICS (Industrial Control System) OT solutions. Meeting the operational requirements for these systems will enable MQTT based infrastructures to provide more valuable real-time information to Line of Business MES/OEE/Track-and-Trace/Cloud Services solution requirements as well.</p>
		<p>The purpose of the Sparkplug specification is to remain true to the original notion of keeping the topic namespace and message sizes to a minimum while still making the overall message transactions and session state management between MQTT devices and MQTT SCADA/DCS/ICS applications simple, efficient and easy to understand and implement.</p>
		<p>The Eclipse Tahu project includes the Sparkplug specification itself, client libraries, and sample applications.</p>
	<h3>Scope</h3>
		<p>The scope of the Eclipse Tahu project consists of three major components:</p>
			<ol>
				<li>The Sparkplug Specification - This document describes the MQTT topic namespace, the payload-encoding scheme, and the required flow of messages, which ensure state of data originating from an edge device reporting to a backend/central application.</li>
				<li>Client library implementations initially in the following programming languages:
					<ol>
						<li>C</li>
						<li>Java</li>
						<li>Javascript</li>
						<li>Python</li>
					</ol>
				</li>
				<li>Reference implementations of Sparkplug applications using the client libraries.</li>
			</ol>
	<h3>Description</h3>
		<p>Eclipse Tahu addresses the existence of legacy SCADA/DCS/ICS protocols and infrastructures and provides a much-needed definition of how best to apply MQTT into these existing industrial operational environments.</p>
		<p>Basic network architecture:</p>
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/october/images/networkarch.png"></p>
		<p>Tahu is currently addressing the following features required for MQTT centric IIoT:</p>
			<p><strong>Well-defined MQTT Topic Namespace applicable for the IIoT market</strong><br>
			In order to be interoperable across the plethora of OEM device manufacturers of industrial equipment and the SCADA/HMI/Control/Cloud Services backend components that desired to subscribe to the resulting information, a well-defined MQTT topic namespace needs to be defined. The topic namespace needs to be both efficient yet applicable to the applications that currently exist in the industrial market. The topic namespace needs to provide both the contextual information all the way to an individual device in the field, but also provide topic "verbs" to efficiently manage the "life cycle" of an MQTT session.</p>
			<p><strong>Efficient MQTT payload definition</strong><br>
			Staying with the original intent of MQTT, the payload specification needs to stay "lean and mean" to best utilize low bandwidth networks (VSAT, Radio, Cellular). Not only does the payload need to be efficient, but it also needs to recognize the fact that the primary purpose of IIoT solutions is to provide Industrial Process Variable information from devices in the field to both SCADA/DCS/FCS OT solutions as well as a line of business IT solutions.</p>
			<p><strong>MQTT STATE Management definition</strong><br>
			MQTT has the awareness of the current MQTT session built in using the LWT feature. But for STATE aware OT applications like SCADA/DCS/FCS, there needs to be a succinct definition on how best to use MQTT's built-in STATE awareness in an overall SCADA/DCS/ICS infrastructure.</p>
			<p>The Sparkplug specification provides all of the definitions and requirements listed above. In addition to the Sparkplug specification, Tahu will provide the required libraries and reference implementation code following the Sparkplug specification in:</p>
				<ul>
					<li>C</li>
					<li>Java</li>
					<li>Javascript</li>
					<li>Python</li>
				</ul>
			<p>Tutorials on getting the reference implementation code running on popular open hardware platforms like the Raspberry Pi will also be included in Tahu.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/october/images/Wes.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
          	 Wes Johnson<br>
            <a target="_blank" href="https://twitter.com/CirrusLink">Cirrus Link</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/CirrusLink">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>