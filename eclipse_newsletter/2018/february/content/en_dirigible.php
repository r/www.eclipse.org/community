<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2><a id="Overview_2"></a>Overview</h2>
<p><strong><a target="_blank" href="http://www.dirigible.io">Eclipse Dirigible</a></strong> is an open source cloud development platform, part of the Eclipse Foundation and the top-level <strong>Eclipse Cloud Development</strong> project. The ultimate goal of the platform is to provide software developers with the right toolset for building, running, and operating business applications in the cloud. To achieve this goal, Dirigible provides both independent <strong>Design Time</strong> and <strong>Runtime</strong> components.</p>
<h2><a id="Mission_6"></a>Mission</h2>
<p>Nowadays, providing a full-stack application development platform is not enough. Building and running on top of it has to be fast and smooth! Having that in mind, slow and cumbersome <em>“Build”</em>, <em>“CI”</em>, and <em>“Deployment”</em> processes have a direct impact on development productivity. In this line of thought, it isn’t hard to imagine that the Java development model for web applications doesn’t fit in the cloud world. Luckily, one of the strongest advantages of Dirigible comes at hand - the <strong>In-System Development</strong> model. Right from the early days of Dirigible, it was clear that it is going to be the platform for <strong>Business Applications Development</strong> in the cloud and not just another general purpose IDE in the browser. The reason for that decision is pretty simple - <strong><em>“One size doesn’t fit all”</em></strong>! Making a choice between providing “In-System Development” in the cloud and adding support for a new language <em>(Java, C#, PHP, …)</em>, is really easy. The new language doesn’t really add much to the uniqueness and usability of the platform, as the In-System development model does!</p>
<h2><a id="Architecture_10"></a>Architecture</h2>
<p>The goal of the In-System development model is to ultimately change the state of the system while it’s up and running, without affecting the overall performance and without service degradation. You can easily think of several such systems like Programmable Microcontrollers, Relational Database Management Systems, ABAP. As mentioned earlier, Dirigible provides a suitable design time and runtime for that, so let’s talk a little bit about the architecture. The Dirigible stack is pretty simple:</p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/architecture.png" alt="Dirigible Architecture"></p>
<p>The building blocks are:</p>
<ul>
    <li><strong>Application Server</strong> <em>(provided)</em></li>
    <li><strong>Runtime</strong> <em>(built-in)</em></li>
        <ul>
            <li><strong>Engine(s)</strong> - <em>(Rhino/Nashorn/V8)</em></li>
            <li><strong>Repository</strong> - <em>(fs/database)</em></li>
        </ul>
	<li><strong>Design Time</strong> <em>(built-in)</em></li>
        <ul>
        		<li><strong>Web IDE</strong> <em>(workspace/database/git/… perspective)</em></li>
        </ul>
	<li><strong>Applications</strong> <em>(developed)</em>
        <ul>
            <li>Application <em>(database/rest/ui)</em></li>
            <li>Application <em>(indexing/messaging/job)</em></li>
            <li>Application <em>(extensionpoint/extension)</em></li>
            <li>…</li>
        </ul>
</ul>

<h2><a id="Enterprise_JavaScript_30"></a>Enterprise JavaScript</h2>
<p>The language of choice in the Dirigible business application platform is <strong>JavaScript</strong>! But why JavaScript? Why not Java? Is it mature enough, is it scalable, can it satisfy the business application needs? The answer is: It sure does! The code that is being written is similar to Java. The developers can write their business logic in a synchronous fashion and can leverage a large set of Enterprise JavaScript APIs. For heavy loads, the Dirigible stack performs better than the NodeJS due to multithreading of the underlying JVM and the application server, and using the same V8 engine underneath.</p>
<h3><a id="Examples_34"></a>Examples</h3>

<ul>
<li>
<p><em><strong>Request/Response API</strong></em></p>
<pre style="background:#fff;color:#000"><span style="color:#ff7800">var</span> response <span style="color:#ff7800">=</span> require(<span style="color:#409b1c">'http/v3/response'</span>);

response.println(<span style="color:#409b1c">"Hello World!"</span>);
response.flush();
response.<span style="color:#3b5bb5">close</span>();
</pre>
</li>

<li>
<p><em><strong>Database API</strong></em>:</p>
<pre style="background:#fff;color:#000"><span style="font-weight:700">var</span> database <span style="color:#00f">=</span> require(<span style="color:#093">'db/v3/database'</span>);
<span style="font-weight:700">var</span> response <span style="color:#00f">=</span> require(<span style="color:#093">'http/v3/response'</span>);

<span style="font-weight:700">var</span> connection <span style="color:#00f">=</span> database.getConnection();
<span style="color:#00f">try</span> {
    <span style="font-weight:700">var</span> statement <span style="color:#00f">=</span> connection.prepareStatement(<span style="color:#093">"select * from MY_TABLE where MY_PATH like ?"</span>);
    <span style="font-weight:700">var</span> i <span style="color:#00f">=</span> <span style="color:#06f">0</span>;
    statement.setString(<span style="color:#00f">++</span>i, <span style="color:#093">"%"</span>);
    <span style="font-weight:700">var</span> resultSet <span style="color:#00f">=</span> statement.executeQuery();
    <span style="color:#00f">while</span> (resultSet.<span style="color:#6782d3">next</span>()) {
        response.println(<span style="color:#093">"[path]: "</span> <span style="color:#00f">+</span> resultSet.getString(<span style="color:#093">"MY_PATH"</span>));
    }
    resultSet.<span style="color:#33f;font-weight:700">close</span>();
    statement.<span style="color:#33f;font-weight:700">close</span>();
} <span style="color:#00f">catch</span>(e) {
    <span style="">console</span>.trace(e);
    response.println(e.message);
} <span style="color:#00f">finally</span> {
    connection.<span style="color:#33f;font-weight:700">close</span>();
}

response.flush();
response.<span style="color:#33f;font-weight:700">close</span>();
</pre>
</li>
</ul>

<p>The provided <strong>Enterprise JavaScript APIs</strong> leverage some of the mature Java specifications and de facto standards (e.g. JDBC, Servlet, CMIS, ActiveMQ, File System, Streams, etc.). Eliminating the build process <em>(due to the lack of compilation)</em> and at the same time exposing proven frameworks <em>(that does the heavy lifting)</em>, results in having the perfect environment for in-system development of business applications, with close to <strong><em>“Zero Turn-Around-Time”</em></strong>. In conclusion, the Dirigible platform is really tailored to the needs of <em>Business Application Developers</em>.</p>


<h2><a id="Getting_Started_76"></a>Getting Started</h2>
<ol>
<li>
<p><strong><em>Download</em></strong></p>
    <ul>
        <li>Get the latest <strong><em>release</em></strong> from: <a href="http://download.eclipse.org/dirigible">http://download.eclipse.org/dirigible</a></li>
        <li>The latest <strong><em>master</em></strong> branch can be found at: <a href="https://github.com/eclipse/dirigible">https://github.com/eclipse/dirigible</a></li>
        <li>Download the latest <em>Tomcat 8.x</em> from: <a href="https://tomcat.apache.org/download-80.cgi">https://tomcat.apache.org/download-80.cgi</a></li>
    </ul>
    
<blockquote>
<p><em><strong>NOTE:</strong> You can use the <strong>try out</strong> instance, that is available at <a href="http://dirigible.eclipse.org">http://dirigible.eclipse.org</a> and skip through the <strong>Develop</strong> section</em></p>
</blockquote>
</li>

<li>
<p><strong><em>Start</em></strong></p>
    <ul>
        <li>Put the <strong><em>ROOT.war</em></strong> into the <code>${tomcat-dir}/webapps</code> directory</li>
        <li>Execute <code>./catalina.sh start</code> from the <code>${tomcat-dir}/bin</code> directory</li>
    </ul>
</li>

<li>
<p><strong><em>Login</em></strong></p>
    <ul>
        <li>Open: <a href="http://localhost:8080">http://localhost:8080</a></li>
        <li>Log in with the default <code>dirigible/dirigible</code> credentials</li>
    </ul><br/>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/login.gif" alt="Login"></p><br>
</li>

<li>
<p><strong><em>Develop</em></strong></p>
	<ul>
		<li>
		<p>Project</p>
			<ol>
                <li>
                <p>Create a project</p>
                <ul>
                		<li>Click <code>+ -&gt; Project</code></li>
                </ul><br/>
        			</li>
			</ol>
		</li>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/project.gif" alt="Create Project"></p><br>

		<li>
			<p>Database table</p>
				<ol>
		<li>
		<p>Generate a <em><strong>Database Table</strong></em></p>
			<ul>
				<li>Right-click <code>New &gt; Generate &gt; Database table</code></li><br/>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/generate.gif" alt="Generate a Database Table"></p>
			</ul>
		</li>
		<li>
			<p>Edit the <code>students.table</code> definition</p>
<pre style="background:#fff;color:#000">{
    <span style="color:#093">"name"</span>: <span style="color:#093">"Students"</span>,
    <span style="color:#093">"type"</span>: <span style="color:#093">"TABLE"</span>,
    <span style="color:#093">"columns"</span>: [{
        <span style="color:#093">"name"</span>: <span style="color:#093">"ID"</span>,
        <span style="color:#093">"type"</span>: <span style="color:#093">"INTEGER"</span>,
        <span style="color:#093">"primaryKey"</span>: <span style="color:#093">"true"</span>
    }, {
        <span style="color:#093">"name"</span>: <span style="color:#093">"FIRST_NAME"</span>,
        <span style="color:#093">"type"</span>: <span style="color:#093">"VARCHAR"</span>,
        <span style="color:#093">"length"</span>: <span style="color:#093">"50"</span>
    }, {
      <span style="color:#093">"name"</span>: <span style="color:#093">"LAST_NAME"</span>,
        <span style="color:#093">"type"</span>: <span style="color:#093">"VARCHAR"</span>,
        <span style="color:#093">"length"</span>: <span style="color:#093">"50"</span>
    }, {
        <span style="color:#093">"name"</span>: <span style="color:#093">"AGE"</span>,
        <span style="color:#093">"type"</span>: <span style="color:#093">"INTEGER"</span>
    }]
}
</pre>
	</li>
	<li>
		<p>Publish</p></li>
			<ul>
				<li>Right-click the project and select <code>Publish</code></li>
			</ul>
<blockquote>
<em><strong>NOTE:</strong> The auto publish function is enabled by default</em>
</blockquote>

	<li>
	<p>Explore</p></li>
		<ul>
            <li>The database scheme can be explored from the <strong><em>Database perspective</em></strong></li>
            <li>Click <code>Window &gt; Open Perspective &gt; Database</code></li>
            <li>Insert some sample data<br>
            <code>insert into students values(1, 'John', 'Doe', 25)</code><br>
            <code>insert into students values(2, 'Jane', 'Doe', 23)</code><br>
			<blockquote><em><strong>Note:</strong> The perspectives are available also from the side menu</em></blockquote>
			</li>
        </ul>
</ol></li>
<ul>
<li>REST service<li>
	<ol>
		<li>Generate a <em><strong>Hello World</strong></em> service</li>
        		<ul>
        			<li>Right-click <code>New &gt; Generate &gt; Hello World</code></li>
        		</ul>
		<li>Edit the <code>students.js</code> service</li>
<br/>
<pre style="background:#fff;color:#000"><span style="font-weight:700">var</span> database <span style="color:#00f">=</span> require(<span style="color:#093">'db/v3/database'</span>);
<span style="font-weight:700">var</span> response <span style="color:#00f">=</span> require(<span style="color:#093">'http/v3/response'</span>);

<span style="font-weight:700">var</span> students <span style="color:#00f">=</span> listStudents();

response.println(students);
response.flush();
response.<span style="color:#33f;font-weight:700">close</span>();

<span style="font-weight:700">function</span> <span style="color:#ff8000">listStudents</span>() {
    let students <span style="color:#00f">=</span> [];
    <span style="font-weight:700">var</span> connection <span style="color:#00f">=</span> database.getConnection();
    <span style="color:#00f">try</span> {
        <span style="font-weight:700">var</span> statement <span style="color:#00f">=</span> connection.prepareStatement(<span style="color:#093">"select * from STUDENTS"</span>);
        <span style="font-weight:700">var</span> resultSet <span style="color:#00f">=</span> statement.executeQuery();
        <span style="color:#00f">while</span> (resultSet.<span style="color:#6782d3">next</span>()) {
            students.<span style="color:#33f;font-weight:700">push</span>({
                <span style="color:#093">'id'</span>: resultSet.getInt(<span style="color:#093">'ID'</span>),
                <span style="color:#093">'firstName'</span>: resultSet.getString(<span style="color:#093">'FIRST_NAME'</span>),
                <span style="color:#093">'lastName'</span>: resultSet.getString(<span style="color:#093">'LAST_NAME'</span>),
                <span style="color:#093">'age'</span>: resultSet.getInt(<span style="color:#093">'AGE'</span>)
            });
        }
        resultSet.<span style="color:#33f;font-weight:700">close</span>();
        statement.<span style="color:#33f;font-weight:700">close</span>();
    } <span style="color:#00f">catch</span>(e) {
        <span style="">console</span><span style="color:#33f;font-weight:700">.error</span>(e);
       response.println(e.message);
    } <span style="color:#00f">finally</span> {
        connection.<span style="color:#33f;font-weight:700">close</span>();
    }
    <span style="color:#00f">return</span> students;
}
</pre>

<li>Explore</li>
    <ul>
    		<li>The <code>student.js</code> service is accessible through the <code>Preview</code> view</li>
    </ul>
</ol>
</li></ul>
<blockquote>
<p><em><strong>NOTE:</strong> All backend services are up and running after save/publish, due to the In-System Development</em></blockquote>

<ul>
<li>Create a UI</li>
	<ol>
		<li>Generate a  <em><strong>HTML5 (AngularJS)</strong></em> page</li>
			<ul>
				<li>Right-click <code>New &gt; Generate &gt; HTML5 (AngularJS)</code></li>
			</ul>
		<li>Edit the page</li>

<pre style="background:#fff;color:#000"><span style="color:#00f">&lt;</span><span style="color:#00f">!</span>DOCTYPE html<span style="color:#00f">></span>
<span style="color:#00f">&lt;</span>html lang<span style="color:#00f">=</span><span style="color:#093">"en"</span> ng<span style="color:#00f">-</span>app<span style="color:#00f">=</span><span style="color:#093">"page"</span><span style="color:#00f">></span>
<span style="color:#00f">&lt;</span>head<span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>meta charset<span style="color:#00f">=</span><span style="color:#093">"utf-8"</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>meta http<span style="color:#00f">-</span>equiv<span style="color:#00f">=</span><span style="color:#093">"X-UA-Compatible"</span> content<span style="color:#00f">=</span><span style="color:#093">"IE=edge"</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>meta name<span style="color:#00f">=</span><span style="color:#093">"viewport"</span> content<span style="color:#00f">=</span><span style="color:#093">"width=device-width, initial-scale=1.0"</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>meta name<span style="color:#00f">=</span><span style="color:#093">"description"</span> content<span style="color:#00f">=</span><span style="color:#093">""</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>meta name<span style="color:#00f">=</span><span style="color:#093">"author"</span> content<span style="color:#00f">=</span><span style="color:#093">""</span><span style="color:#00f">></span>

    <span style="color:#00f">&lt;</span>link type<span style="color:#00f">=</span><span style="color:#093">"text/css"</span> rel<span style="color:#00f">=</span><span style="color:#093">"stylesheet"</span> href<span style="color:#00f">=</span><span style="color:#093">"/services/v3/core/theme/bootstrap.min.css"</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>link type<span style="color:#00f">=</span><span style="color:#093">"text/css"</span> rel<span style="color:#00f">=</span><span style="color:#093">"stylesheet"</span> href<span style="color:#00f">=</span><span style="color:#093">"/services/v3/web/resources/font-awesome-4.7.0/css/font-awesome.min.css"</span><span style="color:#00f">></span>

    <span style="color:#00f">&lt;</span>script type<span style="color:#00f">=</span><span style="color:#093">"text/javascript"</span> src<span style="color:#00f">=</span><span style="color:#093">"/services/v3/web/resources/angular/1.4.7/angular.min.js"</span><span style="color:#00f">></span><span style="color:#00f">&lt;</span>/script<span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>script type<span style="color:#00f">=</span><span style="color:#093">"text/javascript"</span> src<span style="color:#00f">=</span><span style="color:#093">"/services/v3/web/resources/angular/1.4.7/angular-resource.min.js"</span><span style="color:#00f">></span><span style="color:#00f">&lt;</span>/script<span style="color:#00f">></span>
<span style="color:#00f">&lt;</span>/head<span style="color:#00f">></span>

<span style="color:#00f">&lt;</span>body ng<span style="color:#00f">-</span>controller<span style="color:#00f">=</span><span style="color:#093">"PageController"</span><span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>div<span style="color:#00f">></span>
        <span style="color:#00f">&lt;</span>div <span style="font-weight:700">class</span><span style="color:#00f">=</span><span style="color:#093">"page-header"</span><span style="color:#00f">></span>
            <span style="color:#00f">&lt;</span>h1<span style="color:#00f">></span>Students<span style="color:#00f">&lt;</span>/h1<span style="color:#00f">></span>
        <span style="color:#00f">&lt;</span>/div<span style="color:#00f">></span>
        <span style="color:#00f">&lt;</span>div <span style="font-weight:700">class</span><span style="color:#00f">=</span><span style="color:#093">"container"</span><span style="color:#00f">></span>
            <span style="color:#00f">&lt;</span>table <span style="font-weight:700">class</span><span style="color:#00f">=</span><span style="color:#093">"table table-hover"</span><span style="color:#00f">></span>
                <span style="color:#00f">&lt;</span>thead<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>th<span style="color:#00f">></span>Id<span style="color:#00f">&lt;</span>/th<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>th<span style="color:#00f">></span>First Name<span style="color:#00f">&lt;</span>/th<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>th<span style="color:#00f">></span>Last Name<span style="color:#00f">&lt;</span>/th<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>th<span style="color:#00f">></span>Age<span style="color:#00f">&lt;</span>/th<span style="color:#00f">></span>
                <span style="color:#00f">&lt;</span>/thead<span style="color:#00f">></span>
                <span style="color:#00f">&lt;</span>tbody<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>tr ng<span style="color:#00f">-</span>repeat<span style="color:#00f">=</span><span style="color:#093">"student in students"</span><span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>td<span style="color:#00f">></span>{{student.<span style="color:#6782d3">id</span>}}<span style="color:#00f">&lt;</span>/td<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>td<span style="color:#00f">></span>{{student.firstName}}<span style="color:#00f">&lt;</span>/td<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>td<span style="color:#00f">></span>{{student.lastName}}<span style="color:#00f">&lt;</span>/td<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>td<span style="color:#00f">></span>{{student.age}}<span style="color:#00f">&lt;</span>/td<span style="color:#00f">></span>
                    <span style="color:#00f">&lt;</span>/tr<span style="color:#00f">></span>
                <span style="color:#00f">&lt;</span>/tbody<span style="color:#00f">></span>
            <span style="color:#00f">&lt;</span>/table<span style="color:#00f">></span>
        <span style="color:#00f">&lt;</span>/div<span style="color:#00f">></span>
    <span style="color:#00f">&lt;</span>/div<span style="color:#00f">></span>

    <span style="color:#00f">&lt;</span>script type<span style="color:#00f">=</span><span style="color:#093">"text/javascript"</span><span style="color:#00f">></span>
        angular.module(<span style="color:#093">'page'</span>, []);
        angular.module(<span style="color:#093">'page'</span>).controller(<span style="color:#093">'PageController'</span>, <span style="font-weight:700">function</span> ($scope, $http) {

            <span style="color:#00f">$</span>http.get(<span style="color:#093">'../../js/university/students.js'</span>)
            .success(<span style="font-weight:700">function</span>(data) {
                <span style="color:#00f">$</span>scope.students <span style="color:#00f">=</span> data;
            });
        });
    <span style="color:#00f">&lt;</span>/script<span style="color:#00f">></span>

<span style="color:#00f">&lt;</span>/body<span style="color:#00f">></span>
<span style="color:#00f">&lt;</span>/html<span style="color:#00f">></span>
</pre>
</ol></ul></ul></li></ol>

<h2><a id="Whats_next_242"></a>“What’s next?”</h2>
<p>The <em>In-System Development</em> model provides the <em>business application developers</em> with the right toolset for rapid application development. By leveraging a few built-in <em>templates</em> and the <em>Enterprise JavaScript API</em>, whole vertical scenarios can be set up in several minutes. With the close to <em>Zero Turn-Around-Time</em>, changes in the backend can be made and applied on the fly, through an elegant Web IDE. The perfect fit for your <em>digital transformation</em>!</p>
<blockquote>
<p>The goal of the Dirigible platform is clear - ease the developers as much as possible and let them concentrate on the development of critical business logic.</p>
</blockquote>
<p>So, what’s next? Can I provide my own set of templates? Can I expose a new Enterprise JavaScript API? Can I provide a new perspective/view? Can I build my own Dirigible stack? Can it be integrated with the services of my cloud provider?</p>
<p>To all these questions, the answer is simple: <strong><em>Yes</em></strong>, you can do it!</p>
<h2><a id="Resources_252"></a>Resources</h2>
<ul>
<li>Site: <a href="http://www.dirigible.io">http://www.dirigible.io</a></li>
<li>Help: <a href="http://www.dirigible.io/help/">http://www.dirigible.io/help/</a></li>
<li>API: <a href="http://www.dirigible.io/api/">http://www.dirigible.io/api/</a></li>
<li>YouTube: <a href="https://www.youtube.com/c/dirigibleio">https://www.youtube.com/c/dirigibleio</a></li>
<li>Facebook: <a href="https://www.facebook.com/dirigible.io">https://www.facebook.com/dirigible.io</a></li>
<li>Twitter: <a href="https://twitter.com/dirigible_io">https://twitter.com/dirigible_io</a></li>
</ul>
<p><strong>Enjoy!</strong></p>
   
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/february/images/pavlov.jpeg"
        alt="Yordan Pavlov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Yordan Pavlov<br />
            <a target="_blank" href="https://www.sap.com/">SAP</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/jordan-pavlov-ab3602107">LinkedIn</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ThuF">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>