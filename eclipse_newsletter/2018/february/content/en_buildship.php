<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <h2>Introduction</h2>
<p><a target="_blank" href="https://projects.eclipse.org/projects/tools.buildship">Buildship</a> is an Eclipse plugin that allows you to build applications and libraries using <a target="_blank" href="https://gradle.org">Gradle</a> through your IDE.</p> 

<p>It is actively maintained by the Gradle team, with 30 releases since the project’s inception, and is now included by default in the most popular Eclipse distributions such as “Eclipse IDE for Java EE Developers”. There are also a few popular Eclipse tools that rely on Eclipse Buildship to support Gradle-based projects, such as the <a target="_blank" href="https://docs.spring.io/sts/nan/v384/NewAndNoteworthy.html">Spring Tool Suite</a> or <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls#eclipse-jdt-language-server">JDT Language Server</a>.</p>

<p>The basic functionality of Buildship is <a target="_blank" href="https://github.com/eclipse/buildship/blob/master/docs/user/Overview.md">well-documented</a>, but there are some cool advanced features that I would like to present in this post. Let's get started.</p>

<h2>Project import</h2>
<p>First, let’s take a look at the import wizard. On the first page of the project import dialog box you can simply select the root project, click the Finish button, and the import process starts. But did you know that you can get a preview of your project in the same wizard? Just navigate to the third page of the wizard (click the Next> button twice) to make the preview visible.</p>
   
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/import_gradle_project.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/import_gradle_project_sm.png"></a></p>
   
   <h2>Project synchronization</h2>
<p>Another fundamental feature is the project synchronization. When modifying the build configuration, you can apply the changes by executing the <code>Gradle > Refresh Gradle Project</code> command from the context menu of the project node or the build script editor. Project synchronization even respects the customizations done in Gradle <code>eclipse</code> plugin configuration.</p> 

<p>For instance, you can use this to define a custom name for your projects. It might come handy if other workspace project names overlap with the ones in your Gradle build. To do that, just paste the snippet below into your build script.</p>

<pre>
apply plugin: 'eclipse'

eclipse {
    project {
        name = 'custom-project-name'
    }
}
</pre>

<p>There are some cases where you might want to modify the classpath entries that Gradle auto-generates for you. For those times, you can add, remove or modify entries through <code>eclipse.classpath.file.whenMerged</code>. The example below shows how you can make the Eclipse compiler ignore optional warnings.</p>
 
<pre>
apply plugin: 'java'
apply plugin: 'eclipse'

eclipse {
    classpath {
      file {
            whenMerged {
                def source = entries.find { it.path == 'src/main/java' }
                source.entryAttributes['ignore_optional_problems'] = 'true'
            }
        }
    }
}
</pre>

<p>You can learn more about configuring the Gradle Eclipse integration through the <a target="_blank" href="https://docs.gradle.org/current/dsl/org.gradle.plugins.ide.eclipse.model.EclipseModel.html">eclipse plugin DSL</a>.</p> 

<p>More examples on the classpath customization are available <a target="_blank" href="https://discuss.gradle.org/t/buildship-1-0-18-is-now-available">here</a>.</p>

<h2>Tasks view and task execution</h2>
<p>In the Eclipse IDE, you can execute tasks from the Gradle Tasks view. The functionality is simple, yet there are a couple of things that might be useful to know.  For instance, you can filter the content with the view's content with the corresponding action from the toolbar.</p>
   
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/buildship_toolbar.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/buildship_toolbar_sm.png"></a></p>
   
  <p>The other thing is that the view has a toolbar, which offers a filtering and grouping options.</p> 
   
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/buildship_show_taskselector.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/buildship_show_taskselector_sm.png"></a></p>
   
   <p>Regarding the task types, there are 3 options: show the task selectors, the project tasks, and all tasks. A task selector executes a task on the target project and on all subprojects that have the same task. On the command-line, the same thing happens when you run <code>gradle build</code>. A project task works on the target project only. This corresponds to the <code>gradle :subproject:build</code> command. The "Show all tasks" option makes the private tasks visible. The only particularity of a private task is that it has no group defined. You can list the private tasks by executing <code>gradle tasks --all</code>.</p>

<h2>Build scans support</h2>
<p>A <a target="_blank" href="https://gradle.com/build-scans">build scan</a> is a shareable and centralized record of a build. It is great tool analyze and share what happened during a Gradle execution. Since version 2.1, Buildship provides support for build scans.</p> 

<p>Automatic build scan publishing can be enabled in the preferences for a project or the entire workspace. If set, the <code>--scan</code> argument is automatically used for each build.</p> 
  
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/buildship_buildscans.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/buildship_buildscans_sm.png"></a></p>
   
   <p>If a build scan is published during the build then the <code>Open Build Scan</code> action will be enabled on the Gradle Executions view toolbar. Clicking on it will open the scan in the browser. </p>
   

      <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/buildship_open_build_scans.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/buildship_open_build_scans_sm.png"></a></p>
   
   <h2>Classpath separation</h2>

<p>Gradle provides a fine-grained classpath separation when building a Java project. Each project has a collection of source sets, which are the sources that are built together. A source set defines different classpaths for compilation and execution of the code. These classpaths are fully user-configurable.</p>

<p>The Eclipse IDE defines one classpath per project, which means that mapping the Gradle source sets is challenging. Prior versions of Buildship ignored the problem altogether and merged all source sets into one classpath. This was insufficient for application frameworks like Spring Boot, where the configuration is loaded from the classpath.</p> 

<p>Buildship 2.2 introduced a runtime classpath separation that solves the majority of use cases. It keeps track of which source folders and dependencies belong to what source sets and filters the runtime classpath accordingly. So, when you launch a Java application from <code>src/main/java</code> then the classes compiled from <code>/src/test/java</code> and the test dependencies won't be available.</p>

<p>This functionality works automatically. Just make sure that you have Buildship 2.2 installed, and your project uses Gradle 3.4 or newer.</p>
   
   <h2>Execute and rerun tests</h2>
<p>One popular way to execute a test is to double-click on the test task in the Tasks view, but there are ways run a subset of tests to get much faster feedback. You can run a single test class or method right from the editor. Just select the desired class or method and execute <code>Run.. > Gradle Test</code> command from the context menu. The same context menu element is available in the <code>Outline</code> view.</p>
   
   
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/run_gradle_test.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/run_gradle_test_sm.png"></a></p>
   
   <p>When a test fails, the error dialog shows the details of the failure. If you want to revisit the problem, you can either check the console output or find the same information via the Executions view. Just right-click on the failed test case node and select <code>Show failures</code>. You can check the same thing on the <code>:test</code> node. If a test result report is generated then a link to it will show up in the dialog.</p>
   
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2018/february/images/buildship_testreport.png"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/buildship_testreport_sm.png"></a></p>
   
   <p>From the same menu, you can open the test sources as well, which makes it easier to navigate between the Executions view and the source editor. There’s also a context menu option to let you re-run one chosen test.</p>
   
<h2>Get in touch</h2>
<p>Buildship is an open project with an active and welcoming community around it. If you have something to share — be it a bug report, a code change, or just a cool idea — there are several ways to get in touch.</p> 

<p>Buildship development is coordinated on <a target="_blank" href="https://github.com/eclipse/buildship">GitHub</a>, using a <a target="_blank" href="https://www.zenhub.com/">ZenHub</a> board. You can file new issues, add your &#128077; to existing ones to help us prioritize, or submit pull requests to the project there. Talk to us on the <a target="_blank" href="https://discuss.gradle.org/c/help-discuss/buildship">Gradle forums</a> if you want to take on a bigger task; a welcoming community is waiting to help you there.</p>

<p>You can get updates on Buildship development from the <a target="_blank" href="https://newsletter.gradle.com">Gradle Newsletter</a> and the <a target="_blank" href="https://blog.gradle.org">Gradle Blog</a>.</p>
   
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/february/images/donat.jpeg"
        alt="Donat Csikos" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Donát Csikós<br />
            <a target="_blank" href="https://gradle.org/">Gradle</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/donatcsikos">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/donatcsikos/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>