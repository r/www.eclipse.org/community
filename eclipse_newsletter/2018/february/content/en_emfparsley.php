<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <h2>Introduction</h2>
<p>Eclipse Modeling Framework (EMF) models are easily maintainable, useful, pluggable, amongst other things, but building user interfaces (UIs) from a model wasn't always straightforward. This is why <a target="_blank" href="https://www.eclipse.org/emf-parsley/">EMF Parsley</a> was developed to make creating UIs from EMF models just as easy.</p>

<p>EMF Parsley allows you to build complex custom applications and user interfaces in a matter of minutes. Since, by default, it delegates to EMF.Edit, this means that you can easily port an existing EMF application to EMF Parsley, or simply use both Parsley and your custom .edit plugin projects.</p>
<p>Every single aspect of your project is easily customizable, including label and content providers, resource management, features handling, etc. EMF Parsley does not rely on extension points for specifying customizations: it uses <b>Dependency Injection</b> (relying on Google Guice) and this makes it easy to inject custom implementation in the framework, focusing on the customization of single aspects that will be consistently used by EMF Parsley.</p>
<p>Customizations can be implemented in Java or by using EMF Parsley's flexible Domain Specific Language (DSL).</p>
<p>Summarizing, that is why we think you should use EMF Parsley in your projects:</p>
    <ol>
        <li>it is very lightweight (it just leverages EMF.Edit layer, Eclipse Xtext for the DSL and little more) and can be added to existing projects with minimum impact</li>
        <li>it provides out-of-the-box integrations with XMI, Teneo and CDO persistence implementations (it is however possible - and super easy - to provide your own I/O services)</li>
        <li>it uses Dependency Injection (via Google Guice) instead of Extension Point mechanism</li>
        <li>it ships with an Eclipse/JDT integrated DSL with rich tooling and a static type system</li>
        <li>it is generation-code oriented (debuggable!) instead of using a reflective approach (no bad runtime surprises). This also guarantees seamlessly upgrading when new versions are released</li>
        <li>thanks to the out-of-the-box RAP integration it allows developers to build Web application with the same Eclipse RCP technology stack used for desktop (honoring the web/desktop ‘single-sourcing’ RAP pattern)</li>
    </ol>
   
   <p>Now that we have your attention, find out what's behind the tool and how easy it is to use it!</p>
   
   <h2>Parsley DSL</h2>
<p>The Parsley DSL is built with <a target="_blank" href="http://www.eclipse.org/Xtext/">Xtext</a> and Xbase, which allows for compact definition of many aspects. Thanks to Xtext, the DSL provides a rich IDE tooling; just open the DSL editor and make use of Ctrl-space for auto-completion features for example. The entire Java Type-system and APIs are available within Xtext DSLs just like in the standard Java editor. This means that from the DSL you can access all the Java libraries existing in your classpath.</p>
   
   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley3.png"></p>
   
   <p>You can write code directly using the DSL or call methods from a Java class if reuse is required. As you write in the DSL editor a set of Java classes are generated and maintained automatically.</p>
<p>The Eclipse Quickfix feature is also available, as in the standard Java editor.</p>
   
   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley6.png"></p>
   
   <p>This is where Xbase comes into play and makes the Eclipse IDE tooling integration so seamless and advanced, you can even add breakpoints in the DSL and debug the DSL itself, rather than the generated code.</p>
   
   <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley2.png"></p>
   
   <p>As you can see below, standard and EMF context menus are easily customizable and Undo/Redo features are managed by EMF Parsley automatically.</p>
   
    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley1.png"></p>
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley5.png"></p>
     
     <p>The Parsley DSL is statically typed, in order to promote static checks and to catch possible problems during compilation, not at run-time. This means that the DSL strongly differs from purely reflective approaches. The debugging functionality also allows you to have full control of the code. Again, making possible problems easier to spot compared to a reflective runtime approach.</p>
<p>The Parsley DSL is only an additional facility for the usage of EMF Parsley. All EMF Parsley APIs are Java APIs, thus specifications made with the DSL and code written in Java will seamlessly coexist and interoperate.</p>
     
   <h2>Usage with a generic service or even non-EMF-models</h2>
<p>The fact that Parsley leverages EMF doesn't mean that you can only use it if you adopted EMF for modeling. In fact, although you can have EMF persistence (Teneo and CDO) out-of-the-box, it is extremely easy to provide your own Resource Manager from external services.</p>
<p>For example, you could have web services, legacy code or something else: just wrap your retrieved data in an EMF model as simple as needed and you are done.</p>
<p>Finally, thanks to the data-binding mechanism, any change in the underlying model will be reflected on the UI automatically, so you can focus on the model content instead of dealing with UI custom code.</p>

<h2>Single sourcing for Desktop and Web</h2>
<p>Switching from RCP Running platform to a RAP (Remote Application Platform) Target Platform allows you to use the same source code to run your application on the Web!</p>
   
    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley7.png"></p>
    
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley8.png"></p>
     
     <h2>Opening to other development scenarios</h2>
<p>Recently the Parsley Core has been decoupled from the UI parts and made accessible in plain JEE projects, throughout JSON APIs. Parsley now works with all of its customizations, even virtually in any UI development environment.</p>
<p>Initial implementations have been made with:</p>
    <ul>
        <li>AngularJS</li>
        <li>GWT</li>
        <li>Mobile Android (using Eclipse Andmore project)</li>
        <li>Mobile Hybrid (using Eclipse Thym project)</li>
    </ul>
    
    <h2>Getting Started</h2>
    
    <p>EMF Parsley is part of the Eclipse Release Train, so it can be found in the main Eclipse update site.</p>
<p>For up-to-date releases of EMF Parsley, you may want to use our update site:</p>

<a target="_blank" href="http://download.eclipse.org/emf-parsley/updates">http://download.eclipse.org/emf-parsley/updates</a>

<p>If you want to contribute to EMF Parsley, you can use Oomph: EMF Parsley is listed on the Oomph Catalog, so anyone can setup the development environment to use it or contribute to the project in a matter of minutes.</p>
     
        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2018/february/images/emfparsley4.png"></p>
        
       <h2>Toward version 2.0</h2>
<p>Along with the 2018 Eclipse release Photon, EMF Parsley will be released with its new 2.0 version.</p>
<p>In this version, APIs will be strongly revisited in order to make them simpler and more usable. However, we will ensure backward compatibility as long as the DSL has been used. (A complete migration guide will be part of the release).</p>
<p>Moreover, it will contain the following features:</p>
	<ul>
        <li>Heavy decoupling from SWT, in order to allow UI rendering with other GUI environments (for a broader web and mobile integration with other frameworks)</li>
        <li>Optimizations & bug fixing</li>
        <li>Developer Tools, like the “Live Preview” that allows the developer to see live the changes specified in the DSL without launching a new Eclipse instance</li>
        <li>New built-in components like Search widget that automatically add a filter to existing viewers</li>
	</ul>

<h2>Links & Resources</h2>
<h3>EMF Parsley EclipseCon Talks</h3>
    <ul>
    <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/lesson-learned-using-eclipse-and-emf-building-desktop-web-applications">EclipseCon Europe 2017 - Lesson learned from using Eclipse and EMF for building desktop & web Applications</a></li>
    <li><a target="_blank" href="https://www.eclipsecon.org/france2017/session/emf-parsley-dsl-extensive-use-case-xtextxbase-powerful-mechanisms">EclipseCon France 2017 - The EMF Parsley DSL: an extensive use case of Xtext/Xbase powerful mechanisms</a></li>
    <li><a target="_blank" href="https://www.eclipsecon.org/europe2016/session/six-good-reasons-spice-your-projects-emf-parsley">EclipseCon Europe 2016 - Six good reasons to spice up your projects with EMF Parsley</a></li>
    <li><a target="_blank" href="https://www.eclipsecon.org/france2016/session/five-good-reasons-spice-your-projects-emf-parsley">EclipseCon France 2016 - Five good reasons to spice up your projects with EMF Parsley</a></li>
    <li><a target="_blank" href="https://www.eclipsecon.org/europe2015/session/emf-uis-how-use-emf-parsley-get-desktop-web-and-mobile-uis-model.html">EclipseCon Europe 2015 - From EMF to UIs: how to use EMF Parsley to get desktop, web and mobile UIs from the model</a></li>
    <li><a target="_blank" href="https://www.eclipsecon.org/europe2013/add-some-spice-your-application-using-emf-parsley-your-ui.html">EclipseCon Europe 2013 - Add some spice to your application! (using EMF Parsley in your UI)</a></li>
    </ul>

<h3>Other useful resources</h3>
    <ul>
    <li><a target="_blank" href="http://www.eclipse.org/emf-parsley">EMF Parsley Website</a></li>
    <li><a target="_blank" href="https://www.eclipse.org/emf-parsley/documentation.html">Documentation</a></li>
    <li><a target="_blank" href="https://www.eclipse.org/forums/index.php/f/263/">Forum</a></li>
    <li><a target="_blank" href="https://bugs.eclipse.org/bugs/buglist.cgi?product=EMF.Parsley">Bugzilla</a></li>
    <li><a target="_blank" href="https://sonar.eclipse.org/dashboard/index/26676">Sonar Metrics</a></li>
    </ul>
            
<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/lorenzo.jpg"
        alt="Lorenzo Bettini" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Lorenzo Bettini<br />
        <a target="_blank" href="http://www.lorenzobettini.it/">DISIA - Univ. Firenze, Italy</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/lorenzo_bettini">Twitter</a></li>
      <li><a class="btn btn-small btn-warning" target="_blank" href="www.lorenzobettini.it">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/lorenzo-bettini-2719a02">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/francesco.png"
        alt="Francesco Guidieri" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Francesco Guidieri<br />
        <a target="_blank" href="http://www.rcp-vision.com">RCP Vision</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fraguid">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/francesco-guidieri-1898221">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/vincenzo.png"
        alt="Vincenzo Caselli" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Vincenzo Caselli
       <br />
        <a target="_blank" href="http://www.rcp-vision.com">RCP Vision</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/vcaselli">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/vincenzo-caselli-1613386">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>