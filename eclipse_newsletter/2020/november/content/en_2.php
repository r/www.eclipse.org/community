<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title">Towards a Community Vision for <br> Jakarta EE 10</h1>

<p>Now that Jakarta EE 8 has been delivered and&nbsp;<a href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9">Jakarta EE 9</a> is almost complete, there&rsquo;s no reason work on Jakarta EE 10 should not begin. Ideally, Jakarta EE 10 will be released by late 2021 to meet the community&rsquo;s long-time request for one major platform release a year.</p>

<p>To keep the platform fresh and relevant, Jakarta EE 10 must deliver a compelling set of updates. For several months, the&nbsp;<a href="https://jakartaee-ambassadors.io/">Jakarta EE Ambassadors</a> have been drafting a&nbsp;<a href="https://docs.google.com/document/d/1uZFBoIujXCc-gQhCzh_ZdlKEsrsV0yVVIHzBTI3usF8/edit#heading=h.2fh9nijzga7k">contribution guide</a> to help shape Jakarta EE 10 and facilitate community contribution. So far, three key themes have emerged:</p>

<ul>
	<li>Contexts and Dependency Injection (CDI) alignment</li>
	<li>Java Standard Edition (SE) alignment</li>
	<li>Closing standardization gaps</li>
</ul>

<p>In my view, these are modest, but much needed, changes, especially in the context of releases such as Java EE 7 and Java EE 8. They represent long-standing items with good community consensus and prior discussion in the Java EE 6, Java EE 7, Java EE 8, and Java EE 9 timeframes (Oracle had publicly shared the Java EE 9 scope before the technology was transferred to the Eclipse Foundation).</p>

<p>Here&rsquo;s a brief summary of the Jakarta EE 10 changes currently envisioned in the Jakarta EE Ambassadors&rsquo; draft document.</p>

<h2>Improving CDI Alignment</h2>

<p>Since its introduction in Java EE 6,&nbsp;<a href="https://jakarta.ee/specifications/cdi/">Jakarta CDI</a> has become the central component model in the platform and many parts of the platform already align with it. Key examples include:</p>

<ul>
	<li>Defining <code>@Transactional</code> in&nbsp;<a href="https://jakarta.ee/specifications/transactions/">Jakarta Transactions</a></li>
	<li>Managed bean deprecation in&nbsp;<a href="https://jakarta.ee/specifications/faces/">Jakarta Server Faces</a></li>
	<li>Use of CDI in&nbsp;<a href="https://jakarta.ee/specifications/security/">Jakarta Security</a></li>
</ul>

<p>However, parts of the platform still need to take better advantage of CDI:</p>

<ul>
	<li>Many useful features remain specific to the dated and redundant&nbsp;<a href="https://jakarta.ee/specifications/enterprise-beans/">Jakarta Enterprise Beans</a> programming model. These features could be made more flexibly available anywhere CDI is enabled. Once modernized to be implemented through CDI, these features could be moved into the specifications where they make best sense. Examples include:

	<ul>
		<li>Defining <code>@Asynchronous</code>, <code>@Schedule</code>, <code>@Lock</code>, and <code>@MaxConcurrency</code> in&nbsp;<a href="https://jakarta.ee/specifications/concurrency/">Jakarta Concurrency</a></li>
		<li>Defining Message-Driven Bean style <code>@MessageListener</code> in&nbsp;<a href="https://jakarta.ee/specifications/messaging/">Jakarta Messaging</a></li>
		<li>Moving <code>@RolesAllowed</code>, <code>@RunAs</code> to Jakarta Security</li>
	</ul>
	</li>
	<li>Additional specifications could make their artifacts injectable using CDI, deprecate their own features in favor of CDI, and better integrate with CDI. For example:
	<ul>
		<li><a href="https://jakarta.ee/specifications/batch/">Jakarta Batch</a> artifacts could be made injectable using CDI.</li>
		<li><a href="https://jakarta.ee/specifications/restful-ws/">Jakarta RESTful Web Services</a> could deprecate <code>@Context</code> in favor of CDI injection.</li>
		<li>Jakarta Concurrency could better handle CDI context propagation.</li>
	</ul>
	</li>
</ul>

<h2>Improving Java SE Alignment</h2>

<p>Java EE has a long history of adapting to Java SE innovations. Java EE 5 was the first mainstream technology to prove usage of annotations as metadata. And Java EE 8 embraced Java SE 8 features such as repeatable annotations, the Date-Time API, and <code>CompletableFuture</code>.</p>

<p>There are opportunities to further take advantage of Java SE in Jakarta EE 10. For example:</p>

<ul>
	<li>Jakarta Concurrency is not fully aligned with Java SE 8. The API could be adapted to properly use <code>CompletableFuture</code>.</li>
	<li>An increasing number of Jakarta EE technologies, such as CDI, can be used outside the platform as a standalone technology. This capability makes platform technologies more usable and flexible. In Jakarta EE 10, this concept could be extended. For example, Jakarta Messaging and Jakarta RESTful Web Services could provide standalone bootstrap APIs.</li>
	<li>An important step toward making more technologies usable in Java SE is using Arquillian and JUnit 5 to modernize technology compatibility kits and make them standalone. While some amount of progress toward this objective was made in Jakarta EE 9, more work is needed.</li>
</ul>

<h2>Closing Standardization Gaps</h2>

<p>One of the platform&rsquo;s key value propositions is that it provides portability and vendor neutrality for the broadest set of users in the ecosystem. This is achieved by standardizing commonly used features that are vendor-specific or available outside of Jakarta EE. There are opportunities in this area that represent long-standing feature gaps. For example:</p>

<ul>
	<li>Jakarta Security currently provides out-of-the-box support for databases and LDAP for authentication and authorization. The API could be modernized to add support for OAuth2, JavaScript Object Notation (JSON) Web Token (JWT), and OpenID Connect.</li>
	<li>Jakarta Batch could be updated with a Java job definition API as an alternative to XML. Some implementations make this possible today.</li>
	<li>Jakarta Concurrency could add <code>@ManagedExecutorServiceDefinition</code> to increase portability when defining managed executors.</li>
	<li>A microservices profile that initially consists of CDI, REST, and JSON APIs could be introduced. This small profile could be the foundation for<a href="https://microprofile.io/"> </a><a href="https://microprofile.io/">Eclipse MicroProfile</a> runtimes. The profile would require creation of a lighter version of CDI that includes a subset of features targeting Jakarta EE, but a superset of features targeting the<a href="https://jakarta.ee/specifications/cdi/2.0/cdi-spec-2.0.html#part_1"> </a><a href="https://jakarta.ee/specifications/cdi/2.0/cdi-spec-2.0.html#part_1">core CDI</a> and Java SE.</li>
	<li>Jakarta Messaging interoperability with the Advanced Message Queuing Protocol (AMQP), Apache Kafka stream-processing software platform, and MQ Telemetry Transport or Message Queuing Telemetry Transport (MQTT) protocol could be explored. This could require creation of a lighter weight profile and deprecation of some older, less frequently used features.</li>
	<li>It may be possible to support JCache as a second-level<a href="https://jakarta.ee/specifications/persistence/"> </a><a href="https://jakarta.ee/specifications/persistence/">Jakarta Persistence</a> cache or application server state cache.</li>
	<li>Jakarta Transactions could standardize common optimizations such as last resource commit, one-phase commit, and local transactions. The Oracle WebLogic Server supports such features today and they could be contributed as a basis for standardization.</li>
</ul>

<h2>Potential New Specifications in Jakarta EE 10</h2>

<p>A few new APIs could be added to the Jakarta EE 10 platform. The following APIs were proposed by Oracle for Java EE 9 and represent standardization gaps:</p>

<ul>
	<li><a href="https://jakarta.ee/specifications/nosql/">Jakarta NoSQL</a> and&nbsp;<a href="https://www.jnosql.org/">Eclipse JNoSQL</a> aim to enable NoSQL access for Jakarta EE applications. Jakarta NoSQL could initially be added as an optional part of the Jakarta EE platform.</li>
	<li><a href="https://jakarta.ee/specifications/mvc/">Jakarta Model-View-Controller (MVC)</a> was started in Java EE 8 and aims to provide a more action-oriented Java web framework for Jakarta EE applications. It could initially be added as an optional part of the Jakarta EE platform.</li>
	<li>Adding the ability to externalize configuration is a long-standing gap that&rsquo;s been discussed since Java EE 7. A key goal is to enable specifications that need significant configuration to consume it from outside the application, such as from the environment. Specifications that could use this functionality include Jakarta Persistence, Jakarta Messaging, and&nbsp;<a href="https://jakarta.ee/specifications/mail/">Jakarta Mail</a>. It&rsquo;s possible this need could be met through alignment with Eclipse MicroProfile.</li>
</ul>

<h2>Looking Beyond Jakarta EE 10</h2>

<p>There are a few additional changes on the horizon, but they won&rsquo;t likely be ready for Jakarta EE 10. Most will benefit from further maturity, analysis, development, and experimentation. They include:</p>

<ul>
	<li>Jakarta Persistence alignment with Java SE Records</li>
	<li>More reactive non-blocking input/output (NIO) capabilities in:
	<ul>
		<li><a href="https://jakarta.ee/specifications/servlet/">Jakarta Servlet</a> or an equivalent lighter layer</li>
		<li>Jakarta Persistence, which requires a widely supported version of reactive NIO Java Database Connectivity (JDBC) first, ideally in Java SE</li>
		<li>Jakarta Restful Web Services</li>
		<li>Jakarta MVC</li>
	</ul>
	</li>
</ul>

<h2>Review the Draft Contribution Guide</h2>

<p>While there&rsquo;s already been a reasonable amount of community review on the Jakarta EE Ambassadors&rsquo; Jakarta EE 10 contribution guide, more reviews are welcome.</p>

<p>The objective is to finalize the draft by early 2021 and begin moving forward with Jakarta EE 10. Even though the guide is high-level, there are work items I didn&rsquo;t cover in this brief article, so reading the draft is worthwhile even if you don&rsquo;t have feedback.</p>

<p>Access the draft Jakarta EE Ambassadors&rsquo; Jakarta EE 10 contribution guide&nbsp;<a href="https://docs.google.com/document/d/1uZFBoIujXCc-gQhCzh_ZdlKEsrsV0yVVIHzBTI3usF8/edit#heading=h.2fh9nijzga7k">here on Google Docs</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img-responsive" alt="<?php print $pageAuthor; ?>" src="images/reza_rahman.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
          Reza Rahman is principal program manager for Java on Azure at Microsoft and has been an official Java technologist at Oracle. He is also the author of the popular book EJB 3 in Action. Reza has been a frequent speaker at Java User Groups and conferences worldwide for many years, is an avid contributor to industry journals, and helps lead the Philadelphia Java User Group.
          </p>
		    </div>
      </div>
    </div>
  </div>
</div>