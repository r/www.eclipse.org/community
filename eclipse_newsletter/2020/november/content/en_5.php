<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Arjan Tijms</h2>
<img width="155" class="float-left margin-right-40 img img-responsive" src="images/arjan_tijms_2020.png" alt="Arjan Tijms">
<ul>
  <li>Involved in open source since: 2003</li>
  <li>Works for: Self-employed software consultant, author</li>
  <li>Eclipse Foundation contributor since: 2019</li>
  <li>Involved in: 23 <a href="https://projects.eclipse.org/projects/ee4j">Eclipse Enterprise for Java (EE4J) projects</a></li>
  <li>Committer to: <a href="https://projects.eclipse.org/user/10206">All 23 projects in which he is involved</a></li>
  <li>Committer since: 2019</li>
  <li>Fun fact: Arjan is very international. He was born in Germany, his mother is from the United Kingdom, and he primarily grew up in the Netherlands. He has also programmed in Java while on the island of Java and worked on Jakarta EE while in the city of Jakarta.</li>
</ul>
<hr />
<h2>Why did you first get involved in open source software communities?</h2>
<p>After I completed my Masters thesis in 2003, I started working for a company that used open source software. When something didn’t work, I wanted to resolve the problem and I wanted to share it with other developers. That was my initial dabbling in open source.</p>
<p>But, my real involvement in open source started when I introduced the OmniFaces project in the JavaServer Faces (JSF) community with my friend, Bauke Scholtz. It became quite successful. There were a lot of downloads and a lot of interest from the community.</p>

<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>
<p>When Java EE moved to the Eclipse Foundation and was renamed <a href="https://jakarta.ee/">Jakarta EE</a>, the Eclipse Foundation became the place to be for everything related to Jakarta EE.</p>
<p>I was already a committer on Java projects under the Java Community Process (JCP) — mainly Java EE Security and JSF — and this made me an initial project committer at the Eclipse Foundation. So, it was a very natural move for me.</p>

<h2>How would you summarize your experiences as a committer?</h2>
<p>It’s quite interesting. On one hand, a lot of work goes into it, but on the other hand, it’s good to be able to discuss what you do and get feedback from other people. The Eclipse Foundation has established processes and a handbook to follow so it’s an environment where a lot of things are done for you, but you also have to put in a lot of work yourself.</p>
<p>You have to balance the different viewpoints, interests, and ideas of other committers and negotiate with contributors that don’t always have the most optimal solution. And you have to do this while meeting the deadlines of the company or customers you’re working with.</p>
<p>But, it’s quite cool to influence the project direction and it’s rewarding to have code out there that other people are using. Once, I was booking a flight with a famous German aviation company and I realized they were actually using my code in their software. That was a very fulfilling and rewarding feeling. It’s also a nice surprise to meet people at conferences and they already know who you are.</p>

<h2>
What are your next steps and goals as a committer and Eclipse Foundation community member?
</h2>
<p>I would really like to certify the Piranha software I am working on with the Jakarta EE Web Profile.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>
<p>You absolutely don’t have to be the most brilliant, sophisticated, or advanced person in the world to be a committer, but you do need a lot of passion for the work and for the role. You must be willing to dedicate a lot of time to open source because you really like it and it’s your hobby. That passion will take you a long way.</p>
<p>
Also, if you work with open source software, you have an advantage, and your customers have an even bigger advantage because you implemented the stack you’re working on and you know the other people involved. It’s very different than just using the software as a tool. You have all of those experiences to pull from.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
