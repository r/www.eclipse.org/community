<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>In this short tutorial, I&rsquo;ll explain how to set up and customize a WildFly application server operating in standalone mode to run a&nbsp;<a href="https://jakarta.ee/">Jakarta EE</a> application in a container-based environment orchestrated by Kubernetes.</p>

<p>WildFly provides a modern application framework that simplifies development of web applications and microservices. It is&nbsp;<a href="https://jakarta.ee/compatibility/">Jakarta EE 8 platform compatible and web profile compatible</a>. It also includes&nbsp;<a href="https://microprofile.io/">Eclipse MicroProfile</a> version 3.3, the latest available version. All WildFly runtime services minimize heap allocation and applications start very fast with a minimum of memory.</p>

<h2>Start WildFly in a Docker Container</h2>

<p>WildFly provides an&nbsp;<a href="https://hub.docker.com/r/jboss/wildfly">official container image</a> that is well-documented and provides the latest WildFly containers. To start WildFly in a Docker container, run the command shown below.</p>

<pre>
<code>$ docker run -it jboss/wildfly</code>
</pre>

<p>The Docker image is based on OpenJDK 11, which is optimized to run in container environments such as Kubernetes. Docker Hub includes documentation about the various ways you can build your own images. I describe one such approach here.</p>

<h2>Build Your Custom Docker Image</h2>

<p>To build a WildFly 20 Docker container with your own Jakarta EE application, set up a new Dockerfile and copy your application into the deployment directory, as shown below.</p>

<pre>
<code>
FROM jboss/wildfly
ADD your-awesome-app.war /opt/jboss/wildfly/standalone/deployments/
</code>
</pre>

<p>WildFly automatically deploys your application each time the server starts.</p>

<h2>Customize the Configuration</h2>

<p>In cases where you need additional configurations, such as security realms or database connection pools, to run the application, you can customize the main configuration file &mdash; standalone.xml &mdash; or add modules, such as Java Database Connectivity (JDBC) drivers.</p>

<p>Below, I show how to add a PostgreSQL JDBC driver to your WildFly Server. The ability to separate external resources, such as database connections, from your application is one of Jakarta EE&rsquo;s strengths.</p>

<p>The following directory tree shows a custom configuration that adds a PostgreSQL JDBC driver as a module and a custom standalone.xml file.</p>
<pre>
wildfly
      ├── modules
      │   ├── org
      │   │   └── postgresql
      │   │       └── main
      │   │       	  ├── module.xml
      │   │       	  └── postgresql-42.2.5.jar
      └── standalone.xml
</pre>

<h2>Configure the JDBC Drive Module and Database Pool</h2>

<p>To configure the JDBC driver module and a JDBC database pool, copy the standalone.xml file from a running WildFly container, then add the configuration shown below into the &lt;datasource&gt; section. This adds the new postgreSQL JDBC driver module and a generic database pool that can be configured in Kubernetes using environment variables.</p>

<pre>
<code>
.....
...
&lt;datasource jta="true" jndi-name="java:/jdbc/office"
    pool-name="office" enabled="true" use-ccm="true"&gt;
    &lt;connection-url&gt;${env.POSTGRES_CONNECTION}&lt;/connection-url&gt;
    &lt;driver-class&gt;org.postgresql.Driver&lt;/driver-class&gt;
    &lt;driver&gt;postgresql&lt;/driver&gt;
    &lt;security&gt;
        &lt;user-name&gt;${env.POSTGRES_USER}&lt;/user-name&gt;
        &lt;password&gt;${env.POSTGRES_PASSWORD}&lt;/password&gt;
    &lt;/security&gt;
&lt;/datasource&gt;
&lt;drivers&gt;
    &lt;driver name="h2" module="com.h2database.h2"&gt;
        &lt;xa-datasource-class&gt;org.h2.jdbcx.JdbcDataSource
        &lt;/xa-datasource-class&gt;
    &lt;/driver&gt;
    &lt;driver name="postgresql" module="org.postgresql"&gt;
        &lt;driver-class&gt;org.postgresql.Driver&lt;/driver-class&gt;
    &lt;/driver&gt;
&lt;/drivers&gt;
...
.....
</code>
</pre>

<h2>Build Your Custom Docker Image</h2>

<p>Now you can add the new configuration files into your Dockerfile and build your new customized Docker image, as shown below.</p>

<pre>
<code>
FROM jboss/wildfly:20.0.1.Final
 
COPY ./wildfly/modules/ /opt/jboss/wildfly/modules/
COPY ./wildfly/standalone.xml /opt/jboss/wildfly/standalone/configuration/
 
# Deploy artefact
ADD your-awesome-app.war /opt/jboss/wildfly/standalone/deployments/
</code>
</pre>

<p>If you provide a custom configuration, make sure your Docker image is based on a WildFly version that matches your standalone.xml and module configuration. Otherwise, a newer WildFly container may not start with your configuration.</p>

<h2>Run the Container in Kubernetes</h2>

<p>If you don&rsquo;t already have a Kubernetes environment, the open source project Imixs-Cloud provides an easy way to set up a self-managed Kubernetes cluster.</p>

<p>The example below shows a YAML Ain&#39;t Markup Language (YAML) deployment file for the custom WildFly image.</p>

<pre>
<code>
apiVersion: apps/v1
kind: Deployment
metadata:
  name: awesome-app
  labels:
    app: awesome-app
spec:
  replicas: 1
  selector:
    matchLabels:
  	app: awesome-app
  strategy:
	type: Recreate
  template:
	metadata:
  	labels:
    	app: awesome-app
	spec:
  	containers:
  	- name: awesome-app
    	image: registry.foo.com/library/awesome-app:latest
    	env:
    	- name: POSTGRES_CONNECTION
      	value: jdbc:postgresql://postgre/my-db
    	- name: POSTGRES_PASSWORD
      	value: mypassword
    	- name: POSTGRES_USER
   	   value: myuser
    	ports:
          - name: web
        	containerPort: 8080
    	resources:
      	requests:
        	memory: "512Mi"
      	limits:
        	memory: "1Gi"
  	restartPolicy: Always
</code>
</pre>

<p>Note that the database pool is configured using the environment variables <code>POSTGRES_CONNECTION</code>, <code>POSTGRES_PASSWORD</code>, and <code>POSTGRES_USER</code> as defined in the WildFly standard.xml file. This means our container is still independent from a specific database instance and can be connected to different environments.</p>

<h2>Set Memory Limits</h2>

<p>As previously noted, OpenJDK 11 is optimized to run in containers. Use the memory limits defined in the deployment object to define how Kubernetes should manage container memory.</p>

<p>In the example below, the container starts with 512 MB of memory and can use a maximum of 1 GB. The heap size is automatically controlled by OpenJDK.</p>

<pre>
<code>
resources:
      	requests:
        	memory: "512Mi"
      	limits:
        	memory: "1Gi"
</code>
</pre>

<p>In most cases, no additional configuration is needed. However, if you want to configure factors such as the ratio of how OpenJDK uses available memory for heap, you can include additional java virtual machine (JVM) options, such as the one shown in the example below.</p>

<pre>
<code>
- name: JAVA_OPTS
  value: "-XX:MaxRAMPercentage=75.0"
</code>
</pre>

<p>WildFly automatically uses this new Java option on startup. You can easily verify the settings in the container log file. Depending on your application, other options may be useful as well.</p>

<h2>Configure the Liveness Probe and Health Check</h2>

<p>With Eclipse MicroProfile, WildFly provides various metrics and a Health API out of the box.</p>

<p>The&nbsp;<a href="https://microprofile.io/project/eclipse/microprofile-health">Eclipse MicroProfile Health API</a> can be used for a liveness probe in Kubernetes. The MicroProfile health check specification defines three HTTP endpoints:</p>

<ul>
	<li><code>*/health</code> to test the liveness and the readiness of the runtime.</li>
	<li><code>*/health/live</code> to test the liveness of the runtime.</li>
	<li><code>*/health/ready</code> to test the readiness of the runtime.</li>
</ul>

<p>The health check HTTP endpoints are accessible through WildFly HTTP management interface port 9990. To activate the management interface, change the default command to start WildFly at the end of your Dockerfile, as shown in the example below.</p>

<pre>
<code>
# Run with management interface
CMD ["/opt/jboss/WildFly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
</code>
</pre>

<p>This command publishes port 9990. With the additional configuration shown below, and the corresponding service configuration, you can activate the liveness probe in Kubernetes YAML deployment files.</p>

<p>In the example below, Kubernetes checks the WildFly server for liveness every five seconds after a start delay of 60 seconds, which is sufficient for a complete startup.</p>

<pre>
<code>
livenessProbe:
      	httpGet:
        	path: /health
        	port: 9990
      	initialDelaySeconds: 60
      	periodSeconds: 5
</code>
</pre>

<p>Note that you can also use the Eclipse MicroProfile Health API to add application-specific health checks and use them in your custom liveness probe. For example, you can implement a check to verify the database is up and running and in a consistent state.</p>

<h2>Monitor and Analyze Application Metrics</h2>

<p>WildFly also provides a large number of server metrics based on the&nbsp;<a href="https://microprofile.io/project/eclipse/microprofile-metrics">Eclipse MicroProfile Metrics API</a>. These metrics can be monitored using the Prometheus system monitoring and alerting toolkit and the Grafana analytics platform. Of course, you can easily add your own application metrics too, as shown in the example below.</p>

<pre>
<code>http://your-server:9990/metrics</code>
</pre>

<h2>Monitor and Manage a Running Instance</h2>

<p>WildFly provides a convenient web interface to monitor and manage a running instance. This interface is accessible through port 9990 but is protected by default.</p>

<p>To access the WildFly web interface, use Secure Shell (SSH) to access a running WildFly container and activate the admin user, as shown in the example below.</p>

<pre>
<code>$ ./WildFly/bin/add-user.sh</code>
</pre>

<p>You will be prompted to add a new management user with a password. Complete this step, then log in to the web interface, as shown in the example below.</p>

<pre>
<code>http://your-server:9990</code>
</pre>

<p>WildFly is now ready to run in container-based environments, such as Kubernetes.</p>

<p>With the latest Docker image and Eclipse MicroProfile, you can configure WildFly in various ways and integrate it smoothly into a microservices architecture.</p>

<h2>Learn More</h2>

<p>To learn more about Jakarta EE, click&nbsp;<a href="https://jakarta.ee/">here</a>.</p>

<p>To learn more about Eclipse MicroProfile, click&nbsp;<a href="https://microprofile.io/">here</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/ralph_soika.png" />
				</div>
				<div class="col-sm-16">
					<p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Ralph Soika works at Imixs Software Solutions GmbH. He is the project lead of the open source project Imixs-Workflow, as well as a committer and project lead on the&nbsp;<a href="https://www.eclipse.org/modeling/mdt/?project=bpmn2">Eclipse Business Process Model and Notation (BPMN2)</a> project.
          </p>
        </div>
			</div>
		</div>
	</div>
</div>