<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>With the release of Jakarta EE 9 this month, there are a few key points about the release that developers should keep in mind as they work with the code.</p>

<p>One of the most important things to understand about the Jakarta EE 9 release is that it&rsquo;s not backwards-compatible with Jakarta EE 8 or Java EE 8. To help you understand why this is, here&rsquo;s a quick look at the three&nbsp;<a href="https://jakarta.ee/">Jakarta EE</a> releases so far. Note that each release has a different combination of Apache Maven coordinates and Java package name:</p>

<ul>
	<li>The first Jakarta EE release was announced in January 2019. It was a technical release that was almost identical to Java EE 8, and it was code named EE4J_8. In fact, every Jakarta EE/EE4J project used to have a Git branch with this code name. This release proved that the Java EE code Oracle contributed to the Eclipse Foundation was complete. However, it was not a true Jakarta EE release. The APIs used the javax Maven coordinates (javax.ws.rs:javax.ws.rs-api:2.1.1, for example) and the javax Java package name (javax.ws.rs.core, for example).</li>
	<li>The second release is known as Jakarta EE 8. The APIs in&nbsp;<a href="https://jakarta.ee/release/">Jakarta EE 8</a> use Jakarta EE Maven coordinates (jakarta.ws.rs:jakarta.ws.rs-api:2.1.6, for example), but the javax Java package name (javax.ws.rs.core, for example). Jakarta EE 8 is 99.9 percent backwards-compatible with Java EE 8.</li>
	<li>The third release is known as Jakarta EE 9. The APIs in Jakarta EE 9 use Jakarta EE Maven coordinates (jakarta.ws.rs:jakarta.ws.rs-api:3.0.0, for example), but the Java package name has been changed to jakarta (jakarta.ws.rs.core, for example).</li>
</ul>

<h2>The Jakarta Package Name Change Has Consequences</h2>

<p>The shift to the jakarta package name has a number of consequences that are important for developers to understand:</p>

<ul>
	<li>Every API must transition the Java package name from javax to jakarta.</li>
	<li>Every Jakarta EE specification must be updated to reflect the API changes.</li>
	<li>Every implementation of a specification must be adjusted to handle the new APIs.</li>
	<li>New versions of application servers must be released. The new implementations won&rsquo;t work on Java EE-based application servers and the old implementations won&rsquo;t work on Jakarta EE 9-based application servers.</li>
	<li>Jakarta EE 8 APIs do not work with Jakarta EE 9 implementations and vice versa.</li>
	<li>In cases where the Java EE API is needed, mixing Jakarta EE 8 and Jakarta EE 9 APIs will cause issues with Maven because they both use the same Maven coordinates. For example, this can occur when using the Java Architecture for XML Binding (JAXB) API with the Jackson XML Module and the Jakarta EE 9 API is needed for other JAX-B uses. In these cases, it&rsquo;s better to use older Java EE 8 Java archive (JAR) files instead of Jakarta EE 8 files.</li>
</ul>

<p>If you&rsquo;re an&nbsp;<a href="https://projects.eclipse.org/projects/ee4j.jersey">Eclipse Jersey</a> user, note that:</p>

<ul>
	<li>Eclipse Jersey 3 needs a Jakarta EE 9-based server, such as&nbsp;<a href="https://eclipse-ee4j.github.io/glassfish/">Eclipse GlassFish</a> 6,&nbsp;<a href="https://www.eclipse.org/jetty/">Eclipse Jetty 11</a>, or Apache Tomcat 10.</li>
	<li>Eclipse Jersey 2 needs a Java EE 8 or Jakarta EE 8-based server, such as Eclipse GlassFish 5, Eclipse Jetty 9/10, or Apache Tomcat 9.</li>
</ul>

<h2>Don&rsquo;t Mix Jakarta EE 8 and Jakarta EE 9 Artifacts</h2>

<p>It&rsquo;s not always easy to recognize Jakarta EE 9 APIs and compatible implementations. It&rsquo;s the artifact versions that matter. I&rsquo;ve listed some Jakarta EE 9 artifacts here, but there are many more, especially when you consider the Jakarta EE 9-compatible implementations from various vendors.</p>

<p>Jakarta EE 9 artifacts include:</p>

<ul>
	<li>jakarta.activation:jakarta.activation-api:2.0.0 + com.sun.activation:jakarta.activation:2.0.0</li>
	<li>jakarta.annotation:jakarta.annotation-api:2.0.0</li>
	<li>jakarta.authentication:jakarta.authentication-api:2.0.0</li>
	<li>jakarta.authorization:jakarta.authorization-api:2.0.0</li>
	<li>jakarta.batch:jakarta.batch-api:2.0.0 + com.ibm.jbatch:*:2.0.0</li>
	<li>jakarta.ejb:jakarta.ejb-api:4.0.0</li>
	<li>jakarta.enterprise:jakarta.enterprise.cdi-api:3.0.0 + org.jboss.weld.se:*:4.0.0</li>
	<li>jakarta.enterprise.concurrent:jakarta.enterprise.concurrent-api:2.0.0</li>
	<li>jakarta.el:jakarta.el-api:4.0.0</li>
	<li>jakarta.faces:jakarta.faces-api:3.0.0 + org.glassfish:jakarta.faces:3.0.0</li>
	<li>jakarta.inject:jakarta.inject-api:2.0.0</li>
	<li>jakarta.interceptor:jakarta.interceptor-api:2.0.0</li>
	<li>jakarta.jms.jakarta.jms-api:3.0.0</li>
	<li>jakarta.json:jakarta.json-api:2.0.0 + org.glassfish:jakarta.json:2.0.0</li>
	<li>jakarta.json:jakarta.json.bind-api:2.0.0 + org.eclipse:yasson:2.0.0</li>
	<li>jakarta.jws:jakarta.jws-api:3.0.0</li>
	<li>jakarta.mvc.jakarta.mvc-api:2.0.0 + org.eclipse.krazo:*:2.0.0</li>
	<li>jakarta.persistence:jakarta.persistence-api:3.0.0 + org.eclipse.persistence:*:3.0.0</li>
	<li>jakarta.security.**:*:2.0.0 + org.glassfish.soteria:*:2.0.0</li>
	<li>jakarta.servlet:jakarta.servlet-api:5.0.0</li>
	<li>jakarta.servlet.jsp:jakarta.servlet.jsp-api:3.0.0</li>
	<li>jakarta.servlet.jsp.jstl:jakarta.servlet.jsp.jstl-api:2.0.0</li>
	<li>jakarta.transaction:jakarta.transaction-api:2.0.0</li>
	<li>jakarta.validation.jakarta.validation-api:3.0.0 + org.hibernate.validator:hibernate-validator:7.0.0</li>
	<li>jakarta.websocket:jakarta.websocket-api:2.0.0 + org.glassfish.tyrus:*:2.0.0</li>
	<li>jakarta.ws.rs:jakarta.ws.rs-api:3.0.0 + org.glassfish.jersey:*:3.0.0</li>
	<li>jakarta.xml.bind:jakarta.xml.bind-api:3.0.0 + com.sun.xml.bind:*:3.0.0</li>
	<li>jakarta.xml.ws:jakarta.xml.ws-api:3.0.0 + com.sun.xml.*:*:3.0.0</li>
</ul>

<p>Note that some of the artifacts listed above had not been released yet when this article was written.</p>

<h2>JAX-RS Is Now Jakarta Restful Web Services</h2>

<p>To help distinguish between Java EE and Jakarta EE, the Java API for RESTful Web Services (JAX-RS) is now called Jakarta Restful Web Services. As a result, the acronym is no longer used in the specification document or the TCK document, and its use in the API javadoc is limited.</p>

<p>There are ongoing discussions about a new acronym or abbreviation that could be used for this technology, with the most favored suggestion being Jakarta REST. However, people continue to use the JAX-RS acronym in discussions.</p>

<p>To help avoid confusion between Java EE and Jakarta EE, the full Jakarta Restful Web Services name is preferred.</p>

<h2>Jakarta EE 9.1 Will Quickly Follow Jakarta EE 9</h2>

<p>Jakarta EE 9 is compatible with Java Development Kit (JDK) 8.</p>

<p>Jakarta EE 9.1 is planned for release very soon after Jakarta EE 9, and it will be compatible with JDK 11 as well as JDK 8.</p>

<h2>Get the Latest Updates on Jakarta EE 9 and Beyond</h2>

<p>The Jakarta EE community provides multiple information channels to keep you up-to-date on the latest happenings. To stay connected and join the conversation, click&nbsp;<a href="https://jakarta.ee/connect/">here</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/jan_supol.png" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Jan Supol works at Oracle and is a committer on&nbsp;<a href="https://projects.eclipse.org/user/10115">10 Eclipse EE4J projects</a>. He is the project lead for&nbsp;<a href="https://projects.eclipse.org/projects/ee4j.jersey">Eclipse Jersey</a>.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>