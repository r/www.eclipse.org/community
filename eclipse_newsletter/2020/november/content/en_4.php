<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2>JakartaOne Livestream Program Details Are Available</h2>

<p>The JakartaOne Livestream virtual conference on December 8 will include an interesting and informative mix of sessions that includes:</p>

<ul>
	<li>Keynote talks from Eclipse Foundation Executive Director Mike Milinkovich and Jakarta EE vendors</li>
	<li>Technical talks on Jakarta EE and Eclipse MicroProfile</li>
	<li>Discussions on Jakarta EE 10 contributions and content</li>
	<li>A demo on shortening URLs</li>
</ul>

<p>The complete list of sessions and speakers is available&nbsp;<a href="https://jakartaone.org/2020/">here</a>.</p>

<p>If you haven&rsquo;t already reserved your spot, register&nbsp;<a href="https://www.crowdcast.io/e/jakartaonelivestream_dec8/register">here</a>.</p>

<p>For live event updates, speaker announcements, news, and more, follow us on Twitter&nbsp;<a href="https://twitter.com/JakartaOneConf">@JakartaOneConf</a>.</p>

<h2>Eclipse Foundation Releases Case Study With Payara Services</h2>

<p>The first in our new series of case studies showcases the entrepreneurial value of open source software. Learn about how Payara Services benefited from flexible membership options and support services by reading the case study&nbsp;<a href="https://outreach.eclipse.foundation/payara-services-open-source-case-study">here</a>.</p>

<p>All members are welcome to participate in our case study initiative. Contact <a href="mailto:marketing@eclipse.org">marketing@eclipse.org</a> if you are interested!</p>

<h2>Check Out Our J4K Conference Virtual Booth Presentations</h2>

<p>This was the first year for the J4K Conference, a virtual event for open source and middleware communities that develop applications on Kubernetes. The Eclipse Foundation hosted a virtual booth at the event, with&nbsp;<a href="https://jakarta.ee/">Jakarta EE</a> and&nbsp;<a href="https://microprofile.io/">Eclipse MicroProfile</a> community members sharing their expertise and insight in 11 presentations.</p>

<p>The presentation replays are available&nbsp;<a href="https://www.youtube.com/playlist?list=PLutlXcN4EAwDWZRkOQo5YJEYP7XRmNo2g">here on YouTube</a>.</p>

<h2>IoT Developer Survey Results Reveal Interesting Trends</h2>

<p>More than 1,650 individuals from a broad range of industries and organizations around the globe responded to the 2020 IoT Developer Survey.</p>

<p>The results reveal a number of interesting trends that will affect all IoT and edge ecosystem players. They also help ensure the&nbsp;<a href="https://iot.eclipse.org/">Eclipse IoT Working Group</a> and the&nbsp;<a href="https://edgenative.eclipse.org/">Eclipse Edge Native Working Group</a> continue to focus on developers&rsquo; top priorities for cloud-to-edge IoT solution development.</p>

<p>This year&rsquo;s survey results showed that:</p>

<ul>
	<li>Smart agriculture is the new industry focus area, and last year&rsquo;s top industry focus, home automation, dropped significantly in ranking</li>
	<li>Artificial intelligence is the top choice for edge computing workloads, but control logic, data exchange, and sensor fusion are not far behind</li>
	<li>Open source software is very heavily used in IoT and edge solution development</li>
</ul>

<p>For insight into the top IoT developer concerns, widely used security techniques, popular programming languages, and other trends, download the complete survey results&nbsp;<a href="https://outreach.eclipse.foundation/eclipse-iot-developer-survey-2020">here</a>.</p>

<h2>Coming Soon! Jakarta EE 9 Release: December 8th</h2>

<p>The Jakarta EE 9 General Availability release targeted for December 8 is on track with the final&nbsp;<a href="https://jakarta.ee/specifications/">Jakarta EE 9 specifications</a> about to start the ballot process.</p>

<p>The ballot process follows the&nbsp;<a href="https://jakarta.ee/about/jesp/">Jakarta EE Specification Process (JESP)</a>. To release Jakarta EE 9, the&nbsp;<a href="https://jakarta.ee/committees/specification/operations/">Jakarta EE Specification Committee</a> must review all specifications to verify that one&nbsp;<a href="https://jakarta.ee/compatibility/">compatible product</a> successfully implements the Jakarta EE 9 APIs. As in Jakarta EE 8, that product is the&nbsp;<a href="https://glassfish.org/">Eclipse GlassFish application server</a>.</p>

<h2>Ivar Grimstad Is Re-Elected in the 2020 JCP Executive Committee Elections</h2>

<p>Ivar is the Jakarta EE Developer Advocate at the Eclipse Foundation. He previously participated in the Java Community Process (JCP) Program for two periods as an individual with an associate seat and as the Eclipse Foundation alternate representative in 2019. In the 2020 election, Ivar was re-elected as the Eclipse Foundation&rsquo;s primary representative.</p>

<p>The JCP is the mechanism for developing standard technical specifications for Java technology. The Eclipse Foundation has been participating in the JCP Executive Committee since 2007 to represent the interests of the open source community and independent implementations of Java specifications.</p>

<p>The ballot for the JCP Executive Committee was open for voting between November 3 and 16.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>