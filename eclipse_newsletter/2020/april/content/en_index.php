<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2020.28.04</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2020/april/images/thabang.png" />
                          <h3>Editor's Note</h3>

                          <p dir="ltr">In this month&rsquo;s newsletter, we&rsquo;re taking a closer look at some of the exciting Internet of Things (IoT), edge computing, and digital ledger technology (DLT) projects at the Eclipse Foundation and the relationships among them.</p>

                          <p dir="ltr">Before you read this month&rsquo;s spotlight articles, be sure to check out our
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/4.php">News</a> section for updates on new projects and recent releases
                          in the Eclipse community. We also highlight an Eclipse project
                          that&rsquo;s being used to model COVID-19 data and some new opportunities
                          for community members to stay connected virtually.</p>
                        <p dir="ltr">
                          As a reminder, the 2020 Jakarta EE Developer survey is open until April
                          30. Please take a few minutes to<a
                            href="https://www.surveymonkey.com/r/Y6J7X52"
                          > add your voice</a> to the Java ecosystem&rsquo;s leading technical
                          survey.
                        </p>
                        <h2 dir="ltr">Spotlight Articles</h2>
                        <p dir="ltr">Our spotlight articles focus on three Eclipse community
                          initiatives that solve real-world challenges developers face as they work
                          to securely extend IoT applications to more people and devices:</p>
                        <ul dir="ltr">
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/1.php">Eclipse IoT Packages Project</a>. Kai Hudalla describes the
                            pre-integrated, open source stack for cloud to edge applications
                            that&rsquo;s being developed by the IoT and edge communities.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/3.php">Eclipse Unified Identity and Eclipse Tangle EE Marketplaces
                            Projects</a>. Contributors from the IOTA Foundation and the Eclipse
                            Foundation explain how the proposed Unified Identity and Tangle
                            Marketplaces projects seek to foster industry adoption of the Unified
                            Identity Protocol (UIP) and enable developers to build decentralized
                            marketplaces on the IOTA network.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/2.php">Eclipse ioFog 2.0</a>. Fr&eacute;d&eacute;ric Desbiens
                            explores the role of an Edge Compute Network (ECN) and the new features
                            in the Eclipse ioFog 2.0 release.</li>
                        </ul>
                        <h2 dir="ltr">The Latest Happenings</h2>
                        <p dir="ltr">As always, be sure to check our lists so you can stay up to
                          date with the latest happenings here at the Eclipse Foundation:</p>
                        <ul dir="ltr">
                          <li><a href="#new_project_proposals">New Project Proposals</a></li>
                          <li><a href="#new_project_releases">New Project Releases</a></li>
                          <li><a href="#upcoming_events">Upcoming Eclipse Events</a></li>
                        </ul>
                        <p dir="ltr">
                          Happy Reading!<br /> Thabang Mashologu
                        </p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/april/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/1.php"><h3>A Pre-Integrated, Open Source Stack for Cloud to Edge</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/april/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/2.php"><h3>Eclipse ioFog 2.0: The Next Generation</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/april/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/3.php"><h3>Open Source Identity and Marketplace Technologies for the Digital Economy</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/april/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/april/4.php"><h3>Newsletter News</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_proposals" style="margin-bottom:10px;">New Project Proposals</h3>
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul dir="ltr">
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-lsp4mp-language-server-eclipse-microprofile-0">Eclipse LSP4MP - Language Server for Eclipse MicroProfile</a> will provide core language support capabilities (such as code complete, diagnostics, quick fixes) to enable developers to easily and quickly develop cloud-native applications using MicroProfile APIs.</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-arrowhead">Eclipse Arrowhead</a> consists of systems and services that are needed for anyone to design, implement and deploy Arrowhead-compliant System of Systems.</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-justj">Eclipse JustJ</a> provides fully-functional Java runtimes that can be redistributed by Eclipse Projects.&nbsp;</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-unified-identity">Eclipse Unified Identity</a> consists of several components including a high-level implementation of the UIP core, examples and documentation.&nbsp;</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-jifa">Eclipse Jifa</a> is a web application based on the Eclipse Memory Analyser Tooling (MAT) that provides HTTP services so that users can view the heap dump files analysis through a browser.&nbsp;</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-tangle-marketplaces">Eclipse Tangle Marketplaces</a> provides a foundation for developers to build decentralized marketplaces on the IOTA network, allowing them to prototype concepts and explore new decentralized business models.&nbsp;</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a href="/projects/project_activity.php">Read on!</a></p>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_releases" style="margin-bottom:10px;">New Project Releases</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul dir="ltr">
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.gmf-runtime">Eclipse GMF Runtime&nbsp;&nbsp;&nbsp; 1.13.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.transformer">Eclipse Transformer Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.leshan">Eclipse Leshan 1.0.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.oneofour">Eclipse OneOFour Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ecd.theia">Eclipse Theia Graduation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/science.chemclipse">Eclipse ChemClipse 0.8.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.milo">Eclipse Milo 0.4.0 Release</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0"
                        cellspacing="0"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 id="upcoming_events">Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>

                            <p>Upcoming industry events and workshops where our members will play a key role. Hope to see you there!</p>

<p dir="ltr"><a href="https://www.redhat.com/en/summit">Red Hat Summit </a><br />
Apr 28-29 | Virtual Event</p>

<p dir="ltr"><a href="https://www.ibm.com/events/think/">IBM Think</a><br />
May 5-7 | Virtual Event</p>

<p dir="ltr"><a href="https://jakartaone.org/cn4j2020/">JakartaOne Livestream CN4J</a><br />
May 12, 2020 | Virtual Event</p>

<p dir="ltr"><a href="https://www.eclipsecon.org/2020">EclipseCon 2020</a><br />
Oct 19-22, 2020 | Virtual Event</p>

<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 id="contact_us" style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->