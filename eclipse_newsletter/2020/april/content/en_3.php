<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In February of this year, the IOTA Foundation and the Eclipse Foundation launched the<a
    href="https://tangle.ee/"
  > Tangle EE Working Group</a> to accelerate the development of permissionless, enterprise-grade,
  and open source tools based on the IOTA Tangle for creating payment and data use cases.
</p>
<p>IOTA envisions a new trustless digital economy facilitated by its Distributed Ledger Technology
  (DLT), known as the Tangle. The Tangle provides an open infrastructure that overcomes many of the
  issues with traditional blockchain technology. The Tangle protocol is scalable, publicly
  available, and does not require fees to use. It also provides much faster overall throughput than
  classical blockchains. These advantages make the Tangle an ideal platform for all types of
  organizations to participate in the new digital economy.</p>
<p>
  <a
    href="https://blog.iota.org/the-iota-and-eclipse-foundations-launch-tangle-ee-working-group-to-accelerate-commercial-adoption-6c52083f5854"
  >Founding members</a> of the Tangle EE Working Group include major corporations, such as Dell
  Technologies and STMicroelectronics, as well as a number of software companies, energy industry
  players, standards bodies, and universities from across Europe. The Working Group is currently
  promoting collaboration around two projects that will enable important use cases in the digital economy:
</p>
<ul>
  <li><a href="https://projects.eclipse.org/proposals/eclipse-unified-identity">Eclipse Unified
      Identity</a></li>
  <li><a href="https://projects.eclipse.org/proposals/eclipse-tangle-marketplaces">Eclipse Tangle
      Marketplaces</a></li>
</ul>
<p>Each of these projects builds on the base Tangle protocol, which provides a free and secure way
  to exchange value and data of any kind.</p>
<h2>Eclipse Unified Identity Project: A Trust Layer for Digital Transactions</h2>
<p>Today, there is very little trust in online transactions. You have to enter your personal
  information on many websites, but there is no effective way to prove that the identity presented
  actually represents the person claiming it. There is no trust layer.</p>
<p>To create trust, the Eclipse Unified Identity project builds on the base Tangle protocol to
  enable people, organizations, and machines to prove information about themselves online. The
  Unified Identity protocol is an implementation of Self Sovereign Identity (SSI) that implements
  the W3C proposed standards for Decentralized Identifiers (DID), Verifiable Credentials, and other
  proposed decentralized identity standards (Figure 1).</p>
<p>
  <strong>Figure 1: Decentralized Identifiers and Self-Sovereign Identity</strong>
</p>
<p>
  <img src="images/4_1.png" alt="Figure 1: Decentralized Identifiers and Self-Sovereign Identity" />
</p>
<p>This trust layer can be applied to almost every type of internet transaction. For example, proof
  of identity is required between:</p>
<ul>
  <li>An individual and a business in the case of online shopping</li>
  <li>Two machines in the case of a self-driving car that automatically pays its fees at toll booths</li>
  <li>A machine and a business in the case of a refrigerator that orders groceries</li>
</ul>
<p>The ability to provide trusted identity data when needed, then immediately remove it and provide
  it again later, helps people protect the privacy of their personal information. It also helps
  organizations comply with privacy laws, such as the General Data Protection Regulation (GDPR) in
  Europe and the California Consumer Privacy Act (CCPA).</p>
<p>For an identity protocol to be effective, it must be easy for developers to use and integrate.
  For unified identity solutions to be successful, they must be easy for any entity to adopt and
  integrate, whether that entity is a retailer, bank, government agency, or other organization. The
  protocol must also be usable by many different types of devices so they can act as economic
  agents.</p>
<p>Without this widespread adoption, it is just a technology stack. The creation of the Tangle EE
  Working Group and the Unified Identity project have allowed the group to build a well-governed,
  vendor-neutral community to enhance the protocol, stimulate adoption, and encourage its ubiquitous
  use.</p>
<p>At this point, the IOTA Foundation has developed an experimental version of the Unified Identity
  protocol and expects to have a demo platform for developers to experiment with in the coming
  months. It will likely take some time to reach production-quality, but growing the community will
  help considerably.</p>
<p>We welcome all developers and organizations that are interested in contributing to the protocol
  to join the community. We&rsquo;re also very interested in hearing about organizations&rsquo;
  requirements and the types of user experience they will be looking for when they incorporate the
  technology into their processes.</p>
<h2>Eclipse Tangle Marketplaces Project: Decentralized, Peer-To-Peer Transactions</h2>
<p>Virtual marketplaces exist today, but with a centralized structure where the marketplace provider
  acts as the intermediary between those using the platform. These marketplaces are primarily
  designed to bring supply and demand together and coordinate price building. They are typically
  operated by major market players, and are almost never open to competing organizations in the
  industry they serve.</p>
<p>The Eclipse Tangle Marketplaces project is unique in that it builds on the base Tangle protocol
  to allow developers to build decentralized marketplaces where people, organizations, and machines
  can negotiate with one another for specific services and payment for those services. There is no
  central authority that assumes a superior position over other marketplace participants and manages
  fee exchanges.</p>
<p>All marketplace members are of equal rank and have equal rights. All financial transactions are
  trustless, permissionless, and peer-to-peer. They&rsquo;re also feeless, which is extremely
  important in marketplaces where large numbers of transactions are required. Even non-payments can
  be made with no need for an intermediary of any kind (Figure 2). The Eclipse Unified Identity
  protocol provides the trust layer that is needed for market participants to make financial
  arrangements and sign contracts with one another.</p>
<p>
  <strong>Figure 2: A Decentralized Marketplace Eliminates the Middleman</strong>
</p>
<p>
  <img src="images/4_2.png" alt="Figure 2: A Decentralized Marketplace Eliminates the Middleman" />
</p>
<p>There are numerous use cases for decentralized marketplaces across industries.</p>
<p>In a smart city, a streetlight that needs repair could negotiate with several repair technicians,
  choose the technician that offers the lowest price or the best economies of scale, then establish
  a contract with that person to complete the work. Or a self-driving car could negotiate requests
  for transportation and the associated payments in a decentralized Uber-type model.</p>
<p>Organizations and individuals can also use the technology to negotiate services and make
  payments. For example, a company in the energy sector might negotiate with the residents in a
  particular neighborhood to purchase their power data. Similarly, a municipality might negotiate
  with vehicles to purchase their data to better understand traffic flows.</p>
<p>There are also numerous Industry 4.0 applications. For example, in the automotive industry,
  manufacturing facilities could negotiate with each other to buy and sell the car parts needed to
  fill specific orders from car dealerships.</p>
<p>Today, there are almost no standards for developing decentralized marketplaces. That means
  innovation itself is the single biggest technical hurdle we face in advancing the technology. It
  also means developers that join the community have an opportunity to help define the de facto
  standard for decentralized marketplaces. Everyone who is interested in this challenging, but
  exciting, opportunity is welcome to join us.</p>
<h2>Expanding the Tangle: Calling All Eclipse IoT Developers</h2>
<p>
  With the relationship between the Tangle and IoT, we believe there are many opportunities to
  collaborate with the<a href="https://iot.eclipse.org/"> Eclipse IoT community</a>. We encourage
  everyone involved in Eclipse IoT projects to learn more about how the Unified Identity and Tangle
  Marketplaces projects can bring identity verification, negotiation, and payment capabilities to
  their applications.
</p>
<p>Once the Unified Identity and Tangle Marketplaces projects are more mature, we also see the potential to create
  new Tangle EE projects at the Eclipse Foundation, for example, to focus on DLT solutions for
  specific industries or applications.</p>
<ul>
  <li>For additional information about our goals, visit the<a href="https://tangle.ee/"> Tangle EE
      Working Group website</a>.
  </li>
  <li>For IOTA documentation, visit<a href="https://docs.iota.org/"> docs.iota.org</a>.
  </li>
  <li>For an introduction to the world of digital identity,<a
    href="https://files.iota.org/comms/IOTA_The_Case_for_a_Unified_Identity.pdf"
  > read the white paper</a>.
  </li>
  <li>For Tangle Marketplaces project documentation and a demo, visit<a
    href="https://industry.iota.org/"
  > industry.iota.org</a>.
  </li>
  <li>Contact us directly at <a href="mailto:tangle-ee@iota.org">tangle-ee@iota.org</a>.</li>
</ul>
<p>*This article was written with contributions from Jelle Millenaar, Jan Pauseback, Alexander
  Belyaev, Alex Westerhof, Charlie Varley, Daniel Yvetot, Cara Harbor, and Thabang Mashologu.</p>
