<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>The Eclipse STEM Project Supports COVID-19 Modeling</h2>
<p>
  The<a href="/stem/"> Eclipse Spatio-Temporal Epidemiological Modeler (STEM)</a>
  is helping scientists and public health officials rapidly model data related to the COVID-19
  pandemic. The <a href="https://wiki.eclipse.org/2019_novel_Corona_Virus_Model">COVID-19 model</a>
  illustrates the expected spread of the pandemic, as well as the effects of interventions, such as
  quarantines, social distancing, and travel restrictions, on the spread of the disease.
</p>
<p>The modeling tool was developed by IBM Research using the Eclipse Equinox implementation of the
  Open Services Gateway initiative (OSGi) framework specification, and has been used to model the
  spread of infectious diseases around the world since about 2006. The structure of the COVID-19
  model was suggested by Dr. Brian M. Gurbaxani, a researcher at the Centers for Disease Control and
  Prevention (CDC) in the United States.</p>
<p>James Kaufman, a STEM project lead and one of the original IBM Research team members on the
  project, says the tool&rsquo;s modular architecture is key to enabling users to quickly build
  models and consider the effect of various interventions based on global data.</p>
<p>&ldquo;There&rsquo;s a graphical framework for drawing the model with an equation editor that any
  epidemiologist would understand. A single button click generates the Java code in the Eclipse
  plug-ins that define and solve the differential equations. You can drag-and-drop models, export
  them, and share them. You can also create new plugins for the tool,&rdquo; Kaufman explains.</p>
<p>&ldquo;In the case of COVID-19, people want to find out how different interventions will flatten
  the curve so hospitals are not overwhelmed with COVID-19 patients. They also want to determine the
  effects of removing those interventions in ways to help avoid a second wave of infection,&rdquo;
  he says.</p>
<p>While it&rsquo;s impossible to know how many people are using the Eclipse STEM tool for COVID-19
  modeling, Kaufman notes the team has been contacted about the topic by researchers in Korea and
  the Philippines.</p>
<p>Simone Bianco, who leads the cellular engineering lab at IBM Research, adds that the speed at
  which the STEM generates models is particularly critical with fast-moving infectious diseases,
  such as COVID-19.</p>
<p>&ldquo;Before STEM, modeling was very painful,&rdquo; Bianco says. &ldquo;You&rsquo;re limited by
  how fast you can reprogram your own software. It can take a week, even for a good programmer to
  write and debug new software that simulates a new model. In a pandemic, that&rsquo;s not fast
  enough. A tool like this one is absolutely paramount for public health. For me, it really was a
  game changer.&rdquo;</p>
<p>Kaufman notes that people who are familiar with the tool can generate and compare three
  epidemiological models that use similar parameters in about 10 minutes.</p>
<p>&ldquo;We know a lot about the mathematics and dynamics of infectious disease, and there is a
  huge amount of data available globally,&rdquo; says Kaufman. &ldquo;It&rsquo;s extremely important
  that we make policy decisions based on data, and avoid decisions that failed in past pandemics. It
  was wonderful that IBM decided to contribute this tool to the Eclipse Foundation and I encourage
  people to share data, share models, and help each other. This is vital if we want to respond
  quickly to pandemics, such as COVID-19. And it&rsquo;s why the project is at the Eclipse
  Foundation.&rdquo;</p>
<p>For more information about the Eclipse STEM project, how to join the community, and download the
  latest build, visit the STEM:</p>
<ul>
  <li><a href="/stem/">Website</a></li>
  <li><a href="https://wiki.eclipse.org/STEM">Wiki page</a></li>
</ul>
<hr>
<h2>Virtual Connection Opportunities During COVID-19</h2>
<p>We hope members of our community are continuing to stay safe and healthy as we face unique
  challenges related to COVID-19. The Eclipse Foundation staff is fortunate to be able to work
  remotely, so we can continue to support our community through this difficult time. We are still a
  community and will continue to connect and work virtually.</p>
<p>
  Although the situation we&rsquo;re experiencing is very challenging, it does offer an opportunity
  to improve how we connect and collaborate virtually. Of course,<a
    href="https://accounts.eclipse.org/mailing-list"
  > mailing lists</a>,<a href="https://wiki.eclipse.org/Main_Page"> wikis</a>, and other public and
  archived means of communications will continue to be the default and preference, as they are key
  to transparency. But, we invite you to boost the online activities of your project or community of
  projects. There are a few ways project teams can do this:
</p>
<ul>
  <li>Consider having an office hour session with committers.</li>
  <li>Schedule &ldquo;ask me anything&rdquo; sessions.</li>
  <li>Book a town hall meeting.</li>
  <li>Plan a virtual democamp or an online meetup.</li>
  <li>Set up a public meeting for your Eclipse top level project.</li>
</ul>
<p>Our Foundation staff is committed to helping community members communicate and collaborate
  effectively to drive the success and adoption of Eclipse projects and technologies.</p>
<p>If you are hosting an online event relevant to the Eclipse community, we would be happy to share
  the details through this newsletter, or through our social media channels. Just send an email with
  the event details to events@eclipse.org.</p>
<p>If you have community-relevant content, news, or announcements, please contact us at
  news@eclipse.org and we can share and promote your updates through our channels.</p>
<hr>
<h2>Eclipse Theia 1.0 Is Released</h2>
<p>Eclipse Theia is a true open source alternative to Microsoft Visual Studio (VS) Code software.
  Developers and vendors can build, modify, extend, and distribute cloud and desktop integrated
  development environments (IDEs) that look as great as those developed in VS Code while taking full
  advantage of the VS Code extension ecosystem.</p>
<p>Theia is designed from the ground up to run in desktop environments as well as in browser and
  remote server environments. IDE developers can write the source code for their development
  environment once, then build a desktop IDE, a cloud IDE, or both, without rewriting any code.</p>
<p>
  Theia uses the<a href="https://projects.eclipse.org/projects/ecd.openvsx"> Eclipse Open VSX</a>
  marketplace, an open-source alternative to the Microsoft VS Code Extension Marketplace. The
  extensions in the VS Code Marketplace can only be used by products developed using Visual Studio
  products. The extensions in the Open VSX marketplace can be used by anyone, whether they&rsquo;re
  developing in Theia or VS Code.
</p>
<p>
  Many big-name companies already rely on Theia as the foundational building block for their IDEs.
  It&rsquo;s used in Google Cloud Shell and Arduino&rsquo;s new Pro IDE. SAP, Arm, and IBM have also
  built highly customized IDEs on top of the Theia platform. Theia has also replaced the IDE in<a
    href="/che/"
  > Eclipse Che</a> and is driving<a
    href="https://www.gitpod.io/blog/continuous-dev-environment-in-devops/"
  > Gitpod&rsquo;s continuous development environments</a>.
</p>
<p>
  To get started with Theia, visit the<a href="https://theia-ide.org/"> website</a> today.
</p>
<hr>
<h2>The Jakarta EE Developer Survey Is Open Until April 30</h2>
<p>
  Please take a few minutes (less than 10!) to add your voice to the 2020<a
    href="https://www.surveymonkey.com/r/Y6J7X52"
  > Jakarta EE Developer Survey</a>.
</p>
<p>This year, we&rsquo;re asking you to tell us more about your next steps for Java and cloud native
  development as well as your choices for architectures, approaches, and tools as cloud native
  technologies for Java mature.</p>
<p>With this updated information, everyone in the Java ecosystem will have a better understanding of
  how the cloud native world for enterprise Java is unfolding and what that means for their
  strategies and businesses. And the Jakarta EE community will have a better understanding of
  developers&rsquo; top priorities for future Jakarta EE releases.</p>
<p>We encourage everyone to complete the survey!</p>
<hr>
<h2>Eclipse Foundation Supports SmartCLIDE European Research Project</h2>
<p>The SmartCLIDE European research project team is partnering with the Eclipse Foundation to build
  a vibrant ecosystem that will allow software developers and businesses to access, use, and enhance
  the project&rsquo;s research results.</p>
<p>SmartCLIDE (Cloud, Deep-Learning, IDE, Discovery and Programming-by-Example) is a
  4.9-million-euro research project funded by the European Union&rsquo;s Horizon 2020 research and
  innovation program. It was created in January 2020 under the leadership of the ATB Institute for
  Applied Systems Technology in Bremen, Germany, and includes 11 partners from Germany, Greece,
  Luxembourg, Portugal, Spain, and the United Kingdom.</p>
<p>Together, this international team of researchers, academics, software developers, and security by
  design experts will create a smart, cloud native development environment based on the
  coding-by-demonstration principle. The new development environment will make it faster and easier
  for small- and medium-sized enterprises and public sector organizations to take advantage of cloud
  and big data technologies.</p>
<p>For more information:</p>
<ul>
  <li><a href="https://smartclide.eu/">Visit the SmartCLIDE website</a></li>
  <li><a href="/org/press-release/20200326-smartclide.php">Read the press
      release</a></li>
  <li><a href="/org/research/">Check out other European research projects at
      the Eclipse Foundation</a></li>
</ul>
<hr>
<h2>Introducing Eclipse zenoh for Edge Computing</h2>
<p>Eclipse zenoh is a new edge computing project that recently entered the incubation phase at the
  Eclipse Foundation.</p>
<p>The zenoh protocol provides a stack that unifies data in motion, data in use, and data at rest.
  It carefully blends the proven publish/subscribe paradigm with geo-distributed storage, queries,
  and computations. The zenoh software is designed to minimize network overhead and can support
  extremely constrained devices while ensuring low latency and high throughput.</p>
<ul>
  <li><a href="http://zenoh.io/">Visit the zenoh website</a></li>
  <li><a href="https://projects.eclipse.org/projects/iot.zenoh">Learn more about the project</a></li>
</ul>
