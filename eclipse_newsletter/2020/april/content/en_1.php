<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In the October 2019 Eclipse Foundation newsletter, Jens Reimann and I explored<a
    href="/community/eclipse_newsletter/2019/october/why_eclipse_iot.php"
  > the need for Eclipse IoT packages</a> and<a
    href="/community/eclipse_newsletter/2019/october/a_close_look.php"
  > introduced the Eclipse IoT Packages project</a>. Since then, the IoT Packages project has been
  expanded to include edge technology and we&rsquo;re now working to add the<a
    href="https://iofog.org/"
  > Eclipse ioFog</a> project to the package.
</p>
<p>The resulting package will give developers a pre-integrated, open source, cloud-to-edge
  stack to address edge use cases. If you&rsquo;re familiar with Microsoft&rsquo;s Azure IoT Edge,
  we&rsquo;re working to solve the same issues, but with open source software.</p>
<p>
  Before I get to the benefits and use cases of the<a
    href="/packages/packages/cloud2edge/"
  > Cloud2Edge package</a>, here&rsquo;s a quick summary of the four projects it will encompass:
</p>
<ul>
  <li><a href="/hono/">Eclipse Hono</a> for device connectivity</li>
  <li><a href="/ditto/">Eclipse Ditto</a> for managing and organizing digital
    representations of physical objects &mdash;digital twins</li>
  <li><a href="/hawkbit/">Eclipse hawkBit</a> for rolling out and managing
    software updates to IoT devices</li>
  <li><a href="https://iofog.org/">Eclipse ioFog</a> for building and running applications
    at the edge at enterprise scale
  </li>
</ul>
<p>Developers can install a single, off-the-shelf package and immediately start using the
  technologies within it without the time and effort required to manually integrate them.</p>
<h2>Inserting an Edge Node in IoT Applications Increases Efficiency</h2>
<p>With a classic IoT application, the devices at the edge of the network are directly
  connected to a cloud backend. But, in many applications, this is not the optimal approach.</p>
<p>For example, if application logic is developed in a cloud environment and deployed in
  the cloud, you can run into situations where you&rsquo;re transferring large volumes of data from
  edge devices to the cloud, but most of the data is filtered out when it&rsquo;s processed in the
  cloud. You&rsquo;re using valuable bandwidth for data that&rsquo;s not needed. In applications,
  such as data analytics, where edge devices are producing data at very high sampling rates, you
  can&rsquo;t afford to transmit all of that data over the internet to the cloud.</p>
<p>The Cloud2Edge package inserts an edge node in between the cloud backend and the edge
  devices. Now, the edge devices connect to the edge node and the edge node connects to the cloud
  backend.</p>

<h2>Shifting Workloads for Maximum Flexibility</h2>
<p>The business logic is implemented in the cloud using containers and orchestrated using
  Kubernetes, as is typical in IoT applications. However, the business logic is also deployed on the
  edge node in the form of a container image. This approach gives developers the flexibility to move
  workloads from the cloud to the edge node that&rsquo;s closer to the devices.</p>
<p>The edge node processes, filters, and aggregates the data from the edge devices so only
  relevant data is sent to the cloud. This is possible thanks to the trend toward ever-increasing
  computing resources at the edge. It used to be that only minimal computing resources were located
  close to the edge device, but today, we&rsquo;re more likely to see rack-mounted server systems
  with plenty of compute power and RAM.</p>
<p>Shifting workloads closer to edge devices reduces:</p>
<ul>
  <li>Bandwidth consumption because less data is transmitted to the cloud backend</li>
  <li>Backend processing power requirements because data processing occurs on the edge node</li>
  <li>Latency because the edge node is closer to the edge devices</li>
</ul>
<p>Because the edge node is already integrated into the overall system, developers can
  redeploy the workload with a simple system configuration change. In the past, this flexibility
  wasn&rsquo;t possible because development and deployment models for the cloud backend and the
  gateway were completely separate.</p>
<h2>Delivering Software Updates to the Edge</h2>
<p>In businesses with many distributed franchises or branch locations, the ability to take
  business logic closer to the network edge enables more efficient and cost-effective operations.</p>
<p>In the past, the organization responsible for IoT operations had to send software
  updates, or an IT person, to each point of sale location so the software could be updated locally.</p>
<p>With integrated cloud and edge technologies, the computers at each branch location are
  considered the edge nodes in the overall cluster that&rsquo;s managed from the cloud. This allows
  the IoT operations organization to roll out and manage the software updates to each point of sale
  location from the cloud. The IoT operations organization can ensure all locations migrate to the
  updated software at the right time with no need to send IT personnel to each site.</p>
<p>As manufacturers continue to evolve their global operations, they may also be able to
  leverage the edge paradigm to roll out software updates to the furthest reaches of their
  organization from a central system. This capability will help manufacturers ensure every facility
  is running the same software and applying the same monitoring processes to meet manufacturing
  consistency and corporate compliance requirements.</p>
<h2>IoT and Edge Are Complementary and Evolutionary</h2>
<p>Moving workloads from the cloud to the edge is important no matter which way you
  approach IoT and edge.</p>
<p>Some people believe edge computing originates at the field devices and the goal is to
  extract the business logic from the device onto a gateway that&rsquo;s close to the device. In
  this case, the technologies are complementary.</p>
<p>Others believe edge computing originates with the business logic in the cloud and
  extends out to the devices at the edge. This is the evolutionary viewpoint.</p>
<p>With the Eclipse Foundation&rsquo;s approach to integrating IoT and edge, developers
  can approach applications from any perspective. They can run business logic in the cloud backend.
  At the same time, they can integrate and manage edge nodes to bring workloads to them using the
  same management tools they use in the cloud.</p>
<p>Developers have a de-facto standard, open source stack that allows them to move cloud
  workloads wherever needed, whenever needed. They can customize the stack for their unique
  requirements and deploy it anywhere from a public or private cloud to a private data center or
  even their local laptop.</p>
<h2>Get Involved in Our IoT and Edge Working Groups</h2>
<p>We&rsquo;re in the early stages of experimenting with the Cloud2Edge package, but we
  see a lot of potential for it and we&rsquo;re excited about the opportunities it enables.</p>
<p>We invite everyone with an interest in IoT and edge applications to get involved with
  advancing the projects at the Eclipse Foundation, even if you&rsquo;re not ready for integration.</p>
<ul>
  <li><a href="https://iot.eclipse.org/">Eclipse IoT</a> provides access to more than 40 open source
    IoT projects and a community that includes leading organizations from a variety of industries.</li>
  <li>The recently created<a href="https://edgenative.eclipse.org/"> Edge Native Working Group</a>
    delivers open source edge computing code that&rsquo;s already widely deployed in production
    environments.
  </li>
</ul>
<p>The more we collaborate, the easier it will be for everyone to take advantage of the
  innovative technologies in these two rapidly evolving groups at the Eclipse Foundation.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/april/images/kai.jpg" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>