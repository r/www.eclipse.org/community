<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Later this quarter, the<a href="https://iofog.org"> Eclipse ioFog</a> team will release version
  2.0 of the platform. To understand the value of the new features in the release, it is important
  to first understand some of the key concepts in ioFog.
</p>
<p>The Edge Compute Network (ECN) is the central pillar of ioFog. The ECN is comprised of
  edge devices that operate outside the data center, at the edge of the enterprise network,
  performing compute, storage, and networking services locally. The ECN provides resilience, fault
  tolerance, security, and low-latency connections between edge devices which, in turn, deliver the
  scalability necessary for large-scale edge computing deployments.</p>
<p>The core component of each ECN is the Controller.&nbsp; It orchestratesItorchestrates
  the tasks of all other components. The Controller is supported by instances of the ioFog Agent,
  which report directly to it. Agents are responsible for infrastructure tasks such as running edge
  microservices, mounting volumes, and managing resources.</p>
<p>The most important change in this release of ioFog is the replacement of the ioFog
  Connector with a trio composed of the:</p>
<ul>
  <li>ioFog Router</li>
  <li>ioFog Proxy</li>
  <li>ioFog Port Manager</li>
</ul>
<p>This overhaul of the ECN adds tremendous flexibility to ioFog and illustrates how open
  source is a catalyst for innovation.</p>
<h2>ioFog Router</h2>
<p>
  The ioFog Router is based on<a href="https://qpid.apache.org/"> Apache Qpid</a>. It enables
  microservices to communicate with each other and tunnels traffic on public ports to these
  microservices. By default, each Controller and Agent run their own instance of the Router.
</p>
<p>Although the Controller and Agents communicate over HTTP, Router instances rely on the
  Advanced Message Queuing Protocol (AMQP). AMQP, an OASIS standard, was designed with security,
  reliability and interoperability in mind.</p>
<h2>ioFog Proxy</h2>
<p>The Proxy microservice is an internal component on the ECN. Its purpose is to translate
  HTTP requests to AMQP where necessary. When exposing a microservice, instances of the Proxy are
  deployed and linked to the router instances involved.</p>
<p>
  The ioFog Proxy is based on<a href="https://skupper.io"> project Skupper</a> from Red Hat. Skupper
  is a Layer 7 service interconnect. Its main use case is to enable secure communications across
  Kubernetes clusters with no virtual private networks (VPNs) or special firewall rules. Because
  ioFog leverages Skupper, ECNs can span multiple cloud providers, data centers, and regions.
</p>
<p>Figure 1 illustrates the relationship between the core components of the ECN in ioFog
  2.0.</p>
<p>
  <strong>Figure 1: ioFog 2.0 Core Components</strong>
</p>
<p>
  <img src="images/2_1.png"/>
</p>

<h2>ioFog Port Manager</h2>

<p>The ioFog Port Manager is only required when the platform is integrated into a
  Kubernetes cluster. The Port Manager is responsible for deploying proxies on the Kubernetes
  cluster as needed when exposing microservices on external public ports.</p>
<h2>Additional Improvements in ioFog 2.0</h2>
<p>In addition to the overhaul of the ECN, the specification of the Controller to Agent
  API has been stabilized in ioFog 2.0. This means ioFog community members can now create custom
  implementations of the ioFog Agent to fit specific use cases. For example, they can write an
  Android-native agent to drive microservices on a mobile device.</p>
<p>Several smaller features have also been added to the ioFog Engine, including:</p>
<ul>
  <li>Better Docker image pruning</li>
  <li>Registry management</li>
  <li>Better centralized logging support</li>
</ul>
<h2>ioFog 2.0 Is Just the Beginning</h2>
<p>The roadmap for the ioFog 2.x series of releases is equally impressive.</p>
<p>The ioFog team intends to further improve the ioFog ECN by adding support for peering
  optimization, cost mechanisms, and dynamic routing, among others.</p>
<p>The team will also implement a new container tagging format that provides detailed
  descriptions of the edge nodes&rsquo; hardware capabilities, including the CPU architecture, type
  of onboard GPUs, and the presence of onboard hardware accelerators. This capability will be
  augmented by a new build system that will automatically produce optimized versions of the
  containers hosting the microservices for all relevant hardware permutations.</p>
<p>
  With these kinds of features in ioFog, the<a href="https://edgenative.eclipse.org/"> Eclipse Edge
    Native Working Group</a> will be one step closer to fulfilling its vision for edge devops
  &mdash; edgeops. The goal of edgeops is to enable people to use their existing skills and devops
  tools, write in their preferred languages, reuse their existing data center and cloud
  applications, and make it all work seamlessly with the edge.
</p>
<h2>Get Involved in ioFog</h2>
<p>
  To connect with other ioFog enthusiasts and learn more about the platform, visit our<a
    href="https://iofog.org/community.html/"
  > community page</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/april/images/frederic.jpg" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>