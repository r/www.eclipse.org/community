<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
$App = new App ();
$path = $App->getWWWPrefix();
$path_to_index = $path . preg_replace('/index-mailchimp.php/u', "index.php", $_SERVER['REQUEST_URI']);
include '_variables.php';

// Override default variables for mailchip
$email_body_width = "600px";
$col_1_img = "540px";
$col_2_table = "260";
$col_2_img = "260px";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width" />
<title><?php print $pageTitle; ?></title>

<?php include '../../_includes/_index-css.php'; ?>

<!--
    Outlook Conditional CSS
      These two style blocks target Outlook 2007 & 2010 specifically, forcing
      columns into a single vertical stack as on mobile clients. This is
      primarily done to avoid the 'page break bug' and is optional.
      More information here:
http://templates.mailchimp.com/development/css/outlook-conditional-css
-->
<!--[if mso 12]>
    <style type="text/css">
      .flexibleContainer{display:block !important; width:100% !important;}
    </style>
<![endif]-->
<!--[if mso 14]>
    <style type="text/css">
      .flexibleContainer{display:block !important; width:100% !important;}
    </style>
<![endif]-->

</head>
<body>
  <center>
  <table border="0" cellpadding="0" cellspacing="0" height="100%"
    width="100%" id="bodyTable">
    <tr>
      <td align="center" valign="top" id="bodyCell">

        <!-- EMAIL CONTAINER -->
        <?php include 'content/en_index.php'; ?>

      </td>
    </tr>
  </table>
  </center>
</body>
</html>