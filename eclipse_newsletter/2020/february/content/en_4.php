<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  My latest YouTube series,<a
    href="https://www.youtube.com/playlist?list=PLFjB4VDnlT_1UH_Ncopre4nhCRNtBxohX"
  > Jakarta EE Quickstart Guides</a>, provides short, 5- to 15-minute, easy-to-understand tutorials
  for common Jakarta EE tasks. The videos target Jakarta EE newcomers as well as experienced
  developers.
</p>
<p>I use all Jakarta EE-compatible application servers in the tutorials to vary scenarios
  and fulfill everyone&rsquo;s needs. I&rsquo;m also uploading the source code for the examples on
  GitHub.</p>
<p>Here&rsquo;s a look at the playlist.</p>
<h2>Jakarta EE Project Setup</h2>
<ul>
  <li><a href="https://youtu.be/aiDcgEZY21I">Bootstrapping your Jakarta EE 8 Project in Less Than
      One Minute</a></li>
  <li>Planned: Bootstrapping a Jakarta EE 8 JSF Project With PrimeFaces</li>
</ul>
<h2>Jakarta EE Tooling</h2>
<ul>
  <li><a href="https://youtu.be/oZVVTO5m4ss">Liberty Maven Plugin for Improved Jakarta EE Developer
      Experience</a></li>
  <li><a href="https://youtu.be/-2JbA7JlO6w">Deploying and Debugging Your Jakarta EE Application
      With Payara and IntelliJ</a></li>
  <li>&middot;<a href="https://www.youtube.com/watch?v=l4uAJlvb9IY">Deploying and Debugging Your
      Jakarta EE Application With WildFly and IntelliJ</a></li>
</ul>
<h2>Resource Connections</h2>
<ul>
  <li><a href="https://youtu.be/b2rtkYKoshA">Configure a JDBC DataSource (PostgreSQL) for Open
      Liberty</a></li>
  <li><a href="https://youtu.be/XhIX_tQgnng">Jakarta Messaging Introduction With Open Liberty</a></li>
</ul>
<h2>Testing a Jakarta EE Project</h2>
<p>Jakarta EE Integration Tests with MicroShed Testing:</p>
<ul>
  <li><a href="https://youtu.be/T9G7BBbBszs">Part I: Introduction</a></li>
  <li>Part II: Planned</li>
</ul>
<h2>Development</h2>
<p>Jakarta EE CRUD API tutorial:</p>
<ul>
  <li><a href="https://youtu.be/4-9CS_S82IA">Part I: Introduction</a></li>
  <li>&middot;<a href="https://www.youtube.com/watch?v=QfIAg2Smyzg">Part II: Create API</a></li>
  <li>&middot;<a href="https://www.youtube.com/watch?v=iT7G7yvE1Zw">Part III: Read API</a></li>
  <li>&middot;<a href="https://www.youtube.com/watch?v=ZMpF2AY6YFU">Part IV: Update API</a></li>
  <li><a href="https://www.youtube.com/watch?v=IJCj7N_uNF0">Part V: Delete API</a></li>
</ul>
<h2>Topics to Watch for</h2>
<ul>
  <li>Integration with Eclipse MicroProfile</li>
  <li>Deployment to Kubernetes</li>
  <li>Best Practices</li>
</ul>
<h2>&nbsp;Additional Jakarta EE Resources</h2>
<ul>
  <li><a href="https://jakarta.ee/">Jakarta EE homepage</a></li>
  <li><a href="https://jakarta.ee/compatibility/">All compatible Jakarta EE application servers</a></li>
</ul>
<h2>Looking for a Particular Jakarta EE Guide?</h2>
<p>
  I&rsquo;m happy to create a quick-start guide for your topic. To let me know what you need, either
  add a comment to an existing video on <a
    href="https://www.youtube.com/channel/UCEbZTcePN9QwMr1Lr1SED0Q"
  >YouTube</a> or create an <a
    href="https://github.com/rieckpil/quickstart-jakarta-ee-guides/issues"
  >issue</a> on the corresponding GitHub repository.
</p>
<p>
  For more insight into using Jakarta EE, take a look at all of the<a
    href="https://rieckpil.de/category/jakarta-ee/"
  > Jakarta EE tutorials</a> on my blog.
</p>
<p>Have fun with these Jakarta EE tutorials!</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/february/images/philip.jpg" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>