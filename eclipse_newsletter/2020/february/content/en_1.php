<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>This article is about the technical possibilities that Open Liberty provides and why I&rsquo;m
  very confident you can smoothly migrate a Java EE application to a cloud native application.</p>
<p>I won&rsquo;t be talking about splitting monoliths into microservices, domain-driven design, or
  event storming. I&rsquo;m a senior consultant for cloud native topics and have worked on Java EE
  applications with servers such as IBM&rsquo;s WebSphere Application Server (WAS) and Red
  Hat&rsquo;s JBoss Enterprise Application Platform (EAP). I advise companies to move to cloud
  technologies and to pick the right tools for their environment.</p>
<p>I recently had the task of modernizing an application. It was a Java EE 6 app running on WAS v8.
  The app itself is a kind of dispatcher. It has a Simple Object Access Protocol (SOAP) endpoint to
  receive standardized XML messages which it forwards to the appropriate service. When a second
  version of the XML standard was added, it was easier to split the app into multiple services.</p>
<p>There were two services at the front &mdash; one for each version &mdash; that parsed the
  message. The core service made some transformations and distributed the message to the business
  services (Figure 1).</p>
<p>
  <strong>Figure 1: Java EE 6 App Split Into Services for Each Version of the XML Standard</strong>
</p>
<p>
  <img src="/community/eclipse_newsletter/2020/february/images/1_1.png"/>
</p>
<p>As a first step, it&rsquo;s always a good idea to eliminate cumbersome traditional application
  servers. They&rsquo;re stable, high-performance environments, but they need a lot of time to start
  and are hard to configure.</p>
<p>If you&rsquo;re using traditional WAS, move to Open Liberty or WebSphere Liberty, which is built
  on Open Liberty. With some limitations (remote Enterprise JavaBeans (EJBs), for example) it should
  be possible to easily run your app on Liberty without major adjustments. In fact, some of our
  clients are still using WAS. As developers, we use Liberty to build our apps locally, while in
  other environments, we use the client&rsquo;s setup.</p>
<p>
  Liberty enables you to add features from Java EE or<a href="https://jakarta.ee/"> Jakarta EE</a>
  alongside features from<a href="https://microprofile.io/"> Eclipse MicroProfile</a>. Figure 2
  shows the MicroProfile features:
</p>
<ul>
  <li>Blue blocks are known good features from Java EE.</li>
  <li>Green blocks are the ones we used in our migrated microservices. I explain why they are useful
    later in this article.</li>
  <li>White blocks are other MicroProfile features that are worth looking into but weren&rsquo;t
    used in this app.</li>
</ul>
<p>
  <strong>Figure 2: MicroProfile Features</strong>
</p>
<p>
  <img src="/community/eclipse_newsletter/2020/february/images/1_2.png"/>
</p>
<h2>Resiliency in Microservices With MicroProfile and Open Liberty</h2>
<p>When you create microservices, they communicate with each other. Sometimes you have to use
  synchronous RESTful calls as we did from the standard services to the core service. In this
  situation, use resilience in your microservices. If you don&rsquo;t, a broken application can
  break every app in the calling chain, especially when response times are long.</p>
<p>
  With Eclipse MicroProfile, you can add resilience to your infrastructure for example, by defining<a
    href="https://openliberty.io/guides/retry-timeout.html"
  > timeouts</a> in your REST client and by adding annotations to your app code to use<a
    href="https://openliberty.io/guides/circuit-breaker.html#interactive-circuit-breaker-playground"
  > circuitbreaker</a> architectures.
</p>
<p>
  If you disagree and say that resilience is the responsibility of a service mesh, you&rsquo;re
  right. But, you might not have a service mesh installed in your infrastructure. When you do
  install a service mesh, I&rsquo;ve got good news for you:<a
    href="/community/eclipse_newsletter/2018/september/MicroProfile_istio.php"
  > Open Liberty and Eclipse MicroProfile integrate well with Istio</a> and recognize the
  configurations from the mesh.
</p>
<p>
  If there are configurations on the app and the service mesh, the service mesh overrules the app.
  But, the service mesh cannot provide<a
    href="https://github.com/OpenLiberty/guide-microprofile-fallback"
  > fallback</a> resilience. This is because you need business logic to react to errors with
  solutions (fallbacks) such as caching, returning default values, and throwing errors with
  descriptive error messages.
</p>
<h2>Building Twelve-Factor Apps With Eclipse MicroProfile and Open Liberty</h2>
<p>
  If you want to develop a cloud native application, it&rsquo;s a good idea to look into the<a
    href="https://12factor.net/"
  > twelve-factor app</a> paradigm, which provides guidelines on how to build a Software as a
  Service (SaaS) application. Emily provides<a
    href="https://openliberty.io/blog/2019/09/05/12-factor-microprofile-kubernetes.html"
  > a good overview</a>.
</p>
<p>It&rsquo;s important to prepare your application for cloud platforms and their requirements. I
  consider &quot;lift-and-shift&quot; to be an anti-pattern. Lift-and-shift just means you run your
  application on a different virtual machine. But it&rsquo;s difficult, if not impossible, to
  benefit from the advantages of elastic scaling, self-healing, and zero-downtime deployments. A
  better approach is to use the strangler pattern, in which you create a new system around the old
  system, gradually letting the new system grow over a few years until the old system is
  &quot;strangled.&quot;</p>
<h2>Providing Observability With MicroProfile Metrics in Open Liberty</h2>
<p>
  MicroProfile provides features, such as<a
    href="https://openliberty.io/guides/microprofile-metrics.html"
  > MicroProfile Metrics</a>, that you really should look into when developing cloud native apps.
  When you have multiple, maybe hundreds or thousands of apps running simultaneously, you have to
  monitor them. Metrics help you do this.
</p>
<p>
  The applications provide monitoring systems such as<a href="https://prometheus.io/"> Prometheus</a>
  (and<a href="https://grafana.com/"> Grafana</a> for the UI) with information you can analyze and
  add alerts to. You can probably get metrics such as RAM or CPU usage, or the number of restarts,
  from the cloud platform (Kubernetes does this).
</p>
<p>Furthermore, you can create business-driven metrics such as &quot;how many orders are placed in
  my shop&quot; or &quot;how many logins are made in this time&quot; and so on. Be creative! You
  gain new possibilities with this feature. In our case, we added metrics to identify which standard
  version was called with which metadata. That helped us migrate the clients and business services
  to the new version.</p>
<h2>Tracing Microservices With MicroProfile OpenTracing and Open Liberty</h2>
<p>Tracing is another important topic. When you create a microservice infrastructure, you end up
  managing services that communicate synchronously and asynchronously. Either way, it&rsquo;s
  important to be able to follow the path of communications, especially when diagnosing bugs.</p>
<p>
  Imagine you have to find the bug in a calling chain with 10 apps. Maybe the 10 apps were
  implemented by different teams. This is very hard with no further support. You can handle this by
  passing a trace ID to the message and sending the information to services such as<a
    href="https://www.jaegertracing.io/"
  > Jaeger</a> or<a href="https://zipkin.io/"> Zipkin</a>.
</p>
<p>
  MicroProfile provides support through a<a
    href="https://github.com/OpenLiberty/guide-microprofile-opentracing"
  > MicroProfile OpenTracing</a> implementation. Ideally, you trace the call the whole way. In our
  case, we trace the call from the standard service to the core service as well as from the client
  calling the standard service to the services called by the core service.
</p>
<h2>Checking Microservice Health With MicroProfile Health Check and Open Liberty</h2>
<p>Last, but not least, Platform as a Service (PaaS) cloud platforms such as Kubernetes provide
  self-healing.</p>
<p>Kubernetes checks for &quot;liveness&quot; and, if your application is in an unstable state,
  reboots the app for you. Kubernetes also checks for &quot;readiness&quot; before adding your app
  to the load balancer so it can receive traffic.</p>
<p>However, Kubernetes defines a pod as ready to receive work when the container is started, not
  when the server with the app is started and ready. This is very important: imagine scaling your
  app so that a new instance is created and started. If Kubernetes doesn&rsquo;t check the readiness
  of the app itself, the first requests will fail because the server and the app aren&rsquo;t
  already started. You can help Kubernetes by giving it endpoints in your app to provide information
  about your app&rsquo;s readiness.</p>
<h2>Convert Your Java EE Apps to Cloud Native Apps on Open Liberty</h2>
<p>Overall, Open Liberty is a good choice for stepwise conversion of your Java EE applications to
  the cloud native paradigm. You can add one feature at a time while you refactor your app to comply
  with the 12 factors and remove older technologies such as remote EJBs.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/february/images/michael.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>