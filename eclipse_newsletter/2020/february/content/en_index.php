<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2020.20.02</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2020/february/images/shabnam.png" />
                          <h3>Editor's Note</h3>
                        <p>
                          With the<a
                            href="https://blogs.eclipse.org/post/mike-milinkovich/moving-forward-jakarta-ee-9"
                          > January 16 announcement</a> that the Jakarta EE 9 release plan is
                          approved, the year is off to a great start for the Jakarta EE community
                          and everyone at the Eclipse Foundation. And, it gets even better with news
                          about industry-first events, including Cloud Native for Java (CN4J) Day
                          and JakartaOne Livestream &ndash; Japan. For details, check out our
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/5.php">News</a> section.
                        </p>
                        <h2>Spotlight Articles</h2>
                        <p>Our spotlight articles this month bring you some of the most
                          interesting and informative content from around the web to help you move
                          forward with Jakarta EE:</p>
                        <ul>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/1.php">Michael Frembs</a> explains how to modernize Java EE apps to
                            cloud native with MicroProfile, and Open Liberty.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/2.php">Patrik Dudit&scaron;</a> shares the process Payara followed to
                            achieve Jakarta EE compatibility.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/4.php">Philip Riecks</a> provides links to a variety of 5- to
                            15-minute, easy-to-understand online tutorials for common Jakarta EE
                            tasks.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/3.php">Richard Monson-Haefel</a> looks at the key role the Jakarta
                            Servlet API and Tomcat play in providing a foundation for innovation
                            using Jakarta EE.</li>
                        </ul>
                        <h2>Short Takes</h2>
                        <p>We also wanted to take this opportunity to highlight some
                          additional content we think you&rsquo;ll find interesting:</p>
                        <ul>
                          <li><a
                            href="http://adambien.blog/roller/abien/entry/are_java_ee_jakarta_ee"
                          >Adam Bien&rsquo;s Tech Talk</a> and Q&amp;A about whether Jakarta EE
                            servers are dead.</li>
                          <li><a
                            href="https://blogs.oracle.com/theaquarium/summary-of-community-retrospective-on-jakarta-ee-8-release"
                          >Will Lyons&rsquo; summary</a> of the community retrospective on the
                            Jakarta EE 8 release.</li>
                          <li><a href="https://www.agilejava.eu/category/hashtag-jakartaee/">Ivar
                              Grimstad&rsquo;s Hashtag Jakarta EE blog</a> for the Jakarta EE
                            community.</li>
                        </ul>
                        <h2>The Latest Happenings</h2>
                        <p>As always, be sure to check our lists so you can stay up to
                          date with the latest happenings here at the Eclipse Foundation:</p>
                        <ul>
                          <li><a href="#new_project_proposals">New Project Proposals</a></li>
                          <li><a href="#new_project_releases">New Project Releases</a></li>
                          <li><a href="#upcoming_events">Upcoming Eclipse Events</a></li>
                        </ul>
                        <h2>Help Us Improve This Newsletter</h2>
                        <p>Finally, you may have noticed we&rsquo;ve changed the format of
                          our newsletter as well as the mix and types of articles we&rsquo;re
                          providing over the past few months. We&rsquo;d really like your feedback
                          on the changes we&rsquo;ve made and your thoughts on how we can make this
                          newsletter even more useful and relevant to you. Send your thoughts to
                          news@eclipse.org.</p>
                        <p>
                          Happy Reading!<br /> Shabnam Mayel
                        </p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/february/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/1.php"><h3>Modernizing Java EE Apps to Cloud Native With MicroProfile, Jakarta EE, and Open Liberty</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/february/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/2.php"><h3>The Road to Jakarta EE Compatibility</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/february/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/3.php"><h3>Jakarta EE: Servlets and Tomcat — 23 Years and Counting</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/february/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/february/4.php"><h3>Jakarta EE Quickstart Guides for Each Application Server</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_proposals" style="margin-bottom:10px;">New Project Proposals</h3>
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul dir="ltr">
                              <li>The <a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/openhw-group-core-v">OpenHW Group CORE-V</a> project produces industrial quality fully documented and verified implementations of the CV32E and CV64A</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a href="/projects/project_activity.php">Read on!</a></p>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_releases" style="margin-bottom:10px;">New Project Releases</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul dir="ltr">
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.elogbook">Eclipse eLogbook@openK Restructuring</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/reviews/eclipse-openk-core-modules-creation-review">Eclipse openK Core Modules Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.collections">Eclipse Collections 10.2.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/tools.tcf">Eclipse Target Communication Framework Restructuring</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.lemminx">Eclipse LemMinX Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/tools.titan">Eclipse RDF4J 3.1 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/tools.titan">Eclipse Titan 6.6.1 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.starter">Eclipse Starter for Jakarta EE Creation</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0"
                        cellspacing="0"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 id="upcoming_events">Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>

                            <p>Upcoming industry events and workshops where our members will play a key role. Hope to see you there!</p>

<p dir="ltr"><a style="color:#000;" href="https://festival.oscafrica.org/">Open Source Africa Festival</a><br />
Feb 20-22 | Lagos, Nigeria</p>

<p dir="ltr"><a style="color:#000;" href="https://devnexus.com/">Devnexus</a><br />
Feb 18-21, 2020 | Atlanta, GA</p>

<p dir="ltr"><a style="color:#000;" href="https://www.javaland.eu/en/home/">Javaland</a><br />
Mar 17-19, 2020 | Phantasialand, Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://www.devdotnext.com/">Dev.next</a><br />
Mar 24-27, 2020 | Broomfield, Colorado</p>

<p dir="ltr"><a style="color:#000;" href="http://eclipse-5413615.hs-sites.com/cn4j-day">Cloud Native for Java Day</a><br />
March 30, 2020 | Amsterdam, The Netherlands</p>

<p dir="ltr"><a style="color:#000;" href="https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/">KubeCon Europe</a><br />
March 30 - April 2, 2020 | Amsterdam, The Netherlands</p>

<p dir="ltr"><a style="color:#000;" href="https://tmt.knect365.com/iot-world/">IoT World</a><br />
Apr 6-9, 2020 | San Jose, CA</p>

<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 id="contact_us" style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->