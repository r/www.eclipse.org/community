<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Twenty-four years ago, in May 1996, Sun Microsystems announced it was developing the server-side
  equivalent to the Java applet, the Java Servlet. Since then, the <a
    href="https://projects.eclipse.org/projects/ee4j.servlet"
  >Servlet API</a> has maintained a prominent position in the enterprise Java ecosystem.
</p>
<p>Here&rsquo;s a brief summary of how we got to where we are today.</p>
<h2>1995-1996: The Dawn of Server-Side Java</h2>
<p>
  <a href="https://twitter.com/errcraft">James Gosling</a> originally proposed the idea of Servlets
  in 1995, the same year Java 1.0 was in beta. The concept of servlets was bandied around until
  early 1996 when it was picked up by <a href="https://twitter.com/pavni">Pavani Diwanji</a> who
  wanted to include it in a pure Java HTTP server Sun was developing, code-named Jeeves. At that
  time, server-side Java was almost non-existent. Netscape had an extension for Java in its web
  server, and Oracle was working on something similar, but otherwise, it was a green field[1].
</p>
<p>
  The Servlet API made its debut at JavaOne in May 1996. Two months later, Sun released the first
  alpha of Jeeves, later named Java Web Server, with the new <a
    href="http://www.cs.unc.edu/~jbs/java/servers/jeeves/JeevesA1.2/doc/apidoc/packages.html"
  >Servlet API</a>. While Jeeves was the first to implement the Servlet API, it was quickly followed
  by open source projects Apache JServ, Jigsaw, Jetty, Localmotive Application Server, and Enhydra.
  Only <a href="https://twitter.com/JettyProject">Jetty</a> remains active today. Commercial
  offerings came from Weblogic Tengah, ATC Dynamo, and LiveSoftware&rsquo;s JRun[2].
</p>
<p>
  In August of 1996, Sun published the first <a
    href="http://www.cs.unc.edu/~jbs/java/servers/jeeves/JeevesA1.2/doc/api.html"
  >white paper</a>, and arguably the first formal documentation of Servlet technology with the
  release of Jeeves Alpha 1.2[3]. The first release of a Servlet specification was in December 1996.
</p>
<h2>1997-2005: The Java Web Servlet Development Kit Becomes Apache Tomcat</h2>
<p>
  After that, the new reference implementation (a full rewrite) of the Servlet API was developed by
  <a href="https://twitter.com/duncan">James Duncan Davidson</a> with the first release of the Java
  Web Servlet Development Kit (JWSDK) in June 1997, and the Java Servlet API 2.1. That was when I
  first used Servlets with Sun&rsquo;s Java Web Server to develop a website that allowed you to look
  up schedules and ports-of-call for Radisson Seven Seas Cruises. That was 23 years ago when the web
  was still young.
</p>
<p>
  Davidson, who incidentally also founded the <a href="https://ant.apache.org/">Apache Ant</a>
  project, convinced Sun Microsystems to donate the JWSDK, the official reference implementation of
  the Servlet API, to the <a href="https://www.apache.org/">Apache Software Foundation</a> in 1999
  under a subproject called Tomcat[4].<a href="http://tomcat.apache.org/"> Apache Tomcat</a> became
  a top-level Apache project in 2005.
</p>
<h2>Tomcat Is Ideal for Super-High-Traffic Sites</h2>
<p>
  Today, Tomcat is the dominant implementation of the Servlet API and its offspring (<a
    href="https://projects.eclipse.org/projects/ee4j.jsp"
  >JSP</a>, <a href="https://projects.eclipse.org/projects/ee4j.el">EL</a>, <a
    href="https://projects.eclipse.org/projects/ee4j.jstl"
  >JSPTL</a>). While Tomcat is not the most pervasive web server in existence &mdash; that honor
  goes to <a href="https://httpd.apache.org/">Apache httpd</a> &mdash; it is the platform of choice
  for super-high-traffic sites, including <a href="https://www.alibaba.com/">Alibaba.com</a> (<a
    href="https://www.similarweb.com/website/alibaba.com"
  >176 million visits/day</a>) and <a href="https://www.politico.com/news/alibaba">Politico.com</a>
  (<a href="https://www.similarweb.com/website/politico.com">44 million visits/day</a>)[5]. These
  numbers are a testament to the importance of the Servlet API and the performance of Tomcat.
</p>
<p>
  The Jakarta Servlet API is arguably the first standard for Java server-side development and is the
  bedrock of everything you know about enterprise Java. It&rsquo;s the foundation of JSP, <a
    href="https://struts.apache.org/"
  >Struts</a>, <a href="http://www.javaserverfaces.org/">JSF</a>, <a href="https://jakarta.ee/">Jakarta
    EE</a>, and <a href="https://spring.io/">Spring</a>, to name a few. Without the Servlet API,
  Java might never have become a leading platform for server-side development.
</p>
<h2>Proven, Hardened Technologies Drive Ongoing Innovation</h2>
<p>
  The Servlet API continues to provide a foundation for innovation. Java microservices and cloud
  native applications increasingly use Servlets in combination with <a
    href="https://projects.eclipse.org/projects/ee4j.jaxrs"
  >JAX-RS</a> to provide robust and fast RESTful web service endpoints.
</p>
<p>
  I&rsquo;m proud to say that Tomitribe is the leading <a
    href="https://www.tomitribe.com/services/enterprise-support/"
  >support</a> company for Tomcat and its Jakarta EE sibling, <a href="http://tomee.apache.org/">TomEE</a>.
  It&rsquo;s fascinating helping companies solve modern problems with hardened and proven
  technologies such as the Servlet API and Tomcat. If you need support for your Tomcat or TomEE
  deployment, please sure to <a href="https://www.tomitribe.com/contact/">contact us</a>. We know
  Tomcat.
</p>
<h2>References</h2>
<p>
  [1] Driscoll, Jim. &ldquo;Servlet History Blog&rdquo; Oracle Blogs. 10 Dec 2005.<br /> <a
    href="https://community.oracle.com/blogs/driscoll/2005/12/10/servlet-history"
  >https://community.oracle.com/blogs/driscoll/2005/12/10/servlet-history</a>&nbsp;
</p>
<p>[2] Jason Brittain, Ian F. Darwin. (2007). Where Did Tomcat Come From. Tomcat: The
  Definitive Guide: The Definitive Guide. O&rsquo;Reilly Media, Inc.&nbsp;</p>
<p>
  [3] CSD, UNC. &ldquo;Java Servlet Application Programming Interface White Paper.&rdquo; August
  1996.<br /> <a href="http://www.cs.unc.edu/~jbs/java/servers/jeeves/JeevesA1.2/doc/api.html">http://www.cs.unc.edu/~jbs/java/servers/jeeves/JeevesA1.2/doc/api.html</a>&nbsp;
</p>
<p>
  [4] James Duncan Davidson. &ldquo;What was the history of Tomcat inside Sun before it was
  open-sourced?&rdquo; Answer. Nov 2014.<br /> <a
    href="https://www.quora.com/What-was-the-history-of-Tomcat-inside-Sun-before-it-was-open-sourced"
  >https://www.quora.com/What-was-the-history-of-Tomcat-inside-Sun-before-it-was-open-sourced#</a>&nbsp;
</p>
<p>
  [5] W3 Technology Surveys. &ldquo;Usage statistics of Tomcat.&rdquo; January 2020.<br /> <a
    href="https://w3techs.com/technologies/details/ws-tomcat"
  >https://w3techs.com/technologies/details/ws-tomcat</a>
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/february/images/richard.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>