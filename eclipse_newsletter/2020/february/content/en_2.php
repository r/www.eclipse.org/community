<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Payara Platform 5.194 was released recently, and just like the previous release, it&rsquo;s a
  certified Jakarta EE implementation. The<a
    href="https://github.com/eclipse-ee4j/jakartaee-platform/issues/131"
  > request for certification</a>, which is the last step required for certification, is now on the
  Jakarta EE platform project issue tracker.
</p>
<p>Here&#39;s what it took for Payara Server to become compatible with Jakarta EE.</p>
<h2>Get the Jakarta EE TCK</h2>
<p>
  The Jakarta EE Technology Compatibility Kit (TCK), also referred to as Compatibility Test Suite
  (CTS), can be downloaded from the<a href="https://jakarta.ee/specifications/platform/8/"> Jakarta
    EE Platform website</a>, or it can be built from<a
    href="https://github.com/eclipse-ee4j/jakartaee-tck"
  > source code</a>, given enough time and determination.
</p>
<p>The test suite is based on Java Test Harness &mdash; the tool used for running Java Development
  Kit (JDK) tests &mdash; as well as a large number of deployment descriptors and Apache Ant files
  to drive the execution. There are actually 50 percent more XML files in TCK distribution than
  there are Java files. In addition to this primary distribution, a compatible server also needs to
  pass TCKs for Context and Dependency Injection and Bean Validation that are distributed
  separately.</p>
<p>
  There are strict rules about what you are permitted to change in the files that make up the TCK.
  &mdash; primarily only the configuration file ts.jte and the porting package. This code
  facilitates management of the server and deployment of test applications. We benefitted from our
  roots in<a href="https://projects.eclipse.org/proposals/eclipse-glassfish"> Eclipse GlassFish</a>
  because we could use its porting package with only minor changes. Eclipse Foundation shell scripts
  that drive Glassfish certification were also very helpful.
</p>
<h2>Run TCK Test Cases</h2>
<p>In total, we ran 49,850 TCK test cases. Each test case usually involves deployment to the
  application server or setting up an application client container and CORBA connection to the
  server. This takes time. The full test suite requires a little more than 119 hours or five full
  days.</p>
<p>
  Here, we were again able to learn from the efforts of Eclipse Foundation members, including
  ourselves, with regard to running TCK test suites in<a href="https://wiki.eclipse.org/Jenkins">
    Jenkins</a>. Like they did, we split the almost 50,000 test cases by technology or by vehicle
  &mdash; a term describing the technology in which API use is tested. For example, every Java
  Database Connectivity (JDBC) test is invoked in the context of Enterprise JavaBean (EJB),
  JavaServer Page (JSP), servlet, and application client containers. We ended up with 77 parts that
  we ran in parallel on nine Linux virtual machines.
</p>
<p>
  With this approach, we received our full test report in less than 14 hours. When all tests passed,
  Jenkins provided us with the<a
    href="https://docs.payara.fish/jakartaee-certification/5.194/tck-results-full-5.194.html#_test_results"
  > test results summary</a>. If tests didn&#39;t pass, we also received test logs and server logs
  for inspection. We then needed to fix the server.
</p>
<h2>Publish the Test Results Summary and Request Certification</h2>
<p>The test results summary is published as part of our documentation and serves as proof of
  compatibility, which we requested from the Jakarta Platform project using a GitHub issue, as
  described at the beginning of this article.</p>
<p>Running the Jakarta EE TCK has become part of our quarterly release process and our monthly patch
  release process. It helps ensure we deliver new functionality with each release, and that we can
  guarantee the same behavior for existing applications.</p>
<h2>Looking Ahead</h2>
<p>We&rsquo;re looking into further improving this test process by shortening the configuration
  phases for partial jobs or even running parts of the suite on JDK 11. Due to classloading changes
  since Java 8, not all of the suites are capable of running on JDK 11, and these results will help
  us shape future Jakarta EE releases so they can become compatible with newer Java versions.</p>
<p>
  To learn more and get involved with the future of Jakarta EE, click<a
    href="https://jakarta.ee/?hsCtaTracking=a296beaa-f248-40b7-b242-cccf7b9af44b%7C94188319-d582-44b7-9086-957015d0a204"
  > here</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/february/images/patrick.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>