<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>The Jakarta EE 9 Release Plan Is Approved</h2>
<p>Great news for everyone in the Jakarta EE community: Jakarta EE 9 is moving forward with a target
  release date of mid-2020. The release has three main goals:</p>
<ul>
  <li>Lower the barrier of entry to new vendors and implementations to achieve compatibility</li>
  <li>Quickly make the release available as a platform for future innovation</li>
  <li>Provide a platform that developers can use as a stable target for testing migration to the new
    namespace</li>
</ul>
<p>Defining this release plan took an enormous amount of community time, effort, and energy.
  Here&rsquo;s a quick look at the key elements in the approved plan:</p>
<ul>
  <li>Move all specification APIs to the Jakarta namespace (sometimes referred to as the &ldquo;big
    bang&rdquo;)</li>
  <li>Remove unwanted or deprecated specifications</li>
  <li>Make minor enhancements to a small number of specifications</li>
  <li>Add no new specifications, apart from specifications pruned from Java SE 8 where appropriate</li>
  <li>Java SE 11 support</li>
</ul>
<p>
  For details about Jakarta EE 9 release content, visit the<a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9ReleasePlan"
  > Jakarta EE platform project website</a>.
</p>
<p>
  For the story behind the Jakarta EE 9 release plan, read<a
    href="https://blogs.eclipse.org/post/mike-milinkovich/moving-forward-jakarta-ee-9"
  > Mike Milinkovich&rsquo;s blog</a>.
</p>
<hr />
<h2>Introducing the Jakarta EE Ambassadors</h2>
<p>
  We&rsquo;re very pleased to tell you the Java EE Guardians have<a
    href="https://jakartaee-ambassadors.io/blog/"
  > rebranded as the Jakarta EE Ambassadors</a>. This independent, grassroots group focuses on
  educating and engaging with as many people as possible to help them understand why Jakarta EE
  really matters, what&rsquo;s in it for them, and how they can contribute.
</p>
<p>Here&rsquo;s how to get more information about this important, all-volunteer team:</p>
<ul>
  <li>Website:<a href="https://jakartaee-ambassadors.io"> https://jakartaee-ambassadors.io</a></li>
  <li>Twitter: @jee_ambassadors</li>
  <li>Google Group: https://groups.google.com/forum/#!forum/jakartaee-ambassadors</li>
</ul>
<hr />
<h2>Cloud Native for Java Day Is Coming up Fast</h2>
<p>Mark Cloud Native for Java (CN4J) Day on March 30 on your calendars. This all-day event at
  KubeCon + CloudNativeCon Europe will be the first time the best and brightest minds from the Java
  ecosystem and the Kubernetes ecosystem join forces at a single event to collaborate and share
  their expertise.</p>
<p>The day will include expert talks, demos, and thought-provoking sessions focused on building
  cloud native enterprise applications using Jakarta EE-based microservices on Kubernetes. Featured
  speakers include:</p>
<ul>
  <li>Tim Ellison, Java CTO at IBM</li>
  <li>Adam Bien, an internationally recognized Java architect, developer, workshop leader, and
    author</li>
  <li>Sebastian Daschner, lead java developer advocate at IBM</li>
  <li>Clement Escoffier, principal software engineer at Red Hat</li>
  <li>Ken Finnegan, senior principal engineer at Red Hat</li>
  <li>Emily Jiang, liberty architect for MicroProfile and CDI at IBM</li>
  <li>Dmitry Kornilov, Jakarta EE and Helidon team leader at Oracle</li>
  <li>Tomas Langer, Helidon architect and developer at Oracle</li>
</ul>
<p>To register, simply add the event to your KubeCon + CloudNativeCon Europe registration.</p>
<p>
  For more details about CN4J Day and a link to the registration page, click<a
    href="https://eclipse-5413615.hs-sites.com/cn4j-day"
  > here</a>. If you have questions, please reach out to events-info@eclipse.org.
</p>
<hr />
<h2>JakartaOne Livestream &ndash; Japan Is February 26</h2>
<p>JakartaOne Livestream &ndash; Japan builds on the success of the first JakartaOne Livestream
  event in September 2019. This one-day virtual conference will be presented entirely in Japanese
  and will include keynotes and technical talks on using Jakarta EE and MicroProfile to build cloud
  native Java applications.</p>
<p>
  To register for this event, click<a href="https://www.crowdcast.io/e/aduh2zba/register"> here</a>.
</p>
<p>For all the details, follow the event on Twitter @JakartaOneJPN.</p>
