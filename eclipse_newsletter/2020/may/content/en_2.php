<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Processing huge volumes of data creates challenges for every enterprise system. The<a
    href="https://projects.eclipse.org/projects/ee4j.batch"
  > </a><a href="https://projects.eclipse.org/projects/ee4j.batch"><span style="color: #1155cc"><u>Jakarta
        Batch project</u></span></a>, which continues the standardization work of Java Specification
  Request 352 (JSR-352), provides a good approach to resolving these challenges.
</p>
<p>Before I describe how to implement a typical batch processing use case, here&rsquo;s a closer
  look at the need for batch processing and the requirements associated with it from the JSR-352
  specification.</p>
<p>
  &ldquo;<em>Batch processing is a pervasive workload pattern, expressed by a distinct application
    organization and execution model. It is found across virtually every industry, applied to such
    tasks as statement generation, bank postings, risk evaluation, credit score calculation,
    inventory management, portfolio optimization, and on and on. Nearly any bulk processing task
    from any business sector is a candidate for batch processing.</em>
</p>
<p>
  <em>Batch processing is typified by bulk-oriented, non-interactive, background execution.
    Frequently long-running, it may be data or computationally intensive, executed sequentially or
    in parallel, and may be initiated through various invocation models, including ad hoc,
    scheduled, and on-demand.</em>
</p>
<p>
  <em>Batch applications have common requirements, including logging, checkpointing, and
    parallelization. Batch workloads have common requirements, especially operational control, which
    allow for initiation of, and interaction with, batch instances; such interactions include stop
    and restart.&rdquo;</em>
</p>
<h2>A Typical Use Case for Batch Processing</h2>
<p>One of the typical use cases for batch workloads is the need to import data from different
  sources and formats into an internal database.</p>
<p>To demonstrate how well structured an application that performs this import can be, I&rsquo;ve
  created a sample application that imports data from json and xml files into a database.</p>
<p>
  As shown in Figure 1, the application can be easily designed using the<a
    href="https://marketplace.eclipse.org/content/red-hat-jboss-developer-studio/help"
  > </a><a href="https://marketplace.eclipse.org/content/red-hat-jboss-developer-studio/help"><span
    style="color: #1155cc"
  ><u>Red Hat CodeReady Studio software</u></span></a> that&rsquo;s available in the<a
    href="https://marketplace.eclipse.org/"
  > </a><a href="https://marketplace.eclipse.org/"><span style="color: #1155cc"><u>Eclipse
        Marketplace</u></span></a>.
</p>
<p>
  <strong>Figure 1: The Sample Application Design</strong>
</p>
<p><img src="images/2_1.png"></p>
<p>In this case, the Jakarta Batch descriptor for XML files looks like the code shown in Figure 2,</p>
<p>
  <span style="font-family: Courier New, serif">META-INF/batch-jobs/hugeImport.xml</span>
</p>
<p>
  <strong>Figure 2: XML Batch Descriptor Code</strong>
</p>
<p><img src="images/2_2.png"></p>
<p>Now, we need to implement each brick above and try to keep each batchlet as independent as
  possible. As you can see in Figure 2, our sample job consists of:</p>
<ul>
  <li><strong>fileSelector</strong>: A batchlet that selects files based on the configuration file
    extension.</li>
  <li><strong>decider</strong>: The decision-maker that is responsible for choosing the right parser
    for the file type.</li>
  <li><strong>xmlParserBatchlet </strong>and<strong> jsonParserBatchlet</strong>: Parser batchlets
    that are responsible for file parsing to a list of items.</li>
  <li><strong>chunkProcessor</strong>: Item-processing chunk reader, (optionally also a chunk
    processor and writer) with partitioning to boost performance.</li>
</ul>
<p>
  First, let&#39;s design a solution to share states between steps. Unfortunately, the Jakarta Batch
  specification does not provide job-scoped Contexts and Dependency Injection (CDI) beans yet. But,
  we can use <span style="font-family: Courier New, serif">JobContext.set\getTransientUserData()</span>to
  deal with the current batch context.
</p>
<p>
  In our case, we want to share <span style="font-family: Courier New, serif">File</span> and <span
    style="font-family: Courier New, serif"
  >Queue</span> with items for processing, as shown in Figure 3.
</p>
<p>
  <strong>Figure 3: Sharing </strong><span style="font-family: Courier New, serif"><strong>File</strong></span><strong>
    and </strong><span style="font-family: Courier New, serif"><strong>Queue </strong></span><strong>With
    Items for Processing</strong>
</p>
<p><img src="images/2_3.png"></p>
<p>
  Now we can inject <span style="font-family: Courier New, serif">ImportJobContext</span> to share
  type-safe state information between batchlets. The first step is to search for the file for
  processing by providing the file name in the properties path, as shown in Figure 4.
</p>
<p>
  <strong>Figure 4: Searching for the File to Be Processed</strong>
</p>
<p><img src="images/2_4.png"></p>
<p>
  Now, it&rsquo;s time to make the decision about which parser to use based on the file extension,
  as shown in Figure 5. When the decider returns the file extension as a string, the batch runtime
  should give control to the corresponding parser batchlet. For reference, see the <span
    style="font-family: Courier New, serif"
  >&lt;decision id=&quot;decider&quot; ref=&quot;myDecider&quot;&gt;</span> section in the XML batch
  descriptor code shown in Figure 2.
</p>
<p>
  <strong>Figure 5: Deciding Which Parser to Use</strong>
</p>
<p><img src="images/2_5.png"></p>
<p>
  The appropriate ParserBatchlet should now parse the file using JSON-B or JAXB, depending on the
  file type, and fill the <span style="font-family: Courier New, serif">Queue</span> with <span
    style="font-family: Courier New, serif"
  >ImportItem</span> objects, as shown in Figure 6.
</p>
<p>
  I would like to use <span style="font-family: Courier New, serif">ConcurrentLinkedQueue</span> to
  share items between partitions, but if you need a different behavior here, you can provide <span
    style="font-family: Courier New, serif"
  >javax.batch.api.partition.PartitionMapper</span> with your own implementation.
</p>
<p>
  <strong>Figure 6: Filling the </strong><span style="font-family: Courier New, serif"><strong>Queue</strong></span><strong>
    With </strong><span style="font-family: Courier New, serif"><strong>ImportItem</strong></span><strong>
    Objects</strong>
</p>
<p><img src="images/2_6.png"></p>
<p>
  <span style="background-color: #ffffff">At this point, the ItemReader is simply pooling items from
    the Queue, as shown in Figure 7.</span>
</p>
<p>
  <strong>Figure 7: ItemReader Pooling Items From the Queue</strong>
</p>
<p><img src="images/2_7.png"></p>
<p>And persist time, as shown in Figure 8.</p>
<p>
  <strong>Figure 8: Setting up Persistence</strong>
</p>
<p><img src="images/2_8.png"></p>
<p>From here, it&rsquo;s easy to extend the application with new parsers, processors, and writers
  without changing the existing code. You can simply describe the new flow, and update existing
  flows, using the Jakarta Batch descriptor.</p>
<p>The Jakarta Batch specification provides much more helpful functionality than I have covered in
  this article, including checkpoints, exception handling, listeners, flow control, and failed job
  restarting, but hopefully this is enough to help you see how simple, powerful, and well-structured
  batch processing can be.</p>
<p>
  The sample application source code in this article is available<a
    href="https://github.com/kostenkoserg/ee-batch-processing-examples"
  > </a><a href="https://github.com/kostenkoserg/ee-batch-processing-examples"><span
    style="color: #1155cc"
  ><u>here in GitHub</u></span></a>.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/may/images/sergii.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>