<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>The Eclipse Foundation Is Transitioning to Europe</h2>
<p>On May 12, we announced [link]the Eclipse Foundation is establishing itself as a European
  organization based in Brussels, Belgium[link]. The transition is expected to be legally finalized
  in July, and will help us build on our recent international growth and our strength in Europe.</p>
<p>In the short term, the move has no impact on committers and contributors. It&rsquo;s business as
  usual. By this summer, we expect to have a new GitLab-based forge that&rsquo;s physically hosted
  in Europe and available for any projects that would like to use it.</p>
<p>As we expand our presence in Europe, we&rsquo;ll share details about any changes to our
  operations with our committers and contributors before they&rsquo;re implemented.</p>
<hr />
<h2>2020 Jakarta EE Developer Survey Results Available in June</h2>
<p>Thank you to everyone who took the time to participate in the 2020 Jakarta EE Developer Survey.
  We had great community engagement, and received more than 2,100 responses from developers around
  the world.</p>
<p>With your input, everyone in the Java ecosystem will have a better understanding of how the cloud
  native world for enterprise Java is unfolding and what that means for their strategies and
  businesses. And the Jakarta EE community will have a better understanding of developers&rsquo; top
  priorities for future Jakarta EE releases.</p>
<p>Stay tuned for the survey results in late June.</p>
<hr />
<h2>Five New Jakarta EE 8 Compatible Products</h2>
<p>We&rsquo;re very pleased to tell you the following products are now certified as Jakarta EE 8
  full platform compatible products:</p>
<ul>
  <li>Red Hat JBoss EAP runtimes</li>
  <li>TmaxSoft JEUS runtime</li>
  <li>Primeton AppServer</li>
  <li>Apusic application server</li>
</ul>
<p>In addition, the Red Hat JBoss Enterprise Application Platform is now certified as a Jakarta EE 8
  web profile compatible product.</p>
<p>
  For a complete list of Jakarta EE 8 compatible products, and access to download links, visit the<a
    href="https://jakarta.ee/compatibility/"
  ><span style="color: #1155cc"> </span></a><a href="https://jakarta.ee/compatibility/"><span
    style="color: #1155cc"
  ><u>Jakarta EE Compatible Products webpage</u></span></a>.
</p>
<hr />
<h2>Jakarta EE Election Results Are in</h2>
<p>The election period for Jakarta EE committee members ran May 1-15, and results are available May
  21, as this newsletter is being published.</p>
<p>
  For a complete list of the winning candidates, check out Tanja’s <a
    href="https://blogs.eclipse.org/post/tanja-obradovic/jakarta-ee-2020-working-group-committee-elections-results-are-here"
  >blog</a>.
</p>
<hr />
<h2>Subscribe to the Jakarta EE Working Group YouTube Channel</h2>
<p>
  To help keep the community connected, the Jakarta EE Working Group launched the<a
    href="https://www.youtube.com/channel/UC4M7h5b6elu9VlzjLfzuXyg"
  > </a><a href="https://www.youtube.com/channel/UC4M7h5b6elu9VlzjLfzuXyg"><span
    style="color: #1155cc"
  ><u>Jakarta EE YouTube Channel</u></span></a> on April 28.
</p>
<p>Subscribe to the channel to stay up to date on the latest happenings in the Jakarta EE community
  &mdash; Tech Talks, community calls, and more.</p>
<p>We also encourage you to promote and share the channel with your networks so everyone can benefit
  from the latest Jakarta EE content.</p>
<hr />
<h2>Learn About Eclipse MicroProfile and Jakarta EE in Brazil&rsquo;s Medical Industry</h2>
<p>
  Watch this<a href="https://www.youtube.com/watch?v=mWtzfYkbN2s"><span style="color: #1155cc"> </span></a><a
    href="https://www.youtube.com/watch?v=mWtzfYkbN2s"
  ><span style="color: #1155cc"><u>Jakarta EE Tech Talk</u></span></a> to learn how Jakarta EE and
  Eclipse MicroProfile are being used to create a safe and scalable architecture that will allow the
  Brazilian medical industry to migrate to the cloud.
</p>
<p>You&rsquo;ll learn how MicroProfile projects, including Health Check, JSON Web Token (JWT)
  Authentication, Metrics, OpenAPI, Rest Client, and Config, are contributing to the success of the
  project. You&rsquo;ll also gain an understanding of challenges that had to be overcome, how they
  were resolved, and lessons learned.</p>
<hr />
<h2>IoT Developers: Please Complete Our 2020 Survey</h2>
<p>
  The 2020 IoT Developer Survey is<a href="https://www.surveymonkey.com/r/newsletterjune"> </a><a
    href="https://www.surveymonkey.com/r/newsletterjune"
  ><span style="color: #1155cc"><u>open until June 26</u></span></a>, and for the first time, the
  survey includes questions about how developers are incorporating edge computing technologies into
  their IoT solutions.
</p>
<p>With your input, everyone in the IoT ecosystem &mdash; original equipment manufacturers (OEMs),
  software vendors, IoT hardware manufacturers, service providers, enterprises, and individual
  developers &mdash; will have a better understanding of the latest IoT solutions and service
  development trends and what they mean for their strategies and businesses.</p>
<p>We encourage all developers in the IoT ecosystem to add their voice. It takes less than 10
  minutes to complete.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>