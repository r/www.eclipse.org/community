<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
  id="bodyTable"
  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; height: 100%; margin: 0; padding: 0; width: 100%; background-color: #FAFAFA;"
>
  <tr>
    <td align="center" valign="top" id="bodyCell"
      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; height: 100%; margin: 0; padding: 10px; width: 100%; border-top: 0;"
    >
      <!-- BEGIN TEMPLATE // --> <!--[if (gte mso 9)|(IE)]>
                        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                        <tr>
                        <td align="center" valign="top" width="600" style="width:600px;">
                        <![endif]-->
      <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
        style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0;"
      >
        <tr>
          <td valign="top" id="templateHeader"
            style="background: #3d3935 url(&amp;quot;https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png&amp;quot;) no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: url(https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png); background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 0;"
          ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnTextBlockInner"
                    style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                      style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      width="100%" class="mcnTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td valign="top" class="mcnTextContent"
                            style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #ffffff; font-size: 16px; line-height: 150%; text-align: left;"
                          >
                            <h4 class="null"
                              style="display: block; margin: 0; padding: 0; color: #ffffff; font-size: 12px; font-style: normal; font-weight: normal; line-height: 125%; letter-spacing: normal; text-align: left;"
                            >
                              Eclipse Newsletter - 2020.21.05<br> &nbsp;
                            </h4>
                            <h1 class="null"
                              style="text-align: center; display: block; margin: 0; padding: 0; color: #ffffff; font-size: 26px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal;"
                            >Jakarta EE 9 and Beyond</h1> <br> &nbsp;
                          </td>
                        </tr>
                      </tbody>
                    </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="templateUpperBody"
            style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;"
          ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
              <tbody class="mcnBoxedTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnBoxedTextBlockInner"
                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                      style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      class="mcnBoxedTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td
                            style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >
                            <table border="0" cellspacing="0" class="mcnTextContentContainer"
                              width="100%"
                              style="min-width: 100% !important; background-color: #3D3935; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            >
                              <tbody>
                                <tr>
                                  <td valign="top" class="mcnTextContent"
                                    style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                  >
                                    <h2 class="null"
                                      style="display: block; margin: 0; padding: 0; color: #ffffff; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                    >Editor's Note</h2>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnCaptionBlock"
              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnCaptionBlockOuter">
                <tr>
                  <td class="mcnCaptionBlockInner" valign="top"
                    style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <table border="0" cellpadding="0" cellspacing="0"
                      class="mcnCaptionRightContentOuter" width="100%"
                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    >
                      <tbody>
                        <tr>
                          <td valign="top" class="mcnCaptionRightContentInner"
                            style="padding: 0 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >
                            <table align="left" border="0" cellpadding="0" cellspacing="0"
                              class="mcnCaptionRightImageContentContainer" width="176"
                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            >
                              <tbody>
                                <tr>
                                  <td class="mcnCaptionRightImageContent" align="center"
                                    valign="top"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><img alt=""
                                    src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/3c015b47-e88f-42e3-a541-c70ec69dea74.png"
                                    width="176"
                                    style="max-width: 200px; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; vertical-align: bottom;"
                                    class="mcnImage"
                                  ></td>
                                </tr>
                              </tbody>
                            </table>
                            <table class="mcnCaptionRightTextContentContainer" align="right"
                              border="0" cellpadding="0" cellspacing="0" width="500"
                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            >
                              <tbody>
                                <tr>
                                  <td valign="top" class="mcnTextContent"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; line-height: 150%; text-align: left;"
                                  >As the target mid-year release date for Jakarta EE 9 approaches,
                                    we wanted to share some technical insights provided by our
                                    community members and get everyone thinking about potential next
                                    steps for Jakarta EE.<br> &nbsp;<br> While Jakarta EE 9 is a
                                    tooling release aimed at infrastructure developers, Jakarta EE
                                    10 gives us the opportunity to add new features and
                                    functionality that will enhance and extend cloud native
                                    technologies for Java.
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnTextBlockInner"
                    style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                      style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      width="100%" class="mcnTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td valign="top" class="mcnTextContent"
                            style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; line-height: 150%; text-align: left;"
                          >
                            <h3 class="null"
                              style="display: block; margin: 0; padding: 0; color: #3d3935; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                            >Spotlight Articles</h3>
                            <p
                              style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                            >Our spotlight articles this month are intended to help you fully
                              leverage Jakarta EE technologies today and tomorrow:</p>
                            <ul>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a
                                href="https://www.eclipse.org/community/eclipse_newsletter/2020/may/1.php"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Steve Millidge</a>&nbsp;shares an update on Jakarta EE 9 progress,
                                provides key considerations for developers wondering whether they
                                should adopt the release, and explains why now is the right time to
                                start thinking about Jakarta EE 10.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a
                                href="https://www.eclipse.org/community/eclipse_newsletter/2020/may/2.php"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Sergii Kostenko</a> explains how to process huge volumes of data
                                using Jakarta EE Batch.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a
                                href="https://www.eclipse.org/community/eclipse_newsletter/2020/may/3.php"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Otavio Santana</a> provides an overview of Jakarta NoSQL, its
                                benefits, and considerations for converting a Jakarta NoSQL
                                application to cloud native.</li>
                            </ul>
                            <h3 class="null"
                              style="display: block; margin: 0; padding: 0; color: #3d3935; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                            >News and Community Updates</h3>
                            <p
                              style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                            >
                              For the latest news and community updates, be sure to check out the <a
                                href="https://www.eclipse.org/community/eclipse_newsletter/2020/may/4.php"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >News</a> section in this newsletter. You’ll learn about:
                            </p>
                            <ul>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >The Eclipse Foundation’s transition to Brussels, Belgium, and what
                                that means for committers and contributors.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >Release dates for Jakarta EE Developer Survey results and Jakarta EE
                                election results.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >The latest Jakarta EE 8 full platform and web profile compatible
                                products.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >The new Jakarta EE Working Group YouTube channel and our latest Tech
                                Talk.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >The recently opened 2020 IoT Developer Survey.</li>
                            </ul>
                            <p
                              style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                            >
                              For a complete list of all available channels for Jakarta EE community
                              updates, click <a href="https://jakarta.ee/connect/" target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >here</a>.<br> &nbsp;<br> Happy Reading!<br> Shabnam Mayel
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnBoxedTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
              <tbody class="mcnBoxedTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnBoxedTextBlockInner"
                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%"
                      style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      class="mcnBoxedTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td
                            style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >
                            <table border="0" cellspacing="0" class="mcnTextContentContainer"
                              width="100%"
                              style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            >
                              <tbody>
                                <tr>
                                  <td valign="top" class="mcnTextContent"
                                    style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                  >
                                    <h2 class="null"
                                      style="display: block; margin: 0; padding: 0; color: #ffffff; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                    >
                                      <strong>New Project Proposals</strong>
                                    </h2>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnTextBlockInner"
                    style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                      style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      width="100%" class="mcnTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td valign="top" class="mcnTextContent"
                            style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; line-height: 150%; text-align: left;"
                          >
                            <ul>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a href="https://projects.eclipse.org/proposals/eclipse-arrowhead"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Eclipse Arrowhead</a>&nbsp;-&nbsp;provides a Service Oriented
                                Architecture-based interoperability and integrability framework for
                                the design and operation of complex systems of systems in the
                                industrial automation domain.&nbsp;</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a
                                href="https://projects.eclipse.org/proposals/eclipse-lsp4mp-language-server-eclipse-microprofile-0"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Eclipse LSP4MP - Language Server for Eclipse MicroProfile</a>&nbsp;-&nbsp;provides
                                core language support capabilities for the specifications defined
                                under the Eclipse MicroProfile project.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a href="https://projects.eclipse.org/proposals/eclipse-phizog"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Eclipse Phizog</a>&nbsp;-&nbsp;provides open implementations
                                conformant with the FACE-TSS (Transport Services Segment) standard
                                for mainstream programming languages.&nbsp;</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a href="https://projects.eclipse.org/proposals/jakarta-mvc"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Jakarta MVC</a>&nbsp;-&nbsp;defines a standard for creating web
                                applications following the action-based model-view-controller
                                pattern.</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a href="https://projects.eclipse.org/proposals/asciidoc-language"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >AsciiDoc Language</a>&nbsp;- defines and maintains the AsciiDoc
                                Language Specification and Technology Compatibility Kit (TCK), its
                                artifacts, and the corresponding language and API
                                documentation.&nbsp;</li>
                              <li
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              ><a href="https://projects.eclipse.org/proposals/eclipse-cloe"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Eclipse Cloe</a>&nbsp;-&nbsp;provides simulation middleware and
                                simulation-engine bindings for connecting simulation engines to the
                                "software under test".</li>
                            </ul>
                            <p
                              style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                            >
                              Interested in more project activity? <a
                                href="https://www.eclipse.org/projects/project_activity.php"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                              >Read on!</a>
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td valign="top" id="templateColumns"
            style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;"
          >
            <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
              class="columnWrapper"
              style="width:50%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tr>
                <td valign="top" class="columnContainer"
                  style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                ><table border="0" cellpadding="0" cellspacing="0" width="100%"
                    class="mcnBoxedTextBlock"
                    style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                    <tbody class="mcnBoxedTextBlockOuter">
                      <tr>
                        <td valign="top" class="mcnBoxedTextBlockInner"
                          style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        >
                          <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                          <table align="left" border="0" cellpadding="0" cellspacing="0"
                            width="100%"
                            style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            class="mcnBoxedTextContentContainer"
                          >
                            <tbody>
                              <tr>
                                <td
                                  style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                >
                                  <table border="0" cellspacing="0" class="mcnTextContentContainer"
                                    width="100%"
                                    style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  >
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent"
                                          style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                        >
                                          <h2 class="null"
                                            style="display: block; margin: 0; padding: 0; color: #ffffff; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                          >New Project Releases</h2>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%"
                    class="mcnTextBlock"
                    style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <tbody class="mcnTextBlockOuter">
                      <tr>
                        <td valign="top" class="mcnTextBlockInner"
                          style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        >
                          <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
                          <table align="left" border="0" cellpadding="0" cellspacing="0"
                            style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            width="100%" class="mcnTextContentContainer"
                          >
                            <tbody>
                              <tr>
                                <td valign="top" class="mcnTextContent"
                                  style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; line-height: 150%; text-align: left;"
                                >
                                  <ul>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/iot.tangleid"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Tangle Identity Creation</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/iot.smarthome"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse SmartHome Termination</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a
                                      href="https://projects.eclipse.org/projects/iot.tanglemktplaces"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Tangle Marketplaces Creation</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/openhw.corev"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >OpenHW Group CORE-V Cores Creation</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/tools.atf"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse Ajax Tools Framework (ATF) Termination</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a
                                      href="https://projects.eclipse.org/projects/technology.remus"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse Remus Termination</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a
                                      href="https://projects.eclipse.org/projects/ee4j.eclipselink"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >EclipseLink 2.7.7 Release</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/technology.jifa"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse Jifa Creation</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a href="https://projects.eclipse.org/projects/ee4j.metro"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse Metro 2.4.4 Release</a></li>
                                    <li
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    ><a
                                      href="https://projects.eclipse.org/projects/technology.justj"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse JustJ Creation</a></li>
                                  </ul>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://www.eclipse.org/projects/tools/reviews.php"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >View all the project releases!</a>
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
            <table align="left" border="0" cellpadding="0" cellspacing="0"
              class="columnWrapper"
              style="width:50%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tr>
                <td valign="top" class="columnContainer"
                  style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                ><table border="0" cellpadding="0" cellspacing="0" width="100%"
                    class="mcnBoxedTextBlock"
                    style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                    <tbody class="mcnBoxedTextBlockOuter">
                      <tr>
                        <td valign="top" class="mcnBoxedTextBlockInner"
                          style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        >
                          <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                          <table align="left" border="0" cellpadding="0" cellspacing="0"
                            width="100%"
                            style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            class="mcnBoxedTextContentContainer"
                          >
                            <tbody>
                              <tr>
                                <td
                                  style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                >
                                  <table border="0" cellspacing="0" class="mcnTextContentContainer"
                                    width="100%"
                                    style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  >
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent"
                                          style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                        >
                                          <h2 class="null"
                                            style="display: block; margin: 0; padding: 0; color: #ffffff; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                          >Upcoming Events</h2>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table border="0" cellpadding="0" cellspacing="0" width="100%"
                    class="mcnTextBlock"
                    style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <tbody class="mcnTextBlockOuter">
                      <tr>
                        <td valign="top" class="mcnTextBlockInner"
                          style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        >
                          <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
                          <table align="left" border="0" cellpadding="0" cellspacing="0"
                            style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                            width="100%" class="mcnTextContentContainer"
                          >
                            <tbody>
                              <tr>
                                <td valign="top" class="mcnTextContent"
                                  style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; line-height: 150%; text-align: left;"
                                >
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://iot.eclipse.org/eclipse-virtual-iot-2020/"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Virtual Eclipse IoT and Edge Native Day</a> | May 28, 2020 |
                                    Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://www.siriuscon.org/" target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >SiriusCon2020</a> | June 18, 2020 | Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://jakartaone.org/brazil2020/"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >JakartaOne Livestream Brazil</a> | August 29, 2020 | Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://www.osdforum.org/" target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >OSD Forum</a> | September 15, 2020 | Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://events.eclipse.org/2020/sam-iot/"
                                      target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >Eclipse SAM IoT 2020</a> | September 17-18, 2020 | Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://www.eclipsecon.org/2020" target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >EclipseCon 2020</a> | October 19-22, 2020 | Virtual
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    <a href="https://sumo.dlr.de/2020" target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >SUMO User Conference 2020</a> | October 25-28, 2020 | Berlin,
                                    Germany<br> &nbsp;
                                  </p>
                                  <p
                                    style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; line-height: 150%; text-align: left;"
                                  >
                                    Do you know about an event relevant to the Eclipse community?
                                    Submit your event to&nbsp;<a
                                      href="https://newsroom.eclipse.org/" target="_blank"
                                      style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-weight: normal; text-decoration: underline;"
                                    >newsroom.eclipse.org</a>.
                                  </p>
                                </td>
                              </tr>
                            </tbody>
                          </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table></td>
              </tr>
            </table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
          </td>
        </tr>
        <tr>
          <td valign="top" id="templateLowerBody"
            style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 2px solid #EAEAEA; padding-top: 0; padding-bottom: 9px;"
          ></td>
        </tr>
        <tr>
          <td valign="top" id="templateFooter"
            style="background: #3d3935 none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;"
          ><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnImageBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnImageBlockOuter">
                <tr>
                  <td valign="top"
                    style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    class="mcnImageBlockInner"
                  >
                    <table align="left" width="100%" border="0" cellpadding="0" cellspacing="0"
                      class="mcnImageContentContainer"
                      style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    >
                      <tbody>
                        <tr>
                          <td class="mcnImageContent" valign="top"
                            style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          ><img align="center" alt=""
                            src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/54993d58-f464-469b-b433-24729f44b06f.png"
                            width="186.12"
                            style="max-width: 1108px; padding-bottom: 0px; vertical-align: bottom; display: inline !important; border-radius: 0%; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                            class="mcnImage"
                          ></td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnFollowBlockOuter">
                <tr>
                  <td align="center" valign="top"
                    style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    class="mcnFollowBlockInner"
                  >
                    <table border="0" cellpadding="0" cellspacing="0" width="100%"
                      class="mcnFollowContentContainer"
                      style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    >
                      <tbody>
                        <tr>
                          <td align="center"
                            style="padding-left: 9px; padding-right: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >
                            <table border="0" cellpadding="0" cellspacing="0" width="100%"
                              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              class="mcnFollowContent"
                            >
                              <tbody>
                                <tr>
                                  <td align="center" valign="top"
                                    style="padding-top: 9px; padding-right: 9px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  >
                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                    >
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="top"
                                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          >
                                            <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="https://www.eclipse.org/"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png"
                                                                      alt="Website"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="mailto:newsletter@eclipse.org"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png"
                                                                      alt="Email"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="https://twitter.com/EclipseFdn"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-twitter-48.png"
                                                                      alt="Twitter"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="https://www.facebook.com/eclipse.org"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png"
                                                                      alt="Facebook"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="https://www.youtube.com/user/EclipseFdn"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-youtube-48.png"
                                                                      alt="YouTube"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                            <table align="left" border="0" cellpadding="0"
                                              cellspacing="0"
                                              style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                            >
                                              <tbody>
                                                <tr>
                                                  <td valign="top"
                                                    style="padding-right: 0; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    class="mcnFollowContentItemContainer"
                                                  >
                                                    <table border="0" cellpadding="0"
                                                      cellspacing="0" width="100%"
                                                      class="mcnFollowContentItem"
                                                      style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                    >
                                                      <tbody>
                                                        <tr>
                                                          <td align="left" valign="middle"
                                                            style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          >
                                                            <table align="left" border="0"
                                                              cellpadding="0" cellspacing="0"
                                                              width=""
                                                              style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                            >
                                                              <tbody>
                                                                <tr>
                                                                  <td align="center" valign="middle"
                                                                    width="24"
                                                                    class="mcnFollowIconContent"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><a
                                                                    href="https://www.linkedin.com/company/eclipse-foundation"
                                                                    target="_blank"
                                                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                  ><img
                                                                      src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-linkedin-48.png"
                                                                      alt="LinkedIn"
                                                                      style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                      height="24" width="24"
                                                                      class=""
                                                                    ></a></td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </td>
                                                        </tr>
                                                      </tbody>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; table-layout: fixed !important;"
            >
              <tbody class="mcnDividerBlockOuter">
                <tr>
                  <td class="mcnDividerBlockInner"
                    style="min-width: 100%; padding: 10px 18px 25px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0"
                      width="100%"
                      style="min-width: 100%; border-top: 2px solid #EEEEEE; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                    >
                      <tbody>
                        <tr>
                          <td
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          ><span></span></td>
                        </tr>
                      </tbody>
                    </table> <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                  </td>
                </tr>
              </tbody>
            </table>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock"
              style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
            >
              <tbody class="mcnTextBlockOuter">
                <tr>
                  <td valign="top" class="mcnTextBlockInner"
                    style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  >
                    <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                      style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      width="100%" class="mcnTextContentContainer"
                    >
                      <tbody>
                        <tr>
                          <td valign="top" class="mcnTextContent"
                            style="padding: 0px 18px 9px; color: #FFFFFF; font-family: Roboto,&amp; quot; Helvetica Neue&amp;quot; , Helvetica , Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; font-size: 12px; line-height: 150%; text-align: center;"
                          ><span style="font-size: 12px">This email was sent to&nbsp;*|EMAIL|*<br>
                              You can <a href="*|DEFAULT:UPDATE_PROFILE|*"
                              style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-weight: normal; text-decoration: underline;"
                            ><span style="color: #FFFFFF"><u>update your preferences</u></span></a>
                              or <a href="*|DEFAULT:UNSUB|*"
                              style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-weight: normal; text-decoration: underline;"
                            ><span style="color: #FFFFFF"><u>unsubscribe from this list</u></span></a>.<br>
                              Eclipse Foundation · 2934 Baseline Road, Suite 202 · Ottawa, ON K2H
                              1B2 · Canada
                          </span></td>
                        </tr>
                      </tbody>
                    </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </table> <!--[if (gte mso 9)|(IE)]>
                        </td>
                        </tr>
                        </table>
                        <![endif]--> <!-- // END TEMPLATE -->
    </td>
  </tr>
</table>
