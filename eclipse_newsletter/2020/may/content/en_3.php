<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Jakarta EE picks up where Java EE 8 left off, but the roadmap going forward will be focused on
  modern innovations such as microservices, modularity, and NoSQL databases.</p>
<p>
  The<a href="https://projects.eclipse.org/proposals/jakarta-nosql"><span style="color: #1155cc"> </span></a><a
    href="https://projects.eclipse.org/proposals/jakarta-nosql"
  ><span style="color: #1155cc"><u>Jakarta NoSQL specification</u></span></a> simplifies integration
  between Java applications and NoSQL databases by providing a common API that works with different
  types of NoSQL databases and different vendors&rsquo; database versions.
</p>
<p>To achieve this simplification, the spec has two layers:</p>
<ul>
  <li><strong>Communication API</strong>: This API provides the same capabilities as Java Database
    Connectivity (JDBC) provides to SQL. It has four specializations, one for each type of database
    &mdash; column, document, key-value, and graph. Each specialization is independent from the
    others, optional for database vendors, and has its own technology compatibility kit (TCK).</li>
  <li><strong>Mapping API</strong>: This is the API developers should use to integrate their Java
    application into a database. It is based on Annotations and Contexts and Dependency Injection
    (CDI), and it preserves integration with other Jakarta EE technologies, such as Bean Validation.</li>
</ul>
<p>Figure 1 shows the two Jakarta NoSQL layers, their relationship to one another, and to the four
  database types.</p>
<p>
  <strong>Figure 1: Jakarta NoSQL Layers in Context</strong>
</p>
<p><img src="images/3_1.png"></p>
<h2>Choosing a NoSQL Database</h2>
<p>Vendor lock-in is one of the key factors Java developers must consider when choosing a NoSQL
  database. If you&rsquo;re switching from one database to another, considerations also include the:</p>
<ul>
  <li>Amount of time required to make the change</li>
  <li>Learning curve required to use a new API with the database</li>
  <li>Code that will be lost</li>
  <li>Persistence layer that needs to be replaced</li>
</ul>
<p>Jakarta NoSQL avoids most of these issues through its Communication API. Jakarta NoSQL also
  provides template classes that apply the design pattern &ldquo;template method&rdquo; to database
  operations.</p>
<p>
  In addition, the Jakarta NoSQL <span style="font-family: Courier New, serif">Repository</span>
  interface allows Java developers to create and extend interfaces, with implementation
  automatically provided by Jakarta NoSQL. This means the support method queries you build are
  automatically implemented for you.
</p>
<p>Figure 2 shows four document NoSQL databases.</p>
<p>
  <strong>Figure 2: Four Document NoSQL Databases and Their Unique Syntax</strong>
</p>
<p><img src="images/3_2.png"></p>
<p>Each code snippet shown in Figure 2 is doing the same thing &mdash; creating a document, a tuple
  with a name, and the information itself &mdash; with the same behavioral goal. But, each database
  uses a different class, method name, and so on. If you want to move your code from one database to
  another, you need to learn a new API and update all of the database code to the new database API
  code target.</p>
<p>With the Jakarta NoSQL Communication API, you can use driver databases that look like JDBC
  drivers to quickly and easily switch between NoSQL databases. This ability also makes it easier to
  learn about the software architecture of a new NoSQL database. Figure 3 illustrates the concept.</p>
<p>
  <strong>Figure 3: Common Jakarta NoSQL Code for Different Document Databases </strong>
</p>
<p><img src="images/3_3.png"></p>
<h2>Considerations for Converting a Jakarta NoSQL Application to Cloud Native</h2>
<p>When it comes time to convert your Jakarta NoSQL application to cloud native, there are several
  factors to consider, including which best practices to follow to avoid mistakes and code smell.</p>
<p>As far as we know, there aren&rsquo;t best practices related specifically to cloud native
  development. However, there are patterns from agile, microservices, and the twelve-factor app that
  are useful to follow to help ensure you develop a healthy, pain-free application.</p>
<p>I recommend familiarizing yourself with the aspects of the twelve-factor app, as well as the
  Manifesto for Agile Software Development and the concepts of continuous integration, continuous
  delivery, and domain-driven design.</p>
<p>
  The most well-known practices related to any application that includes cloud computing are
  inspired by Martin Fowler&rsquo;s book, <em>Patterns of Enterprise Application Architecture</em>.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/may/images/otavio.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
