<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>As we get closer to our target mid-year timeframe for the Jakarta EE 9 release, I want to update
  you on our progress and some of the challenges we&rsquo;re working through. I also want to review
  some of the considerations for developers who are wondering whether they should adopt Jakarta EE
  9. Finally, I want to explain why now is the right time for everyone in the community to start
  thinking about Jakarta EE 10 and the new features they would like it to include.</p>
<h2>Jakarta EE 9: Certifying Compatibility Is Complicated</h2>
<p>
  By now, everyone knows that<a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9ReleasePlan"
  > </a><a href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9ReleasePlan">Jakarta
    EE 9 is a tooling release</a> that&rsquo;s focused on updating specifications to the new Jakarta
  EE 9 namespace, jakarta.*, and removing Jakarta EE 8 specifications that are no longer relevant.
  It&rsquo;s not full of exciting new features &mdash; and I say that as a developer who is working
  on the release &mdash; but Jakarta EE 9 is an<a
    href="https://blogs.eclipse.org/post/mike-milinkovich/moving-forward-jakarta-ee-9"
  > </a><a href="https://blogs.eclipse.org/post/mike-milinkovich/moving-forward-jakarta-ee-9">important
    and necessary step</a> on the road to further innovation using cloud native technologies for
  Java.
</p>
<p>We&rsquo;ve completed the namespace changes in all of the Jakarta EE APIs and the release
  candidates are available on maven central. However, the challenges and complexity increased
  significantly when we started certifying the first Jakarta EE 9-compatible product.</p>
<p>
  To release Jakarta EE 9, we must verify that one<a href="https://jakarta.ee/compatibility/"> </a><a
    href="https://jakarta.ee/compatibility/"
  >compatible product</a> successfully implements the Jakarta EE 9 APIs. As in Jakarta EE 8, the
  product leading the way at Eclipse is the<a
    href="https://projects.eclipse.org/projects/ee4j.glassfish"
  > </a><a href="https://projects.eclipse.org/projects/ee4j.glassfish">Eclipse GlassFish</a>
  application server. However, because GlassFish code depends on namespace updates in code developed
  by other community members, such as IBM and Red Hat, we need to take a step-by-step approach to
  the implementation.
</p>
<p>To further complicate the requirement, the companies and code we&rsquo;re dependent on have their
  own namespace dependencies that must also be addressed. In some cases, the dependencies run five
  or six layers deep. The namespace changes must be made in each project, in the right order, before
  we can successfully build a Jakarta EE 9-compatible version of GlassFish. Needless to say,
  certifying GlassFish compatibility has been very slow going, but we have now completed the bulk of
  the work and GlassFish is compiling without errors in the new namespace.</p>
<p>
  I&rsquo;ve had a number of discussions about how the community can help to accelerate Jakarta EE
  9. Unfortunately, the intertwined nature of the updates that need to be made mean there are many
  tasks that are either very difficult, or impossible, to complete in parallel. However, community
  members can help by updating the specification documents to the new namespace. You can find
  information about the different ways to stay connected with the Jakarta EE community and get
  involved<a href="https://jakarta.ee/connect/"> </a><a href="https://jakarta.ee/connect/">here</a>.
</p>
<h2>Jakarta EE 9 Targets Infrastructure Developers</h2>
<p>In many cases, developers who are creating applications for Jakarta EE 8 or Java EE 8 have no
  reason to move to Jakarta EE 9 because there&rsquo;s no new functionality in the release. They can
  wait for Jakarta EE 10 and adopt the new namespace along with the enhanced functionality this
  release will include. Of course, application developers can always move their application to the
  new namespace before Jakarta EE 10 to ensure there are no surprises down the road, but it&rsquo;s
  not required.</p>
<p>On the other hand, developers who create tools, libraries, and other infrastructure that are
  based on Jakarta EE technologies will want to move to Jakarta EE 9. These infrastructure
  developers are the primary target for Jakarta EE 9.</p>
<p>The Jakarta EE APIs are foundational across the industry and are used in many popular runtime
  frameworks, web servers, and other tools. Spring Boot, Apache Tomcat, and Dropwizard are just
  three examples. Infrastructure developers will want to ensure their tools and libraries implement
  the new namespace so they can be used by application developers who adopt Jakarta EE 10.</p>
<p>In some cases, application developers may not realize the infrastructure tools they rely on are
  based on Jakarta EE or Java EE APIs and will require the namespace change. As I&rsquo;ve been
  working on the GlassFish compatibility certification, I&rsquo;ve certainly been surprised at how
  far and wide the namespace change extends. To ensure you&rsquo;re able to move your application
  forward, I recommend taking a close look at your infrastructure tools and asking the providers
  when they&rsquo;re moving to Jakarta EE 9.</p>
<h2>Start Sharing Your Ideas for Jakarta EE 10 Now</h2>
<p>Jakarta EE 10 will be a much more exciting release for application developers than Jakarta EE 9
  because we can include features and functionality that will really help the community start
  delivering on the potential of cloud native technologies for Java.</p>
<p>There&rsquo;s no overall theme for Jakarta EE 10 at this point, but as a Jakarta EE Steering
  Committee member, some of the key areas where I would like to see community input and ideas
  include:</p>
<ul>
  <li>How we align the platform around Contexts and Dependency Injection (CDI), which is one of the
    foundational aspects of Jakarta EE.</li>
  <li>How we can improve Jakarta EE messaging and take advantage of new messaging technologies, such
    as Apache Kafka, and cloud technologies.</li>
  <li>How we can improve individual Jakarta EE projects, such as<a
    href="https://projects.eclipse.org/projects/ee4j.cu"
  > </a><a href="https://projects.eclipse.org/projects/ee4j.cu">Jakarta Concurrency</a>.
  </li>
</ul>
<p>I strongly encourage the entire community to start putting forward their ideas for Jakarta EE 10
  immediately. There are two main ways to do this:</p>
<ul>
  <li>Subscribe to<a href="https://jakarta.ee/connect/mailing-lists/"> </a><a
    href="https://jakarta.ee/connect/mailing-lists/"
  >Jakarta EE project mailing lists</a> and get involved in Jakarta EE 10 discussions.
  </li>
  <li>Raise enhancement requests on the GitHub repository for an Eclipse Enterprise for Java (EE4J)
    project. You can find the complete list of EE4J projects<a
    href="https://projects.eclipse.org/projects/ee4j"
  > </a><a href="https://projects.eclipse.org/projects/ee4j">here</a>.
  </li>
</ul>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/may/images/steve.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>