<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>COVID-19 Impact on Upcoming Events</h2>
<p>As the novel coronavirus (COVID-19) makes headlines around the world, conferences and
  community events throughout our industry continue to be cancelled or postponed.</p>
<p>With the health and safety of our employees and community members as a top priority,
  the Eclipse Foundation has decided to restrict all non-essential travel for our staff through
  mid-May. We will not be attending any events during this period, and will assess our participation
  beyond that point following recommendations from the Canadian federal government, the World Health
  Organization, and the Centers for Disease Control (CDC).</p>
<p>
  We will continue to provide updates about our participation in events through our social media
  channels:<a href="https://twitter.com/EclipseFdn?mc_cid=5b47a23005&amp;mc_eid=%5bUNIQID%5d">
    Twitter</a>,<a
    href="https://www.facebook.com/eclipse.org?mc_cid=5b47a23005&amp;mc_eid=%5bUNIQID%5d"
  > Facebook</a>, and<a
    href="https://www.linkedin.com/company/eclipse-foundation?mc_cid=5b47a23005&amp;mc_eid=%5bUNIQID%5d"
  > LinkedIn</a>.
</p>
<h2>IoT Commercial Adoption Survey Results Now Available</h2>
<p>
  The results of our 2019 IoT Commercial Adoption Survey are now available<a
    href="http://eclipse-5413615.hs-sites.com/iot-adoption-survey-results-2019"
  > here</a>.
</p>
<p>The goal of this survey was to gather insights that would help IoT ecosystem
  stakeholders gain a better understanding of the IoT industry landscape and the requirements,
  priorities, and challenges faced by organizations that are deploying and using commercial IoT
  solutions.</p>
<p>Key findings from the survey include:</p>
<ul>
  <li>IoT may well live up to the hype, if somewhat slower than expected. Just under 40 percent of
    survey respondents are deploying IoT solutions today. Another 22 percent plan to start deploying
    IoT within the next two years.</li>
  <li>IoT investment is on the rise, with 40 percent of organizations planning to increase their IoT
    spending in the next fiscal year.</li>
  <li>Open source pervades IoT as a key enabler with 60 percent of companies factoring open source
    into their IoT deployment plans.</li>
  <li>Hybrid clouds lead the way for IoT deployments. Overall, AWS, Azure, and GCP are the leading
    cloud platforms for IoT implementations.</li>
</ul>
<p>
  To learn more about how each player in the IoT ecosystem can use the survey results to drive IoT
  adoption,<a href="https://blogs.eclipse.org/post/mike-milinkovich/real-world-iot-adoption"> read
    this blog</a> by Eclipse Foundation Executive Director, Mike Milinkovich.
</p>

<h2>The Eclipse IDE 2020-03 Release is Here</h2>
<p dir="ltr">
  The Eclipse IDE 2020-03 release is <a href="https://www.eclipse.org/eclipseide/2020-03/">here</a>
  and available for download. To learn more about the release&rsquo;s key features and improvements,
  head over to the <a href="https://www.eclipse.org/eclipseide/2020-03/noteworthy/">2020-03 New and
    Noteworthy</a> summary.
</p>
<p dir="ltr">The quarterly simultaneous release of Eclipse projects is a huge accomplishment made
  possible by the tireless efforts of many committers, projects, and Foundation staff. There are 74
  projects in the 2020-03 simultaneous release, consisting of over 72 million lines of code, with
  contributions by 196 developers, 130 of whom are Eclipse committers.</p>
<p dir="ltr">Congratulations and thank you to everyone who contributed to this achievement.</p>