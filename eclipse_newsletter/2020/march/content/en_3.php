<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  When the <a href="https://www.eclipse.org/che/">Eclipse Che</a> project started four years ago
  this month, the most common question I heard was &ldquo;This is really cool technology, but what
  do you use it for?&rdquo;
</p>
<p>At the time, there wasn&rsquo;t a lot of activity happening around cloud-based developer tools such as integrated development environments (IDEs).
  There were even fewer activities around container-focused, cloud-based IDEs. People were very
  interested in what was happening in Eclipse Che, but they weren&rsquo;t quite sure what to make of
  it.</p>
<p>Fast-forward to March 2020 and the landscape has changed dramatically. People have a
  much better understanding of cloud-based development and containers. And they appreciate
  how much faster and easier Eclipse Che makes it to develop enterprise applications that leverage
  containers and Kubernetes.</p>
<p>With the speed and simplicity that Che enables, people have flocked to it. Today, a
  number of corporations and communities across the globe rely heavily on Eclipse Che to expand,
  enhance, and tailor their developer experiences. Leading technology providers, including IBM, CA
  Technologies, SAP, Red Hat, Samsung, and Broadcom use Che. It&rsquo;s also used broadly across
  industries, including banking, automotive, and healthcare.</p>
<p>While most people have a good understanding of Eclipse Che and its benefits, there are
  still a few facts about this very versatile technology that surprise people.</p>
<h2>1. You Can Use Che for Just About Anything</h2>
<p>Instead of asking what you would use Che for, the question now is more along the lines
  of &ldquo;what wouldn&rsquo;t you use it for?&rdquo; In addition to the expected use cases — helping developers get
  started and contribute to applications that will be deployed on a Kubernetes platform — we&rsquo;re also seeing some
  really interesting and unexpected applications.</p>
<ul>
  <li><strong>Mainframes. </strong>At least two major corporations are using the cutting-edge
    technology in Eclipse Che to make it easier for developers to build mainframe applications.
    While mainframes are very established technology, they&rsquo;re also arguably one of the most
    complex developer environments to set up. Newer developers typically don&rsquo;t have the
    experience or expertise needed, but they’re intrigued by the salaries COBOL developers can receive!
    Eclipse Che gives them an easy on-ramp if they want to pursue that interest.</li>
</ul>
<p>Once you get past the novel idea of combining mainframes with new technology like containers
 and Kubernetes, it makes perfect sense. But, it&rsquo;s certainly not a use case we
  envisioned when we started Che.</p>
<ul>
  <li><strong>IoT.</strong> Eclipse Che is also making it easier for developers to contribute to IoT
    device development &mdash; from laptops, from the cloud, from anywhere really. We&rsquo;ve seen
    Che used for IoT application development in the automotive industry and on devices such as the
    Raspberry Pi.</li>
</ul>
<p>These are just two of the many innovative ways Che is being used. Those of us who work
  on the project feel we&rsquo;re just starting to scratch the surface of how the technology can be
  used.</p>
<h2>2. The Real Power of Che Is the Backend Functionality</h2>
<p>Many people think of Eclipse Che as a web-based IDE. But, the IDE represents only about
  20 percent of Che&rsquo;s capabilities. The most interesting and powerful part of Che is the 80
  percent that happens behind the scenes.</p>
<p>A growing number of people in the Eclipse Che community are focusing on what the Che
  backend can do for them. In fact, some don&rsquo;t use the Che IDE at all. They swap it out for
  something that&rsquo;s more appropriate for their needs.</p>
<p>For example, data scientists don&rsquo;t need an IDE that looks like Visual Studio Code
  because they&rsquo;re not coders. They want to use their Jupyter Notebook or something like it.
  Other folks don&rsquo;t want to use a web-based editor. They prefer to take advantage of the Che
  server and the workspaces it provisions directly from their desktop IDE. That’s possible too.
</p>
<p>The point here is the conversation shouldn&rsquo;t be about the Che IDE. It should be
  about comparing Eclipse Che to other tools that provision workspaces in the background so
  developers don&rsquo;t have to. Today, there aren&rsquo;t many tools that provide this capability
  in a Kubernetes environment. You can do it with virtual machines, but VMs are heavy, slow, and not
  easily portable. And they certainly don&rsquo;t support hyperscale like containers and Kubernetes
  do.</p>
<h2>3. You Can Use Che Offline</h2>
<p>You can use Eclipse Che in an environment that&rsquo;s completely disconnected, or
  air-gapped, from the internet. There are two reasons this is important:</p>
<ul>
  <li>Many organizations in government, defense, financial, and academic sectors have walled-off
    their most important applications from the internet for security reasons. However, they still
    want their developers to be able to quickly contribute code to those critical applications.</li>
</ul>
<p>When these organizations find out they can use Che offline, it&rsquo;s typically a big
  &ldquo;wow&rdquo; moment for them. Their developers are very intelligent people, but they work in
  highly constrained worlds with limited access to sophisticated, modern tools like Che.</p>
<ul>
  <li>When developers are traveling, they often have periods of time where they&rsquo;re not
    connected to the internet. With Che, they can remain productive during these times.</li>
</ul>
<h2>4. Che Has Many Innovative Contributors</h2>
<p>Many people still think of Eclipse Che as a Red Hat project, but it&rsquo;s not. It
  started as a project from a company called Codenvy that was acquired by Red Hat, but the community
  has broadened considerably since then. It now includes contributors from a wide range of locations
  and corporations.</p>
<p>Che is your typical Eclipse community that&rsquo;s always expanding and is very
  approachable. We have weekly community calls, and they now often include demos from our community
  members. Some of these demos are very impressive, which shows we&rsquo;ve reached an
  exciting and interesting stage in the project&rsquo;s growth.</p>
<h2>5. The Che IDE Has Come a Long Way Since It Was First Released</h2>
<p>Although the IDE isn’t the only part of Che you should care about, it’s where most people first interact with the project.
  When Eclipse Che was first released, many people probably liked the functionality it
  provided, but were put off by the fact that it didn&rsquo;t look like the IDE they were already
  using. It likely seemed easier to stick with what they knew rather than go through a learning
  curve they didn&rsquo;t have time for.</p>
<p>Today, the Che IDE looks and behaves like Visual Studio Code so it&rsquo;s a painless
  move for millions of developers around the world. Developers also have the flexibility to stick
  with their preferred cloud or on-premises IDE. That learning curve they were so worried about has
  virtually disappeared.</p>
<p>In addition, in Che 7, we made key changes that mean production containers look the
  same in production as they do in development. Tooling and other required elements are wrapped
  around the production container, using container sidecars, so the application containers aren’t
  changed when you move from development to production or vice-versa. This is very important to ensure the validity of testing.</p>
<h2>Take Another Look at Che</h2>
<p>If you haven&rsquo;t looked at Che in a while, it&rsquo;s definitely time to take
  another look and re-evaluate how it can help you take advantage of new opportunities and tackle
  major challenges. There are a lot of people in the Che community doing some very innovative work
  with the technology and we&rsquo;re constantly making improvements.</p>
<p>
  To get started, visit our <a href="https://www.eclipse.org/che/">website</a>. You&rsquo;ll find an
  overview of Che 7, our newest and biggest release ever, along with detailed information about
  using Che and the links you need to get started.If you want to try it right away, sign up to use
  the <a href="https://che.openshift.io/dashboard/">free Eclipse Che Software as a Service (SaaS) that Red Hat hosts</a>
  on our OpenShift Kubernetes platform.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/march/images/brad.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>