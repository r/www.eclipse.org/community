<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2020.19.03</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/thabang.png" />
                          <h3>Editor's Note</h3>

                        <p>
                          When we announced the Eclipse Cloud Development Tools Working Group last
                          fall, our Executive Director, Mike Milinkovich,<a
                            href="https://blogs.eclipse.org/post/mike-milinkovich/re-defining-cloud-development-tools"
                          > noted in a blog post</a> that he hadn&rsquo;t seen an initiative with
                          this level of potential impact since the Eclipse Foundation was created in
                          2004.
                        </p>
                        <p>Less than six months later, several Eclipse projects have exciting updates to share.
                          The progress and innovations you&rsquo;ll read about in this month&rsquo;s
                          newsletter truly validate the value of an open source, vendor-neutral, and
                          collaborative approach to cloud development tools.</p>
                        <h2>Spotlight Articles</h2>
                        <p>Our spotlight articles focus on four Eclipse projects that
                          enable faster, easier, more flexible cloud-based development:</p>
                        <ul>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/4.php">Eclipse Theia</a>. Sven Efftinge reviews the key features in
                            the March 31 Theia 1.0 release and the big-name companies that are
                            already benefiting from Theia&rsquo;s single-source approach to desktop
                            and cloud IDE development.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/3.php">Eclipse Che</a>. Brad Micklea shares five Che features that
                            will make you want to take another look at this Kubernetes-native
                            developer platform.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/2.php">Eclipse Dirigible</a>. Nedelcho Delchev explains why
                            developers will want to switch to an open source, low-code and no-code
                            cloud-based development platform.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php">Eclipse Open VSX</a>. Miro Sponemann and Sven Efftinge
                            explore the need for, and benefits of, a free marketplace for Virtual
                            Studio Code extensions.</li>
                        </ul>
                        <h2>Short Takes</h2>
                        <p>
                          We also want to point you to some recent blogs by Jonas Helming and the
                          team at<a href="https://eclipsesource.com/"> EclipseSource</a>.
                          They&rsquo;re very interesting, informative, and you&rsquo;ll learn about
                          a couple of Eclipse projects you may not know about:
                        </p>
                        <ul>
                          <li><a
                            href="https://eclipsesource.com/blogs/2019/02/25/strategies-towards-web-cloud-based-tools-and-ides/"
                          >Strategies Towards Web/Cloud-Based Tools and IDEs</a></li>
                          <li><a
                            href="https://eclipsesource.com/blogs/2019/12/16/introducing-emf-cloud/"
                          >Introducing EMF.cloud</a></li>
                          <li><a
                            href="https://eclipsesource.com/blogs/2019/11/04/introducing-the-graphical-language-server-protocol-platform-eclipse-glsp/"
                          >Introducing the Graphical Language Server Protocol / Platform (Eclipse
                              GLSP)</a></li>
                          <li><a
                            href="https://eclipsesource.com/blogs/2019/12/06/the-eclipse-theia-ide-vs-vs-code/"
                          >The Eclipse Theia IDE vs. VS Code</a></li>
                          <li><a
                            href="https://eclipsesource.com/blogs/2018/12/03/eclipse-che-vs-eclipse-theia/"
                          >Eclipse Che vs. Eclipse Theia</a></li>
                        </ul>

                        <h2 dir="ltr">Eclipse IDE 2020-03 Release</h2>
                        <p>
                          The Eclipse IDE 2020-03 release is <a
                            href="https://www.eclipse.org/eclipseide/2020-03/"
                          >here</a> and available for download now! To learn more about the key
                          features and improvements that make this version better than ever, explore
                          the <a href="https://www.eclipse.org/eclipseide/2020-03/noteworthy/">2020-03
                            New and Noteworthy</a> summary. Thank you to all the committers, contributors,
                            projects, and Eclipse Foundation staff who made this possible!
                        </p>
                        <p>
                          To make sure the Eclipse IDE keeps getting better and stays 100% free,
                          please donate to the IDE here: <a
                            href="https://www.eclipse.org/donate/ide/"
                          >https://www.eclipse.org/donate/ide/</a>
                        </p>
                        <h2>The Latest Happenings</h2>
                        <p>As always, be sure to check our update lists so you can stay
                          current with the latest happenings here at the Eclipse Foundation:</p>
                        <ul>
                          <li><a href="#new_project_proposals">New Project Proposals</a></li>
                          <li><a href="#new_project_releases">New Project Releases</a></li>
                          <li><a href="#upcoming_events">Upcoming Eclipse Events</a></li>
                        </ul>
                        <p>
                          Happy Reading!<br /> Thabang Mashologu
                        </p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/1.php"><h3>Eclipse Open VSX Registry Is a Free Marketplace for VS Code Extensions
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/2.php"><h3>Why Eclipse Dirigible Is Ideal for Cloud-Based Development</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/3.php"><h3>Five Things You Might Not Know About Eclipse Che</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/4.php"><h3>Eclipse Theia Is Ready for Primetime</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/march/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/march/5.php"><h3>News</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_proposals" style="margin-bottom:10px;">New Project Proposals</h3>
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-grace">Eclipse Grace</a> is a web application based on the Eclipse Memory Analyser Tooling (MAT) that provides HTTP services so that users can view the heap dump files analysis through a browser.&nbsp;</li>
                              <li>The <a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-oneofour">Eclipse OneOFour</a> project provides an implementation of the protocol IEC 60870-5-104 as a Java library with master and slave functionality over TCP connections between SCADA systems.</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-unified-identity">Eclipse Unified Identity</a> provides a toolkit to foster the adoption of the Unified Identity Protocol (UIP), an implementation of the Self Sovereign Identity (SSI) concept based on the IOTA Distributed LedgerTechnology (DLT).</li>
                              <li>The <a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-decentralized-marketplaces">Eclipse Decentralized Marketplaces</a> project provides a foundation for developers to build decentralized marketplaces on the IOTA network, allowing them to prototype concepts and explore new decentralized business models.</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a href="/projects/project_activity.php">Read on!</a></p>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_releases" style="margin-bottom:10px;">New Project Releases</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.passage">Eclipse Passage 0.8.0 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/science.swtchart">Eclipse SWTChart 0.12.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.adore">Eclipse Automated Driving Open Research (ADORe) Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.mdmbl">Eclipse MDM|BL 5.1.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.jaf">Jakarta Activation 2.0 Plan</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.ejb">Jakarta Enterprise Beans 4.0 Plan</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.emft.emf-client">Eclipse EMF Client Platform 1.24.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.emft.emf-facet">Eclipse EMF Facet Termination&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/science.statet">Eclipse StatET: Tooling for the R language 4.1.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.reddeer">Eclipse RedDeer 2.9.0 Release&nbsp;</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/rt.vertx">Eclipse Vert.x&nbsp;&nbsp;&nbsp; 3.9.0 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.sumo">Eclipse SUMO 1.5.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.streamsheets">Eclipse Streamsheets&nbsp;&nbsp;&nbsp; Creation&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.tmf.xtext">Eclipse Xtext 2.21.0 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.jaxb">Jakarta XML Binding&nbsp;&nbsp;&nbsp; 2.3.3 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.jaxws">Jakarta XML Web Services 2.3.3 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.tyrus">Eclipse Tyrus 1.16 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.jaf">Jakarta Activation 1.2.2 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.jersey">Eclipse Jersey&nbsp; 2.30 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.emf.mwe">Eclipse Modeling Workflow Engine 2.11.2 Release&nbsp;&nbsp;&nbsp; </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.fog05">Eclipse fog05 0.1.0 (Prelude) Release</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0"
                        cellspacing="0"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 id="upcoming_events">Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>

                            <p>Upcoming industry events and workshops where our members will play a key role. Hope to see you there!</p>

<p><a style="color:#000;" href="https://www.redhat.com/en/summit">Red Hat Summit</a><br />
Apr 28-29 | Virtual Event</p>

<p><a style="color:#000;" href="https://www.ibm.com/events/think/">IBM Think</a><br />
May 5-7 | Virtual Event</p>

<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 id="contact_us" style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->