<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  After three years of hard work and several high-profile production deployments,<a
    href="https://theia-ide.org/"
  > Eclipse Theia</a> 1.0 will be released on March 31. The technology is mature, stable, and ready
  for anyone and everyone to use as a foundation for their custom cloud or desktop integrated
  development environment (IDE).
</p>
<p>
  The framework that Theia provides is the natural evolution of open source desktop IDE platforms,
  such as the<a href="https://www.eclipse.org/eclipseide/"> Eclipse IDE</a>. With Theia,
  corporations and IDE vendors have a single, modern technology stack that can be used to build
  development environments for desktops and browsers. And they don&rsquo;t need to make that
  decision upfront.
</p>
<h2>Flexibility for Today and Tomorrow</h2>
<p>
  Theia gives the global community a next-generation IDE platform that&rsquo;s ideal for meeting
  today&rsquo;s needs and for developing future solutions. Because it&rsquo;s open source,
  vendor-neutral, and follows the<a href="https://www.eclipse.org/org/documents/"> Eclipse
    Foundation governance model</a>, you never have to worry about being tied to a particular
  vendor&rsquo;s business objectives or roadmap.
</p>
<p>The potential ways of leveraging Theia are as varied as the organizations that adopt
  it. Adopters can build commercial tools for resale as well as internal tools that meet specific
  engineering requirements.</p>
<p>We have a number of adopters who start by using Theia to build a desktop IDE, but
  don&rsquo;t know when, or even whether, they&rsquo;ll need a cloud IDE. But, they know they can
  make that move at any time without having to rewrite the source code. Another major corporation
  that&rsquo;s adopted Theia has developed a desktop IDE and a cloud version, but are using the
  cloud IDE as a playground for experimenting. The long-term goal is to fully replace the Theia
  desktop IDE with the cloud version.</p>
<p>The images below illustrate Theia&rsquo;s architectural flexibility.</p>
<p>
  <img src="images/4_1.png"/>
</p>
<p>&nbsp;
<p>
  <img src="images/4_2.png"/>
</p>
</p>
<p>
  <img src="images/4_3.png"/>
</p>
<h2>Proven in Numerous Real-World Scenarios</h2>
<p>Many big-name companies already rely on Theia as the foundational building block for their IDEs.</p>

<p>For example, it’s used in Google Cloud Shell and Arduino’s new Pro IDE. SAP, Arm, and IBM have also built highly
customized IDEs on top of the Theia platform. Theia has also replaced the IDE in Eclipse Che and is driving <a href="https://www.gitpod.io/blog/continuous-dev-environment-in-devops/">Gitpod’s
 continuous development environments</a>.</p>


<h2>Support for Visual Studio Code Extensions</h2>
<p>Eclipse Theia is the first cloud IDE to support VS Code extensions and bring the same
  capabilities and features that local desktop IDEs offer into the cloud.</p>
<p>This is a significant step forward from first-generation cloud IDEs that struggled to
  provide state-of-the-art tools support. With these early, sandbox-like IDEs, each company had to
  develop its own extensions for the IDE &mdash; if extensions were supported at all. Developing
  these extensions requires a huge amount of time, programming effort, and money. And it was
  replicated across companies.</p>
<p>As an open source community, we can spread the extension development effort across many
  contributors and collectively benefit from the results.</p>
<p>
  Theia 1.0 will use the<a href="https://projects.eclipse.org/proposals/eclipse-open-vsx-registry">
    Eclipse Open VSX Registry</a> as its default registry. The extensions available in this free
  marketplace for VS Code extensions can be used in VS Code as well as in Theia. For additional
  insight into the rationale behind the registry and its benefits, <a href="1.php">read the
    accompanying article</a> in this month&rsquo;s newsletter.
</p>

<h2>Get Started With Theia</h2>
<p>We invite all users, adopters, and contributors to <a href="https://gitpod.io/#https://github.com/eclipse-theia/theia">try Theia</a> and to build
  extensions. Getting started is extremely easy. As with Gitpod, you can actually use Theia to develop Theia, which is really cool and super-convenient. You can find the Theia files in <a href="https://github.com/eclipse-theia/theia">GitHub</a>.</p>
<p>
  Finally, here’s a screenshot of the Theia user interface to show you what you can expect.
</p>

<p>
  <img src="images/4_4.png"/>
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/march/images/sven.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>