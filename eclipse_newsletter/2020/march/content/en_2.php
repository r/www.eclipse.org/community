<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p dir="ltr">
  <a href="/community/eclipse_newsletter/2018/february/dirigible.php">Eclipse
  Dirigible</a> is a leading open source cloud-based low-code and no-code development platform.
  And it includes a number of high-productivity tools and user-friendly design features that aren&rsquo;t
  available in on-premises integrated development environments (IDEs) or other cloud-based IDEs.
</p>
<p dir="ltr">We&rsquo;ve put such a strong focus on the development experience because we recognize
  this is what&rsquo;s needed to encourage developers to make the move from the on-premises IDEs
  they&rsquo;re so familiar with to cloud development tools.</p>
<p dir="ltr">If we simply provided a general-purpose IDE in a browser, there would be little
  incentive for developers to switch to a cloud-based development environment. Local IDEs would
  always deliver higher performance and better integration with operating systems than the
  browser-based version.</p>
<p dir="ltr">As a result, we&rsquo;ve spent a lot of time and effort ensuring Eclipse Dirigible
  provides added value for developers with features and functionality that are a natural fit and
  beneficial for cloud development.</p>
<p dir="ltr">Before I describe the latest and greatest advances in Dirigible, here&rsquo;s a look at
  the foundational elements that make it the ideal platform for cloud-based development.</p>
<h2 dir="ltr">Dirigible Is an All-In-One Web IDE and Runtime</h2>
<p>I&rsquo;ll start by clearing up one of the biggest misconceptions about Eclipse Dirigible.
  It&rsquo;s not just another web IDE. Dirigible seamlessly combines a web IDE with an integrated
  runtime, as shown in the platform illustration below.</p>
<p>&nbsp;
<p dir="ltr">
  <img src="images/2_1.png"/>
</p>
</p>
<p dir="ltr">The self-contained runtime environment runs in the cloud so there are no additional
  downloads or installations. Integrated combinations of pre-selected engines ensure the two aspects
  of Dirigible work together. You can execute your application code in the runtime through the web IDE, or you can use
  the runtime on its own for production purposes.</p>
<p dir="ltr">This integrated approach to application development and production is a significant
  advantage if you&rsquo;re used to working with standalone IDEs, whether they&rsquo;re local or
  cloud-based. It means you&rsquo;re no longer forced to switch tools or simulate a local
  environment when it&rsquo;s time to produce the production-ready artifacts to be deployed. Using a
  single system also avoids the unexpected behaviors and issues that can occur when you develop
  locally and deploy in the cloud.</p>
<h2 dir="ltr">Live System Development Delivers Immediate Results</h2>
<p dir="ltr">Like all cloud-based development environments, Eclipse Dirigible offers fast and easy
  startup. You don&rsquo;t need to spend half an hour installing an entire IDE. You can simply go to
  the website and everything you need is already there &mdash; plugins, integrations, even data
  integrations.</p>
<p dir="ltr">Dirigible takes the convenience of web development one step further. With the unique
  Dirigible system development model, you can develop on a live system. If you have a system already
  running in the cloud, you can access it and start developing in it. You simply write code, click
  save, and you can immediately see the results. This dramatically shortens development time.</p>
<h2 dir="ltr">Features Focus on Business Application Development</h2>
<p dir="ltr">We&rsquo;ve also implemented a number of features that are ideal for developing
  business applications.</p>
<p dir="ltr">Because business applications are used in real-world business processes, they must be
  very dynamic. Business processes change constantly, and applications need to keep pace.</p>
<p dir="ltr">To ensure developers can quickly adapt applications to align with changing business
  processes, Dirigible enables easy application extensibility and fast database access. The
  low-code/no-code approach we&rsquo;ve taken also accelerates business application development.</p>
<p dir="ltr">In addition, Dirigible supports a full-stack development process so you can create
  everything you need within the Dirigible development environment.</p>
<h2 dir="ltr">A Stable API Simplifies Development</h2>
<p dir="ltr">A stable API is essential when you&rsquo;re frequently evolving business applications.
  While there are numerous well-known stable APIs in very established technologies, such as Java,
  that&rsquo;s not the case in the cloud right now.</p>
<p dir="ltr">To simplify development, Dirigible provides what we call Enterprise JavaScript, which
  is a subset of stable Java APIs we&rsquo;ve created to lower the barrier of entry to cloud
  development. Here&rsquo;s a brief example of a request/response API.</p>
<p dir="ltr">
  <img src="images/2_2.png"/>
</p>
<h2 dir="ltr">New Models Jumpstart Development</h2>
<p dir="ltr">Recently, we added two new models and approaches to Eclipse Dirigible to make it even
  faster and easier for developers to generate complete business applications:</p>
<ul dir="ltr">
  <li>A domain model lets you start with business objects then generate the application.</li>
  <li>A process model lets you start with business processes then apply them to business
    objects.&nbsp;</li>
</ul>
<p dir="ltr">In just five or six minutes, you can model two or three entities for an application,
  generate them, and immediately start entering data to see how they function and behave. The two
  models can also be used together.</p>
<p dir="ltr">Afterwards, you can write custom extensions that enhance the overall applications. You
  can also embed custom views and third-party frameworks, such as Google Maps, for really
  complex applications.</p>
<h2 dir="ltr">Customization Capabilities Increase Flexibility</h2>
<p dir="ltr">Models are a great way to very quickly and easily develop 80 percent of a business
  application. But, the other 20 percent of the application is also very important. Many developers
  won&rsquo;t use a development environment unless it provides the ability to tailor that final 20
  percent of the application.</p>
<p dir="ltr">While many low-code/no-code development platforms limit the way you can customize the
  application, we&rsquo;ve taken the opposite approach. With Eclipse Dirigible, you have complete
  flexibility to add new templates and generators, and to write and include your own custom code.</p>
<h2 dir="ltr">Integration With Eclipse Che Brings Devops to a Kubernetes Environment</h2>
<p dir="ltr">
  Finally, in an excellent example of the benefits of collaborating in and across Eclipse
  communities, we&rsquo;ve integrated Dirigible into<a href="/che/"> Eclipse
    Che</a>.
</p>
<p dir="ltr">
  The integration with Che enables Dirigible to provide devops processes for application management
  in a containerized Kubernetes environment. Dirigible is embedded in Che workspaces, similar to the
  way<a href="https://theia-ide.org/"> Eclipse Theia</a> is embedded.
</p>
<p dir="ltr">Interestingly, we completed the integration over just two or three days while we were
  all together during EclipseCon Europe in Ludwigsburg last October! It was an incredible
  collaborative experience and a great opportunity to work together face-to-face with the broader
  Eclipse Cloud Development (ECD) community.</p>
<p dir="ltr">Here&rsquo;s a look at Eclipse Dirigible&rsquo;s predefined application templates in
  the integrated user interface.</p>
<p dir="ltr">
  <img src="images/2_3.png"/>
</p>
<h2 dir="ltr">We&rsquo;re Looking for Partners, Customers, and Developers</h2>
<p dir="ltr">Because Eclipse Dirigible is a mature project, we&rsquo;re actively seeking university
  and business partners, as well as customers, so we can collaborate with them and demonstrate
  what&rsquo;s possible with Eclipse Dirigible. Of course, we also welcome all developers who are
  interested in joining the project.</p>
<p dir="ltr">
  We encourage everyone with an interest in cloud development to take a closer look at Eclipse
  Dirigible. We have a<a href="https://www.dirigible.io/"> comprehensive website</a> with
  documentation, links to helpful videos, and other information.
</p>
<p dir="ltr">
  While you&rsquo;re on the site, be sure to click<a
    href="http://trial-dirigible.ingress.aws.promart.shoot.canary.k8s-hana.ondemand.com/index.html"
  > TRY IT OUT</a> so you can go directly to our trial package and start experimenting.
</p>
<p>
  If you would like to join our mailing list, click<a
    href="https://accounts.eclipse.org/mailing-list/dirigible-dev"
  > here</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/march/images/nedelcho.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>