<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>According to the 2019 Stack Overflow developer survey, Visual Studio (VS) Code is used
  by more than 50&nbsp;percent of all developers worldwide. Its extension protocol has enabled the
  broader developer community to create more than 16,000 extensions supporting all major programming
  languages, frameworks, and tools used in modern software projects. While large parts of VS Code
  are developed as open source, the actual product is not open source.</p>
<h2>Vendor-Neutral Alternatives to VS Code Are Available</h2>
<p>
  <a href="http://theia-ide.org">Eclipse Theia</a>, the integrated developer environment (IDE) that
  powers Gitpod, Google Cloud Shell, and others, offers a vendor-neutral alternative to VS Code and
  is based on parts of the same open-source codebase as VS Code.
</p>
<p>
  Theia works as an Electron-based desktop app, and it runs in browsers. It also features a highly
  flexible architecture that allows extensive customizations.<a
    href="https://www.arduino.cc/pro/arduino-pro-ide"
  > Arduino&rsquo;s new Pro IDE</a> and<a href="https://os.mbed.com/studio/"> Arm&rsquo;s Mbed
    Studio</a> are great examples of Theia&rsquo;s flexibility. In addition to this built-in
  flexibility, Theia can also run VS Code extensions.
</p>
<p>
  Another community-driven project called<a href="https://github.com/VSCodium/vscodium"> VSCodium</a>
  offers a 100 percent open-source version of VS Code.
</p>
<h2>Only Visual Studio Products Can Access the Extension Marketplace</h2>
<p>While all of the projects listed above support VS Code extensions, only Microsoft
  products can use and connect to Microsoft&rsquo;s Extension Marketplace. The terms of use for the
  Marketplace prevent any non-Visual Studio products from accessing it.</p>
<p>Gitpod employs a workaround where users upload .vsix files to install extensions. This
  causes unnecessary overhead as users have to download the files from GitHub, then upload them to
  Gitpod. Downloading extensions from the Microsoft Marketplace for any use other than in Microsoft
  products is prohibited as well.</p>
<p>Most extensions are developed by communities and published under permissive open source
  licenses. The requirement to distribute and access these community-owned extensions in a system
  with such restrictive terms of service does not seem right.</p>
<p>
  Our goal is to resolve this issue by hosting an open source extension registry at the Eclipse
  Foundation, a vendor-neutral organization. We&rsquo;re doing this through the<a
    href="https://github.com/eclipse/openvsx"
  > Eclipse Open VSX Registry</a> project.
</p>
<h2>You Can Host Your Own Registry With Eclipse Open VSX Registry</h2>
<p>In addition to providing a publicly hosted, vendor-neutral extension registry, the
  Eclipse Open VSX project also makes the code available as open source. This way everyone
  can install their own registry and use it, for example, on a corporate network.</p>
<p>You&rsquo;re not forced to publish proprietary extensions to a public marketplace, but
  you still maintain full control over their availability. This is very similar to common practices
  in other ecosystems such as npm, Cargo, and Maven.</p>
<p>The illustration below shows the Open VSX Registry.</p>
<p>
  <img src="/community/eclipse_newsletter/2020/march/images/1_1.png"/>
</p>
<h2>Using the Eclipse Open VSX</h2>
<p>Open VSX contains the registry server, a web interface, and a command line
  interface (CLI) to simplify publishing.</p>
<p>
  The publicly hosted instance of the registry is available<a href="about:blank"> here</a>. The
  upcoming Theia version 1.0 release will use this installation as its default registry.
</p>
<p>Because Microsoft&rsquo;s VS Code Extension Marketplace API is proprietary and
  undocumented, the Open VSX project has defined its own API. An adapter between the two
  APIs is planned and will be required to connect VS Code and VSCodium to the Open VSX Registry.</p>
<h2>Four Easy Steps to Publish Extensions</h2>
<p>We would love to see extension developers publish to our registry in addition to
  Microsoft&rsquo;s. To encourage publishing, we developed a CLI that makes publishing very easy.
  The four basic steps to publish extensions are:</p>
<ol>
  <li>Register on open-vsx.org through<a href="https://open-vsx.org/oauth2/authorization/github">
      GitHub OAuth</a>.
  </li>
  <li><a href="https://open-vsx.org/user-settings/tokens">Create an access token</a> and copy it.</li>
  <li>Run <code>npx ovsx create-namespace &lt;publisher&gt; --pat &lt;token&gt;</code> with the publisher name
    specified in the extension&rsquo;s package.json.</li>
  <li>Run <code>npx ovsx publish --pat &lt;token&gt;</code> in the directory of the extension you want to
    publish.</li>
</ol>
<h2>We&rsquo;re Seeding the Registry</h2>
<p>
  Until extension developers start publishing to open-vsx.org themselves, we will publish open
  source extensions through a script maintained in a<a
    href="https://github.com/open-vsx/publish-extensions"
  > GitHub repository</a>. A continuous integration build regularly runs through the list of changes
  and automatically deploys all listed projects.
</p>
<p>If you&rsquo;re missing a particular extension:</p>
<ol>
  <li>Ask the original maintainer to publish to open-vsx.org, pointing them to this article for
    context.</li>
  <li>If the maintainer does not respond, create a pull request<a
    href="https://github.com/open-vsx/publish-extensions"
  > here</a>.
  </li>
</ol>
<h2>Open Source Should Remain Open Source</h2>
<p>The VS Code team is doing a great job building a developer experience that is loved and
  used by millions of developers. The large ecosystem of extensions is unique and provides
  state-of-the-art tool support for all modern programming languages, frameworks, and tools.</p>
<p>However, we believe that assets released as open source should remain readily available
  to everyone. A vendor-neutral, community-driven marketplace for VS Code extensions at the Eclipse
  Foundation is an important first step to make this happen.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/march/images/miro.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>