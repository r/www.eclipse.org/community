<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Traceability Management Using Eclipse Capra for Safety and Security Assessments";
  $pageKeywords = "eclipse, newsletter, Traceability, Traceability Management, Eclipse Capra, Safety, Security, Security Assessments";
//   $pageAuthor = "Reza Rahman";
  $pageDescription = "Traceability between different development artifacts is an essential requirement in safety and security standards such as ISO 26262, ARP 4754, IEC 62304, IEC 62443, and the upcoming ISO/SAE 21434 standard.";

  $pageAuthorArray[] = array(
    'name' => 'Jan Steffen Becker',
    'bio' => 'Jan Steffen Becker is a researcher in the Safety and Security Oriented Design Methods and Processes group in the Transportation division of OFFIS e.V. in Oldenburg, Germany. His primary research is on formal methods for design of safety-critical embedded software.',
    'img' => 'images/jan_steffen.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Björn Koopmann',
    'bio' => 'Björn Koopmann is a researcher in the Safety and Security Oriented Analysis group in the Transportation division of OFFIS e.V. in Oldenburg, Germany. He focuses on design and analysis methods for continuous and consistent handling of safety and real-time properties in industrial design processes.',
    'img' => 'images/bjorn_koopmann.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'David Schmelter',
    'bio' => 'David Schmelter is a researcher in the Software Engineering and IT Security department at the Fraunhofer Institute for Mechatronic Systems Design in Paderborn, Germany. His primary research areas are search-based model-driven engineering (MDE) in early design phases of cyber-physical systems as well as data security in collaborative engineering.',
    'img' => 'images/david_schmelter.png'
  );

  $pageAuthorArray[] = array(
      'name' => 'Jan-Philipp Steghöfer',
      'bio' => 'Jan-Philipp Steghöfer is an associate professor in the Software Engineering Division of Chalmers University of Technology and the University of Gothenburg in Germany. He studies software traceability in all of its facets and is one of the driving forces behind Eclipse Capra.',
      'img' => 'images/jan_philipp.png'
  );

  $pageAuthorArray[] = array(
      'name' => 'Marc Zeller',
      'bio' => 'Marc Zeller works as senior key expert for model-based reliability and safety engineering at Siemens AG. His research interests are focused on the efficient and effective development of dependability-relevant cyber-physical systems using model-based engineering techniques.',
      'img' => 'images/marc_zeller.png'
  );

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");