<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Eclipse APP4MC: Open Source Tools for Performance Analysis of Heterogeneous Embedded Systems";
  $pageKeywords = "eclipse, newsletter, APP4MC, Open Source, tools, Performance Analysis, Heterogeneous, Embedded Systems";
//   $pageAuthor = "Ralph Soika";
  $pageDescription = "Eclipse APP4MC is an open platform for model-based performance analysis and optimization of heterogenous embedded hardware and software systems. The platform allows developers to create and manage complex toolchains, including simulation and validation. It has been used by Bosch and its partners in the automotive sector and is proven to provide interoperability and extensibility, and to unify data exchanges in cross-organizational projects.";


  $pageAuthorArray[] = array(
    'name' => 'Mahmoud Bazzal',
    'bio' => 'Mahmoud Bazzal is an M.Eng student and research assistant at the Institute for Digital Transformation of Application and Living Domains at Dortmund University of Applied Sciences in Germany. His research focuses on real-time calculus and modular timing analysis of real-time systems. Mahmoud is involved in the PANORAMA European research project, and also participated in the Google Summer of Code (GSoC) 2019 with the Eclipse Foundation.',
    'img' => 'images/mahmoud_bazzal.jpg'
  );

  $pageAuthorArray[] = array(
    'name' => 'Benjamin Beichler',
    'bio' => 'Benjamin Beichler holds an M.Sc. degree in Information Technology and Computer Engineering from the University of Rostock, Germany, and is currently a researcher at the Institute of Applied Microelectronics and Computer Engineering at the University of Rostock. Benjamin focuses on model-based embedded system design and virtual prototyping, and is work package 3 co-leader for dynamic analysis methods for the PANORAMA European research project.',
    'img' => 'images/benjamin_beichler.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Dirk Fauth',
    'bio' => 'Dirk Fauth is a research engineer at Robert Bosch GmbH in Stuttgart, Germany. He actively develops, teaches, and speaks about Eclipse Rich Client Platform (RCP) applications, Eclipse IDE-related technologies, and Open Services Gateway initiative (OSGi) technologies in general.',
    'img' => 'images/dirk_fauth.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Lukas Krawczyk',
    'bio' => 'Lukas Krawczyk is a Ph.D. student and computer science researcher at the Institute for the Digital Transformation of Application and Living Domains at Dortmund University of Applied Sciences. His research focuses on analyzing timing behavior and optimizing deployment of automotive software for embedded multi- and many-core systems. Lukas has been involved in a number of ITEA projects, including AMALTHEA, AMALTHEA4public, APPSTACLE, and PANORAMA.',
    'img' => 'images/lukas_krawczyk.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Harald Mackamul',
    'bio' => 'Harald Mackamul is a senior expert at Bosch Corporate Research and currently focuses on development of embedded multicore systems. Harald was the technical project lead, responsible for implementation and integration of the AMALTHEA platform in the AMALTHEA and AMALTHEA4public European research projects. He is project lead and committer for Eclipse APP4MC and work package 1 leader for the PANORAMA European research project.',
    'img' => 'images/harald_mackamul.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Joerg Tessmer',
    'bio' => 'Joerg Tessmer is a project manager in the Cross Automotive Platform division at Robert Bosch GmbH. He has more than 15 years of experience in model-based design approaches for embedded software engineering. Joerg is the project lead for several European research projects in the automotive domain related to resource optimization of embedded systems and connected vehicle ecosystems. He currently leads the PANORAMA European research project.',
    'img' => 'images/joerg_tessmer.png'
  );

  $pageAuthorArray[] = array(
    'name' => 'Raphael Weber',
    'bio' => 'Raphael Weber is a senior research engineer at Vector Informatik GmbH in Regensburg, Germany. He supports the solution engineering team in designing and discovering new concepts, particularly in the field of multi-core timing analysis and simulation, optimization, and modeling. Raphael has been actively involved in several public research projects. He currently works on the PANORAMA European research project and contributes to Eclipse APP4MC.',
    'img' => 'images/raphael_weber.png'
  );

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");