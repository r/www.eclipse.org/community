<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Traceability between different development artifacts is an essential requirement in safety and security standards such as ISO 26262, ARP 4754, IEC 62304, IEC 62443, and the upcoming ISO/SAE 21434 standard.</p>

<p>&nbsp;<a href="https://projects.eclipse.org/proposals/capra">Eclipse Capra</a> helps engineers create, visualize, and manage trace links from an IDE. It supports many of the most common artifact types directly, and can use trace links that are annotated in code as well as those that it stores in its own database. The Eclipse Capra technology is one of the results of the ITEA-funded European research project, AMALTHEA4public.</p>

<p>In this article, we introduce Eclipse Capra and its capabilities, illustrating them with an example from the automotive domain. However, before we describe Eclipse Capra in more detail, here&rsquo;s a closer look at the challenges associated with developing safety- and security-critical systems.</p>

<h2>Safety- and Security-Critical Systems Are Complex, Multivendor Development Projects</h2>

<p>Standards, such as those listed above, imply that:</p>

<ul>
	<li>Requirements must be assigned to components from the functional architecture</li>
	<li>Dependencies between requirements must be tracked</li>
	<li>Safety- and security-relevant aspects must be tracked from hazard or threat identification through to implementation and verification of the safety or security mechanisms used</li>
</ul>

<p>To create the final product in safety- and security-critical domains, such as automotive, avionics, and medical devices, heterogeneous embedded systems are built by different suppliers and integrated by original equipment manufacturers (OEMs). The collaborative systems engineering processes necessary for such products create new traceability challenges because each participant in this complex value chain uses different processes, methods, and tools.</p>

<p>These systems are already the subject of research projects, such as PANORAMA. In addition, organizations are working to create tooling and methods to address the need for traceability.</p>

<p>One recurring challenge is the need to connect artifacts from different tools, including commercial tools used for modeling, requirements management, and analysis, and the open source tools that complement these commercial tools.</p>

<p>Current approaches are often based on manually created documents that are time-consuming and error-prone to maintain. In contrast,&nbsp;<a href="https://eclipse.org/capra">Eclipse Capra</a> offers customizable and extensible traceability management right in the IDE.</p>

<h2>All Safety-Related Claims Must Be Proven With Evidence</h2>

<p>When developing a safety-critical system, engineers must be able to demonstrate that all safety requirements have been fulfilled. That means a mitigation strategy must be defined for each identified hazard, or rather, for the safety requirements derived from it.</p>

<p>A safety or assurance case demonstrates there is evidence for every safety-related claim and that the mitigation strategies are effective in addressing the hazard. The evidence can be process-related, demonstrating, for example, that code reviews ensured code quality. Alternatively, the evidence can be artifact-related, demonstrating that all safety requirements have been tested. Artifact-related evidence can be collected using trace links between development artifacts.</p>

<p>Similar considerations are necessary in systems that are vulnerable to cybersecurity attacks. For example, the IEC 62443 standard requires security design and implementation reviews, among other requirements. Upcoming standards, such as ISO/SAE 21434 for cybersecurity engineering in road vehicles, demand the creation of cybersecurity cases, which are similar in structure to safety assurance cases.</p>

<h2>Trace Links Support Safety and Security Claims</h2>

<p>Trace links strongly support the argumentation of safety and security claims during engineering reviews and assessments.</p>

<p>A trace link connects two artifacts and denotes the relationship between them. Typical examples of trace links that are relevant for safety assurance cases are those between requirements and test cases.</p>

<p>For example, developers might leave issue identifiers in the test code, or they might create trace links between code and the corresponding tests using a specialized tool for storage in a model or database. In both cases, trace links should be accessible for analysis, or for visualization to show that all requirements are tested in a traceability matrix. Such matrices can be used in assurance cases as evidence that safety or security requirements have been validated.</p>

<p>In addition, when analyzing failures or security incidents during operation, trace links support impact analysis, such as identification of affected system components.</p>

<h2>Eclipse Capra Simplifies Traceability</h2>

<p>Out of the box, Eclipse Capra supports requirements in ReqIF, Microsoft Excel, and Google Spreadsheets. It can create trace links to Java, C, C++, and PHP code. It can also create and manage trace links to&nbsp;<a href="https://www.eclipse.org/papyrus">Eclipse Papyrus</a> models, FeatureIDE feature models,&nbsp;<a href="https://www.eclipse.org/app4mc">Eclipse APP4MC</a> models, and many others.</p>

<p>Generic handlers for&nbsp;<a href="https://www.eclipse.org/modeling/emf/">Eclipse Modeling Framework (EMF)</a> models and arbitrary files provide additional interoperability. Importantly, Eclipse Capra can easily be extended to support additional artifact types. Together, these capabilities mean Eclipse Capra can create and manage trace links between artifacts created and maintained in commercial and open source tools (Figure 1).</p>

<p><span style="font-size:12px">Figure 1: A Selection of Artifacts Eclipse Capra Can Trace to</span></p>
<img class="img img-responsive" src="images/article2_figure_1.png" alt="Figure 1: A Selection of Artifacts Eclipse Capra Can Trace to">

<p>In terms of visualizations, Eclipse Capra supports a variety of graph types as well as a matrix view. All visualizations are configurable to ensure engineers see the links that are relevant to them.</p>

<p>Eclipse Capra&rsquo;s main strength is its customizability. Adding new handlers to support additional artifact types usually only requires a few lines of code. Creating custom traceability information models that define which trace types exist and how they relate to the artifacts is easily done with an&nbsp;<a href="https://wiki.eclipse.org/Xcore">Eclipse Xcore</a> model. More advanced customizations, such as changing the way Eclipse Capra stores its trace model, are also possible.</p>

<h2>Relationships Among Artifacts Are Illustrated</h2>

<p>To illustrate Eclipse Capra&rsquo;s capabilities, Figure 2 shows how the software is used to trace between the different artifacts in a demonstration we&rsquo;re currently working on in the context of the PANORAMA European research project. In the demonstration, we&rsquo;re using a realistic prototypical example of an automated driving application that includes a number of relevant artifacts for safety analysis, including:</p>

<ul>
	<li>A set of functional requirements</li>
	<li>An initial component model</li>
	<li>A list of hazards, as well as a set of safety goals and requirements</li>
</ul>

<p><span style="font-size:12px">Figure 2: Traceability Between Different Artifacts With Eclipse Capra</span></p>
<img class="img img-responsive" src="images/article2_figure_2.png" alt="Figure 2: Traceability Between Different Artifacts With Eclipse Capra">
<p>The traceability graph drawn by Eclipse Capra illustrates how artifacts created in different commercial and open source tools are connected by traces. In this case, a safety claim modeled in Open Dependability Exchange (ODE) is traced to a hazard described in Microsoft Excel and connected to a safety goal and a functional requirement, both in Microsoft Excel. The functional requirement is traced to a component modeled in Eclipse Papyrus and the component is traced to a Runnable in Eclipse APP4MC.</p>

<p>The system modeled in Eclipse APP4MC is intended to calculate proper throttle, steering, and brake signals to follow a map of predetermined waypoints. In addition, a high-priority task to brake and perform emergency maneuvers to avoid obstacles is included.</p>

<p>Based on the assumptions of operation in urban areas and implementation of level 3 automation, we performed a hazard analysis and risk assessment according to ISO 26262 to determine the corresponding automotive safety integrity levels. In addition, we performed an initial safety analysis that includes a failure modes and effects analysis (FMEA) based on the component model and the derived safety requirements.</p>

<p>We also created a custom traceability information model to support our goal of managing traces for model-based safety assessments.</p>

<p>We&rsquo;re currently working on supporting engineers who are constructing safety and security assurance cases and semi-automatic execution of change impact analyses. This includes model-spanning assessments that enable integrating domain-specific models, such as the ODE meta-model.</p>

<h2>Learn More</h2>

<p>For more information about Eclipse Capra, visit our&nbsp;<a href="https://projects.eclipse.org/projects/modeling.capra">website</a> and our&nbsp;<a href="https://wiki.eclipse.org/Capra">Eclipse Wiki page</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
  <h3>About the Authors</h3>
  <?php foreach($pageAuthorArray as $item): ?>
    <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-6">
          <img width="150" class="img-responsive margin-top-10" alt="<?php echo $item['name']; ?>" src="<?php echo $item['img']; ?>" />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php echo $item['name']; ?></p>
          <p class="author-bio">
		  	<?php echo $item['bio']; ?>
          </p>
		    </div>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>