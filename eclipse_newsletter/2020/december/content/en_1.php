<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>With the publication of the&nbsp;<a href="https://openadx.eclipse.org/resources/">OpenADx Manifesto</a>, it&rsquo;s the ideal time to take a closer look at the&nbsp;<a href="https://openadx.eclipse.org/">OpenADx Working Group</a> and its goals.</p>

<p>To get first-hand insight, we sat down with Andreas Riexinger. Andreas is a product manager for autonomous driving at Robert Bosch GmbH and chair of the OpenADx Working Group Steering Committee. He was involved in developing the OpenADx Manifesto and is a committer to the&nbsp;<a href="https://projects.eclipse.org/projects/technology.cloe">Eclipse Cloe</a> and&nbsp;<a href="https://projects.eclipse.org/projects/technology.iceoryx">Eclipse iceoryx</a> projects.</p>

<p>Here&rsquo;s an edited version of our conversation.</p>

<h2>Q. Why was the OpenADx Working Group created?</h2>

<p><strong>A.</strong> A few years ago, when companies started working on autonomous driving (AD) technologies, everyone was developing their own toolchain. The toolchain is something everyone needs, but it&rsquo;s not a competitive differentiator and everyone was using similar tools anyway.</p>

<p>So, we saw the opportunity to stop wasting time and duplicating effort by joining forces to collaborate on toolchain technologies in an open environment so we could share costs and risks. The OpenADx Working Group at the Eclipse Foundation is where we do that.</p>

<h2>Q. What types of companies are involved in the OpenADx Working Group?</h2>

<p><strong>A.</strong> We currently have quite a few different domains represented in the working group.</p>

<ul>
<li>We have companies such as Bosch and Denso, which are quite well-known in the automotive industry.</li>

<li>We have tool vendors, such as AVL, because they want their tools to meet as many user needs as possible out of the box without having to customize them.</li>

<li>We also have infrastructure and IT giants such as Microsoft and Red Hat.</li>

<li>We have engineering services providers, such as EclipseSource, ADLINK, which is an edge computing and IoT solutions company, and consulting companies.</li>

<li>In addition, we have the Dortmund University of Applied Sciences and Arts and two other open source foundations: Open Robotics and The Autoware Foundation.</li>

<li>We have other members as well. The complete list is available on our&nbsp;<a href="https://openadx.eclipse.org/members/">website</a>.</li>
</ul>
<p>So, it&rsquo;s quite a diverse group of organizations. They bring a broad perspective to our efforts, which is good for the industry and for the open source technologies we&rsquo;re developing.</p>

<h2>Q. What are the goals of the OpenADx Working Group?</h2>

<p><strong>A.</strong> One of our main goals is to develop an interoperable toolchain where everyone can plug in their own toolchain or their preferred tool. The toolchain model is adaptable to each company&rsquo;s tools and way of working.</p>

<p>If you look at the development chain, everyone needs to complete the same tasks, starting with the architecture definition and moving through other steps until you jump in the vehicle to test drive it and validate the connectivity. The illustration below shows an example of the development chain we are using.</p>

<p>Companies have already invested heavily in licenses and employee training for the tools they&rsquo;re using at each stage of development. It&rsquo;s simply not feasible to define just one toolchain and force everyone to use the same tools.</p>

<p>This is why we&rsquo;re focusing on the data models and the interfaces between the tools. With a reference architecture for interoperability, it will be easier for companies to exchange tools, and to use their preferred tools.</p>

<h2>Q. Does the OpenADx Working Group focus on technologies beyond the toolchain?</h2>

<p><strong>A.</strong> Yes. In addition to the toolchain, we&rsquo;re focusing on:</p>

<ul>
	<li>The software stack and its reference architecture for interoperability</li>
	<li>Infrastructure such as the integration backend</li>
</ul>

<p>We&rsquo;re aiming to have everything hosted in the cloud. This will ensure that technologies are always up-to-date and accessible by everyone, from everywhere, with no need to install software locally.</p>

<h2>Q. How does the OpenADx Working Group relate to the other automotive working groups and technologies at the Eclipse Foundation?</h2>

<p><strong>A.</strong> We collaborate with other Eclipse Foundation working groups and projects that focus on areas that are relevant for the OpenADx Working Group. This illustration captures it well.</p>

<p>In addition, we collaborate with organizations such as the Association for Standardization of Automation and Measuring Systems (ASAM), which is also an Eclipse Foundation member. The Association is a non-profit organization that promotes standardization for toolchains in automotive development and testing.</p>

<h2>Q. Looking ahead, is there an opportunity for the OpenADx Working Group to focus on the differentiating aspects of automotive development one day?</h2>

<p><strong>A.</strong> About five years ago, talking about open source software in the automotive industry was more or less a no-go. But things are changing and from my point of view, they&rsquo;re on the right track.</p>

<p>For example, the automotive parts manufacturing company, Continental, uses an open source tool for research and development. Audi has open sourced some of its testing equipment. Tesla has open sourced some of its Model X and Model S code. And the U.S. Department of Transportation Federal Highway Administration has developed the CARMA Platform, which is an open source software platform for testing and evaluating cooperative automation concepts.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>

<p>To really focus on open source software in differentiating areas, the mindset in the automotive industry needs to change more. But there are good opportunities for companies to innovate on top of open source and build business models around it.</p>

<p>For example, the traffic jam pilot or the highway pilot could use open source base communications mechanisms. Or a company could provide ISO 26262 artifacts and certification services for tools that leverage open source software and must be certified.</p>

<h2>Q. What will it take to change the mindset toward open source in the automotive industry?</h2>

<p><strong>A.</strong> It will take more people understanding that open source has an important role to play in the automotive industry. It&rsquo;s not a threat. It&rsquo;s a chance to save money, share risks, and reduce risks. And there are business opportunities associated with it.</p>

<h2>Q. How can people get involved in the OpenADx Working Group and projects?</h2>

<p><strong>A.</strong> There are no hurdles. Everyone is invited to use the results of our work and participate in the community, even if they&rsquo;re not members of the Eclipse Foundation. If they would like to help shape the direction of the OpenADx Working Group, then they should become members of the Eclipse Foundation and the working group.</p>

<p>Feel free to use our&nbsp;<a href="https://accounts.eclipse.org/mailing-list/openadx">mailing list</a> to start a discussion or become part of one.</p>

<p>To learn more about the OpenADx Working Group, our vision, and our projects,&nbsp;<a href="https://openadx.eclipse.org/">visit our website</a>. You can download the OpenADx Manifesto from our&nbsp;<a href="https://openadx.eclipse.org/resources/">Resources page</a>.</p>

<p>To learn more about the benefits of becoming an Eclipse Foundation member, visit the&nbsp;<a href="https://www.eclipse.org/membership/">Members page</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/thabang.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Thabang Mashologu is vice president of marketing at the Eclipse Foundation, responsible for global marketing strategies, brand management, and communications. Thabang is passionate about the power of technology and diversity to solve big problems, create economic value, and make the world a better place.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>