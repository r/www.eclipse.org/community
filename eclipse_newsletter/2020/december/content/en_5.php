<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Jakob Erdmann</h2>

<div style="display:inline;">
<img width="200" class="float-left margin-right-40 img img-responsive" src="images/jakob_erdmann.jpeg" alt="Jakob Erdmann">
</div>
<div style="display:inline;">
<ul>
  <li>Involved in open source since: 2010</li>
  <li>Works for: The German Aerospace Center, also known as DLR, the abbreviation for Deutsches Zentrum für Luft- und Raumfahrt e.V.</li>
  <li>Eclipse Foundation contributor since: 2018</li>
  <li>Involved in: <a href="https://www.eclipse.org/sumo/">Eclipse SUMO</a></li>
  <li>Committer to: Eclipse SUMO</li>
  <li>Committer since: 2018</li>
  <li>Fun fact: Jakob has been a dedicated user of the vi text editor for 15 years and is teaching himself to play the hurdy-gurdy, an ancient type of fiddle that uses a hand crank to generate sound.</li>
</ul>
</div>

<hr />
<h2>Why did you first get involved in open source software communities?</h2>
<p>During my time at university, I often used open source software, so I learned to appreciate it. But I didn’t get around to committing to an open source project.</p>
<p>That changed in 2010 when I started working for the German Aerospace Center because my first project was open source from the start. And for good reason. When you work on scientific software, it has to be open source. Otherwise, other scientists can’t replicate your results with confidence. If scientists can’t look at the models and how they interact, and make changes to them, it’s just a black box. And that’s not my idea of science.</p>

<h2>How did that involvement lead to you becoming a committer at the Eclipse Foundation?</h2>
<p>In 2018, we wanted to strengthen and expand our user community, so we contributed the project I was working on to the Eclipse Foundation as <a href="https://www.eclipse.org/sumo/">Eclipse Sumo</a>. This allowed us to keep our existing academic user base and helped us gain an industry user base.</p>
<p>When the move happened, all of the original developers on the project immediately became committers in order to maintain our previous work flow. Thus, I became a contributor and a committer at the same time.</p>

<h2>How would you summarize your experiences as a committer?</h2>
<p>I love writing software and committing code.</p>
<p>The biggest challenge is keeping up with the growth. The number of questions and problems that are brought to me has continued to grow, whereas the number of hours in my day has remained pretty stable.</p>
<p>At the same time, it’s really rewarding to have so much interaction with a very active user base and to receive feedback every day that the software we are building is used and useful. It’s great — it’s one of the best things about developing this software.</p>
<p>Also, sometimes you’re surprised because you discover that several things you created have been combined to create a new feature that you did not anticipate. On the other hand, bugs and problems rarely surprise me anymore.</p>

<h2>
What are your next steps and goals as a committer and Eclipse Foundation community member?
</h2>
<p>I would like to stay on the track we’re already on. We’re making the software more useful every day and we’re on a good trajectory.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>
<p>If the goal of the Eclipse Foundation, which is industry adoption of open source software, aligns with your goals, then go for it. If you’re working on an open source project and you want industry exposure, the Eclipse Foundation is a good place to be.The German Aerospace Center has become a <a href="https://www.eclipse.org/membership/#tab-levels">Strategic member</a> of the Eclipse Foundation so I’m obviously not the only person who thinks the Eclipse Foundation provides important benefits.</p>
<p>
I encourage everyone who can do so to contribute to open source software. Software is increasingly shaping our lives and I think open source software is a necessity if we want to be somewhat in control over the technology in our lives.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
