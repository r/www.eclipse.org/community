<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><a href="https://www.eclipse.org/app4mc/">Eclipse APP4MC</a> is an open platform for model-based performance analysis and optimization of heterogenous embedded hardware and software systems. The platform allows developers to create and manage complex toolchains, including simulation and validation. It has been used by Bosch and its partners in the automotive sector and is proven to provide interoperability and extensibility, and to unify data exchanges in cross-organizational projects.</p>

<p>The Eclipse APP4MC project is based on the work done in the ITEA-funded AMALTHEA, AMALTHEA4public, and PANORAMA European research projects.</p>

<h2>Developers Can Combine Open Source and Commercial Components</h2>

<p>To help engineering teams that are facing increasing issues when setting up their environments, the Eclipse APP4MC project features a service-oriented architecture that is deployable as individual cloud services hosted on Eclipse Foundation cloud infrastructure. However, because some services are not available as open source projects, the Eclipse APP4MC platform also provides the ability to connect individual cloud services to an existing architecture.</p>

<p>The software provides a classic IDE with modeling and data modification capabilities as well as the ability to use cloud processing for simulation, data analysis, and result visualization. With these two approaches, developers and users can create a solution that meets their individual needs.</p>

<p>Figure 1 illustrates the Eclipse APP4MC IDE user interface.</p>

<p><span style="font-size:12px">Figure 1: Eclipse APP4MC IDE User Interface</span></p>
<img class="img img-responsive" src="images/article3_figure_1.png" alt="Figure 1: Eclipse APP4MC IDE User Interface">
<p>Figure 2 illustrates the Eclipse APP4MC platform in a cloud environment with PANORAMA cloud services and individually connected cloud services.&nbsp;<a href="https://panorama-research.org/">PANORAMA</a> is an ITEA 3-funded European research project that aims to boost design efficiency for heterogenous systems.</p>

<p><span style="font-size:12px">Figure 2: Eclipse APP4MC Cloud Services</span></p>
<img class="img img-responsive" src="images/article3_figure_2.png" alt="Figure 2: Eclipse APP4MC Cloud Services">
<h2>Cloud Services for Operations on AMALTHEA Models and Service Chaining</h2>

<p>Eclipse APP4MC cloud services allow developers to perform operations on AMALTHEA models with no need to install tooling software locally. Developers can integrate static analysis and simulation of an AMALTHEA model in a continuous integration process and automatically verify the results of model modification.</p>

<p>Eclipse APP4MC cloud services also include a well-defined API through which service chaining is possible. This allows additional cloud services to be added to the processing chain without adjusting the processing implementation. The&nbsp;<a href="https://app4mc.eclipseprojects.io/">APP4MC Cloud Manager</a> provides a default processing chain implementation with a web-based user interface to give developers a better idea of the possibilities enabled by the cloud services.</p>

<h2>A Real-Time Calculus Analysis Tool for Modular Performance Analysis</h2>

<p>Performance analysis based on technologies, such as a Real-Time Calculus (RTC) analysis tool, makes it possible to perform modular performance analysis of heterogeneous real-time systems.</p>

<p>The RTC analysis tool converts a given system model into a Directed Acyclic Graph (DAG) called a scheduling network. The nodes in this network correspond to the tasks in the system, while the edges between the nodes indicate resource-sharing and execution order.</p>

<p>In its current state, the RTC analysis tool derives upper and lower bounds for the delay and buffering of each task corresponding to the end-to-end delay and number of subsequent missed deadlines respectively. Additional metrics will be added to this tool in the future.</p>

<p>The RTC analysis tool has been implemented to analyze systems modeled with AMALTHEA, and is integrated in the Eclipse APP4MC cloud platform, which means no local setup is required. Furthermore, the tool&#39;s API can be called separately to allow its use as a standalone service in custom workflows. The RTC analysis tool was presented as part of the IEEE IDAACS-SWS conference 2020. You can find the presentation&nbsp;<a href="https://www.youtube.com/watch?v=Ttc2tChHKC0&amp;t=6s&amp;ab_channel=EclipseFoundation">here</a>.</p>

<h2>Simulations for Early Evaluations of Models</h2>

<p>Design and development with a model-based process creates the need for early evaluation of the models created. The Eclipse APP4MC toolchain allows simulation of AMALTHEA models with the SystemC simulation framework based on a model-to-text transformation named APP4MC.sim. The resulting SystemC simulation contains proxy classes for important AMALTHEA meta-model components and represents the execution semantics.</p>

<p>During the simulation, all execution time semantics are sampled according to their specification. The simulation currently creates two trace file types from the executed specification.</p>

<p>First, the simulation provides a Best Trace Format (BTF) file, which allows analysis of software execution interactions with standard tools. Second, tracing of hardware aspects, such as communications resource utilization, are done in the Value Change Dump (VCD) format (Figure 3).</p>

<p><span style="font-size:12px">Figure 3: VCD Trace of an AMALTHEA SystemC Simulation</span></p>
<img class="img img-responsive" src="images/article3_figure_3.png" alt="Figure 3: VCD Trace of an AMALTHEA SystemC Simulation">
<p>For easier analysis, the simulation also annotates software entities, such as memory and communications units, that occupy hardware components based on an extension of the GTKWave wave viewer.</p>

<h2>Trace Analysis Is Based on Best Trace Format and the AMALTHEA Trace Database</h2>

<p>After a successful simulation run, there are several ways to process the results. One way is to write out the simulation trace by serializing it into a Best Trace Format (BTF) artifact, as described in&nbsp;<a href="https://assets.vector.com/cms/content/products/TA_Tool_Suite/Docs/BTF_Specification.pdf">BTF_Specification_2.2.0.pdf</a>. BTF is a simple, comma separated values (CSV) format where each line represents a single event in the trace. BTF files can be produced using commercial simulation tools, such as the Vector TA Tool Suite, INCHRON Tool Suite, and trace debuggers.</p>

<p>In a second step, the BTF file can be visualized in a Gantt chart (Figure 4), or analyzed by deriving several metrics from trace data. These derived metrics can, in turn, be visualized in various charts.</p>

<p><span style="font-size:12px">Figure 4: Gantt Chart of a Simple Example Trace</span></p>
<img class="img img-responsive" src="images/article3_figure_4.png" alt="Figure 4: Gantt Chart of a Simple Example Trace">
<p>To enable calculation of metrics only, which can be user-defined, we designed a new AMALTHEA Trace Database (ATDB). Initially, this SQLite database contains only the trace events from the BTF artifact. Then, some predefined typical metrics are calculated and inserted in metrics tables.</p>

<p>A typical metric is the amount of time an instance of an entity, such as a task, is in a specific state &mdash; pre-empted, for example. Based on the state chart provided in the BTF specification, all state time metrics can be automatically calculated using SQL queries by combining the timestamps of the respective events.</p>

<p>These instance metrics are subsequently aggregated to calculate the average pre-emption time, the standard deviation of a task&rsquo;s runtime, and other metrics. Once all of the metrics are calculated, the source trace event table can be removed from the ATDB if the trace producer does not want to disclose the trace details. This enables a standardized metrics data exchange using the ATDB format.</p>

<p>The ATDB can also be enriched with event and event chain specifications from an AMALTHEA model. The specifications can then be used to find all event and event chain instances that are visible in the source trace. With this information, aggregated metrics for event chains can be calculated. For example, the minimum and maximum event chain latency can be calculated. The results can then be visualized in various charts and compared to latency requirements.</p>

<p>The new ATDB is part of the&nbsp;<a href="https://www.eclipse.org/app4mc/news/2020-12-01-release-1-0-0/">Eclipse APP4MC v1.0.0</a> release. The release includes converters, documentation, examples, and Java APIs that enable access and interactions with the database.</p>

<p>Figure 5 shows the Eclipse APP4MC tool platform with an example trace in BTF and the corresponding metrics in the related ATDB file.</p>

<p><span style="font-size:12px">Figure 5: BTF and ATDB representations in the Same Trace in Eclipse APP4MC</span></p>
<img class="img img-responsive" src="images/article3_figure_5.png" alt="Figure 5: BTF and ATDB representations in the Same Trace in Eclipse APP4MC">
<p>Together, the capabilities described in this article give developers an open source-based collaborative environment for heterogenous embedded hardware and software development. They can combine open source and commercial components to create an environment that meets their unique needs.</p>

<h2>Learn More</h2>

<p>To learn more about the Eclipse APP4MC project, visit our&nbsp;<a href="https://www.eclipse.org/app4mc/">website</a> and follow us on&nbsp;<a href="https://twitter.com/PanoramaEng">Twitter</a>.</p>

<p>To learn more about the OpenADx Working Group at the Eclipse Foundation and the autonomous driving toolchain, click&nbsp;<a href="https://openadx.eclipse.org/">here</a>.</p>


<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Authors</h3>
	<?php foreach($pageAuthorArray as $item): ?>
    <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-6">
          <img width="150" class="img-responsive margin-top-10" alt="<?php echo $item['name']; ?>" src="<?php echo $item['img']; ?>" />
        </div>
        <div class="col-sm-18">
          <p class="author-name"><?php echo $item['name']; ?></p>
          <p class="author-bio">
		  	<?php echo $item['bio']; ?>
          </p>
		    </div>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>