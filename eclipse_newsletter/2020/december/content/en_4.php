<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h2>The Eclipse IDE 2020-12 Release Is Out</h2>
<p>The latest quarterly release of the&nbsp;<a href="https://www.eclipse.org/eclipseide/2020-12/">Eclipse IDE</a> includes new features such as the new generic Unit Test view and extended Linux aarch64 support. It also features tooling improvements and the latest Java and JUnit versions.</p>

<p>The Eclipse IDE is the leading open platform for professional developers and is used by millions of developers globally.</p>

<p>Download the Eclipse IDE 2020-12 packages&nbsp;<a href="https://www.eclipse.org/eclipseide/2020-12/">here</a>.</p>

<h2>Jakarta EE 9 Is Released</h2>
<p>The long-awaited and much-discussed&nbsp;<a href="https://jakarta.ee/release/9/">Jakarta EE 9</a> &ldquo;big bang&rdquo; is complete. The package namespace is now jakarta.* instead of javax.*.</p>

<p>Jakarta EE 9 gives tool vendors, product vendors, and custom application developers the ideal opportunity to update their software:</p>

<ul>
	<li>Migration to Jakarta EE 9 is straightforward because only the namespace change needs to be considered.</li>
	<li>The&nbsp;<a href="https://jakarta.ee/about/jesp/">Jakarta EE Specification Process</a> means it&rsquo;s much easier than it was in the past for products to be certified as full profile and web compatible.</li>
	<li>Going forward, community members working on each Jakarta EE specification can deliver as many specification releases as needed to meet the community&rsquo;s requirements. That means developers and enterprises that need technology advances in a particular Jakarta EE specification, such as Jakarta Servlet, no longer have to wait for a full platform release to start using those advances.</li>
</ul>

<p>Use #JakartaEE9 on Twitter to share your migration stories, connect with others on the same migration path, and let the world know the massive installed base of enterprise Java applications is making the transition to the next phase of innovation.</p>

<p>For more insight into the momentum around Jakarta EE 9, read&nbsp;<a href="https://blogs.eclipse.org/post/mike-milinkovich/jakarta-ee-9-delivers-big-bang">Mike Milinkovich&rsquo;s blog</a>.</p>

<p>Also, if you weren&rsquo;t able to join the&nbsp;<a href="https://jakartaone.org/2020/">JakartaOne Livestream</a> sessions live on December 8, you can always watch the replays on the&nbsp;<a href="https://www.youtube.com/channel/UC4M7h5b6elu9VlzjLfzuXyg">Jakarta EE YouTube channel</a>.</p>


<h2>OSGi Core Release 8 Is Published</h2>
<p>The OSGi Alliance has approved the specifications, reference implementations, and compliance tests for OSGi Core Release 8. The approval is one of the group&rsquo;s final actions before transitioning to the&nbsp;<a href="https://www.eclipse.org/org/workinggroups/osgi-charter.php">OSGi Working Group</a> at the Eclipse Foundation.</p>

<p>As part of the transition to the Eclipse Foundation, the OSGi git repositories are now public. Check out the&nbsp;<a href="https://github.com/osgi">OSGi GitHub organization</a> and, in particular, the&nbsp;<a href="https://github.com/osgi/osgi">OSGi Specification Project Build repository</a>.</p>

<p>In addition, project proposals have been submitted to create the&nbsp;<a href="https://projects.eclipse.org/proposals/osgi-specification-project">OSGi Specification Project</a> and the&nbsp;<a href="https://projects.eclipse.org/proposals/osgi-technology-project">OSGi Technology Project</a> at the Eclipse Foundation. For insight into an OSGi technology project, watch the replay of the&nbsp;<a href="https://github.com/osgi/osgi-test">osgi-test project</a> presentation from&nbsp;<a href="https://www.youtube.com/watch?v=gj8IsieHa4s&amp;feature=youtu.be">EclipseCon 2020</a>.</p>

<h2>451 Research Explores the Eclipse Foundation Role in the Automotive Industry</h2>
<p>A recent 451 Research report highlights the important role the automotive working groups and projects hosted at the Eclipse Foundation are playing in the automotive industry.</p>
<p>The report describes the momentum in automotive initiatives at the Eclipse Foundation, including the value provided in the four automotive working groups &mdash;&nbsp;<a href="https://openadx.eclipse.org/">OpenADx</a>,&nbsp;<a href="https://openmobility.eclipse.org/">openMobility</a>,&nbsp;<a href="https://openpass.eclipse.org/">openPASS</a>, and&nbsp;<a href="https://openmdm.org/">openMDM</a> &mdash; broad participation by industry leaders, such as Bosch, and the strong uptake of projects, such as&nbsp;<a href="https://www.eclipse.org/sumo/">Eclipse SUMO</a>. The report also looks at the role of other open source foundations focused on automotive technologies.</p>
<p>In its final analysis, the report notes the Eclipse Foundation has the history and membership strength needed to influence software direction in the automotive industry.</p>
<p><a href="https://f.hubspotusercontent10.net/hubfs/5413615/451_The%20Eclipse%20Foundation%20brings%20open%20source%20software%20to%20the%20automotive%20industry.pdf">Read the 451 Research report</a></p>

<h2>Read the Cedalo Member Case Study</h2>
<p>In our latest member case study, we share Cedalo’s story. Cedalo is an active member of the Eclipse IoT ecosystem and a great example of how businesses and entrepreneurs naturally discover one another and new opportunities to co-innovate in Eclipse Foundation communities.</p>
<p>Cedalo Co-Founder and CEO, Philipp Struss, says the company’s Eclipse Foundation membership is essential to build connections and relationships with other companies. He calls joining Eclipse IoT a “no-brainer” decision for anyone working with open source in the IoT domain.</p>
<p><a href="https://outreach.eclipse.foundation/cedalo-iot-open-source-case-study">Read the Cedalo case study.</a></p>
<p>If you missed our member case study on Payara Services, you can download it&nbsp;<a href="https://outreach.eclipse.foundation/payara-services-open-source-case-study">here</a>. And be sure to watch for our upcoming case studies on Obeo, itemis, and others.</p>
<p>We&rsquo;re publishing all of our member case studies under the Creative Commons Attribution 4.0 International (CC BY 4.0) license, so you can repurpose the content with attribution.</p>
<p>To get involved in our member case study initiative, email <a href="mailto:marketing@eclipse.org">marketing@eclipse.org</a>.</p>

<h2>Our Virtual Events Were a Success in 2020</h2>
<p>While 2020 has been an extremely challenging year for all of us, we&rsquo;re pleased to tell you that one of the adaptations we had to make has gone very well: We&rsquo;ve turned virtual events into a core competency at the Eclipse Foundation.</p>

<p>Eclipse Foundation community members and staff hosted 53 virtual events in 2020, up 63 percent from 2019. In total, these virtual events attracted more than 8,000 attendees.</p>

<p>Many thanks to everyone who organized and attended our virtual events in 2020. They&rsquo;re all available for replay on the&nbsp;<a href="https://www.youtube.com/channel/UCej18QqbZDxuYxyERPgs2Fw">Eclipse Foundation YouTube channel</a>.</p>

<p>We look forward to hosting more virtual events in 2021. If you have a project or topic you&rsquo;d like to share with the community, email <a href="mailto:events@eclipse.org">events@eclipse.org</a>, or contact an Eclipse Foundation marketing manager to discuss setting up a webinar with us.</p>


<h2>The Eclipse Foundation Supports the Indy Autonomous Challenge</h2>
<p>Eclipse Foundation member,&nbsp;<a href="https://www.eclipse.org/membership/showMember.php?member_id=1266">ADLINK Technology</a>, is bringing together the Eclipse Foundation and other open source software foundations to support the Indy Autonomous Challenge (IAC) car race, scheduled for October 23, 2021.</p>

<p>ADLINK is an official sponsor of the 2021 IAC race, in which 39 university teams will compete for $1.5 million in prize money in the world&rsquo;s first head-to-head autonomous car race at the Indianapolis Motor Speedway.</p>

<p>ADLINK is a founding member of&nbsp;<a href="https://edgenative.eclipse.org/">Eclipse Edge Native Working Group</a>, and is an active contributor to&nbsp;<a href="http://zenoh.io/">Eclipse zenoh</a> and&nbsp;<a href="https://projects.eclipse.org/projects/iot.cyclonedds">Eclipse Cyclone DDS</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>