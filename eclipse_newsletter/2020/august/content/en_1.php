<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Since the <a href="https://jakarta.ee/specifications/platform/9/">Jakarta EE 9 milestone release</a>
  in June, we&rsquo;ve been making good progress toward a final release in Fall 2020. Here&rsquo;s a
  brief summary of our most recent efforts, our progress, and key opportunities to get involved.
</p>
<h2>There Are Still Opportunities to Help</h2>
<p>
  We&rsquo;re continuing to update, review, and test the <a href="https://jakarta.ee/specifications/">Jakarta
    EE specifications</a> so they can be sent to the <a
    href="https://jakarta.ee/committees/specification/operations/"
  >Jakarta EE Specification Committee</a> for approval. It&rsquo;s going well, but the process is
  complicated because we need to update the Jakarta EE specifications in &ldquo;waves&rdquo; that
  take their dependencies into account.
</p>
<p>For example, the specifications in wave zero are standalone specifications with no dependencies,
  so they can be updated at any time. But, the specifications in wave two build on those in wave
  one, so they can&rsquo;t be updated until wave one is complete. And so, it goes until all of the
  waves of specifications are approved. At that point, we need to ensure Eclipse GlassFish
  successfully implements all of the specifications so it can become the compatible implementation
  that&rsquo;s required for the release.</p>
<p>We&rsquo;re seeing successful test passes across large numbers of the required documents, so
  that&rsquo;s good news. And, I&rsquo;m happy to tell you that GlassFish is about 99 percent of the
  way to passing the full platform test suite. However, with so many moving parts, we could
  definitely use more help reviewing and testing specifications to ensure they&rsquo;re ready to go
  to the Specification Committee for approval.</p>
<p>Please, step in and help if you can. It&rsquo;s a great opportunity to demonstrate what can be
  achieved by a community that&rsquo;s dedicated to true open source collaboration.</p>
<h2>We&rsquo;ve Switched Our Java Release Priorities</h2>
<p>In the original Jakarta EE 9 Release Plan, the priority was to make Java 11 support mandatory and
  Java 8 support optional. The goal was to drive implementation and acceptance of Java 11 because
  most vendors ship Java 11 versions of Jakarta EE 8. However, we&rsquo;ve switched those
  priorities.</p>
<p>In the Jakarta EE 9 release, Java 8 support is mandatory and Java 11 support is optional. We made
  this decision for two reasons:</p>
<ul>
  <li>To stay as close as possible to the target release date in Fall 2020 and reduce the risk of
    issues related to mandatory Java 11 support.</li>
  <li>To make the namespace change the only difference between Jakarta EE 8 and Jakarta EE 9 and
    increase the consistency between the two releases.</li>
</ul>
<h2>Software Providers Are Implementing the New Namespace</h2>
<p>
  A number of software providers have already implemented the Jakarta.* namespace, and others have
  planned to implement the change. Here&rsquo;s a snapshot of the current implementation roadmap
  from our<a href="https://jakarta.ee/resources/JakartaEE-Datasheet-July172020_final.pdf"> </a><a
    href="https://jakarta.ee/resources/JakartaEE-Datasheet-July172020_final.pdf"
  >Tools Vendor Datasheet</a> for Jakarta EE 9.
</p>
<img src="images/1_1.png" alt="Software Providers Are Implementing the New Namespace">
<h2>We Need Your Input for Future Releases</h2>
<p>The Jakarta EE 9 release is the foundation for the future. Once it&rsquo;s out, you can expect to
  see rapid platform innovation and Jakarta EE releases with new and exciting features and
  functionality. The next release will be Jakarta EE 9.1, which will make Java 11 support mandatory
  and Java 8 support optional.</p>
<p>To move beyond Jakarta EE 9.1, we need the community&rsquo;s input and ideas about how Jakarta EE
  should evolve. The Jakarta EE community is a very open and self-organizing group. Everyone is
  welcome and encouraged to participate and collaborate.</p>
<p>If you have ideas about how we should push forward with Jakarta EE, share them by:</p>
<ul>
  <li>Subscribing to<a href="https://jakarta.ee/connect/mailing-lists/"><span style="color: #1155cc">
    </span></a><a href="https://jakarta.ee/connect/mailing-lists/"><span style="color: #1155cc"><u>Jakarta
          EE project mailing lists</u></span></a> and getting involved in Jakarta EE 10 discussions.
  </li>
  <li>Raising enhancement requests on the GitHub repository for Jakarta EE projects. You can find
    the complete list of projects<a href="https://projects.eclipse.org/projects/ee4j"><span
      style="color: #1155cc"
    > </span></a><a href="https://projects.eclipse.org/projects/ee4j"><span style="color: #1155cc"><u>here</u></span></a>.
  </li>
</ul>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/steve.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">Steve Millidge is the founder and CEO of <a href="https://www.payara.fish/">Payara Services</a>,
  creators of <a href="https://www.payara.fish/products/payara-server/">Payara
        Server Enterprise</a> and <a href="https://www.payara.fish/products/payara-micro/">Payara
        Micro Enterprise</a>. He also founded <a
    href="https://www.payara.fish/enterprise/consultancy/"
  >Payara Accelerator</a>, which provides comprehensive
  open source consulting services to Payara Services customers.</p>
        </div>
      </div>
    </div>
  </div>
</div>