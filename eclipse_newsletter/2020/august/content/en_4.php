<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>EclipseCon 2020 is October 19-22</h2>
<p>
  If you haven&rsquo;t already registered for <a
    href="https://www.eclipsecon.org/2020"
  >EclipseCon 2020</a>, take a minute to <a
    href="https://www.eclipsecon.org/2020/registration"
  >reserve your spot today</a>. This year&rsquo;s EclipseCon is a virtual event, and there&rsquo;s
  no fee to attend, but space is limited, so don&rsquo;t delay.
</p>
<p>
  EclipseCon kicks off with <a
    href="https://www.eclipsecon.org/2020/community-day"
  >Community Day</a> on October 19. Attending Community Day sessions and round tables are a great
  way to reconnect with the communities you&rsquo;re already involved in, and explore opportunities
  to participate in new projects and working groups.
</p>
<p>
  The week continues with a great lineup of <a
    href="https://www.eclipsecon.org/2020/sessions"
  >informative and helpful sessions</a> from Eclipse community experts. With 10 conference tracks to
  choose from, we&rsquo;re sure there&rsquo;s at least one session that will interest every Eclipse
  community member. Here are just a few of the conference tracks at this year&rsquo;s event:
</p>
<ul>
  <li>Cloud native Java</li>
  <li>IoT and edge</li>
  <li>Java and Java development tools (JDT)</li>
  <li>Open Service Gateway Initiative (OSGi)</li>
  <li>Web and desktop tools and IDEs</li>
</ul>
<p>
  <a href="https://www.eclipsecon.org/2020/registration">Get all the registration details and
    register now.</a>
</p>
<p>
  Also, keep an eye on <a href="https://blogs.eclipse.org/">our
    blog</a>. Over the coming weeks, we&rsquo;ll be highlighting sessions from several conference
  tracks and providing tips on how to get the most from your virtual EclipseCon experience.
</p>
<hr />
<h2>The Jakarta EE 9 Tools Vendor Datasheet is Now Available</h2>
<p>
  With the Jakarta EE 9 General Availability (GA) release almost finalized, a growing number of
  tools vendors are implementing the namespace change. Recently, the Jelastic PaaS team
  <a href="https://jelastic.com/blog/jakartaee-9-release/">announced it is actively working to
    support Jakarta EE 9 in its cloud platform</a>.
</p>
<p>To help these vendors make the transition, the Jakarta EE community has developed a datasheet
  summarizing the namespace migration challenge and opportunity.</p>
<p>Tools vendors that take advantage of Jakarta EE 9 to implement the new namespace can migrate
  faster and more cost effectively than those who wait until the Jakarta EE 10 release as Jakarta EE
  10 will also include new features, enhancements, and other major changes.</p>
<p>The datasheet describes:</p>
<ul>
  <li>The key elements of the Jakarta EE 9 release for tools vendors</li>
  <li>Vendor implementation roadmaps</li>
  <li>The list of affected packages</li>
</ul>
<p>
  Download the datasheet
  <a href="https://jakarta.ee/resources/JakartaEE-Datasheet-July172020_final.pdf">here</a>.
</p>
<hr />
<h2>The Friends of Jakarta EE Monthly Call is For Everyone</h2>
<p>The Friends of Jakarta EE monthly calls are by the community, for the community. These calls are
  held on the fourth Wednesday of every month and are an opportunity for the Jakarta EE community to
  get together virtually and talk with one another.</p>
<p>
  The calls play no formal role in<a
    href="https://jakarta.ee/about/"
  >Jakarta EE Working Group</a> activities. The people who attend are the right people, the topics
  discussed are the right topics, and the outcomes are the right outcomes.
</p>
<p>
  The next call is Wednesday, August 26 at 11:00 a.m. EDT. You&rsquo;ll find the connection details
  on the<a
    href="https://calendar.google.com/calendar/embed?src=eclipse-foundation.org_3281qms6riu4kdf354jn5idon0@group.calendar.google.com"
  >Jakarta EE community calendar</a>.
</p>
<hr />
<h2>The Eclipse SAM IoT Research Conference is September 17-18</h2>
<p>The Eclipse SAM IoT virtual conference will bring together industry experts and researchers who
  are working on the next generation of IoT with a focus on:</p>
<ul>
  <li>Security and privacy</li>
  <li>Artificial intelligence and analytics</li>
  <li>Digital twins and modeling</li>
</ul>
<p>These innovations provide the groundwork for next-generation IoT solutions that enable smart
  factories, smart cities, critical infrastructure, and cooperative service robotics.</p>
<p>
  For conference details, visit the<a
    href="https://events.eclipse.org/2020/sam-iot/"
  >webpage</a>.
</p>
<p>
  To register, click <a href="https://www.eventbrite.de/e/eclipse-sam-iot-2020-tickets-107823242220">here</a>.
</p>
<hr />
<h2>IoT Developer Survey Results are Coming Soon</h2>
<p>The 2020 IoT Developer Survey closed on July 10 so watch for the results to be available.</p>
<p>This is the first year the survey included questions about how developers are incorporating edge
  computing technologies into their IoT solutions. With the next generation of IoT applications
  extending to the network edge, and strong momentum in edge technologies at the Eclipse Foundation,
  the survey results will be interesting and insightful.</p>
<p>With more than 1,600 survey responses, the results will give everyone in the IoT ecosystem
  &mdash; from original equipment manufacturers (OEMs), software vendors, and hardware manufacturers
  to service providers, enterprises, and individual developers &mdash; a better understanding of the
  latest IoT solutions and service development trends. They&rsquo;ll also have a clear view of how
  these trends impact their strategies and businesses.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
