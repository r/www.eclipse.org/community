<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Lukas Jungmann</h2>
<img class="float-left margin-right-40" src="images/lukas.png" alt="Lukas Jungmann">
<ul>
  <li>Involved in open source since: 2004</li>
  <li>Works for: Oracle Corporation</li>
  <li>Eclipse Foundation contributor since: 2010</li>
  <li>Involved in: <a href="https://projects.eclipse.org/user/3043">14 Jakarta EE projects</a></li>
  <li>Committer to: 12 Jakarta EE projects</li>
  <li>Committer since: 2013</li>
  <li>Fun fact: You can add the month numbers his first two children were born in &mdash; three and
    five for March and May &mdash; to get the month number his third child was born in &mdash; eight
    for August. Three, five, and eight also represent the last number of the year in which each
    child was born.</li>
</ul>
<hr />
<h2>Why did you first get involved in open source software and communities?</h2>
<p>In 2004, in my first job as a quality engineer at Sun Microsystems, I was working on the NetBeans
  IDE, which was an open source project with a community around it. Sometimes, I was supporting
  users until the early morning hours. I received a big thank you email and that was really
  rewarding. It made me realize how much I enjoy helping people so I increased my activities.</p>
<h2>How did that involvement lead to you becoming an Eclipse committer?</h2>
<p>
  I became involved with the Eclipse Foundation in 2010 when I was working for Red Hat. Then, in
  2013, I was working as a developer at Oracle and there was an opportunity to move to the <a
    href="https://www.eclipse.org/eclipselink/"
  >EclipseLink project</a>, and that&rsquo;s when I gained committer status.
</p>
<p>In EclipseLink, if you want to be a committer, you need to provide at least 10 valuable
  contributions. It could be a bug fix, a documentation update, a wiki page. Everything counts, but
  we want to see that people are actively trying to help the project.</p>
<p>When Oracle decided to donate everything related to Java EE to the Eclipse Foundation, I became
  even more involved. Because I had the benefit of knowing how things work within the Eclipse
  Foundation, I was able to share my knowledge and help my colleagues move projects over to the
  Eclipse Foundation faster.</p>
<h2>How would you summarize your experiences as a committer?</h2>
<p>The best thing about being a committer is to see that you&rsquo;re able to help people move their
  projects forward. There&rsquo;s real personal satisfaction.</p>
<p>It&rsquo;s challenging to understand the processes around the project life cycle and the legal
  implications, but the Eclipse Foundation provides a Committer&rsquo;s Handbook that contains
  everything a committer needs to know. It&rsquo;s an invaluable source of information.</p>
<p>Also, the people involved in the Eclipse Foundation are really helpful and kind. If something is
  not clear or you need help, you can just file a bug through Bugzilla and ask for help and the
  problem will be solved pretty quickly.</p>
<h2>What are your next steps and goals as a committer and Eclipse Foundation community member?</h2>
<p>
  Right now, I&rsquo;m not looking very far ahead because I&rsquo;m completely focused on helping to
  make <a href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9">Jakarta EE
    9</a> a successful release.
</p>
<h2>What would you say to developers who are considering getting more involved in open source
  software projects at the Eclipse Foundation?</h2>
<p>It&rsquo;s a very good way to solve problems you&rsquo;re having because Eclipse open source is
  all about contributions and collaboration. If you have a project that&rsquo;s not getting
  appropriate attention, the Eclipse community can help you move it forward.</p>
<p>When you get involved, start by doing small stuff. It may seem as though it&rsquo;s not
  important, but it very often is, and it&rsquo;s the best way to understand the project and how it
  works. You can grow your knowledge about a project and the processes around it to gain experience
  and work toward committer status if that&rsquo;s what you want.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
