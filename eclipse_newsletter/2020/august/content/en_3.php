<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  The <a
    href="https://projects.eclipse.org/projects/ee4j.jakartaee-tck"
  >Eclipse Jakarta EE Technology Compatibility Kit (TCK)</a> team needs your help. We&rsquo;re
  creating more opportunities for the Jakarta EE community to help develop Jakarta EE platform TCK
  tests. We need to do this because as we add new technologies to Jakarta EE, we also have to add
  the appropriate tests to prove the technologies are correctly implemented in Jakarta EE releases
  and server implementations.
</p>
<p>Before I explain how you can get involved in developing TCK tests, here&rsquo;s a brief overview
  of why they&rsquo;re so important to the future of Jakarta EE.</p>
<h2>Platform and Standalone TCK Tests Must Be Passed</h2>
<p>Every Jakarta EE server implementation must pass 100 percent of the platform TCK tests. The TCK
  tests prove the server is compatible with the Jakarta EE specification requirements.</p>
<p>
  Every Jakarta EE server implementation must also pass standalone TCK tests. Generally, these tests
  are focused on a specific Jakarta API, such as the<a href="https://jakarta.ee/specifications/cdi">
  </a><a href="https://jakarta.ee/specifications/cdi">Jakarta Contexts and Dependency Injection</a>
  standalone TCK, while the platform TCK tests ensure all Jakarta EE specifications (technologies)
  work correctly when called from various Jakarta EE containers/technologies.
</p>
<h2>Jakarta EE Platform TCK Tests Verify Compatibility Levels</h2>
<p>The Jakarta EE platform TCK tests verify the level of compatibility applications can expect for
  each Jakarta EE specification technology. This testing of expected behavior increases application
  portability to different Jakarta EE server implementations.</p>
<p>
  For example, some TCK tests invoke<a href="https://projects.eclipse.org/projects/ee4j.jpa"> </a><a
    href="https://projects.eclipse.org/projects/ee4j.jpa"
  >Jakarta Persistence</a> persistence contexts through remote session bean calls where a different
  Java virtual machine (JVM) thread may be used for every call. This can create conditions that are
  similar to those your applications may be using and make it easier to discover edge cases that can
  only be reached when testing with a Jakarta EE server implementation. This approach has helped me
  find problems during TCK testing that I might not have discovered as quickly. Applications will
  reach the edge cases eventually, but it&rsquo;s better to discover them as early as possible.
</p>
<p>The roughly five million lines of TCK test code cover many different situations, each of which
  offers value to the applications that deploy on Jakarta EE server implementations. If the TCKs
  were not used to test the Jakarta EE specifications, application developers may be caught in
  situations where an application server is not fully implementing some aspect of the Jakarta EE
  specifications. And that&rsquo;s why we need your help.</p>
<h2>Get Involved in TCK Test Development</h2>
<p>To start participating in TCK test development, follow the steps below.</p>
<p>
  1. Join the<a href="https://accounts.eclipse.org/mailing-list/jakartaee-tck-dev"> </a><a
    href="https://accounts.eclipse.org/mailing-list/jakartaee-tck-dev"
  >jakartaee-tck-dev</a> mailing list to start participating in TCK conversations and voice your
  opinions.
</p>
<p>
  It&rsquo;s perfectly fine to ask how you can help or what&rsquo;s new this week. We have fewer
  than 100 subscribers on this mailing list, which means we&rsquo;re missing you, so please join.
  After<a href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9"> </a><a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9"
  >Jakarta EE 9</a> is released, we&rsquo;ll take a moment to introduce ourselves on the Platform
  TCK mailing list and celebrate the release. We can also start discussing changes you can help
  with.
</p>
<p>
  One challenge is that we need to document the internal workings of the TCK. We can start asking
  questions and adding answers to the<a href="https://github.com/eclipse-ee4j/jakartaee-tck/wiki"> </a><a
    href="https://github.com/eclipse-ee4j/jakartaee-tck/wiki"
  >wiki</a> for all to learn from. Here are some<a
    href="https://docs.github.com/en/github/building-a-strong-community/adding-or-editing-wiki-pages#adding-or-editing-wiki-pages-locally"
  > </a><a
    href="https://docs.github.com/en/github/building-a-strong-community/adding-or-editing-wiki-pages#adding-or-editing-wiki-pages-locally"
  >instructions for contributing to a wiki</a> that may help us make remote changes to the wiki. The
  wiki is a work in progress. If it doesn&rsquo;t help, we&rsquo;ll find a different way to document
  TCK questions and answers.
</p>
<p>
  2. The easiest way to get started is to git fork a Jakarta EE git repository. To do this, go to<a
    href="https://github.com/eclipse-ee4j/jakartaee-tck"
  > </a><a href="https://github.com/eclipse-ee4j/jakartaee-tck">github.com/eclipse-ee4j/jakartaee-tck</a>,
  then click on <strong>Watch</strong>, <strong>Star</strong>, and <strong>Fork</strong> near the
  top right of the browser screen:
</p>
<ul>
  <li>Click <strong>Star</strong> to show you like the repository or want to remember it.
  </li>
  <li>Click <strong>Watch</strong> and choose <strong>Watching</strong> to be notified of changes
    made to files in the repository.
  </li>
  <li>Click <strong>Fork</strong> to make a copy of the repository to your personal GitHub account.
    For example,<a href="https://github.com/scottmarlow/jakartaee-tck"> </a><a
    href="https://github.com/scottmarlow/jakartaee-tck"
  >github.com/scottmarlow/jakartaee-tck</a> is a fork of the<a
    href="https://github.com/eclipse-ee4j/jakartaee-tck"
  > </a><a href="https://github.com/eclipse-ee4j/jakartaee-tck">github.com/eclipse-ee4j/jakartaee-tck</a>
    repository.
  </li>
</ul>
<p>3. Set up a copy of your forked jakartaee-tck repository using the git clone command:
  <code>git@github.com:YourGitHubID/jakartaee-tck.git</code>. Progress will be indicated as shown in the example
  below:</p>
<ul>
  <li>Cloning into &#39;jakartaee-tck&#39;...</li>
  <li>remote: Enumerating objects: 4, done.</li>
  <li>remote: Counting objects: 100% (4/4), done.</li>
  <li>remote: Compressing objects: 100% (4/4), done.</li>
  <li>remote: Total 92888 (delta 0), reused 1 (delta 0), pack-reused 92884</li>
  <li>Receiving objects: 100% (92888/92888), 59.82 MiB | 27.20 MiB/s, done.</li>
  <li>Resolving deltas: 100% (71537/71537), done.</li>
</ul>
<p>
  4. Join other<a href="https://jakarta.ee/connect/mailing-lists/"> </a><a
    href="https://jakarta.ee/connect/mailing-lists/"
  >Jakarta EE mailing lists</a> so you can monitor and participate in the discussions.
</p>
<p>
  For example, if you subscribe to<a href="https://accounts.eclipse.org/mailing-list/jpa-dev"> </a><a
    href="https://accounts.eclipse.org/mailing-list/jpa-dev"
  >jpa-dev</a>, you can participate in developing the Jakarta Persistence specification. You can
  also participate in discussions about new APIs and voice concerns about issues with current APIs.
</p>
<p>5. Look for other Jakarta EE git repositories you can contribute to:</p>
<ul>
  <li>Go to<a href="https://github.com/eclipse-ee4j"> </a><a href="https://github.com/eclipse-ee4j">github.com/eclipse-ee4j</a>.
  </li>
  <li>Click <strong>Star</strong> on the projects you like.
  </li>
  <li>Fork the projects you would like to contribute to. We have a lot of documentation, code, and
    scripts that need your help.</li>
</ul>
<p>6. Sign the Eclipse Contributor Agreement with the same email address you use for GitHub:</p>
<p>
  Go to<a href="https://www.eclipse.org/legal/ECA.php"> </a><a
    href="https://www.eclipse.org/legal/ECA.php"
  >eclipse.org/legal/ECA.php</a> and read the agreement.
</p>
<p>
  Click the<a href="https://accounts.eclipse.org/user/eca"> </a><a
    href="https://accounts.eclipse.org/user/eca"
  >ECA form link</a>.
</p>
<p>When making changes locally to your fork of a GitHub repository, be sure to sign all git commits.
  For example: <code>git commit -s -a -m&quot;add new test for Jakarta Persistence transaction timeout
  handling</code>&quot;.</p>
<h2>If You Only Do One Thing</h2>
<p>
  Remember to join the<a href="https://accounts.eclipse.org/mailing-list/jakartaee-tck-dev"> </a><a
    href="https://accounts.eclipse.org/mailing-list/jakartaee-tck-dev"
  >Jakarta EE Platform TCK mailing list</a> and other<a
    href="https://jakarta.ee/connect/mailing-lists/%20mailing%20lists"
  > </a><a href="https://jakarta.ee/connect/mailing-lists/%20mailing%20lists">Jakarta EE mailing
    lists</a> so you can ask questions, follow, and participate in the future development of Jakarta
  EE.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="<?php print $pageAuthor; ?>"
						src="images/scott.jpg" />
				</div>
				<div class="col-sm-16">
					<p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
            <a href="https://projects.eclipse.org/user/3326">Scott Marlow</a> is principal software
            engineer of the <a href="https://wildfly.org/">WildFly</a>/<a
              href="https://www.redhat.com/en/technologies/jboss-middleware/application-platform"
            >JBoss Enterprise Application Platform</a> team at Red Hat and is the Jakarta EE
            Platform TCK Project Lead.
          </p>
        </div>
			</div>
		</div>
	</div>
</div>