<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Miro Spönemann</h2>
<img width="185" class="float-left margin-right-40 img img-responsive" src="images/miro-sponemann.png" alt="Miro Spönemann">
<ul>
  <li>Involved in open source since: 2008</li>
  <li>Works for: TypeFox in Kiel, Germany</li>
  <li>Eclipse Foundation contributor since: 2014</li>
  <li>Involved in: Eclipse Open VSX, Eclipse Sprotty, Eclipse Theia, Eclipse Layout Kernel, Eclipse Xtext, Eclipse LSP4J</a></li>
  <li>Committer to: <a href="https://projects.eclipse.org/user/4867">All six Eclipse Foundation projects in which he is involved</a></li>
  <li>Committer since: 2014</li>
  <li>Fun fact: Miro and his wife plan to release their recordings of traditional German Christmas songs on Spotify at the end of November 2020</li>
</ul>
<hr />
<h2>Why did you first get involved in open source software and OSS communities?</h2>
<p>When I wrote my university diploma thesis, the group I was working with started using some of the tools in the <a href="https://projects.eclipse.org/projects/modeling">Eclipse Modeling Project</a> and I found that very interesting &mdash; especially the graphical modeling framework and <a href="https://projects.eclipse.org/projects/modeling.tmf.xtext">Eclipse Xtext</a>, a domain-specific language framework. We used these tools for our teaching and research and it really got me interested.</p>
<p>After I finished my work at the university and completed my PhD in 2014, I looked for a company that would allow me to do open source software development. I found one in Itemis, where there was a great opportunity to get involved in the Eclipse Xtext project. That involvement helped me learn about the processes at the Eclipse Foundation and understand how things work.</p>

<h2>How did that involvement lead to you becoming a committer?</h2>
<p>Quite soon after I started working at Itemis, I also started contributing to Eclipse Xtext and was very quickly asked to join the team of committers.</p>
<p>As time passed, we started more projects at the Eclipse Foundation. For many projects, I was not only a committer, but also a co-founder. I think there are actually more projects that I helped to get started than projects I joined after they started.</p>

<h2>How would you summarize your experiences as a committer?</h2>
<p>I really like that by committing code to an open source project, I can then reuse those features and contributions in other projects I&#39;m involved in, such as closed source projects with customers. I can see a direct impact in the open source world, and also in real-world applications. That&#39;s something I find very valuable and rewarding.</p>
<p>One very challenging thing is it&#39;s quite hard to find time to write documentation and I see open source projects that are becoming stale because of the lack of documentation. A more personal aspect is that I regard software development as a highly creative process, and it is often hard for me to get into other creative expressions, such as music and photography, after eight hours of work.</p>
<p>But, developing in the open source world really motivates me because I appreciate the openness and transparency of what I&#39;m doing. I think whatever happens in my career I will quite likely want to stay in the context of open source software.</p>

<h2>
  What are your next steps and goals as a committer and Eclipse Foundation community member?
</h2>
<p>Currently, my most active project is <a href="https://projects.eclipse.org/projects/ecd.openvsx#:~:text=Eclipse%20Open%20VSX%20is%20a,line%20tool%20for%20publishing%20extensions.">Eclipse Open VSX</a>, the new registry for VS Code extensions. We started the project at TypeFox and now want to hand it over to the Eclipse Foundation. I will help transfer the service and website to Eclipse Foundation infrastructure.</p>

<p>A few months ago, I submitted a change to VSCodium so it now uses Eclipse Open VSX as its main registry for extensions. Since then, we&rsquo;ve seen a lot of traction with users and contributors wanting to push their extensions to Eclipse Open VSX. I want to continue to support that community and see how we can best meet all the different needs.</p>

<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>
<p>My main advice would be to find a great company that gives you the flexibility to be involved in open source software during work time. It's much better than just doing it at night.</p>
<p>
It also gives you the opportunity to use your own open source code for your work. That adds value to your company through the open source projects, which is really great.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
