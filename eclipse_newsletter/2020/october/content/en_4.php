<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Autonomous vehicles have been making headlines for a number of years now. As the automotive industry continues to transform itself, we can expect the trend to grow. According to a 2019 McKinsey report[1], automotive software and electronics will outgrow the automotive market overall by 2030 &mdash; driven primarily by the shift toward autonomous, connected, electric, and shared vehicles. A lesser known fact is that the Eclipse Foundation is very active in the automotive industry.</p>

<p>Eclipse Foundation involvement in the automotive industry includes working groups and open source projects for automotive software development, as well as the organizational structure needed for community collaboration. While a number of general Eclipse Foundation projects, such as<a href="https://www.eclipse.org/jdt/">Eclipse Java Development Tools (JDT)</a>, <a href="https://www.eclipse.org/cdt/">Eclipse C/C++ Development Tooling (CDT)</a>, and many others, are used in automotive software development, this article focuses on automotive-specific activities at the Eclipse Foundation.</p>

<h2>Working Groups Provide Organizational Umbrella for Multi-Company Cooperation</h2>

<p>The Eclipse Foundation working group structure makes it easy for different parties to create a legally sound framework to exchange information, jointly develop software and concepts, and set up different models for financing the activities.</p>

<p>Several years ago, a number of companies joined the <a href="https://wiki.eclipse.org/Auto_IWG">Auto IWG</a> at the Eclipse Foundation, an open source initiative that fostered exchanges around using the <a href="https://projects.eclipse.org/projects/eclipse.platform">Eclipse Platform</a> in automotive software engineering. Over time, it became clear that collaboration works better when working groups focus on specific topics. As a result, a number of automotive-related working groups are now hosted at the Eclipse Foundation:</p>

<h3>OpenADX Working Group</h3>
<p>The <a href="https://openadx.eclipse.org">OpenADx Working Group</a> delivers software tools and open source software for autonomous driving (AD) development by defining open interface specifications for the software used in vehicle-based systems and in testing environments where cloud services are typically used to build software, run virtual tests, and collect results. The name OpenADx stands for Open, Autonomous Driving Accelerator.</p>

<h3>OpenPASS Working Group</h3>
<p>The <a href="https://openpass.eclipse.org/">OpenPASS Working Group</a> promotes a collaborative and innovative ecosystem by offering tools, systems, and adapters for a standardized, openly available, and vendor-neutral platform for traffic scenario simulations. The simulations predict the real-world effectiveness of advanced driver assistance system (ADAS) and AD functions.</p>

<h3>OpenMobility Working Group</h3>
<p>The <a href="https://openmobility.eclipse.org/">OpenMobility Working Group</a> drives the evolution and broad adoption of mobility modeling and traffic simulation technologies for testing autonomous driving functions, developing new mobility solutions, and creating digital twins of urban areas. The working group shapes and fosters the development of software tools and frameworks based on validated mobility models to provide a common platform for industrial applications and academic research.</p>

<h3>OpenMDM Working Group</h3>
<p>The <a href="https://www.openmdm.org/">OpenMDM&reg; Working Group</a> fosters and supports an open and innovative ecosystem for providing tools and systems, qualification kits, and adapters for standardized and vendor-independent management of measurement data in accordance with the Association for Standardization of Automation and Measuring Systems (ASAM) Open Data Services (ODS) standard.</p>

<h2>Hosted Projects Are Widely Used in the Automotive Industry</h2>

<p>The first projects adopted by the automotive industry were related to the <a href="https://www.eclipse.org/modeling/">Eclipse Modeling Project.</a> Many companies use these projects to build their in-house or commercial tool offerings. The basis of this ecosystem is the <a href="https://www.eclipse.org/modeling/emf/">Eclipse Modeling Framework (EMF)</a>, which provides a rich feature set for developing modelling tools. Many other projects build on these technologies to provide graphic and text editors, code generators, configuration management, and other functionality.</p>

<p>Together, all of these projects laid the foundation for the AUTOSAR Tool Platform (Artop), an automotive industry collaboration outside of the Eclipse Foundation.</p>

<p>The automotive industry started developing the AUTOSAR common standard around 2003. The standard includes a very complex meta model to describe all of a vehicle&rsquo;s software-relevant aspects. Artop provides an implementation of this meta model that is based on technologies at the Eclipse Foundation and is managed and maintained by a consortium. This tool platform saves companies uncountable man hours of effort when implementing the AUTOSAR standard &mdash; savings that would not be possible without the Eclipse Modeling Project ecosystem.</p>

<p>The Eclipse Foundation also hosts the <a href="https://www.eclipse.org/app4mc/">Eclipse APP4MC project</a>, an application platform for engineering embedded multi- and many-core software systems. The platform enables the creation and management of complex tool chains including simulation and validation. Users can distribute data and tasks to the target hardware platforms with a focus on optimizing timing and scheduling. This open platform is proven in the automotive industry by Bosch and its partners to support interoperability and extensibility and to unify data exchanges in cross-organizational projects.</p>

<h2>Eclipse Foundation Hosts More Than Tooling Projects for the Automotive Industry</h2>

<p>Tooling projects, such as the Eclipse Modeling project and the Eclipse APP4MC project, are expected based on the Eclipse Foundation history in such projects. However, the Eclipse Foundation also hosts projects that are directly related to development of in-vehicle or vehicle-to-anything (V2X) communications software and embedded systems:</p>

<h3>Eclipse Kuksa</h3>
<p>The open and secure <a href="https://www.eclipse.org/kuksa/">Eclipse Kuksa</a> project provides a cloud platform that interconnects a wide range of vehicles to the cloud through in-car and internet connections. This platform is supported by an integrated, open source software development environment that includes technologies to address the software challenges for vehicles designed in the IoT, cloud, and digital era.</p>

<h3>Eclipse iceoryx</h3>
<p><a href="https://projects.eclipse.org/projects/technology.iceoryx">Eclipse iceoryx</a> is middleware with a zero-copy shared memory approach that is optimized for the huge volumes of data that must be transferred between different parts of the system in domains such as automotive, robotics, and gaming. The project provides algorithms and libraries for shared memory communications on Portable Operating System Interface (POSIX)-based operating systems, such as Linux.</p>

<p>The newest automotive-related project at the Eclipse Foundation addresses a major issue in autonomous vehicle development: Simulation. It is impossible to validate every autonomous function in on-road vehicles. We need virtualization technologies that allow vehicles to be simulated, distributed, and in the cloud, with real timing and physics models.</p>

<p><a href="https://projects.eclipse.org/projects/technology.mosaic">Eclipse MOSAIC</a> lays the foundation for the open source infrastructure to achieve these requirements.</p>

<p>The Eclipse MOSAIC co-simulation environment includes a runtime infrastructure (RTI) for coupling discrete event simulators from multiple domains. It also includes a collection of pre-packaged simulators for specific simulations. To meet the standardized coupling principles in the IEEE High Level Architecture (HLA), simulators are embedded in a federation and the interface to the RTI is achieved by an ambassador.</p>

<h2>A Bright Outlook for Automotive Technologies at the Eclipse Foundation</h2>

<p>I started using Eclipse Modeling projects for in-house tooling at BMW almost 15 years ago, and it&rsquo;s really amazing to see how far the Eclipse Foundation and the associated projects and working groups have developed since then.</p>

<p>A lot has changed and I am happy and proud to have contributed on behalf of itemis to the original automotive working group, <a href="https://www.eclipse.org/sphinx/">Eclipse Sphinx</a>, and now through participation in the OpenADX Working Group. There is growing acceptance of open source in the automotive industry, through consumption of open source software and through contributions.</p>

<p>Many players in the automotive industry have become Eclipse Foundation members, including BMW Group, Bosch, Denso Corporation, AVL List, and Volkswagen America, to name just a few. I expect the <a href="https://blogs.eclipse.org/post/mike-milinkovich/update-eclipse-foundation%E2%80%99s-move-europe">Eclipse Foundation move to Europe</a> will increase its attractiveness to the industry and it will be interesting to be part of that further development.</p>

<h2>Learn More and Get Involved</h2>

<p>Follow the links to the Eclipse Foundation working groups and projects described in this article to learn more about what&rsquo;s happening in open source software for the automotive industry, how you can use the technologies in your next project, and how to get more involved in Eclipse Foundation automotive communities.</p>
<hr />
<p>[1]<a href="https://www.mckinsey.com/~/media/mckinsey/industries/automotive%20and%20assembly/our%20insights/mapping%20the%20automotive%20software%20and%20electronics%20landscape%20through%202030/automotive-software-and-electronics-2030-final.ashx">Automotive software and electronics 2030: Mapping the sector&rsquo;s future landscape</a>. McKinsey &amp; Company, July 2019.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>

<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/andreasgraf.jpg" />
				</div>
				<div class="col-sm-16">
					<p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Andreas Graf is the business development manager for automotive and new business segments at itemis in Germany, an active participant in the <a href="https://openadx.eclipse.org/">OpenADx Working Group</a>, and a committer to the <a href="https://www.eclipse.org/rmf/">Eclipse Requirements Modeling Framework</a> project.
          </p>
        </div>
			</div>
		</div>
	</div>
</div>
