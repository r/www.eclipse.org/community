<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The OpenHW Group is a non-profit, global organization where industry experts collaborate to develop the CORE-V family of RISC-V-based open source cores and related intellectual property, software, and tools. To work on all of these different aspects, application developers need an integrated frontend. That&rsquo;s where an IDE comes into play and why the OpenHW Group is developing the CORE-V IDE with Eclipse Foundation development tools.</p>

<p>The OpenHW Group has a close partnership with the Eclipse Foundation. All <a href="https://projects.eclipse.org/projects/openhw">OpenHW Group projects</a> are hosted at the Eclipse Foundation, and we&rsquo;re integrating a number of the open source technologies hosted at the Eclipse Foundation into the OpenHW Group CORE-V IDE.</p>

<p>An open source approach to the CORE-V IDE makes sense. Today, open source software is being used across many different technologies. And it&#39;s not just about the software. We can see open source benefiting all spheres of life: science, education, government, manufacturing, health, law, and many others. From small startups to large enterprises, companies are choosing open source because it provides high flexibility, lower cost, and greater control. Hardware development is no exception to this trend.</p>

<p>Before I explain how we&rsquo;re using the open source technologies at the Eclipse Foundation, I want to take a closer look at what we should expect from an IDE.</p>

<h2>IDEs Should Be Configurable, Extendable, Scalable</h2>

<p>Some people have the opinion that an IDE is not needed at all. If a short list of shell commands works well for your program, I can agree with that belief. However, if your source files grow for any reason, it becomes harder and harder to manage them without a proper IDE. That doesn&rsquo;t mean the IDE should neglect command-line scenarios. Ideally, all batch operations are available in &ldquo;headless&rdquo; mode.</p>

<p>The IDE should be fully configurable and it should help developers focus on the current activity. It should also have enough preferences so developers can pick the right strategy for event and data processing. Here, I could mention artificial intelligence and machine learning, but let&rsquo;s think about &ldquo;what&rdquo; first and select the right &ldquo;how&rdquo; later.</p>

<p>The IDE also needs to be extendable. Developers should be able to install support for more hardware, toolchains, debuggers, software development kits (SDKs), and other tools the community can contribute to and vendors can offer. Yes, both free and commercial tools.</p>

<p>Also, we can no longer ignore the scalability problem. Modern systems on a chip (SoCs) bring this challenge, which wasn&#39;t typical in the embedded world just a few years ago. Huge headers generated from hardware design and powerful SDKs may exceed all of our expectations about the resources required to parse and represent the compilation unit.</p>

<h2>Open Source Projects at the Eclipse Foundation Jump-Start IDE Development</h2>

<p>Fortunately, we don&rsquo;t need to start CORE-V IDE development from scratch because the Eclipse Foundation ecosystem has already developed a lot of the required functionality.</p>

<p>Figure 1 shows the technology components we&rsquo;ve already integrated in the OpenHW Group CORE-V IDE to establish the starting point of our story, with more to come.</p>

<p><span style="font-size:12px">Figure 1: Technology Components in the OpenHW Group CORE-V IDE</span></p>
<p><img class="img-responsive" src="images/3-1.png"></p>

<p>The <a href="https://projects.eclipse.org/projects/eclipse.platform">Eclipse Platform project</a> gives us the Open Service Gateway Initiative (OSGi)-based extensible runtime and p2 update engine without binding to a specific language. We can use:</p>

<ul>
	<li>Workspaces to manage files, folders, and project metadata</li>
	<li>Project Natures and Builders to distinguish and configure content processing</li>
	<li>The Eclipse <a href="https://wiki.eclipse.org/SWT">Standard Widget Toolkit (SWT)</a> and <a href="https://wiki.eclipse.org/JFace">JFace</a> to create reusable user interfaces</li>
	<li>Workbench models to organize activities</li>
	<li>Rich user assistance capabilities such as context-sensitive help, cheat sheets, and more</li>
</ul>

<p>The <a href="https://www.eclipse.org/modeling/emf/">Eclipse Modeling Framework (EMF) project</a> is a modeling framework and code generation facility for building tools and other applications based on a structured data model. Everything with a declarative nature can be effectively managed by this technology. It is widely used in the Eclipse Foundation ecosystem, and seems to have great potential to be expanded to more domains.</p>

<p>The <a href="https://www.eclipse.org/cdt/">Eclipse C/C++ Development Tooling (CDT) project</a> implements the language services for C/C++ projects. We can use this technology to make the leap from a color editor to a fully functional C/C++ IDE that many vendors use as the basis for their products. It is far beyond the scope of this article to enumerate all the Eclipse CDT features, but I do need to mention the extraordinary capabilities for project configuration, rich visualization of debug information, and seamless integration with command line tools.</p>

<p>The <a href="https://eclipse-embed-cdt.github.io/">Eclipse Embedded CDT project</a> adds numerous out-of-the-box project templates to support different boards, toolchains, and debuggers.</p>

<h2>Get Involved in OpenHW Group CORE-V IDE Development</h2>

<p>We welcome everyone who wants to participate and help us in our efforts.</p>

<ol>
	<li>Here&rsquo;s how you can get started in five simple steps:</li>
	<li>Create an account at <a href="https://www.eclipse.org/">www.eclipse.org</a>.</li>
	<li>Sign the <a href="https://www.eclipse.org/legal/ECA.php">Eclipse Contributor Agreement</a> electronically.</li>
	<li>Specify your GitHub id in your Eclipse profile.</li>
	<li>Fork <a href="https://github.com/openhwgroup/core-v-ide-cdt">github.com/openhwgroup/core-v-ide-cdt</a>.</li>
	<li>Don&rsquo;t forget to add &ldquo;Signed-off-by&rdquo; to your commit message.</li>
</ol>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/alexanderf.jpg" />
				</div>
				<div class="col-sm-16">
					<p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Alexander Fedorov is the founder and CEO of ArSysOp, an active Open Source contributor, and conference speaker. After 20 years of working in the area of IDE and tools with TogetherSoft, Borland, Micro Focus, Embarcadero, NXP, and others he switched to consulting on architecture and development for Eclipse-based solutions.
          </p>
        </div>
			</div>
		</div>
	</div>
</div>