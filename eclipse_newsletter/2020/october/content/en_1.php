<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included

if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>After nine months of incubation, the <a href="https://www.eclipse.org/jkube/">Eclipse JKube</a> 1.0.0 General Availability (GA) release is now <a href="https://download.eclipse.org/jkube/">available for download</a>.</p>

<p>Eclipse JKube is a collection of plug-ins plus a standalone Java library that are used to:</p>

<ul>
	<li>Build container images using Docker, Java Image Builder (Jib), or Source-to-Image (S2I) build strategies</li>
	<li>Create resource descriptors for Kubernetes or Red Hat OpenShift</li>
	<li>Deploy resource descriptors</li>
</ul>

<p>If you have a Java project that will be deployed in a Kubernetes or a Red Hat OpenShift environment, this is the right tool for you. Eclipse JKube takes care of everything related to cluster deployment so you, the developer, can concentrate on implementing your application without worrying about where it needs to be deployed.</p>

<h2>A Replacement for the Fabric8 Maven Plug-In Project</h2>

<p>The Eclipse JKube 1.0.0 GA release also marks the final deprecation of the great Fabric8 Maven Plug-in (FMP) project, and it&rsquo;s only possible thanks to the hard work and code originally developed by the Fabric8 Maven Plug-in Project Team.</p>

<p>Eclipse JKube is a complete replacement for the FMP project and includes all of its major features as well as new features, bug fixes, and upstream project maintenance capabilities.</p>

<p>If your project relies on the FMP project to create containers based on Apache Maven Java projects, you can now <a href="https://www.eclipse.org/jkube/docs/migration-guide/">migrate to Eclipse JKube 1.0.0</a> to take advantage of its new features, including:</p>

<ul>
	<li>Support for the S2I build strategy for all of our generators</li>
	<li>Support for Jib (Docker-less) build and push</li>
	<li>Separate plug-ins for Kubernetes and OpenShift, including specific resources and build strategies for OpenShift</li>
</ul>

<p>In addition, all base images are based on Java 11.</p>

<h2>A Quick Overview of Eclipse JKube</h2>

<p>Eclipse JKube provides Apache Maven plug-ins with specific goals and a set of developer tools for meeting them. Goals include:</p>

<ul>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:build">Building container images</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:build">Pushing images to remote registries</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:resource">Generating cluster resource configurations</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:apply">Deploying resource configurations in your cluster</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:log">Checking container logs</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:watch">Watching for project changes</a></li>
	<li><a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin#jkube:debug">Debugging Java applications in the cloud</a></li>
</ul>

<p>Once the plug-in is configured, you can use Eclipse JKube to build Java container images using the build strategies listed above. Just add our <a href="https://www.eclipse.org/jkube/docs/kubernetes-maven-plugin">kubernetes-maven-plugin</a> or <a href="https://www.eclipse.org/jkube/docs/openshift-maven-plugin">openshift-maven-plugin</a> dependency and you&rsquo;re ready to build and deploy your application into your Kubernetes or OpenShift cluster.</p>

<p><span style="font-size:12px">Figure 1: How to Include the Kubernetes Maven Plug-In in Your pom.xml File</span></p>
<pre>
<code>
&lt;build&gt;
&nbsp; &lt;plugins&gt;
&nbsp; &lt;plugin&gt;
&nbsp; &lt;groupId&gt;org.eclipse.jkube&lt;/groupId&gt;
&nbsp; &lt;artifactId&gt;kubernetes-maven-plugin&lt;/artifactId&gt;
&nbsp; &lt;version&gt;1.0.0&lt;/version&gt;
&nbsp; &lt;/plugin&gt;
&nbsp; &lt;/plugins&gt;
&lt;/build&gt;
</code>
</pre>
<p><span style="font-size:12px">Figure 2: How to Build and Deploy Your Java Project From the Command Line</span></p>

<pre>
<code>
$ mvn clean package k8s:build k8s:resource k8s:apply
</code>
</pre>

<p>The Kubernetes Maven plug-in:</p>

<ul>
	<li>Generates container images with flexible and powerful configurations that integrate with the Docker daemon service or Jib</li>
	<li>Generates generic Kubernetes descriptors as YAML Ain&rsquo;t Markup Language (YAML) files</li>
	<li>Provides zero configuration for quick ramp-up in cases where opinionated defaults will be pre-selected</li>
	<li>Provides inline configuration within the plug-in configuration using XML syntax</li>
	<li>Provides external configuration templates for real deployment descriptors that are enriched by the plug-in</li>
</ul>

<p>The OpenShift Maven plug-in is built on top of the Kubernetes Maven plug-in. It provides all of the configuration capabilities in the Kubernetes Maven plug-in as well as features that are specific to OpenShift. For example, it:</p>

<ul>
	<li>Manages S2I images, inheriting the S2I tool&rsquo;s flexible and powerful configuration capabilities</li>
	<li>Generates OpenShift descriptors as YAML files</li>
</ul>

<p>The Eclipse JKube software infers its configuration from opinionated defaults that work for most Java applications. If this setup is not suitable for your project, you can always customize the plug-in to suit your specific project requirements.</p>

<h2>Learn More</h2>

<p>To learn more about Eclipse JKube, visit the <a href="https://www.eclipse.org/jkube/">project website</a>, check our <a href="https://www.eclipse.org/jkube/quickstarts/">quickstarts and examples</a>, and take the <a href="https://katacoda.com/jkubeio">Katacoda courses</a>.</p>

<p>You can also reach us on <a href="https://gitter.im/eclipse/jkube?utm_source=badge&amp;utm_medium=badge&amp;utm_campaign=pr-badge">Gitter</a>, and don&rsquo;t forget to follow us on Twitter, <a href="https://twitter.com/jkubeio">@jkubeio</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/markn.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
		  Marc Nuri is an open source enthusiast and software developer. Currently, Marc is working as a senior software engineer at Red Hat in the Developer Tools team focusing on Java. He leads the development efforts of Eclipse JKube and is part of the core maintainer team for the Fabric8 Kubernetes client.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>