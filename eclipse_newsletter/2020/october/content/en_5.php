<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Eclipse Embedded C/C++ Development Tools (CDT) Version 5.1.2 Is Now Available</h2>
<p>The <a href="https://eclipse-embed-cdt.github.io/">Eclipse Embedded CDT</a> project provides a set of Eclipse IDE plug-ins and tools that allow you to create, build, debug, and manage 32-bit and 64-bit Arm and RISC-V executables and libraries within the Eclipse IDE framework. The project was formerly known as GNU MCU/ARM Eclipse. It is now part of the Eclipse Foundation and is hosted on <a href="https://github.com/eclipse-embed-cdt">GitHub</a>.</p>

<p>Eclipse Embedded CDT Version 5.1.2 is a maintenance release that includes bug fixes and enhancements. For convenience, this version is also available as Eclipse packages that combine the Eclipse IDE for C/C++ Developers standard distribution with the Eclipse Embedded CDT plug-ins.</p>

<p>For complete details about the Eclipse Embedded CDT technologies, visit the <a href="https://eclipse-embed-cdt.github.io/">project website</a>.</p>

<p>For more insight into the Version 5.1.2 release and quick access to download links, click <a href="https://projects.eclipse.org/projects/iot.embed-cdt/releases/5.1.2">here</a>.</p>
<hr />

<h2>Register Today: JakartaOne Livestream Is December 8</h2>
<p>Take a minute to <a href="https://www.crowdcast.io/e/jakartaonelivestream_dec8/register">register</a> for our one-day virtual conference on the future of Jakarta EE and related technologies for developing cloud native Java applications. This year&rsquo;s event is being held December 8 to coincide with the release of Jakarta EE 9.</p>

<p>All of the proposed session topics have been submitted and our independent program committee is working hard to put together an interesting and informative day that will appeal to developers and technical business leaders.</p>

<p>For more information about JakartaOne Livestream, click <a href="https://jakartaone.org/2020/">here</a>.</p>

<p>To reserve your spot, click <a href="https://www.crowdcast.io/e/jakartaonelivestream_dec8/register">here</a>.</p>

<p>For live event updates, speaker announcements, news, and more, follow us on Twitter <a href="https://twitter.com/JakartaOneConf">@JakartaOneConf</a>.</p>
<hr />

<h2>Read the Eclipse Kuksa Case Study</h2>
<p>Our recently published case study on the <a href="https://www.eclipse.org/kuksa/">Eclipse Kuksa</a> project explores how the technology breaks down silos in the automotive industry to provide an open source platform for vehicle-to-cloud connectivity.</p>

<p>Eclipse Kuksa unifies vehicle, IoT, cloud, and security technologies across the complete tooling stack for the connected vehicle domain to enable a standardized approach to vehicle-to-cloud scenarios across all vehicles. Ecosystem participants include academic institutions and corporations that do business in the automotive industry.</p>

<p>In the case study, Eclipse Kuksa project Co-Lead, Johannes Kristan, discusses the technology&rsquo;s roots in the publicly funded APPSTACLE research project, its strategic move to the Eclipse Foundation, and how ecosystem participants are using the technology to demonstrate real-world applications.</p>

<p>Download the case study PDF <a href="https://f.hubspotusercontent10.net/hubfs/5413615/Kuksa.Case.Study.pdf">here</a>.</p>
<hr />

<h2>Learn How Eclipse Che4z Is Opening Mainframes to More Developers</h2>
<p>The <a href="https://projects.eclipse.org/projects/ecd.che.che4z">Eclipse Che4z</a> project extends <a href="https://www.eclipse.org/che/">Eclipse Che</a> capabilities to bring the modern developer experience to mainframe software development.</p>

<p>With Eclipse Che4z, on-boarding developers who are new to the world of mainframe application development is faster, easier, and more cost-effective. Single-click workspace provisioning allows developers to access mainframe artifacts, edit, build, and test them in the same way as a Java, .NET, or web application. And IT organizations can reduce maintenance costs because there&rsquo;s no developer client application. All Eclipse Che4z technologies are centrally hosted.</p>

<p>Eclipse Che4z project Co-Lead, Venkat Balabhadrapatruni, provides more insight into Eclipse Che4z and its benefits in this <a href="https://www.share.org/blog/eclipse-che4z-opening-the-mainframe-to-more-developers">article</a>.</p>
<hr />

<h2>Check Out Our New Events Page</h2>
<p>The <a href="https://events.eclipse.org/">Eclipse Foundation Events</a> page has a new look that makes it easier than ever to keep track of Eclipse Foundation events as well as events throughout the broader open source community.</p>

<p>You can sort events by type and category to narrow your search, clearly spot event dates and times, and easily share your own event with the open source community.</p>

<p>To check the list of upcoming events, click <a href="https://events.eclipse.org/">here</a>.</p>

<p>To submit your event, start <a href="https://accounts.eclipse.org/user/login?destination=oauth2/authorize">here</a>.</p>
<hr />

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
