<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

$pageTitle = "New Research at Eclipse Foundation";
$pageKeywords = "";
$pageAuthor = "Christopher Guindon";
$pageDescription = "";

// Make it TRUE if you want the sponsor to be displayed
$displayNewsletterSponsor = FALSE;

// Sponsors variables for this month's articles
$sponsorName = "";
$sponsorLink = "";
$sponsorImage = "";

  // Set the breadcrumb title for the parent of sub pages
$breadcrumbParentTitle = $pageTitle;