<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>

<table border="0" cellpadding="0" cellspacing="0" width="100%"
	class="templateContainer"
	style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0; max-width: 600px !important;">
	<tbody>
		<tr>
			<td valign="top" id="templatePreheader"
				style="background: #fafafa none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fafafa; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;"><table
					border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnTextBlockOuter">
						<tr>
							<td valign="top" class="mcnTextBlockInner"
								style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									width="100%" class="mcnTextContentContainer">
									<tbody>
										<tr>

											<td valign="top" class="mcnTextContent"
												style="padding: 0px 18px 9px; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #656565; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 150%;">

												<div style="text-align: right;">
													<span style="font-size: 12px"><a
														href="https://us6.campaign-archive.com/?e=[UNIQID]&amp;u=eaf9e1f06f194eadc66788a85&amp;id=f33cda1c44"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #656565; font-weight: normal; text-decoration: underline;">View
															this email in your browser</a></span>
												</div>

											</td>
										</tr>
									</tbody>
								</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table></td>
		</tr>
		<tr>
			<td valign="top" id="templateHeader"
				style="background: #3d3935 url(&amp;quot;https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png&amp;quot;) no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: url(https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png); background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 0;"><table
					border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnTextBlockOuter">
						<tr>
							<td valign="top" class="mcnTextBlockInner"
								style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									width="100%" class="mcnTextContentContainer">
									<tbody>
										<tr>

											<td valign="top" class="mcnTextContent"
												style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 150%; text-align: left;">

												<h4 class="null"
													style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-style: normal; font-weight: normal; line-height: 125%; letter-spacing: normal; text-align: left;">
													Eclipse Newsletter - 2020.22.07<br> &nbsp;
												</h4>

												<h1 class="null"
													style="text-align: center; display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 26px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal;">Research
													@ Eclipse: Industrial Automation</h1> <br> &nbsp;
											</td>
										</tr>
									</tbody>
								</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table></td>
		</tr>
		<tr>
			<td valign="top" id="templateUpperBody"
				style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;"><table
					border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnBoxedTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
					<tbody class="mcnBoxedTextBlockOuter">
						<tr>
							<td valign="top" class="mcnBoxedTextBlockInner"
								style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

								<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									width="100%"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									class="mcnBoxedTextContentContainer">
									<tbody>
										<tr>

											<td
												style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

												<table border="0" cellspacing="0"
													class="mcnTextContentContainer" width="100%"
													style="min-width: 100% !important; background-color: #3D3935; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
													<tbody>
														<tr>
															<td valign="top" class="mcnTextContent"
																style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;">
																<h2 class="null"
																	style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">Editor's
																	Note</h2>

															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnCaptionBlock"
					style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnCaptionBlockOuter">
						<tr>
							<td class="mcnCaptionBlockInner" valign="top"
								style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">




								<table border="0" cellpadding="0" cellspacing="0"
									class="mcnCaptionRightContentOuter" width="100%"
									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody>
										<tr>
											<td valign="top" class="mcnCaptionRightContentInner"
												style="padding: 0 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
												<table align="left" border="0" cellpadding="0"
													cellspacing="0"
													class="mcnCaptionRightImageContentContainer" width="176"
													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
													<tbody>
														<tr>
															<td class="mcnCaptionRightImageContent" align="center"
																valign="top"
																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">



																<img alt=""
																src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/6f70d140-a482-4aba-a365-26b08ce41ddc.png"
																width="176"
																style="max-width: 400px; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; vertical-align: bottom;"
																class="mcnImage">



															</td>
														</tr>
													</tbody>
												</table>
												<table class="mcnCaptionRightTextContentContainer"
													align="right" border="0" cellpadding="0" cellspacing="0"
													width="352"
													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
													<tbody>
														<tr>
															<td valign="top" class="mcnTextContent"
																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																The Eclipse Foundation partners in research projects
																from various domains, including Internet of Things
																(IoT), modelling, cloud technologies, robotics,
																industrial automation, automotive, and many more.<br>
																&nbsp;<br> In this year’s edition of the research
																newsletter, we focus on industrial automation, featuring
																European research projects that will shape the future of
																digital factories. We also look at an initiative to
																bring together research projects and industrial partners
																in a new Eclipse Working Group.
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>




							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnTextBlockOuter">
						<tr>
							<td valign="top" class="mcnTextBlockInner"
								style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									width="100%" class="mcnTextContentContainer">
									<tbody>
										<tr>

											<td valign="top" class="mcnTextContent"
												style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">

												<h3 class="null"
													style="display: block; margin: 0; padding: 0; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">Spotlight
													Articles</h3>

												<p
													style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">Our
													spotlight articles highlight three projects that, in
													addition to their value for industrial automation,
													demonstrate why an open source strategy is key to
													sustaining research results and sharing the benefits of
													research with the European community and the rest of the
													world.</p>

												<ul>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">Anna
														Principato and Luz Martinez provide <a
														href="https://www.eclipse.org/community/eclipse_newsletter/2020/july/3.php"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">an
															overview of the Horizon 2020 RobMoSys Project,</a> which
														has successfully created two well-recognized modelling
														tools for robotics application developers:<a
														href="https://projects.eclipse.org/projects/modeling.smartmdsd"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															Eclipse SmartMDSD</a> and<a
														href="https://www.eclipse.org/papyrus/components/robotics/"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															Eclipse Papyrus For Robotics</a>.
													</li>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
														href="https://www.eclipse.org/community/eclipse_newsletter/2020/july/2.php"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Daniel
															Stock and Frank Schnicke present FabOS, a research
															project </a>to develop an open, distributed, real-time
														capable, and secure operating system for production
														focused on artificial intelligence. FabOS builds on and
														extends<a href="https://www.eclipse.org/basyx/"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															Eclipse BaSyx</a>, which is a result of the BaSys 4.2
														research project.</li>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">Jerker
														Delsing and Pal Varga <a
														href="https://www.eclipse.org/community/eclipse_newsletter/2020/july/1.php"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">describe
															the creation of the</a><a
														href="https://projects.eclipse.org/projects/iot.arrowhead"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															Eclipse Arrowhead</a><a
														href="https://www.eclipse.org/community/eclipse_newsletter/2020/july/1.php"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															project</a> as a result of the Arrowhead Tools research
														project, which brings together more than 80 partners to
														drive digitalization of European industry.
													</li>
												</ul>

												<p
													style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
													&nbsp;<br> In addition, we look at our new Working Group
													initiative:
												</p>

												<ul>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
														href="https://www.eclipse.org/community/eclipse_newsletter/2020/july/4.php"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Marco
															Jahn reports on the Eclipse Foundation’s efforts </a>to
														channel industry and research initiatives into an Eclipse
														Working Group focused on open industrial automation.</li>
												</ul>

												<h3 class="null"
													style="display: block; margin: 0; padding: 0; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">News
													and Community Updates</h3>

												<p
													style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
													Finally, <a href="/community/eclipse_newsletter/2020/july/5.php">read our News section</a> to find out why June was a
													big month for Jakarta EE.<br> &nbsp;<br> And, be sure to
													register for these upcoming free, virtual events:
												</p>

												<ul>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">September
														17-18:<a href="https://events.eclipse.org/2020/sam-iot/"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															SAM IoT</a>, the first virtual Eclipse Conference on
														Security, Artificial Intelligence, and Modeling for
														next-generation IoT.
													</li>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">October
														19-22:<a href="https://www.eclipsecon.org/2020"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															EclipseCon 2020</a> is our annual conference and<a
														href="https://www.eclipsecon.org/2020/registration"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">
															registration is now open</a>. Connect with
														Research@Eclipse during the conference to meet the
														research project teams and learn more about their
														technologies.
													</li>
												</ul>

												<p
													style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
													Happy reading!<br> Marco Jahn
												</p>

											</td>
										</tr>
									</tbody>
								</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnBoxedTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
					<tbody class="mcnBoxedTextBlockOuter">
						<tr>
							<td valign="top" class="mcnBoxedTextBlockInner"
								style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

								<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									width="100%"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									class="mcnBoxedTextContentContainer">
									<tbody>
										<tr>

											<td
												style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

												<table border="0" cellspacing="0"
													class="mcnTextContentContainer" width="100%"
													style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
													<tbody>
														<tr>
															<td valign="top" class="mcnTextContent"
																style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;">
																<h2 class="null"
																	style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">
																	<strong>New Project Proposals</strong>
																</h2>

															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnTextBlockOuter">
						<tr>
							<td valign="top" class="mcnTextBlockInner"
								style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									width="100%" class="mcnTextContentContainer">
									<tbody>
										<tr>

											<td valign="top" class="mcnTextContent"
												style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">

												<ul>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
														href="https://projects.eclipse.org/proposals/asciidoc-language"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">E</a><a
														href="https://projects.eclipse.org/proposals/eclipse-editdor"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">clipse
															EdiTDor</a>:&nbsp;provides a&nbsp;<a
														href="https://www.w3.org/TR/wot-thing-description/"
														rel="nofollow"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Thing
															Descriptions</a>&nbsp;Template&nbsp;authoring tool that
														follows the new standard of the W3C Web of Things&nbsp;<a
														href="https://www.w3.org/TR/wot-thing-description/"
														rel="nofollow"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Thing
															Descriptions (TD)</a></li>
													<li
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
														href="https://projects.eclipse.org/proposals/eclipse-digital-cockpit"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
															Digital Cockpit</a>:&nbsp;The Eclipse Digital Cockpit
														provides a modular, polyglot and service-based Edge
														Runtime which enables fast and easy development of apps
														and services that interact with machines.</li>
												</ul>

												<p
													style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
													Interested in more project activity? <a
														href="https://www.eclipse.org/projects/project_activity.php"
														target="_blank"
														style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Read
														on!</a>
												</p>

											</td>
										</tr>
									</tbody>
								</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table></td>
		</tr>
		<tr>
			<td valign="top" id="templateColumns"
				style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;">
				<!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
				<table align="left" border="0" cellpadding="0" cellspacing="0"
					width="300" class="columnWrapper"
					style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody>
						<tr>
							<td valign="top" class="columnContainer"
								style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><table
									border="0" cellpadding="0" cellspacing="0" width="100%"
									class="mcnBoxedTextBlock"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
									<tbody class="mcnBoxedTextBlockOuter">
										<tr>
											<td valign="top" class="mcnBoxedTextBlockInner"
												style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

												<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
												<table align="left" border="0" cellpadding="0"
													cellspacing="0" width="100%"
													style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
													class="mcnBoxedTextContentContainer">
													<tbody>
														<tr>

															<td
																style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

																<table border="0" cellspacing="0"
																	class="mcnTextContentContainer" width="100%"
																	style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																	<tbody>
																		<tr>
																			<td valign="top" class="mcnTextContent"
																				style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;">
																				<h2 class="null"
																					style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">New
																					Project Releases</h2>

																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
								<table border="0" cellpadding="0" cellspacing="0" width="100%"
									class="mcnTextBlock"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody class="mcnTextBlockOuter">
										<tr>
											<td valign="top" class="mcnTextBlockInner"
												style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
												<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
												<table align="left" border="0" cellpadding="0"
													cellspacing="0"
													style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
													width="100%" class="mcnTextContentContainer">
													<tbody>
														<tr>

															<td valign="top" class="mcnTextContent"
																style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">

																<ul>
																	<li
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
																		href="https://projects.eclipse.org/projects/iot.om2m"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																			OM2M</a></li>
																	<li
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
																		href="https://projects.eclipse.org/projects/iot.cyclonedds"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																			Cyclone DDS</a></li>
																	<li
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
																		href="https://projects.eclipse.org/projects/rt.jetty"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																			Jetty</a></li>
																	<li
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
																		href="https://projects.eclipse.org/projects/technology.justj"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																			JustJ</a></li>
																	<li
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a
																		href="https://projects.eclipse.org/projects/technology.passage"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																			Passage</a><br> &nbsp;</li>
																</ul>

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	<a
																		href="https://www.eclipse.org/projects/tools/reviews.php"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">View
																		all the project releases!</a>
																</p>

															</td>
														</tr>
													</tbody>
												</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
											</td>
										</tr>
									</tbody>
								</table></td>
						</tr>
					</tbody>
				</table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
				<table align="left" border="0" cellpadding="0" cellspacing="0"
					width="300" class="columnWrapper"
					style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody>
						<tr>
							<td valign="top" class="columnContainer"
								style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><table
									border="0" cellpadding="0" cellspacing="0" width="100%"
									class="mcnBoxedTextBlock"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
									<tbody class="mcnBoxedTextBlockOuter">
										<tr>
											<td valign="top" class="mcnBoxedTextBlockInner"
												style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

												<!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
												<table align="left" border="0" cellpadding="0"
													cellspacing="0" width="100%"
													style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
													class="mcnBoxedTextContentContainer">
													<tbody>
														<tr>

															<td
																style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">

																<table border="0" cellspacing="0"
																	class="mcnTextContentContainer" width="100%"
																	style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																	<tbody>
																		<tr>
																			<td valign="top" class="mcnTextContent"
																				style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;">
																				<h2 class="null"
																					style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;">Upcoming
																					Virtual Events</h2>

																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
											</td>
										</tr>
									</tbody>
								</table>
								<table border="0" cellpadding="0" cellspacing="0" width="100%"
									class="mcnTextBlock"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody class="mcnTextBlockOuter">
										<tr>
											<td valign="top" class="mcnTextBlockInner"
												style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
												<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
												<table align="left" border="0" cellpadding="0"
													cellspacing="0"
													style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
													width="100%" class="mcnTextContentContainer">
													<tbody>
														<tr>

															<td valign="top" class="mcnTextContent"
																style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	<a href="https://jakartaone.org/brazil2020/"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">JakartaOne
																		Livestream Brazil</a> | August 29, 2020
																</p>

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	<a href="https://www.osdforum.org/" target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">OSD
																		Forum</a> | September15, 2020
																</p>

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	<a href="https://events.eclipse.org/2020/sam-iot/"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">Eclipse
																		SAM IoT 2020</a> | September 17-18, 2020
																</p>

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	<a href="https://www.eclipsecon.org/2020"
																		target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">EclipseCon
																		2020</a> |&nbsp;October&nbsp;19-22,&nbsp;2020<br>
																	&nbsp;
																</p>

																<p
																	style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
																	Do you know about an event relevant to the Eclipse
																	community? Submit your event to <a
																		href="https://newsroom.eclipse.org/" target="_blank"
																		style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;">newsroom.eclipse.org</a>.
																</p>

															</td>
														</tr>
													</tbody>
												</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
											</td>
										</tr>
									</tbody>
								</table></td>
						</tr>
					</tbody>
				</table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
			</td>
		</tr>
		<tr>
			<td valign="top" id="templateLowerBody"
				style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 2px solid #EAEAEA; padding-top: 0; padding-bottom: 9px;"></td>
		</tr>
		<tr>
			<td valign="top" id="templateFooter"
				style="background: #3d3935 none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;"><table
					border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnImageBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnImageBlockOuter">
						<tr>
							<td valign="top"
								style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
								class="mcnImageBlockInner">
								<table align="left" width="100%" border="0" cellpadding="0"
									cellspacing="0" class="mcnImageContentContainer"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody>
										<tr>
											<td class="mcnImageContent" valign="top"
												style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">


												<img align="center" alt=""
												src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/54993d58-f464-469b-b433-24729f44b06f.png"
												width="186.12"
												style="max-width: 1108px; padding-bottom: 0px; vertical-align: bottom; display: inline !important; border-radius: 0%; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
												class="mcnImage">


											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnFollowBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnFollowBlockOuter">
						<tr>
							<td align="center" valign="top"
								style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
								class="mcnFollowBlockInner">
								<table border="0" cellpadding="0" cellspacing="0" width="100%"
									class="mcnFollowContentContainer"
									style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody>
										<tr>
											<td align="center"
												style="padding-left: 9px; padding-right: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
												<table border="0" cellpadding="0" cellspacing="0"
													width="100%"
													style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
													class="mcnFollowContent">
													<tbody>
														<tr>
															<td align="center" valign="top"
																style="padding-top: 9px; padding-right: 9px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																<table align="center" border="0" cellpadding="0"
																	cellspacing="0"
																	style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																	<tbody>
																		<tr>
																			<td align="center" valign="top"
																				style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																				<!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a href="https://www.eclipse.org/"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png"
																																	alt="Website"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a href="mailto:newsletter@eclipse.org"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png"
																																	alt="Email"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a href="https://twitter.com/EclipseFdn"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-twitter-48.png"
																																	alt="Twitter"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a
																																href="https://www.facebook.com/eclipse.org"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png"
																																	alt="Facebook"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a
																																href="https://www.youtube.com/user/EclipseFdn"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-youtube-48.png"
																																	alt="YouTube"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->


																				<table align="left" border="0" cellpadding="0"
																					cellspacing="0"
																					style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																					<tbody>
																						<tr>
																							<td valign="top"
																								style="padding-right: 0; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
																								class="mcnFollowContentItemContainer">
																								<table border="0" cellpadding="0"
																									cellspacing="0" width="100%"
																									class="mcnFollowContentItem"
																									style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																									<tbody>
																										<tr>
																											<td align="left" valign="middle"
																												style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																												<table align="left" border="0"
																													cellpadding="0" cellspacing="0" width=""
																													style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																													<tbody>
																														<tr>

																															<td align="center" valign="middle"
																																width="24" class="mcnFollowIconContent"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
																																<a
																																href="https://www.linkedin.com/company/eclipse-foundation"
																																target="_blank"
																																style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><img
																																	src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-linkedin-48.png"
																																	alt="LinkedIn"
																																	style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
																																	height="24" width="24" class=""></a>
																															</td>


																														</tr>
																													</tbody>
																												</table>
																											</td>
																										</tr>
																									</tbody>
																								</table>
																							</td>
																						</tr>
																					</tbody>
																				</table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
																			</td>
																		</tr>
																	</tbody>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</tbody>
								</table>

							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnDividerBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; table-layout: fixed !important;">
					<tbody class="mcnDividerBlockOuter">
						<tr>
							<td class="mcnDividerBlockInner"
								style="min-width: 100%; padding: 10px 18px 25px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<table class="mcnDividerContent" border="0" cellpadding="0"
									cellspacing="0" width="100%"
									style="min-width: 100%; border-top: 2px solid #EEEEEE; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
									<tbody>
										<tr>
											<td
												style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
												<span></span>
											</td>
										</tr>
									</tbody>
								</table> <!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
							</td>
						</tr>
					</tbody>
				</table>
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="mcnTextBlock"
					style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
					<tbody class="mcnTextBlockOuter">
						<tr>
							<td valign="top" class="mcnTextBlockInner"
								style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
								<!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
								<table align="left" border="0" cellpadding="0" cellspacing="0"
									style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
									width="100%" class="mcnTextContentContainer">
									<tbody>
										<tr>

											<td valign="top" class="mcnTextContent"
												style="padding: 0px 18px 9px; color: #FFFFFF; font-family: Roboto,&amp; quot; Helvetica Neue&amp;quot; , Helvetica , Arial, sans-serif; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; font-size: 12px; line-height: 150%; text-align: center;">

												<span style="font-size: 12px">This email was sent
													to&nbsp;&amp;lt;&amp;lt;Email Address&gt;&gt;<br> You can <a
													href="https://eclipse.us6.list-manage.com/profile?u=eaf9e1f06f194eadc66788a85&amp;id=46e57eacf1&amp;e=[UNIQID]"
													style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-weight: normal; text-decoration: underline;"><span
														style="color: #FFFFFF"><u>update your preferences</u></span></a>
													or <a
													href="https://eclipse.us6.list-manage.com/unsubscribe?u=eaf9e1f06f194eadc66788a85&amp;id=46e57eacf1&amp;e=[UNIQID]&amp;c=f33cda1c44"
													style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-weight: normal; text-decoration: underline;"><span
														style="color: #FFFFFF"><u>unsubscribe from this list</u></span></a>.<br>
													Eclipse Foundation · 2934 Baseline Road, Suite 202 ·
													Ottawa, ON K2H 1B2 · Canada
											</span>
											</td>
										</tr>
									</tbody>
								</table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
							</td>
						</tr>
					</tbody>
				</table></td>
		</tr>
	</tbody>
</table>