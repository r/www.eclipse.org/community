<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Industry 4.0 promises to create the fourth industrial revolution and
	digitally transform production into smart manufacturing. New and
	ever-improving technologies are expected to improve flexibility and
	changeability for manufacturing systems. However, rigid, functionally
	separate IT architectures and fear of making data available for
	artificial intelligence (AI) applications still limit flexibility and
	use of data-driven technologies.</p>
<p>
	The FabOS research project aims to resolve these issues with an
	AI-focused operating system for production that is open, distributed,
	real-time capable, and secure. The project is funded by the German
	Federal Ministry of Economic Affairs and Energy (BMWi) as part of the
	2019 AI innovation competition and combines the efforts of 22 partners,
	including the <a href="https://www.eclipse.org/org/research/project/">Eclipse
		Foundation</a>, research institutions, universities, and companies.
</p>
<h2>FabOS Enables Cyber-Physical Production</h2>
<p>The FabOS operating system should not be thought of like a PC
	operating system or comparable dedicated operating systems, but as a
	system of orchestrated components and services for the operation of a
	networked factory that consists of cyber-physical production systems.
	Together, these systems shape the factory as a cyber-physical
	production operation.</p>
<p>The production operating system forms the IT backbone for versatile
	automation in the factory of the future and the basis of an ecosystem
	for data-driven services and AI applications. The base framework of the
	operating system is the meta-kernel that provides the core
	functionality, similar to the kernel of an operating system.</p>
<p>FabOS integrates technologies that improve the real-time proximity of
	existing applications and enable and guarantee the hard-real-time
	capability and determinism of real-time systems in a modern and
	flexible infrastructure.</p>
<h2>Open Source and Open Standards Are Key Enablers for Open Systems</h2>
<p>Today, there’s a trend to virtualize hardware functions, which means
	software now lives longer than hardware. Companies are also
	increasingly using many innovative software products that are developed
	by open source communities. Much of the progress that has been made in
	data science and AI research and in application development over the
	last few years was enabled by open source tools that were provided to a
	broad community. As a result, open source has become one of the most
	important innovation drivers.</p>
<p>The FabOS project aims to follow the established principles of open
	source innovation, which includes an open architecture, integration of
	accepted standards, and strong stakeholder involvement. The project
	partners are developing an open architecture for FabOS and are
	implementing it as open source.</p>
<p>The FabOS architecture is based on existing and established reference
	architectures from various domains and integrates common and open
	established standards. Open standards such as HTTP, SMTP, MIME, and
	OpenDocument have accelerated the success of the internet and its
	associated web technologies. Similarly, web technologies have been
	successfully applied to develop distributed systems based on a
	service-oriented architecture (SOA) and are widely accepted in many
	industrial applications.</p>
<h2>Data Governance for Data-Driven Technologies</h2>
<p>FabOS integrates multiple standards and technologies and is adopting
	upcoming standards, such as the Asset Administration Shell (AAS), which
	is expected to become the unifying standard for interoperability in
	Industry 4.0. By adopting a unifying standard to virtually represent
	every asset in the digital production environment, the data governance
	that is needed for efficient use of data-driven technologies can be put
	in place.</p>
<p>Figure 1 illustrates the key criteria for data governance.</p>
<p>Figure 1: Key Criteria for Data Governance in Digital Production
	Environments</p>

	<img alt="(No Description)" src="images/2_1.png" class="img img-responsive"/>

<p>Because FabOS is based on an open architecture with open interfaces,
	the components integrated into it can be open source or they can be
	proprietary technology. This flexibility underlines the importance of
	adhering to open standards and interfaces to enable the simple exchange
	of components based on their compatible open interfaces.</p>
<p>FabOS adopters can select components from a broad range of open
	source and proprietary solutions, and even develop their own components
	where needed. As a result, the FabOS AI platform can easily be tailored
	to specific needs and requirements.</p>
<p>Figure 2 illustrates the FabOS AI platform service layers.</p>
<p>Figure 2: FabOS AI Platform Service Layers</p>
	<img alt="(No Description)" src="images/2_2.png" class="img img-responsive"/>
<p>Open source draws its strength from being easy to integrate and
	reuse. Consequently, FabOS is not starting from scratch, but integrates
	and adapts existing solutions.</p>
<p>
	For example, <a href="https://www.eclipse.org/basyx/">Eclipse BaSyx</a> will
	be an integral part of the FabOS ecosystem. Eclipse BaSyx is developed
	as part of BaSys 4.2, the continuation of the BaSys 4.0 research
	project. It implements key Industry 4.0 concepts, such as the AAS, and
	provides off-the-shelf components that enable quick setup of an
	Industry 4.0 infrastructure.
</p>
<p>The FabOS project team also plans to integrate open source solutions
	from other domains, such as edge computing. That means FabOS will
	provide an open source infrastructure that is built on other open
	source components.</p>
<h2>Open Communities Drive Innovation</h2>
<p>In addition to easy integration and reuse of software, open source
	also enables an open and transparent process that allows stakeholders,
	such as users and developers, to be involved. The process is based on
	global collaboration, distributed change management, and iterative
	development, along with seminars, workshops, hackathons, and other
	means of involving FabOS community members.</p>
<p>A process that is this open ensures quality and security because
	defect-correction cycles and high security assurance can be quickly and
	effectively implemented.</p>
<p>FabOS uses this transparent approach to implement appropriate
	security mechanisms and precautions at all necessary levels and to
	crowd source security requirements from developers and users. Because
	FabOS is modular and adheres to open source principles, these
	mechanisms and precautions should always correspond to the state of the
	art. This keeps the statistical safety and operational reliability
	(safety) of the applications at the highest possible technical level.</p>
<p>The open architecture also allows for evolutionary designs that
	fulfill new or changed requirements and are supported by robust and
	modular components that are interchangeable because they adhere to open
	standards. Modern and future IT architectures, systems, and
	infrastructure often consist of modular components that are distributed
	logically and locally. FabOS is primarily targeting this type of open
	and flexible architecture, but also offers ways to integrate existing
	systems and resources.</p>
<p>The open community approach also allows stakeholders to collaborate
	and to identify new business models. Developers and solution providers
	can create collaborative distribution models and new services, such as
	liability support, or solutions for service level agreements.
	Transparency and participation can also be used as marketing tools,
	leading to fresh competition, innovation, and customer participation in
	previously closed oligopolies.</p>
<h2>Get More Information</h2>
<p>
	To learn more about FabOS or to get in touch, visit our <a
		href="https://www.fab-os.org/en/">website</a>.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img-responsive" alt="Daniel Stock" src="images/2_3.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Daniel Stock</p>
          <p class="author-bio">Daniel Stock is a group leader at the Fraunhofer Institute for Manufacturing Engineering and Automation IPA. His group focusses on research on IT Architectures for Digital Production, which includes building integration solutions for machines, IT infrastructure for production environments and service deployment strategies for edge-cloud platforms for I4.0-compliant applications.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive" alt="Frank Schnicke" src="images/2_4.jpg" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Frank Schnicke</p>
          <p class="author-bio">Frank Schnicke is project manager at the Fraunhofer Institute for Experimental Software Engineering IESE. His research focuses on Industry 4.0 software architectures that enable changeability in industrial plants. Additionally, he is coordinating the implementation of the Eclipse BaSyx reference implementation.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
	<div class="col-xs-24 col-sm-8"><img src="images/2_5.png" alt="" class="img img-responsive" /></div>
	<div class="col-xs-24 col-sm-8"><a href="https://www.fab-os.org/">https://www.fab-os.org/</a>
<a href="https://www.eclipse.org/basyx/">https://www.eclipse.org/basyx/</a>
	</div>
	<div class="col-xs-24 col-sm-8"><img src="images/2_6.png" alt="" class="img img-responsive" /></div>
</div>
<p class="margin-top-20">This project has received funding from the German Federal Ministry
	for Economic Affairs and Energy (BMWi)'s AI Innovation Competition
	under grant agreement No 01MK20010A.</p>