<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
	<img alt="RobMoSys logo" src="images/3_1.png" />
</p>
<p>The European Union-funded research project, RobMoSys, is a robotics
	development approach that enables a major step toward an open,
	industry-grade software development ecosystem for robotics. With the
	RobMoSys approach, you can reduce software development complexity,
	increase system predictability, and maintain flexibility, while saving
	costs.</p>
<p>
	The Eclipse Foundation <a
		href="https://www.eclipse.org/org/research/project/">is a partner in
		the RobMoSys project</a> and two Eclipse Foundation projects — <a
		href="https://projects.eclipse.org/projects/modeling.smartmdsd">Eclipse
		SmartMDSD </a> and <a
		href="https://www.eclipse.org/papyrus/components/robotics/">Eclipse
		Papyrus for Robotics</a> — can be used to implement the RobMoSys
	model-driven approach to robotics software development. However, before
	we explain those relationships in more detail, it’s important to
	understand RobMoSys in the context of Industry 4.0.
</p>
<h2>RobMoSys Stimulates Industry 4.0 Development and Usage</h2>
<p>RobMoSys closely follows Industry 4.0 design principles:</p>
<ul>
	<li>Interoperability</li>
	<li>Decentralization</li>
	<li>Autonomy</li>
	<li>Real-time capabilities</li>
	<li>Technical assistance</li>
	<li>Service orientation</li>
	<li>Modularity</li>
</ul>
<p>This is a key RobMoSys feature because it encourages Industry 4.0
	implementations and usage. To achieve these design goals, RobMoSys is
	based on two foundational principles:</p>
<ul>
	<li><strong>Separation of roles and concerns</strong> allows you to
		focus on your development workflow and expertise. You are not required
		to become an expert in every field because different roles cover
		different problem areas. Each role works autonomously and excels in
		its performance.</li>
	<li><strong>Composability</strong> is the ability to combine and
		re-combine building blocks “as is” into different systems for
		different purposes in a meaningful way. The models follow a general,
		composition-oriented approach where systems can be constructed from
		reusable software building blocks with explicated properties. This
		allows you to use the same software components across different
		systems to save software development costs and increase the
		predictability of your robotics systems.</li>
</ul>
<p>In addition, there’s a very good opportunity to bring RobMoSys
	contributions to the OPC Unified Architecture (OPC UA) domain and link
	the robotics and automation domains. A significant part of the Industry
	4.0 domain is shifting toward the OPC UA and, in general, the Industry
	4.0 world that’s based on the OPC UA fully conforms with the RobMoSys
	way of thinking.</p>
<h2>Eclipse Foundation Projects for Model-Driven Robotics Software
	Development</h2>
<p>To follow the RobMoSys model-driven approach, you can use the Eclipse
	Foundation projects described below.</p>
<h3>Eclipse SmartMDSD</h3>
<p>
	The <a href="https://projects.eclipse.org/projects/modeling.smartmdsd">Eclipse
		SmartMDSD</a> Toolchain is an <a
		href="https://www.eclipse.org/eclipseide/"> </a> integrated
	development environment (IDE) for robotics software development. It
	uses the RobMoSys model-driven approach to support the different types
	of people, or roles, that develop robotic systems.
</p>
<p>Robotics solution providers can use Eclipse SmartMDSD to develop
	software components. Robotics system builders can use Eclipse SmartMDSD
	to build systems using the software components. Both parties gain the
	benefit of 20 years of best practices in robotics components and system
	development.</p>
<p>Figure 1 illustrates an Eclipse SmartMDSD Toolchain simulation using
	the Webots Simulator. The picture on the left shows the SmartMDSD
	toolchain System Component Architecture developed with building blocks.
	The picture on the right shows the Webots simulation of a TIAGo robot.</p>
<p>Figure 1:  Eclipse SmartMDSD Toolchain Simulation Using the Webots
	Simulator</p>
<p>
	<img alt="(No Description)" src="images/3_2.png" />
</p>
<p>
	To learn more about using the SmartMDSD Toolchain with the Webots
	Simulator, watch this <a
		href="https://www.youtube.com/watch?v=oFLCagIU0EE">YouTube video</a>.
</p>
<h3>Eclipse Papyrus for Robotics</h3>
<p>
	The <a href="https://www.eclipse.org/papyrus/components/robotics/">Eclipse
		Papyrus for Robotics</a> Toolchain is an IDE for robotics software
	development. It features a modeling front-end that conforms to the
	RobMoSys foundational principles described above.
</p>
<p>
	Eclipse Papyrus for Robotics supports model-based safety analysis
	through a safety module that performs dysfunctional analysis on system
	architecture components. The IDE’s performance analysis capability
	supports the <a
		href="https://robmosys.eu/wiki/general_principles:architectural_patterns:stepwise_management_nfp">architectural
		pattern for stepwise management of extra-functional properties</a>. It
	also enables analysis of end-to-end response times and resource
	utilization of component compositions.
</p>
<p>The code-generation capability of the Eclipse Papyrus for Robotics
	Toolchain transforms models of software component architectures,
	platform descriptions, and deployment specifications into code. It
	works with the Robotic Operating System 2 (ROS2) framework.</p>
<p>Figure 2 shows an Eclipse Papyrus for Robotics sequence example for a
	robotic arm.</p>
<p>Figure 2: Eclipse Papyrus for Robotics Sequence Example for a Robotic
	Arm</p>
<p>
	<img alt="(No Description)" src="images/3_3.png" />
</p>
<p>
	To see a demonstration of Papyrus for Robotics, be sure to attend the <a
		href="https://www.eclipsecon.org/2020/sessions/getting-started-papyrus-robotics">
		EclipseCon 2020 session</a>.
</p>
<h2>Integrated Technical Projects Leverage RobMoSys Tools and Approach</h2>
<p>
	Between 2017 and 2019, RobMoSys provided <strong>financial support</strong> to
	strengthen community involvement through two <a
		href="https://robmosys.eu/open-calls/">open calls</a> with cascaded
	funding. In the second open call for “fast adoption,” eight Integrated
	Technical Projects (ITPs) were selected to implement the RobMoSys
	approach in industry. The following ITPs are currently using the
	RobMoSys tools and approach to achieve their objectives:
</p>
<ul>
	<li><strong>Underwater Robotic Simulator for RobMoSys (UWROSYS)</strong> is
		bringing a high-fidelity underwater robotic simulator with advanced
		sensors into the RobMoSys framework to open the underwater domain to
		RobMoSys users. This technology solves the problem of expensive and
		very time-consuming underwater operations such as inspection,
		maintenance, and repair.</li>
	<li><strong>Mobile Inspection Robot Autonomy for Nuclear
			Decommissioning Authority (MIRANDA)</strong> uses RobMoSys with
		existing GMV hardware (www.GMV.com) and software to produce a system
		that can run a typical field trial in the context of a nuclear site.</li>
	<li><strong>Mixed Reality Implementation for RobMoSys (MR4RobMoSys)</strong> aims
		to simplify interactions between humans and robots. The ITP uses an
		existing bridge between ROS-Industrial and the game engine Unity
		developed in a ROSIN Focused Technical Project (FTP) to implement
		augmented reality (AR) and virtual reality (VR) visualization with the
		help of Eclipse SmartMDSD and Eclipse Papyrus for Robotics.</li>
	<li><strong>Applying a Model-Based Software Platform to Standardize the
			Rehab Robot (AMBSPSRR)</strong> is adapting the Rehab Robot to a
		RobMoSys-conformant pilot project based on software and tools from the
		RobMoSys ecosystem.</li>
	<li><strong>HRI Components for Assistive Robot (HRICAR)</strong> is
		developing a set of RobMoSys models and components for human-robot
		interactions.</li>
	<li><strong>RoMan</strong> focuses on RobMoSys adoption in the lighting
		system manufacturing industry. To prove the reusability of the
		RobMoSys methodology in new robots and the benefits of its structure
		for building industrial applications, this ITP is building an
		industrial application with the SUMMIT-XL STEEL robotic platform in a
		company that’s focused on manufacturing lighting systems.</li>
	<li><strong>Experiment in Applied Modular Food Robots Applications
			(EXAMFORA)</strong> is investigating use of the RobMoSys method
		within the Goal-Discovering Robotic Architecture for
		Intrinsically-Motivated Learning (GRAIL) Robot System, a hard
		real-time system. The application is for a vision-based system that
		increases the overall efficiency of an assembly system that involves
		human-robot interactions by choosing paths that are least likely to
		result in collisions.</li>
	<li><strong>Stereovision and Radar for Affordable and Safe Navigation
			(STERAS)</strong> is developing a low-cost navigation system that is
		capable of simultaneous localization and mapping (SLAM). SLAM
		capabilities allow mobile robots and drones to navigate autonomously
		and safely through a dynamic territory in the presence of dynamic
		objects such as other robots, humans, or cars.</li>
</ul>
<h2>Learn More and Get Involved</h2>
<p>
	To learn more about RobMoSys, join our <a
		href="https://discourse.robmosys.eu/">public discussion forum </a> and
	check out our <a href="https://robmosys.eu/events/">upcoming events</a>.
</p>
<p>
	To learn more about the Eclipse SmartMDSD project, click <a
		href="https://projects.eclipse.org/projects/modeling.smartmdsd">here</a>.
</p>
<p>
	To learn more about Eclipse Papyrus for Robotics, visit the project’s <a
		href="https://robmosys.eu/wiki/community:start">Community Corner</a>.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="Dr. Luz Martinez"
						src="images/3_4.png" />
				</div>
				<div class="col-sm-16">
					<p class="author-name">Dr. Luz Martinez</p>
					<p class="author-bio">Dr. Luz Martinez graduated with a PhD degree
						in Robotics and Computer Vision from the University of Chile in
						2019. She is currently working as the Chair of Robotics,
						Artificial Intelligence, and Real-Time Systems at the Technical
						University of Munich (TUM). She has been working on a perception
						stack for the European-funded project, RobMoSys, and coordinating
						the European Union project, SHOP4CF. Her research interests
						include computer vision for robotic manipulation, task planning,
						and human-robot interaction.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="Anna Carolina"
						src="images/3_5.png" />
				</div>
				<div class="col-sm-16">
					<p class="author-name">Anna Carolina Principato</p>
					<p class="author-bio">Anna Carolina Principato has a Master&#39;s
						degree in Romance Philology from the Ludwig Maximilian University
						of Munich. For more than 10 years, Anna has been working as an
						Account Manager at global marketing communications agencies, such
						as Text100 and Harvard Public Relations, supporting technology and
						IT brands. Currently, she is in charge of public relations for the
						European Horizon 2020 project, RobMoSys, at TUM, and has been
						responsible for public relations at the European FP7 project
						ECHORD++.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-24 col-sm-8">
		<img src="images/3_6.png" alt="" class="img img-responsive" />
	</div>
	<div class="col-xs-24 col-sm-8">
		<ul>
			<li>Website: <a href="https://robmosys.eu/">robmosys.eu/</a></li>
			<li>Twitter: <a href="https://www.twitter.com/RobMoSys">@RobMoSys</a></li>
			<li>Youtube: <a href="https://www.youtube.com/channel/UCURqFtHgAPsXl9mB_QgbB0w/featured">RobMoSys</a></li>
		</ul>
        <p>Eclipse Projects:</p>
        <ul>
        	<li><a href="https://projects.eclipse.org/projects/modeling.smartmdsd">Eclipse SmartMDSD | projects.eclipse.org</a></li>
            <li><a href="https://www.eclipse.org/papyrus/components/robotics/">Papyrus for Robotics</a></li>
        </ul>
	</div>
	<div class="col-xs-24 col-sm-8">
		<img src="images/3_1.png" alt="" class="img img-responsive" />
	</div>
</div>
<p>This project has received funding from European
	Union's Horizon 2020 research and innovation programme under grant
	agreement No 732410.</p>