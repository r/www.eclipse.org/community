<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1><?php echo $pageTitle; ?></h1>
<h2>Jakarta EE Survey Results and Milestone Release Now Available</h2>
<p>June was a big month for Jakarta EE with the release of the 2020
	Jakarta EE Developer Survey results and the Jakarta EE 9 milestone
	release.</p>

<p>This year&rsquo;s Jakarta EE Developer Survey revealed a number of
	interesting trends, including the fact that Jakarta EE adoption is on
	the rise. Survey responses showed that:</p>
<ul>
	<li>Jakarta EE is emerging as the second-place cloud native framework
		with 35 percent usage.</li>
	<li>Together, Java 8 and Jakarta EE 8 hit the mainstream with 55
		percent adoption. Despite only shipping in September 2019, Jakarta EE
		8 usage skyrocketed to 17 percent.</li>
</ul>
<p>
	Get the complete survey results <a
		href="https://www.google.com/url?q=https://outreach.jakartaee.org/2020-developer-survey-report&amp;sa=D&amp;ust=1595864660443000&amp;usg=AOvVaw1aPeF7dPPcZB5QA3dCRtXh">here</a>.
</p>
<p>The Jakarta EE 9 milestone release reinforces the value the industry
	sees in Jakarta EE. Technology leaders are investing to ensure their
	software supports the Jakarta EE 9 namespace and others have indicated
	they will do the same. With the full Jakarta EE 9 release later this
	year, Jakarta EE will be ideally positioned to drive true open source,
	cloud native innovation using Java.</p>

<p>
	Find Jakarta EE 9 milestone platform information <a
		href="https://www.google.com/url?q=https://jakarta.ee/specifications/platform/9/&amp;sa=D&amp;ust=1595864660443000&amp;usg=AOvVaw1cX95BYUtwneMX6Q5tHaH_">here</a>.
</p>
<p>
	For deeper insight into industry adoption of Jakarta EE, how
	architectural approaches are evolving, and Jakarta EE community growth,
	read<a
		href="https://www.google.com/url?q=https://blogs.eclipse.org/post/mike-milinkovich/jakarta-ee-taking&amp;sa=D&amp;ust=1595864660444000&amp;usg=AOvVaw3ylcWRAZROh9jSlqhfD_N5">this
		blog</a>&nbsp;by Eclipse Foundation Executive Director, Mike
	Milinkovich.

</p>
<hr />
<h2>Start Planning Your EclipseCon 2020 Experience</h2>
<p>
	<a
		href="https://www.google.com/url?q=https://www.eclipsecon.org/2020&amp;sa=D&amp;ust=1595864660445000&amp;usg=AOvVaw058BcLcZDwkHeLmP9AJiae">EclipseCon
		2020</a>&nbsp;will be held October 19-20 and is a free, virtual event
	this year so more community members can join in.
</p>

<p>Here are just a few ways you can start planning your EclipseCon 2020
	experience today:</p>
<ul>
	<li><a
		href="https://www.google.com/url?q=https://www.eclipsecon.org/2020/registration&amp;sa=D&amp;ust=1595864660445000&amp;usg=AOvVaw3FMONcy1EM4YiuBtESqyhB">Register
			online</a>. There&rsquo;s no registration fee, but you do need to
		register to attend.</li>
	<li><a
		href="https://www.google.com/url?q=https://www.eclipsecon.org/2020/community-day&amp;sa=D&amp;ust=1595864660446000&amp;usg=AOvVaw0dmKF-BvVBcOQU_s95dRh4">Reserve
			your spot in Community Day</a>. Community Day will be held on Monday,
		October 19. Participating in Community Day is a great way to grow your
		own Eclipse community and gain visibility into other communities and
		projects. Space for sessions is limited, so be sure to request your
		spot early.</li>
	<li><a
		href="https://www.google.com/url?q=https://www.eclipsecon.org/2020/sessions&amp;sa=D&amp;ust=1595864660447000&amp;usg=AOvVaw2N6oVN_3UghHjTAI1ql-7R">Check
			out the EclipseCon 2020 sessions</a>.The EclipseCon 2020 program has
		been announced, so take a few minutes to find the topics and talks
		that interest you most.</li>
</ul>
<hr />
<h2>Get Involved in Eclipse SAM IoT 2020</h2>
<p>
	<a
		href="https://www.google.com/url?q=https://events.eclipse.org/2020/sam-iot/&amp;sa=D&amp;ust=1595864660448000&amp;usg=AOvVaw136mIvYH4qp45Px-mATW7n">Eclipse
		SAM IoT</a>&nbsp;is the first virtual Eclipse conference on Security,
	Artificial Intelligence, and Modeling (SAM) for next-generation
	Internet of Things (IoT). This free event will be held September 17-18,
	2020.
</p>

<p>As we move into the next stage of IoT evolution, resolving technical
	challenges such as low latency, high reliability, security, and dynamic
	resource allocation is critical to enable smart factories, smart
	cities, critical infrastructure, and cooperative service robotics.
	Efforts so far have led to innovations in edge computing, artificial
	intelligence and analytics, digital twins, and security and trust
	technologies.</p>

<p>Experts and Researchers: Submit a Paper</p>
<p>If you&rsquo;re an expert or a researcher in any of these areas,
	please consider presenting your work to SAM IoT conference participants
	&mdash; a mix of researchers, industry players, and representatives
	from standards bodies. We welcome all submissions and there&rsquo;s no
	need for projects to be associated with the Eclipse Foundation or its
	projects.</p>

<p>
	To review the submission guidelines and submit your paper, click <a
		href="https://www.google.com/url?q=https://events.eclipse.org/2020/sam-iot/call-for-papers/&amp;sa=D&amp;ust=1595864660449000&amp;usg=AOvVaw1XooKefz9kC71_dEZRNl1P">here</a>.
</p>
<p>
	The final date for submissions is July 22, 2020, and selected papers
	will be published under the Creative Commons License 4.0 on <a
		href="https://www.google.com/url?q=http://ceur-ws.org/&amp;sa=D&amp;ust=1595864660449000&amp;usg=AOvVaw0XdwjHpqiTF1XoUnDBnkOU">ceur-ws.org</a>.
</p>

<p>Participants: Register Today</p>
<p>
	To register for the Eclipse SAM IoT virtual conference, click <a
		href="https://www.google.com/url?q=https://www.eventbrite.de/e/eclipse-sam-iot-2020-tickets-107823242220&amp;sa=D&amp;ust=1595864660450000&amp;usg=AOvVaw0Q5rMbYoPJyXFMcEUKHTzG">here</a>.
</p>