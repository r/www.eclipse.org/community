<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
	The Eclipse Arrowhead project was created to provide long-term
	governance and promotion of the Arrowhead Framework, a Service Oriented
	Architecture (SOA) with a reference implementation for Internet of
	Things (IoT) interoperability that was originally developed as part of
	the <a href="https://www.eclipse.org/org/research/project/"> <span> </span>
		<span>Arrowhead Tools European research project</span>
	</a>.
</p>
<p>
	The Eclipse Foundation was chosen to host the Arrowhead project because
	it provides a <a href="https://www.eclipse.org/legal/epl-2.0/"> <span> </span>
		<span>well-established open source license</span>
	</a>, strong intellectual property (IP) support, and a well-developed
	network for reaching out to developers who are interested in the
	Arrowhead Framework.
</p>
<p>Before we describe the Eclipse Arrowhead project in more detail,
	here’s some background on the Arrowhead Framework to help put the
	project in context.</p>
<h2>An Architecture to Enable IoT Interoperability and Integration</h2>
<p>The Arrowhead Framework enables the design and implementation of
	automation systems in application domains such as production, smart
	cities, e-mobility, energy, and buildings.</p>
<p>It was created to efficiently address Industry 4.0 requirements,
	primarily through scalable, secure, and flexible information sharing
	that enables system interoperability and integration. To achieve these
	goals, the SOA architecture abstracts each interface that exchanges
	information as a service. Instead of hardwiring the connections, it
	enables loose coupling, late binding, and lookups to discover services.</p>
<p>Since 2016 when the Arrowhead Framework was released, a number of
	other European Union and national projects have added to it. As a
	result, the Arrowhead Framework architecture and its reference
	implementation can be used to implement Industry 4.0 architectures,
	such as the Reference Architecture Model for Industry 4.0 (RAMI 4.0)
	and the Industrial Internet Reference Architecture (IIRA).</p>
<p>The Arrowhead Framework architecture has already been applied in:</p>
<ul>
	<li>Industrial control systems, such as supervisory control and data
		acquisition (SCADA) and distributed control systems (DCSs)</li>
	<li>Manufacturing execution systems (MESs)</li>
	<li>Programmable logic controllers (PLCs)</li>
	<li>IoT solutions, such as building energy management, industrial
		gateways for smart city applications, and intelligent rock bolts for
		mining safety</li>
</ul>
<p>In the current Arrowhead Tools research project, the Arrowhead SOA
	architecture is being applied to the engineering process,
	interoperability among engineering tools, and machine-to-machine (M2M)
	business transactions to further extend its ability to implement
	advanced industry 4.0 architectures and their extensions.</p>
<h2>Eclipse Arrowhead Fundamentals</h2>
<p>The Eclipse Arrowhead project provides an SOA-based framework for
	designing and building automation and digitalization solutions. The key
	requirements for the architecture are:</p>
<ul>
	<li>Real-time performance</li>
	<li>Scalability</li>
	<li>Seamless interoperability between IoT systems</li>
	<li>Security</li>
	<li>Engineering simplicity, process, and tool chains</li>
	<li>Evolvable System of Systems</li>
	<li>Flexible automation</li>
	<li>Run-time management</li>
	<li>Integration along value chain</li>
	<li>Integration along product life cycle</li>
	<li>Integration to across stakeholders</li>
</ul>
<p>
	To a large extent, automation is geographically local. Combining local
	automation with real-time and security requirements leads to the
	concept of self-contained local clouds. A local cloud is a private
	network that becomes a shell within which sensitive functionality is
	grouped. If the private network has a real-time, physical network
	layer, hard real-time performance can be realized with the local cloud.
	The local cloud concept also provides some interesting security
	properties. <a href="#footnote_1">[i]</a>
</p>
<p>The Eclipse Arrowhead project is based on an SOA that features loose
	coupling, late binding, and lookups. Together, these features enable
	discovery of services in operation. They also enable run-time
	definition of service bindings and provide autonomous service exchange
	operation until further notice. These capabilities are supported by the
	Arrowhead core systems ServiceRegistry and Orchestration. In addition,
	security of service exchanges requires authentication of the service
	consumer and authorization of the specific service consumption. This is
	supported by the Arrowhead core system Authorisation.</p>
<h2>Building Applications With Eclipse Arrowhead</h2>
<p>As an example, the fundamental properties of Eclipse Arrowhead allow
	you to create a simple control loop, as shown in Figure 1. The three
	core systems support deployment of the control loop — the dotted lines
	in Figure 1 — to enable autonomous execution of the control loop. The
	run-time deployment is supported by the Arrowhead core systems Service
	Registry, Orchestration, and Authorisation.</p>
<p>
	<img
		alt="Figure 1: Basic Control Loop Implemented in Eclipse Arrowhead"
		src="images/1_1.jpg" class="img img-responsive" />
</p>
<p>Once deployed, the control loop operates autonomously until further
	notice from the Orchestration or Authorisation system.</p>
<p>Interoperability between various IoT systems is addressed using a
	translation concept. As shown in Figure 2, translations to and from
	various SOA protocols, encodings, and even semantics are supported. The
	Orchestration system detects a service contract mismatch of protocols,
	such as HTTP and MQTT, and instantiates a Translator service that
	translates between the two protocols.</p>
<p></p>
<p>
	<img
		alt="Figure 2: Translation Service for Protocols, Encodings, and Semantics"
		src="images/1_2.png" class="img img-responsive" />
</p>
<p>Current implementations support translation between HTTP, CoAP, and
	MQTT protocols.</p>
<p>Combining the concept of local clouds with the ability to exchange
	services between local clouds allows you to build a complex system of
	systems (SoS), which is conceptualized in Figure 3.</p>
<p>
	<img
		alt="Figure 3: A Complex SoS Engineered From Interactions Among Local
	Clouds"
		src="images/1_3.jpg" class="img img-responsive" />
</p>
<h2>Looking at the Bigger Picture</h2>
<p>Engineering larger automation and digitalization solutions is
	currently guided by the International Society of Automation ISA-95
	architecture and upcoming RAMI 4.0 and IIRA architectures. Eclipse
	Arrowhead consists of building blocks — released core systems or work
	on such support systems that address many aspects of RAMI 4.0 and IIRA.
	Eclipse Arrowhead includes work on:</p>
<ul>
	<li>Fundamental deployment procedure ensuring basic security down to
		hardware level</li>
	<li>Interoperability with other frameworks and platforms</li>
	<li>Inter-cloud service exchanges</li>
	<li> System of Systems support</li>
	<li>Execution support</li>
	<li>Supply chain and product life cycle support for M2M business
		process support</li>
	<li>Management support</li>
	<li>Engineering support</li>
</ul>
<p>Figure 4 provides an overview of the current roadmap for Eclipse
	Arrowhead development, showing the core system status for released
	(v4.1.3), release candidate, and prototype software.</p>
<p>
	<img alt="Figure 4: Current Roadmap for Eclipse Arrowhead Core Systems"
		src="images/1_4.jpg" class="img img-responsive" />
</p>
<p>The focus of the releases is as follows:</p>
<ul>
	<li><strong>Released core systems:</strong> Stability, documentation, and
		usability.</li>
	<li><strong>Release candidates:</strong> Finalization of implementation and
		passing the continuous integration (CI) and continuous delivery (CD)
		processes.</li>
	<li><strong>Prototypes:</strong> Integration with the overall architecture
		and how the software contributes to implementation of the RAMI 4.0 and
		IIRA architectures.</li>
</ul>
<h2>Get More Information</h2>
<p>
	To learn more about the Eclipse Arrowhead project, visit our <a
		href="https://www.arrowhead.eu/arrowheadframework">website</a>.
</p>
<p>
	To access the source code, installation packages, libraries, and
	documentation for the latest Eclipse Arrowhead release (v4.1.3), visit
	our <a href="https://github.com/arrowhead-f">GitHub site</a>. You’ll
	also find release candidates and most prototypes in branches on this
	site.
</p>
<p>
	Finally, you can read the book  <span>IoT Automation - Arrowhead
		Framework</span>, which was edited by Jerker Delsing, one of the
	authors of this article. The book describes Arrowhead Framework
	fundamentals, architecture, reference implementations, and
	applications. It was published by CRC Press in February 2017 and the
	ISBN number is 9781498756754. A quick online search should find the
	book.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="Prof. Jerker Delsing" src="images/1_5.png" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Prof. Jerker Delsing</p>
          <p class="author-bio">Prof. Jerker Delsing received his M.Sc. in Engineering Physics at Lund Institute of Technology, Sweden, in 1982, and his Ph.D in Electrical Measurement at the Lund University in 1988. Since 1995, he has been a full professor of Industrial Electronics in the Embedded Intelligent Systems Lab (EISLAB) at Lulea University of Technology, specializing in IoT and SoS automation. Over the years, Professor Delsing and his group have partnered on several large European Union research projects, including Socrades, IMC-AESOP, Arrowhead, FAR-EDGE, and MIDIH. Current projects include Productive4.0 and Arrowhead Tools. Delsing is also a vice president and board member of the ARTEMIS Industry Association and a board member of ProcessIT.EU and ProcessIT Innovations.</p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="Pal Varga" src="images/1_6.png"/>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Pal Varga</p>
          <p class="author-bio">Pal Varga is associate professor in the Department of Telecommunication and Media Informatics at the Budapest University of Technology and Economics in Hungary, where he also received his M.Sc. and Ph.D. He is also a director at AITIA International Inc. His current research interests include hardware acceleration, communications systems, mobile networking, network performance measurements, root cause analysis, and fault localization. In the last ten years he has been intensively involved in research and development within the Industrial Internet of Things (IIoT) domain, focusing on service-oriented information exchanges. Varga and his group have contributed to a number of European research projects including IST-MUSE, CELTIC TIGER-2 and COMBO, ARTEMIS SCALOPES, ARROWHEAD, ECSEL MANTIS, Productive4.0, and Arrowhead Tools.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row margin-top-20">
	<div class="col-xs-24 col-sm-8  match-height-item-by-row">
		<img src="images/3_6.png" alt="European Union Horizon 2020" class="img img-responsive" />
		<img src="images/1_8.png" alt="ECSEL Joint Undertaking" class="img img-responsive" />
	</div>
	<div class="col-xs-24 col-sm-8 vertical-align match-height-item-by-row">
		<ul>
			<li>Website: <a href="https://arrowhead.eu/">arrowhead.eu</a></li>
			<li>Twitter: <a href="https://www.twitter.com/ArrowheadEU">@ArrowheadEU</a></li>
			<li>Youtube: <a href="https://www.youtube.com/user/ArrowheadProject">Arrowhead</a></li>
			<li>Eclipse Project: <a href="https://projects.eclipse.org/projects/iot.arrowhead">Eclipse Arrowhead | projects.eclipse.org</a></li>
		</ul>
	</div>
	<div class="col-xs-24 col-sm-8 vertical-align match-height-item-by-row">
		<img src="images/1_9.png" alt="Arrowhead" class="img img-responsive" />
	</div>
</div>
<p>The Arrowhead Tools research project is funded by the European
	Commission, through the European Horizon 2020 research and innovation
	program, the Electronic Components and Systems for European Leadership
	(ECSEL) Joint Undertaking, and national funding authorities in Sweden,
	Spain, Poland, Germany, Italy, Czech Republic, Netherlands, Belgium,
	Latvia, Romania, Hungary, Finland, Turkey, and Switzerland under the
	research project Arrowhead Tools with Grant Agreement no. 826452.</p>
<p class="margin-top-20" id="footnote_1">[i] Delsing, J. Eliasson, J. van Deventer, H. Derhamy, and P. Varga,
	“Enabling iot automation using local clouds,” in 2016 IEEE 3rd World
	Forum on Internet of Things (WF-IoT), 2016, pp. 502–507</p>