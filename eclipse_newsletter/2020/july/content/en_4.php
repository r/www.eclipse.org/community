<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The Eclipse Foundation, together with its research and industrial
	partners, is fostering creation of a Working Group that’s focused on
	open industrial automation to bring industry-grade open source software
	to shop floors. The goal is to provide reliable open source
	technologies that meet the needs of industries and small- to
	medium-sized enterprises (SMEs) as they evolve to digital factories.</p>
<p>The Working Group is an opportunity for innovative companies to:</p>
<ul>
	<li>Share the effort and cost of developing reliable open source
		software</li>
	<li>Actively shape software development according to their needs to
		avoid vendor lock-in to proprietary solutions and accelerate time to
		market</li>
</ul>
<h2>Closed and Proprietary Systems Limit Industrial Automation</h2>
<p>Since the last century when computers first found their way onto shop
	floors, the basic concepts and computational paradigms used in
	industrial automation haven’t changed much. Most innovations have been
	confined to vendor-specific, proprietary ecosystems.</p>
<p>With the advent of the internet and the digital economy that came
	with it, supply chains and markets did change. But, industrial
	automation cannot develop to its full potential.</p>
<p>The outdated computational concepts used in closed and proprietary
	environments limit the new production capabilities and business models
	that become possible with open and highly interconnected systems. Those
	who are first to overcome these obstacles will gain a competitive
	advantage thanks to the flexibility that results from easily
	understood, networked, and adaptable production processes.</p>
<h2>A User-Driven Approach Meets Strategic Requirements</h2>
<p>
	Eclipse Working Groups provide a <a
		href="https://www.eclipse.org/org/workinggroups/about.php#wg-neutral">vendor-neutral
		governance structure</a> that allows organizations to freely
	collaborate on new technology development. The core principles of
	Eclipse Working Groups help to ensure development of a reliable, open
	platform that is free to use, adapt, and redistribute, and that is in
	sync with users’ requirements and strategic goals. 
</p>
<p>Membership in the proposed Open Industrial Automation Working Group
	is centered around open source software users and producers.</p>
<p>User members are from organizations that need software components
	that are developed under a vendor-neutral open source software license
	that leverages collaboration with industry partners to decrease time to
	market and costs and eliminate vendor lock-in.</p>
<p>Producer members are from organizations that develop open source
	software for industrial automation or build solutions and products
	based on open source.</p>
<p>Here’s how the relationship between the two works:</p>
<ul>
	<li>User members collaboratively define the vision and details of the
		software that will be developed based on what is strategically
		important to them.</li>
	<li>Producer members listen, discuss the requirements, and ultimately
		implement them.</li>
	<li>The work is jointly funded by user members.</li>
	<li>The process of defining requirements, having discussions, and
		collaboratively developing software are transparent to all Working
		Group members and the results are available as open source.</li>
</ul>
<h2>It’s All About the Code</h2>
<p>Contrary to other well-known and successful industrial automation
	initiatives, the Eclipse Foundation Open Industrial Automation Working
	Group puts a clear focus on code — code that is open source, solves
	real-world problems, and runs reliably in production environments.</p>
<p>In addition to providing governance and infrastructure for open
	source industrial automation software, the Working Group supports
	existing standards and interoperability in an open environment with the
	following characteristics:</p>
<ul>
	<li><strong>Compatibility:</strong> The ecosystem will have the ability to expand to support existing
		and upcoming standards and technologies such as new communications
		standards or data formats. We welcome organizations that want to make
		their standard or product compatible with our ecosystem.</li>
	<li><strong>Integration:</strong> The technologies provided by the Working Group are licensed under
		the <a href="https://www.eclipse.org/legal/epl-2.0/">Eclipse Public
			License 2.0</a> and will form an open platform that provides base
		technologies and solutions. Commercial or open source products can be
		built on top of the platform.
	</li>
	<li><strong>Standardization:</strong> The Working Group is committed to standardizing the core elements
		of the technologies provided. An important example here is the <a
		href="https://www.eclipse.org/basyx/">Eclipse BaSyx project</a> Asset
		Administration Shell, which is currently standardized among industrial
		associations and can provide a backbone for interoperability within
		the Working Group.
	</li>
	<li><strong>Practice:</strong> The Working Group will collect reference solutions and provide
		educational materials for implementing the next generation of
		production environments.</li>
</ul>
<p>
	For more information about the Eclipse Open Industrial Automation
	Working Group and what it means for your organization, email <a
		href="mailto:research@eclipse.org">research@eclipse.org</a>.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img-responsive" alt="Marco Jahn" src="images/4_1.png" />
				</div>
				<div class="col-sm-16">
					<p class="author-name">Marco Jahn</p>
					<p class="author-bio">Marco Jahn is research project manager at the Eclipse Foundation. He obtained his diploma in computer science from Ulm University in 2006 and his PhD from RWTH Aachen in 2016. He worked as software developer at denkwerk GmbH before moving to Fraunhofer FIT. There, he has been working as researcher and (technical) project manager in various European research projects in the areas of IoT and Smart Cities. Furthermore, he led the IoT Platforms team and coordinated the IoT Large-Scale Pilot Project MONICA. He joined the Eclipse Foundation in 2019 where he participates in the foundation’s research activities, helping to turn innovations into successful open source projects.</p>
				</div>
			</div>
		</div>
	</div>
</div>
