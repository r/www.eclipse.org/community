<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2020.30.01</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/thabang.png" />
                          <h3>Editor's Note</h3>

                          <p>Welcome to 2020 and our first community newsletter of the new
                          year and the new decade! With so many interesting and innovative projects
                          underway and strong community engagement, I have no doubt that 2020 will
                          be an exciting year for all of us in the Eclipse community.</p>
                        <p>To give you just a small glimpse into the great progress some
                          of our project teams are making, we&rsquo;re featuring three projects
                          you&rsquo;ll want to follow throughout 2020 and beyond:</p>
                        <ul>
                          <li><a href="https://eclipse.org/community/eclipse_newsletter/2020/january/2.php">Eclipse Corrosion</a>: Max Bureck provides an overview of this
                            Eclipse IDE-based development environment for the Rust programming
                            language and its comprehensive feature set.</li>
                          <li><a href="https://eclipse.org/community/eclipse_newsletter/2020/january/1.php">Eclipse Kuksa</a>: Johannes Kristan celebrates the first
                            release of this software platform and ecosystem for Vehicle-2-X
                            scenarios with an update on project achievements.</li>
                          <li><a href="https://eclipse.org/community/eclipse_newsletter/2020/january/3.php">Eclipse Theia</a>: Sven Efftinge explains why so many
                            companies are using this extensible, multi-language platform to develop
                            all-in-one cloud and desktop IDEs.</li>
                        </ul>
                        <p>Also, don&rsquo;t miss our <a href="https://eclipse.org/community/eclipse_newsletter/2020/january/4.php">News</a> section this month.
                          We&rsquo;re heavily involved in a number of important, upcoming events
                          &mdash; including Cloud Native for Java (CN4J) Day, which will be a
                          watershed day in the open source movement &mdash; and we&rsquo;ve provided
                          details to help you plan your participation and take advantage of special
                          offers.</p>
                        <p>Finally, be sure to check our lists so you can stay up to date
                          with the latest happenings here at the Eclipse Foundation:</p>
                        <ul>
                          <li><a href="#new_project_proposals">New Project Proposals</a></li>
                          <li><a href="#new_project_releases">New Project Releases</a></li>
                          <li><a href="#upcoming_events">Upcoming Eclipse Events</a></li>
                        </ul>
                        <p>
                          In next month&rsquo;s newsletter, we&rsquo;ll turn our focus to Jakarta
                          EE. With momentum toward Jakarta EE 9 building, there&rsquo;s a lot going
                          on in this very vibrant Eclipse community, so make sure to bookmark our <a
                            href="/community/eclipse_newsletter/"
                          >newsletter page</a> and watch your inbox for details.
                        </p>
                        <p>Happy Reading!</p>
                        <p>Thabang Mashologu</p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/january/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/1.php"><h3>Eclipse Kuksa</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/january/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/2.php"><h3>Eclipse Corrosion</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2020/january/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2020/january/3.php"><h3>Eclipse Theia</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_proposals" style="margin-bottom:10px;">New Project Proposals</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li>The <a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-openk-core-modules">Eclipse openK Core Modules</a> provides a set of core modules for developing open source software for system operators which matches the needs of power networks in the 21st century.&nbsp;</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-automated-driving-open-research-adore">Eclipse ADORe</a> provides a modular software library and toolkit for decision making, planning, control and simulation of automated vehicles. The project is focused on decision making, planning and control for automated vehicles.</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li>The <a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-lemminx">LemMinX project</a> provides an LSP implementation for the XML language (aka XML Language Server). It can be used with any editor that supports the protocol, to offer editing support for XML documents, as well as schema-based (XSD and DTD) validation.</li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a href="/projects/project_activity.php">Read on!</a></p>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_releases" style="margin-bottom:10px;">New Project Releases</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.rcptt">Eclipse RCP Testing Tool 2.5.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.escet">Eclipse ESCET Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.zenoh">Eclipse zenoh  Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.skills">Eclipse Skills Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ee4j.krazo">Eclipse Krazo  1.0.0 Release</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.efbt">Eclipse Free BIRD Tools Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.app4mc">Eclipse APP4MC Creation</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0"
                        cellspacing="0"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 id="upcoming_events">Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>

                            <p>Upcoming industry events and workshops where our members will play a key role. Hope to see you there!</p>


<p dir="ltr"><a style="color:#000;" href="https://fosdem.org/2020/">FOSDEM</a><br />
Jan 31-Feb 2 2020 | Brussels, Belgium</p>

<p dir="ltr"><a style="color:#000;" href="https://www.sigs-datacom.de/order/payment/oop2020_en.php?_ga=2.47394720.624396651.1574374423-1714592911.1574111120">OOP Munich</a><br />
Feb 2-Feb 7, 2020 | Munich, Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://bosch-connected-world.com/attend/">Bosch ConnectedWorld</a><br />
Feb 9-10, 2020 | Berlin, Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://www.eventbrite.com/e/eclipse-iot-day-grenoble-2020-tickets-89502320851">IoT Day Grenoble</a><br />
Feb 14, 2020 | Grenoble, France</p>

<p dir="ltr"><a style="color:#000;" href="https://bosch-connected-world.com/hackathon/">Bosch ConnectedExperience</a><br />
Feb 17-19, 2020 | Berlin,Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://festival.oscafrica.org/">Open Source Africa Festival</a><br />
Feb 20-22, 2020 | Lagos, Nigeria</p>

<p dir="ltr"><a style="color:#000;" href="https://devnexus.com/">Devnexus</a><br />
Feb 18-21, 2020 | Atlanta, GA</p>

<p dir="ltr"><a style="color:#000;" href="https://www.mwcbarcelona.com/">MCW20</a><br />
Feb 24-27, 2020 | Barcelona, Spain</p>

<p dir="ltr"><a style="color:#000;" href="http://eclipse-5413615.hs-sites.com/mwc20-workshop-invitation-request-form">Barcelona Strategic Open Source Summit</a><br />
Feb 25, 2020 | Barcelona, Spain</p>

<p dir="ltr"><a style="color:#000;" href="https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/">KubeCon + CloudNativeCon Europe</a><br />
March 30 - April 2, 2020 | Amsterdam, The Netherlands</p>

<p dir="ltr"><a style="color:#000;" href="http://eclipse.org/cn4j-day">Cloud Native for Java Day</a><br />
March 30, 2020 | Amsterdam, The Netherlands</p>

<p dir="ltr"><a style="color:#000;" href="https://tmt.knect365.com/iot-world/">IoT World</a><br />
Apr 6-9, 2020 | San Jose, CA</p>


<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 id="contact_us" style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->