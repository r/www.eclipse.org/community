<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Cloud Native for Java Day Marks an Industry First</h2>
<p>Don&rsquo;t miss your opportunity to engage with the best and brightest minds from the
  enterprise Java and Kubernetes communities at Cloud Native for Java (CN4J) Day on March 30 at
  KubeCon + CloudNativeCon Europe in Amsterdam.</p>
<p>The all-day event includes expert talks, demos, and thought-provoking sessions focused
  on building cloud native enterprise applications using Jakarta EE-based microservices on
  Kubernetes. It marks the first time Java and Kubernetes experts are coming together at one event
  to collaborate and share their expertise.</p>
<p>CN4J Day is an important and truly unique opportunity to:</p>
<ul>
  <li>Learn more about the future of cloud native Java development from industry and community
    leaders</li>
  <li>Gain deeper insight into key aspects of Jakarta EE, MicroProfile, and Kubernetes technologies</li>
  <li>Meet and share ideas with global Java and Kubernetes ecosystem innovators</li>
</ul>
<p>The day features a stellar lineup of keynote speakers and informative technical talks
  from leading Java experts and Eclipse Foundation community members.</p>
<p>Thanks to the generous support of our sponsors, there&rsquo;s a minimal cost of $50 to
  attend CN4J Day. Simply add the event to your KubeCon + CloudNativeCon Europe registration.</p>
<p>
  For more details and a link to the registration page, click<a href="/cn4j-day">
    here</a>.
</p>
<hr>
<h2>Bosch ConnectedWorld to Showcase Eclipse IoT and Edge Native Innovations</h2>
<p>
  Drop by and see the Eclipse Foundation community in booth #24 at<a
    href="https://bosch-connected-world.com/"
  > Bosch ConnectedWorld 2020</a> (BCW20) Feb 19-20 in Berlin, Germany, where we&rsquo;ll be sharing
  the latest advances in open source IoT and edge computing innovations. You&rsquo;ll find a great
  mix of demos and presentations featuring key<a href="https://iot.eclipse.org/"> IoT</a> and<a
    href="https://edgenative.eclipse.org/"
  > Edge Native</a> Working Group projects, including:
</p>
<ul>
  <li><a href="/ditto/">Eclipse Ditto</a> for organizing and managing digital
    twins for IoT devices</li>
  <li><a href="/hono/">Eclipse Hono</a> for connecting large numbers of IoT
    devices</li>
  <li><a href="/hawkbit/">Eclipse hawkBit</a> for rolling out software
    updates to edge devices, controllers, and gateways</li>
  <li><a href="/vorto/">Eclipse Vorto</a> for modeling IoT devices</li>
  <li><a href="https://iofog.org/">Eclipse ioFog</a> for building and running enterprise-scale
    applications at the edge</li>
  <li><a href="https://fog05.io/">Eclipse fog05</a> for distributing computing, storage, control,
    and networking functions closer to users</li>
</ul>
<p>
  Beyond our booth, you&rsquo;ll also find numerous Eclipse Foundation technologies integrated into<a
    href="https://www.bosch-iot-suite.com/"
  > commercial IoT solutions from Bosch</a> and its ecosystem partners.
</p>
<p>
  We&rsquo;re very pleased to offer Eclipse community members a discounted registration fee of 850
  euros. To take advantage of the special price, register<a href="http://www.bcw-registration.com/">
    here</a> and use the code EclipseFdn@BCW20.
</p>
<hr>
<h2>Eclipse IoT Community Members to Adjudicate Open Source Prize at Bosch
  ConnectedExperience 2020</h2>
<p>
  Eclipse Foundation IoT and Edge Computing Program Manager, Fr&eacute;d&eacute;ric Desbiens, and
  members of the<a href="https://iot.eclipse.org/"> Eclipse IoT</a> community will adjudicate the
  open source prize at<a href="https://bosch-connected-world.com/hackathon/"> Bosch
    ConnectedExperience 2020</a> (BCX20), Europe&rsquo;s largest IoT hackathon.
</p>
<p>
  BCX20 takes place February 17-19 in Berlin, Germany and is collocated with<a
    href="https://bosch-connected-world.com/"
  > Bosch ConnectedWorld</a> 2020 (BCW20). Participants have 30 hours to team up and tackle several
  hack challenges using more than 135 connectable devices from Bosch and its ecosystem partners. On
  the final day, each team will present their prototypes and ideas to other hackathon participants
  and the jury.
</p>
<p>
  BCX attracts more than 700 developers and 90 hack coaches from around the world and has become a
  &ldquo;can&rsquo;t miss&rdquo; event for the global IoT ecosystem. Anyone can<a
    href="https://bosch-connected-world.com/attend/tickets-prices/"
  > purchase a ticket</a> to BCX. Students and employees of start-ups can<a
    href="https://www.bcw-registration.com/registration/index"
  > apply for a free ticket</a>.
</p>
<hr>
<h2>IoT Community Meetings to Follow BCW20 in Berlin</h2>
<p>Everyone in the Eclipse community is invited to participate in the Eclipse IoT
  community meetings that will be hosted by Bosch in Berlin on Friday, February 21 after BCW20
  finishes up.</p>
<p>
  These face-to-face meetings are a great way for Eclipse IoT community members to meet and connect
  with one another and for everyone in the broader Eclipse community to learn more about
  what&rsquo;s happening in the<a href="https://iot.eclipse.org/"> Eclipse IoT</a> and<a
    href="https://edgenative.eclipse.org/"
  > Edge Native</a> Working Groups.
</p>
<p>If you&rsquo;re interested in attending the meetings and haven&rsquo;t already
  registered, email iot@eclipse.org.</p>
<hr>
<h2>Eclipse Che Webinar Now Available for On-Demand Viewing</h2>
<p>
  Watch this<a href="https://webinars.devops.com/eclipse-che"> recorded webinar</a> to learn more
  about<a href="/che/"> Eclipse Che 7</a>, the world&rsquo;s first open
  source, Kubernetes-native IDE. Che defines workspaces that include their dependencies including
  embedded containerized runtimes, web-based IDE (based on <a href="https://theia-ide.org/">Eclipse
    Theia</a>), and project code.
</p>
<p>You&rsquo;ll learn from a panel of industry experts how this revolutionary IDE for
  distributed and mainframe development drives developer productivity and improves overall software
  delivery. The panel features:</p>
<ul>
  <li>Paul Buck, vice president of community development at the Eclipse Foundation</li>
  <li>Stevan Le Meur, product manager at Red Hat</li>
  <li>Dana Boudreau, product manager at Broadcom</li>
</ul>
<hr>
<h2>Eclipse Foundation to Host Barcelona Strategic Open Source Summit</h2>
<p>On Tuesday, February 25, the Eclipse Foundation will host the first-ever Barcelona
  Strategic Open Source Summit. This exclusive, invitation-only event is sponsored by Huawei and
  runs concurrently with Mobile World Congress 2020 (MWC20).</p>
<p>Located within walking distance of MWC20, the summit brings together C-level
  executives, decision-makers, and thought leaders to share best practices, discuss emerging open
  source technologies, and learn about sustainable and scalable open collaboration in Europe. Topics
  will include opportunities in telecommunications, artificial intelligence, cloud, automotive,
  Industry 4.0, Internet of Things (IoT), and edge computing markets and technologies.</p>
<p>
  To request your complimentary invitation, click <a
    href="http://eclipse-5413615.hs-sites.com/mwc20-workshop-invitation-request-form"
  >here</a>. If you&rsquo;re interested in sponsoring the Barcelona Strategic Open Source Summit,<a
    href="mailto:gael.blondelle@eclipse-foundation.org"
  > email Gael Blondelle</a> at the Eclipse Foundation.
</p>
<hr>
<h2>Eclipse Foundation Sponsoring First-Ever Open Source Africa Festival</h2>
<p>
  The Eclipse Foundation proudly supports <a href="https://www.oscafrica.org/">Open Source Community
    Africa</a>&rsquo;s mission to increase African contributions to open source projects. That&#39;s
  why we are sponsoring the first-ever <a href="https://festival.oscafrica.org/">Open Source Africa
    Festival</a>, February 20-22, in Lagos, Nigeria.
</p>
<p>According to Open Source Community Africa, the festival is expected to attract student
  delegates, developers, designers, and corporate organizations with a series of talks, workshops,
  and insight into open source developer tools.</p>
<p>We&rsquo;re pleased to offer three free passes to this event to Eclipse community
  members in the Lagos, Nigeria, area. An Eclipse account is required to claim a free pass and
  there&rsquo;s a limit of one pass per person. For more information, contact events@eclipse.org.</p>
