<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>A Single IDE for Cloud and Desktop Deployments</h2>
<p>
  As the first official release of<a href="https://theia-ide.org/"> Eclipse Theia</a> approaches,
  Project Lead, Sven Efftinge, explains why so many companies &mdash; big and small &mdash; are
  using this extensible, multi-language platform to develop all-in-one cloud and desktop IDEs.
</p>
<p>
  <strong>Q. What exactly is Eclipse Theia?</strong>
</p>
<p>
  A. Eclipse Theia is a framework for building a fully customized, single-source IDE that runs in
  web browsers and as a native desktop application. It combines the design ideas and ecosystem from
  the Microsoft Visual Studio Code IDE with the flexibility and customization capabilities from the<a
    href="/eclipseide/"
  > Eclipse IDE</a> so it offers the best of both worlds. Plus, it runs in a browser.
</p>
<p>
  <strong>Q. Why is it important that Eclipse Theia supports cloud and desktop IDE development?</strong>
</p>
<p>A.&nbsp; Traditionally, IDEs run as a native app in a desktop operating system. But,
  more and more of the applications developers use, from email to spreadsheets to text documents,
  are in the cloud and accessed through browsers. The IDE is one of the last remaining desktop
  applications developers use.</p>
<p>For years now, companies have been thinking about how they can move to an IDE with a
  fresh technology stack and cloud-based development tools. But, from a strategic perspective,
  it&rsquo;s often difficult for them to determine whether they should invest in redeveloping their
  desktop IDE for the web. The same is true if they started with a cloud IDE, but now need desktop
  development capabilities.</p>
<p>With Eclipse Theia, companies are freed from having to make these difficult decisions.
  They can develop a modern IDE once then deploy it in both cloud and desktop development
  environments.</p>
<p>
  <strong>Q. Who are some of the companies that are using the Theia technology?</strong>
</p>
<p>A. Right from the beginning, major, global companies recognized the value that Theia
  provides. In fact, Ericsson was one of our first contributors &mdash; even before the project was
  part of the Eclipse Foundation &mdash; and they&rsquo;re still one of our top contributors.</p>
<p>Today, a number of very prominent companies use Theia as the basis for their IDEs,
  including:</p>
<ul>
  <li>Arm and Arduino, who are using Theia to write embedded code</li>
  <li>Google, who is using Theia in its Google Cloud shell</li>
  <li>IBM, who is using Theia for multiple projects</li>
  <li>Red Hat, who has replaced its Java-based IDE with Theia as part of the<a
    href="/che/"
  > Eclipse Che</a> project
  </li>
  <li>SAP, who has replaced its existing web IDE with Theia</li>
</ul>
<p>We&rsquo;re also seeing a lot of engagement and adoption among smaller businesses and
  startups, and across industries &mdash; from hardware design to aerospace, insurance, and finance.</p>
<p>Theia really is ideal for all companies of all sizes in all industries.</p>
<p>
  <strong>Q. Why do you think companies that have traditionally used in-house or proprietary IDEs
    are moving to Eclipse Theia, which is open source?</strong>
</p>
<p>A. Companies want to ensure that when they invest in an IDE, no single vendor is
  controlling the future or scope of it. Projects at the Eclipse Foundation are guaranteed to be
  completely vendor-neutral.</p>
<p>They also want to leverage the contributions of a diverse and engaged community.
  Eclipse Theia has 125 contributors so the project is very active and continually advancing.</p>
<p>Companies that use Theia gain all the advantages of a truly open source IDE, and they
  have the flexibility to easily customize the platform for their own development requirements.</p>
<p>
  <strong>Q. What makes Eclipse Theia so easy to customize?</strong>
</p>
<p>A. From an architectural perspective, the project consists of many small extensions
  that work together. You can pick the extensions you want to include and skip those you don&rsquo;t
  want to include. You can also write your own extensions.</p>
<p>In addition, you can modify the functionality and the user interface at a very
  fine-grained level using libraries. There&rsquo;s no need to patch or fork the core code.</p>
<p>
  <strong>Q. Why is this flexibility important?</strong>
</p>
<p>A. The architectural flexibility makes it easy to build an IDE that&rsquo;s aligned
  with business goals and developer requirements.</p>
<p>For example, one goal might be to minimize the onboarding time and learning curve for
  developers migrating to the new IDE. Another might be to provide access to more advanced
  development tools. We helped Arduino tailor the user interface and user experience so it&rsquo;s
  very similar to their legacy IDE, &ldquo;hiding&rdquo; more advanced capabilities behind a button.
  These types of approaches are particularly helpful for companies that need to migrate a large
  developer base to the new IDE.</p>
<p>Theia is also very flexible from a programming perspective. It natively supports Visual
  Studio Code extensions, and there are more than 15,000 of them in the marketplace. That means
  developers can use just about any programming language in a Theia-based IDE.</p>
<p>
  <strong>Q. Is it hard to contribute to Eclipse Theia?</strong>
</p>
<p>A. No, it&rsquo;s super-easy. There&rsquo;s no long readme about how to set up your
  computer, the compilers you need, or the tools that are required. You just click one button and
  you can start coding.</p>
<p>You can create a workspace using Gitpod.io, our professional hosting solution for
  Theia. In just a few seconds, it gives you a fully built Theia code base you can use to contribute
  changes to the project.</p>
<p>
  We see this simplicity as one of the reasons we have so many high-quality contributions. The
  project has a star rating of almost 7,000 on<a href="https://github.com/eclipse-theia/theia">
    GitHub</a> so developers obviously like what we&rsquo;re doing.
</p>
<p>
  <strong>Q. What are the upcoming plans for Eclipse Theia?</strong>
</p>
<p>A. We&rsquo;re focusing on being 100 percent compatible with all Visual Studio Code
  extensions. Right now, we support about 90 percent of them so we&rsquo;re working on the last 10
  percent.</p>
<p>And, we&rsquo;re targeting a Theia 1.0 release on March 26.</p>
<p>Longer term, we&rsquo;ll be focusing on strengthening all of the aspects associated
  with a mature, production-ready platform &mdash; robustness, speed, reliable performance, and
  APIs.</p>
<p>
  <strong>Q. How can people get involved in Eclipse Theia? </strong>
</p>
<p>A. We have weekly developer meetings and we welcome everyone who is interested to join
  the calls.</p>
<p>
  For more details, the best place to start is the<a href="https://theia-ide.org/"> Theia website</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/january/images/sven.png" alt="Sven Efftinge"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name">Sven Efftinge</p>
          <p>CEO<br>TypeFox GmbH</p>
        </div>
      </div>
    </div>
  </div>
</div>