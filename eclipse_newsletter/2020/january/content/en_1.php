<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Celebrating Our First Release</h2>
<p>
  Almost two years ago,<a href="/kuksa/"> Eclipse Kuksa</a> began as a new
  project at the Eclipse Foundation. In the July 2018 newsletter, we provided<a
    href="/community/eclipse_newsletter/2018/july/kuksa.php"
  > an overview of the project</a>, its goals, and organization. Now, we&rsquo;re celebrating<a
    href="/kuksa/blog/2019/11/01/2019-11-01-release010/"
  > our first release</a>, and we would like to take this opportunity to provide an update on our
  activities.
</p>
<h2>A Quick Look at Eclipse Kuksa</h2>
<p>
  First, some context. Eclipse Kuksa is a software platform and ecosystem for Vehicle-2-X scenarios.
  It was initiated within the publicly funded ITEA project,<a
    href="https://itea3.org/project/appstacle.html"
  > APPSTACLE</a>.&nbsp;
</p>
<p>From a technical perspective, Eclipse Kuksa is based on three main pillars:</p>
<ul>
  <li>An in-vehicle platform<br /> The in-vehicle platform consists of a container-based app runtime
    and supporting facilities that sit on top of a Linux-based operating system such as<a
    href="https://www.automotivelinux.org/"
  > Automotive Grade Linux</a> (AGL) or<a href="https://apertis.org/"> Apertis</a>. The platform
    provides a controlled environment to execute new vehicle functionality as well as secure and
    standardized (<a href="https://www.w3.org/TR/vehicle-information-service/">W3C Vehicle
      Information Service Specification (VISS)</a> or<a href="https://sensor-is.org/"> Sensoris</a>)
    access to vehicle data.
  </li>
  <li>A cloud backend<br /> The cloud backend integrates several projects from the<a
    href="https://iot.eclipse.org/"
  > Eclipse IoT Working Group</a>, including<a
    href="https://sensor-is.org/%20%20%5b6%5d%20https:/www.eclipse.org/hono/"
  > Eclipse Hono</a>,<a href="https://www.eclipse.org/hawkbit/"> Eclipse hawkBit</a>, and<a
    href="https://www.eclipse.org/ditto/"
  > Eclipse Ditto</a>, and provides a common Kubernetes-based deployment to set up an automotive IoT
    cloud backend. It provides an app store that offers a user-focused management UI and automated
    deployment of in-vehicle apps using Eclipse hawkBit as the underlying technology.
  </li>
  <li>An IDE<br /> The IDE is based on<a href="/che/"> Eclipse Che</a> and
    Visual Studio Code, providing automation for developing in-vehicle apps. The IDE interacts with
    the app store to simplify app deployments.
  </li>
</ul>
<h2>2019 Was a Big Year for Eclipse Kuksa</h2>
<p>The Eclipse Kuksa project was very active in 2019. The year started with a large meeting and
  project hackathon in Berlin. This event allowed interested developers to check out the project and
  tinker with its components to increase development traction. It also allowed us to kickstart work
  on two bigger features: the container-based app management needed to support software over the air
  (SOTA) and firmware over the air (FOTA) functionality.</p>
<p>The first versions of these two features, along with several other improvements, were included in
  our first milestone release, Kuksa-0.1.0-M1, in February. This was a great achievement because the
  feature set for our first release was almost finalized, our third-party dependencies were almost
  cleared, and we were ready to work toward our first release.</p>
<p>Just prior to the release, the project team met for another hackathon in Paderborn to fix the
  remaining open issues and polish the software to get release 0.1.0 out the door in September.</p>
<h2>Release 0.1.0 Includes Key Features</h2>
<p>All of this hard work meant we were able to pack a solid set of features into release 0.1.0.</p>
<p>On the in-vehicle side, we introduced the new container-based app management, a virtual CANbus
  that allows apps to access the Controller Area Network (CAN) in a secure and controlled way.</p>
<p>The in-vehicle platform also includes an Intrusion Detection System (IDS) that identifies network
  traffic anomalies to prevent malicious access. Another important feature is a unified API based on
  the W3C VISS that provides access to data from the vehicle through a tree-like data structure.
  Finally, the FOTA functionality provides a means to update the in-vehicle platform within a
  vehicle to a newer version over the air.&nbsp;</p>
<p>On the cloud backend side, we improved security, usability, and stability. We also introduced the
  Kuksa app store that provides a user-facing frontend to install, update, and remove apps on
  registered vehicles.</p>
<h2>The New Features Are Great for Demos</h2>
<p>The new features are handy for creating use cases and demonstrating them at different events.</p>
<p>At an APPSTACLE project meeting, OTOKAR presented two interesting use cases:</p>
<ul>
  <li>A driver authentication system that blocks vehicle ignition until the user logs in and
    receives authorization to start the engine.</li>
  <li>A system that identifies aggressive driver behavior by detecting anomalies in data collected
    from the vehicle.</li>
</ul>
<p>You can see part of these demonstrations in the photo below.</p>
<p><img src="images/1_1.png" style="height: 198px; width: 318px"/></p>
<p>
  In addition, Robert Bosch GmbH demonstrated a roadside assistance system (shown below) at
  EclipseCon Europe 2019 and other events. The system demonstrates how a car repair shop can use
  remote vehicle access to check for possible problems and to either resolve the issue or analyze it
  so service technicians are informed and know which parts are affected.
</p>
<p><img src="images/1_2.png" style="height: 189px; width: 142px"/></p>
<p>These examples are great ways of showing the potential that Eclipse Kuksa opens up. A big thank
  you to everyone who has contributed so far and made all of this possible!</p>
<p>
  If you&rsquo;re interested in Eclipse Kuksa, please<a href="/kuksa/"> visit
    our website</a>,<a href="https://accounts.eclipse.org/mailing-list/kuksa-dev"> subscribe to our
    mailing list</a>, or<a href="https://github.com/eclipse?q=kuksa"> check out the code</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/january/images/johannes.png" alt="Johannes Kristan"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Johannes Kristan</p>
          <p>Senior Expert<br>Bosch Software Innovations GmbH</p>
        </div>
      </div>
    </div>
  </div>
</div>