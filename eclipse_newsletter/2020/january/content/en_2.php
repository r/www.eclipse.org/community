<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Supporting the Future of Reliable Infrastructure</h2>
<p>
  <a href="https://projects.eclipse.org/projects/tools.corrosion">Eclipse Corrosion</a> is the
  Eclipse IDE-based development environment for the Rust programming language. The Rust programming
  language is gaining popularity, ranking as the &ldquo;most loved&rdquo; language on<a
    href="https://insights.stackoverflow.com/survey/2018/#technology-_-most-loved-dreaded-and-wanted-languages"
  > stackoverflow&lsquo;s developer survey</a> three years in a row. The language is so interesting,
  it&rsquo;s even finding its way into the projects of tech giants, such as Amazon, Google,
  Microsoft, and Facebook.
</p>
<h2>Rust Offers Key Language Features</h2>
<p>Rust is a relatively new contender in the realm of so-called &ldquo;systems
  programming&rdquo; languages.</p>
<p>The language allows fine-grained control of memory usage and layout, which makes it
  suitable for low-level tasks. The compiled programs usually have low memory overhead and fast
  execution times. These benefits allow Rust to compete with C and C++, the longstanding predominant
  languages in systems programming. There are also important differentiators that can make Rust a
  better choice for certain use cases, rather than the ubiquitous C and C++ languages.</p>
<p>Rust&rsquo;s key language features are:</p>
<ul>
  <li>Compile-time-guaranteed memory safety (no null-reference access, no use-after-free, no
    double-free, no buffer over- or underflows)</li>
  <li>Data race freedom</li>
</ul>
<p>These features eliminate a range of problems commonly associated with languages that
  use manual memory management. As a result, Rust is ideal for implementing critical infrastructure
  components and programs that need performance as well as a high standard of safety and security.
  However, the compiler is strict and allows fewer patterns of data flow and memory access than are
  allowed in C and C++.</p>
<p>The following simple code example (Figure 1) illustrates a case where the Rust compiler
  has identified a potential use-after-free error.</p>
<p>
  <strong>Figure 1: Potential Use-After-Free Error Flagged by the Rust Compiler</strong>
</p>
<p>
  <img src="images/2_1.png" style="height: 185px; width: 454px"/>
</p>
<h2>Rust&rsquo;s Toolchain Also Offers Advantages</h2>
<p>
  But, Rust has more to offer than plain language features. The toolchain is lauded for being easy
  to use and offering simple dependency management compared to C and C++. Rust offers a toolchain
  version manager, called<a href="https://rustup.rs/"> Rustup</a>, and a default build tool, called
  Cargo, that is installed with the compiler. Cargo also manages Rust projects dependencies. It can
  easily fetch dependencies (so-called crates) from the central package repository<a
    href="https://crates.io/"
  > crates.io</a>.
</p>
<p>Concrete examples of projects that are a great fit for Rust include:</p>
<ul>
  <li>Fast command line tools, such as Ripgrep</li>
  <li>Databases, such as TiKV</li>
  <li>Virtualization technologies, such as Amazon&rsquo;s Firecracker</li>
  <li>Embedded and Internet of Things (IoT) systems, such as parts of Microsoft&rsquo;s Azure IoT
    Edge technology</li>
  <li>Operating systems, such as Tock OS or Redox OS</li>
  <li>Network applications with very low overhead, such as Linkerd Proxy</li>
  <li>Parts of safety and security sensitive applications, such as the Firefox browser</li>
</ul>
<p>
  To become familiar with the Rust language, reading the free<a
    href="https://doc.rust-lang.org/book/"
  > Rust Book</a> is a great way to start.&nbsp;
</p>
<h2>Getting Started With Corrosion and Rust</h2>
<p>
  The Eclipse IDE has supported systems programming for a long time. The<a
    href="/cdt/"
  > Eclipse CDT</a> project is a widely used C and C++ programming plug-in for the Eclipse IDE. The
  Eclipse Corrosion plug-in follows a similar path, providing an integrated environment for
  developing Rust applications within the Eclipse IDE.
</p>
<p>
  To start writing Rust programs with Corrosion, the easiest approach is to download the<a
    href="/downloads/packages/release/2019-12/r/eclipse-ide-rust-developers-includes-incubating-components"
  > Eclipse IDE for Rust Developers</a>. If you use the Eclipse installer, you can install the
  package from there as well.
</p>
<p>
  To start using the Rust toolchain, most people use Rust&#39;s toolchain version manager,<a
    href="https://rustup.rs/"
  > Rustup</a>.
</p>
<p>
  For more information about installing and using Corrosion, visit the<a
    href="https://github.com/eclipse/corrosion"
  > Corrosion GitHub Page</a>.&nbsp;
</p>
<h2>A Comprehensive Feature Set</h2>
<p>Corrosion supports most of the features one would expect from a modern IDE:</p>
<ul>
  <li>Syntax highlighting</li>
  <li>Code completion (with code templates)</li>
  <li>Outline view</li>
  <li>Compile error and warning diagnostics, shown in the editor</li>
  <li>Quick fixes</li>
  <li>Compiling and running programs and tests with cargo</li>
  <li>Debugging programs using GDB (Figure 2)</li>
  <li>Code formatting</li>
  <li>Jump-to-definition (using Ctrl+Click in the code editor)</li>
  <li>Find references</li>
  <li>Documentation pop-up when hovering over elements</li>
  <li>Toggle comment&nbsp;&nbsp;</li>
</ul>
<p>
  <strong>Figure 2: Debugging in Corrosion</strong>
</p>
<p>
  <img src="images/2_2.png" style="height: 269px; width: 453px"/>
</p>
<p>And there are more features to explore.</p>
<p>The secret to Corrosion is that this rich feature set is, at its core, not implemented
  in the Corrosion project itself. Many puzzle pieces make up the software that is shipped as
  Eclipse Corrosion.&nbsp;</p>
<h2>Standing on the Shoulders of Giants</h2>
<p>It is remarkable how compact the Corrosion plug-in actually is. This is accomplished by
  building on top of the great work in the Eclipse technologies Corrosion depends on, as well as a
  relatively new concept: The language server protocol (LSP) (Figure 3).</p>
<p>
  <strong>Figure 3: The Stack Corrosion Is Built on</strong>
</p>
<p>
  <img src="images/2_3.png" style="height: 224px; width: 350px"/>
</p>
<p>The language server protocol provides an API between an IDE and a backend to ensure the
  language is understood by a frontend IDE or editor. For Rust, this server is the Rust Language
  Server (RLS) that can be installed using the Rustup tool.</p>
<p>
  To leverage the RLS from the Eclipse IDE, the<a
    href="https://projects.eclipse.org/projects/technology.lsp4j"
  > Eclipse LSP4J</a> project provides a Java implementation of the LSP for language servers and
  language server clients. The<a href="https://projects.eclipse.org/projects/technology.lsp4e">
    Eclipse LSP4E</a> project sits on top of LSP4J to generically integrate the protocol with the
  Eclipse IDE.
</p>
<p>
  The<a href="https://projects.eclipse.org/projects/technology.tm4e"> Eclipse TM4E</a> project
  provides the ability to perform syntax highlighting based on TextMate grammar definitions.
  Corrosion uses this framework to perform syntax highlighting based on an existing TextMate grammar
  file.&nbsp;
</p>
<p>The Rust toolchain does not currently come with a Debug Adapter Protocol (DAP) server.
  The DAP is a sister protocol to the LSP, allowing IDEs to start and debug applications. The lack
  of a DAP server means debugging support in Corrosion must be accomplished in a different way.</p>
<p>
  The<a href="/cdt/"> Eclipse CDT</a> project allows native applications to
  be debugged using the GDB debugger. Corrosion reuses this functionality to support debugging of
  Rust applications.
</p>
<p>
  Using these existing technologies is a mutually beneficial relationship. To improve Corrosion,
  committers are contributing to the Eclipse CDT, LSP4E, and<a href="/mylyn/">
    Mylyn</a> projects. These efforts improve Corrosion, as well as the upstream Eclipse
  technologies.
</p>
<h2>Improvements Are Ongoing</h2>
<p>The development pace in Corrosion is steady as we eliminate minor issues in Rust
  development one by one. For example, the upcoming version will support a CodeLense for running
  single test cases directly from their definition (Figure 4). It will also come with diagnostics in
  the debug launch configuration to indicate any problems with the debugger configuration (Figure
  5).</p>
<p>
  <strong>Figure 4: Starting a Test From Its Definition</strong>
</p>
<p>
  <img src="images/2_4.png" style="height: 115px; width: 324px"/>
</p>
<p>
  <strong>Figure 5: Debugger Errors Shown in the Launch Dialog</strong>
</p>
<p><img src="images/2_5.png" style="height: 307px; width: 401px"/></p>
<p>Other improvements will be available in Corrosion through improvements to the
  underlying components, which are evolving as well.</p>
<p>
  When RLS v2.0 is completed, Corrosion will gain many great features, including more intelligent
  (postfix) code completions and much more. For a glimpse into the future of RLS, take a look at the<a
    href="https://rust-analyzer.github.io/thisweek"
  > rust-analyzer project</a>.
</p>
<h2>Get Involved in Corrosion</h2>
<p>We always welcome contributions to Corrosion and are keen to know how we can improve
  the project as well as which features are most important to our users.</p>
<p>
  To report issues, make a feature request, comment on existing issues, or discuss contributions,
  please visit the<a href="https://github.com/eclipse/corrosion/issues"> Corrosion issue tracker</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/january/images/max.png" alt="Max Bureck"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Max Bureck</p>
          <p>Senior Researcher<br>Fraunhofer Institute for Open Communication Systems (FOKUS)</p>
        </div>
      </div>
    </div>
  </div>
</div>