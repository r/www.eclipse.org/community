<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Modular user interfaces, particularly the<a href="https://wiki.eclipse.org/Rich_Client_Platform"> Eclipse Rich Client Platform (RCP)</a>, can help developers leverage the benefits of microservices. The first step is to create the technical infrastructure required to integrate with the backend microservices. In this article, I&rsquo;ll demonstrate how an Eclipse RCP client can easily consume a service that&rsquo;s exposed as a REST endpoint.</p>

<p>When demonstrating this approach, it&rsquo;s helpful to have a stable and interesting REST API to consume. Luckily, SpaceX provides a set of well-documented services that describe various domains &mdash; launches, missions, rockets, and many others. In this example, I&rsquo;ll demonstrate how to consume the SpaceX Launch Service.</p>

<p>I&rsquo;ve kept the example as simple as possible to clearly show the moving parts. You should be able to clone this repository and get things running very quickly. I&rsquo;ve also created a<a href="https://github.com/modular-mind/spacex-client"> repository in GitHub</a> that shows the implementation in more detail.</p>

<h2>Setting up the Environment</h2>

<p>The approach relies on a set of Eclipse frameworks that work together to make it very simple to consume REST services using Jakarta RESTful Web Services (JAX-RS). Figure 1 shows the stack of these frameworks.</p>

<p><strong>Figure 1: Eclipse Frameworks Stack Used to Consume REST Services</strong></p>

<p><img src="images/3_1.png" alt="Figure 1: Eclipse Frameworks Stack Used to Consume REST Services" /></p>

<p>These frameworks allow you to create a Java interface that&rsquo;s annotated with JAX-RS, then expose a proxy to that interface as an Open Services Gateway initiative (OSGi) service. Apache CXF and<a href="https://eclipse-ee4j.github.io/jersey/"> Eclipse Jersey</a> implementations are provided, and Jersey is used in the example. Both providers use the Jackson JavaScript Object Notation (JSON) processor to handle data binding between the service and the Java types.</p>

<p>There is a GitHub repository for the<a href="https://github.com/ECF/JaxRSProviders"> JAX-RS providers</a>. If you would like to consume the pre-built features in an Eclipse target definition file or Tycho build, you can use<a href="https://raw.githubusercontent.com/ECF/JaxRSProviders/master/build"> this p2 repository location</a>.</p>

<p>However, if you&rsquo;re trying to set this up for the first time, I highly recommend using the target definition I&rsquo;ve included in the GitHub repository.</p>

<h2>Describing and Discovering the Service</h2>

<p>One way to describe a service is to use an<a href="https://wiki.eclipse.org/EIG:File-based_Discovery_with_the_Endpoint_Description_Extender_Format"> Endpoint Description Extender Format (EDEF)</a> file. An EDEF file describes how to connect to an endpoint and how to map the service to Java types using JAX-RS. The two most important attributes described by an EDEF file are the Uniform Resource Identifier (URI) of the endpoint and the Java type that will represent the service. Figure 2 illustrates these descriptions.</p>

<p><strong>Figure 2: EDEF File Base Endpoint URI and Java Interface Descriptions</strong></p>

<p><img src="images/3_2.png" alt="Figure 2: EDEF File Base Endpoint URI and Java Interface Descriptions" /></p>

<p>To ensure the EDEF file is discovered at runtime, include a Remote-Service entry in your OSGi manifest, as shown below:</p>

<p><code>Remote-Service: OSGI-INF/LaunchService_EDEF.xml</code></p>

<p>At runtime, the EDEF file is read and a proxy for the Java interface is registered as an OSGi service. The service can then be called without knowing or caring that a REST service is working behind the scenes.</p>

<h2>Implementing the Service</h2>

<p>The EDEF file specifies a Java interface annotated using JAX-RS, as shown below.</p>

<p><img src="images/3_3.png" alt="The EDEF file specifies a Java interface annotated using JAX-RS" /></p>

<p>The launch data returned from the service must also be represented. In the example code shown below, Jackson annotations are used to help with object mapping, but it&rsquo;s possible to have a completely unannotated Plain Old Java Objects (POJOs) mapping as well.</p>

<p><img src="images/3_4.png" alt="example code" /></p>

<p>Running the code at this point would result in a LaunchService instance that is available as an OSGi service. This service could be consumed using regular OSGi mechanisms, such as a Service Tracker or Declarative Services, and used in any OSGi environment. I&rsquo;ll describe how to access it from an Eclipse RCP application.</p>

<h2>Integrating With Eclipse RCP</h2>

<p>While an Eclipse RCP client can consume OSGi services using a Service Tracker or Declarative Services, you can also inject services directly into the UI components.</p>

<p>To inject a service created using the JAX-RS provider:</p>

<ul>
	<li>Inject the service using the @Inject and @Service annotations. The @Service annotation fixes some class-loading issues that occur with the normal injection mechanism.</li>
	<li>Configure the plug-in containing the UI component to be activated when one of its classes is requested. You can do this on the Overview page of the Manifest Editor, as shown in Figure 3.</li>
</ul>

<p><strong>Figure 3: Manifest Editor Overview Page</strong></p>

<p><img src="images/3_5.png" alt="Figure 3: Manifest Editor Overview Page" /></p>

<p>The injected service can now be used inside the Eclipse RCP UI component to request SpaceX launch data, as shown below.</p>

<p><img src="images/3_6.png" alt="Eclipse RCP UI" /></p>

<p>In this section of the code, I&rsquo;m retrieving the launch number and name, then displaying the data in a table. Figure 4 shows the running application.</p>

<p><strong>Figure 4: Retrieved SpaceX Launch Data</strong></p>

<p><img src="images/3_7.png" alt="Figure 4: Retrieved SpaceX Launch Data" /></p>

<h2>Get Started in Just 10 Minutes</h2>

<p>Very little code is required to consume REST services from an Eclipse RCP client. Once this infrastructure is set up, you can start using the power of a modular user interface to leverage microservices architectures across the entire stack.</p>

<p>The easiest way to start is to clone<a href="https://github.com/modular-mind/spacex-client"> the GitHub repository</a> and run the code. It shouldn&rsquo;t take longer than 10 minutes to get things running.</p>

<p>I&rsquo;d like to thank Scott Lewis and the<a href="https://www.eclipse.org/ecf/"> Eclipse Communication Framework</a> team for doing the work that makes all of this possible. If it seems a bit too easy, remember all the effort that went into making that easy.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/patrick.png" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
