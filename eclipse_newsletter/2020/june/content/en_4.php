<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>IoT Developers: Please Complete the Survey by June 26</h2>
<p>
  The<a href="https://www.surveymonkey.com/r/newsletterjune"> 2020 IoT Developer Survey</a> closes
  June 26 so please take a few minutes to add your voice to the survey if you haven&rsquo;t already
  done so. The more responses we receive, the more insight we gain, and the more value IoT
  developers and other members of the IoT ecosystem will realize from the survey results.
</p>
<p>
  This is the first time the IoT Developer Survey includes questions about edge technologies and
  tools so it&rsquo;s a unique opportunity to influence IoT industry direction at a time when
  it&rsquo;s rapidly evolving. With your responses, the<a href="https://iot.eclipse.org/"> Eclipse
    IoT Working Group</a> and the<a href="https://edgenative.eclipse.org/"> Eclipse Edge Native
    Working Group</a> will have the insight needed to continue aligning their roadmaps with your
  priorities and requirements for cloud-to-edge IoT solution development.
</p>
<p>
  The survey can be completed in less than 10 minutes.<a
    href="https://www.surveymonkey.com/r/newsletterjune"
  > Start now</a>.
</p>
<hr />
<h2>Jakarta EE: Watch for Our Survey Results and the Jakarta EE 9 Milestone Release</h2>
<p>In just a few days, we&rsquo;ll be sharing the results of the 2020 Jakarta EE Developer
  Survey. We received more than 2,100 survey responses from developers around the world so the
  results will go a long way toward helping everyone in the Java ecosystem understand how the cloud
  native world for enterprise Java is unfolding.</p>
<p>
  Also, keep an eye out for our announcement about the Jakarta EE 9 milestone release date. This
  milestone release is an<a
    href="/community/eclipse_newsletter/2020/may/1.php"
  > important and necessary step</a> on the road to further innovation using cloud native
  technologies for Java.
</p>
<p>To celebrate the milestone release, we&rsquo;re hosting a virtual release party on the
  release day. Check our social media for the announcement and be sure to register to hear from
  community superstars and industry leaders in the cloud native Java ecosystem as they discuss the
  release.</p>
<p>
  For the latest updates on Jakarta EE 9 progress, click<a
    href="https://eclipse-ee4j.github.io/jakartaee-platform/jakartaee9/JakartaEE9"
  > here</a>.
</p>
<hr />
<h2>Virtual Events Are Gaining Momentum</h2>
<p>We&rsquo;re pleased to report the Eclipse community is actively engaging in the
  Foundation&rsquo;s virtual events and more events are planned. Be sure to check our list of
  upcoming Eclipse Foundation events below.</p>
<p>First, here&rsquo;s a brief summary of recent successful events:</p>
<ul>
  <li><a href="https://jakartaone.org/2020/cn4j/">JakartaOne Livestream Cloud Native for Java</a><br />
    More than 300 people attended the event live and more than 240 people have watched session
    replays. As the map below shows, we&rsquo;ve had participants from every continent, except
    Antarctica, confirming the great potential for virtual events to keep our global community
    connected.<br>
    <img src="images/4_1.jpg" class="img-responsive"></li>
  <li><a href="https://iot.eclipse.org/eclipse-virtual-iot-2020/">Virtual Eclipse IoT and Edge
      Native Day</a><br /> More than 100 people attended the event live and more than 85 have
    watched session replays.</li>
  <li><a href="https://events.eclipse.org/2020/open-source-ai-workshop/">Open Source AI Workshop</a><br />
    More than 200 people registered and 100 people attended the event live.</li>
</ul>
<h3>Save the Dates</h3>
<p>Below is a list of upcoming Eclipse Foundation events. Check the individual webpages
  for details about paper submission and registration dates:</p>
<ul>
  <li><a href="https://jakartaone.org/brazil2020/">JakartaOne Livestream Brazil</a>, August 29, 2020</li>
  <li><a href="https://www.osdforum.org/">OSD Forum</a>, September 15, 2020</li>
  <li><a href="https://events.eclipse.org/2020/sam-iot/">Eclipse SAM IoT 2020</a>, September 17-18,
    2020</li>
  <li><a href="https://www.eclipsecon.org/2020">EclipseCon 2020</a>, October 19-22, 2020</li>
</ul>
<p>
  For a complete list of community events, visit our<a href="https://events.eclipse.org/"> events
    page</a>.
</p>
<p>
  We encourage all community members to submit their events for promotion through our<a
    href="https://newsroom.eclipse.org/node/add/events"
  > newsroom</a>.
</p>
<hr />
<h2>Learn More About Our Value to European Organizations in the Global Open Source Ecosystem</h2>
<p>
  When we announced the<a
    href="https://blogs.eclipse.org/post/mike-milinkovich/eclipse-foundation-moving-europe"
  > Eclipse Foundation is transitioning to become a European-based organization</a>, we also
  published supporting materials to help people better understand the value the Foundation brings to
  European organizations delivering open source innovation with a global impact:
</p>
<ul>
  <li>Read our new white paper:<a
    href="https://outreach.eclipse.foundation/enabling-digital-transformation-white-paper"
  > Enabling Digital Transformation in Europe Through Global Open Source Collaboration</a></li>
  <li>Learn about our European research partnerships:<a
    href="https://outreach.eclipse.foundation/european-research-case-study"
  > Bringing European Research Innovations to the World Through Open Source Collaboration</a></li>
</ul>
<p>
  To learn more about the transition, visit<a href="/europe/">
    eclipse.org/Europe</a>.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>