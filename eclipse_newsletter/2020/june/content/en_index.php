<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<table class="templateContainer"
  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; border: 0;"
  width="100%" cellspacing="0" cellpadding="0" border="0"
>
  <tbody>
    <tr>
      <td id="templatePreheader"
        style="background: #fafafa none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #fafafa; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;"
        valign="top"
      ><table class="mcnTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnTextBlockOuter">
            <tr>
              <td class="mcnTextBlockInner"
                style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td id="templateHeader"
        style="background: #3d3935 url(&amp;quot;https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png&amp;quot;) no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: url(https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/f161e076-20c8-422d-8ffe-87c9057f42b1.png); background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 0;"
        valign="top"
      ><table class="mcnTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnTextBlockOuter">
            <tr>
              <td class="mcnTextBlockInner"
                style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table
                  style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0"
                  border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td class="mcnTextContent"
                        style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 150%; text-align: left;"
                        valign="top"
                      >
                        <h4 class="null"
                          style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 12px; font-style: normal; font-weight: normal; line-height: 125%; letter-spacing: normal; text-align: left;"
                        >
                          Eclipse Newsletter - 2020.18.06<br> &nbsp;
                        </h4>
                        <h1 class="null"
                          style="text-align: center; display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 26px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal;"
                        >Eclipse IDE Evolution</h1> <br> &nbsp;
                      </td>
                    </tr>
                  </tbody>
                </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td id="templateUpperBody"
        style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;"
        valign="top"
      ><table class="mcnBoxedTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
          <tbody class="mcnBoxedTextBlockOuter">
            <tr>
              <td class="mcnBoxedTextBlockInner"
                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0"
                  border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td
                        style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      >
                        <table class="mcnTextContentContainer"
                          style="min-width: 100% !important; background-color: #3D3935; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          width="100%" cellspacing="0" border="0"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnTextContent"
                                style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                valign="top"
                              >
                                <h2 class="null"
                                  style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                >Editor's Note</h2>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnCaptionBlock"
          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnCaptionBlockOuter">
            <tr>
              <td class="mcnCaptionBlockInner"
                style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <table class="mcnCaptionRightContentOuter"
                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <tbody>
                    <tr>
                      <td class="mcnCaptionRightContentInner"
                        style="padding: 0 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      >
                        <table class="mcnCaptionRightImageContentContainer"
                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          width="176" cellspacing="0" cellpadding="0" border="0" align="left"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnCaptionRightImageContent"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                valign="top" align="center"
                              ><img alt=""
                                src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/a203c31f-0f74-4aad-b64c-4a5e61308c7e.png"
                                style="max-width: 400px; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; vertical-align: bottom;"
                                class="mcnImage" width="176"
                              ></td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="mcnCaptionRightTextContentContainer"
                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          width="500" cellspacing="0" cellpadding="0" border="0" align="right"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnTextContent"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                valign="top"
                              >With the <a href="https://www.eclipse.org/eclipseide/"
                                target="_blank"
                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                              >Eclipse IDE 2020-06 simultaneous release </a>this month, we wanted to
                                focus our newsletter on articles that explore different
                                aspects&nbsp;of the Eclipse desktop IDE evolution and what those
                                advancements mean for developers.<br> &nbsp;<br> As you’ll see in
                                our article summary below, our community authors have approached the
                                topic from two main perspectives — how IDE features and
                                functionality are evolving and how developers are evolving the way
                                they use the IDE.
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnTextBlockOuter">
            <tr>
              <td class="mcnTextBlockInner"
                style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table
                  style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0"
                  border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td class="mcnTextContent"
                        style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        valign="top"
                      >
                        <h3 class="null"
                          style="display: block; margin: 0; padding: 0; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                        >Spotlight Articles</h3>
                        <p
                          style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        >Explore this month’s spotlight articles:</p>
                        <ul>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >The <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2020/june/1.php"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >Eclipse IDE community</a> highlights some of the new and noteworthy
                            features in the Eclipse IDE 2020-06 release.
                          </li>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          ><a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2020/june/3.php"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >Patrick Paulin</a> explains how to create the technical infrastructure
                            required to integrate the Eclipse Rich Client Platform (RCP) with
                            backend microservices.</li>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          ><a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2020/june/2.php"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >Ed Merks</a> introduces the Eclipse JustJ project, which will provide
                            fully functional Java runtime packages for redistribution in the Eclipse
                            IDE 2020-09 release.</li>
                        </ul>
                        <h3 class="null"
                          style="display: block; margin: 0; padding: 0; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 20px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                        >News and Community Updates</h3>
                        <p
                          style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        >
                          Also, be sure to check out our <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2020/june/4.php"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >News</a> section. This month, we provide updates on:
                        </p>
                        <ul>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >The Jakarta EE 9 release and the Jakarta EE survey results.</li>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >Our recent virtual events and upcoming events to mark on your calendar.</li>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          >Our latest white paper and case study that highlight the unique value the
                            Eclipse Foundation brings to European organizations to support their
                            participation in the global open source ecosystem.</li>
                        </ul>
                        <p
                          style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        >
                          Finally, a quick reminder to all IoT developers: The IoT Developer Survey
                          closes June 26, so please take a few minutes to add your voice if you
                          haven’t already done so. The survey can be completed in less than 10
                          minutes. <a href="https://www.surveymonkey.com/r/newsletterjune"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >Start now.</a><br> &nbsp;<br> Happy Reading!<br> Thabang Mashologu
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnBoxedTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
          <tbody class="mcnBoxedTextBlockOuter">
            <tr>
              <td class="mcnBoxedTextBlockInner"
                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                <table
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  class="mcnBoxedTextContentContainer" width="100%" cellspacing="0" cellpadding="0"
                  border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td
                        style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      >
                        <table class="mcnTextContentContainer"
                          style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          width="100%" cellspacing="0" border="0"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnTextContent"
                                style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                valign="top"
                              >
                                <h2 class="null"
                                  style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                >
                                  <strong>New Project Proposals</strong>
                                </h2>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnTextBlockOuter">
            <tr>
              <td class="mcnTextBlockInner"
                style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <table
                  style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  class="mcnTextContentContainer" width="100%" cellspacing="0" cellpadding="0"
                  border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td class="mcnTextContent"
                        style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        valign="top"
                      >
                        <ul>
                          <li
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          ><a href="https://projects.eclipse.org/proposals/asciidoc-language"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >AsciiDoc Language</a>:&nbsp;defines and maintains the AsciiDoc Language
                            Specification and Technology Compatiblity Kit (TCK), its artifacts, and
                            the corresponding language and API documentation.</li>
                        </ul>
                        <p
                          style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                        >
                          Interested in more project activity? <a
                            href="https://www.eclipse.org/projects/project_activity.php"
                            target="_blank"
                            style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                          >Read on!</a>
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
    <tr>
      <td id="templateColumns"
        style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 0; padding-bottom: 0;"
        valign="top"
      >
        <!--[if (gte mso 9)|(IE)]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
                                    <tr>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
        <table class="columnWrapper"
          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="370" cellspacing="0" cellpadding="0" border="0" align="left"
        >
          <tbody>
            <tr>
              <td class="columnContainer"
                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              ><table class="mcnBoxedTextBlock"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                  <tbody class="mcnBoxedTextBlockOuter">
                    <tr>
                      <td class="mcnBoxedTextBlockInner"
                        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      >
                        <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                        <table
                          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          class="mcnBoxedTextContentContainer" width="100%" cellspacing="0"
                          cellpadding="0" border="0" align="left"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >
                                <table class="mcnTextContentContainer"
                                  style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  width="100%" cellspacing="0" border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td class="mcnTextContent"
                                        style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                        valign="top"
                                      >
                                        <h2 class="null"
                                          style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                        >New Project Releases</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="mcnTextBlock"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <tbody class="mcnTextBlockOuter">
                    <tr>
                      <td class="mcnTextBlockInner"
                        style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      >
                        <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
                        <table
                          style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          class="mcnTextContentContainer" width="100%" cellspacing="0"
                          cellpadding="0" border="0" align="left"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnTextContent"
                                style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                valign="top"
                              >
                                <ul>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/ee4j.jaf"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Jakarta Activation&nbsp;2.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/ee4j.mail"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Jakarta Mail 2.0.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/tools.shellwax"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Shellwax 1.0.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/modeling.capra"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Capra&nbsp;0.8.0&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/ecd.dirigible"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Dirigible 5.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/modeling.graphiti"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Graphiti 0.17.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.egit"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Git Team Provider 5.8.0&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.handly"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Handly 1.4</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.jgit"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse JGit 5.8.0&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.m2e"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse m2eclipse 1.16.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.nebula"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Nebula 2.4.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a
                                    href="https://projects.eclipse.org/projects/technology.packaging"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Packaging Project 4.16.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/technology.swtbot"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse SWTBot 3.0.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/tools.titan"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Titan 7.1.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a
                                    href="https://projects.eclipse.org/projects/tools.tracecompass"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Trace Compass 6.0.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/webtools"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Web Tools Platform Project 3.18 (2020-06)</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/iot.cyclonedds"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Cyclone DDS 0.6.0 (Florestan)&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/iot.kapua"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Kapua 1.2.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/iot.thingweb"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Thingweb 0.7.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a
                                    href="https://projects.eclipse.org/projects/technology.passage"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Passage 0.9.0&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/modeling.epsilon"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Epsilon 2.0</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/modeling.hawk"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse Hawk 2.0.0&nbsp;</a></li>
                                  <li
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  ><a href="https://projects.eclipse.org/projects/polarsys.chess"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse CHESS 1.0.0</a></li>
                                </ul>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://www.eclipse.org/projects/tools/reviews.php"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >View all the project releases!</a>
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    <td align="center" valign="top" width="300" style="width:300px;">
                                    <![endif]-->
        <table class="columnWrapper"
          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="370" cellspacing="0" cellpadding="0" border="0" align="left"
        >
          <tbody>
            <tr>
              <td class="columnContainer"
                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              ><table class="mcnBoxedTextBlock"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <!--[if gte mso 9]>
	<table align="center" border="0" cellspacing="0" cellpadding="0" width="100%">
	<![endif]-->
                  <tbody class="mcnBoxedTextBlockOuter">
                    <tr>
                      <td class="mcnBoxedTextBlockInner"
                        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      >
                        <!--[if gte mso 9]>
				<td align="center" valign="top" ">
				<![endif]-->
                        <table
                          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          class="mcnBoxedTextContentContainer" width="100%" cellspacing="0"
                          cellpadding="0" border="0" align="left"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="padding-top: 9px; padding-left: 18px; padding-bottom: 9px; padding-right: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                              >
                                <table class="mcnTextContentContainer"
                                  style="min-width: 100% !important; background-color: #404040; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  width="100%" cellspacing="0" border="0"
                                >
                                  <tbody>
                                    <tr>
                                      <td class="mcnTextContent"
                                        style="padding: 18px; color: #F2F2F2; font-family: Helvetica; font-size: 14px; font-weight: normal; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; line-height: 150%;"
                                        valign="top"
                                      >
                                        <h2 class="null"
                                          style="display: block; margin: 0; padding: 0; color: #ffffff; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 22px; font-style: normal; font-weight: bold; line-height: 125%; letter-spacing: normal; text-align: left;"
                                        >Upcoming Virtual Events</h2>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table> <!--[if gte mso 9]>
				</td>
				<![endif]--> <!--[if gte mso 9]>
                </tr>
                </table>
				<![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table class="mcnTextBlock"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <tbody class="mcnTextBlockOuter">
                    <tr>
                      <td class="mcnTextBlockInner"
                        style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      >
                        <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="300" style="width:300px;">
				<![endif]-->
                        <table
                          style="max-width: 100%; min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          class="mcnTextContentContainer" width="100%" cellspacing="0"
                          cellpadding="0" border="0" align="left"
                        >
                          <tbody>
                            <tr>
                              <td class="mcnTextContent"
                                style="padding-top: 0; padding-right: 18px; padding-bottom: 9px; padding-left: 18px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; word-break: break-word; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                valign="top"
                              >
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://www.scale-up-360.com/en/adas/" target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >OpenADx @ ScaleUp 360° Advanced Driver Assistance Systems Europe</a>
                                  |&nbsp;June 30 -&nbsp;July 1, 2020
                                </p>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://jakartaone.org/brazil2020/" target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >JakartaOne Livestream Brazil</a> | August 29, 2020
                                </p>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://www.osdforum.org/" target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >OSD Forum</a> | September15, 2020
                                </p>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://events.eclipse.org/2020/sam-iot/" target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >Eclipse SAM IoT 2020</a> | September 17-18, 2020
                                </p>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  <a href="https://www.eclipsecon.org/2020" target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >EclipseCon 2020</a> |&nbsp;October&nbsp;19-22,&nbsp;2020<br>
                                  &nbsp;
                                </p>
                                <p
                                  style="margin: 10px 0; padding: 0; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #3d3935; font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;"
                                >
                                  Do you know about an event relevant to the Eclipse community?
                                  Submit your event to <a href="https://newsroom.eclipse.org/"
                                    target="_blank"
                                    style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; color: #007C89; font-weight: normal; text-decoration: underline;"
                                  >newsroom.eclipse.org</a>.
                                </p>
                              </td>
                            </tr>
                          </tbody>
                        </table> <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                      </td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
          </tbody>
        </table> <!--[if (gte mso 9)|(IE)]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
      </td>
    </tr>
    <tr>
      <td id="templateLowerBody"
        style="background: #ffffff none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #ffffff; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 2px solid #EAEAEA; padding-top: 0; padding-bottom: 9px;"
        valign="top"
      ></td>
    </tr>
    <tr>
      <td id="templateFooter"
        style="background: #3d3935 none no-repeat center/cover; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background-color: #3d3935; background-image: none; background-repeat: no-repeat; background-position: center; background-size: cover; border-top: 0; border-bottom: 0; padding-top: 9px; padding-bottom: 9px;"
        valign="top"
      ><table class="mcnImageBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnImageBlockOuter">
            <tr>
              <td
                style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                class="mcnImageBlockInner" valign="top"
              >
                <table class="mcnImageContentContainer"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0" align="left"
                >
                  <tbody>
                    <tr>
                      <td class="mcnImageContent"
                        style="padding-right: 9px; padding-left: 9px; padding-top: 0; padding-bottom: 0; text-align: center; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        valign="top"
                      ><img alt=""
                        src="https://mcusercontent.com/eaf9e1f06f194eadc66788a85/images/54993d58-f464-469b-b433-24729f44b06f.png"
                        style="max-width: 1108px; padding-bottom: 0px; vertical-align: bottom; display: inline !important; border-radius: 0%; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                        class="mcnImage" width="186.12" align="middle"
                      ></td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnFollowBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnFollowBlockOuter">
            <tr>
              <td
                style="padding: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                class="mcnFollowBlockInner" valign="top" align="center"
              >
                <table class="mcnFollowContentContainer"
                  style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="padding-left: 9px; padding-right: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                        align="center"
                      >
                        <table
                          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                          class="mcnFollowContent" width="100%" cellspacing="0" cellpadding="0"
                          border="0"
                        >
                          <tbody>
                            <tr>
                              <td
                                style="padding-top: 9px; padding-right: 9px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                valign="top" align="center"
                              >
                                <table
                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                  cellspacing="0" cellpadding="0" border="0" align="center"
                                >
                                  <tbody>
                                    <tr>
                                      <td
                                        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                        valign="top" align="center"
                                      >
                                        <!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a href="https://www.eclipse.org/"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-link-48.png"
                                                                  alt="Website"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a
                                                                href="mailto:newsletter@eclipse.org"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-forwardtofriend-48.png"
                                                                  alt="Email"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a
                                                                href="https://twitter.com/EclipseFdn"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-twitter-48.png"
                                                                  alt="Twitter"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a
                                                                href="https://www.facebook.com/eclipse.org"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-facebook-48.png"
                                                                  alt="Facebook"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 10px; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a
                                                                href="https://www.youtube.com/user/EclipseFdn"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-youtube-48.png"
                                                                  alt="YouTube"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                        <table
                                          style="display: inline; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                          cellspacing="0" cellpadding="0" border="0" align="left"
                                        >
                                          <tbody>
                                            <tr>
                                              <td
                                                style="padding-right: 0; padding-bottom: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                class="mcnFollowContentItemContainer" valign="top"
                                              >
                                                <table class="mcnFollowContentItem"
                                                  style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                  width="100%" cellspacing="0" cellpadding="0"
                                                  border="0"
                                                >
                                                  <tbody>
                                                    <tr>
                                                      <td
                                                        style="padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                        valign="middle" align="left"
                                                      >
                                                        <table
                                                          style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                          width="" cellspacing="0" cellpadding="0"
                                                          border="0" align="left"
                                                        >
                                                          <tbody>
                                                            <tr>
                                                              <td class="mcnFollowIconContent"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                                width="24" valign="middle"
                                                                align="center"
                                                              ><a
                                                                href="https://www.linkedin.com/company/eclipse-foundation"
                                                                target="_blank"
                                                                style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                                                              ><img
                                                                  src="https://cdn-images.mailchimp.com/icons/social-block-v2/outline-light-linkedin-48.png"
                                                                  alt="LinkedIn"
                                                                  style="display: block; border: 0; height: auto; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;"
                                                                  class="" width="24" height="24"
                                                                ></a></td>
                                                            </tr>
                                                          </tbody>
                                                        </table>
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                              </td>
                                            </tr>
                                          </tbody>
                                        </table> <!--[if mso]>
                                        </td>
                                        <![endif]--> <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnDividerBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; table-layout: fixed !important;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnDividerBlockOuter">
            <tr>
              <td class="mcnDividerBlockInner"
                style="min-width: 100%; padding: 10px 18px 25px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
              >
                <table class="mcnDividerContent"
                  style="min-width: 100%; border-top: 2px solid #EEEEEE; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                  width="100%" cellspacing="0" cellpadding="0" border="0"
                >
                  <tbody>
                    <tr>
                      <td
                        style="mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                      ><span></span></td>
                    </tr>
                  </tbody>
                </table> <!--
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
              </td>
            </tr>
          </tbody>
        </table>
        <table class="mcnTextBlock"
          style="min-width: 100%; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
          width="100%" cellspacing="0" cellpadding="0" border="0"
        >
          <tbody class="mcnTextBlockOuter">
            <tr>
              <td class="mcnTextBlockInner"
                style="padding-top: 9px; mso-line-height-rule: exactly; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"
                valign="top"
              >
                <!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]--> <!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                <!--[if mso]>
				</td>
				<![endif]--> <!--[if mso]>
				</tr>
				</table>
				<![endif]-->
              </td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
