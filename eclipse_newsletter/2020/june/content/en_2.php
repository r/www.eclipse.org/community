<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  The Eclipse IDE 2020-09 release is just a few months away and for the first time, the Eclipse
  Installer will give end users the option to install the required Java&trade; Runtime Environment
  (JRE) as part of the installation. The new feature is enabled by the<a
    href="/justj/"
  > Eclipse JustJ project</a>.
</p>
<p>The JustJ project provides fully functional Java runtime packages that can be
  redistributed by Eclipse Foundation projects. The technology has benefits for end users, package
  creators, and enterprises that redistribute technologies hosted by the Eclipse Foundation. Project
  Lead, Ed Merks, explains.</p>
<p>
  <strong>Q. Why is the JustJ project needed?</strong>
</p>
<p>
  <strong>A.</strong> Much of the software developed at the Eclipse Foundation, including the<a
    href="/eclipseide/"
  > Eclipse IDE</a>, is written in Java and relies on end users having the right JRE version
  installed. For Java developers, this isn&rsquo;t an issue. They&rsquo;re already very familiar
  with Java Development Kits (JDKs), where to download them, and they do this all the time. But,
  developers who work in other programming languages don&rsquo;t want to have to think about Java
  versions or where to get them.
</p>
<p>With JustJ, non-Java developers never have to think about Java runtime versions. The
  Eclipse Installer will include the required JRE and developers can simply install a JRE with the
  software they are installing, if they want to. That is, they can choose whether to use a JRE
  provided by JustJ instead of one that&rsquo;s already installed on their computer. Similarly, some
  of the pre-packaged Eclipse IDEs will include a JRE so the developer can simply unpack it and run
  it with no concern about the Java runtime versions on their computer. It&rsquo;s one-stop shopping
  with complete flexibility.&nbsp;</p>
<p>In the case of the Eclipse IDE, including the JRE in the installer and in the
  pre-packaged download could also help to attract developers who are currently using other IDEs
  that include Java.</p>
<p>
  <strong>Q. Which projects can the JustJ technology be used with?</strong>
</p>
<p>
  <strong>A.</strong> Our first step is to include the JustJ technology with the Eclipse Installer
  and the individual pre-packaged Eclipse IDEs, but it can be used with any Eclipse project
  that&rsquo;s based on Java. With input from project teams, we can package JREs in forms that are
  easily consumable for their project.
</p>
<p>
  <strong>Q. Why is this capability being delivered now?</strong>
</p>
<p>
  <strong>A.</strong> Until about a year ago, the Eclipse Foundation didn&rsquo;t have the rights
  needed to redistribute Java runtimes. But, it&rsquo;s good timing because the 2020-09 release will
  require end users to have the Java 11 runtime environment installed instead of Java 8 so many
  developers will need to upgrade their JRE.
</p>
<p>
  <strong>Q. What does JustJ mean for people who build products and create packages at the Eclipse
    Foundation?</strong>
</p>
<p>
  <strong>A. </strong>The JustJ project repackages the JRE as a p2 installable unit that is
  available through<a href="https://download.eclipse.org/justj/www/?page=download#p2-update-sites">
    p2 repositories</a> and<a
    href="https://download.eclipse.org/justj/www/?page=download#jre-downloads"
  > packaged downloads</a> so it&rsquo;s easy to include in builds. People can simply indicate their
  software requires a specific JRE and the product is built and bundled with the JRE embedded. They
  can also test with that JRE version to verify the software runs well with it.
</p>
<p>
  <strong>Q. What about external companies that redistribute Eclipse Foundation technologies?</strong>
</p>
<p>
  <strong>A.</strong> Many of these companies are creating this same kind of infrastructure for
  embedding JRE p2 installable units into their products. With JustJ, they no longer need to do that
</p>
<p>
  <strong>Q. Where can people find information about building products that include JREs?</strong>
</p>
<p>
  <strong>A.</strong> The<a href="/justj/?page=index"> JustJ website</a>
  provides complete documentation, including tutorials, examples, videos, and online references to
  help people understand the end-user experience and include JREs in their installation packages.
  The site also provides links to JRE distributions and update sites.
</p>
<p>
  <strong>Q. How does adding the JustJ technology affect package size?</strong>
</p>
<p>
  <strong>A.</strong> The full JRE is about 70 MB while most of the Eclipse IDE packages are between
  150 and 300 MB. We&rsquo;ve been stripping out debug information and reducing the number of
  modules that are available to reduce the size. These efforts have been a big focal point in the
  project and<a href="/justj/?page=documentation"> the website explains</a>
  the reasoning behind the different ways we&rsquo;re packaging JREs to reduce size.
</p>
<p>
  <strong>Q. How is the JustJ project handling Java releases? Which JRE will be available with the
    Eclipse IDE 2020-09 release? </strong>
</p>
<p>
  <strong>A.</strong> JustJ will always provide the latest available Java JRE release, but
  it&rsquo;s generally backwards-compatible so it will also provide the minimum JRE version required
  by the software.
</p>
<p>For the 2020-09 IDE release, JustJ will provide JRE 14, which will be the latest
  release at the time, and potentially also JRE 11. JRE 11 is a long-term support Java release and
  the minimum requirement for this IDE release. We know that for production purposes, many companies
  and developers use long-term support versions of Java.</p>
<p>
  <strong>Q. How can the community get involved in JustJ?</strong>
</p>
<p>
  <strong>A.</strong> Right now, we need input on requirements for building packages, how many
  different versions of JREs will be needed, and whether people want stripped-down or minimized JRE
  versions.
</p>
<p>
  The best way to provide this input is through the<a
    href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Justj"
  > JustJ Bugzilla</a> link on our<a href="https://www.eclipse.org/justj/?page=support"> Support
    page</a>. This page also includes a link to the<a
    href="/forums/index.php/f/532/"
  > JustJ Forum</a> if people have general questions.
</p>
<p>
  We also have a<a href="https://accounts.eclipse.org/mailing-list/justj-dev"> mailing list</a> the
  community can subscribe to.
</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/ed.jpg" alt="<?php print $pageAuthor; ?>"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>