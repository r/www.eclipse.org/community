<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>With the Eclipse IDE 2020-06 (4.16) simultaneous release just out, it&rsquo;s the ideal
  time to highlight some of the new and noteworthy features in the release.</p>
<p>The June release includes 73 participating projects and delivers a number of
  performance optimizations that accelerate startup and IDE interactions to help you work more
  efficiently.</p>
<h2>Integrated Java&trade; 14 Support</h2>
<p>With more frequent Java releases, it&rsquo;s an exciting time to be a Java developer.
  Java 14 is out and the 2020-06 Eclipse IDE release provides integrated support.</p>
<p>The Eclipse Compiler for Java implements all of the new language enhancements, existing
  functionality has been updated to blend with the new language features, and new functionality has
  been added to help developers working with these language constructs.</p>
<p>For a sneak peek into how the Eclipse IDE supports some of the major Java 14 features,
  click <a href="/community/eclipse_newsletter/2020/june/java-14-features.pdf">here</a>.</p>
<h2>Improved User Experience</h2>
<p>The 2020-06 IDE release includes a number of improvements that give developers more
  options to customize the look and feel of the IDE to fit their preferences and requirements.</p>
<p>To make theme selection easier and more consistent, outdated themes have been retired
  and the theme selection Preferences page has been simplified, as shown in Figure 1.</p>
<p>
  <strong>Figure 1: Simplified Theme Selection</strong>
</p>
<p>
  <img src="images/1_1.png" alt="Figure 1: Simplified Theme Selection" />
</p>
<p>In addition:</p>
<ul>
  <li>In the dark theme, multiple issues were addressed. For example, the toolbar is now
    consistently styled across platforms.</li>
  <li>In the light theme, colors have been updated and aligned with the Windows 10 styling.</li>
  <li>Both themes now default to square tabs, which gives the IDE a fresher look and feel. You can
    revert to rounded tabs through the Windows-&gt; Preferences -&gt; General -&gt; Appearance page.</li>
</ul>
<p>The IDE for the Windows platform includes a number of Standard Widget Toolkit (SWT)
  updates that improve the way light and dark themes are configured and displayed. There&rsquo;s no
  longer a need for workarounds to support dark theme functionality, which makes the IDE faster and
  more consistent. There are now options to style the top-level menu, scrollbars, and tree arrows.
  And the blurry text issue in older versions of the IDE has been resolved.</p>
<p>Future improvements for both themes are already scheduled for the Eclipse 2020-09
  release, so the user experience will continue to improve.</p>
<p>In addition to the theme improvements, the 2020-06 IDE release includes a number of
  overall platform and SWT improvements:</p>
<ul>
  <li><strong>Font ligatures are supported on all platforms</strong><br /> The Eclipse text editor
    and SWT StyledText widget now support font ligatures on all platforms so you can choose your
    preferred coding font no matter which platform you&rsquo;re using (Figure 2).</li>
</ul>
<p>
  <strong>Figure 2: Font Ligature Support</strong>
</p>
<p>
  <img
    src="images/1_2.png" alt="Figure 2: Font Ligature Support"
  />
</p>
<ul>
  <li><strong>Colors don&rsquo;t need to be disposed of</strong><br /> SWT colors created in code no longer need to
    be disposed of as no operating system resources are allocated to them. To ensure backwards
    compatibility, disposing of a color is not an error.
  </li>
  <li><strong>New File wizard creates missing folders</strong><br /> You can now create missing folders using the New
    File wizard without creating folders beforehand
  </li>
  <li><strong>Show key binding can be enabled by type</strong><br /> It&rsquo;s now possible to enable key binding
    separately for keyboard interactions and mouse clicks. Enabling Show key binding for mouse
    clicks helps to learn existing key bindings.
  </li>
</ul>
<h2>More Intelligent, Automated API Tooling Features</h2>
<p>As in previous releases, the latest IDE release includes features that help you find,
  fix, and get more information about API tooling issues.</p>
<p>Starting in the 2020-06 release, information about why you need to make a major or
  minor version change is provided as a tool tip on the version change error marker. For minor
  version changes, all compatibility changes are listed and for major version changes, all breaking
  changes are listed. Figure 3 shows a typical tool tip for a minor version error.</p>
<p>
  <strong>Figure 3: Tooltip for a Minor Version Error</strong>
</p>
<p>
  <img
    src="images/1_3.png" alt="Figure 3: Tooltip for a Minor Version Error"
  />
</p>
<p>Quick fix support has also been improved.</p>
<p>In many cases, developers need to work in multiple branches to support various Java
  releases. When these branches are merged to the main branch, issues such as missing or invalid
  <code>@since</code> tags typically arise. The branches also have different baselines. The Eclipse IDE 2020-06
  release resolves these issues.</p>
<p>You can now select all related <code>@since</code> tag errors and warnings and apply quick fix to
  all of them at once. Quick fix applies the appropriate versions even when errors occur across
  files and plugins.</p>
<p>We&rsquo;ve also made improvements to the way minor and micro version changes are
  handled.</p>
<p>Previously, minor and micro versions were unnecessarily increased in some development
  scenarios, even if a major or minor version was increased. Now, these scenarios are automatically
  detected. The software now also reports the following unnecessary minor and micro version changes:</p>
<ul>
  <li>Minor version increased when major version was already increased in the release.</li>
  <li>Micro version increased when minor version or major version was already increased in the
    release.</li>
</ul>
<p>
  To reflect these improvements, the preference option <strong>Report minor version change without
    API changes</strong> has been renamed to <strong>Report unnecessary minor or micro version
    change</strong>.
</p>
<h2>Faster, Easier Debugging</h2>
<p>The 2020-06 IDE release also includes numerous enhancements that make debugging
  programs faster and easier, including:</p>
<ul>
  <li><strong>Functional debug expressions</strong><br /> Lambda expressions and method references
    are now supported in debug expressions, such as in the Expressions view and in breakpoint
    condition expressions (Figure 4).</li>
</ul>
<p>
  <strong>Figure 4: Conditional Line Breakpoint Example</strong>
</p>
<p>
  <img
    src="images/1_4.png" alt="Figure 4: Conditional Line Breakpoint Example"
  />
</p>
<p>&nbsp;</p>
<ul>
  <li><strong>Support for Console control characters, form feed, and vertical bars</strong><br />
    The interpretation of ASCII control characters in the Console view was extended to recognize the
    characters: \f for form feed and \v for vertical tab in languages that support these functions.<br />
    The Console view can also now interpret the control characters backslash (\b) and
    carriage return (\r) when the preference is enabled on the in the Run/Debug &gt; Console
    preferences.</li>
  <li><strong>Termination time in Console view</strong><br /> The Console view label now shows the termination time
    for a process in addition to the launch time.
  </li>
  <li><strong>Collapse All button in Debug view</strong><br /> A new Collapse All button collapses all launched debug
    activities.
  </li>
</ul>
<h2>Get More Information and Get Started</h2>
<p>
  We&rsquo;ve summarized just some of the Eclipse IDE 2020-06 release highlights here. For the
  complete list of new and noteworthy features in this release, click<a
    href="/eclipse/news/4.16/"
  > here</a>.
</p>
<p>
  To download the 2020-06 IDE release and start taking advantage of the improvements, click<a
    href="/downloads/"
  > here</a>.
</p>
<p>*This article was written with contributions from Vikas Chandra, Noopur Gupta, Lakshmi
  P Shanmugam, and Sarika Sinha.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <p><img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/vikas.png" alt="Vikas Chandra"
          /></p>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Vikas Chandra</p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8">
          <p><img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/noopur.png" alt="Noopur Gupta"
          /></p>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Noopur Gupta</p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8">
          <p><img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/lakshmi.png" alt="Lakshmi P Shanmugam"
          /></p>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Lakshmi P Shanmugam</p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8">
          <p><img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/sarika.png" alt="Sarika Sinha"
          /></p>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Sarika Sinha</p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8">
          <p><img class="img-responsive"
            src="/community/eclipse_newsletter/2020/june/images/lars.png" alt="Lars Vogel"
          /></p>
        </div>
        <div class="col-sm-16">
          <p class="author-name">Lars Vogel</p>
        </div>
      </div>

    </div>
  </div>
</div>