<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Bringing the Power of Spreadsheets to Stream Processing with Eclipse Streamsheets 2.0";
  $pageKeywords = "eclipse, newsletter, Spreadsheets, Stream Processing, Eclipse Streamsheets, 2.0";
  $pageAuthor = "Philipp Struss and Philip Ackermann";
  $pageDescription = "The Eclipse Streamsheets community is excited to share details about the milestone release of Eclipse Streamsheets 2.0, an Eclipse IoT project.";

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");