<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Our Biggest Event of the Year Is Just Around the Corner</h2>
<p>The EclipseCon virtual conference is coming up October 19-22. If you haven&rsquo;t already registered, take a minute to <a href="https://www.eclipsecon.org/2020/registration">do that now</a>.</p>
<p>This year&rsquo;s event features four great <a href="https://www.eclipsecon.org/2020/keynotes">keynote speakers</a>, including Eclipse Foundation Executive Director and chief cheerleader, Mike Milinkovich.</p>
<p>To plan your EclipseCon experience day-by-day, check out the complete schedule, <a href="https://www.eclipsecon.org/2020/schedule/2020-10-20">here</a>.</p>
<hr />

<h2>Watch for IoT Developer Survey Results in the October Newsletter</h2>
<p>We&rsquo;ll be sharing the final report on the 2020 IoT Developer Survey results in the October community newsletter, so be sure to check back for details.</p>
<p>The survey results provide essential insight that will give all of us a better understanding of the IoT industry landscape, the challenges IoT developers are facing, and the opportunities for enterprise IoT stakeholders in the IoT open source ecosystem.</p>
<p>For the first time, the survey results will also provide insight into how respondents are using edge computing, which will influence the <a href="https://edgenative.eclipse.org/">Eclipse Edge Native Working Group</a> roadmap.</p>
<hr />

<h2>Eclipse Streamsheets Helps Give Freiburg University Hospital Critical New Visibility</h2>
<p><a href="https://iot.eclipse.org/">Eclipse IoT</a> member, <a href="https://www.eclipse.org/membership/showMember.php?member_id=1376">Cedalo AG</a>, combined the <a href="https://projects.eclipse.org/projects/iot.streamsheets">Eclipse Streamsheets</a> stream processing software with Apache Kafka to help Freiburg University Hospital link and process events such as patient admissions, lab test results, bed transfers, and other activities in real time.</p>
<p>The solution has been particularly helpful during the COVID-19 pandemic. With the ability to link and evaluate data from a wide variety of sources in real time, hospital staff have an overview of suspected cases, sick and non-infected patients, and current bed capacities so they can immediately recognize and respond to changes in the situation.</p>
<p><a href="https://cedalo.com/newsposts/2020/08/10/Freiburg-University-Hospital.html">Learn more</a>.</p>
<hr />

<h2>Meet Darcy — the Ground-Breaking Edge AI Thermal Camera Developed by Edgeworx</h2>
<p><a href="https://www.eclipse.org/membership/showMember.php?member_id=1334">Edgeworx</a>, a founding member of the <a href="https://edgenative.eclipse.org/">Eclipse Edge Native Working Group</a>, developed Darcy to help businesses that are reopening during the pandemic detect fevers, face masks, and symptoms when people enter the business.</p>
<p>Darcy provides high-accuracy, no-touch temperature readings, seamless symptom capture, and powerful AI that analyzes the environment to check for masks and more. Alerts and daily, weekly, and monthly reports help businesses spot locations where symptoms are trending, anticipate problem areas, and track compliance.</p>
<p>Darcy includes the ioFog Fabric, which is based on <a href="https://iofog.org/">Eclipse ioFog</a>, to ensure sensitive data stays on Darcy, not on the web.</p>
<p><a href="https://www.meetdarcy.com/">Learn more</a>.</p>
<hr />

<h2>Mike Milinkovich Talks You Won’t Want to Miss</h2>
<p>In addition to his keynote address at EclipseCon, Mike will be sharing his insights in:</p>
<ul>
	<li>A keynote address at the <strong>2020 Future Developer Summit</strong>, an interactive virtual event focused on developer marketing and developer relations. This event features a community edition that will take place September 29-30 and an exclusive edition on October 6-7. For registration details and the complete schedule, click <a href="https://www.futuredeveloper.io/">here</a>.</li>
	<li>A talk on open source software at the <strong>Open Source Business Forum (O4B)</strong>, which will be held October 7-8. O4B is the first business-oriented European conference to discuss opportunities and tackle the challenges of commercial open source software in the European ecosystem. The Eclipse Foundation is a Gold Sponsor of this event. Learn more, <a href="https://www.o4b.org/">here</a>.</li>
</ul>
<hr />

<h2>Capella Days Is October 12-15</h2>
<p><a href="https://www.eclipse.org/capella/capella_days_2020.html">Capella Days</a>, which is organized by Obeo, in partnership with Thales, Altran, and TNO-ESI, brings together the <a href="https://www.eclipse.org/capella/">Eclipse Capella</a> and Arcadia communities, including:</p>
<ul>
	<li>Creators of this innovative systems engineering solution</li>
	<li>Providers of Eclipse Capella add-ons and services</li>
	<li>Model-based systems engineering (MBSE) experts and industrial users</li>
</ul>
<p>Eclipse Capella is an open source tool created by Thales in 2007, and it has been continuously evolving ever since.</p>
<p>Capella Days attendees will gain insight into:</p>
<ul>
	<li>The Eclipse Capella methodology</li>
	<li>MBSE trends and the latest Eclipse Capella features</li>
	<li>The experiences of industrial adopters who have successfully deployed an MBSE approach with Eclipse Capella and Arcadia</li>
</ul>
<p>For complete Capella Days event details, click <a href="https://www.eclipse.org/capella/capella_days_2020.html">here</a>.</p>
<p>To register for Capella Days, click <a href="https://www.crowdcast.io/e/capella-days-2020/register">here</a>.</p>
<p>For more information into learning opportunities, check our list of <a href="https://events.eclipse.org/">Upcoming Virtual Events</a>.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
