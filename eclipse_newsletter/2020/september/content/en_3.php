<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>With the growing use of IoT in industrial environments, it&rsquo;s important for developers to ensure they take the right approach to developing, scaling, and managing new digital factory applications.</p>
<p>A recent PAC RADAR report by European research and consulting firm, teknowlogy Group, provides insight into how developers can ensure DevOps are efficient, simple, and agile. The report is entitled Open Digital Platforms for the Industrial World in Europe 2020, and it notes that the Eclipse IoT Working Group is the leading open source community around IoT. Two of the three companies &mdash; Bosch.IO and Eurotech &mdash; the report rates as Best in Class providers of IoT platforms based on open source are members of the <a href="https://iot.eclipse.org/">Eclipse IoT Working Group</a> &nbsp;(Figure 1).</p>
<p><span style="font-size:12px">Figure 1: Open Digital Platforms for Enterprise IoT in Europe 2020</span></p>
<p><img class="img-responsive" src="images/3-1.png"></p>
<p><span style="font-size:12px">Source: PAC RADAR Report: Open Digital Platforms for the Industrial World, July 2020. &copy; PAC GmbH 2020.</span></p>

<h2>IoT Platforms Based on Open Source Software Are Becoming a Real Alternative for Many Users</h2>
<p>The open digital platforms for enterprise IoT evaluated in the report are open in two respects. They are:</p>
<ul>
	<li>Open to all types of IoT devices</li>
	<li>Based on open source software</li>
</ul>
<p>The report states: &ldquo;Besides an attractive business model, broad service and support capabilities, and strong client references at enterprise level (as proof points for a robust and highly scalable solution), we consider a constant stream of platform enhancements as highly relevant. The faster this stream, the better for users. This includes bug fixing on existing code, but also the development of totally new components to enhance the current functionality. This basically means for users that large and highly dedicated developer communities can provide the best value in the long run. This underlines the relevance of agile open source communities.&rdquo;</p>
<p>The report also highlights the following examples of commercial products that are based on Eclipse IoT projects:</p>
<ul>
	<li>Eurotech Everyware Software Framework is based on <a href="https://www.eclipse.org/kura/">Eclipse Kura</a></li>
	<li>Eurotech Everyware Cloud is based on <a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a></li>
	<li>Bosch IoT Hub is based on <a href="https://www.eclipse.org/hono/">Eclipse Hono</a></li>
	<li>Bosch IoT Rollouts is based on <a href="https://www.eclipse.org/hawkbit/">Eclipse hawkBit</a></li>
	<li>Bosch IoT Things is based on <a href="https://www.eclipse.org/ditto/">Eclipse Ditto</a></li>
</ul>
<p>These examples confirm the open source software projects hosted at Eclipse IoT provide strong foundations for commercial offerings and are important to the development strategies of leading IoT solution vendors and their customers.</p>

<h2>Digital Platforms Extend Agile DevOps Across All Applications</h2>
<p>To extend the benefits of agile development, containers, and microservices across all digital factory initiatives, the report recommends development of digital platforms that orchestrate applications and data in a holistic way across:</p>
<ul>
	<li>Connected devices, such as embedded systems and programmable logic controllers</li>
	<li>Industrial edge computing devices, such as industrial PCs, gateways, and servers</li>
	<li>The cloud</li>
</ul>
<p>The digital platform manages all factory applications as an integrated and agile digital system. Figure 2 illustrates the concept.</p>
<p><span style="font-size:12px">Figure 2: Digital Platforms Orchestrate Industrial Applications Across Devices, Edge, and Cloud</span></p>
<p><img class="img-responsive" src="images/3-2.png"></p>
<p><span style="font-size:12px">Source: PAC RADAR Report: Open Digital Platforms for the Industrial World, July 2020. &copy; PAC GmbH 2020.</span></p>

<h2>An Open Approach to Digital Platforms Is Essential</h2>
<p>The PAC RADAR report clearly recognizes the value of an open approach to digital platforms for the industrial world to ensure that new and innovative applications can be quickly and efficiently made available to platform users. For example, the report recommends that digital platforms integrate an open app store that gives end users access to new, internally developed applications and to external applications from the platform provider or other third-party developers.</p>
<p>&ldquo;Only a big ecosystem and openness to all third-party application developers can provide the maximum agility required in the market today,&rdquo; says Arnold Vogt, Head of Digital CX and IoT at the teknowledgy Group. &ldquo;In a high-speed world, no individual application creates a lasting competitive advantage &mdash; It is the ability to move faster on a large scale that makes the difference. Today, open digital platforms are the best approach to achieving this for the digital factory in an efficient way.&rdquo;</p>

<h2>Positive Recognition for Eurotech&rsquo;s Commitment to Open Source and the Eclipse Foundation</h2>
<p>Eurotech is a leading provider of rugged and industrial embedded boards and systems, edge computing platforms, and IoT enablement solutions. The company&rsquo;s Best in Class positioning in the report stems from:</p>
<ul>
	<li>The company&rsquo;s commitment to being an active participant in an IoT partner ecosystem</li>
	<li>The company&#39;s expertise in numerous vertical markets</li>
	<li>The quality of Eurotech&rsquo;s product portfolio, as confirmed by its customers in case studies</li>
</ul>
<p>The report concludes: &ldquo;Eurotech is a uniquely positioned company in the market, able to offer IoT and edge computing building blocks, from very compact High Performance Embedded Computers (HPECs) to edge servers and IoT gateways, in combination with integrated IoT software framework for edge and cloud computing, fully based on open source.&quot;</p>
<p>Eurotech is a committed founding member of the Eclipse IoT Working group and the contributor of open source projects such as:</p>
<ul>
	<li><a href="https://www.eclipse.org/kura/">Eclipse Kura</a>, an IoT edge middleware for IoT gateways, edge servers, and other smart devices&nbsp;</li>
	<li><a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a>, an IoT platform providing device management capabilities and data integration with cloud and IT applications</li>
</ul>
<p>&ldquo;We are strongly committed to enterprise-ready open source software as a solid foundation for successful IoT and digital transformation implementations,&rdquo; says Robert Andres, Eurotech Chief Strategy Officer. &ldquo;It allows us to offer our customers and system integration partners a simple and fast way to develop and deploy IoT solutions, while minimizing risks and avoiding vendor lock-in. This commitment towards open source-based components, together with our excellent track record in the world of operational technologies, ensures solutions geared towards lowest total cost of ownership with a focus on manageability and security of end-to-end IoT applications.&rdquo;</p>

<h2>Get More Information</h2>
<ul>
	<li>For more information about the Eclipse IoT Working Group, click <a href="https://iot.eclipse.org/">here</a>.</li>
	<li>To download the PAC RADAR report from the Eurotech website, click <a href="https://www.eurotech.com/en/page/open-digital-platforms-for-enterprise-iot-in-europe-2020">here</a>.</li>
	<li>To explore the more than 45 open source projects hosted at Eclipse IoT, click <a href="https://iot.eclipse.org/projects/">here</a>.</li>
</ul>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
	<h3>About the Author</h3>
	<div class="row">
		<div class="col-sm-24">
			<div class="row margin-bottom-20">
				<div class="col-sm-8">
					<img class="img img-responsive" alt="<?php print $pageAuthor; ?>"
						src="images/eurotech.png" />
				</div>
				<div class="col-sm-16">
					<p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
            This article was developed by the team at <a href="https://www.eurotech.com/en">Eurotech</a>, a founding member of the Eclipse IoT Working Group. Eurotech integrates hardware and software components under the brand name <a href="https://www.eurotech.com/en/products/iot">Everyware IoT</a> to enable end-to-end IoT solutions that are secure, fully managed, and based on open standards to avoid vendor lock-in.
          </p>
        </div>
			</div>
		</div>
	</div>
</div>