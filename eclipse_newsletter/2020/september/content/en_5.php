<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>At a Glance: Kai Hudalla</h2>
<img class="float-left margin-right-40" src="images/kai-hudalla.jpg" alt="Kai Hudalla">
<ul>
  <li>Involved in open source since: 1999</li>
  <li>Works for: Bosch.IO GmbH on the IoT Hub product team</li>
  <li>Eclipse Foundation contributor since: 2014</li>
  <li>Involved in: <a href="https://projects.eclipse.org/user/4185">8 Eclipse Foundation projects, Eclipse IoT Project Management Committee (PMC), Eclipse Foundation Architecture Council</a></li>
  <li>Committer to: Eclipse Hono, Eclipse Californium, Eclipse IoT Packages, Eclipse Leshan</li>
  <li>Committer since: 2015</li>
  <li>Fun fact: Kai’s favorite thing about Eclipse Foundation events is the opportunity to meet interesting people and socialize with his friends in the community.</li>
</ul>
<hr />
<h2>Why did you first get involved in open source software and OSS communities?</h2>
<p>I’ve been using open source software since the beginning of my software development career when I graduated from university 21 years ago and joined the Bosch Group. We depended heavily on open source libraries and frameworks and such in our day-to-day work. Then, about six years ago, Bosch.IO, the subsidiary I work in, engaged with the Eclipse Foundation and started contributing to open source projects.
</p>
<h2>How did that involvement lead to you becoming a committer?</h2>
<p>
In the first Eclipse Foundation project I engaged in, I was contributing more and more code and taking part in architectural discussions. The other committers on the project asked if I wanted to become a committer and that was something I wanted to do. It was a new project so they were actively seeking people who had a profound interest in the technology and would invest more time in the project over the long term to help determine its future.</p>
<p>Also, if you start a project, you’re automatically a committer so that’s the case with the Eclipse Foundation projects Bosch initiated and I’m involved in.</p>
<h2>How would you summarize your experiences as a committer?</h2>
<p>As a committer, you have great freedom but also great responsibilities so it’s both rewarding and challenging. You have to carefully weigh corporate interests versus the interests others have in the project, and the best interests of the project itself. Sometimes it’s difficult to figure out, and you have to say no to some requests, but the need to wear different hats is also an interesting aspect of the role.</p>
<p>You end up getting in touch with these incredibly smart people from other companies and you immediately see that you can learn so much from them and how they do things. Then you also get to know them personally. I am now friends with many of the people I’ve met through the Eclipse Foundation and I’ve even visited them, so it’s really cool.</p>
<h2>What are your next steps and goals as a committer and Eclipse Foundation community member?</h2>
<p>
I’m not really sure what my future will bring and I don’t have a concrete plan. I’m involved in strategic discussions in the IoT Working Group and the Edge Native Working Group, which are very interesting, but also very time-consuming. And coding is more rewarding because you get immediate feedback. So, I’ll continue trying to balance the theoretical and conceptual aspects of the work with coding.
</p>
<h2>What would you say to developers who are considering getting more involved in open source software projects at the Eclipse Foundation?</h2>
<p>I would share my personal experiences and encourage them to get involved. There’s nothing you can do wrong. You start by reporting bugs, fixing bugs, contributing small things. You ask questions, you engage in the community, then over time you figure out whether it’s interesting for you. We’re all very happy to have new contributors because it’s necessary to grow the community and make the technology more relevant. So, there’s nothing to fear.</p>
<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
