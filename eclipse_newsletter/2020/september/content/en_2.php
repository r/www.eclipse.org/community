<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The <a href="https://projects.eclipse.org/projects/iot.streamsheets">Eclipse Streamsheets</a> community is excited to share details about the milestone release of Eclipse Streamsheets 2.0, an <a href="https://iot.eclipse.org/projects/">Eclipse IoT</a> project.</p>

<p>Eclipse Streamsheets empowers business and technical users to leverage their spreadsheet know-how to build full-fledged services and individual applications (Figure 1). Instead of writing lines of code, users fill in cells with formulas and drag-and-drop charts to process, visualize, connect, and control data in Industrial IoT (IIoT), the world of event streams, and beyond.</p>

<p><span style="font-size:12px">Figure 1: Example Eclipse Streamsheets Application</span></p>
<p><img class="img-responsive" src="images/2-1.png"></p>

<p>With the power and flexibility that Eclipse Streamsheets offers, the application potential is diverse and solutions are highly individual. Typical use cases include:</p>

<ul>
	<li><strong>Continuous processing</strong> and <strong>analysis</strong> of data streams from machines, sensors, or transactions.</li>
	<li><strong>Dashboards </strong>and <strong>visualizations </strong>of key performance indicators (KPIs), devices, and assets.</li>
	<li><strong>Real-time monitoring</strong> of processes as well as <strong>alerts</strong> and <strong>automation </strong>for workflows.</li>
	<li><strong>Decision support systems.</strong></li>
	<li><strong>Connectivity </strong>among assets and systems, and across infrastructures at the edge, on-premises, and in the cloud.</li>
	<li><strong>Simulations </strong>of data sources and devices.</li>
	<li><strong>Semantic mappings</strong> between protocols, APIs, and data formats.</li>
</ul>

<p>The formulas in Eclipse Streamsheets are calculated based on each incoming stream event or periodically based on a cycle timer. The cycle timer can be set to control how often a sheet receives, processes, and sends data. To ensure accurate operations, Eclipse Streamsheets can run up to 1,000 calculation cycles per second. The resulting Streamsheets apps run as microservices, receiving, transforming, and transmitting data from multiple sources, 24 hours a day, even if you close the browser.</p>

<h2>New Features in Eclipse Streamsheets 2.0</h2>

<p>With every release, we continue to ensure that Eclipse Streamsheets is user friendly, easy to use, and meets diverse needs. Eclipse Streamsheets 2.0 includes new features and improvements that meet these objectives, including:</p>

<ul>
	<li><strong>Stream Wizard</strong>: The new Stream Wizard simplifies stream creation (Figure 2). A stream is any connection or way that data gets into, or out of, a Streamsheet. With the wizard, you no longer need to go to the administration area to create new streams. Instead, you can call up new wizards directly from an App or a Service. This means your workflow is not interrupted and streams can be added and adjusted without leaving the App or Service.</li>
</ul>

<p><span style="font-size:12px">Figure 2: Eclipse Streamsheets Stream Wizard</span></p>
<p><img class="img-responsive" src="images/2-2.png"></p>

<ul>
	<li><strong>Streams dashboard</strong>: The dashboard for streams has been completely redesigned and is no longer in the administration area (Figure 3). Streams are now part of the main dashboard, which means there are no longer separate dashboards for Connectors, Producers, and Consumers. They can now be managed centrally, in a single dashboard, with Apps and Services. The new hierarchical arrangement of Connectors and the associated Consumers and Producers also gives you a better overview of the interrelationships and relationships among the individual stream components.</li>
</ul>

<p><span style="font-size:12px">Figure 3: Eclipse Streamsheets Streams Dashboard</span></p>
<p><img class="img-responsive" src="images/2-3.png"></p>

<ul>
	<li><strong>Renaming</strong>: The term &ldquo;Stream Machine&rdquo; has been renamed to better reflect the diverse uses of Eclipse Streamsheets. For example, you can build complex dashboards and apps, as well as services, with pure backend functionality. As a result, Stream Machines are now called &ldquo;Apps &amp; Services.&rdquo;</li>
	<li><strong>Improved UI</strong>: The user interface, which is based on Material Design, includes numerous improvements that affect dialogs, dashboards, and the general appearance of the application.</li>
	<li><strong>Improved touch experience</strong>: Interacting with Eclipse Streamsheets through touch displays has been improved, resulting in a smoother user experience on tablets, smartphones, and similar devices.</li>
	<li><strong>Chart improvements</strong>: Eclipse Streamsheets 2.0 adds several new features, including a new chart interaction menu and transparency, to charts. These features enhance extensive charting features, such as custom and time-series charts that update with real-time data and several types of charts.</li>
	<li><strong>New functions</strong>: Eclipse Streamsheets 2.0 includes the following new functions:
	<ul>
		<li>AWAIT(): Previously, the sheet calculation continued when asynchronous functions were executed. This function pauses the sheet calculation until all referenced cells and requests are resolved. This function can be combined with asynchronous Streamsheets functions such as HTTP.REQUEST().</li>
		<li>AWAIT.ONE(): This function works like AWAIT(), but only pauses the sheet calculation until at least one of the specified requests is resolved.</li>
		<li>JSON.VALUE(): This function returns a value from a JSON object. The value to return corresponds to the path specified by given keys.</li>
		<li>SLEEP(): To provide even more flexibility for sheet calculations, the new SLEEP() function allows the sheet calculation to be paused for a specified period of time.</li>
		<li>OUTBOX.GETIDS(): This function creates a list of IDs for all messages currently available in the Outbox. The Outbox is a storage area within the App that allows you to store JSON messages without using a database. An optional id filter can be used to specify the IDs that are returned.</li>
		<li>RANGE(): This function makes it even easier to generate JSON objects. It creates a flat JSON array from a specified cell range.</li>
	</ul>
	</li>
	<li><strong>Updated Streamsheets functions</strong>: Functions that have been updated include:
	<ul>
		<li>READ(): When dragging and dropping data elements from the Inbox into the sheet, the ErrOnMissing parameter is now set to TRUE by default.</li>
		<li>WRITE(): This function can now be used to add JSON data to the Outbox with a set time to live (TTL). When the TTL expires, the data is deleted from the Outbox. This function can also be used with the new OUTBOX.GETIDS() function.</li>
	</ul>
	</li>
	<li>&nbsp;<strong>Bug fixes and other improvements</strong>: Streamsheets 2.0 includes several bug fixes as well as stability and performance improvements.</li>
</ul>

<h2>Get More Information</h2>

<p>Eclipse Streamsheets runs on Linux, Microsoft Windows, and Apple macOS as well as systems such as the Raspberry Pi. Installation is easiest when Docker is used. For detailed installation instructions, visit <a href="http://docs.cedalo.com">docs.cedalo.com</a>.</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem margin-bottom-20">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img-responsive" alt="<?php print $pageAuthor; ?>" src="images/pstruss.jpg" />
        </div>
		<div class="col-sm-8">
		  <img class="img-responsive" alt="<?php print $pageAuthor; ?>" src="images/packermann.jpg" />
        </div>

        <div class="col-sm-24 margin-top-20">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
            This article was developed by Philipp Struss, the co-founder and CEO of <a href="https://cedalo.com/">Cedalo AG</a>, and Philip Ackermann, the CTO at Cedalo AG. In addition to Eclipse Streamsheets, Cedalo AG is also the official sponsor of <a href="https://mosquitto.org/">Eclipse Mosquitto</a>, the popular open source MQTT broker. Eclipse Streamsheets and Eclipse Mosquitto are included in the Cedalo Community Platform. The company also offers premium editions of both technologies with additional features.
          </p>
		</div>
      </div>
    </div>
  </div>
</div>