<?php
/**
 * Copyright (c) 2020 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   Martin Lowe (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The Robot Operating System 2 (ROS 2) is experiencing an incredible level of adoption across robotics applications, from autonomous vacuum cleaners to industrial and commercial autonomous vehicles. ROS 2 applications are also expanding to include:</p>
<ul>
	<li>Robot swarms, where a group of resourceful robots coordinates among themselves and potentially a group of resource-constrained robots.</li>
	<li>Edge robotics, where an arbitrary collection of robot swarms can be monitored, managed, and controlled from the edge. In some cases, this reduces the need to include powerful hardware on the robot to enhance it with additional capabilities.</li>
</ul>
<p>However, ROS 2 support for robot swarms and edge robotics has limitations. That&rsquo;s where <a href="http://zenoh.io/">Eclipse zenoh</a> comes into the picture.</p>
<p>Eclipse zenoh is an innovative protocol that allows robots to be easily monitored, managed, and controlled from anywhere on the internet. It supports use cases ranging from cloud to edge robotics so you can decide which data must be pushed, which should be pulled, and which should be stored and eventually queried. And it accomplishes all of this with a uniform and minimal set of abstraction operations while maintaining location transparency.</p>
<p>Before we get into more details about Eclipse zenoh and why it&rsquo;s ideal for edge robotics, it&rsquo;s important to understand how ROS 2 handles robot communications and why this leads to limitations.</p>
<h2>ROS 2 Communications Were Not Designed for Wide Area or Wi-Fi Networks</h2>
<p>ROS 2 applications are built as a collection of nodes that communicate through the Data Distribution Service (DDS). <a href="https://iot.eclipse.org/adopters/#iot.cyclonedds">Eclipse Cyclone DDS</a> is the rising star of DDS implementations in the ROS 2 ecosystem and is used for communications across nodes within the same robot as well as among robots. As a result, communications among ROS 2 nodes is peer-to-peer and makes extensive use of UDP/IP multicast techniques.</p>
<p><span style="font-size:12px">Figure 1: ROS 2 Applications Use DDS for Robot Communications</span></p>
<p><img class="img-responsive" src="images/1-1.png"></p>
<p>While DDS is extremely effective for supporting the node-to-node communications within and among robots on a wired LAN, its underlying protocol was not designed for wireless or to scale easily across a WAN. Challenges include discovering traffic and the nature of the reliability protocol. Using multicast on Wi-Fi networks is also problematic.</p>

<h2>Eclipse zenoh Is an Incredibly Efficient Protocol</h2>
<p>Eclipse zenoh blends leading research on named-data-networking with IoT publish/subscribe protocols to provide unified abstraction for data in movement and data at rest. It is designed to scale out, up, and down, and its routing infrastructure is designed to run at internet scale. We&rsquo;ve proven that an Eclipse zenoh runtime requires as little as 300 bytes on an 8-bit Atmel microcontroller. Finally, the Eclipse zenoh protocol transparently supports peer-to-peer and routed communications and the protocol needs only a handful of bytes of wire overhead to do all of this.</p>
<p>Together, these capabilities make Eclipse zenoh the most wire-efficient protocol to date (Figure 2).</p>
<h3>Key Abstractions and Primitives Enable Data Management</h3>
<p>Eclipse zenoh defines four key abstractions:</p>
<ul>
	<li><strong>Resource</strong>: A named piece of data, such as a key value. For example: /company/cool-robot/42/battery-level, 0.25.</li>
	<li><strong>Publisher</strong>: A spring of values for a key expression. For example, a robot publishing its position as the key /company/cool-robot/42/position.</li>
	<li><strong>Subscriber</strong>: A sink of values for a key expression. For example, an application monitoring a robot&rsquo;s position and subscribing to the key expression /company/cool-robot/*/position.</li>
	<li><strong>Queryable</strong>: A well of values for a key expression. In other words, an entity available on the network that will answer queries matching the queryable key expression.</li>
</ul>
<p>It&rsquo;s worth noting that queryable can be used to model:</p>
<ul>
	<li>&nbsp;A <strong>resource </strong>that can<strong> produce data on demand</strong>. For example, querying /company/cool-robot/42/battery-level returns the battery level for a specific robot, while /company/cool-robot/*/battery-level returns the battery level for all robots.</li>
	<li>A <strong>computation </strong>that is triggered as a result of the query.</li>
	<li>The read-side of <strong>geodistributed storage</strong>.</li>
</ul>
<p>These fundamental abstractions can be natively combined to manage data in motion, data at rest, and remote computations. Publishers and subscribers are the natural abstraction to manage data in motion. Likewise, combining a subscriber with a queryable expression creates a model for distributed storage. Queryable can also be used to model distributed computations.</p>
<p>Along with abstractions, Eclipse zenoh provides the following primitives:</p>
<ul>
	<li><strong>scout</strong>: Performs a discovery, looking for other zenoh entities, such as routers or peers, on the network. Once an entity is discovered, a zenoh session can be established.</li>
	<li><strong>open/close</strong>: Opens or closes an Eclipse zenoh session with an entity, such as a router or a peer.</li>
	<li><strong>declare/undeclare</strong>: Declares or undeclares a resource, publisher, subscriber, or queryable entity.
	<ul>
		<li>For subscribers, the declare primitive registers a user-provided call-back that&rsquo;s triggered when data is available.</li>
		<li>For a queryable entity, the declare primitive registers a user-provided call-back that&rsquo;s triggered when a query must be answered.</li>
	</ul>
	</li>
	<li><strong>write</strong>: Writes data for a key expression and can be viewed as the publish operation.</li>
	<li><strong>pull</strong>: Pulls data for a pull subscriber. This allows the infrastructure to proactively move the data as close as possible to the subscriber so it is usually only one hop away when the pull request is issued.</li>
	<li><strong>query</strong>: Issues a distributed query and returns a stream of results. The query target, coverage, and consolidation depend on configured policies.</li>
</ul>
<p>Eclipse zenoh also provides a higher level API that combines core abstraction and primitives to provide key/value geodistributed storage and distributed computation abstraction. To support an unlimited number of storage backends, Eclipse zenoh&rsquo;s storage abstraction uses a plug-in that integrates almost any database technology, including relational database management systems (RDBMSs), NoSQL, and time series databases.</p>
<p>In spite of the back-end technology, Eclipse zenoh provides uniform access to data. Stored values are retrieved by issuing a distributed query, and data encoding is transcoded as necessary to deal with different internal representations.</p>

<h2>Edge Robotics with Eclipse zenoh</h2>
<p>At this point, it should start to be clear why Eclipse zenoh plays a key role in enabling edge robotics. But, there is still one important question to answer: How does Eclipse zenoh support DDS data flows?</p>
<p>We&rsquo;ve been working on a DDS plug-in for the Eclipse zenoh router and we expect to release it in the coming weeks. The plug-in is entirely built using Eclipse Foundation technologies, including Eclipse Cyclone DDS, for DDS communications (Figure 2).</p>
<p><span style="font-size:12px">Figure 2: Edge Robotics With Eclipse zenoh</span></p>
<p><img class="img-responsive" src="images/1-2.png"></p>
<p>As a result, it&rsquo;s now possible to bridge DDS data on Eclipse zenoh. That means:</p>
<ul>
	<li>Robot data can be streamed at internet scale with the Eclipse zenoh routing infrastructure.</li>
	<li>Robot data can be stored anywhere it makes sense on the Eclipse zenoh network &mdash; on the robot, on the edge infrastructure, in the cloud, or in all three. More importantly, the data can be queried from anywhere on the network using Eclipse zenoh&rsquo;s distributed queries.</li>
	<li>Computation can be triggered on robots and the results can be collected anywhere on the network using a combination of the queryable abstraction and query primitive.</li>
</ul>
<h2>Get More Information</h2>
<p>To learn more about Eclipse zenoh, visit the <a href="http://zenoh.io/">website</a>.</p>
<p>To get involved in the Eclipse zenoh conversation on Gitter, click <a href="https://gitter.im/atolab/zenoh">here</a>.</p>
<p>For more information about the Eclipse Edge Native Working Group, click <a href="https://edgenative.eclipse.org/#members">here</a>.
</p>

<div class="margin-bottom-20">
  <?php print $Theme->getShareButtonsHTML(); ?>
</div>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row margin-bottom-20">
        <div class="col-sm-8">
          <img class="img img-responsive" alt="<?php print $pageAuthor; ?>" src="images/adlink.png" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><?php print $pageAuthor; ?></p>
          <p class="author-bio">
            This article was developed by the Eclipse zenoh team at <a href="https://www.adlinktech.com/en/Index">ADLINK Technology</a>. ADLINK plays a leadership role in edge computing by providing the network nodes that enable practical and scalable IoT systems. The company builds on its strong heritage in embedded computing to provide the hardware and advanced, edge-aware software capabilities required for multivendor, multi-standard connectivity in end-to-end IT and operational technology business solutions.
          </p>
        </div>
      </div>
    </div>
  </div>
</div>