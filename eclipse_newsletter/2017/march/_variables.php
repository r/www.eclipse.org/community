<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "Internet of all the Things";
  $pageKeywords = "eclipse, newsletter, internet of things, iot, hono, kapua, iofog, fog computing, vorto";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "This month, the Eclipse Newsletter is all about the Internet of Things, aka IoT. The articles feature various Eclipse IoT projects including, Eclipse Hono, Eclipse IoFog, Eclipse Kapua, and Eclipse Vorto.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "Devoxx US";
  $sponsorLink = "https://devoxx.us/";
  $sponsorImage = "/community/eclipse_newsletter/assets/public/images/devoxxus_small.png";

  // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;