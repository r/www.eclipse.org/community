<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The <a href="https://www.eclipse.org/"> Eclipse Foundation</a> is the main open source community which has a great focus on the Internet of Things world and the related <a href="https://iot.eclipse.org/">Eclipse IoT</a> ecosystem involves a lot of different projects and partners like Red Hat, Bosch, Eurotech, IBM, and many more. Recently, publishing this <a target="_blank" href="https://iot.eclipse.org/resources/white-papers/Eclipse%20IoT%20White%20Paper%20-%20The%20Three%20Software%20Stacks%20Required%20for%20IoT%20Architectures.pdf">white paper</a>, they showed a full stack with all the available tools for building a complete IoT solution from devices to the Cloud through the gateways.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/selection_005.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/selection_005_sm.png" alt="Selection Eclipse Hono"/></a></p>

<p>In relation to the Cloud side, one of the main problems in the IoT world is the ability to handle millions of connections from devices (and gateways) in the field, their registration for managing authentication and authorisation and, last but not least, the ingestion of the data received from them like telemetry or events. Finally, the last point is related to control these devices sending them commands in order to execute actions in the environment around them or upgrading their software and configuration.</p>

<p>The Eclipse Hono&trade; project is the answer to these problem !</p>

<h2>The APIs</h2>

<p>From the <a target="_blank" href="https://www.eclipse.org/hono/">official website</a>, we can read :</p>

<p><i>Eclipse Hono&trade; provides remote service interfaces for connecting large numbers of IoT devices to a back end and interacting with them in a uniform way regardless of the device communication protocol.</i></p>

<p>The mantra from the landing page of the website project is <b>“Connect. Command. Control”</b> which is made a reality through a well defined API for :</p>

<ul>
	<li><b>Registration</b> : for handling the requests related to the registration (so creation) of a new device so that it can be authorised to connect. It’s also possible retrieving information about registered devices or delete them;</li>
	<li><b>Telemetry</b> : for the ingestion of a large volume of data from devices (i.e sensors) available for analysis to the backend applications;</li>
	<li><b>Event</b> : for receiving specific events (i.e. alarms, notification, …) from devices for making decision on the Cloud side. This API is quite similar to a telemetry path but it uses a “different” channel in order to avoid such events going through the overwhelmed telemetry one;</li>
	<li><b>Command & Control</b> : for sending commands to the devices (from a backend application) for executing operations and actions on the field (receiving the related response) and/or upgrading local software and configuration;</li>
</ul>

<p>All the above APIs are accessible through the <a target="_blank" href="https://www.amqp.org/">AMQP 1.0</a> protocol (the only standardised AMQP version!) and it means that they are defined in terms of addresses on which devices need to connect for interacting with the system and the properties/content of the messages; of course, it’s true not only for devices but even for business and backend applications which can receive data from devices or send them commands. In any case, it doesn’t mean that devices which aren’t able to speak such protocol can’t connect but they can leverage on the <b>protocol adapters</b> provided by Hono; the current implementation provides an <a target="_blank" href="http://mqtt.org/">MQTT</a> and HTTP REST adapter.</p>

<p>All these APIs are though in order to allow <b>multi-tenancy</b> so that using a single deployment, it’s possible to handle channels for different tenants so that each of them can’t see data or exchanged messages from the others.</p>

<h2>The Architecture</h2>

<p>The main components which build the Eclipse Hono&trade; architecture are :</p>

<ol>
	<li><b>Protocol Adapters</b> : these components adapt a device protocol to the first citizens protocol used in Hono, the AMQP 1.0. Today, an MQTT and HTT REST adapters are provided out of box but thanks to the available interfaces, the user can develop a new adapter even for some custom protocols;</li>
	<li><b>Hono Server</b> : this is the main component to which devices can connect directly through AMQP 1.0 or through the protocol adapters. It’s in charge to expose the APIs in terms of endpoints and handling the authentication and authorisation of devices;</li>
	<li><b>Qpid Dispatch Router</b> : this is an AMQP 1.0 router, part of the <a target="_blank" href="http://qpid.apache.org/components/dispatch-router/">Apache Qpid</a> project, which provides the underlying infrastructure for handling millions of connections from devices in the fields. The simpler deployment can use only one router but in order to guarantee reliability and high volume ingestion, a router network should be used;</li>
	<li><b>ActiveMQ Artemis</b> : this is the broker mainly used for handling command and control API so for storing commands which have to be delivered to devices;</li>
</ol>

<p>While the devices connect directly to the Hono Server (or through protocol adapters), the backend applications connect to the Qpid Dispatch Router leveraging on direct addressing for receiving telemetry data (if no application is online, no devices are able to send data) or sending commands (the queus backed in the broker are reachable through the router).</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/selection_0041.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/selection_0041_sm.png" alt="Selection Eclipse Hono branch"/></a></p>

<h2>The running environment</h2>

<p>All the artifacts from the project are provided as Docker images for each of the above components that can run using <b>Docker Compose</b> (the <b>Docker Swarm</b> support will be available soon) or using a more focused Cloud platform like <b>OpenShift</b> (compatible with <b>Kubernetes</b> deployment as well).</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/hono_logos.png" alt="logos"/></p>

<p>Regarding the OpenShift deployment, the building process of the Hono project provides a bunch of YAML files which describe the objects to deploy like deployment configurations, services, persistent volumes and related claims. All the need is having an OpenShift platform instance accessible and deploy such objects for having Eclipse Hono&trade; running in a full featured Cloud environment with all the related scaling capabilities.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/hono_openshift.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/hono_openshift_sm.jpg" alt="Hono openshift"/></a></p>

<p>The example deployment is based on four pods, one for each of the main components so there is the router pod, the Hono Server pod and one pod for each protocol adapter; of course if you need the command & control path, even the broker pod need to be deployed.</p>

<p>In order to try it, an <a target="_blank" href="https://www.openshift.org/">OpenShift Origin</a> instance can be used on a local PC for deploying the entire Hono solution; for example, the above picture is related to my tests where you can see all the pods running on OpenShift (left side) with simulated devices that interact using MQTT and HTTP REST (on the right side).</p>

<p>The documentation which describes the steps for having such a deployment is available on the official website <a href="https://www.eclipse.org/hono/deployment/openshift/">here</a>. So what are you waiting for? Give it a try!</p>

<h2>Conclusion</h2>

<p>In my mind every IoT solution should be made of three different layers (a little bit different from the Eclipse vision) : the devices/gateways, the connectivity layer and the service layer.</p>

<p>While the <a href="https://www.eclipse.org/kura/">Eclipse Kura</a> project fits in the gateways layer and the <a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a> in the service layer, Eclipse Hono is the glue between them in order to handle the connections from devices and making available their data to the backend services (and vice versa in the opposite direction for command and control). Thanks to the API standardisation  and the usage of a standard protocol like AMQP 1.0, Hono can be used for connecting any kind of devices with any kind of services; of course leveraging on a these three project has the big advantage of having big companies working on them, mainly Red Hat, Bosch, and Eurotech.</p>

<p>Finally, the solution is always the same …. open source and collaboration!</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/march/images/paolo_p.jpg"
        alt="Paolo Patierno" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Paolo Patierno<br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ppatierno">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://paolopatierno.wordpress.com/">Blog</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/paolopatierno">LinkedIn</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/ppatierno">GitHub</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
