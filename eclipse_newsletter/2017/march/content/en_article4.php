<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<img class="img-responsive" align="right" src="/community/eclipse_newsletter/2017/march/images/kapualogo.png"/><p><a href="http://www.eclipse.org/kapua/">Eclipse Kapua</a> is a modular, integrated, interoperable IoT cloud platform to manage and integrate devices and their data and provide a solid foundation for IoT services for any IoT application.</p>

<p>Eclipse Kapua began as an open source incubator project under the Eclipse IoT Workgroup. In October, 2016 the source code of the first drop of Eclipse Kapua was contributed by Eurotech, in collaboration with Red Hat.</p>

<p>Eclipse Kapua provides the services required to manage IoT gateways and smart edge devices, through a core integration framework. It offers an initial set of services for device registry, device management, message routing, data integration, and application enablement.</p>

<p>The goal of the Eclipse Kapua project is to provide a cloud-based IoT integration platform with the following high-level functionality:</p>

	<ul>
		<li>Manages the connectivity for IoT devices and IoT gateways through a number of different protocols. Initial support will be offered to established IoT protocols like MQTT. Other protocols like AMQP, HTTP, and CoAP will be added over time. The connectivity layer is also responsible for managing device authentication and authorization.</li>
		<li>Manages the devices on the edge. Device management offers the ability to inspect device configuration, update device applications and firmware, and control the device remotely. The IoT platform exposes an open contract towards the target device being managed with no assumption on the device software stack. The device management will evolve to adopt emerging standard device management protocols like LWM2M.</li>
		<li>Data pipelines for data from IoT devices collecting large amounts of telemetry data. The data pipelines can offer data archival for dashboards or business intelligence applications andenable real-time analytics and business rules. An important feature is flexible and configurable data integration routes offering data storage options to collect the incoming data and make it available to upstream enterprise applications.</li>
		<li>Multi-tenant account management, user management, permissions and roles.</li>
		<li>Fully programmable via the RESTful API. A web-based administration console for a device operator is desirable.<li>
		<li>Deployment either in the cloud or ‘on premise’, with its packaging allowing for flexible deployment options.</li>
	</ul>
	
<p>The following diagram provides a functional architecture of the Eclipse Kapua project.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/kapuaarchitecture.png" alt="Eclipse Kapua Architecture" /></p>
	
<h2>Getting Started</h2>

<h3>1. Clone and Run Eclipse Kapua</h3>

<p>> Clone the Kapua git repository.</p>
<pre>$ git clone https://github.com/eclipse/kapua</pre>

<p>> Build and run the Kapua Demo Box using the quick start script.</p>
<pre>
$ cd ./kapua/dev-tools/src/main/bin
$ ./start-demo.sh
</pre>

<p>> The following services are now available:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/blueblocks.png" alt="building process" /></p>
	
<p>> For more information on the manual build process or for creating a development Vagrant machine, please refer to the <a target="_blank" href="https://github.com/eclipse/kapua">GitHub Readme</a>. More documentation will be coming soon.</p>

<h3>2- Connect an Eclipse Kura device</h3>

<p>> Connect a Kura-powered device like a Raspberry Pi to Kapua.</p>
<p align="center"><a href="/community/eclipse_newsletter/2017/march/images/eclipse_kura_device.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/eclipse_kura_device_sm.png" alt="Eclipse Kura device" /></a></p>

<p>> Connect other MQTT devices.</p>

<h3>3 – Manage the device</h3>

<p>> Manage the IoT device.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/managethedevice_sm.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/managethedevice_sm.png" alt="Manage Kura Device" /></a></p>

<p>> Install the heater app.</p>

<h3>4 – Analyze device data</h3>

<p>> Review your IoT device data.</p>

<h2>Status and Roadmap</h2>

<p>The Eclipse Kapua project is an important example of an ecosystem of organizations collaborating on IoT core components that work well together and that can be used to bootstrap IoT applications and solutions. In the future, it is expected that other Eclipse IoT projects, open source projects, and commercial vendors will provide additional services that integrate with Eclipse Kapua.</p>

<p>We invite you to learn more about <a href="http://www.eclipse.org/kapua/index.php">Eclipse Kapua</a> and <a href="https://www.eclipse.org/kapua/index.php#community">join the community</a>. We look forward to collaborating with you soon.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/march/images/jamesk.jpg"
        alt="James Kirkland" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            James Kirkland<br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jkirklan?lang=en">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://developerblog.redhat.com/author/kirk10kirk/">Blog</a></li>
           	<li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/jameskirkland/">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
                <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/march/images/marcoc.jpg"
        alt="Marco Carrer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Marco Carrer<br />
            <a target="_blank" href="http://www.eurotech.com/en/">Eurotech</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/marco-carrer-a535311/?locale=de_DE">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

