<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><a href="https://www.eclipse.org/vorto/">Eclipse Vorto</a> is an open source project that aims to help compile and manage abstract device descriptions (information models). What’s special about this project is that it’s not an attempt to prescribe any one standard that encompasses all existing technologies. Instead, Eclipse Vorto takes a completely technology-independent approach – and this could eventually make it easier to implement standards.</p>

<p>To achieve interoperability for devices in the IoT, device manufactures and platform operators have to take into account all the various communication protocols, hardware environments, and programming languages, as well as their idiosyncrasies. Eclipse Vorto addresses these issues. What are known as information models provide an abstract description of device properties and functions. These information models can then be managed and made available through the <a href="http://vorto.eclipse.org/">Vorto Repository</a>. This means, device manufacturers can release information models for users to download; they can then use code generators to create platform-specific source code – so they can integrate their devices far more easily.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/eclipsevorto.jpg" alt="Eclipse Vorto" /></p>

<h2>Getting Started</h2>
<p>All you need to get started, is to install the <a href="https://marketplace.eclipse.org/content/vorto-toolset">Vorto Toolset Plugins</a> from Eclipse Marketplace. Once installed, you can switch to the Vorto perspective where you have the option to either create a new Information Model or browse through existing information models in the <a href="https://www.eclipse.org/vorto/">Eclipse Vorto Repository</a>.</p>

<p>In this simple example, we are going to describe a grove light sensor and use the Vorto Code Generation Infrastructure to generate source code for this description in order to visualize sensor data in an AngularJS web interface.</p>

<p>Let's begin.</p>

<p><b>Step 1</b>: Create a new Vorto Demo Project by clicking on the + icon and then create a new Information Model 'GroveLightSensor'. The functionality of the light sensor is already described as a functionblock and can be looked up in the Model Repository. Just drag and drop the functionblock onto the Information Model. This will download the model to your local workspace.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/vortobrowser.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/vortobrowser_sm.png" alt="Vorto Browser" /></a></p>
	
<p><b>Step 2:</b> Now that you have described the grove sensor, you can share it with the Vorto Repository where users can find it and integrate it into their IoT platform based on your information model using platform - specific code generators. Let's do that and integrate the grove sensor by building a tiny web application that is able to display the grove sensor value. To save us some development time, we can use the existing Web UI Code Generator provided by Vorto to implement this use case. Just right-click on the Grove Light Sensor information model, choose <i>Generate Code &rarr; Web UI Generator</i>.</p>
	
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/march/images/webuigenerator.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/webuigenerator_sm.jpg" alt="Vorto Web UI Generator" /></a></p>

<p>Run the generated application from your Eclipse IDE and open the following URL in your browser:</p>

<pre>http://localhost:8080/webdevice</pre>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/grovelightsensor.png" alt="Grove Light Sensor Vorto" /></p>

<p>Voila, the UI is fairly simple but keep in mind, that we just spent a couple of seconds "implementing" this. Take a look at the <a target="_blank" href="http://www.eclipse.org/vorto/documentation/tutorial/mqttwithwebapp.html">Vorto tutorial</a> that walks you through some additional steps by consuming MQTT data for your device and bind the device values to the web application UI elements.</p>
<p>Good luck!</p>
	
<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/march/images/alexedelmann.png"
        alt="Alexander Edelmann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Alexander Edelmann<br />
            <a target="_blank" href="https://www.bosch-si.com/corporate/home/homepage.html">Bosch SI</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/alex07_80">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/aedelmann">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

