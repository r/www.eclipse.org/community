<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Discussions of <a href="https://projects.eclipse.org/projects/iot.iofog">Eclipse ioFog</a> (and all edge/fog computing solutions) normally focus on running microservices at the edge. However, connections between edge compute nodes and data centers and clouds are just as important, and they must be secure. Nobody should be opening edge environments to allow incoming connections. Furthermore, no openings into data centers or clouds should be required, either. In addition, connectivity reliability and bandwidth are ongoing issues. Also, don’t forget that dynamic interconnection is one of the main points of the Internet of Things, so that capability is key too. How do we accomplish these connections securely and reliably?</p>

<p>We need to think differently about how connections are made. Ideally, we should have secure connection principles that are applied adaptively to different implementation scenarios but yet are never compromised. But at the same time, any implementation should be thoroughly practical with high efficiency, scalability, and low cost in mind.</p> 

<p>The principles of IoT interconnections in Eclipse ioFog are:</p>
	<ul>
		<li>No inbound connections</li>
		<li>Never reveal locations</li>
		<li>Identities are flexible, but trust is not</li>
		<li>Each component assumes the others are compromised and requires proof that they are not</li>
		<li>Uses minimum bandwidth</li>
		<li>Network connectivity is always be assumed to be unreliable</li>
		<li>An encrypted connection is only a starting point – ioFog’s security model builds on top of that</li>
		<li>All entities must seek help outside of themselves for establishing trust</li>
		<li>Connections between all things are dynamic</li>
		<li>No manual effort is required to connect two points</li>
		<li>When connections are decommissioned, they leave no trace</li>
		<li>No node should have access to any information about the greater system</li>
	</ul>

<p>This full set of principles might seem only theoretical, but Eclipse ioFog provides a full implementation ready for you to use.</p>

<p>The way ioFog achieves a secure dynamic interconnect model is to split the operations into separate parts  – Let’s have a look at the three system components and how they work together.</p>

<p>The <b>ioFog Agent</b> is the software that turns commodity hardware (e.g. inexpensive single board computers that run Linux) into edge computing nodes. Each ioFog Agent has a trusted relationship (validated from both sides) with a <b>FogController</b>, which has the authority to tell ioFog Agent nodes what software (via microservice containers) to run and also pass that software as well as configuration information to the nodes at runtime. Using this ability, the FogController can tell two separate ioFog Agent nodes to connect with each other securely.</p>

<p>Two ioFog Agent nodes can connect with each other even if they are separated by great distances, NATed networks, and/or firewalls. Each side receives instructions from the FogController to run a special fog microservice that performs the internetworking. The microservices are delivered identity and connection information at runtime. Each side will only know its half of the puzzle and therefore cannot directly connect, but only “opt in” to the connection with its security credentials.</p>

<p>Those internetworking microservices each create an outbound connection to a third Eclipse ioFog component called <b>ComSat</b> – because it acts as a virtual communications satellite that ioFog Agent nodes use as a relay point to converse with other nodes. The ComSat has received the full set of security information from the FogController and is therefore able to validate the incoming connections of the ioFog Agent nodes and ignore any other attempted connections. When ComSat is satisfied with the security information of the two connecting ioFog Agent nodes (i.e. both have successfully opted in), it will facilitate communication between them. ComSat can buffer the flow of data between nodes in order to handle connectivity interruptions, and the communication pattern between the ioFog Agent nodes and ComSat is designed for minimal bandwidth requirements while maintaining reliability. Each ComSat can handle thousands of internetworking pairs and any number of ComSats can be deployed in an IoT solution for load handling.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/march/images/diagramiofog.jpg" alt="Eclipse IoFog" /></p>

<p>No VPN connection or WAN connectivity was required between locations. No inbound connection openings were made to either edge. And no stated intent to connect the two fog nodes was needed prior to runtime. Yet, we were able to connect those two arbitrary points securely on a whim.</p>

<p>An important point here is that this enhanced security model is only possible with edge/fog computing – you can’t deliver identity and connectivity unless you have a mechanism for dynamically distributing software in a secure fashion. Another point is that you will want to deploy the entire Eclipse ioFog ecosystem on your infrastructure in order to achieve the most secure fog computing environment.</p> 

<p>Using multiple system components allows for external security validation operations. A key architectural feature of Eclipse ioFog is that authentication of compute nodes happens in the ComSat component, not in FogController, thus keeping that authentication separate from the component that pushes software and configuration to the nodes. But the entire secure dynamic interconnection model truly rests on the ability to distribute software to the edge dynamically, remotely and securely, at runtime. Using Eclipse ioFog, we can assemble and deploy secure enterprise IoT infrastructure that has never been possible before.</p>

<p>You can get started with Eclipse ioFog today by using the resources at <a target="_blank" href="https://iotracks.com">https://iotracks.com</a>. Installation packages are available for the ioFog Agent for all major Linux distros at <a target="_blank" href="https://iotracks.com/developer">https://iotracks.com/developer</a>. A FogController and a set of ComSats are already hosted for trial purposes, so you just need to put the ioFog Agent on some compute hardware (e.g. Raspberry Pi, a Linux box, or even a Linux VM) in order to see Eclipse ioFog in action. You can use the Eclipse ioFog developer <a target="_blank" href="http://forum.iotracks.com">forum</a> to get help and support.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/march/images/kilton.jpg"
        alt="Kilton Hopkins" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Kilton Hopkins<br />
            <a target="_blank" href="https://iotracks.com/">IOTRACKS</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kiltonhopkins?lang=en">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/kilton">LinkedIn</a>
            <?php // echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
