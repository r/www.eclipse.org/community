<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>When we try to implement code authoring tools for a particular language we need a thorough understanding of the structure and usage of the language in question: for example, in order to provide accurate code completion for the Java language, we not only need to be able to parse Java source code and class files, but also understand the build process and dependencies (for example a maven pom.xml file) in order to know the visible universe at any given source location. The chances that these essential language tools are compatible with the language and technology of the development tool are small; if these language-aware tools exist, they are often written in the target language itself (for example, the typescript compiler is written in typescript). The team developing Visual Studio Code solved this mismatch by putting the language smartness engines in a separate process (a language server) and communicating with it via a standardized wire protocol: the Language Server Protocol. Developers from Red Hat, Codenvy and IBM have since worked to bring support for LSP to other development tools like Eclipse IDE and Eclipse Che.</p>

<p>This genesis has influenced the design of the protocol: one of its assumptions is that the host tool tightly controls the lifecycle of the language server and that the language server has access to the files making up the development workspace.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/developer_machine.png" alt="Developer Machine LSP"/></p>

<h2>How LSP support works in Eclipse Che</h2>

<p>Eclipse Che, however is already a distributed system with the IDE running in the browser and communicating with a workspace machine that holds the necessary tools and runtimes to develop for a particular runtime stack. The language servers add a third tier that needs to run somewhere.</p> 

<p>In Che, the front end does not directly communicate with the various language servers, instead, it talks to a single back end service that handles dispatching requests to the appropriate language server. Language servers are added to the system by plugging into an extension point in the wsagent process that runs in every workspace.</p>
 
<p>Since many existing language servers expect to find the workspace files in a local file system, and since the workspace machine likely already contains many prerequisites for the language server, it makes sense to to run the language servers in the workspace machine. The language servers that are integrated with the Che distribution are packaged as so called “workspace agents”. Workspace agents are components that can be installed into a workspace via a the runtime configuration of the workspace machine.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/browser_machine.png" alt="Browser Developer Machine LSP"/></p>

<h2>How to add your own LS to Che</h2>
<p>In order to familiarize yourself with Che extension development I recommend you read through the che docs beginning with <a href="https://www.eclipse.org/che/docs/assemblies/intro/index.html">https://www.eclipse.org/che/docs/assemblies/intro/index.html</a>.</p>
 
<p>Let’s follow the example of the JSON language server to illustrate what needs to be done. First, let’s package the json language server as a workspace agent (see <a target="_blank" href="https://www.eclipse.org/che/docs/assemblies/sdk-custom-agents/index.html">https://www.eclipse.org/che/docs/assemblies/sdk-custom-agents/index.html</a>). A new agent should be added as a new module in the /che/agents maven project. An agent requires some metadata and a shell script that will be executed when the the workspace machine is started. A simple way to implement the agent interface is to extend <a target="_blank" href="https://github.com/eclipse/che/blob/master/agents/che-core-api-agent-shared/src/main/java/org/eclipse/che/api/agent/shared/model/impl/BasicAgent.java">BasicAgent</a>, as in the <a target="_blank" href="https://github.com/eclipse/che/blob/master/agents/ls-json/src/main/java/org/eclipse/che/api/agent/LSJsonAgent.java">JSON Language Server Agent</a>: it reads metadata from a json file</p>

<pre>
{
  "id": "org.eclipse.che.ls.json",
  "name": "JSON language server",
  "description": "JSON intellisense",
  "dependencies": [],
  "properties": {}
}
</pre>

<p>and reads the agent startup script from a script file: the file first installs necessary packages:</p>

<pre>
command -v tar >/dev/null 2>&1 || { PACKAGES=${PACKAGES}" tar"; }
...

# Red Hat Enterprise Linux 7
if echo ${LINUX_TYPE} | grep -qi "rhel"; then
	test "${PACKAGES}" = "" || {
    	${SUDO} yum install ${PACKAGES};
	}

	command -v nodejs >/dev/null 2>&1 || {
    	curl --silent --location https://rpm.nodesource.com/setup_6.x | ${SUDO} bash -;
    	${SUDO} yum -y install nodejs;
	}
…handle other distros...
</pre>

<p>Finally it downloads and installs the agent. Note how the script writes a launch script for the language server into a local file on the last line:</p>

<pre>
AGENT_BINARIES_URI=https://codenvy.com/update/repository/public/download/org.eclipse.che.ls.json.binaries
...
curl -s ${AGENT_BINARIES_URI} | tar xzf - -C ${LS_DIR}

touch ${LS_LAUNCHER}
chmod +x ${LS_LAUNCHER}
echo "nodejs ${LS_DIR}/vscode-json-server/server.js" > ${LS_LAUNCHER}
</pre>

<p>Both the JSON file and install script are read from the project resources. Workspace agents need to be registered in the wsmaster project: we need to add them as a dependency of the project and register the LSJsonAgent class in the <a target="_blank" href="https://github.com/eclipse/che/blob/master/assembly/assembly-wsmaster-war/src/main/java/org/eclipse/che/api/deploy/WsMasterModule.java">WsMasterModule</a>:</p>

<pre>
@DynaModule
public class WsMasterModule extends AbstractModule {
	@Override
	protected void configure() {
      ...
    	Multibinder<Agent> agents = Multibinder.newSetBinder(binder(), Agent.class);
            ...
    	    	agents.addBinding().to(LSJsonAgent.class);
</pre>

<p>After rebuilding Che, the new agent should be visible in the runtime configuration of workspaces. Turn it on and watch the dev machine log to see the startup script run when the workspace starts up.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/may/images/eclipseche.png"></a><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/eclipseche_sm.png" alt="Eclipse Che IDE"/></a></p>

<p>Once the agent launches correctly, we need to register the language server with the workspace agent and provide a way to start the language server and to set up a communication channel to it. For this, add a new module to the /che/plugins project. To hook up the language server, we will extend the class <a target="_blank" href="https://github.com/eclipse/che/blob/master/wsagent/che-core-api-languageserver/src/main/java/org/eclipse/che/api/languageserver/launcher/LanguageServerLauncherTemplate.java">LanguageServerTemplate</a>: The first step in the implementation is to set up a description of the files this language server is supposed to handle:</p>

<pre>
@Singleton
public class JsonLanguageServerLauncher extends LanguageServerLauncherTemplate {

    private static final String LANGUAGE_ID = "json";
    private static final String[] EXTENSIONS  = new String[] { "json", "bowerrc", "jshintrc", "jscsrc", "eslintrc", "babelrc" };
    private static final String[] MIME_TYPES  = new String[] { "application/json" };
    private static final LanguageDescription description;
    static {
        description = new LanguageDescription();
        description.setFileExtensions(asList(EXTENSIONS));
        description.setLanguageId(LANGUAGE_ID);
        description.setMimeTypes(asList(MIME_TYPES));
    }

    @Override
    public LanguageDescription getLanguageDescription() {
        return description;
    }
...
</pre>

<p>Then we need some code that will start the language server process: note that it calls the script we have created in the agent startup script above.</p>

<pre>
   @Inject
    public JsonLanguageServerLauncher() {
        launchScript = Paths.get(System.getenv("HOME"), "che/ls-json/launch.sh");
    }

    @Override
    public boolean isAbleToLaunch() {
        return Files.exists(launchScript);
    }

    protected Process startLanguageServerProcess(String projectPath) throws LanguageServerException {
        ProcessBuilder processBuilder = new ProcessBuilder(launchScript.toString());
        processBuilder.redirectInput(ProcessBuilder.Redirect.PIPE);
        processBuilder.redirectOutput(ProcessBuilder.Redirect.PIPE);
        try {
            return processBuilder.start();
        } catch (IOException e) {
            throw new LanguageServerException("Can't start JSON language server", e);
        }
    }
</pre>

<p>And finally we need to hook up the language server to a Eclipse LSP4J <a target="_blank" href="https://github.com/eclipse/lsp4j/blob/v0.1.2/org.eclipse.lsp4j/src/main/java/org/eclipse/lsp4j/services/LanguageServer.java">LanguageServer</a> endpoint: </p>

<pre>
    protected LanguageServer connectToLanguageServer(Process    
                                                    languageServerProcess, 
                                                    LanguageClient client) {
        Launcher<LanguageServer> launcher = 
           Launcher.createLauncher(client, LanguageServer.class, 
                                   languageServerProcess.getInputStream(),                                                                           
                                   languageServerProcess.getOutputStream());
        launcher.startListening();
        return launcher.getRemoteProxy();
    }
</pre>

<p>This particular language server communicates via standard in and standard out. Other language servers may implement communication via other channels, for example sockets. It is up to the LanguageServerLauncher to set up the appropriate communication channels.</p>

<p>All that needs to be done now is to register our JsonLanguageServerLauncher with the dependency injection framework:</p>

<pre>
@DynaModule
public class JsonModule extends AbstractModule {
	@Override
	protected void configure() {
    	    Multibinder.newSetBinder(binder(), LanguageServerLauncher.class)
                     .addBinding()
                     .to(JsonLanguageServerLauncher.class);
	}
}
</pre>

<p>In order for the project to be included in the wsagent process, it needs to be added as a dependency to the wsagent assembly in the /che/assembly/assembly-wsagent-war maven project.</p>

<h2>Restrictions</h2>
<p>Currently, there can only be one language server registered for any file type. VS Code supports multiple language servers per file and defines rules how results from the language servers are merged. This can be used to implement add-on language servers like linters.</p>
<p>Che will currently start one instance of a language server per project, not a single server per workspace. So language servers must be able to function with multiple copies of the server running.</p> 

<h2>Future Directions</h2>
<p>LSP support in Che is under active development. Current areas of interest include implementing more protocol features (see issue #2109) and support for multiple language servers per file. The workspace agent presented in this article uses a script to install prerequisites. However, in a containerized world, it would make more sense to run each language server in its own container.</p>
<p>Another area of work is the inclusion of more language servers into Che.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/may/images/thomas.jpg"
        alt="Thomas Mader" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Thomas Mäder<br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/tsmaeder">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/tsmaeder/">GitHub</a></li>
           	<li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/tmaeder/">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

