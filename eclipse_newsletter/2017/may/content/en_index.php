<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2017.05.24</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/mikael.jpg" />
                            <h3>Editor's Note</h3>
                            <p> In this month's newsletter we will be re-thinking how advanced editor support for all kinds of languages is built. The current state of the art is implementing support for each language like Java, PHP or C++ independently for every IDE. This labor-intensive and error-prone approach is about to change with the introduction of the Language Server Protocol (LSP).</p>

<p>The more advanced the tooling you are developing, the closer you are to writing a full-blown compiler/interpreter for the language. This is a huge waste of time, as the developers of the language have already written these tools, but often not with IDE requirements in mind. They are usually designed to only handle complete, valid source files, while an IDE usually has to handle source files that are currently being edited and thus by definition incomplete.</p>

<p>Another caveat about the traditional approach is attracting contributors. Let's say your IDE is written in Java and provides PHP support and some PHP developers are pointing out bugs. Many of them would probably love to fix those bugs themselves, but the completely different language creates a high barrier for contribution.</p>

<p>Last but not least, language tooling developers not only have to support a broad range of IDEs, but they also need to chase after new languages features/versions. What about Java 10 features? PHP 8? Swift 4?...</p>

<p>The LSP developed by Microsoft for Visual Studio Code, aims at fixing these shortcomings. It is a language and IDE agnostic protocol which clearly separates language semantics from UI presentation. Language developers can implement the protocol and benefit from immediate support in all IDEs, while IDE developers who implement the protocol get automatic support for all these languages without having to write any language-specific code. This way both parties can focus on what they do best; language developers can ensure that auto completion proposals and error markers match the language specification perfectly, while IDE developers can focus on providing a great user experience. Even better, both parties can implement the protocol using the technology of their choice. The Eclipse IDE can consume the protocol with a Java API, while a PHP language server might be implemented in PHP.  Did we just find the Holy Grail?</p>

<p>Not quite yet. The Language Server Protocol is still limited. You do not yet get all the wonderful Java tooling that Eclipse JDT provides. But it's a paradigm shift. As I see it, IDEs will become a set of UI clients for several tooling services, all communicating via standard protocols. Microservices are everywhere, and they are coming to your IDE very soon.</p>

<p>Read the 8 great articles below to learn more!</p>
					<p>Mikaël Barbero (<a target="_blank" style="color:#000;" href="https://twitter.com/mikbarbero">@mikbarbero</a>)</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article1.php"><h3>Language Server Protocol</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>What is the Language Server Protocol? How was it created? All your questions are answered by the Microsoft team in this article.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article2.php"><h3>Implementing a JSON-RPC Protocol with Eclipse LSP4J</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Get a introduction to the core concepts of Eclipse LSP4J and learn how to implement a JSON-RPC protocol.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article3.php"><h3>Using Language Servers to Edit Code in the Eclipse IDE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Learn how the Eclipse IDE is able to consume language servers to provide rich editor features and interact with them without having to learn anything new.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article4.php"><h3>Eclipse JDT Language Server Project</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>What is Eclipse JDT Language Server (jdt.ls)? It's an open source Java language specific implementation of the Language Server Protocol. Find out more!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

       <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article5.php"><h3>Building & Running a Language Server with Eclipse Xtext & Theia</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Learn how to implement a Language Server for an arbitrary domain-specific language (DSL) using Eclipse Xtext and then run it in an IDE.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article6.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article6.php"><h3>LSP Support in Eclipse Che</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse Che, a developer workspace server and cloud IDE, now offers support for the Language Server Protocol. Read this article to find out more.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

         <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article7.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/7.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article7.php"><h3>Getting Started with LSP in Eclipse Orion</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Read this article to discover how to get started with the Language Server Protocol in Eclipse Orion.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article8.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/8.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/may/article8.php"><h3>Sourcegraph, Code Intelligence, and the Language Server Protocol</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>How is the Language Server Protocol being used by companies like Sourcegraph?</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="http://blog.ttoine.net/en/2017/05/23/its-time-to-organise-eclipse-oxygen-democamps/">It’s time to organise Eclipse Oxygen DemoCamps</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://iot.eclipse.org/resources/case-studies/Eclipse%20IoT%20Success%20Story%20-%20DB.pdf">Case Study: Deploying Eclipse IoT on Germany's DB Railway System</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2017/cfp">EclipseCon Europe 2017 | Call for Papers Open</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2017/conference/schedule/session/2017-06-21">Program Ready for EclipseCon France 2017</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/20170426_iottestbeds.php">New Eclipse IoT Open Testbeds</a></li>
                           </ul>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
                            <ul>
                             	<li><a target="_blank" style="color:#000;" href="https://waynebeaton.wordpress.com/2017/05/14/devoxx4kids-ottawa-june-2017/">Devoxx4Kids comes to Ottawa June 2017</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://adtmag.com/articles/2017/05/10/eclipse-iot.aspx?m=1">Eclipse IoT Group Unveils New Eclipse Kura and Eclipse Kapua and Open Testbeds</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://www.incquerylabs.com/news/2017/5/5/eclipsetoolmakers-day">Recap on Eclipse ToolMakers’ Day

                             	</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://developers.redhat.com/promotions/building-reactive-microservices-in-java">Mini-book about Reactive Microservices with Eclipse Vert.x</a></li>
                             </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

    <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/PuViwVGuDuM" frameborder="0" allowfullscreen></iframe>
</div>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Virtual Eclipse Meetup</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Last week, the first <a target="_blank" style="color:#000;" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/">Virtual Eclipse Community Meetup</a> aired LIVE. Sopot Cela, Senior Software Engineer at Red Hat, joined us to share Productive Coding with the Eclipse Java IDE. Watch the video now (simply click on the left) and <a target="_blank" style="color:#000;" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/">join the Meetup!</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
   <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/bar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Java IDE Tips &amp; Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>This new section will feature monthly Tips &amp; Tricks for Eclipse Java IDE developers.</p>
                            <p>This month's tip in action &#10145;<br>
                            <pre> Edit -> Toggle Block Selection tool
                            (or just hit Shift+Alt+A)</pre></p>
                            <p>For more tips and tricks follow <a target="_blank" href="https://twitter.com/eclipsejavaide">@EclipseJavaIDE</a>, a Twitter account run by Eclipse developers! Their tweets will help you improve your Java coding skills. Share your very own Eclipse Java IDE tips and tricks via Twitter #EclipseTips.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                                            <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							 <a target="_blank" href="/community/eclipse_newsletter/2017/may/images/eclipsejavatiptrick.gif"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/eclipsejavatiptrick.gif"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/bar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-ceylon">Eclipse Ceylon</a>: a modern statically-typed programming language for the Java, Android, and JavaScript virtual machines.</li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-elogbookopenk">Eclipse eLogbook@openK</a>: provides a digital logbook for Distribution System Operators (DSO).</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.efxclipse/releases/2.6.0/review">Eclipse e(fx)clipse 2.6</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/gemini/blueprint/documentation/reference/2.1.0.M2/html/what-is-new.html#gemini-blueprint-2.1.0">Eclipse Gemini Blueprint 2.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/app4mc/news/2017-04-28_release_0.8.0/">Eclipse APP4MC 0.8</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/kura/downloads.php">Eclipse Kura 3.0</a></li>
                            </ul>
                            <p>Interested in more project release/review activity? <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">Read on</a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/april/images/ecfrance_india.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                     <tr>
                           <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://blog.ttoine.net/en/2017/05/23/its-time-to-organise-eclipse-oxygen-democamps/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/may/images/democamps.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                             <p>It's time to organize <a target="_blank" style="color:#000;" href="http://blog.ttoine.net/en/2017/05/23/its-time-to-organise-eclipse-oxygen-democamps/">Eclipse Oxygen DemoCamps</a> for the summer. You might be asking yourself, what is a DemoCamp and why should I care? Find out!</p>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Zurich">Eclipse DemoCamp Zurich</a><br>
                            May 29, 2017 | Zurich, Switzerland</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Hamburg">Eclipse DemoCamp Hamburg</a><br>
                            Jun 19, 2017 | Hamburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/france2017/">EclipseCon France 2017</a><br>
                            Jun 21-22, 2017 | Toulouse, France</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Paderborn">Eclipse DemoCamp Paderborn</a><br>
                            Jun 27, 2017 | Paderborn, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Munich">Eclipse DemoCamp Munich</a><br>
                            Jun 28, 2017 | Munich, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Shenzhen">Eclipse DemoCamp Shenzhen</a><br>
                            Jun 29, 2017 | Shenzhen, China</p>
                            <p><a target="_blank" style="color:#000;" href="http://eclipsesummit.in/">Eclipse Summit India 2017</a><br>
                            Jul 27-29, 2017 | Bangalore, India</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamps_Oxygen_2017/Braunschweig">Eclipse DemoCamp Braunschweig</a><br>
                            Aug 8, 2017 | Braunschweig, Germany</p>
                             <p><a target="_blank" style="color:#000;" href="https://www.meetup.com/Trondheim-Eclipse-User-Group/events/236894151/">Eclipse DemoCamp Trondheim</a><br>
                            Aug 24, 2017 | Trondheim, Norway</p>
                            <p><a target="_blank" style="color:#000;" href="http://thingmonk.com/">Eclipse IoT Day @ Thingmonk</a><br>
                            Sep 11, 2017 | London, UK</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2017/">EclipseCon Europe 2017</a><br>
                            Oct 24-26, 2017 | Ludwigsburg, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 100%; margin: 0 auto; margin-top: 30px; text-align: center;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
