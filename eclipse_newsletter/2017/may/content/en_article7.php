<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p><a target="_blank" href="https://orionhub.org/">Eclipse Orion</a> is made up of two different parts: a client portion that runs inside a browser and a server. There are two separate implementations of the Orion server - one written in Java and the other one written in Node.js. For the Language Server Protocol (LSP) work, we used the Node.js server.</p>

<p>The integration of LSP inside Orion focused on getting support for the Java Language inside the Orion editor. The <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls.git">Eclipse JDT Language Server</a> (a.k.a. jdt.ls) is implementing a language server for the Java language based on the well-known JDT projects.</p>

<h2>How to get started</h2>

<p>To get the latest Orion LSP code, do the following steps:</p>

<pre>
git clone https://github.com/eclipse/orion.client
git checkout java-lsp
cd orion.client
</pre>

<p>All steps to contribute and get started are explained in the file called <code>CONTRIBUTING_lsp.md.</code></p>

<h3>Using Docker</h3> 

<p>There is also a docker file inside orion.client/modules/orionnode/. This can be used to create Docker image that contains the latest build for the Eclipse jdt.ls and the latest Orion node.js build that contains the lsp support. To start building the image run:</p>

<pre>
./docker_build.sh
</pre>

<p>Once the build finishes, you can then start the image:</p>

<pre>
docker run -p 8083:8083 orionlsp
</pre>

<p>You can connect to it by opening a browser on <code>http://localhost:8083/</code>.</p>

<p>We will now examine the changes made to the Orion server and the Orion client in order to enable support for the Java language inside the Orion editor.</p>

<h2>The chosen architecture</h2>

<p>Before we start, we should explain how we got Orion to interact with the language server.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/language_server.png" alt="Eclipse Orion Language Server Architecture"/></p>

<p>The LSP server and the Node.js server are both running on the same machine right now. The LSP server is installed locally in the Node.js server inside a folder called <i>‘server’</i>. The purpose of this is to get one LSP server running for each Node.js server. At the moment, the LSP server cannot be shared between two Orion servers.</p>

<h2>The Orion server</h2>

<p>On the Orion server side, there is not much to do to get the language server communication going. We defined an extension that we register in the server code (see line 100 in server.js). That extension starts listening on a named socket that is used to start and initialize the LSP server.</p>

<h2>The Orion client</h2>

<p>The Language Server Protocol defines a lot of requests that can be made to a LSP server: document life cycle (open/close/change files), code formatting, hover, occurrences, search references, code completions, etc. <a target="_blank" href="https://github.com/Microsoft/language-server-protocol">Click here</a> for more details.</p>

<p>The current implementation supports most of them. Once the server is started and initialized on your workspace contents, the user can format code, get problems and warnings, search for references, get Javadoc hovers, get errors/warnings as you type, etc.</p>

<p>Right now, there is no support for code actions. So, there are no quick fixes available for the reported error and warnings in the Orion editor.</p>

<p>The editor integration is done using an Orion plugin. The plugin defines a named socket that is used to fire up the language server. The plugin is registered to be initialized on files with the following content types: <i>"text/x-java-source"</i> or <i>"application/x-jsp"</i>. These two content types are registered in the Orion plugin for files with extensions <i>‘.java’</i> and <i>‘.jsp’</i>. So, when the plugin is started and initialized, it triggers the connection for the named socket and sends the ‘start’ event. This event initializes the LSP server and enables the two sockets for bidirectional communication between the LSP server and the Orion client. The Orion plugin is also used to register the syntax highlighting for the Java language by using Orion stylers.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/code_orion_stylers.png" alt="Eclipse Orion Stylers"/></p>

<p>where mJava and mJSP are defined as:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/code_orion_stylers2.png" alt="Eclipse Orion Stylers"/></p>

<p>Each feature of the Orion editor like content assist, occurrences, etc. has been updated to check whether a LSP server is registered and needs to be handled.<p>

<p>For example, for the formatter, in the file called <code>org.eclipse.orion.client.ui/web/orion/formatter.js</code>, we check whether there is a LSP server registered for the current file content type. To do that, we register all LSP servers inside a registry that is used to speed up the lookup for LSP server based on a specific content type. If we find one, we use it by invoking the corresponding handler from the protocol. In this case, it would be either a document formatting request or a document range formatting request depending on if there is a current selection inside the editor.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/code_orion_formatter.png" alt="Eclipse Orion Formatter"/></p>

<p>We invoke this by doing:</p>

<p align="center"><a href="/community/eclipse_newsletter/2017/may/images/code_orion_formatters.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/code_orion_formatter2_sm.png" alt="Eclipse Orion Formatter"/></a></p>

<p>We use the same principle to implement all Orion editor features (occurrences, search references, etc.) that could take advantage of the LSP server features.</p>

<h2>Future directions</h2>

<p>We still need to add support for the code action request defined in the LSP to be able to add quick fixes inside the Orion editor.</p>

<p>Right now, the LSP server works fine on Maven and Gradle projects. This is still an area where things are moving fast. So, it is possible that more projects settings will be handled in the future.</p>

<p>We also need to see how we can leverage the support for the Java<sup>TM</sup> language to other languages like what is done with the generic <a href="https://www.eclipse.org/eclipse/news/4.7/M3/#generic-editor">Eclipse editor</a>.</p>

<p>Feel free to contribute to either the Orion integration or directly to the Eclipse Java LSP server implementation.</p>

<p>This is what you get once it is running:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/may/images/orion_lsp.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/orion_lsp_sm.png" alt="Eclipse Orion Editor"/></a></p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/may/images/olivier.png"
        alt="Olivier Thomann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Olivier Thomann<br />
            <a target="_blank" href="http://www.ibm.com/ca/">IBM</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/othomann">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

