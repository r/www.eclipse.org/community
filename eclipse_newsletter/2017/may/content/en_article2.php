<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/default.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/highlight.min.js"></script>
<h2>Introduction</h2>
<p>The <a href="https://github.com/Microsoft/language-server-protocol">Language Server protocol</a> (LSP) is a communication protocol between tools and compilers. The LSP enables compilers to expose their interfaces into a language agnostic fashion via so-called “language servers” and allows tools to consume new languages by connecting to these servers.</p>
<p><a href="https://github.com/eclipse/lsp4j">Eclipse LSP4J</a> supports the development and consumption of language servers in Java. More than that, it allows developers to define new JSON-RPC protocols, implement endpoints and exchange an underlying transport.</p>
<p>In this article, I will introduce you to core concepts of LSP4J with an example. We will define a new JSON-RPC protocol for a chat app, as well as implement a chat client and server talking over this protocol. We will use Java sockets for the transport layer and finally, we look at how these concepts can be applied to consume and provide a language server. You can find the source code of the example <a href="https://github.com/TypeFox/lsp4j-chat-app">here</a>.</p>
<h2>Defining a Simple Chat JSON-RPC Protocol</h2>
<p><a href="http://www.jsonrpc.org/specification">JSON-RPC</a> is a transport-independent remote procedure call protocol using JSON as a data format. A call is represented by sending request or notification messages from the client to the server. A new protocol is defined by declaring requests, notifications and types used by the messages. Below you can see definition of the simple chat protocol. It is defined using LSP4J service layer's annotations.</p>
<pre class="hljs"><code><span class="hljs-meta">@JsonSegment</span>(<span class="hljs-string">"server"</span>)
<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">interface</span> <span class="hljs-title">ChatServer</span> </span>{
    
    <span class="hljs-comment">/**
     * The `server/fetchMessage` request is sent by the client to fetch messages posted so far.
     */</span>
    <span class="hljs-meta">@JsonRequest</span>
    CompletableFuture&lt;List&lt;UserMessage&gt;&gt; fetchMessages();
    
    <span class="hljs-comment">/**
     * The `server/postMessage` notification is sent by the client to post a new message.
     * The server should store a message and broadcast it to all clients.
     */</span>
    <span class="hljs-meta">@JsonNotification</span>
    <span class="hljs-function"><span class="hljs-keyword">void</span> <span class="hljs-title">postMessage</span><span class="hljs-params">(UserMessage message)</span></span>;

}

<span class="hljs-meta">@JsonSegment</span>(<span class="hljs-string">"client"</span>)
<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">interface</span> <span class="hljs-title">ChatClient</span> </span>{
    
    <span class="hljs-comment">/**
     * The `client/didPostMessage` is sent by the server to all clients 
     * in a response to the `server/postMessage` notification.
     */</span>
    <span class="hljs-meta">@JsonNotification</span>
    <span class="hljs-function"><span class="hljs-keyword">void</span> <span class="hljs-title">didPostMessage</span><span class="hljs-params">(UserMessage message)</span></span>;

}

<span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">UserMessage</span> </span>{

    <span class="hljs-comment">/**
     * A user posted this message.
     */</span>
    <span class="hljs-keyword">private</span> String user;

    <span class="hljs-comment">/**
     * A content of this message.
     */</span>
    <span class="hljs-keyword">private</span> String content;

}
</code></pre>
<h2>Implementing Chat Client and Server Endpoints</h2>
<p>LSP4J allows bi-directional communication, meaning that each side can receive and send requests and notifications. Each side exposes its interface by implementing the <a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j.jsonrpc/src/main/java/org/eclipse/lsp4j/jsonrpc/Endpoint.java">Endpoint</a> interface.</p>
<p><code>Endpoint</code> is a minimal interface to handle JSON-RPC messages with generic
JSON elements. To provide type-safe and convenient interfaces, LSP4J uses typed service objects that reflectively delegate to the generic <code>Endpoint</code> API. A service interface is a plain Java interface that has methods annotated with <a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j.jsonrpc/src/main/java/org/eclipse/lsp4j/jsonrpc/services/JsonRequest.java">@JsonRequest</a> or <a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j.jsonrpc/src/main/java/org/eclipse/lsp4j/jsonrpc/services/JsonNotification.java">@JsonNotification</a> annotations.</p>
<p>Below you can find the two implementations of our service interfaces <a href="https://github.com/TypeFox/lsp4j-chat-app/blob/master/src/main/java/io/typefox/lsp4j/chat/server/ChatServerImpl.java">ChatServerImpl</a> and <a href="https://github.com/TypeFox/lsp4j-chat-app/blob/master/src/main/java/io/typefox/lsp4j/chat/client/ChatClientImpl.java">ChatClientImpl</a>.</p>
<pre class="hljs"><code><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">ChatServerImpl</span> <span class="hljs-keyword">implements</span> <span class="hljs-title">ChatServer</span> </span>{
    
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">final</span> List&lt;UserMessage&gt; messages = <span class="hljs-keyword">new</span> CopyOnWriteArrayList&lt;&gt;();
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">final</span> List&lt;ChatClient&gt; clients = <span class="hljs-keyword">new</span> CopyOnWriteArrayList&lt;&gt;();

    <span class="hljs-comment">/**
     * Return existing messages.
     */</span>
    <span class="hljs-meta">@Override</span>
    <span class="hljs-keyword">public</span> CompletableFuture&lt;List&lt;UserMessage&gt;&gt; fetchMessages() {
        <span class="hljs-keyword">return</span> CompletableFuture.completedFuture(messages);
    }

    <span class="hljs-comment">/**
     * Store the message posted by the chat client
     * and broadcast it to all clients.
     */</span>
    <span class="hljs-meta">@Override</span>
    <span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">postMessage</span><span class="hljs-params">(UserMessage message)</span> </span>{
        messages.add(message);
        <span class="hljs-keyword">for</span> (ChatClient client : clients) {
            client.didPostMessage(message);
        }
    }

    <span class="hljs-comment">/**
     * Connect the given chat client.
     * Return a runnable which should be executed to disconnect the client.
     */</span>
    <span class="hljs-function"><span class="hljs-keyword">public</span> Runnable <span class="hljs-title">addClient</span><span class="hljs-params">(ChatClient client)</span> </span>{
      <span class="hljs-keyword">this</span>.clients.add(client);
      <span class="hljs-keyword">return</span> () -&gt; <span class="hljs-keyword">this</span>.clients.remove(client);
    }

}
</code></pre>
<pre class="hljs"><code><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">ChatClientImpl</span> <span class="hljs-keyword">implements</span> <span class="hljs-title">ChatClient</span> </span>{
    
    <span class="hljs-keyword">private</span> <span class="hljs-keyword">final</span> Scanner scanner = <span class="hljs-keyword">new</span> Scanner(System.in);
    
    <span class="hljs-comment">/**
     * 1. Ask the user for a name
     * 2. Fetch existing messages from the remote server and display them
     * 3. Ask the user for a next message
     * 4. Post a new message to the chat server, continue with step 3
     */</span>
    <span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">start</span><span class="hljs-params">(ChatServer server)</span> <span class="hljs-keyword">throws</span> Exception </span>{
        System.out.print(<span class="hljs-string">"Enter your name: "</span>);
        String user = scanner.nextLine();
        server.fetchMessages().get().forEach(message -&gt; <span class="hljs-keyword">this</span>.didPostMessage(message));
        <span class="hljs-keyword">while</span> (<span class="hljs-keyword">true</span>) {
            String content = scanner.nextLine();
            server.postMessage(<span class="hljs-keyword">new</span> UserMessage(user, content));
        }
    }

    <span class="hljs-comment">/**
     * Display the posted message.
     */</span>
    <span class="hljs-meta">@Override</span>
    <span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">void</span> <span class="hljs-title">didPostMessage</span><span class="hljs-params">(UserMessage message)</span> </span>{
        System.out.println(message.getUser() + <span class="hljs-string">": "</span> + message.getContent());
    }

}
</code></pre>
<p>As you can see in the code above, the server needs to be connected to any chat clients that want to participate. With this, we could already build the chat system by simply creating an instance of the server and configuring it with any number of client objects. But of course, we want the clients and server to communicate over a remote connection.</p>
<h2>Launching the Chat Server</h2>
<p>We want to start a chat server over Java sockets, so first, we have to create an instance of <code>SocketServer</code> and accept a new socket connection. Once we have the connection, we need to turn it into an instance of <code>ChatClient</code> and pass it to the chat server. The <a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j.jsonrpc/src/main/java/org/eclipse/lsp4j/jsonrpc/Launcher.java">Launcher</a> class provides means to</p>
<ul>
<li>listen for incoming messages via <code>startListening</code> method;</li>
<li>test whether the connection is still opened based on the state of a <code>Future</code> returned by <code>startListening</code>;</li>
<li>and most importantly access a remote proxy of a desirable JSON-RPC interface, in our case <code>ChatClient</code>.</li>
</ul>
<pre class="hljs"><code><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">ChatServerLauncher</span> </span>{

    <span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">main</span><span class="hljs-params">(String[] args)</span> <span class="hljs-keyword">throws</span> Exception </span>{
        <span class="hljs-comment">// create the chat server</span>
        ChatServerImpl chatServer = <span class="hljs-keyword">new</span> ChatServerImpl();
        ExecutorService threadPool = Executors.newCachedThreadPool();

        Integer port = Integer.valueOf(args[<span class="hljs-number">0</span>]);
        <span class="hljs-comment">// start the socket server</span>
        <span class="hljs-keyword">try</span> (ServerSocket serverSocket = <span class="hljs-keyword">new</span> ServerSocket(port)) {
            System.out.println(<span class="hljs-string">"The chat server is running on port "</span> + port);
            threadPool.submit(() -&gt; {
                <span class="hljs-keyword">while</span> (<span class="hljs-keyword">true</span>) {
                    <span class="hljs-comment">// wait for clients to connect</span>
                    Socket socket = serverSocket.accept();
                    <span class="hljs-comment">// create a JSON-RPC connection for the accepted socket</span>
                    SocketLauncher&lt;ChatClient&gt; launcher = <span class="hljs-keyword">new</span> SocketLauncher&lt;&gt;(chatServer, ChatClient.class, socket);
                    <span class="hljs-comment">// connect a remote chat client proxy to the chat server</span>
                    Runnable removeClient = chatServer.addClient(launcher.getRemoteProxy());
                    <span class="hljs-comment">/*
                     * Start listening for incoming messages.
                     * When the JSON-RPC connection is closed
                     * disconnect the remote client from the chat server.
                     */</span>
                    launcher.startListening().thenRun(removeClient);
                }
            });
            System.out.println(<span class="hljs-string">"Enter any character to stop"</span>);
            System.in.read();
            System.exit(<span class="hljs-number">0</span>);
        }
    }

}
</code></pre>
<h2>Connecting to the Chat Server</h2>
<p>For the chat client, we have to establish a socket connection and then use <code>Launcher</code> to get a remote proxy for <code>ChatServer</code> that is used to start the chat session.</p>
<pre class="hljs"><code><span class="hljs-keyword">public</span> <span class="hljs-class"><span class="hljs-keyword">class</span> <span class="hljs-title">ChatClientLauncher</span> </span>{

    <span class="hljs-function"><span class="hljs-keyword">public</span> <span class="hljs-keyword">static</span> <span class="hljs-keyword">void</span> <span class="hljs-title">main</span><span class="hljs-params">(String[] args)</span> <span class="hljs-keyword">throws</span> Exception </span>{
        <span class="hljs-comment">// create the chat client</span>
        ChatClientImpl chatClient = <span class="hljs-keyword">new</span> ChatClientImpl();

        String host = args[<span class="hljs-number">0</span>];
        Integer port = Integer.valueOf(args[<span class="hljs-number">1</span>]);
        <span class="hljs-comment">// connect to the server</span>
        <span class="hljs-keyword">try</span> (Socket socket = <span class="hljs-keyword">new</span> Socket(host, port)) {
            <span class="hljs-comment">// open a JSON-RPC connection for the opened socket</span>
            SocketLauncher&lt;ChatServer&gt; launcher = <span class="hljs-keyword">new</span> SocketLauncher&lt;&gt;(chatClient, ChatServer.class, socket);
            <span class="hljs-comment">/*
             * Start listening for incoming message.
             * When the JSON-RPC connection is closed, 
             * e.g. the server is died, 
             * the client process should exit.
             */</span>
            launcher.startListening().thenRun(() -&gt; System.exit(<span class="hljs-number">0</span>));
            <span class="hljs-comment">// start the chat session with a remote chat server proxy</span>
            chatClient.start(launcher.getRemoteProxy());
        }
    }

}
</code></pre>
<h2>How is LSP4J then specific to the Language Server Protocol?</h2>
<p>The LSP part of LSP4J is actually a separated module and uses the very same mechanism:</p>
<ul>
<li><a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j/src/main/java/org/eclipse/lsp4j/services/LanguageServer.java">LanguageServer</a> interface should be implemented to provide a language server, similar to <code>ChatServerImpl</code>;</li>
<li><a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j/src/main/java/org/eclipse/lsp4j/services/LanguageClient.java">LanguageClient</a> interface should be implemented to provide a language client, similar to <code>ChatClientImpl</code>;</li>
<li><a href="https://github.com/eclipse/lsp4j/blob/master/org.eclipse.lsp4j/src/main/java/org/eclipse/lsp4j/launch/LSPLauncher.java">LSPLauncher</a> should be used to start a language server or connect a language client to an existing language server as shown below.</li>
</ul>
<pre class="hljs"><code>MyLanguageServer server = <span class="hljs-keyword">new</span> MyLanguageServer();
Launcher&lt;LanguageClient&gt; launcher = LSPLauncher.createServerLauncher(server, input, output);
server.setClient(launcher.getRemoteProxy());
launcher.startListening();
</code></pre>
<pre class="hljs"><code>MyLanguageClient client = <span class="hljs-keyword">new</span> MyLanguageClient();
Launcher&lt;LanguageServer&gt; launcher = LSPLauncher.createClientLauncher(client, input, output);
client.setServer(launcher.getRemoteProxy());
launcher.startListening();
</code></pre>
<h2>Conclusion</h2>
<p>Although LSP4J carries the LSP bit in its name, it is, in fact, a generic and very convenient JSON-RPC implementation for Java, that can be used for all JSON-RPC protocols. The LSP specific part is nicely separated, and you don't need to deal with that when working with your own protocols.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/may/images/anton.jpeg"
          alt="Anton Kosyakov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Anton Kosyakov<br />
            <a target="_blank" href="http://typefox.io/">TypeFox</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/akosyakov">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/akosyakov">Twitter</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
