<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>The Eclipse JDT Language Server (a.k.a. jdt.ls) is an open source Java language specific implementation of the <a target="_blank" href="https://github.com/Microsoft/language-server-protocol">Language Server Protocol</a>, incubating at the <a href="https://projects.eclipse.org/projects/eclipse.jdt.ls">Eclipse Foundation</a>.</p>

<p>From the <a target="_blank" href="https://github.com/Microsoft/language-server-protocol">Language Server Protocol</a> website:</p> 

<blockquote>The Language Server Protocol is used between a tool (the client) and a language smartness provider (the server) to integrate features like auto complete, goto definition, find all references and alike into the tool.</blockquote>

<p>The Language Server Protocol, an open source project under the MIT License, was originally developed by Microsoft while <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/wiki/Protocol-History">integrating OmniSharp and the TypeScript Server into the Visual Studio Code editor</a> with the goal to simplify new programming language integration into the editor. It was later developed independently and is now completely editor/IDE agnostic. The protocol specifies the messages must be exchanged in the JSON RPC v2.0 format, but doesn't mandate a specific transport mechanism or how the server should be implemented.</p>

<p>jdt.ls is written in Java and is basically a small, headless <a href="https://www.eclipse.org/jdt/">Eclipse JDT</a> distribution, providing comprehensive Java support to a wide array of clients.</p>
<p>The server part is itself built with the <a target="_blank" href="https://github.com/eclipse/lsp4j">Eclipse LSP4J</a> framework.</p>

<h2>How does it work?</h2>
<p>Basically, any editor/IDE (the client) compatible with the Language Server Protocol can start jdt.ls, which will initialize an Eclipse IDE workspace under the hood and will then import all the Java projects found in the current working directory of the client.</p>

<p>The client will then submit requests to the language server, as the user interacts with the Java files:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/sequence.jdt.ls.png" alt="Sequence Diagram"/>

<p>The server allows the client to provide users with:</p>
<ul>
 <li>As-you-type reporting of parsing and compilation errors
 <li>Code completion</li>
 <li>Code navigation</li>
 <li>Code lens (references)</li>
 <li>Code formatting</li>
 <li>Code outline</li>
 <li>Code actions (quick fixes)</li>
 <li>Highlights</li>
 <li>Javadoc hovers</li>
 <li>Type search</li>
</ul>

<h3>Build types</h3>
<p>The server looks for project/build descriptors in order to correctly configure the Java support (compiler level, classpath). The project import mechanism will lookup build descriptors in the following order:</p>
<ol>
	<li><b>Search for *.gradle files</b>: if a Gradle project is detected, the import will be delegated to <a href="https://github.com/eclipse/buildship">Eclipse Buildship</a>.</li>
	<li><b>Search for pom.xml files</b>: if a Maven project is detected, the import will be delegated to <a href="https://www.eclipse.org/m2e/">Eclipse m2e</a>.</li>
	<li><b>Search for Eclipse IDE .project files</b>: the projects will be imported as simple Eclipse IDE projects.</li>
</ol>

<p>Depending on the project type, some specific behavior can be expected:</p>

<ul>
<li><b>Gradle</b></li>
	<ul>
		<li>sources of dependencies are automatically downloaded.
		<li>changes to build.gradle dependencies require an explicit project configuration update.</li>
		<li>please note that Android projects are not supported at the moment.</li>
	</ul>
<li><b>Maven</b></li>
	<ul>
		<li>sources of dependencies are automatically downloaded.</li>
		<li>Maven errors are reported on the pom.xml.</li>
		<li>Changes to pom.xml dependencies automatically updates the projects classpath.</li>
	</ul>
<li><b>Eclipse IDE</b></li>
	<ul>
		<li>please note that WTP based projects can not be configured properly, as the JLS is missing all the WTP plugins</li>
	</ul>
</ul>

<h3>Standalone files</h3>
<p>Getting informations on a single java file, out of any project context is a bit tricky. The Java tooling has no information about what would be the classpath required to compile the file. So, for that specific use case, type checking errors are silenced, as it's not useful to let clients displays dozens of compilation errors. However the server can still analyze the file for syntax errors, or provide assistance for at least the default JDK classes.</p>


<h2>Supported Language Server Protocol features</h2>
<p>The current implementation of jdt.ls supports most of <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md">Language Server Protocol v3</a>, as described below:</p> 

<table class="table table-striped">
  <tr>
    <th><p align="center">Message</p></td>
    <th><p align="center">Supported</p></td> 
  <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#initialize">Initialize</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center">  &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#shutdown">shutdown</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#exit">exit</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#cancelRequest">$/cancelRequest</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#11013; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#window_showMessage">window/showMessage</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8618; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#window_showMessageRequest">window/showMessageRequest</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#11013; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#window_logMessage">window/logMessage</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#11013; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#telemetry_event">telemetry/event</a></p></td>
    <td><p align="center">&#10008;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#window_didChangeConfiguration">window/didChangeConfiguration</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#window_didChangeWatchedFiles">window/didChangeWatchedFiles</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#11013; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_publishDiagnostics">textDocument/publishDiagnostics</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_didChange">textDocument/didChange</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_didClose">textDocument/didClose</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_didOpen">textDocument/didOpen</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#10145; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_didSave">textDocument/didSave</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_completion">textDocument/completion</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#completionItem_resolve">completionItem/resolve</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_hover">textDocument/hover</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_signatureHelp">textDocument/signatureHelp</a></p></td>
    <td><p align="center">&#10008;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_references">textDocument/references</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_documentHighlight">textDocument/documentHighlight</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_documentSymbol">textDocument/documentSymbol</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_formatting">textDocument/formatting</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_rangeFormatting">textDocument/rangeFormatting</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_onTypeFormatting">textDocument/onTypeFormatting</a></p></td>
    <td><p align="center">&#10008;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_definition">textDocument/definition	
    </a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_codeAction">textDocument/codeAction</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_codeLens">textDocument/codeLens</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#codeLens_resolve">codeLens/resolve</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_documentLink">textDocument/documentLink</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#documentLink_resolve">documentLink/resolve</a></p></td>
    <td><p align="center">&#10004;</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/blob/master/versions/protocol-2-x.md#textDocument_rename">textDocument/rename</a></p></td>
    <td><p align="center">&#10008;</p></td> 
  </tr>
</table>

<p>But jdt.ls also enhances the default Language Server Protocol by adding specific messages:</p>

<table class="table table-striped">
  <tr>
    <th><p align="center">Message</p></td>
    <th><p align="center">Description</p></td> 
  <tr>
    <td><p align="center"> &#11013; language/status</p></td>
    <td><p align="center">The status notification is sent to the client to request the client to display a particular message in the user interface. This is typically used during server startup</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#11013; language/actionableNotification</p></td>
    <td><p align="center">An actionable notification is sent to the client to display a particular message in the user interface with possible commands to execute. The commands must be implemented on the client side.</p></td> 
  </tr>
   <tr>
    <td><p align="center"> &#8617; java/classFileContents</p></td>
    <td><p align="center">The client requests the contents of a .class file URI, the server returns the source contents, if available.</p></td> 
  </tr>
    <tr>
    <td><p align="center"> &#10145; java/projectConfigurationUpdate</p></td>
    <td><p align="center">The client requests a project configuration update, i.e. to synchronize the jdt.ls internal project model with the project build descriptor (Maven or Gradle)</p></td> 
  </tr>
 </table>

<h2>Integrating with jdt.ls</h2>

<h3>Installation</h3>
<p>First of all, make sure you have a JDK 8 to run the server.</p>

<p>The latest jdt.ls compressed binary can be downloaded from the <a href="http://download.eclipse.org/jdtls/snapshots/?d">download area</a>. There's a stable link to the latest build:</p> 

  <a href="http://download.eclipse.org/jdtls/snapshots/jdt-language-server-latest.tar.gz">http://download.eclipse.org/jdtls/snapshots/jdt-language-server-latest.tar.gz</a>

<p>You then need to decompress the server tar.gz to some location of your choice.</p>

<h3>Setting up the connections</h3>

<p>jdt.ls supports sockets, named pipes, and standard streams of the server process to communicate with the client. A client can communicate its preferred connection methods by setting up environment variables.</p>

<ul>
	<li>The standard streams(stdin, stdout) of the server process are used by default.</li>
	<li>To use name pipes set the following environment variables before starting the server.</li>
</ul>

<pre>
STDIN_PIPE_NAME --&gt; where client reads from
STDOUT_PIPE_NAME --&gt; where client writes to
</pre>

<ul>
	<li>To use plain sockets set the following environment variables before starting the server.</li>
</ul>

<pre>
STDIN_PORT --&gt; client reads
STDOUT_PORT --&gt; client writes to
</pre>

<p>Optionally you can set host values for socket connections.</p>

<pre>
STDIN_HOST
STDOUT_HOST
</pre>

<p>For socket and named pipes the client is expected to create the connections and wait for server the connect.</p>

<h3>Running the server</h3>

<p>Once the connection environments have been set, jdt.ls can be run by executing:</p>

<pre>
/path/to/jdk/java -Declipse.application=org.eclipse.jdt.ls.core.id1 / 
    -Dosgi.bundles.defaultStartLevel=4 /
    -Declipse.product=org.eclipse.jdt.ls.core.product /
    -noverify -Xmx1G -XX:+UseG1GC -XX:+UseStringDeduplication /
    -jar /path/to/server/plugins/org.eclipse.equinox.launcher_&lt;version&gt;.jar / 
    -configuration /path/to/server/config_mac /
    -data /path/to/some/workspace/
</pre>

<ul>
<li>You need to replace <code>server/plugins/org.eclipse.equinox.launcher_&lt;version&gt;.jar</code> with the actual name of the org.eclipse.equinox.launcher jar</li>
<li>The <code>configuration</code> flag can point to either:</li>
	<ul>
		<li>config_win, for Windows</li>
		<li>config_mac, for MacOS</li>
		<li>config_linux, for Linux</li>
	</ul>
<li>The <code>data</code> flag value should be the absolute path to the working directory of the server. This should be different from the path of the user's project files (which is sent during the <code>initialize</code> handshake).</li>
</ul>

<h3>Known clients</h3>
<p>The Developer Tooling community largely embraced the Language Server Protocol paradigm and several initiatives were started, aiming at integrating jdt.ls in other editors/IDEs:</p>
<ul>
<li><a target="_blank" href="https://github.com/redhat-developer/vscode-java/">Visual Studio Code</a>, more below.</li>
<li><a target="_blank" href="https://github.com/eclipse/orion.client/tree/java-lsp3">Eclipse Orion</a>, see a <a target="_blank" href="https://www.youtube.com/watch?v=A-jnqySRA50">demo screencast</a>.</li>
<li><a href="http://eclipse.org/che">Eclipse Che</a></li>
<li><a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls/pull/170#issuecomment-284246112">Vim</a></li>
<li><a target="_blank" href="https://github.com/emacs-lsp/lsp-java">Emacs</a></li>
<li><a target="_blank" href="https://github.com/atom/languageserver-java">Atom</a></li>
</ul>

<h2>Visual Studio Code and the Language Support for Java extension</h2>
<p>The Red Hat team, participating in the jdt.ls project, is also actively developing the <code>Language Support for Java</code> extension for Visual Studio Code (a.k.a. <a target="_blank" href="https://github.com/redhat-developer/vscode-java/">vscode-java</a>, as a way to validate the jdt.ls implementation. This <a target="_blank" href="https://github.com/redhat-developer/vscode-java/">extension</a> is fairly popular, in the <a target="_blank" href="https://marketplace.visualstudio.com/search?target=VSCode&category=All%20categories&sortBy=Downloads">top 20</a> of the most installed extensions for VS Code.</p>

<p>You can install it from the Visual Studio Marketplace, or from the command palette:</p>

<p><code>ext install java</code></p>

<p>The following animation gives you an overview of the Java support in VS Code:</p>

	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/may/images/vscode-java.0.0.1.gif"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/vscode-java.0.0.1.gif" alt="java support vs code"/></a></p>

<p>During Devoxx.us 2017, the Red Hat team presented jdt.ls and how to develop a Spring Boot based application with VS Code:</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/l96vUqfB3R4" frameborder="0" allowfullscreen></iframe>
</div>

<h2>Conclusion</h2>
<p>The Eclipse JDT Language Server is already a really exciting piece of technology, but we want to make it even better. Our goal is to provide comprehensive refactoring capabilities, as well as a Java debugger. So stay tuned!</p>

<p>Both the jdt.ls and vscode-java are developed under the open source <a href="https://www.eclipse.org/legal/epl-v10.html">Eclipse Public License v1.0</a>. All contributions are welcome, whether it’s code, feedback, bug reports. Please do so under any of these GitHub repositories:</p>

<ul>
	<li>Eclipse JDT Language Server: <a target="_blank" href="https://github.com/eclipse/eclipse.jdt.ls">https://github.com/eclipse/eclipse.jdt.ls</a></li>
	<li>vscode-java: <a target="_blank" href="https://github.com/redhat-developer/vscode-java/">https://github.com/redhat-developer/vscode-java/</a></li>
</ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/may/images/fred.jpeg"
        alt="Fred Bricon" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Fred Bricon<br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fbricon">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/fbricon">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/may/images/gorkem.jpeg"
        alt="Gorkem Ercan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Gorkem Ercan<br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          	<li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/GorkemErcan">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/gorkem">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
