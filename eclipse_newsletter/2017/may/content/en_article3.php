<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Now, you’re well aware about the benefits of the Language Server Protocol (LSP) to ship reusable edition smartness for your language, and of how LSP4J allows to very easily interact with Language Servers in Java, you probably figured out that the next natural step for the Eclipse Community has been to make the Eclipse IDE able to consume language servers in order to provide typical rich editor features (Completion, Hover, Outline, Problems/Markers, Quick-fixes…)  and interact with them from the Eclipse IDE as usual, without having to learn anything new as a user. This is the purpose of the <a href="https://projects.eclipse.org/projects/technology.lsp4e">Eclipse LSP4E</a> project, which already covers most of the features of language servers and already shows a nice list of adopters.</p>

<h2>For end-users: nothing changes</h2>

<p>End-users shouldn’t have to do anything different to work with language servers. Whether a language support tool uses a language server or not is a blackbox to them, they simply have to install language support as they usually do from Marketplace or anywhere else. Then, just open an Eclipse editor for their file and the LSP features are automatically enabled and bound to the typical shortcuts, commands, views… that have been used successfully for a while by JDT, CDT, PDT and most Eclipse editors.</p>

<p>The language support would typically associate Eclipse Platform’s generic editor (which is extended with features coming from LSP4E and Language Server) with the right files. If the association is missing and you find another editor is associated, you’re still free to use the Generic Editor from the <code>Open With</code> contextual menu to take advantage of all LSP4E features, and to associate the Generic Editor with some filetype.</p>

<p>End-users are given the ability to add a Language Server and to connect to it dynamically via Preferences if they need or want to. But it’s not the main user-story so we’ll skip it as part of this article and invite anyone interested to come and ask on the <a href="https://dev.eclipse.org/mailman/listinfo/lsp4e-dev">LSP4E mailing-list</a>.</p>

<h2>For Eclipse plugins or Language Servers developers: an extension point, a basic interface to implement, and packaging</h2>

<p>Plugin developers willing to add editing features for a given language should consider the adoption of Language Servers as one of the best options. One of the reasons being that Language Servers are reusable across various editors and help the language to easily reach a diverse set of clients. So first things first, plugin developers interested in language support should get familiar with the protocol and if applicable, should consider implementing this protocol for their language (using LSP4J for instance).</p>

<p>As this is more a topic general to the Language Server Protocol and LSP4J, I’ll let you refer to those articles in order to create your language server; and I’ll only focus on how to bind an existing Language Server into the Eclipse IDE here.</p>

<p>The most efficient approach so far consists of using the Eclipse Platform’s <a href="https://www.eclipse.org/eclipse/news/4.7/M3/#generic-editor">Generic Editor</a> for your files, to which LSP4E is already contributing; and then to define how to start/connect to the language server, and finally to bind the language server declaration to the target content-types.</p>

<p>Let’s have a more detailed look into this using the aCute example, which brings C# edition into Eclipse IDE.</p>

<p>First, the plugin does define as extension in plugin.xml a content-type for the target files and associate this content-type with the Generic Editor:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/image1.png" alt="define content type"></p><br>

<p>Then, the plugin defines a Language Server and associates it with the content-type:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/image2.png" alt="define language server"></p><br>

<p>The Language Server we use for C# is OmniSharp, hence the names in plugin.xml. From there, the only missing piece is to implement "org.eclipse.acute.OmnisharpStreamConnectionProvider". This class is the one responsible for providing an input and an output stream to chat with the language server (as it’s a request/response/notification communication pattern). In this case, the decision was to have the class also startup the language server, but some other stories may simply connect to a server that is already running. The of the server lifecycle is up to the adopter according to the specificities of the language server.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/may/images/image3.png" alt="connecting to language server"></p><br>

<p>This simply runs a command like <code>/usr/bin/node /path/to/omnisharp/server/languageserver.js</code> from inside the IDE and connect to the streams of this process to send requests and notifications conforming to LSP specification. LSP4E takes care of everything else, so with this ~100 lines of simple code, you have rich editior support for C# in Eclipse IDE!</p>

<p>As defining the connection of Eclipse IDE to the Language Servers is almost trivial, more care should be taken by the plugin developers on <b>how to package the Language Server</b>. This is indeed an important and tricky question and the answer strongly depend on the Language Server: for some which are available a CLI and part of some official SDK you expect your users to be using, you can simply reference the CLI; for some others who are not distributed as CLI tool, you may have to embed them in some bundle (it’s the case for the example above), some language servers may be remote and can use streams over TCP or WebSocket&hellip; There is not one which is strongly better than the other, and the choice depends almost solely on how the Language Server is distributed.</p>

<h2>Current adopters and success stories</h2>
<p>LSP4E and the Language Server architecture are already adopted by a few projects, some of which already deliver a better user experience than traditional Eclipse editors.Here is the list of Eclipse IDE plugins providing editors using LSP4E:</p>
<ul>
	<li><a target="_blank" href="https://marketplace.eclipse.org/content/acute-c-edition-eclipse-ide-experimental">aCute</a>: C# development with OmniSharp Language Server</li>
	<li><a target="_blank" href="https://marketplace.eclipse.org/content/bluesky-web-development-eclipse-ide-experimental">BlueSky</a>: Web (HTML, CSS, JSon with schema, JavaScript, TypeScript) edition with VSCode Language Servers and SourceGraph JSTS Language Server</li>
	<li><a target="_blank" href="https://github.com/eclipselabs/lsp4e-php">Lsp4e-php</a>: PHP editing with Soucegraph’s PHP Language Server with contributions from Zend, who implement Lsp4e-php.</li>
	<li>Fabric8 dependency analysis: adds edition assist in pom.xml and package.json to prevent using versions with known security issues, using <a target="_blank" href="https://github.com/fabric8-analytics/fabric8-analytics-lsp-server">Fabric8 dependency analysis Language Server</a> and OpenShift.io</li>
	<li>Your project? If so come to share it with us on the <a target="_blank" href="https://dev.eclipse.org/mailman/listinfo/lsp4e-dev">LSP4E mailing-list</a>!</li>
</ul>
<p>Note: most of the examples above do rely on TextMate grammars and <a href="https://projects.eclipse.org/projects/technology.tm4e">Eclipse TM4E</a> for syntax highlighting, since the Language Server Protocol doesn’t include support for syntax highlighting.</p>

<h2>Contribute</h2>
<p>All the layers of this architecture (Eclipse Platform, LSP4J, LSP4E&hellip;) and all examples (including the language servers) listed here are open source and welcome contributions. Here is a good entry-point to <a href="https://projects.eclipse.org/projects/technology.lsp4e/developer">start contributing to LSP4E</a>. We can’t wait to review your patches!</p>

<h2>Learn more</h2>
<p>I'll be presenting all that in greater detail at <a target="_blank" href="https://www.eclipsecon.org/france2017/session/language-server-protocol-action-c-eclipse-ide-java-vscode-your-language-everywhere">EclipseCon France</a> in Toulouse at the end of June. Join me to have a chat about LSP!</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/may/images/mickael.jpeg"
          alt="Mickael Istria" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Mickael Istria<br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/mickaelistria">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mickaelistria">Twitter</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>