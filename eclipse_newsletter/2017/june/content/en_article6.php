<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	<p>It’s always pleasing to be able to compile a “what’s new” overview that contains features that the community have been asking for. Which is why we think you’ll like <a href="">Eclipse Jubula</a> Oxygen edition: the new version lets you write more complex tests using conditions and loops, and lets you conditionally skip test steps. We’ve also put some work into helping people who have larger test projects - we’ve improved the navigation in tests and have a new way of cleaning up dead test code.</p>
	
	<h2>Sometimes it’s complicated</h2>
 
<p>I’m the first person to advocate for deterministic tests that run the same each time. Otherwise, it’s hard to tell whether a green test is still green for the right reasons. Nevertheless, some test scenarios require conditionality and loops. And that’s why we’ve introduced four new nodes: <i>if-then-else, do-while, while-do and repeat</i>.</p>

	<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/jubula_add.png"></p>
        

<p>This is something that we’ve very explicitly not implemented in the past, because of the inherent danger of making tests over-complicated and unreadable. We still see that danger, but we also see that there are some examples where having these constructs is going to make tests more readable, or certain more complex scenarios easier to automate. As always, with great power comes great responsibility ;)</p>
 
<p>Some of the cool examples that you’ll be able to implement now are:</p>

<h3>“If a confirmation dialog is present, then click OK, otherwise do nothing”</h3>

<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/occasional_dialog.png"></p>

<p>This was previously (and still is) possible using retry event handlers, but the conditions mean that it is easier to specify and to read.</p>
        
  <h3>“While there are tabs open, close the first tab”</h3>
  
  <p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/close_tabs.png"></p>
	
<p>The <i>do-while</i> loop works in the same way, but performs the action(s) in the <i>do-block</i> before the first check.</p>
 
<p>Conditions and loops are negatable (for those who can get their head around that!), and the <i>true</i> or <i>false</i> value of each condition is based on the outcome of the check actions within that block.</p>

<h3>“Create 5 projects”</h3>
	<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/create_project.png"></p>

<p>The repeat node allows you to enter an amount of times to repeat the do-block. You can enter concrete values, parameters, references and functions.</p>


<h2>Easier synchronization</h2>
<p>One aspect that could previously make tests unnecessarily complicated was dynamic synchronisation. Jubula does offer a great deal of “wait for” actions, but sometimes it’s necessary to e.g. wait until a text has changed / a list item has appeared. To make these actions more streamlined, we’ve added the parameter “TIMEOUT” to each check action. The default is 0ms, so existing tests will still run. But now you can choose to set a timeout for any check action. If the check fails on the first try, it will be retried automatically until the timeout has expired. The polling interval for the retries is configurable via a variable.</p>
	
	<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/sync_test.png"></p>
        
        
<h2>Skipping individual test steps</h2>
<p>With Jubula’s focus on reusability, the question has often come up how keywords can be created that variably execute all or just some of the steps contained in them. Our test consultants at BREDEX use this pattern frequently – they create a keyword that will e.g. fill out all the fields in a form / wizard page. Depending on the test case this is used in, it might only be relevant (or possible) to fill out the mandatory fields. In Eclipse Oxygen, you can use the parameter value ##jbskip## for any of the parameter values for a test step. The test step will simply not be executed. This reduces the amount of duplication required to fill out fields in forms.</p>

	<p align="center"><img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/test_results.png"></p>
        
<p>In the test result report, you can see which steps have been skipped.</p>

<h2>Navigating in tests</h2>
<p>A while ago, we gathered some feedback from users and found that navigating in the ITE could be more comfortable in some places. Two new options are available in Oxygen:</p>
	<ul>
		<li>Double-clicking on a root node in the Test Case Editor jumps to the open editor where this Test Case is reused. This is really useful for navigating “back up” the hierarchy when you have multiple editors open.</li>
		<li>For users working with the function <i>getCentralTestDataSetValue</i>, it’s now possible to navigate between the properties view and the central test data sets view (and back again) to quickly see where a value is referenced, and which value is actually used in the function. This feature should save users a lot of clicking and scrolling.</li>
	</ul>
	
<h2>Getting rid of dead test code: delete with orphans</h2>
<p>The new feature “delete with orphans” lets you delete an unused Test Case (that you’ve found for example using the filter to show unused Test Cases) – and all of the Test Cases in it that will become unused once their parent is deleted. This works recursively as well, so you can clean up your unused Test Cases quickly and easily.</p>

<h2>Other cool stuff</h2>
<p>We’ve also made two improvements to test result reports – first of all, they can now also be exported in JUnit report format to make it easier to integrate them into your continuous build and integration process. Secondly, screenshots taken on error now highlight the component that was under test to make it easier to identify what the problem could be.</p>
 
<p>In terms of the Client API for writing tests in Java, it’s often useful to have a little more grey-box testing – with the new actions <i>getProperty</i> and <i>isSelected</i> this becomes easier. <i>getProperty</i> lets you access properties of a component, and <i>isSelected</i> can be used to find out the toggle state of a checkbox or radio button.</p>
 
<p>As always, more information is in the release notes - we hope you like working with Oxygen!</p>
	
<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/december/images/alex.jpg"
        alt="Alexandra Schladebeck" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Alexandra Schladebeck
            <br />
            <a target="_blank" href="https://www.bredex.de/">Bredex</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/alex_schl">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.schladebeck.de/">Website</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

