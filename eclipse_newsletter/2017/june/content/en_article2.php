<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

    <h1 class="article-title"><?php echo $pageTitle; ?></h1>

	<p>The Eclipse Oxygen release was yet another productive year for the <a href="https://www.eclipse.org/pdt/">Eclipse PHP Development Tools (PDT)</a> project. During the Oxygen development cycle (July 2016 – June 2017) the PDT project had released feature versions for every update of the Eclipse Simultaneous Release: PDT 4.1 with Neon.1, PDT 4.2 with Neon.2 , PDT 4.3 with Neon.3 and PDT 5.0 with Oxygen due June 28, 2017.</p>
<p>In these four releases we introduced support for PHP 7.1, tools for Composer, PHPUnit and PHP profiling, an integration with the Built-in PHP Server, and more. Each of them will be covered in details in the next lines.</p>
<p>Last but not least, during the last year we welcomed two new committers to the PDT project – great news for the project’s diversity and future.</p>
	

<h2>PHP 7.1</h2>
<p>The initial PHP 7.1 support was introduced in PDT 4.1 and was completed in PDT 4.3. The code editor supports all the <a target="_blank" href="https://devzone.zend.com/7459/php-7-1-released-🎉/">new cool features in PHP 7.1</a>, including:</p>
	<ul>	
		<li>Nullable types</li>
		<li>Void return type</li>
		<li>Iterable pseudo-type</li>
		<li>Class constant visibility modifiers</li>
		<li>Square bracket syntax for <code>list()</code></li>
		<li>Ability to specify keys in <code>list()</code></li>
		<li>Catching multiple exceptions types</li>
	</ul>
	
	<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php5.png" alt="php exceptions" /></a></p>
          
    <h2>Composer</h2>
<p><a target="_blank" href="https://getcomposer.org/">Composer</a> is one of the most popular tools for PHP developers nowadays. A Composer plugin existed for years in the <a target="_blank" href="http://p2.pdt-extensions.org/">PDT Extension Group</a> project. It has been contributed to Eclipse PDT and since then a lot of additional improvements have been done on it.</p>
<p>At present, in PDT 5.0, the Composer tooling provides two new project wizards: “Empty Composer Project” and “PHP Project from Composer Package”.</p>

	<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php4.png" alt="php exceptions" /></a></p>
          
         
	<p>The “PHP Project from Composer Package” wizard is particularly useful when starting new project from the skeleton of a PHP framework like Laravel, Symfony and Zend Framework. It acts in the same way as if the <code>'composer create-project'</code> command has been executed.</p>
	
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php12.png" alt="execution stats" /></a></p> 
          
          <p>The output of the Composer tooling is printed in the Console view. The Console view integrates the Terminal component, which allows displaying the output in a native way – with the nice ANSI coloring and progress display.</p>
          
	<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php8.png" alt="new project composer package php" /></a></p>
          
          <p>Interactive user input is also supported for those situation where Composer requires some input from the user.</p>
          
     <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php7.png" alt="composer workspace php" /></a></p>
          
          <p>When dependencies are installed, the Composer tooling scans the “autoload” section of the project’s composer.json file and adds all declared namespaces to the project’s build path. The same is done recursively for all project’s dependencies. This way all autoloading classes are indexed and available to the PDT intellisense features.</p>
          
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php11.png" alt="composer workspace installation type php" /></a></p>     
          
          <p>A verbose graphical multi tab editor is provided for the composer.json file. It makes it easier to embrace all the configuration options. Adding new dependencies is easier as the editor queries the Packagist API to list the available Composer packages and their versions. Handy buttons in the editor’s toolbar can be used to install and update the selected dependencies.</p>
	
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/php13.png"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php13_sm.png" alt="lavarel properties" /></a></p>
          
          <p>For those of you who prefer working with the composer.json in text mode, the last tab provides a source view of the file that embeds the JSON editor, which provides intellisense according to the Composer’s JSON schema: code assist, syntax validation, hover info, etc.</p>
          
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php3.png" alt="composer json" /></a></p> 
          
         <h2>PHPUnit</h2>
<p><a target="_blank" href="https://phpunit.de/">PHPUnit</a> is another popular tool that PDT 5.0 introduces integration with. The PHPUnit tooling was a feature of the <a target="_blank" href="http://www.zend.com/en/products/studio">Zend Studio</a> commercial PHP IDE for long years and is now contributed to PDT. As part of the contribution, the integration has been updated to the latest major release of PHPUnit – 6.x.</p>

<p>Running PHPUnit tests is as simple as calling “Run As &rarr; PHPUnit Test” on the project, a folder, or a specific test file. The test results are displayed in the designated PHPUnit view. It gives an overview of all tests: passed or failed. A detailed Trace Failure view is available for each failed test, which makes easier to determine the reason for the test failure.</p>
         
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php9.png" alt="trace failure" /></a></p> 
          
<p>Debugging PHPUnit tests can be easily done by running them with “Debug As &rarr; PHPUnit Test”. Breakpoints placed in the tests will pause the execution and allow step-debugging with the PHP debugger (Xdebug or Zend Debugger).</p>
<p>New test cases and test suites can be easily created by using the “PHPUnit Test Case” and “PHPUnit Test Suite” file wizards.</p>
          
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php10.png" alt="wizard php unit" /></a></p> 
          
<h2>PHP Profiling</h2>
<p>Profiling is not a common development task. But when it comes to resolving performance issues, profiling the code execution is a proven way to detect the bottlenecks.</p>
<p>The PHP profiling feature introduced in PDT 5.0 is another contribution from Zend Studio. It allows code profiling of:</p>
	<ul>
		<li>PHP scripts</li>
		<li>PHP web apps</li>
		<li>PHPUnit tests</li>
	</ul>
	
<p>Simply use the Profile As submenu from the Project Explorer’s context menu and choose the desired profile action.</p>
<p>The results are displayed in a designated PHP Profile perspective, which contains several views with profiling information.</p>
<p>The “Profile Information” view provides general information on the profiling duration and date, number of files constructing the requested URL and more. In addition, it displays a Time Division pie chart for the files in the URL.</p>
          
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/php2.png"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php2_sm.png" alt="lavarel properties" /></a></p>
          
<p>The “Execution Statistics” view displays the list of files that were called during the profiling process and detailed information on processing times for elements within the files. The window contains statistics relevant to each element as follows:</p>
	<ul>
		<li><b>Function</b> - The name and location of the function.</li>
		<li><b>Calls Count</b> - The number of times that the function was called.</li>
		<li><b>Average Own Time</b> - The average duration without internal calls.</li>
		<li><b>Own Time(s)</b> - The net process duration without internal calls.</li>
		<li><b>Others Time(s)</b> - Time spent on calling other files.</li>
		<li><b>Total Time(s)</b> - The total time taken to process.</li>
	</ul>
          
          
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php14.png" alt="execution stats" /></a></p> 
          
<p>The “Execution Flow” view shows the flow of the execution process and summarizes percentages and times spent on each function.</p>
	<ul>
		<li><b>Function</b> - Function name.</li>
		<li><b>File</b> - The file in which the function is located.</li>
		<li><b>Total Execution Time</b> - Percent of time taken per function.</li>
		<li><b>Duration Time</b> - Time taken per function in milliseconds.</li>
    </ul>      
          
    <p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php6.png" alt="execution stats" /></a></p>
	
	
	<p>Right-clicking a function in the list gives you the option to:</p>
	<ul>
		<li>View Function Call - Will open the selected function call in the editor.</li>
		<li>View Function Declaration - Will open the selected function declaration in the editor.</li>
		<li>Open Function Invocation statistics - Will open a view with statistics about the selected function, the functions it was invoked by and functions it invoked.</li>
	</ul>
	
<p>Currently, it is required to have Zend Debugger configured and enabled in the PHP runtime. Xdebug is not supported yet.</p>


<h2>Built-in PHP Server</h2>
<p>The <a target="_blank" href="http://php.net/manual/en/features.commandline.webserver.php">built-in web server</a> is a feature of PHP that allows to quickly launch a lightweight web server for development purposes like testing and debugging.</p>

<p>Thanks to <a target="_blank" href="https://accounts.eclipse.org/users/ibazzi">Alex Xu</a> – our latest PDT committer – PDT 5.0 now introduces an integration of the built-in PHP server with the server framework of the Eclipse Web Tools Platform (WTP). Those of you, who are familiar with the WTP Servers workflow, will find the same way of running and debugging web apps written in PHP – even better – without the need to setup a PHP web server!</p>
<p>For those of you who are new to the WTP Servers workflow, here is how it works:</p>
	<ol>
		<li>Right-click on the PHP project or a PHP file and call “Run As &rarr; Run On Server”.</li>
		<li>Select the PHP Built-in Server from the list of available server types and click Next.</li>
		<li>Pick a PHP executable to be used as the runtime for the built-in server.</li>
		<li>Click Finish to have the server started and the PHP project published.</li>
		<li>The internal web browser will appear showing the requested project or file.</li>
	</ol>
<p>In a similar way you can debug on the built-in server if the selected PHP executable has Xdebug or Zend Debugger configured. Just use “Debug As &rarr; Debug On Server” instead of “Run As &rarr; Run On Server”.</p>
<p>The built-in PHP server can be managed from the Servers view. You can start, stop and restart it, add and remove PHP projects. Double-clicking on the server’s node opens the server editor with some configuration options, e.g. setting the server’s HTTP port.</p>
 
	
	<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/php1.png" alt="php built-in server" /></a></p>
          
   <h2>And more…</h2>
<p>We've just seen the a selection of the major new features and improvements. There are many more big and small improvements included in the Oxygen release like:</p>
	<ul>
		<li>New mechanism for organizing use statements: sorting, removing of unused and adding unimported use statements</li>
		<li>Wizards for creating PHP classes, interfaces and traits</li>
		<li>Wizard for generation setter and getter methods</li>
		<li>Improved PHP syntax check and validation</li>
		<li>Improved structure of the PHP preferences</li>
	</ul>
<p>Visit the New & Noteworthy pages of <a href="https://wiki.eclipse.org/PDT/NewIn41">PDT 4.1</a>, <a href="https://projects.eclipse.org/projects/tools.pdt/releases/4.2/bugs">PDT 4.2</a>, <a href="https://wiki.eclipse.org/PDT/NewIn43">PDT 4.3</a> and <a href="https://wiki.eclipse.org/PDT/NewIn50">PDT 5.0</a> for the complete list of changes. The PDT team closed more than 250 Bugzilla items in the last 12 months.</p>

<h2>How to get it</h2>
<p>There are many ways for getting the latest Eclipse PDT tools:</p>
	<ul>
		<li>Download the “Eclipse for PHP Developers” package from the <a href="https://www.eclipse.org/downloads/eclipse-packages/">Eclipse Downloads</a> page</li>
		<li>Use the <a href="https://wiki.eclipse.org/Eclipse_Installer">Eclipse Installer</a> to install the “Eclipse IDE for PHP Developers” profile</li>
		<li>Extend your existing Eclipse installation by installing the “PHP Development Tools (PDT)” feature from:</li>
		<ul>
			<li>The <a href="https://marketplace.eclipse.org/content/php-development-tools">Eclipse Marketplace</a></li>
			<li>The Eclipse Oxygen Simultaneous Release <a href="http://download.eclipse.org/releases/oxygen/">software repository</a></li>
		</ul>
	</ul>
		
<h2>Contact us</h2>
<p>We want to hear from you! You can reach us in our <a href="https://eclipse.org/forums/index.php?t=thread&frm_id=85">user forum</a> and in our <a href="https://dev.eclipse.org/mailman/listinfo/pdt-dev">developers mailing list</a>. Tell us what you like and what not in Eclipse PDT.</p>
<p>Please report bugs in the project's <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=pdt">Bugzilla</a>.</p>
<p>Happy PHPing!</p>         
	
	
<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/kaloyan.jpeg"
          alt="Kaloyan Raev" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Kaloyan Raev<br />
            <a target="_blank" href="http://www.zend.com/en/products/studio">Zend Studio</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/kaloyan-raev">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kaloyanraev">Twitter</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
