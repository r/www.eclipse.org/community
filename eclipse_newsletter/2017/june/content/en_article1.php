<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p><b>Debugging</b> is the routine process of locating and removing bugs, errors or abnormalities from programs. It’s a must have skill for any Java developer because it helps to find subtle bug that are not visible during code reviews or that only happens when a specific condition occurs. The Eclipse Java IDE provides many debugging tools and views grouped in the <b>Debug Perspective</b> to help the you as a developer debug effectively and efficiently.</p>

<p>There are many improvements included in the latest <a href="https://projects.eclipse.org/projects/eclipse.jdt">Eclipse Java Development Tools (JDT)</a> release included in the <a href="https://www.eclipse.org/oxygen/">Eclipse Oxygen</a> Simultaneous Release. This article will start with a beginner’s guide to start you with debugging. In the second part of the article, you will find a more advanced guide to debugging and you’ll discover what’s new for debugging in Eclipse Oxygen.</p>

<h2>Beginner’s Guide to Quick Start Debugging</h2> 

<p>Here are some quick tips and tools that will help you get started quickly with debugging your Java project.</p>

<h3>1. Launching and Debugging a Java program</h3> 
<p>A Java program can be debugged simply by right clicking on the Java editor class file from Package explorer. Select <b>Debug As &rarr; Java Application</b> or use the shortcut <b>Alt + Shift + D, J</b> instead.</p>  

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Launching_debugging-1.PNG" /></p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Launching_debugging-2.PNG" /></p>
	

<p>Either actions mentioned above creates a new <b>Debug Launch Configuration</b> and uses it to start the Java application.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Launching_debugging-3.PNG" /></p>
	
<p>In most cases, users can edit and save the code while debugging without restarting the program.This works with the support of <b>HCR</b> (Hot Code Replacement), which has been specifically added as a standard Java technique to facilitate experimental development and to foster iterative trial-and-error coding.</p> 

<h3>2. Breakpoints</h3>
<p>A breakpoint is a signal that tells the debugger to temporarily suspend execution of your program at a certain point in the code.</p> 

<p>To define a breakpoint in your source code, right-click in the left margin in the Java editor and select <i>Toggle Breakpoint</i>.  Alternatively, you can double-click on this position.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Breakpoints-1.PNG" /></p>	
	
	<p>The <i>Breakpoints</i> view allows you to delete and deactivate Breakpoints and modify their properties.</p>
	
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Breakpoints-2.PNG" /></p>	
		
		<p align="right"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Breakpoints-3.PNG" /></p>	
		
		<p>All breakpoints can be enabled/disabled using <b>Skip All Breakpoints</b>. Breakpoints can also be imported/exported to and from a workspace.</p>
		
<h3>3. Debug Perspective</h3>
<p>The debug perspective offers additional views that can be used to troubleshoot an application like Breakpoints, Variables, Debug, Console etc. When a Java program is started in the debug mode, users are prompted to switch to the debug perspective.</p>  
	<ul>
		<li><b>Debug view</b> – Visualizes call stack and provides operations on that.</li>
		<li><b>Breakpoints view</b> – Shows all the breakpoints.</li>
		<li><b>Variables/Expression view</b> – Shows the declared variables and their values.  Press <b>Ctrl+Shift+d</b> or <b>Ctrl+Shift+i</b> on a selected variable or expression to show its value. You can also add a permanent watch on an expression/variable that will then be shown in the <i>Expressions view</i> when debugging is on.</li> 
		<li><b>Display view</b> – Allows to Inspect the value of a variable, expression or selected text during debugging.</li>
		<li><b>Console view</b> – Program output is shown here.</li>
	</ul>
		
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Debug_Perspective.PNG" /></p>	
		
<h3>4.Stepping commands</h3>
<p>The Eclipse Platform helps developers debug by providing buttons in the toolbar and key binding shortcuts to control program execution.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Stepping_Commands.PNG" /></p>	
		
<p>For more information about debugging visit: <a href="http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftask-stepping.htm">Eclipse Stepping Commands Help</a></p>

<h2>Advanced Tools to Debug Complex Scenarios</h2>

<p>This section will give you more advanced tips and tricks to help you debug your Java project. The Eclipse Oxygen release includes many great improvements for Java debugging. Here’s a quick overview.</p>

<h3>1. Watchpoints, Exception Breakpoints, Conditional Breakpoints</h3>
<p>a. <b>Watchpoints</b> - A watchpoint is a special breakpoint that stops the execution of an application whenever the value of a given expression/field changes, without specifying where it might occur. User can specify by <b>Breakpoint Properties…</b> if they want the execution to stop when the watch expression is <b>Accessed</b>, <b>Modified</b> or both.</p>  

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Watchpoint.PNG" /></p>
<p>b. <b>Exception Breakpoints</b> – An exception breakpoint is specified for thrown exception using <b>Add Java Exception Breakpoint</b>.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Exception_Breakpoint.PNG" /></p>
		
		<p>Breakpoint for <b>NullPointerException</b> will halt whenever/wherever this exception is thrown.</p>
		
<p>c. <b>Condition Breakpoints</b> – Eclipse users can create conditions to restrict the activation of breakpoints.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Condition_Breakpoints.PNG" /></p>
			
	<p>Breakpoint will be activated only if value of Boolean b is true. Hit count can be provided to halt the execution at nth hit of the breakpoint. The breakpoint is disabled until either it is re-enabled or its hit count is changed or the program ends.</p>
	
<h3>2. Remote Debugging</h3>
 – The Eclipse IDE allows you to debug applications that runs on another Java Virtual Machine (JVM) or even on another machine.  You can create a new debug configuration of the <i>Remote Java Application</i> type. To enable remote debugging you need to start your Java application with certain flags. Connection Type can be specified as a Socket Attach or Socket Listen. Socket Listen supports multiple incoming connections.
 
 		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Remote_Debugging.PNG" /></p>
		
<h2>New Features in Eclipse Oxygen</h2>

<p>Here are the new features that have been added to the latest Eclipse Java IDE release.</p>

	<h3>1. Tracepoints</h3>
	<p>A new feature in the Eclipse Platform that allows users to creates conditional breakpoints to print out messages without halting at the breakpoints and cluttering the code base.</p>
		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Tracepoints-1.PNG" /></p>
		
<p>The Eclipse Platform created tracepoint with systrace template.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Tracepoints-2.PNG" /></p>
		
<h3>2. Trigger Point</h3> 
<p>Now users can activate <i>Trigger Point</i>. A set of trigger points can be defined for the breakpoints in a workspace.</p>  
<p>All the other breakpoints that are initially suppressed by triggers will be hit only after any of the all Trigger Points have been hit. All the triggers are disabled after a Trigger Point is hit and will be re-enabled after the run.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Trigger_point-1.PNG" /></p>
		
<p>Any breakpoint can be set as a trigger point by using Breakpoint Properties via the dialog or the detail pane of the <b>Breakpoints view</b>.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Trigger_Point-2.PNG" /></p>
	
	<p>Triggers are rendered with an overlay of <b>"T"</b> and the breakpoints suppressed by the triggers are rendered with an overlay of <b>"T"</b> with a cut.</p>
           
	<h3>2. Logical Structures</h3> 
	<p>In the <b>Variables view</b>, collection objects directly show their contained elements instead of their internal structure. Logical structures are now activated by default in the Oxygen release. <b>Show Logical Structure</b> can be turned off to show the internal structure.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Logical_Structures-1.PNG" /></p>
	
	<p>The Show Logical Structure context menu lets you create, choose or edit the representation.</p>
		
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Logical_Structures-2.PNG" /></p>
	
	
<h3>3. Method Result After Step Operation</h3>
<p>Also new to the Oxygen release are Method Results. During debugging, the last method result (per return or throw) that was observed during <b>Step Into</b>, <b>Step Over</b> or <b>Step Return</b>, is shown as first line in the <b>Variables view</b>.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Method_Result.PNG" /></p>
	
<h3>4. Launch Group</h3>	
<p>Also new in Oxygen, <b>Launch Group</b> launch configuration type allows you to launch multiple other launch configurations sequentially, with configurable actions after launching each group member. New launch groups can be created via the <b>Run &rarr; Run Configurations</b>... or <b>Run &rarr; Debug Configurations</b>... dialogs.</p>	

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Launch_Group.PNG" /></p>
	
<h3>Breakpoints Grouping and Sorting</h3>
<p>Eclipse users can now group the breakpoints by different categories in Eclipse Oxygen. Breakpoint Working Sets defines a group of breakpoints. User can perform actions like enable/disable on a working set.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Breakpoints_grouping.PNG" /></p>
	
	<p>Breakpoints are sorted by <b>Name</b> by default, sorting order can be changed to <b>Creation Time</b>.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/Breakpoints_sorting.PNG" /></p>
	
	<p>For more information about debugging visit: <a href="http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftask-stepping.htm">Eclipse Remote Debugging Help</a></p>

<h2>Conclusion</h2>
<p>This article covered some of the important debugging tools and some new tools provided by the Eclipse Java IDE in the Oxygen Release. Eclipse Oxygen is now available for <a href="https://www.eclipse.org/downloads/">download here</a>.</p> 

<p>For in depth knowledge, user can refer to:</p>
	<ul>
		<li><a href="http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftasks%2Ftask-running_and_debugging.htm&cp=1_3_6">Eclipse IDE Debugging Help</a></li>
		<li><a href="https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2Ftips%2Fjdt_tips.html&cp=1_5_6&anchor=debugging_section">Eclipse IDE Debugging Tips and Tricks</a></li>
	</ul>
	
<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/sarika.jpg"
        alt="Sarika Sinha" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Sarika Sinha<br />
            <a target="_blank" href="https://ibm.com">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/sarika-sinha-a851584/">Linkedin</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/sarikasinha1">Twitter</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

