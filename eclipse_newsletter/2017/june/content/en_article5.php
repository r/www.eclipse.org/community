<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The Eclipse IDE has a long history in providing various tools targeting Linux development. The Eclipse Linux Tools project was created back in 2006. The project was first named as “Linux Distro” and aimed at improving build and installation under Linux. It gained plugins for RPM packaging, Linux specific profiling (oprofile, valgrind….), Autotools integration in Eclipse CDT (C/C++ Development Tooling), and the latest two big additions have been Docker and Vagrant integration plugins. Some of the components have fully matured and graduated into components or improvements in projects lower down the stack. For example, Autotools build support is now part of CDT itself, a lot of the build improvements found their place into the Eclipse Platform releng and Eclipse Common Build Infrastructure (CBI) projects. Now, the project’s main goal is to further improve integration with Linux systems as a first hand experience for Eclipse IDE users. That is why making some parts of the project natural parts of the technology specific plugins is a win for us.</p>
<p>The Linux Tools 6.0 release, part of the Eclipse Oxygen release, doesn’t focus that much on new features, but focuses on realigning with new features and APIs in the Eclipse Platform so we can make next Linux Tools releases slimmer, easier to maintain and more consistent with rest of the ecosystem. This work helped to identify some new and easy to add functionalities too. Here are the major areas that are currently being worked on.</p>

<h2>Native touchpoints</h2>
<p>The <a target="_blank"href="https://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Fguide%2Fp2_actions_touchpoints.html">checkAndPromptNativePackage</a> touchpoint is not a new feature in p2, the but Linux Tools project was not making use of it until now. As many of the different plugins in the project are useless without the underlying tool it was high time to start using it. It will help users to detect and install missing tools for smoother overall experience. In the process, we found that there is a <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=515519">missing integration</a> for <a target="_blank" href="https://fedoraproject.org/">Fedora Linux</a> so this was fixed in P2 itself for Oxygen. All plugins have P2 instructions to ensure that the proper tools are installed on Fedora Linux. We are looking forward to add support for more Linux distributions, but this needs active involvement from users on these distributions to come up with the specific package names and versions, and to ensure they work properly.</p>

	<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/nativesoftware.png" alt="install native software packages" /></p>
          
<h2>Generic Editor</h2>
<p>One of the big new features added by the Eclipse Platform in Oxygen is Generic extension based editor. As we have a number of different editors (RPM .spec file, Dockerfile, ChangeLog and others) in the project, this is an area of special interest.</p>
<p>Current editors will be kept, but there is alternative to “Open with/Generic Text Editor” that would use the new extensions to the generic text editor instead of the dedicated one. This is preview of the future of editors in Eclipse IDE - not only in Linux Tools project but overall. Help us shape it together by trying and reporting issues or even try with editors in plugins you develop. The refactorings done are:</p>
	<ul>
		<li>Register content-type (org.eclipse.core.contenttype.contentTypes extension point) for the file names and extensions</li>
		<li>Extract PresentationReconciler child class and register it to org.eclipse.ui.genericeditor.presentationReconcilers extension point</li>
		<li>Register existing completion processors to org.eclipse.ui.genericeditor.contentAssistProcessors extension point</li>
		<li>Register existing hover providers to org.eclipse.ui.genericeditor.hoverProviders extension point</li>
		<li>Decouple setup done in code to rely on extension points and/or proper API</li>
	</ul>

<p>In addition to making our code structure way better (at the price of some API breakage to hide the editor to no longer be exported) there were some nice effects that end users can see like:</p>
	<ul>
		<li>Adding hover help to Dockerfile editor</li>
		<p align="center"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/dockerfilehover.png" alt="hovering dockerfile editor" /></p>
		<li>Many corner cases identified so completion and hover providers should be way more reliable now</li>
	</ul>
	
	<p>A number of areas in the Generic editor infrastructure were identified and marked to be “not on par” with existing editors. This will be worked on for the upcoming Eclipse Photon release in 2018 for both in Platform and Linux Tools. Once these are fixed, the current editors will be phased out in favor of the generic editor.</p>

<h2>Editor Highlighting Definitions</h2>
<p>Another long existing, but not used in Linux Tools editors feature is org.eclipse.ui.themes. All highlighting colors were hard coded in the editors and this wasn’t an issue in the past, but with the increasing popularity of Dark Theme it became a little unappealing. The first steps to improve this have already been taken. In this release, all colors are defined using the themes extension points so the door is open for CSS overrides. And we are looking for contributions from people using the Dark Theme too, so don’t be shy!</p>

<h2>Docker Tooling</h2>
<p>The most actively developed subcomponent in Linux Tools have seen a number of changes like:</p>
	<ul>
		<li>Updated version of docker-client for proper working with latest docker daemon versions</li>
		<li>New security options - Support has been added when launching commands in a Container to specify a security option profile. This can be done in lieu of specifying privileged mode. For example, to run gdbserver, one can specify "seccomp:unprofiled" to allow ptrace commands to be run by the gdb server. Details can be seen <a target="_blank" href="https://wiki.eclipse.org/Linux_Tools_Project/News/NewIn60#Docker_Tooling">here</a>.</li>
		<li>Fixes to enable features like “Launch C/C++ executable in container” in CDT as described in this <a href="https://www.eclipse.org/community/eclipse_newsletter/2017/april/article1.php">previous article</a>. There is work in progress for similar feature for Java applications that should land in one of the next releases.</li>
	</ul>
	
<h2>Oprofile</h2>
<p>The Oprofile tool itself stopped shipping opcontrol command line executable years ago and none of the current Linux distributions ship it, so we had to finally remove it since it was virtually untested for quite some time by any of the committers.</p>


<h2>Systemtap</h2>
<p>The Error view included in this plugin has been removed in favor of hyperlinks in the Console view.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/hyperlink.png"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/hyperlink_sm.png" alt="hyperlink console view" /></a></p>
        
<p>There are also a number of other small enhancements like:</p>
	<ul>
		<li>editor enhanced to treat try/catch/continue as keywords,</li>
		<li>fixed launch settings to be properly applied,</li>
		<li>improved auto-indentation,</li>
		<li>other smaller improvements.</li>
	</ul>  
	
	<h2>And more…</h2>
<p>There are a number of other small fixes and improvements, but due to project’s diversity it’s best if you try it out yourself. Just download and start Eclipse Oxygen/Install New Software.. then look at the various plugins in the Linux Tools group from the Oxygen update site.</p> 

<h2>Contact Us</h2>
<p>We want to hear from you! You can reach us in our developers <a href="https://dev.eclipse.org/mailman/listinfo/linuxtools-dev">mailing list</a>.</p> 	
<p>Please report any bugs in the project's <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Linux%20Tools">Bugzilla</a>.</p>
	

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/alexanderk.jpeg"
        alt="Alexander Kurtakov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Alexander Kurtakov
            <br />
            <a target="_blank" href="http://redhat.com">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/akurtakov">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/akurtakov">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

