<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<img align="right" class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/sirius_sm.png" alt="Eclipse Sirius Logo" />
<p><a href="https://eclipse.org/sirius/">Eclipse Sirius</a> is the easiest way to create your own modeling tool. It dramatically reduces the time when creating domain-specific modeling workbenches by leveraging the Eclipse Modeling technologies, including EMF and GMF. Based on a viewpoint approach, Sirius makes it possible to equip teams who have to deal with complex architectures on specific domains.</p>
 
<p>Sirius 5.0.0 version will be available in the Eclipse Oxygen release with a significant number of new features to create impressive graphical modeling tools. The Obeo team works hard to keep improving the experience for end-users of Sirius-based modelers. Since the Sirius 4.0.0 version, released with Eclipse Neon, 260 Bugzilla issues have been closed!.</p>
 
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/typing.gif" alt="Typing Gif" /></br>
<small>Source: <a target="_blank" href="https://giphy.com/gifs/hard-work-27Y1W0GCKQtDq">giphy</a></small></p>

<p>Since version 4.0, Sirius provides an integrated way to specify custom properties views like the other parts of the modeler are defined. Without writing any code it is possible to implement rich and dynamic views with elaborate styling and validation rules.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/runtime.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/runtime_sm.png" alt="runtime" /></a></p>
 
 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/propertiesview.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/propertiesview_sm.png" alt="properties view eclipse sirius" /></a></p>
 
 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/eclipsesirius.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/eclipsesirius_sm.png" alt="eclipse sirius workspace" /></a></p>
 
<p>Sirius 5.0.0 brings two main improvements to this feature.</p> 
 
<h2>Dialogs</h2>
<p>Let’s start with the dialogs. Without writing a single line of code, you can define dialogs directly from the properties view or graphical editors. Dialogs can be used in your Sirius tools to help users configure model elements, this can be done during the creation for example. Enjoy a smooth work process, and get a more ergonomic tool.</p>
 
  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/newman.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/newman_sm.png" alt="create new man" /></a></p>
 
<h2>Properties View</h2>
<p>We also simplified the properties view definition by providing a new extension mechanism. Thanks to this, the dialogs and properties views are able to share some common parts of their definition by using composition and extension mechanisms to keep a consistent look and feel. Again, priority is given to a smoother user experience: time saved and maintenance easier!</p> 

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/extendproperties.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/extendproperties_sm.png" alt="properties view extend" /></a></p>
  
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/warning.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/warning_sm.png" alt="properties warning sirius" /></a></p>
    
    
 <p>You can now improve the discoverability of your Sirius projects with the brand new *.aird editor which will let you manipulate all the concepts of your modeling projects with its semantic models, its usable viewpoints, and all its representations in one common place. The new editor also includes a wizard to create any kind of EMF model to easily kickstart your modeling projects.</p>
 
 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/dependencies.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/dependencies_sm.png" alt="dependencies sirius" /></a></p>
 
 <p>Of course, there are many other features, performance improvements, etc. For instance, on diagrams, the decorator mechanism has been improved to manage the layout of decorators. Before Sirius 5.0.0, you could put decorators on boxes or images, but only in a few restricted areas (corners or borders middle) and there was always the possibility to overlay decorators accidently, which could be inconvenient.</p>
 
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/oldicon.png" alt="old icons sirius" /></p>
 
 <p>In this new version, you can put several decorators in the same area: Sirius automatically manages their layout to avoid overlap. Again no code writing is needed, you just need to define  how you want your decorators to be organized within your Sirius project.</p>
 
  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/icons.png" alt="new icons sirius" /></p>
  
  <p>One other useful new feature is the improvement of the image resolution of exported diagrams. Before Sirius 5.0.0, the resolution was sometimes too low to give a proper final and professional look to an end user document. This isn’t what you want when you’re presenting something to your stakeholders. That time is now over with the Sirius higher resolution update!</p>
 
<h2>What’s Next?</h2>
<p>Coming in a future version of Sirius is a new dashboard view based on the Activity Explorer. This will help you guide your end users.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/activityexplorer.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/activityexplorer_sm.png" alt="eclipse sirius activity explorer" /></a></p>

<h2>Want to learn more?</h2>
 
<p>Want to get more informations on all these new Sirius 5.0.0 features?</p> 
<p>&#10145; <a href="https://www.eclipse.org/sirius/download.html">Download Sirius</a> to make your own modeling tool, and get involved in the growing <a href="https://www.eclipse.org/sirius/community.html">Sirius community</a>.</p> 
 
<p>Feel free to join the community on the Sirius forum: 1118 messages from 330 different authors have been posted since June 2016!</p> 
 
<p>If you missed the Eclipse Sirius 5.0.0 ‘All about UX’ webinar as part of the Eclipse Oxygen MeetUp Series, you can watch the <a target="_blank" href="https://youtu.be/EkLkhuA40zw">video here</a>.</p>

<p align="center"><a target="_blank" href="https://youtu.be/EkLkhuA40zw"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/ux_sirius.png" alt="eclipse sirius ux webinar" /></a></p>


<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/june/images/cedric.jpg"
        alt="Cedric Brun" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Cédric Brun<br />
            <a target="_blank" href="http://obeo.fr">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://cedric.brun.io/">Blog</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/bruncedric">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/cbrun">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
