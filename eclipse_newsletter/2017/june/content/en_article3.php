<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
		<img align="right" class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/image3.png" alt="gef logo" />
<p>While the <a href="https://eclipse.org/gef/">Eclipse Graphical Editing Framework (GEF)</a> project has been a citizen of the Eclipse ecosystem for 15 years that has undoubtedly seen many highs (and lows), the graduation of its next generation code base with the Eclipse Neon release was the personal climax of our involvement, and to a journey we started 7 years ago.</p>

<p>We have not rested since, and have spent significant effort in maturing and enhancing the new code base during the Eclipse Oxygen development stream. To reflect this, we have decided that it is about time to also perform a changeover in terms. While the term ‘GEF4’ was initially used to refer to the new code base, we now simply use ‘GEF’, and the old code base originating back from 2002 can now – without discrimination – be referred to as ‘GEF-Legacy’. The Oxygen release will thus be GEF 5.0.0.</p>

		<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/image2.png"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/image2_sm.png" alt="gef versions" /></a></p>

<p>GEF provides some <a target="_blank" href="https://github.com/eclipse/gef/wiki#user-documentation">end user functionality</a>, but it is mostly a framework for the development of arbitrary graphical rich-client applications, integrated into the Eclipse UI or standalone. While there are indeed no restrictions concerning the type of graphical application that can be realized, there are various defaults that make it especially handy when building diagram-like views and editors (nodes, edges, labels).</p>
<p>The framework is accompanied by a number of examples that demonstrate its usage. Most prominent are the MVC Logo example and the Zest Graph example, for rendering geometries and graphs, respectively.</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/image1.png" alt="gef logo example" /></p>
		
<p>The screenshot displays the MVC Logo example. It shows the following features that are available in GEF:</p>
	<ul>
		<li>Integration into Eclipse UI (as an IViewPart)</li>
		<li>JFace actions for the toolbar (zooming, scrolling)</li>
		<li>Infinite canvas with background grid (snap-to-grid is also supported)</li>
		<li>Snapping and alignment feedback (red lines)</li>
		<li>Selection feedback and handles (blue squares)</li>
		<li>Hover feedback and handles ("x" and "plus" icons)</li>
		<li>Rendering of arbitrary geometric shapes and curves</li>
		<li>Layered rendering of contents, feedback, and handles (for manipulation)</li>
	</ul>
	
<P>The framework is coupled to JavaFX as rendering technology, so we get all the nice stuff supported by a modern rendering framework out of the box: a scene graph API, CSS styling, animations, effects, etc. GEF adds additional concepts like visual anchors and connections, which are useful in a diagram context, and it ensures a seamless integration of JavaFX and SWT. The framework also provides a comprehensive geometry API that contains abstractions for geometric shapes and curves. All of that is used in the MVC Logo example: the letters and connections are actually rendered from shape and curve geometries, the connections are visually anchored to the shapes, so intersection points are automatically computed (and updated).</p>

<p>Much effort has been spent in making GEF-based applications feel interactive and intuitive. Besides mouse and key, touch gestures are supported throughout, and all GEF applications follow the principle of ‘live feedback, which ensures that a user interaction is directly and continuously reflected. When manipulating a connection by moving its waypoints, for example, the connection automatically ‘follows’ the handle movement, so the resulting state can directly be observed.</p>

<p>Three basic operations are generically provided to change the visualization as well as the underlying data model: transforming, resizing, and bending (i.e. reshaping). Further interaction capabilities are provided specific to certain specific interaction scenarios, like "bending".</p>

		<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/image4.png" alt="gef logo example" /></p>
        
<p>Orthogonally routed connections can for example be manipulated through their segments: blue rectangular handles can be used to move (or remove) segments, white rectangular handles can be used to split segments.</p>
        
        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/june/images/image5.png" alt="gef logo example" /></p>

<p>Straight-routed connections, in contrast, can be manipulated through their waypoints: blue circular handles can be used to move waypoints, while white circular handles can be used to insert new waypoints.</p>
<p>Other than that, GEF provides a graph data model, a corresponding renderer, and integrated automatic layout. These are all demonstrated in the Zest Graph example:</p>          
        
        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/june/images/image6.png"><img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/image6_sm.png" alt="gef versions" /></a></p>
          
<p>To get started, there is also a nice free tutorial provided by itemis:</p>
<ul>
<li><a target="_blank" href="https://info.itemis.com/en/gef/tutorials/">https://info.itemis.com/en/gef/tutorials/</a></li></ul>

<p>Try it out! Let us know what you think.</p>

<p>For further information about GEF, visit our web presences at:</p>
<ul>
<li>GEF @ GitHub: <a target="_blank" href="https://github.com/eclipse/gef">https://github.com/eclipse/gef</a></li> 
<li>GEF @ Eclipse: <a target="_blank" href="https://www.eclipse.org/gef/">https://www.eclipse.org/gef/</a></li>
</ul> 

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/alexander.jpeg"
          alt="Alexander Nyßen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Alexander Nyßen<br />
            <a target="_blank" href="https://www.itemis.com/">itemis AG</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/nyssen">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://nyssen.blogspot.de/">Blog</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
        <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
          src="/community/eclipse_newsletter/2017/june/images/matthias.png"
          alt="Matthias Wienand" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Matthias Wienand<br />
            <a target="_blank" href="https://www.itemis.com/">itemis AG</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/mwienand">GitHub</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
    
  </div>
</div>