<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Tutorial – Eclipse Marketplace & Favourites List";
  $pageKeywords = "eclipse, newsletter, marketplace, favourites, solutions, plugins, how to";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "This article is a tutorial to explain what Eclipse Marketplace is and how you can use the Favourite Lists to your advantage.";

  // Uncomment and set $original_url if you know the original url of this article.
$original_url = "http://blog.ttoine.net/en/2017/02/16/tutorial-eclipse-marketplace-favourites-list/";
$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");