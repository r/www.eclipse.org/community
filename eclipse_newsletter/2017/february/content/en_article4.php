<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>TestNG is a powerful test framework that enables developers to write flexible tests with <a target="_blank" href="http://testng.org/doc/documentation-main.html#methods">grouping</a>, <a target="_blank" href="http://testng.org/doc/documentation-main.html#methods">sequencing</a>, and data-driven features; it even makes <a target="_blank" href="http://testng.org/doc/documentation-main.html#methods">writing parallel tests</a> easy. And the most powerful feature, in my view, is a set of <i>Listeners</i> that hooks into TestNG, which allows to better control the test cases, or augment the reporting, etc.</p>
	<p>The TestNG Eclipse plugin gives you the ability to run and debug TestNG test cases within Eclipse IDE.</p>
	<p>This article shares some tips about how to be efficient when authoring and running tests with TestNG Eclipse Plugin.</p>

  <h2>Installation</h2>
	<p>TestNG for Eclipse Plugin is available on <a href="http://marketplace.eclipse.org/content/testng-eclipse">Eclipse Marketplace</a>, please follow the <a target="_blank" href="http://testng.org/doc/download.html">installation guide</a>.</p>
  <h2>Use TestNG Eclipse Plugin</h2>
  <h3>Authoring Tests</h3>
	<p>When you're authoring the TestNG test cases, the predefined templates can definitely help you be more efficient. To create a test method, in the editor, type <code>test</code>, then hit the Content Assist hot-key ('<i>Alt+/</i>' in my case), select the template as below:</p> 
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/content_assist.png"></p>
	
	<p>Note: There will be more templates available in TestNG Eclipse Plugin 6.10.1, e.g. 'setup', 'teardown', etc.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/setup_content_assist.png"></p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/teardown_content_assist.png"></p>
	
	<h3>Run/Debug Tests</h3>
	
	<h4>Lauching Tests</h4>
	There are several ways to launch tests, which is very handy. Here are the different ways it can be done:
	<ul>
		<li>Right clicking on the test class or the source editor, to run that test.</li>
		<li>Right clicking on the package, to run all the tests under the package.</li>
		<li>Right clicking on the TestNG suite xml file, to run the custom test suite.</li>
		<li>Or, go to Run Configuration for better control on the launch configuration, for example, the test group to run; pass system properties to the runtime test process, etc.</li>
	</ul>
	
	<h4>Re-run Failed Tests</h4>
	<p>If you have multiple failed tests, after you've fix your test code, you can rerun the failed tests altogether to save time:</p> 
	
	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/rerun_failed.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/rerun_failed_sm.png"></a></p>
	
	<h3>Maven Integration</h3>
	<p>If your project is Maven managed, you might want to configure <code>maven-surefire-plugin</code> or <code>maven-failsafe-plugin</code> to run the test case. Let's say you pass the following JVM arguments and system properties to the runtime test process:</p>
	
<pre>
&lt;artifactId&gt;maven-surefire-plugin&lt;/artifactId&gt;
&lt;configuration&gt;
    &lt;suiteXmlFiles&gt;
    &lt;suiteXmlFile&gt;test-suite/testng.xml&lt;/suiteXmlFile&gt;
    &lt;/suiteXmlFiles&gt;
    &lt;argLine&gt;-javaagent:${settings.localRepository}/org/aspectj/aspectjweaver/${aspectj.version}/aspectjweaver-${aspectj.version}.jar
                -Xmx1024m -XX:MaxPermSize=512m -Xms256m -Xmx1024m -XX:PermSize=128m 
                -Dversion=${project.version}
    &lt;/argLine&gt;
    &lt;systemPropertyVariables&gt;
    &lt;foo&gt;${foo.value}&lt;/foo&gt;
    &lt;/systemPropertyVariables&gt;
    &lt;environmentVariables&gt;
    &lt;bar&gt;${bar.value}&lt;/bar&gt;
    &lt;/environmentVariables&gt;
&lt;/configuration&gt;
</pre>

	
	<p>The tests run well with Maven cli, <code>mvn -e test</code>, but could fail with any TestNG Eclipse Plugin version prior to 6.9.10. In that case, you would have to manually copy the system properties to Launch Configuration for each test.</p>

	<p>Fortunately, today, the TestNG Eclipse Plugin can parse the configuration of <code>maven-surefire/failsafe-plugin</code>, auto append them to the <i>Launch Configuration</i>. This way, it also makes the behavior as close as possible to running on the command line. For more details, please refer to the <a target="_blank" href="http://testng.org/doc/eclipse.html#eclipse-m2e-integration">official guide</a>.</p>
	
	<p>Note: The equivalent feature for Gradle integration with <a target="_blank" href="https://projects.eclipse.org/projects/tools.buildship">Eclipse Buildship</a> is under planning, you can track the ticket <a target="_blank" href="https://github.com/cbeust/testng-eclipse/issues/282">here</a>.</p>
	
	<h2>TestNG Eclipse Plugin Internal</h2>
	
	<p>It's helpful to understand the core of the TestNG Eclipse plugin, especially when you're having trouble on using it.</p>
	<p>When a test running, the plugin needs to get the test results from runtime test process and display them on the view at real time.</p>
	<p>The communication between plugin and the runtime test process is made via socket. The plugin first start the socket server. While it's on the runtime test process there are a set of <i>Listeners</i> that hook into TestNG runtime and send the test results back to plugin via socket.</p>
	<p>The socket communication functionalities are maintained in the project '<a target="_blank" href="https://github.com/testng-team/testng-remote">testng-remote</a>' on GitHub.</p>
	
	<h2>Final Words</h2>
	<p>We keep improving the experience of the TestNG Eclipse Plugin, please send your feedback and ideas on the GitHub project <a target="_blank" href="https://github.com/cbeust/testng-eclipse">here</a>.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/february/images/nickt.jpg"
        alt="XuQing Tan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Nick Tan<br />
            <a target="_blank" href="https://www.ebates.com/">Ebates</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/missedone">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://cn.linkedin.com/in/xuqing-tan">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

