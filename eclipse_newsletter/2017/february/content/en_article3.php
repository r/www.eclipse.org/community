<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

   <p>What if your IDE could warn you as soon as you wrote bad code? More than just warning, what if it could teach you why something is bad, show you best practices, and give you examples of fixes? I'm not talking about poorly formatted code, or similarly minor issues. Thanks to powerful code analyzers also found in the well-known SonarQube platform, you can expect to catch serious flaws that would have led to critical bugs or security breaches.</p>

   <p>Eager to try? Please follow the guide.</p>
   
   <h2>Setup</h2>
   
   <p>You can find SonarLint in the <a target="_blank" href="https://marketplace.eclipse.org/content/sonarlint">Eclipse Marketplace</a>. Just install it and voila! Out of the box it will analyze Java, JavaScript, PHP and Python files. You can unlock more features by connecting <a target="_blank" href="http://www.sonarlint.org/eclipse/index.html">SonarLint</a> with a SonarQube server, but we'll talk about that later. SonarLint and the embedded analyzers are free and open-source.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/sonarlint_in_eclipsemarketplace.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonarlint_in_eclipsemarketplace_sm.png" alt="SonarLint in the Eclipse Marketplace"/></a></p>

<h2>Use</h2>

<p>To use SonarLint, simply write code as you usually do. Bugs or vulnerabilities will be annotated directly in the editor when you save changes.

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/coding_as_usual.gif" alt="writing code as usual"/></p>

<p>A dedicated view lets you browse all the issues detected by SonarLint. It's sorted by date, most recent first, so you can easily spot the one you've just introduced.</p>

<p>If the message is not clear enough to understand an issue, simply open the rule description. We make an extra effort to give you a lot of details so you can learn and make an informed decision (there is not always one absolutely correct way to fix an issue).</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonar_rule_description.gif" alt="rule description"/></p>

<h2>How to find (non trivial) bugs with static analysis</h2>

<p>As I teased earlier, SonarLint aims to find complex bugs. This is an ongoing effort, and every new version adds more and more checks. <br>Finding complex bugs requires the best possible understanding of the code. Here is a very approximate description of the process:</p>

<ul>
<li> Parsing: to understand the code, the analyzer needs to parse it and build a tree based on a grammar. This is somewhat easy for well-specified languages like Java, but much more difficult for C++, because there are ambiguities.</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/sonar_statement_sequence.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonar_statement_sequence_sm.png" alt="statement sequence"/></a></p>

<ul>
<li>Resolving symbols and types: now that we are able to distinguish a keyword from a variable or a method/type definition, it's time to build an index of all symbols with their types/signatures. Again, it may seem obvious in Java when you read ‘int a’ or ‘String s’ that ‘a’ is a primitive integer and ‘s’ is a java.lang.String. But when you add in generics or overloaded methods, it becomes more difficult. And don’t get me started on dynamically typed languages such as JavaScript.</li>
<li>Data Flow Analysis: this is similar to mentally executing code while reading it, trying to collect as much information as possible on a possible program state. Is this variable initialized? What if this condition is true? Was this resource closed? Data Flow Analysis could, potentially, consume an infinite amount of resources (CPU/Memory) so this is a best-effort approach. The scope of exploration can be limited to a single method/function or extended to be cross-method or even cross-file.</li>
</ul>

<p>As you probably guessed, simple parsing only enables simple checks. Properly resolving types and Data Flow Analysis is the key that opens the door to very advanced checks, with the potential to reveal serious bugs or vulnerabilities.</p>

<h2>Examples</h2>

<h3>Java</h3>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/sonar_java_flow.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonar_java_flow_sm.png" alt="Java example"/></a></p>

<p>Teaser: this screenshot shows an upcoming feature. SonarLint can highlight the flow leading to some issues. This will become a key feature, since complex Data Flow-based findings may be hard to understand without the context that leads up to them.</p>

<h3>JavaScript</h3>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/sonar_javascript.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonar_javascript_sm.png" alt="JavaScript example"/></a></p>

<h3>C++</h3>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/sonar_c++.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonar_c++_sm.png" alt="C++ example"/></a></p>

<h2>Advanced features</h2>

<p>You can unlock some additional features by connecting SonarLint to a SonarQube server and then binding your projects. It could be your company's instance, or a cloud instance such as <a target="_blank" href="https://sonarqube.com/">sonarqube.com</a>, which is available for free for open source projects.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/sonarqube_server_config.png" alt="SonarQube Server Configuration"/></p>

<p>Here are some of the benefits of connecting to a SonarQube server:</p>
<ul>
<li>Access to all the SonarSource analyzers installed on the server instead of just the default ones. This allows consistent issue reporting and support for additional (including commercial) languages.</li>
<li>Access to rulesets configured on the server, including custom rules.</li>
<li>Hide issues marked as "Won't Fix" or "False Positive".</li>
</ul>

<h2>Future</h2>

<p>SonarLint evolves quickly. We had 7 releases last year and we'll continue the effort in 2017. Each new version will include updated code analyzers, to spot more and more advanced bugs, but also to improve reporting. Don't hesitate to try it out, and give your feedback on our open discussion list: <a href= "mailto:sonarlint@googlegroups.com">sonarlint@googlegroups.com</a>. You can report problems, propose new rules, and suggest new features.</p>

<p>Happy bug-hunting!</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/february/images/julien_henry.png"
        alt="Julien Henry" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Julien Henry<br />
            <a target="_blank" href="https://www.sonarsource.com/">Code quality SonarSource</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/SonarLint?lang=en">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.sonarsource.com/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

