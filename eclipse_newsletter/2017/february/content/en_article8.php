<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

	<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	
<div class="alert alert-danger">
  <strong>Warning!</strong> This solution has been removed from Eclipse Marketplace and the Eclipse Foundation strongly suggests that users update to the new <a href="https://marketplace.eclipse.org/content/enhanced-class-decompiler">Enhanced Class Decompiler</a>. <a target="_blank" href="https://eclipse.org/org/press-release/20170814_security_bulletin.php">Read the security bulletin</a>.
</div>
   <p><a href="https://marketplace.eclipse.org/content/eclipse-class-decompiler">Eclipse Class Decompiler</a> is a plugin for the Eclipse platform. It integrates JD, Jad, FernFlower, CFR, and Procyon seamlessly with the Eclipse IDE. It displays all the Java sources during your debugging process, even if you do not have them all. And you can debug these class files directly without source code. It also integrates Javadoc and supports the syntax of JDK8 lambda expressions.</p>
   
<p>This project started in October 2012. When the Eclipse version was 3.x, I used the JadClipse plugin. But it didn’t support Eclipse 4.x, and the author didn't maintain it anymore. I decided to create a new decompiler plugin, and the initial version contained Jad and JD. The plugin now supports five decompilers and is based on the Eclipse JDT plugin. JDT provides a lot of features and has good scalability, so the decompiler plugin can be implemented easily. The following graph is the decompiler plugin architecture diagram:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/decompiler.png" alt="default decompiler setting"/></p>

<h2>Support Several Kinds of Decompiler</h2>

<p>The first Java decompiler was Jad, and the initial release was before 1999, 18 years ago. Jad was so old that it didn’t support Java generic type. Then JD appeared, it supported Java 7, much better than Jad. FernFlower, CFR, and Procyon are new modern decompilers that support Java 8. The Eclipse Class Decompiler integrates all of them in one plugin. The decompilers I highly recommend are FernFlower and JD. FernFlower supports all Java versions, but JD is the fastest. You can set the default decompiler of your choice in the Eclipse preference section.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/default.png" alt="default"/></p>

<h2>Support to Debug Code Without Source</h2>

<p>If the class file contains debug attributes, you can debug it in the Eclipse Class Decompiler Viewer. There are two ways to debug a class file. The first way is to set the decompiler preference, and to realign the line number. The second way is to check the debug mode in the decompiler menu bar. When your Eclipse workspace is in debug perspective, the debug mode becomes the default. The decompiler plugin will ignore your debug mode choice.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/debug_menuitem.png" alt="debug menuitem"/></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/debug_settings.png" alt="debug settings"/></p>

<h2>Support JavaDoc and Java 8 Lambda Expression</h2>

<p>The decompiler plugin implements the JavaDoc feature. If the jar binds javadoc in the Eclipse workspace, the api document will display on the decompiler viewer.</p>

<p>Jad and JD don't support Java 8. If you choose them as the default decompiler, when the class compliance level is Java 8, the decompiler plugin will decompile the code by FernFlower automatically.</p>

<p>FernFlower, CFR, and Procyon also support Java 8 Lambda expression. But the decompiled source codes of these decompilers are not the same, you can choose the one that works best for you.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/lambda&javadoc.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/lambda&javadoc_sm.png" alt="lambda&javadoc"/></a></p>   

<h2>Support to Export Decompiled Source Code</h2>

<p>This is a utility feature. You can export the decompiled codes from one or more classes, even the whole jar.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/export.png" alt="export"/></p>

<h2>Support to DND and Decompile Class</h2>

<p>The decompiler plugin can decompile the class file outside of the Eclipse IDE as well. Drag and drop the class file to the Eclipse editor area, the decompiled source code will display.</p>

<h2>Preference Settings</h2>

<p>The decompiler preference settings section is found here <strong>&quot;Window &gt; Preferences &gt; Java &gt; Decompiler&quot;</strong>. You can open the Eclipse perference dialog to set the preference for yourself. The most important setting is the &quot;Default Class Decompiler&quot;, you may change it frequently.</p>

<p>Please also check the startup option and set the &quot;Class Decompiler Viewer&quot; as the default class viewer, otherwise the decompiler plugin is no longer effective.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/Preferences.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/Preferences_sm.png" alt="Preferences"/></a></p> 

<h2 id="install-eclipse-decompiler-plugin">Install Eclipse Decompiler Plugin</h2>

<p>You can download the plugin on the <a href="https://marketplace.eclipse.org/content/eclipse-class-decompiler">Eclipse Marketplace</a> webpage or in your Eclipse installation:</p>
<ul>
<li>Click on &quot;Help &gt; Eclipse Marketplace...&quot;,</li>
<li>Search &quot;Eclipse Class Decompiler&quot; in the Eclipse Marketplace dialog,</li>
<li>Find &quot;Eclipse Class Decompiler&quot; and click on button &quot;install&quot;,</li>
<li>Check &quot;Eclipse Class Decompiler&quot;,</li>
<li>Next, next, next... and restart.</li>
</ul>

<h2>Status &amp; Roadmap</h2>

<p>The plugin is stable now, but it still has several issues to be fixed. Meanwhile, if the Eclipse IDE upgrades or a new decompiler appears, I will also update this plugin.</p>

<p>Welcome to use the Eclipse Class Decompiler.</p></body>


<div class="bottomitem">
  <h3>About the Author</h3> 
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
       <div class="col-sm-8">
            <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/february/images/chen.jpg"
        alt="Chen Chao" />
        </div> 
        <div class="col-sm-16">
          <p class="author-name">
             Chen Chao<br />
            <a target="_blank" href="http://www.meihuichina.com/">Shanghai Meihui Software Co., Ltd</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/cnfree">GitHub</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/chen-chao-a0174a91/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

