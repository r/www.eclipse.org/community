<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

   <img align="left" src="/community/eclipse_newsletter/2015/august/images/mtkplacelogo.png" alt="Eclipse Marketplace logo"> 
   
   <p>The <a href="https://marketplace.eclipse.org/">Eclipse Marketplace</a> makes it easy for you to discover solutions that will simplify and improve the way you do things in your Eclipse installation. These tools and plugins are all created by the Eclipse Community which means they aim to serve your since the creators are users themselves!</p>
   
	<p>The solutions can be installed directly to your Eclipse Installation with the Eclipse Marketplace Client. To access the Eclipse Marketplace Client from the Eclipse IDE:</p> 
	<ul>
		<li>Click <i>Cltr + 3</i> (Windows) or <i>Command + 3</i> (Mac) - this will launch the Quick Access search bar.</li>
		<li>Start typing “Eclipse Marketplace”; it will auto-complete before you finish typing. Click Enter.</li>
		<li>Search and install solutions.</li>
	</ul>
	 
	<p>You can also access the Eclipse Marketplace Client by clicking the Help tab &#x2192; Eclipse Marketplace or you can simply go to the Eclipse Marketplace website: <a href="https://marketplace.eclipse.org/">https://marketplace.eclipse.org/</a> and browse solutions there.</p>
	<p>The total solution download count is now at 23,867,883 which means there has been 8,444,357 since my <a href="http://www.eclipse.org/community/eclipse_newsletter/2015/august/top10_2015.php">article from July 2015</a>! There are so many solutions to discover, so we decided to help by highlighting some of the more popular ones.</p>
	   
	<h2>Top 10 Most Popular Solutions</h2>
	
	<p>Here are the most popular solutions on Eclipse Marketplace, based solely on the number of downloads in the past 30 days.</p>

<!--  
	<div class="row">
  <div class="col-sm-8">1.<a href="https://marketplace.eclipse.org/content/subversive-svn-team-provider/">Subversive - SVN Team Provider</a></div>
  <div class="col-sm-12">Integrate the Subversion (SVN) version control system with the Eclipse platform.</div>
  <div class="col-sm-4">28804 downloads</div>
</div>
<div class="row">
  <div class="col-sm-8"><a href="https://marketplace.eclipse.org/content/eclipse-color-theme/">Eclipse Color Theme</a></li></div>
  <div class="col-sm-12">Switch color themes conveniently and without side effects.</div>
  <div class="col-sm-4">28054 downloads</div>
</div>
-->	
	<ol>
		<li><a href="https://marketplace.eclipse.org/content/subversive-svn-team-provider/">Subversive - SVN Team Provider</a> (28,804 downloads)<br>
		Integrates the Subversion (SVN) version control system with the Eclipse platform.</li>
		<li><a href="https://marketplace.eclipse.org/content/eclipse-color-theme/">Eclipse Color Theme</a> (28,054 downloads)<br>
		Switch color themes conveniently and without side effects.</li> 
		<li><a href="https://marketplace.eclipse.org/content/subclipse/">Subclipse</a> (23,406 downloads)<br>
		Provides for Subversion within the Eclipse IDE.</li> 
		<li><a href="https://marketplace.eclipse.org/content/pydev-python-ide-eclipse/">PyDev - Python IDE for Eclipse</a> (19,979 downloads)<br>
		Enables Eclipse to be used as a Python IDE (supporting also Jython and IronPython).</li>
		<li><a href="https://marketplace.eclipse.org/content/spring-tool-suite-sts-eclipse/">Spring Tool Suite (STS) for Eclipse</a> (18,689 downloads)<br>
		Eclipse-powered development environment for building Spring-powered enterprise applications.</li>
		<li><a href="https://marketplace.eclipse.org/content/jboss-tools/">JBoss Tools</a> (16,629 downloads)<br>
		Umbrella project for a set of Eclipse plugins that includes support for JBoss and related technologies.</li>
		<li><a href="https://marketplace.eclipse.org/content/testng-eclipse/">TestNG for Eclipse</a> (15,180 downloads)<br>
		Enables developer to write flexible tests grouping, sequencing, and data-driven features.</li>  
		<li><a href="https://marketplace.eclipse.org/content/buildship-gradle-integration/">Buildship Gradle Integration</a> (14,796 downloads)<br>
		Plugins that provide support for building software using Gradle.</li>
		<li><a href="https://marketplace.eclipse.org/content/eclipse-moonrise-ui-theme/">Eclipse Moonrise UI Theme</a> (14,719 downloads)<br>
		Dark UI theme for Eclipse 4+.</li>
		<li><a href="https://marketplace.eclipse.org/content/eclipse-class-decompiler/">Eclipse Class Decompiler</a> (13,188 downloads)<br>
		Integrates JD, Jad, FernFlower, CFR, Procyon seamlessly with the Eclipse IDE and allows Java developers to debug class files without source code directly.</li>
	</ol> 
	<p><small>Note: This list was created on February 8, 2017.</small></p>
	
	<h2>Top 16 Up and Coming Solutions</h2>
	
	<p>These solutions are definitely some you want to have in your toolbox! They have climbed the charts more quickly than the other solutions in 2016. And since the year was 2016, I decided to list the top 16 up and coming solutions:</p>
	
	<ol>
	<li><a href="https://marketplace.eclipse.org/content/jsdt-closure">JSDT for Closure</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/imixs-bpmn">Imixs-BPMN</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/arduino-eclipse-plugin-named-sloeber-product">The Arduino Eclipse Plugin named Sloeber</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/azure-toolkit-eclipse">Azure Toolkit for Eclipse</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/avroclipse">Avroclipse</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/eclipse-archive-utility">Eclipse Archive Utility</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/java-antidecompiler">Java Antidecompiler</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/yatta-launcher-eclipse">Yatta Launcher for Eclipse</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/sql-development-tools">SQL Development Tools</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/eclox">Eclox</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/spartan-refactoring-0">Spartan Refactoring</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/eclipse-zip-editor">Eclipse Zip Editor</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/emftext">EMFText</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/asciidoc-tools">Asciidoc Tools</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/oracle-cloud-tools">Oracle Cloud Tools</a></li> 
	<li><a href="https://marketplace.eclipse.org/content/emfjson">Emfjson</a></li>
	</ol> 
	
	<p><small>Note: This list was created based on data from Jan 2016 to Dec 2016.</small></p>
		   
	<p>Some of these solutions and others are featured in this month's (February 2017) issue of the Eclipse Newsletter article. <a href="/community/eclipse_newsletter/2017/february/">Read them now</a>.</p>	     
   

<div class="bottomitem">
  <h3>About the Author</h3> 
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/assets/public/images/roxanne.jpg"
        alt="Roxanne Joncas" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Roxanne Joncas<br />
            <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation GmbH</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/roxannejoncas">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://medium.com/@roxanne.iot">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

