<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
	
<p>A few months ago we introduced the Eclipse Marketplace Favourites List. To explain how it works I created a <a target="_blank" href="http://blog.ttoine.net/en/2016/09/29/video-eclipse-marketplace-favourites-list/">video tutorial</a>. This article will be a more classic tutorial to explain what Eclipse Marketplace is and how you can use the Favourite Lists to your advantage.</p>

<h2>What is Eclipse Marketplace?</h2>

<p>The Eclipse Marketplace is a place to discover, share, and install relevant Eclipse plugins and solutions. Think of it like an app store for Eclipse solutions.</p>

<p>One of it’s main uses is to add plugins to your Eclipse installation. You will also find applications based on Eclipse Platform, tooling for IoT and other solutions and services. In this article you’ll find out how to do so – you might even learn a few new tricks along the way.</p>

<h2>Favourite list online</h2>

<p>One of the great features of the Eclipse Marketplace is the possibility to create and share a list of plugins. This is very helpful if you have many Eclipse installations, and want to find and install your favourites one quickly. How can you do this on the Eclipse website?</p>

<h3>How to do this on the website?</h3>
	<ol>
		<li>Go to <a target="_blank" href="https://marketplace.eclipse.org/">https://marketplace.eclipse.org/</a> and sign up. Or create an account if you don’t have one yet.</li>
		<li>Search for your favourite solution</li>
		<li>Click on the white star below the logo to add a plugin to your Favourites list.</li>
	</ol>

<h2>Visual Example</h2>

<p>In this example, <i>Darkest Dark Theme</i> will be added to my favourites plugins list. Once a plugin has been “starred” it is automatically added to your list.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/marketplace1.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace1_sm.png"></a></p>

 <p>To view it, simply go to “My Marketplace”:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace2.png"></p>

<h2>Marketplace Client in the Eclipse IDE</h2>

<h3>Manage plugins</h3>

<p>Did you know? You can install plugins from your Eclipse IDEs using the Marketplace Client. To launch it:</p>

<ol>
<li>Click on the “Help” tab in the menu.</li>
<li>Click on “Eclipse Marketplace”.</li>
</ol>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace3.png"></p>

<p>You can also launch it using the Quick Access Bar at the top right of your Eclipse workspace.</p>
<ol>
	<li>Click “Alt + 3” (Windows) or “Command + 3” (Mac) to launch the Quick Access search bar</li>
	<li>Type “Eclipse Marketplace”</li>
	<li>Hit “Enter” and voila – you’re there!</li>
</ol>

<p>You can display and manage your favorites here too:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace4.png"></p>

<p>You can off course add, remove, and install plugins. The Eclipse Marketplace Client and the website store your favorites with your <a target="_blank" href="https://www.eclipse.org/user/">eclipse.org account</a>, so they are synchronised.</p>

<h2>Copying a user’s Favourite List</h2>

<p>If you want to install plugins in the list of someone else, say, a colleague, or another contributor to a project, this is also possible. You can import a favourites list from an other Community users, and then select the plugin you want.</p>

<p>It’s also possible to install plugins from someone else’s list, say, a colleague, or another project contributor. To import a favourites list from an other Community users in your workspace Marketplace client:</p>

<ol>
<li>Click on “Import Favourites List”</li>
<li>Copy/paste the link to someone’s favorites lists.</li>
<li>All plugins are automatically selected. Unselect any plugins you don’t want by unchecking the checkboxes to the left of each individual plugin.</li>
<li>Click import.</li>

<p>Here is an example with <a target="_blank" href="https://accounts.eclipse.org/users/lvogel#tab-marketplace">Lars Vogel's</a> plugins.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace5.png"></p>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace6.png"></p>

<h2>Share</h2>

<p>Long story short, to save time or to share your favourite plugins quickly and easily with someone else, you can add plugins to your favourites list. Sharing is simple, all you need is the link to your list. You can find this link on your <a target="_blank" href="https://www.eclipse.org/user/">user profile</a>:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/marketplace7.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/marketplace7_sm.png"></a></p>

<p>Here are a few Favourites Lists that I would like to share with you. You can import them directly by copying and pasting the link in Eclipse Marketplace Client:</p>

<ul>
<li><a target="_blank" href="https://marketplace.eclipse.org/user/lvogel/favorites">Java – Lars Vogel</a></li>
<li><a target="_blank" href="https://marketplace.eclipse.org/user/dschaefer/favorites">C / C++ – Doug Schaefer</a></li>
<li><a target="_blank" href="https://marketplace.eclipse.org/user/cbrun/favorites">Modeling (Sirius) – Cédric Brun</a></li>
</ul>

<p>Do not hesitate to share your favourite list on social networks. You can use #EclipseMarketplace on Twitter.</p>

<h2>Feedback welcome</h2>

<p>As usual, feedback is welcome and discussion is open. Do not hesitate to comment this article or to open an bug.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/february/images/antoine.jpeg"
        alt="Antoine Thomas" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Antoine Thomas<br />
            <a target="_blank" href="http://eclipse.org">Eclipse Foundation GmbH</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ttoine">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.ttoine.net/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

