<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
<p>Looking for a unique balance between an Eclipse IDE and a modern web development experience? Then the 
<a target="_blank" href="https://www.genuitec.com/products/angular-ide/">Angular IDE</a> plugin is ideal for you. It covers the demands of the modern web developer by allowing you to code JavaScript and TypeScript in the Eclipse IDE, as well as use command-line tools. As part of this IDE, not only do you get access to modern web languages, but also the assistance of instant validation, rich editors with code completion, and even integrated launching of your Angular server.</p>

<p>Want to dive in and give it a try? Install the 
<a href="https://marketplace.eclipse.org/content/angular-ide">Angular IDE plugin</a> from the Eclipse Marketplace or use the the following npm commands to download the Angular IDE bundled with Eclipse IDE for your current Angular project:

<pre style='color:#000000;background:#ffffff;'><span style='color:#e34adc; '>></span> npm install <span style='color:#44aadd; '>-g</span> angular-ide
<span style='color:#e34adc; '>></span> ngide install ~<span style='color:#40015a; '>/Development/angular-ide</span>
<span style='color:#e34adc; '>></span> ngide open Foo<span style='color:#800000; font-weight:bold; '>.</span>ts
</pre>

<p>As soon as the Angular plugin is installed into your Eclipse IDE, you can use its new project wizard to generate your 
first Angular application using the Angular CLI.  While your project is being created, the new Terminal+ view will 
appear.  It will provide a context pertinent to your development -- via the command line. The view also performs 
automatic setup of needed command-line tools like Node and npm.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/new-angular.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/new-angular_sm.png"></a></p>

<p>As with most modern development, there are templates that can be used to rapidly accelerate the creation of your 
first application. Here are examples of Angular CLI-based templates that will make your development much faster:</p>

<ul>
	<li><a target="_blank" href="http://startangular.com/product/sb-admin-bootstrap-4-angular-2/">SB Admin Bootstrap 4 Template</a></li>
	<li><a target="_blank" href="https://github.com/akveo/ng2-admin">ng2-admin Dashboard Template</a></li>
</ul>

<p>Before diving in with an example of getting started with your first Angular project, here are the key capabilities 
that come with the Angular IDE plugin for the Eclipse IDE.</p>

<ul>
	<li>TypeScript 2.1 validation and debugging</li>
	<li>Advanced coding of HTML templates with validation and auto-complete</li>
	<li>Real-time validation inside the editor using TSLint 4</li>
	<li>Integrated usage of the angular-cli for Angular best practices</li>
	<li>Quick fixes to rapidly address common concerns in TypeScript</li>
	<li>Optimized for developers to make the most of Angular</li>
</ul>

<p>Ready to get started with your own application in Angular?  Here are the first few steps to take to get familiar 
with the concepts of components, services, routes and more.</p>

<p>You can read more at the <a target="_blank" href="https://www.genuitec.com/products/angular-ide/learning-center/">Angular IDE learning center</a>.</p>

<p><strong>Step 1:</strong> Use the New Angular 2 Project wizard by selecting File &gt; New &gt; Angular 2 Project, 
give it a name and click "Finish" (default settings will be fine).  The Angular IDE will set up a new project by 
using the Angular CLI "ng" command line.</p>

<p><strong>Step 2:</strong> Once the project has been created, it will be listed in the "Servers" view.  There 
are two options now -- to code more in the project, or to look at the application when it comes up (it will update 
itself anyway as you start to code it).</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/servers-view.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/servers-view_sm.png"></a></p>

<p>If you choose to start the server, go to the "Servers" view and expand Angular CLI.  Select the project in question and 
click the "Start" button.  A new tab will open in the Terminal+ view running "ng serve".  Need more information on this?  
See <a target="_blank" href="https://www.genuitec.com/products/angular-ide/learning-center/angular-server/">Running the Angular Server</a>
guide.</p>

<p><strong>Step 3:</strong> Create your first component by selecting New &gt; Angular 2 &gt; Component. It will be set up in 
the app directory of your project.</p> 

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/february/images/servers-view.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/february/images/new-component_sm.png"></a></p>

<p>The following is a sample of a newly generated component.</p>

<pre style='color:#000000;background:#ffffff;'><span style='color:#800000; font-weight:bold; '>import</span> <span style='color:#800080; '>{</span> Component<span style='color:#808030; '>,</span> OnInit <span style='color:#800080; '>}</span> from <span style='color:#800000; '>'</span><span style='color:#0000e6; '>@angular/core</span><span style='color:#800000; '>'</span><span style='color:#800080; '>;</span>
@Component<span style='color:#808030; '>(</span><span style='color:#800080; '>{</span>
  selector<span style='color:#800080; '>:</span> <span style='color:#800000; '>'</span><span style='color:#0000e6; '>app-widget</span><span style='color:#800000; '>'</span><span style='color:#808030; '>,</span>
  templateUrl<span style='color:#800080; '>:</span> <span style='color:#800000; '>'</span><span style='color:#0000e6; '>./widget.component.html</span><span style='color:#800000; '>'</span><span style='color:#808030; '>,</span>
  styleUrls<span style='color:#800080; '>:</span> <span style='color:#808030; '>[</span><span style='color:#800000; '>'</span><span style='color:#0000e6; '>./widget.component.css</span><span style='color:#800000; '>'</span><span style='color:#808030; '>]</span>
<span style='color:#800080; '>}</span><span style='color:#808030; '>)</span>
<span style='color:#800000; font-weight:bold; '>export</span> <span style='color:#800000; font-weight:bold; '>class</span> WidgetComponent <span style='color:#800000; font-weight:bold; '>implements</span> OnInit <span style='color:#800080; '>{</span>
  <span style='color:#797997; '>constructor</span><span style='color:#808030; '>(</span><span style='color:#808030; '>)</span> <span style='color:#800080; '>{</span> <span style='color:#800080; '>}</span>
  ngOnInit<span style='color:#808030; '>(</span><span style='color:#808030; '>)</span> <span style='color:#800080; '>{</span>
    <span style='color:#696969; '>// initialize the component</span>
  <span style='color:#800080; '>}</span>
<span style='color:#800080; '>}</span>
</pre>

<p>Why not take a moment to follow the <a target="_blank" href="https://www.genuitec.com/products/angular-ide/learning-center/angular-2-application">Angular 2 tutorial</a>
for details on how to make your first application from scratch!</p>

<p>Good luck!</p> 


<div class="bottomitem">
  <h3>About the Author</h3> 
  
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/february/images/timw.jpg"
        alt="Tim Webb" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Tim Webb<br />
            <a target="_blank" href="https://www.genuitec.com/">Genuitec</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/timrwebb">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/trwebb/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

