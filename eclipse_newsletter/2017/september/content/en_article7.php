<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>This July the Eclipse MicroProfile put in motion efforts to file our first JSR, now known as JSR-382 Configuration API 1.0 submitted by the Eclipse Foundation.</p>

<p>While there are several fantastic technical merits around JSR-382 Configuration API 1.0, from an industry perspective, this JSR is the very zeitgeist of where we are at and where we are headed. Like Kevin Bacon, almost everyone and everything is 6 degrees of separation from the Configuration JSR and after this JSR, nothing will ever be the same again.</p>

<h2>Why File a JSR at all?</h2>

<p>Let's call out the elephant in the room. Some of you may be thinking, why does an open source project need a standards body like the JCP? The short answer, trademark.</p>

<p>The difference between creating code in Java and creating Java itself is the JCP. Created in the early days of the language, the Java Community Process (JCP) is the solution Sun devised to allow people who are not Sun to create things named Java and create new APIs inside the java.* and javax.* namespaces. It's why we have a Java industry instead of just a Java company.</p>

<p>In the 18 years since the first JSR was filed, there have been a total of 410 JSRs submitted with leads spanning over 100 companies industry wide. Of that number, 131 have been rejected by the Executive Committee, withdrawn by the lead after feedback or in some way couldn't make it to completion.</p>

<p>The 256 JSRs that made the grade have created over 13,000 Java API classes and 1,200 java and javax packages we use everyday. There have been between 550-900 experts, worldwide, who have worked on those JSRs. The result of that work is owned in some form by around 59 companies and individuals.</p>

<p>We don't think of it everyday, but what we see under java.* and javax.* is actually spread across 59 different IP owners worldwide. Eclipse is the first one to be an open source foundation.</p>

<h2>First JSR from an Open Source Foundation</h2>

<p>Should JSR-382 lead by the Eclipse Foundation complete the JCP process and be granted some package under the java or javax namespace, that means some of what we think of as "Java" will be owned by an open source foundation.</p>

<p>This isn't preventing companies and individuals from participating in the role of lead. The people named as leads for JSR-382 are Emily Jiang and Mark Struberg. Though one works for IBM and the other is independent, it is the Eclipse Foundation that is legally the spec lead. Should the Eclipse community want to do another version of the Configuration JSR in the future, anyone from the project could be chosen in the role of lead.</p>

<p>If this model meets all the JCP requirements, allows creation under the java and javax namespace, and that creation to be owned by a foundation and controlled by its community, why would we ever go back to the old way?</p>

<p>The short answer, we wouldn't. The industry is forever and permanently changed. From here, there is only forward.</p>

<h2>5 Years of Configuration JSR Misses</h2>

<p>This new model isn't just good for a more open-source based approach to JSRs, but it is also good for momentum.</p>

<p>The concept of a Configuration JSR is not new. The first JavaOne presentation of a potential Configuration JSR was by Mike Keith at JavaOne 2013. The effort was picked up by Anatole Tresch in 2014, who put an enormous amount of work in and created the project Apache Tamaya to serve as a potential reference implementation and presented it to the JCP Executive Committee that September. Anatole was grouped with Laird Nelson in 2015 in efforts to help Anatole move it forward. Dmitry Kornilov picks up the mantle and presents a potential Configuration API and JSR at JavaOne 2016. Prior to all JSR attempts, the DNA of @Config can be traced through Tamaya to Apache DeltaSpike and to MyFaces CODI before that.</p>

<p>The company names of those fine folks are left off to protect the innocent and also because it simply does not matter. This kind of changing of the guards for a JSR looking to get off the ground is hard, but for an open source project it is business as usual.</p>

<p>The Eclipse MicroProfile Configuration API effort started in July 2016 and gained steam after JavaOne that October. By the time work on the JSR started in July 2017, the Config API had reached hundreds in commits, was presented at a half dozen conferences, has over a dozen committers from 4 different companies and even more individuals, 2 implementations and growing. The JSR draft itself received almost identical attention.</p>

<p>When you compare 3-4 years to 8-12 months it's hard not to get excited about the possibilities of this new model.</p>

<p>As an added bonus, all those pioneers who pushed Configuration previous can all join the Eclipse Foundation and even lead a future version of the JSR on Foundation's behalf should they want to and they can do this regardless of where they work. In the case of Mark Struberg, a core author of MyFaces CODI config, that is exactly what happened.</p>

<h2>The New Normal</h2>

<p>If you've looked at the Configuration JSR with technical eyes only, it's easy to miss the significance of it. Its impact on our industry is so much bigger than configuration. For our generation, it's the Java standards equivalent of a moon landing. Which for an organization named Eclipse, there is no better description.</p>



<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/david.jpg"
        alt="David Blevins" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            David Blevins<br />
            <a target="_blank" href="http://www.tomitribe.com/">Tomitribe</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/dblevins">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.tomitribe.com/blog/author/dblevins/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

