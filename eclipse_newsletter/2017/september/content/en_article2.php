<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

	<h2>Overview</h2>
<p>The first part of this article describes the background and motivation for the MicroProfile JWT RBAC security specification (MP-JWT).  The second part of the article will get into the specifics of the specification in terms of the JSON web token requirements, APIs. The third and final part will demonstrate example usage.</p>

<p>The security requirements that involve microservice architectures are strongly related with RESTful security. In a RESTful architecture style, services are usually stateless and any security state associated with a client is sent to the target service on every request in order to allow services to re-create a security context for the caller and perform both authentication and authorization checks.</p>

<p>For RESTful based microservices, security tokens offer a very lightweight and interoperable way to propagate identities across different services, where:</p>
	
	<ul>
    		<li>Services don’t need to store any state about clients or users</li>
        <li>Services can verify the token validity if token follows a well known format. Otherwise, services may invoke a separated service.</li>
        <li>Services can identify the caller by introspecting the token. If the token follows a well known format, services are capable to introspect the token by themselves, locally. Otherwise, services may invoke a separated service.</li>
        <li>Services can enforce authorization policies based on any information within a security token</li>
        <li>Support for both delegation and impersonation of identities</li>
	</ul>

<p>Today, the most common solutions involving RESTful and microservices security are based on <a target="_blank" href="https://tools.ietf.org/html/rfc6749">OAuth2</a>, <a target="_blank" href="https://tools.ietf.org/html/rfc6749">OpenID Connect(OIDC)</a> and <a target="_blank" href="https://tools.ietf.org/html/rfc6749">JSON Web Tokens(JWT)</a> standards.</p>

	<h2>Token Based Authentication</h2>
	
	<p>Token Based Authentication mechanisms allow systems to authenticate, authorize and verify identities based on a security token. Usually, the following entities are involved:</p>
	<ul>
        <li><b>Issuer</b> - Responsible for issuing security tokens as a result of successfully asserting an identity (authentication). Issuers are usually related with Identity Providers.</li>
        <li><b>Client</b> - Represented by an application to which the token was issued for. Clients are usually related with Service Providers. A client may also act as an intermediary between a subject and a target service (delegation).</li>
        <li><b>Subject</b> - The entity to which the information in a token refers to.</li>
        <li><b>Resource Server</b> - Represented by an application that is going to actually consume the token in order to check if a token gives access or not to a protected resource.</li>
   </ul>
    	
    	<p>Independent of the token format or protocol in use, from a service perspective, token based authentication is based on the following steps:</p>

	<p><b>1. Extract security token from the request</b></p>
        	<ul>
        		<li>For RESTful services, this is usually achieved by obtaining the token from the Authorization header.</li>
        	</ul>
	<p><b>2. Perform validation checks against the token</b></p>
   		<ul>
        		<li>This step usually depends on the token format and security protocol in use. The objective is make sure the token is valid and can be consumed by the application. It may involve signature, encryption and expiration checks.</li>
        	</ul>
    <p><b>3.Introspect the token and extract information about the subject</b></p>
   	 	<ul>
        		<li>This step usually depends on the token format and security protocol in use. The objective is to obtain all the necessary information about the subject from the token.</li>
        	</ul>
    <p><b>4.Create a security context for the subject</b></p>
    		<ul>
    			<li>Based on the information extracted from the token, the application creates a security context for the subject in order to use the information wherever necessary when serving protected resources.</li>
    		</ul>
	
	<h2>Using JWT Bearer Tokens to Protect Services</h2>
	<p>For the current MP-JWT specification, use cases are based on a scenario where services belong to the same security domain. This is avoids dealing with the complexities associated with federation of security domains. With that in mind, we assume that any information carried along with a token could be understood and processed (without any security breaches) by the different services involved.</p>

	<p>The use case can be described as follows:</p>

	<p>A client sends a HTTP request to Service A including the JWT as a bearer token:</p>
	
<pre>
GET /resource/1 HTTP/1.1
Host: example.com
Authorization: Bearer mF_9.B5f-4.1JqM
</pre>

	<p>On the server, a token-based authentication mechanism in front of Service A perform all steps described in the <a target="_blank" href="https://docs.google.com/document/d/10unKFr3rYkyFYjRV1qlJs-uDCp_CAm5_7wEbgi0Y38s/edit#heading=h.gknkjgq0mmvi">Token Based Authentication</a> section. As part of the security context creation, the server establishes role and group mappings for the subject based on the JWT claims. The role to group mapping is fully configurable by the server along the lines of the Java EE RBAC security model.</p>
	
	<p><a target="_blank" href="">JWT</a> tokens follow a well defined and known standard that is becoming the most common token format to protect services. In addition to providing a token format, it defines additional security aspects like signature and encryption based on another set of standards including <a target="_blank" href="https://tools.ietf.org/html/rfc7515">JSON Web Signature (JWS)</a> and <a target="_blank" href="https://tools.ietf.org/html/rfc7516">JSON Web Encryption (JWE)</a>.</p>

	<p>There are several reasons why JWT is becoming so widely adopted:</p>
	
	<ul>
    		<li>Token validation doesn’t require an additional trip and can be validated locally by each service</li>
        <li>Given its JSON nature, it is solely based on claims or attributes to carry authentication and authorization information about a subject.</li>
        <li>Makes easier to support different types of access control mechanisms such as ABAC, RBAC, Context-Based Access Control, etc.</li>
        <li>Message-level security using signature and encryption as defined by both JWS and JWE standards.</li>
        <li>Given its JSON nature, processing JWT tokens becomes trivial and lightweight. Especially if considering Java EE standards such as JSON-P or the different third-party libraries out there such as Nimbus, Jackson, etc.</li>
        <li>Parties can easily agree on a specific set of claims in order to exchange both authentication and authorization information. Defining this along with the Java API and mapping to JAX-RS APIs are the primary tasks of the MP-JWT specification.</li>
        <li>Widely adopted by different Single Sign-On solutions and well known standards such as OpenID Connect given its small overhead and ability to be used across different different security domains (federation).</li>
    </ul>
	
	<h2>The MP-JWT Specification</h2>
<p>The focus of the MP-JWT specification is the definition of the required format of the JWT used as the basis for interoperable authentication and authorization. The source for the specification, the API and the TCK are available from the <a target="_blank" href="https://github.com/eclipse/microprofile-jwt-auth">Eclipse microprofile-jwt-auth</a> repository.</p>

<p>The maximum utility of MP-JWT as a token format depends on the agreement between both identity providers and service providers. This means identity providers - responsible for issuing tokens - should be able to issue tokens using the MP-JWT format in a way that service providers can understand in order to introspect the token and gather information about a subject. To that end, the requirements for the MicroProfile JWT are:</p>
	
	<ol>
        	<li>Be usable as an authentication token.</li>
        	<li>Be usable as an authorization token that contains Java EE application level roles indirectly granted via a groups claim.</li>
        	<li>Can support additional standard claims described in <a target="_blank" href="https://www.iana.org/assignments/jwt/jwt.xhtml">IANA JWT Assignments</a> as well as non-standard claims.</li>
	</ol>
	
	<p>To meet those requirements, we introduce 2 new claims to the MP-JWT:</p>
		<ul>
            <li><b>"upn"</b>: A human readable claim that uniquely identifies the subject or user principal of the token, across the MicroProfile services the token will be accessed with.</li>
            <li><b>"groups"</b>: The token subject's group memberships that will be mapped to Java EE style application level roles in the MicroProfile service container.</li>
        	</ul>
    This specification has the following Java EE API dependencies:
         <ul>
            <li>JAX-RS 2.0.1</li> 
            <li>JSON-P 1.0</li>
            <li>CDI 1.2</li>
            <li>Common Annotations for the Java Platform 1.2</li> 
    		</ul>
    		
    <h2>Minimum MP-JWT Required Claims</h2>
	<p>The required minimum set of MP-JWT claims is based on claims from RFC7519 along with the two new MP-JWT specific claims. The required set of claims is:</p>
    	
	<table class="table table-striped">
      <tr>
        <th><p>Claim Name</p></th>
        <th><p>Description</p></th> 
        <th><p>Reference</p></th>
      <tr>
        <td><p>typ</p></td>
        <td><p>This JOSE header parameter identifies the token format and must be "JWT"</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-5.1">RFC7519, Section 5.1</a></p></td>
      </tr>
      <tr>
        <td><p>alg</p></td>
        <td><p>This JOSE header parameter identifies the cryptographic algorithm used to secure the JWT. MP-JWT requires the
use of the RSASSA-PKCS1-v1_5 SHA-256 algorithm and must be specified as "RS256"</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7515#section-4.1.1">RFC7515, Section 4.1.1</a></p></td>
      </tr>
      <tr>
        <td><p>kid</p></td>
        <td><p>This JOSE header parameter is a hint indicating which key was used to secure the JWT.</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7515#section-4.1.4">RFC7515, Section-4.1.4</a></p></td>
      </tr>
      <tr>
        <td><p>iss</p></td>
        <td><p>The token issuer</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.1">RFC7519, Section 4.1.1</a></p></td>
      </tr>
      <tr>
        <td><p>sub</p></td>
        <td><p>Identifies the principal that is the subject of the JWT. See the "upn" claim for how this relates to the runtime java.security.Principal</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.2">RFC7519, Section 4.1.2</a></p></td>
      </tr>
      <tr>
        <td><p>aud</p></td>
        <td><p>Identifies the recipients that the JWT is intended for</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.3">RFC7519, Section 4.1.3</a></p></td>
      </tr>
      <tr>
        <td><p>exp</p></td>
        <td><p>Identifies the expiration time on or after which the JWT MUST NOT be accepted for processing*</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.4">RFC7519, Section 4.1.4</a></p></td>
      </tr>
      <tr>
        <td><p>iat</p></td>
        <td><p>Identifies the time at which the issuer generated the JWT*</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.6">RFC7519, Section 4.1.6</a></p></td>
      </tr>
      <tr>
        <td><p>jti</p></td>
        <td><p>Provides a unique identifier for the JWT</p></td> 
        <td><p><a target="_blank" href="https://tools.ietf.org/html/rfc7519#section-4.1.7">RFC7519, Section 4.1.7</a></p></td>
      </tr>
      <tr>
        <td><p>upn</p></td>
        <td><p>Provides the user principal name in the java.security.Principal interface**</p></td> 
        <td><p>MP-JWT 1.0 specification</p></td>
      </tr>
      <tr>
        <td><p>groups</p></td>
        <td><p>Provides the list of group names that have been assigned to the principal of the MP-JWT.
This typically will require a mapping at the application container level to application deployment roles, but a one-to-one between group names and application role names is required to be performed in addition to any other mapping.</p></td> 
        <td><p>MP-JWT 1.0 specification</p></td>
      </tr>
  </table>
  <p><small>* The NumericDate used by `exp`, `iat`, and other date related claims is a JSON numeric value
representing the number of seconds from 1970-01-01T00:00:00Z UTC until the specified
UTC date/time, ignoring leap seconds. </small></p>
  <p><small>**There is fallback logic to leverage existing standard claims if the upn claim is missing. An implementation should first look to the OIDC <a target="_blank" href="http://openid.net/specs/openid-connect-core-1_0.html#StandardClaims">preferred_username</a>, and if that is missing, the "sub" claim should be used.</small></p>
  
  <p>An example minimal MP-JWT in JSON would be:</p>
  
<pre style="background:#000;color:#f8f8f8">{
    <span style="color:#65b042">"typ"</span>: <span style="color:#65b042">"JWT"</span>,
   <span style="color:#65b042">"alg"</span>: <span style="color:#65b042">"RS256"</span>,
   <span style="color:#65b042">"kid"</span>: <span style="color:#65b042">"abc-1234567890"</span>
}
{
      <span style="color:#65b042">"iss"</span>: <span style="color:#65b042">"https://server.example.com"</span>,
      <span style="color:#65b042">"aud"</span>: <span style="color:#65b042">"s6BhdRkqt3"</span>,
      <span style="color:#65b042">"jti"</span>: <span style="color:#65b042">"a-123"</span>,
      <span style="color:#65b042">"exp"</span>: <span style="color:#3387cc">1311281970</span>,
      <span style="color:#65b042">"iat"</span>: <span style="color:#3387cc">1311280970</span>,
      <span style="color:#65b042">"sub"</span>: <span style="color:#65b042">"24400320"</span>,
      <span style="color:#65b042">"upn"</span>: <span style="color:#65b042">"jdoe@server.example.com"</span>,
      <span style="color:#65b042">"groups"</span>: [<span style="color:#65b042">"red-group"</span>, <span style="color:#65b042">"green-group"</span>, <span style="color:#65b042">"admin-group"</span>, <span style="color:#65b042">"admin"</span>],
}
</pre>
  
  <h2>Additional Claims</h2>
<p>The MP-JWT can contain any number of other custom and standard claims. An example MP-JWT that contains additional "auth_time", "preferred_username", "acr", "nbf" and a custom "roles" claims is:</p>
  
  <pre style="background:#000;color:#f8f8f8">{
   <span style="color:#65b042">"typ"</span>: <span style="color:#65b042">"JWT"</span>,
   <span style="color:#65b042">"alg"</span>: <span style="color:#65b042">"RS256"</span>,
   <span style="color:#65b042">"kid"</span>: <span style="color:#65b042">"abc-1234567890"</span>
}
{
  <span style="color:#65b042">"iss"</span>: <span style="color:#65b042">"https://server.example.com"</span>,
  <span style="color:#65b042">"aud"</span>: <span style="color:#65b042">"s6BhdRkqt3"</span>,
  <span style="color:#65b042">"exp"</span>: 1311281970,
  <span style="color:#65b042">"iat"</span>: 1311280970,
  <span style="color:#65b042">"sub"</span>: <span style="color:#65b042">"24400320"</span>,
  <span style="color:#65b042">"upn"</span>: <span style="color:#65b042">"jdoe@server.example.com"</span>,
  <span style="color:#65b042">"groups: ["</span>red-group<span style="color:#65b042">", "</span>green-group<span style="color:#65b042">", "</span>admin-group<span style="color:#65b042">"],
  "</span>roles<span style="color:#65b042">": ["</span>auditor<span style="color:#65b042">", "</span>administrator<span style="color:#65b042">"],
  "</span>jti<span style="color:#65b042">": "</span>a-123<span style="color:#65b042">",
  "</span>auth_time<span style="color:#65b042">": 1311280969,
  "</span>preferred_username<span style="color:#65b042">": "</span>jdoe<span style="color:#65b042">",
  "</span>acr<span style="color:#65b042">": "</span>phr<span style="color:#65b042">",
  "</span>nbf<span style="color:#65b042">":  1311288970
}
</span></pre>

	<h2>The JsonWebToken Interface</h2>
	<p>This specification defines a JsonWebToken java.security.Principal interface extension that makes this set of required claims available via get style accessors. The MP-JWT 1.0 JsonWebToken interface definition is:</p>
	

<pre style="background:#000;color:#f8f8f8">
<span style="color:#e28964">package</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt</span>;
<span style="color:#99cf50">public</span> <span style="color:#99cf50">interface</span> <span style="text-decoration:underline">JsonWebToken</span> <span style="color:#99cf50">extends</span> <span style="color:#9b5c2e;font-style:italic">Principal</span> {

   <span style="color:#aeaeae;font-style:italic">/**
    * Returns the unique name of this principal. This either comes from the upn
    * claim, or if that is missing, the preferred_username claim. Note that for
    * guaranteed interoperability a upn claim should be used.
    *
    * <span style="color:#e28964">@return</span> the unique name of this principal.
    */</span>
   <span style="color:#99cf50">@Override</span>
   <span style="color:#99cf50">String</span> <span style="color:#89bdff">getName</span>();

   <span style="color:#aeaeae;font-style:italic">/**
    * Get the raw bearer token string originally passed in the authentication
    * header
    * <span style="color:#e28964">@return</span> raw bear token string
    */</span>
   default <span style="color:#99cf50">String</span> <span style="color:#89bdff">getRawToken</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>raw_token<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The iss(Issuer) claim identifies the principal that issued the JWT
    * <span style="color:#e28964">@return</span> the iss claim.
    */</span>
   default <span style="color:#99cf50">String</span> <span style="color:#89bdff">getIssuer</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iss<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The aud(Audience) claim identifies the recipients that the JWT is
    * intended for.
    * <span style="color:#e28964">@return</span> the aud claim.
    */</span>
   default <span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span> <span style="color:#89bdff">getAudience</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>aud<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The sub(Subject) claim identifies the principal that is the subject of
    * the JWT. This is the token issuing
    * IDP subject, not the
    *
    * <span style="color:#e28964">@return</span> the sub claim.
    */</span>
   default <span style="color:#99cf50">String</span> <span style="color:#89bdff">getSubject</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>sub<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The jti(JWT ID) claim provides a unique identifier for the JWT.
    The identifier value MUST be assigned in a manner that ensures that
    there is a negligible probability that the same value will be
    accidentally assigned to a different data object; if the application
    uses multiple issuers, collisions MUST be prevented among values
    produced by different issuers as well.  The "jti" claim can be used
    to prevent the JWT from being replayed.
    * <span style="color:#e28964">@return</span> the jti claim.
    */</span>
   default <span style="color:#99cf50">String</span> <span style="color:#89bdff">getTokenID</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>jti<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The exp (Expiration time) claim identifies the expiration time on or
    * after which the JWT MUST NOT be accepted
    * for processing in seconds since 1970-01-01T00:00:00Z UTC
    * <span style="color:#e28964">@return</span> the exp claim.
    */</span>
   default <span style="color:#99cf50">long</span> <span style="color:#89bdff">getExpirationTime</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>exp<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The iat(Issued at time) claim identifies the time at which the JWT was
    * issued in seconds since 1970-01-01T00:00:00Z UTC
    * <span style="color:#e28964">@return</span> the iat claim
    */</span>
   default <span style="color:#99cf50">long</span> <span style="color:#89bdff">getIssuedAtTime</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iat<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * The groups claim provides the group names the JWT principal has been
    * granted.
    *
    * This is a MicroProfile specific claim.
    * <span style="color:#e28964">@return</span> a possibly empty set of group names.
    */</span>
   default <span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span> <span style="color:#89bdff">getGroups</span>() {
       <span style="color:#e28964">return</span> getClaim(<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>groups<span style="color:#e28964">.</span>name());
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * Access the names of all claims are associated with this token.
    * <span style="color:#e28964">@return</span> non-standard claim names in the token
    */</span>
   <span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span> <span style="color:#89bdff">getClaimNames</span>();

   <span style="color:#aeaeae;font-style:italic">/**
    * Verify is a given claim exists
    * <span style="color:#e28964">@param</span> claimName - the name of the claim
    * <span style="color:#e28964">@return</span> true if the JsonWebToken contains the claim, false otherwise
    */</span>
   default <span style="color:#99cf50">boolean</span> <span style="color:#89bdff">containsClaim</span>(<span style="color:#99cf50">String</span> <span style="color:#3e87e3">claimName</span>) {
       <span style="color:#e28964">return</span> claim(claimName)<span style="color:#e28964">.</span>isPresent();
   }

   <span style="color:#aeaeae;font-style:italic">/**
    * Access the value of the indicated claim.
    * <span style="color:#e28964">@param</span> claimName - the name of the claim
    * <span style="color:#e28964">@return</span> the value of the indicated claim if it exists, null otherwise.
    */</span>
   <span style="color:#99cf50">&lt;<span style="color:#99cf50">T</span>></span> <span style="color:#99cf50">T</span> <span style="color:#89bdff">getClaim</span>(<span style="color:#99cf50">String</span> <span style="color:#3e87e3">claimName</span>);

   <span style="color:#aeaeae;font-style:italic">/**
    * A utility method to access a claim value in an <span style="color:#89bdff">{@linkplain <span style="color:#e18964;text-decoration:underline">Optional</span>}</span>
    * wrapper
    * <span style="color:#e28964">@param</span> claimName - the name of the claim
    * <span style="color:#e28964">@param</span> <span style="color:#89bdff">&lt;<span style="color:#89bdff">T</span>></span> - the type of the claim value to return
    * <span style="color:#e28964">@return</span> an Optional wrapper of the claim value
    */</span>
   default &lt;<span style="color:#99cf50">T</span>> <span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">T</span>></span> <span style="color:#89bdff">claim</span>(<span style="color:#99cf50">String</span> <span style="color:#3e87e3">claimName</span>) {
       <span style="color:#e28964">return</span> <span style="color:#99cf50">Optional</span><span style="color:#e28964">.</span>ofNullable(getClaim(claimName));
   }
}
</pre>

<h2>The Claims Enumeration Utility Class, and the Set of Claim Value Types</h2>

<p>The Claims utility class encapsulates an enumeration of all the standard JWT related claims along with a description and the required Java type for the claim as returned from the JsonWebToken#getClaim(String) method.</p>

<pre style="background:#000;color:#f8f8f8">
<span style="color:#99cf50">public</span> <span style="color:#99cf50">enum</span> <span style="text-decoration:underline">Claims</span> {
   <span style="color:#aeaeae;font-style:italic">// The base set of required claims that MUST have non-null values in the JsonWebToken</span>
   <span style="color:#89bdff">iss</span>("<span style="color:#99cf50">Issuer</span>", <span style="color:#99cf50">String</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">sub</span>("<span style="color:#99cf50">Subject</span>", <span style="color:#99cf50">String</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">aud</span>("<span style="color:#99cf50">Audience</span>", <span style="color:#99cf50">Set</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">exp</span>("<span style="color:#99cf50">Expiration</span> <span style="color:#99cf50">Time</span>", <span style="color:#99cf50">Long</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">iat</span>("<span style="color:#99cf50">Issued</span> <span style="color:#99cf50">At</span> <span style="color:#99cf50">Time</span>", <span style="color:#99cf50">Long</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">jti</span>("<span style="color:#99cf50">JWT</span> <span style="color:#99cf50">ID</span>", <span style="color:#99cf50">String</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">upn</span>("<span style="color:#99cf50">MP</span>-<span style="color:#99cf50">JWT</span> <span style="color:#3e87e3">specific</span> <span style="color:#3e87e3">unique</span> <span style="color:#3e87e3">principal</span> <span style="color:#3e87e3">name</span>", <span style="color:#99cf50">String</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">groups</span>("<span style="color:#99cf50">MP</span>-<span style="color:#99cf50">JWT</span> <span style="color:#3e87e3">specific</span> <span style="color:#3e87e3">groups</span> <span style="color:#3e87e3">permission</span> <span style="color:#3e87e3">grant</span>", <span style="color:#99cf50">Set</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">raw_token</span>("<span style="color:#99cf50">MP</span>-<span style="color:#99cf50">JWT</span> <span style="color:#3e87e3">specific</span> <span style="color:#3e87e3">original</span> <span style="color:#3e87e3">bearer</span> <span style="color:#3e87e3">token</span>", <span style="color:#99cf50">String</span>.<span style="color:#3e87e3">class</span>),

   // The IANA registered, but MP-JWT optional claims
   <span style="color:#89bdff">nbf</span>("<span style="color:#99cf50">Not</span> <span style="color:#99cf50">Before</span>", <span style="color:#99cf50">Long</span>.<span style="color:#3e87e3">class</span>),
   <span style="color:#89bdff">auth_time</span>("<span style="color:#99cf50">Time</span> <span style="color:#3e87e3">when</span> <span style="color:#3e87e3">the</span> <span style="color:#3e87e3">authentication</span> <span style="color:#3e87e3">occurred</span>", <span style="color:#99cf50">Long</span>.<span style="color:#3e87e3">class</span>),

...
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">String</span> <span style="color:#89bdff">getDescription</span>() {
       <span style="color:#e28964">return</span> description;
   }

   <span style="color:#99cf50">public</span> <span style="color:#99cf50">Class&lt;?></span> <span style="color:#89bdff">getType</span>() {
       <span style="color:#e28964">return</span> type;
   }
}

</pre>

	<p>Custom claims not defined by the Claims enum are required to be valid JSON-P <code>javax.json.JsonValue</code> subtypes. The current complete set of valid claim types is therefore, (excluding the invalid Claims.UNKNOWN Void type):</p>
   	 	<ul>
            <li>java.lang.String</li>
            <li>java.lang.Long</li>
            <li>java.lang.Boolean</li>
          	<li>java.util.Set&lt;java.lang.String&gt;</li>
            <li>javax.json.JsonValue.TRUE/FALSE</li>
            <li>javax.json.JsonString</li>
            <li>javax.json.JsonNumber</li>
            <li>javax.json.JsonArray</li>
            <li>javax.json.JsonObject</li>
   	 	</ul>


	<h2>Mapping MP-JWT Tokens to Java EE Container APIs</h2>
<p>The requirements of how a JWT should be exposed via the various Java EE container APIs is discussed in this section. For the 1.0 release, the only mandatory container integration is with the JAX-RS container, and CDI injection of the MP-JWT types.</p>

	<h3>CDI Injection Requirements</h3>
	<p>This section describes the requirements for MP-JWT implementations with regard to the injection of MP-JWT tokens and their associated claim values.</p>

		<h4>Injection of `JsonWebToken`</h4>
		<p>An MP-JWT implementation must support the injection of the currently authenticated caller as a JsonWebToken as shown in this code fragment:</p>
	
<pre style="background:#000;color:#f8f8f8">@Path(<span style="color:#65b042">"/endp"</span>)
@<span style="color:#99cf50">DenyAll</span>
@<span style="color:#99cf50">RequestScoped</span>
<span style="color:#99cf50">public</span> <span style="color:#99cf50">class</span> <span style="text-decoration:underline">RolesEndpoint</span> {

   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonWebToken</span> callerPrincipal;
</pre>

        <h4>Injection of JsonWebToken Claims via Raw, ClaimValue and JSON-P Types</h4>
        <p>This specification requires support for injection of claims from the current JsonWebToken using the <code>org.eclipse.microprofile.jwt.Claim</code> qualifier:</p>

<pre style="background:#000;color:#f8f8f8"><span style="color:#aeaeae;font-style:italic">/**
* Annotation used to signify an injection point for a <span style="color:#89bdff">{@link <span style="color:#e18964;text-decoration:underline">ClaimValue</span>}</span> from
* a <span style="color:#89bdff">{@link <span style="color:#e18964;text-decoration:underline">JsonWebToken</span>}</span>
*/</span>
@<span style="color:#99cf50">Qualifier</span>
@Retention(<span style="color:#99cf50">RetentionPolicy</span><span style="color:#3387cc"><span style="color:#e28964">.</span>RUNTIME</span>)
@Target({<span style="color:#99cf50">ElementType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>FIELD</span>, <span style="color:#99cf50">ElementType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>METHOD</span>, <span style="color:#99cf50">ElementType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>PARAMETER</span>, <span style="color:#99cf50">ElementType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>TYPE</span>})
<span style="color:#99cf50">public</span> <span style="color:#99cf50">@interface</span> <span style="text-decoration:underline">Claim</span> {
   <span style="color:#aeaeae;font-style:italic">/**
    * The value specifies the id name the claim to inject
    * <span style="color:#e28964">@return</span> the claim name
    * <span style="color:#e28964">@see</span> JsonWebToken#getClaim(String)
    */</span>
   <span style="color:#99cf50">@Nonbinding</span>
   <span style="color:#99cf50">String</span> <span style="color:#89bdff">value</span>() default "";

   <span style="color:#aeaeae;font-style:italic">/**
    * An alternate way of specifying a claim name using the <span style="color:#89bdff">{@linkplain <span style="color:#e18964;text-decoration:underline">Claims</span>}</span>
    * enum
    * <span style="color:#e28964">@return</span> the claim enum
    */</span>
   <span style="color:#99cf50">@Nonbinding</span>
   <span style="color:#99cf50">Claims</span> <span style="color:#89bdff">standard</span>() default Claims.UNKNOWN;
}
</pre>

    <p>with `@Dependent` scoping. MP-JWT implementations are required to support injection of the claim values using any of:</p>
        	<ul>
        		<li>The raw type associated with the <code>JsonWebToken</code> claim value as defined in the Claims enum.</li>
        		<li><code>org.eclipse.microprofile.jwt.ClaimValue</code> wrapper.</li>
        		<li><code>javax.json.JsonValue</code> JSON-P subtypes.</li>
    		</ul>
    <p>The <code>org.eclipse.microprofile.jwt.ClaimValue</code> interface is:</p>

<pre style="background:#000;color:#f8f8f8">
<span style="color:#aeaeae;font-style:italic">/**
* A representation of a claim in a <span style="color:#89bdff">{@link <span style="color:#e18964;text-decoration:underline">JsonWebToken</span>}</span>
* <span style="color:#e28964">@param</span> <span style="color:#89bdff">&lt;<span style="color:#89bdff">T</span>></span> the expected type of the claim
*/</span>
<span style="color:#99cf50">public</span> <span style="color:#99cf50">interface</span> <span style="text-decoration:underline">ClaimValue</span>&lt;T> <span style="color:#99cf50">extends</span> <span style="color:#9b5c2e;font-style:italic">Principal</span> {

   <span style="color:#aeaeae;font-style:italic">/**
    * Access the name of the claim.
    * <span style="color:#e28964">@return</span> The name of the claim as seen in the JsonWebToken content
    */</span>
   <span style="color:#99cf50">@Override</span>
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">String</span> <span style="color:#89bdff">getName</span>();

   <span style="color:#aeaeae;font-style:italic">/**
    * Access the value of the claim.
    * <span style="color:#e28964">@return</span> the value of the claim.
    */</span>
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">T</span> <span style="color:#89bdff">getValue</span>();
}

</pre>

	<p>The following example code fragment illustrates various examples of injecting different types of claims using a range of generic forms of the ClaimValue, JsonValue subtypes, as well as the raw claim types</p>

<pre style="background:#000;color:#f8f8f8">
<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt.Claim</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt.ClaimValue</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt.Claims</span>;

@Path(<span style="color:#65b042">"/endp"</span>)
@<span style="color:#99cf50">DenyAll</span>
@<span style="color:#99cf50">RequestScoped</span>
<span style="color:#99cf50">public</span> <span style="color:#99cf50">class</span> <span style="text-decoration:underline">RolesEndpoint</span> {
.<span style="color:#e28964">.</span>.

   <span style="color:#aeaeae;font-style:italic">// Raw types</span>
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>raw_token)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">String</span> rawToken;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;1></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span>=<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iat)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">Long</span> issuedAt;

   <span style="color:#aeaeae;font-style:italic">// ClaimValue wrappers</span>
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;2></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>raw_token)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">String</span>></span> rawTokenCV;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iss)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">String</span>></span> issuer;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>jti)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">String</span>></span> jti;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;3></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"jti"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">String</span>></span>></span> optJTI;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"jti"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue</span> objJTI;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;4></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"aud"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span>></span> aud;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"groups"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span>></span> groups;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;5></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span>=<span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iat)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Long</span>></span> issuedAtCV;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"iat"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Long</span>></span> dupIssuedAt;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"sub"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">String</span>></span>></span> optSubject;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"auth_time"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">Long</span>></span>></span> authTime;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;6></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"custom-missing"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">ClaimValue&lt;<span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">Long</span>></span>></span> custom;
   <span style="color:#aeaeae;font-style:italic">//</span>
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>jti)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">Instance&lt;<span style="color:#99cf50">String</span>></span> providerJTI;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;7></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iat)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">Instance&lt;<span style="color:#99cf50">Long</span>></span> providerIAT;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"groups"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">Instance&lt;<span style="color:#99cf50">Set&lt;<span style="color:#99cf50">String</span>></span>></span> providerGroups;
   <span style="color:#aeaeae;font-style:italic">//</span>
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>jti)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonString</span> jsonJTI;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#3387cc">standard</span> <span style="color:#e28964">=</span> <span style="color:#99cf50">Claims</span><span style="color:#e28964">.</span>iat)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonNumber</span> jsonIAT;
   <span style="color:#99cf50">@Inject</span> <span style="color:#aeaeae;font-style:italic">// &lt;8></span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"roles"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonArray</span> jsonRoles;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"customObject"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonObject</span> jsonCustomObject;

</pre>

        <p>&lt;1&gt; Injection of a non-proxyable raw type like java.lang.Long must happen in a RequestScoped bean as the producer will have dependendent scope.</p>
        <p>&lt;2&gt; Injection of the raw MP-JWT token string.</p>
        <p>&lt;3&gt; Injection of the jti token id as an `Optional&lt;String&gt;` wapper.</p>
        <p>&lt;4&gt; Injection of the aud audience claim as a Set&lt;String&gt;. This is the required type as seen by looking at the Claims.aud enum value's Java type member.</p>
        <p>&lt;5&gt; Injection of the issued at time claim using an @Claim that references the claim name using the Claims.iat enum value.</p>
        <p>&lt;6&gt; Injection of a custom claim that does exist will result in an Optional&lt;Long&gt;</p>
        value for which isPresent() will return false.</p>
        <p>&lt;7&gt; Another injection of a non-proxyable raw type like java.lang.Long, but the use of the javax.enterprise.inject.Instance interface allows for injection to occur in non-RequestScoped contexts.</p>
        <p>&lt;8&gt; Injection of a JsonArray of role names via a custom "roles" claim.</p>

	<p>The example shows that one may specify the name of the claim using a string or a <code>Claims</code> enum value. The string form would allow for specifying non-standard claims while the <code>Claims</code> enum approach guards against typos and misspellings.
	</p>
	
	<h3>JAX-RS Container API Integration</h3>
        <p>The behavior of the following JAX-RS security related methods is required for MP-JWT implementations.</p>
		
		<h4>javax.ws.rs.core.SecurityContext.getUserPrincipal()</h4>
        <p>The <code>java.security.Principal</code> returned from these methods MUST be an instance of <code>org.eclipse.microprofile.jwt.JsonWebToken</code>.</p>
		
		<h4>javax.ws.rs.core.SecurityContext#isUserInRole(String)</h4>
        <p>This method MUST return true for any name that is included in the MP-JWT "groups" claim, as well as for any role name that has been mapped to a group name in the MP-JWT "groups" claim.</p>
        
        <h4>Mapping from @RolesAllowed</h4>
        <p>Any role names used in @RolesAllowed or equivalent security constraint metadata that match names in the role names that have been mapped to group names in the MP-JWT "groups" claim, MUST result in an allowing authorization decision wherever the security constraint has been applied.</p>
        
        
       <h2>Example Usage</h2>

<p>Let’s look at a concrete example. The source code repository is available at <a target="_blank" href="https://github.com/MicroProfileJWT/eclipse-newsletter-sep-2017">https://github.com/MicroProfileJWT/eclipse-newsletter-sep-2017</a>. To get things setup, follow these steps:</p>
		<ol>
            <li>Download the wildfly-swarm MP_12-RC3 release from: <a target="_blank" href="https://github.com/MicroProfileJWT/wildfly-swarm-mp1.2/releases/tag/MP_12-RC3">https://github.com/MicroProfileJWT/wildfly-swarm-mp1.2/releases/tag/MP_12-RC3</a></li>
            <li>Follow the instructions on the release page to either download the source and build it, or to download the maven artifacts and unpack into your local maven repository.</li>
            <li>git clone <a target="_blank" href="https://github.com/MicroProfileJWT/eclipse-newsletter-sep-2017">https://github.com/MicroProfileJWT/eclipse-newsletter-sep-2017</a></li>
            <li>cd <a target="_blank" href="https://github.com/MicroProfileJWT/eclipse-newsletter-sep-2017">eclipse-newsletter-sep-2017</a></li>
            <li>Run mvn test from the directory to build and run the test example</li>
        </ol>
	<p>The example project is a maven based TestNG/Arquillian unit test that that deploys a JAX-RS application using Wildfly-Swarm. The structure of the application is shown in the following figure:</p>
      <p style="color:grey">Figure 1, View of the example structure.</p>  
      <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/september/images/figure1_folders.png"/></p>
	  
	  
	  
	  <p>The files are</p>
	  	<ul>
            <li>MyJaxrsApp.java - The JAX-RS Application class</li>
            <li>MySecureWallet.java - A JAX-RS resource endpoint</li>
            <li>MySecureWalletTest.java - An Arquillian/TestNg unit test to deploy and exercise the MySecureWallet endpoint</li>
            <li>resources - test resources directory</li>
            <li>web.xml - servlet metadata descriptor to define security constraints</li>
            <li>jwt-roles.properties - a properties file used to define token group to application role mappings</li>
            <li>privateKey.pem - the test private key used to sign the token</li>
            <li>Project-defaults.yml - a Wildfly-Swarm configuration file that sets up the security domain</li>
            <li>publicKey.pem - the test public key used to verify the token signature</li>
            <li>Token1.json - the JSON content for test token 1</li>
            <li>Token1-50000-limit.json the json content for test token 1 with an alternate warningLimit claim</li>
            <li>Token2.json - the JSON content for test token 2</li>
            <li>Token3.json - the JSON content for test token 3</li>
            <li>Token-noaccess.json - the JSON content for a test token that should not map to any valid access roles.</li>
	  	</ul>
	  	
	  	<p><b>Listing 1, MyJaxrsApp.java</b> provides the JAX-RS Application class. In addition to the standard JAX-RS/CDI annotations that define the application root path and scope, there is the MP-JWT @LoginConfig which declares the “MP-JWT” authentication method, and a “jwt-jaspi” realmName. The MP-JWT runtime will use this information to setup the authentication mechanism to be based on MicroProfile JWT bearer tokens.</p>

<p style="color:grey">Listing 1, MyJaxrsApp.java</p>
	  	
<pre style="background:#000;color:#f8f8f8"><span style="color:#e28964">package</span> <span style="color:#99cf50">org.eclipse.microprofile.test.jwt</span>;

<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.enterprise.context.ApplicationScoped</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.ApplicationPath</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.Path</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.core.Application</span>;

<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.auth.LoginConfig</span>;

<span style="color:#aeaeae;font-style:italic">// We set the authentication method to the MP-JWT for the MicroProfile JWT method</span>
<span style="color:#aeaeae;font-style:italic">// the realmName maps the security-domains setting in the project-defaults.yml</span>
@LoginConfig(authMethod <span style="color:#e28964">=</span> <span style="color:#65b042">"MP-JWT"</span>, realmName <span style="color:#e28964">=</span> <span style="color:#65b042">"jwt-jaspi"</span>)
@<span style="color:#99cf50">ApplicationScoped</span>
@ApplicationPath(<span style="color:#65b042">"/wallet"</span>)
<span style="color:#99cf50">public</span> <span style="color:#99cf50">class</span> <span style="text-decoration:underline">MyJaxrsApp</span> <span style="color:#99cf50">extends</span> <span style="color:#9b5c2e;font-style:italic">Application</span> {
}
</pre>

<p>The JAX-RS endpoint resource is shown in <b>Listing 2, MySecureWallet.java.</b> This is a hypothetical online wallet that provides operations for viewing the wallet balance, debiting money from the wallet, and crediting money to the wallet.</p> 	  	
	
	<p style="color:grey">Listing 2, MySecureWallet.java</p>
	  	
<pre style="background:#000;color:#f8f8f8">
<span style="color:#e28964">package</span> <span style="color:#99cf50">org.eclipse.microprofile.test.jwt</span>;

<span style="color:#e28964">import</span> <span style="color:#99cf50">java.math.BigDecimal</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">java.util.Optional</span>;

<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.annotation.security.DeclareRoles</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.annotation.security.DenyAll</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.annotation.security.RolesAllowed</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.enterprise.context.ApplicationScoped</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.enterprise.inject.Instance</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.inject.Inject</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.json.Json</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.json.JsonNumber</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.json.JsonObject</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.json.JsonObjectBuilder</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.GET</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.Path</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.Produces</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.QueryParam</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.core.Context</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.core.MediaType</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.core.Response</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">javax.ws.rs.core.SecurityContext</span>;

<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt.Claim</span>;
<span style="color:#e28964">import</span> <span style="color:#99cf50">org.eclipse.microprofile.jwt.JsonWebToken</span>;

@<span style="color:#99cf50">ApplicationScoped</span>
@DeclareRoles({<span style="color:#65b042">"ViewBalance"</span>, <span style="color:#65b042">"Debtor"</span>, <span style="color:#65b042">"Creditor"</span>, <span style="color:#65b042">"Debtor2"</span>, <span style="color:#65b042">"BigSpender"</span>})
@Path(<span style="color:#65b042">"/"</span>)
@<span style="color:#99cf50">DenyAll</span>
<span style="color:#99cf50">public</span> <span style="color:#99cf50">class</span> <span style="text-decoration:underline">MySecureWallet</span> {
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">double</span> bigSpenderLimit <span style="color:#e28964">=</span> <span style="color:#3387cc">1000</span>;
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">BigDecimal</span> usdBalance <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#99cf50">BigDecimal</span>(<span style="color:#65b042">"100000.0000"</span>);
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">BigDecimal</span> bitcoinXrate <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#99cf50">BigDecimal</span>(<span style="color:#65b042">"4538.0000"</span>);
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">BigDecimal</span> ethereumXrate <span style="color:#e28964">=</span> <span style="color:#e28964">new</span> <span style="color:#99cf50">BigDecimal</span>(<span style="color:#65b042">"328.0000"</span>);
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonWebToken</span> jwt;
   <span style="color:#99cf50">@Inject</span>
   <span style="color:#99cf50">@Claim</span>(<span style="color:#65b042">"warningLimit"</span>)
   <span style="color:#99cf50">private</span> <span style="color:#99cf50">Instance&lt;<span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">JsonNumber</span>></span>></span> warningLimitInstance;

   <span style="color:#99cf50">@GET</span>
   <span style="color:#99cf50">@Path</span>(<span style="color:#65b042">"/balance"</span>)
   <span style="color:#99cf50">@Produces</span>(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)
   <span style="color:#99cf50">@RolesAllowed</span>({<span style="color:#65b042">"ViewBalance"</span>, <span style="color:#65b042">"Debtor"</span>, <span style="color:#65b042">"Creditor"</span>})
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">JsonObject</span> <span style="color:#89bdff">getBalance</span>() {
       <span style="color:#e28964">return</span> generateBalanceInfo();
   }

   <span style="color:#99cf50">@GET</span>
   <span style="color:#99cf50">@Path</span>(<span style="color:#65b042">"/debit"</span>)
   <span style="color:#99cf50">@Produces</span>(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)
   <span style="color:#99cf50">@RolesAllowed</span>({<span style="color:#65b042">"Debtor"</span>, <span style="color:#65b042">"Debtor2"</span>})
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">Response</span> <span style="color:#89bdff">debit</span>(@<span style="color:#99cf50">QueryParam</span>("<span style="color:#3e87e3">amount</span>") String amount,
                         @Context SecurityContext securityContext) {
       <span style="color:#99cf50">Double</span> damount <span style="color:#e28964">=</span> <span style="color:#99cf50">Double</span><span style="color:#e28964">.</span>valueOf(amount);
       <span style="color:#e28964">if</span>(damount <span style="color:#e28964">></span> bigSpenderLimit) {
           <span style="color:#e28964">if</span>(securityContext<span style="color:#e28964">.</span>isUserInRole(<span style="color:#65b042">"BigSpender"</span>)) {
               <span style="color:#aeaeae;font-style:italic">// Validate the spending limit from the token claim</span>
               <span style="color:#99cf50">JsonNumber</span> spendingLimit <span style="color:#e28964">=</span> jwt<span style="color:#e28964">.</span>getClaim(<span style="color:#65b042">"spendingLimit"</span>);
               <span style="color:#e28964">if</span>(spendingLimit <span style="color:#e28964">==</span> <span style="color:#3387cc">null</span> <span style="color:#e28964">||</span> spendingLimit<span style="color:#e28964">.</span>doubleValue() <span style="color:#e28964">&lt;</span> damount) {
                   <span style="color:#e28964">return</span> <span style="color:#99cf50">Response</span><span style="color:#e28964">.</span>status(<span style="color:#99cf50">Response</span><span style="color:#e28964">.</span><span style="color:#99cf50">Status</span><span style="color:#3387cc"><span style="color:#e28964">.</span>BAD_REQUEST</span>)<span style="color:#e28964">.</span>build();
               }
           } <span style="color:#e28964">else</span> {
               <span style="color:#e28964">return</span> <span style="color:#99cf50">Response</span><span style="color:#e28964">.</span>status(<span style="color:#99cf50">Response</span><span style="color:#e28964">.</span><span style="color:#99cf50">Status</span><span style="color:#3387cc"><span style="color:#e28964">.</span>FORBIDDEN</span>)<span style="color:#e28964">.</span>build();
           }
       }
       usdBalance <span style="color:#e28964">=</span> usdBalance<span style="color:#e28964">.</span>subtract(<span style="color:#e28964">new</span> <span style="color:#99cf50">BigDecimal</span>(amount));
       <span style="color:#e28964">return</span> <span style="color:#99cf50">Response</span><span style="color:#e28964">.</span>ok(generateBalanceInfo())<span style="color:#e28964">.</span>build();
   }

   <span style="color:#99cf50">@GET</span>
   <span style="color:#99cf50">@Path</span>(<span style="color:#65b042">"/credit"</span>)
   <span style="color:#99cf50">@Produces</span>(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)
   <span style="color:#99cf50">@RolesAllowed</span>(<span style="color:#65b042">"Creditor"</span>)
   <span style="color:#99cf50">public</span> <span style="color:#99cf50">JsonObject</span> <span style="color:#89bdff">credit</span>(@<span style="color:#99cf50">QueryParam</span>("<span style="color:#3e87e3">amount</span>") String amount) {
       usdBalance <span style="color:#e28964">=</span> usdBalance<span style="color:#e28964">.</span>add(<span style="color:#e28964">new</span> <span style="color:#99cf50">BigDecimal</span>(amount));
       <span style="color:#e28964">return</span> generateBalanceInfo();
   }

   <span style="color:#99cf50">private</span> <span style="color:#99cf50">JsonObject</span> <span style="color:#89bdff">generateBalanceInfo</span>() {
       <span style="color:#99cf50">BigDecimal</span> balanceInBitcoins <span style="color:#e28964">=</span> usdBalance<span style="color:#e28964">.</span>divide(bitcoinXrate, <span style="color:#99cf50">BigDecimal</span><span style="color:#3387cc"><span style="color:#e28964">.</span>ROUND_HALF_EVEN</span>);
       <span style="color:#99cf50">BigDecimal</span> balanceInEthereum <span style="color:#e28964">=</span> usdBalance<span style="color:#e28964">.</span>divide(ethereumXrate, <span style="color:#99cf50">BigDecimal</span><span style="color:#3387cc"><span style="color:#e28964">.</span>ROUND_HALF_EVEN</span>);
       <span style="color:#99cf50">JsonObjectBuilder</span> result <span style="color:#e28964">=</span> <span style="color:#99cf50">Json</span><span style="color:#e28964">.</span>createObjectBuilder()
               .add(<span style="color:#65b042">"usd"</span>, usdBalance)
               .add(<span style="color:#65b042">"bitcoin"</span>, balanceInBitcoins)
               .add(<span style="color:#65b042">"ethereum"</span>, balanceInEthereum)
               ;

       <span style="color:#99cf50">Optional&lt;<span style="color:#99cf50">JsonNumber</span>></span> warningLimit <span style="color:#e28964">=</span> warningLimitInstance<span style="color:#e28964">.</span>get();
       <span style="color:#e28964">if</span>(warningLimit<span style="color:#e28964">.</span>isPresent() <span style="color:#e28964">&amp;&amp;</span> warningLimit<span style="color:#e28964">.</span>get()<span style="color:#e28964">.</span>doubleValue() <span style="color:#e28964">></span> usdBalance<span style="color:#e28964">.</span>doubleValue()) {
           <span style="color:#99cf50">String</span> warningMsg <span style="color:#e28964">=</span> <span style="color:#99cf50">String</span><span style="color:#e28964">.</span>format(<span style="color:#65b042">"balance is below warning limit: %s"</span>, warningLimit<span style="color:#e28964">.</span>get());
           result<span style="color:#e28964">.</span>add(<span style="color:#65b042">"warning"</span>, warningMsg);
       }

       <span style="color:#e28964">return</span> result<span style="color:#e28964">.</span>build();
   }
}

</pre>

<p>The wallet endpoint is using JAX-RS, CDI and Java EE security annotations to define behaviors. The first MP-JWT feature to notice is the injection of the JsonWebToken and a warningLimit claim value:</p>

<pre style="background:#fff;color:#000">@<span style="color:#ff7800">Inject</span>
<span style="color:#ff7800">private</span> <span style="color:#ff7800">JsonWebToken</span> jwt;
@<span style="color:#ff7800">Inject</span>
@Claim(<span style="color:#409b1c">"warningLimit"</span>)
<span style="color:#ff7800">private</span> <span style="color:#ff7800">Instance&lt;<span style="color:#ff7800">Optional&lt;<span style="color:#ff7800">JsonNumber</span>></span>></span> warningLimitInstance;
</pre>

<p>The JsonWebToken is an interface into the MP-JWT bearer token of the currently authenticated caller. The <code>warningLimitInstance</code> is a view into a custom token claim named “warningLimit”. Here we are using the CDI Instance interface to provide access to the claim because <code>MySecureWallet</code> is <code>@ApplicationScoped</code> while claim values are produced with <code>@Dependent</code> scope while being associated with the <code>@RequestScoped</code> token. Without the Instance interface, the CDI container would bind the warningLimit claim value to the first value seen when the MySecureWallet resource was created. The type of the warningLimit claim value is Optional&lt;/JsonNumber&gt;. We are using Optional because not every caller’s token may include this custom claim. The underlying type is a JsonNumber because custom claim types are represented as the JSON-P type associated with the token’s JSON payload.&lt;/JsonNumber&gt;</p>

<p>The injected JsonWebToken is used in the debit endpoint:</p>
<pre style="background:#fff;color:#000"><span style="color:#ff7800">if</span>(damount <span style="color:#ff7800">></span> bigSpenderLimit) {
   <span style="color:#ff7800">if</span>(securityContext<span style="color:#ff7800">.</span>isUserInRole(<span style="color:#409b1c">"BigSpender"</span>)) {
       <span style="color:#8c868f">// Validate the spending limit from the token claim</span>
       <span style="color:#ff7800">JsonNumber</span> spendingLimit <span style="color:#ff7800">=</span> jwt<span style="color:#ff7800">.</span>getClaim(<span style="color:#409b1c">"spendingLimit"</span>);
       <span style="color:#ff7800">if</span>(spendingLimit <span style="color:#ff7800">==</span> <span style="color:#3b5bb5">null</span> <span style="color:#ff7800">||</span> spendingLimit<span style="color:#ff7800">.</span>doubleValue() <span style="color:#ff7800">&lt;</span> damount) {
           <span style="color:#ff7800">return</span> <span style="color:#ff7800">Response</span><span style="color:#ff7800">.</span>status(<span style="color:#ff7800">Response</span><span style="color:#ff7800">.</span><span style="color:#ff7800">Status</span><span style="color:#3b5bb5"><span style="color:#ff7800">.</span>BAD_REQUEST</span>)<span style="color:#ff7800">.</span>build();
       }
   } <span style="color:#ff7800">else</span> {
       <span style="color:#ff7800">return</span> <span style="color:#ff7800">Response</span><span style="color:#ff7800">.</span>status(<span style="color:#ff7800">Response</span><span style="color:#ff7800">.</span><span style="color:#ff7800">Status</span><span style="color:#3b5bb5"><span style="color:#ff7800">.</span>FORBIDDEN</span>)<span style="color:#ff7800">.</span>build();
   }
}

</pre>

	<p>Here a check is made to see if the debit amount is above a <code>bigSpenderLimit</code> value, and if true, and the caller has the “BigSpender” role, is the debit amount under the “spendingLimit” claim value from the <code>JsonWebToken</code>.</p> 
	
<pre style="background:#fff;color:#000"><span style="color:#ff7800">The</span> warningLimitInstance is used in the generateBalanceInfo method<span style="color:#ff7800">:</span>
<span style="color:#ff7800">Optional&lt;<span style="color:#ff7800">JsonNumber</span>></span> warningLimit <span style="color:#ff7800">=</span> warningLimitInstance<span style="color:#ff7800">.</span>get();
<span style="color:#ff7800">if</span> (warningLimit<span style="color:#ff7800">.</span>isPresent()) {
   <span style="color:#ff7800">if</span> (warningLimit<span style="color:#ff7800">.</span>get()<span style="color:#ff7800">.</span>doubleValue() <span style="color:#ff7800">></span> usdBalance<span style="color:#ff7800">.</span>doubleValue()) {
       <span style="color:#ff7800">String</span> warningMsg <span style="color:#ff7800">=</span> <span style="color:#ff7800">String</span><span style="color:#ff7800">.</span>format(<span style="color:#409b1c">"balance is below warning limit: %s"</span>, 
warningLimit<span style="color:#ff7800">.</span>get());
       result<span style="color:#ff7800">.</span>add(<span style="color:#409b1c">"warning"</span>, warningMsg);
   }
}

</pre>

<p>If the caller’s JsonWebToken has a “warningLimit” claim value, that value is compared to the current balance and if the balance is below the warningLimit, a warning message is added to the balance info JsonObject.</p>

<h3>Role Handling</h3>
<p>To understand how the MP-JWT token is used for authorization in the context of the Java EE security annotations used on the <code>MySecureWallet</code> endpoint, let’s first look at the Token1.json payload:</p>

<p style="color:grey">Listing 3, Token1.json</p>
<pre style="background:#fff;color:#000">{
 <span style="color:#409b1c">"iss"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"https://server.example.com"</span>,
 <span style="color:#409b1c">"jti"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"a-123"</span>,
 <span style="color:#409b1c">"sub"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"24400320"</span>,
 <span style="color:#409b1c">"upn"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"jdoe@example.com"</span>,
 <span style="color:#409b1c">"preferred_username"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"jdoe"</span>,
 <span style="color:#409b1c">"aud"</span><span style="color:#ff7800">:</span> <span style="color:#409b1c">"wallet"</span>,
 <span style="color:#409b1c">"exp"</span><span style="color:#ff7800">:</span> <span style="color:#3b5bb5">1311281970</span>,
 <span style="color:#409b1c">"iat"</span><span style="color:#ff7800">:</span> <span style="color:#3b5bb5">1311280970</span>,
 <span style="color:#409b1c">"auth_time"</span><span style="color:#ff7800">:</span> <span style="color:#3b5bb5">1311280969</span>,
 <span style="color:#409b1c">"groups"</span><span style="color:#ff7800">:</span> [
   <span style="color:#409b1c">"ViewBalance"</span>, <span style="color:#409b1c">"Debtor"</span>, <span style="color:#409b1c">"Creditor"</span>
 ],
 <span style="color:#409b1c">"spendingLimit"</span><span style="color:#ff7800">:</span> <span style="color:#3b5bb5">2500</span>,
 <span style="color:#409b1c">"warningLimit"</span><span style="color:#ff7800">:</span> <span style="color:#3b5bb5">90000</span>
}

</pre>

	<p>Here the “groups” claim is what conveys the base RBAC information. This provides the names of the groups(collections of role names) that the caller bearing the token is granted. The various @RolesAllowed uses define the role names that are allowed to access a protected endpoint. It was mentioned earlier that an MP-JWT implementation is required to provide a one-to-one mapping between the names in the “groups” claim to role names. Therefore, this token bearer will have at least the <span style="color:green">"ViewBalance", "Debtor", "Creditor"</span> role names. MP-JWT containers are free to provide additional group to role mapping configuration. How this is done is specific to the container implementation, as this is a feature of Java EE security that was defined to be an implementation detail. In Wildfly-Swarm, this can be done by configuring a JAAS stack. The resources/project-defaults.yml file contains the following security.security-domains.jwt-jaspi.jaspi-authentication subtree:</p>
	
<pre style="background:#fff;color:#000">login<span style="color:#ff7800">-</span>module<span style="color:#ff7800">-</span>stacks<span style="color:#ff7800">:</span>
 roles<span style="color:#ff7800">-</span>lm<span style="color:#ff7800">-</span>stack<span style="color:#ff7800">:</span>
   login<span style="color:#ff7800">-</span>modules<span style="color:#ff7800">:</span>
     # <span style="color:#ff7800">This</span> stack performs the token verification and group to role mapping
     <span style="color:#ff7800">-</span> login<span style="color:#ff7800">-</span>module<span style="color:#ff7800">:</span> rm
       code<span style="color:#ff7800">:</span> <span style="color:#ff7800">org.wildfly.swarm.mpjwtauth.deployment.auth.jaas<span style="color:#ff7800">.</span>JWTLoginModule</span>
       flag<span style="color:#ff7800">:</span> required
       module<span style="color:#ff7800">-</span>options<span style="color:#ff7800">:</span>
         rolesProperties<span style="color:#ff7800">:</span> jwt<span style="color:#ff7800">-</span>roles<span style="color:#ff7800">.</span>properties

</pre>

<p>This defines a JWTLoginModule which is a JAAS login module that does authentication of the MP-JWT token by verifying the issuer, signer and expiration. It also performs the one-to-one group name to role name mapping of the token “groups” claim, and will augment the roles with any additional group name to role name mapping that is specified in the jwt-roles.properties file found in the deployment classpath. If you look at the resources/jwt-roles.properties file, it contains this entry:</p>

	<p><span style="color:blue">Debtor</span>=<span style="color:green">BigSpender</span></p>
	
	
<p>This says that the “Debtor” group will be assigned the role name “BigSpender”. This is in addition to the role name “Debtor”. If you look through all of the various *.json files for the test token contents, there is no “groups” claim that has the “BigSpender” name. That role name, which we saw being used in the debit endpoint logic, is assigned to tokens with the “Debitor” group through this secondary mapping.
</p>
		
	<h3>The MySecureWalletTest Code</h3>
We will look at the deployment creation and the bigSpenderDebitBalanceFail test to get a feel for how the test code works. An Arquillian test of a container has a deployment archive that contains the application code and resources being tested. This is the deployment creation method for <code>MySecureWalletTest</code>:

<pre style="background:#fff;color:#000"><span style="color:#8c868f">/**
* Create a CDI aware JAX-RS application archive with our endpoints and
* <span style="color:#ff7800">@return</span> the JAX-RS application archive
* <span style="color:#ff7800">@throws</span> IOException - on resource failure
*/</span>
@Deployment(testable<span style="color:#ff7800">=</span><span style="color:#3b5bb5">true</span>)
<span style="color:#ff7800">public</span> <span style="color:#ff7800">static</span> <span style="color:#ff7800">WebArchive</span> createDeployment() throws <span style="color:#ff7800">IOException</span> {
   <span style="color:#8c868f">// Various system properties you can set to enable debug logging, debugging</span>
   <span style="color:#8c868f">//System.setProperty("swarm.resolver.offline", "true");</span>
   <span style="color:#8c868f">//System.setProperty("swarm.logging", "DEBUG");</span>
   <span style="color:#8c868f">//System.setProperty("swarm.debug.port", "8888");</span>

   <span style="color:#8c868f">// Get the public key of the token signer</span>
   <span style="color:#ff7800">URL</span> publicKey <span style="color:#ff7800">=</span> <span style="color:#ff7800">MySecureWalletTest</span><span style="color:#ff7800">.</span>class<span style="color:#ff7800">.</span>getResource(<span style="color:#409b1c">"/publicKey.pem"</span>);
   <span style="color:#ff7800">WebArchive</span> webArchive <span style="color:#ff7800">=</span> <span style="color:#ff7800">ShrinkWrap</span>
           .create(<span style="color:#ff7800">WebArchive</span><span style="color:#ff7800">.</span>class, <span style="color:#409b1c">"MySecureEndpoint.war"</span>)
           <span style="color:#8c868f">// Place the public key in the war as /MP-JWT-SIGNER - Wildfly-Swarm specific</span>
           .addAsManifestResource(publicKey, <span style="color:#409b1c">"/MP-JWT-SIGNER"</span>)
           .addClass(<span style="color:#ff7800">MySecureWallet</span><span style="color:#ff7800">.</span>class)
           .addClass(<span style="color:#ff7800">MyJaxrsApp</span><span style="color:#ff7800">.</span>class)
           .addAsWebInfResource(<span style="color:#ff7800">EmptyAsset</span><span style="color:#3b5bb5"><span style="color:#ff7800">.</span>INSTANCE</span>, <span style="color:#409b1c">"beans.xml"</span>)
           <span style="color:#8c868f">// Add Wildfly-Swarm specific configuration of the security domain</span>
           .addAsResource(<span style="color:#409b1c">"project-defaults.yml"</span>, <span style="color:#409b1c">"/project-defaults.yml"</span>)
           .addAsWebInfResource(<span style="color:#409b1c">"jwt-roles.properties"</span>, <span style="color:#409b1c">"classes/jwt-roles.properties"</span>)
           .setWebXML(<span style="color:#409b1c">"WEB-INF/web.xml"</span>)
           ;
   <span style="color:#ff7800">System</span><span style="color:#ff7800">.</span>out<span style="color:#ff7800">.</span>printf(<span style="color:#409b1c">"WebArchive: %s<span style="color:#3b5bb5">\n</span>"</span>, webArchive<span style="color:#ff7800">.</span>toString(<span style="color:#3b5bb5">true</span>));
   <span style="color:#ff7800">return</span> webArchive;
}

</pre>

<p>The first part contains some commented out system property settings that turn on different Wildfly-Swarm behaviors that we will look at later. The real work of the createDeployment method is the creation of the webArchive. This is a web application archive abstraction which is built up to contain:</p>
   <ul>
    <li>the public key of the signer is placed</li>
    <li>the MySecureWallet and MyJaxrsApp classes</li>
    <li>the Wildfly-Swarm project-defaults.yml file we looked at a portion of earlier</li>
    <li>the jwt-roles.properties file used to augment the group to role name mapping we saw earlier</li>
    <li>A standard web.xml descriptor that specifies that the entire deployment should be secured. This is a current requirement for the Wildfly-Swarm MP-JWT implementation that will be removed in future releases.</li>
    </ul>

<p>The various unit test methods make use of the JAX-RS client API to build the REST web request and handle the responses. The <code>bigSpenderDebitBalanceFail</code> is given in Listing 4:</p>

<p style="color:grey">Listing 4, bigSpenderDebitBalanceFail test method</p>

<pre style="background:#000;color:#f8f8f8">
@<span style="color:#99cf50">RunAsClient</span>
@Test(description <span style="color:#e28964">=</span> <span style="color:#65b042">"Verify that jdoe cannot debit amount about the $2500 spendingLimit of Token1.json"</span>)
<span style="color:#99cf50">public</span> <span style="color:#99cf50">void</span> bigDebitBalanceFail() throws <span style="color:#99cf50">Exception</span> {
   <span style="color:#99cf50">Reporter</span><span style="color:#e28964">.</span>log(<span style="color:#65b042">"Begin bigDebitBalanceFail"</span>);
   <span style="color:#99cf50">String</span> token <span style="color:#e28964">=</span> <span style="color:#99cf50">TokenUtils</span><span style="color:#e28964">.</span>generateTokenString(<span style="color:#65b042">"/Token1.json"</span>);

   <span style="color:#aeaeae;font-style:italic">// First get the current balance</span>
   <span style="color:#99cf50">String</span> uri <span style="color:#e28964">=</span> baseURL<span style="color:#e28964">.</span>toExternalForm() <span style="color:#e28964">+</span> <span style="color:#65b042">"/wallet/balance"</span>;
   <span style="color:#99cf50">WebTarget</span> target <span style="color:#e28964">=</span> <span style="color:#99cf50">ClientBuilder</span><span style="color:#e28964">.</span>newClient()
           .target(uri);
   <span style="color:#99cf50">Response</span> response <span style="color:#e28964">=</span> target<span style="color:#e28964">.</span>request(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)<span style="color:#e28964">.</span>header(<span style="color:#99cf50">HttpHeaders</span><span style="color:#3387cc"><span style="color:#e28964">.</span>AUTHORIZATION</span>, <span style="color:#65b042">"Bearer "</span> <span style="color:#e28964">+</span> token)<span style="color:#e28964">.</span>get();
   <span style="color:#99cf50">JsonObject</span> origBalance <span style="color:#e28964">=</span> response<span style="color:#e28964">.</span>readEntity(<span style="color:#99cf50">JsonObject</span><span style="color:#e28964">.</span>class);
   <span style="color:#99cf50">Assert</span><span style="color:#e28964">.</span>assertTrue(origBalance<span style="color:#e28964">.</span>containsKey(<span style="color:#65b042">"usd"</span>));
   <span style="color:#99cf50">System</span><span style="color:#e28964">.</span>out<span style="color:#e28964">.</span>println(origBalance<span style="color:#e28964">.</span>toString());

   <span style="color:#aeaeae;font-style:italic">// Now try a big debit that is above the $2500 spendingLimit claim</span>
   uri <span style="color:#e28964">=</span> baseURL<span style="color:#e28964">.</span>toExternalForm() <span style="color:#e28964">+</span> <span style="color:#65b042">"/wallet/debit"</span>;
   target <span style="color:#e28964">=</span> <span style="color:#99cf50">ClientBuilder</span><span style="color:#e28964">.</span>newClient()
           .target(uri)
           .queryParam(<span style="color:#65b042">"amount"</span>, <span style="color:#65b042">"3000"</span>);
   response <span style="color:#e28964">=</span> target<span style="color:#e28964">.</span>request(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)<span style="color:#e28964">.</span>header(<span style="color:#99cf50">HttpHeaders</span><span style="color:#3387cc"><span style="color:#e28964">.</span>AUTHORIZATION</span>, <span style="color:#65b042">"Bearer "</span> <span style="color:#e28964">+</span> token)<span style="color:#e28964">.</span>get();
   <span style="color:#99cf50">Assert</span><span style="color:#e28964">.</span>assertEquals(response<span style="color:#e28964">.</span>getStatus(), <span style="color:#99cf50">HttpURLConnection</span><span style="color:#3387cc"><span style="color:#e28964">.</span>HTTP_BAD_REQUEST</span>);

   <span style="color:#aeaeae;font-style:italic">// Now retrieve the balance again to make sure it has not changed</span>
   uri <span style="color:#e28964">=</span> baseURL<span style="color:#e28964">.</span>toExternalForm() <span style="color:#e28964">+</span> <span style="color:#65b042">"/wallet/balance"</span>;
   target <span style="color:#e28964">=</span> <span style="color:#99cf50">ClientBuilder</span><span style="color:#e28964">.</span>newClient()
           .target(uri);
   response <span style="color:#e28964">=</span> target<span style="color:#e28964">.</span>request(<span style="color:#99cf50">MediaType</span><span style="color:#3387cc"><span style="color:#e28964">.</span>APPLICATION_JSON</span>)<span style="color:#e28964">.</span>header(<span style="color:#99cf50">HttpHeaders</span><span style="color:#3387cc"><span style="color:#e28964">.</span>AUTHORIZATION</span>, <span style="color:#65b042">"Bearer "</span> <span style="color:#e28964">+</span> token)<span style="color:#e28964">.</span>get();
   <span style="color:#99cf50">Assert</span><span style="color:#e28964">.</span>assertEquals(response<span style="color:#e28964">.</span>getStatus(), <span style="color:#99cf50">HttpURLConnection</span><span style="color:#3387cc"><span style="color:#e28964">.</span>HTTP_OK</span>);
   <span style="color:#99cf50">JsonObject</span> newBalance <span style="color:#e28964">=</span> response<span style="color:#e28964">.</span>readEntity(<span style="color:#99cf50">JsonObject</span><span style="color:#e28964">.</span>class);
   <span style="color:#99cf50">Reporter</span><span style="color:#e28964">.</span>log(newBalance<span style="color:#e28964">.</span>toString());
   <span style="color:#99cf50">System</span><span style="color:#e28964">.</span>out<span style="color:#e28964">.</span>println(newBalance<span style="color:#e28964">.</span>toString());
   <span style="color:#99cf50">Assert</span><span style="color:#e28964">.</span>assertEquals(origBalance<span style="color:#e28964">.</span>getJsonNumber(<span style="color:#65b042">"usd"</span>), newBalance<span style="color:#e28964">.</span>getJsonNumber(<span style="color:#65b042">"usd"</span>));
}

</pre>

<p>The method starts by generating the JWT using the TokenUtils class that is from the MP-JWT TCK artifacts. This transforms the Token1.json payload we saw in Listing 3, Token1.json into a signed and encoded string with valid header, along with updated issued at and expiration time claims.</p>

<p>The next step is to query the current balance. It then attempts to make a debit of $3,000 which is above the $2,500 spendingLimit claim value of Token1.json. We looked at this logic earlier. The test validates that this fails with a status code, and then re-queries the balance to validate that the debit did not go through.</p>

<p>The other test methods exercise debiting and crediting the wallet with different role grants and tokens. The description member of each @Test annotation gives the gist of what the test is supposed to do.</p>

<h3>Running Tests</h3>
<p>You can run the tests by pulling the project into your favorite IDE and making use of it’s TestNg integration, or by using maven test from a command line. To run the full set of test you would issue mvn test:</p>

<pre style="background:#211e1e;color:#dadada">
[eclipse<span style="color:#47b8d6">-</span>newsletter<span style="color:#47b8d6">-</span>sep<span style="color:#47b8d6">-</span><span style="color:#ccc">2017</span> <span style="color:#ccc">504</span>]$ mvn test
[<span style="color:#6782d3">INFO</span>] Scanning <span style="color:#6969fa;font-weight:700">for</span> projects<span style="color:#47b8d6">.</span><span style="color:#47b8d6">.</span>.
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] Building MicroProfile <span style="color:#6782d3">JWT</span> Auth WFSwarm Example <span style="color:#ccc">1.0</span><span style="color:#47b8d6">-</span><span style="color:#6782d3">SNAPSHOT</span>
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] 
.<span style="color:#47b8d6">.</span>.
[<span style="color:#6782d3">INFO</span>] Tests run<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">9</span>, Failures<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Errors<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Skipped<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Time elapsed<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">18.813</span> s <span style="color:#47b8d6">-</span> in org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWalletTest
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] Results<span style="color:#6969fa;font-weight:700">:</span>
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] Tests run<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">9</span>, Failures<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Errors<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Skipped<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] <span style="color:#6782d3">BUILD</span> <span style="color:#6782d3">SUCCESS</span>
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] Total time<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">22.525</span> s
[<span style="color:#6782d3">INFO</span>] Finished at<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span>07T13<span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">10</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">13</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">00</span>
[<span style="color:#6782d3">INFO</span>] Final Memory<span style="color:#6969fa;font-weight:700">:</span> 42M<span style="color:#47b8d6">/</span>455M
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[eclipse<span style="color:#47b8d6">-</span>newsletter<span style="color:#47b8d6">-</span>sep<span style="color:#47b8d6">-</span><span style="color:#ccc">2017</span> <span style="color:#ccc">505</span>]$

</pre>

<p>To see the gory details of the Wildfly-Swarm implementation behavior, incuding the CDI extension processing of the injection sites, the MP-JWT token handling, etc., use the -Dswarm.logging=DEBUG argument:</p>

<pre style="background:#211e1e;color:#dadada">
[eclipse<span style="color:#47b8d6">-</span>newsletter<span style="color:#47b8d6">-</span>sep<span style="color:#47b8d6">-</span><span style="color:#ccc">2017</span> <span style="color:#ccc">505</span>]$ mvn test <span style="color:#47b8d6">-</span>Dswarm<span style="color:#47b8d6">.</span>logging<span style="color:#47b8d6">=</span><span style="color:#6782d3">DEBUG</span>
[<span style="color:#6782d3">INFO</span>] Scanning <span style="color:#6969fa;font-weight:700">for</span> projects<span style="color:#47b8d6">.</span><span style="color:#47b8d6">.</span>.
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] Building MicroProfile <span style="color:#6782d3">JWT</span> Auth WFSwarm Example <span style="color:#ccc">1.0</span><span style="color:#47b8d6">-</span><span style="color:#6782d3">SNAPSHOT</span>
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] 
.<span style="color:#47b8d6">.</span>.
[org.wildfly.swarm.mpjwtauth.deployment.auth.cdi<span style="color:#47b8d6">.</span>MPJWTExtension] (<span style="color:#6782d3">MSC</span> service thread <span style="color:#ccc">1</span><span style="color:#47b8d6">-</span><span style="color:#ccc">7</span>) MPJWTExtension(), added JWTPrincipalProducer
.<span style="color:#47b8d6">.</span>.
[org.wildfly.swarm.mpjwtauth.deployment.auth.cdi<span style="color:#47b8d6">.</span>MPJWTExtension] (Weld Thread Pool <span style="color:#47b8d6">--</span> <span style="color:#ccc">2</span>) pipRaw<span style="color:#6969fa;font-weight:700">:</span> [BackedAnnotatedField] @Inject @Claim private org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWallet<span style="color:#47b8d6">.</span>warningLimitInstance
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">10</span>,<span style="color:#ccc">786</span> <span style="color:#6782d3">DEBUG</span> [org.wildfly.swarm.mpjwtauth.deployment.auth.cdi<span style="color:#47b8d6">.</span>MPJWTExtension] (Weld Thread Pool <span style="color:#47b8d6">--</span> <span style="color:#ccc">2</span>) pip<span style="color:#6969fa;font-weight:700">:</span> [BackedAnnotatedField] @Inject @Claim private org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWallet<span style="color:#47b8d6">.</span>warningLimitInstance
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">10</span>,<span style="color:#ccc">786</span> <span style="color:#6782d3">DEBUG</span> [org.wildfly.swarm.mpjwtauth.deployment.auth.cdi<span style="color:#47b8d6">.</span>MPJWTExtension] (Weld Thread Pool <span style="color:#47b8d6">--</span> <span style="color:#ccc">2</span>) Checking Provider Claim(warningLimit), ip<span style="color:#6969fa;font-weight:700">:</span> [BackedAnnotatedField] @Inject @Claim private org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWallet<span style="color:#47b8d6">.</span>warningLimitInstance
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">10</span>,<span style="color:#ccc">786</span> <span style="color:#6782d3">DEBUG</span> 
.<span style="color:#47b8d6">.</span>.
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">12</span>,<span style="color:#ccc">470</span> <span style="color:#6782d3">DEBUG</span> [io<span style="color:#47b8d6">.</span>undertow<span style="color:#47b8d6">.</span>request<span style="color:#47b8d6">.</span>security] (<span style="color:#6969fa;font-weight:700">default</span> task<span style="color:#47b8d6">-</span><span style="color:#ccc">45</span>) Attempting to authenticate HttpServerExchange{ <span style="color:#6782d3">GET</span> <span style="color:#47b8d6">/</span>wallet<span style="color:#47b8d6">/</span>debit request {Accept<span style="color:#47b8d6">=</span>[application<span style="color:#47b8d6">/</span>json], Connection<span style="color:#47b8d6">=</span>[Keep<span style="color:#47b8d6">-</span>Alive], Authorization<span style="color:#47b8d6">=</span>[Bearer eyJraWQiOiJcL3ByaXZhdGVLZXkucGVtIiwidHlwIjoiSldUIiwiYWxnIjoiUlMyNTYifQ<span style="color:#47b8d6">.</span>eyJzdWIiOiIyNDQwMDMyMCIsImF1ZCI6IndhbGxldCIsInVwbiI6Impkb2VAZXhhbXBsZS5jb20iLCJzcGVuZGluZ0xpbWl0IjoyNTAwLCJhdXRoX3RpbWUiOjE1MDQ4MTUyNTIsImlzcyI6Imh0dHBzOlwvXC9zZXJ2ZXIuZXhhbXBsZS5jb20iLCJncm91cHMiOlsiVmlld0JhbGFuY2UiLCJEZWJ0b3IiLCJDcmVkaXRvciJdLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJqZG9lIiwid2FybmluZ0xpbWl0Ijo1MDAwMCwiZXhwIjoxNTA0ODE1NTUyLCJpYXQiOjE1MDQ4MTUyNTIsImp0aSI6ImEtMTIzIn0<span style="color:#47b8d6">.</span>damWAS3kRobtORXO3INFOMGy8AXKg4FLVdwR<span style="color:#47b8d6">-</span>vJaCf47Cesr7kokp8y1VLc2GLCJnrB68dcLQ05qAYeYGMKnQuMNHVoB8aL1gpeJz_abJx72UrsAXrPcWAx6fVtKjyGF3GQ4onEvKUwo0puR<span style="color:#47b8d6">-</span>rbeijPyDqUcnUibfP91_2Wo4F72GK1fyqJKlLTwsUsqYI9OizVR1f3C6wBdGzsOSm50e<span style="color:#47b8d6">-</span>FpqMZLI6A4vzOrPzV1RaIa7wpAi<span style="color:#47b8d6">-</span>oGa2Xr1<span style="color:#47b8d6">-</span>0JYNUqpOjzjQSrYY_urv3NfX7lRU3i34wb02ixBxi4cgL4qkwyFhr8s9HPrg8U<span style="color:#47b8d6">-</span>zqYSYspWMmjCYLsuwC4_QLtgG<span style="color:#47b8d6">-</span>x_g], User<span style="color:#47b8d6">-</span>Agent<span style="color:#47b8d6">=</span>[Apache<span style="color:#47b8d6">-</span>HttpClient<span style="color:#47b8d6">/</span><span style="color:#ccc">4.5</span><span style="color:#ccc">.2</span> (Java<span style="color:#47b8d6">/</span><span style="color:#ccc">1.8</span><span style="color:#47b8d6">.</span>0_121)], Host<span style="color:#47b8d6">=</span>[localhost<span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">8080</span>]} response {Expires<span style="color:#47b8d6">=</span>[<span style="color:#ccc">0</span>], Cache<span style="color:#47b8d6">-</span>Control<span style="color:#47b8d6">=</span>[no<span style="color:#47b8d6">-</span>cache, no<span style="color:#47b8d6">-</span>store, must<span style="color:#47b8d6">-</span>revalidate], Pragma<span style="color:#47b8d6">=</span>[no<span style="color:#47b8d6">-</span>cache]}}, authentication required<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#de8e30;font-weight:700">true</span>
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">12</span>,<span style="color:#ccc">470</span> <span style="color:#6782d3">DEBUG</span> [org<span style="color:#47b8d6">.</span>wildfly<span style="color:#47b8d6">.</span>extension<span style="color:#47b8d6">.</span>undertow] (<span style="color:#6969fa;font-weight:700">default</span> task<span style="color:#47b8d6">-</span><span style="color:#ccc">45</span>) validateRequest <span style="color:#6969fa;font-weight:700">for</span> layer [HttpServlet] and applicationContextIdentifier [<span style="color:#6969fa;font-weight:700">default</span><span style="color:#47b8d6">-</span>host ]
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">12</span>,<span style="color:#ccc">472</span> <span style="color:#6782d3">DEBUG</span> [io<span style="color:#47b8d6">.</span>undertow<span style="color:#47b8d6">.</span>request<span style="color:#47b8d6">.</span>security] (<span style="color:#6969fa;font-weight:700">default</span> task<span style="color:#47b8d6">-</span><span style="color:#ccc">45</span>) Authenticated as jdoe@example<span style="color:#47b8d6">.</span>com, roles [Debtor, ViewBalance, BigSpender, Creditor]
<span style="color:#ccc">2017</span><span style="color:#47b8d6">-</span><span style="color:#ccc">09</span><span style="color:#47b8d6">-</span><span style="color:#ccc">07</span> <span style="color:#ccc">13</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">14</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">12</span>,<span style="color:#ccc">472</span> <span style="color:#6782d3">DEBUG</span> [io<span style="color:#47b8d6">.</span>undertow<span style="color:#47b8d6">.</span>request<span style="color:#47b8d6">.</span>security] (<span style="color:#6969fa;font-weight:700">default</span> task<span style="color:#47b8d6">-</span><span style="color:#ccc">45</span>) 

</pre>
<p>You can run an individual test by passing a -Dtest=... argument like:</p>

<pre style="background:#211e1e;color:#dadada">
[eclipse<span style="color:#47b8d6">-</span>newsletter<span style="color:#47b8d6">-</span>sep<span style="color:#47b8d6">-</span><span style="color:#ccc">2017</span> <span style="color:#ccc">506</span>]$ mvn <span style="color:#47b8d6">-</span>Dtest<span style="color:#47b8d6">=</span>org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWalletTest#bigDebitBalanceFail test
[<span style="color:#6782d3">INFO</span>] Scanning <span style="color:#6969fa;font-weight:700">for</span> projects<span style="color:#47b8d6">.</span><span style="color:#47b8d6">.</span>.
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
[<span style="color:#6782d3">INFO</span>] Building MicroProfile <span style="color:#6782d3">JWT</span> Auth WFSwarm Example <span style="color:#ccc">1.0</span><span style="color:#47b8d6">-</span><span style="color:#6782d3">SNAPSHOT</span>
[<span style="color:#6782d3">INFO</span>] <span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span><span style="color:#47b8d6">--</span>
.<span style="color:#47b8d6">.</span>.
{<span style="color:#ad9361">"usd"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">100000.0000</span>,<span style="color:#ad9361">"bitcoin"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">22.0361</span>,<span style="color:#ad9361">"ethereum"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">304.8780</span>}
{<span style="color:#ad9361">"usd"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">100000.0000</span>,<span style="color:#ad9361">"bitcoin"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">22.0361</span>,<span style="color:#ad9361">"ethereum"</span><span style="color:#6969fa;font-weight:700">:</span><span style="color:#ccc">304.8780</span>}
.<span style="color:#47b8d6">.</span>.
[<span style="color:#6782d3">INFO</span>] Tests run<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">1</span>, Failures<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Errors<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Skipped<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Time elapsed<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">18.581</span> s <span style="color:#47b8d6">-</span> in org.eclipse.microprofile.test.jwt<span style="color:#47b8d6">.</span>MySecureWalletTest
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] Results<span style="color:#6969fa;font-weight:700">:</span>
[<span style="color:#6782d3">INFO</span>] 
[<span style="color:#6782d3">INFO</span>] Tests run<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">1</span>, Failures<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Errors<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>, Skipped<span style="color:#6969fa;font-weight:700">:</span> <span style="color:#ccc">0</span>

</pre>

<h2>Future Directions</h2>
<p>In future versions of the API we would like to address service specific role and group claims. The "resource_access" claim originally targeted for the 1.0 release of the specification has been postponed as addition work to determine the format of the claim key as well as how these claims would be surfaced through the standard Java EE APIs needs more work than the 1.0 release timeline will allow.</p>

<p>For reference, a somewhat related extension to the OAuth 2.0 spec <a target="_blank" href="https://tools.ietf.org/html/draft-campbell-oauth-resource-indicators-02">Resource Indicators for OAuth 2.0</a> has not gained much traction. The key point it makes that seems relevant to our investigation is that the service specific grants most likely need to be specified using URIs for the service endpoints. How these endpoints map to deployment virtual hosts and partial, wildcard, etc. URIs needs to be determined.</p>

<p>Additional items include:</p>
        <ul>
       <li>are the standard definition of some type of JsonWebToken factory</li>
        <li>Integration with the MicroProfile Config specification for the configuration of the whitelisted issuers and associated public keys for validation of tokens.</li>
        <li>Integration with the MicroProfile Config specification to standardize things like group to role mapping, and other security configurations that are currently implementation specific.</li>
        <li>Support for Java EE 8 JSR-375 security interfaces</li>
        </ul>

<h2>Resources</h2>
<ul>
<li>The Eclipse MicroProfile JWT RBAC Repository contains the API, specification and TCK.</li>
<li>The article example code</li>
</ul>

<h3>MP-JWT Implementations</h3>
<p>This is a list of current implementations of the MP-JWT feature that are either underway or being planned:</p>
<ul>
<li><a target="_blank" href="https://hammock-project.github.io/">Hammock</a>
<li><a target="_blank" href="https://github.com/MicroProfileJWT/wildfly-swarm-mp1.2">Wildfly-Swarm</a>
<li><a target="_blank" href="http://www.tomitribe.com/">TomiTribe</a>
<li><a target="_blank" href="https://developer.ibm.com/wasdev/downloads/">WAS Liberty</a>
<li><a target="_blank" href="https://ee.kumuluz.com/">KumuluzEE</a>
<li><a target="_blank" href="https://www.payara.fish/">Payara</a>
</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/scott.png"
        alt="Scott Stark" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Scott Stark<br />
            <a target="_blank" href="https://www.redhat.com/">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://developer.jboss.org/blogs/scott.stark">Blog</a></li>
          <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

