<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>


<h2>Building a resilient microservice?</h2>
<p>No need to ask why. Of course, everyone dreams about building a robust and resilient microservice. It can run all the time no matter where and what.</p>
<p>How? The obvious answer is to use <a target="_blank" href="https://github.com/Netflix/Hystrix">Hystrix</a> or <a target="_blank" href="https://github.com/jhalterman/failsafe">Failsafe</a>. However, the third party library usage will clatter your business code. You need to learn about these libraries. If you need to change some Fault Tolerance policies, you have to repackage your microservice.</p>
<p>Anything better out there? MicroProfile Fault Tolerance is the new future to build a resilient microservice.</p>

<h2>What is Eclipse MicroProfile Fault Tolerance?</h2>
<p><a target="_blank" href="https://github.com/eclipse/microprofile-fault-tolerance">Eclipse MicroProfile Fault Tolerance</a> provides a simple and flexible solution to build a Fault Tolerance microservice, which is easy to use and configurable. It offers the following Fault Tolerance policies:</p>
	<ul>
        <li><b>Timeout:</b> Define a duration for timeout.</li>
        <li><b>Retry:</b> Define a criteria on when to retry.</li>
        <li><b>Fallback:</b> provide an alternative solution for a failed execution.</li>
        <li><b>Bulkhead:</b> isolate failures in part of the system while the rest part of the system can still function.</li>
        <li><b>CircuitBreaker:</b> offer a way of fail fast by automatically failing execution to prevent the system overloading and indefinite wait or timeout by the clients.</li>
        <li><b>Asynchronous:</b> invoke the operation asynchronously.</li>
	</ul>
<p>The main design is to separate execution logic from execution. The execution can be configured with fault tolerance policies.</p>
<p>Eclipse MicroProfile Fault Tolerance introduces the following annotations for the corresponding Fault Tolerance policies:</p>
	<ul>
        <li>Timeout</li>
        <li>Retry</li>
        <li>Fallback</li>
        <li>Bulkhead</li>
        <li>CircuitBreaker</li>
        <li>Asynchronous</li>
	</ul>
	
	<p>All you need to do is to add these annotations to the methods or bean classes you would like to achieve fault tolerance.</p>
	<p>This project started in April 2017 with <a target="_blank" href="https://github.com/eclipse/microprofile-fault-tolerance/graphs/contributors">10 amazing contributors</a>. We have relesed MicroProfile Fault Tolerance 1.0.</p>
	<p>MicroProfile Fault Tolerance does not contain an implementation itself but provides the specified API, TCK and documentation.</p>
	
    <p>This is a list of current implementations of MicroProfile Fault Tolerance feature that are either underway or being planned:</p>
    <ul>
        <li><a target="_blank" href="https://developer.ibm.com/wasdev/">WebSphere Liberty</a></li>
        <li>Wildfly Swarm</li>
        <li><a target="_blank" href="https://www.payara.fish">Payara Server</a></li> 
        <li>Apache Safeguard</li>
        <li><a target="_blank" href="https://github.com/kumuluz/kumuluzee-fault-tolerance">KumuluzEE Fault Tolerance</a></li>
	</ul>


<h2>How to use MicroProfile Fault Tolerance?</h2>
<p>Apply Fault Tolerance annotations on the CDI bean classes or methods. Below are some examples:

	<h3>1. Retry</h3>
	<p>In order to recover from a brief network glitch, <span style="color:red">Retry</span> can be used to invoke the same operation again. The @Retry annotation is to achieve this and it can be applied to Class level or method level.</p>
	
<pre style="background:#211e1e;color:#dadada"><span style="color:#555">/**
* The configured the max retries is 90 but the max duration is 1000ms.
* Once the duration is reached, no more retries should be performed,
* even through it has not reached the max retries.
*/</span>
@Retry(maxRetries <span style="color:#47b8d6">=</span> <span style="color:#ccc">90</span>, maxDuration<span style="color:#47b8d6">=</span> <span style="color:#ccc">1000</span>)
public void serviceB() {
    writingService();
}
<span style="color:#555">/**
* There should be 0-800ms (jitter is -400ms - 400ms) delays
* between each invocation.
* there should be at least 4 retries but no more than 10 retries.
*/</span>
@Retry(delay <span style="color:#47b8d6">=</span> <span style="color:#ccc">400</span>, maxDuration<span style="color:#47b8d6">=</span> <span style="color:#ccc">3200</span>, jitter<span style="color:#47b8d6">=</span> <span style="color:#ccc">400</span>, maxRetries <span style="color:#47b8d6">=</span> <span style="color:#ccc">10</span>)
public Connection serviceA() {
    <span style="color:#6969fa;font-weight:700">return</span> connectionService();
}
<span style="color:#555">/**
* Sets retry condition, which means Retry will be performed on
* IOException.
*/</span>
@Retry(retryOn <span style="color:#47b8d6">=</span> {IOException<span style="color:#47b8d6">.</span>class})
public void serviceB() {
    writingService();
}

</pre>

	<h3>2. Timeout</h3> 
	
	<p><span style="color:red">Timeout</span> prevents from the execution from waiting forever. @Timeout is used to specify a timeout and it can be used on methods or class.</p>

   <pre style="background:#211e1e;color:#dadada">@Timeout(<span style="color:#ccc">400</span>) <span style="color:#555">// timeout is 400ms</span>
   public Connection serviceA() {
       Connection conn = <span style="color:#de8e30;font-weight:700">null</span>;
       conn = connectionService();
       <span style="color:#6969fa;font-weight:700">return</span> conn;
}

</pre>

<p>When a timeout occurs, a TimeoutException will be thrown.</p>

<h3>3. CircuitBreaker</h3>
<p>Circuit Breaker prevents repeating timeout, so that invoking dysfunctional services or APIs fail fast. Applying @CircuitBreaker on method or class level will have CircuitBreaker applied.</p>


<pre style="background:#211e1e;color:#dadada">@CircuitBreaker(successThreshold = <span style="color:#ccc">10</span>, requestVolumeThreshold = <span style="color:#ccc">4</span>, failureRatio=<span style="color:#ccc">0.75</span>,delay = <span style="color:#ccc">1000</span>)
public Connection serviceA() {
       Connection conn = <span style="color:#de8e30;font-weight:700">null</span>;
       conn = connectionService();
       <span style="color:#6969fa;font-weight:700">return</span> conn;
}

</pre>

<p>The above code-snippet means the method serviceA applies the <span style="color:red">CircuitBreaker</span> policy, which is to open the circuit once 3 (4x0.75) failures occur among the rolling window of 4 consecutive invocations. The circuit will stay open for 1000ms and then back to half open. After 10 consecutive successful invocations, the circuit will be back to close again. When a circuit is open, A <span style="color:red">CircuitBreakerOpenException</span> will be thrown.</p>

<h3>4. Bulkhead</h3>

<p>The <span style="color:red">Bulkhead</span> pattern is to prevent faults in one part of the system from cascading to the entire system, which might bring down the whole system. The implementation is to limit the number of concurrent requests accessing to an instance.</p>
 
<p>There are two different approaches to the bulkhead: thread pool isolation and semaphore isolation.</p>

<h4>Semaphore style Bulkhead</h4>
<p>Annotating a method or a class with <span style="color:red">@Bulkhead</span> applies a semaphore style bulkhead, which allows the specified concurrent number of requests.</p>

<pre style="background:#211e1e;color:#dadada">@Bulkhead(<span style="color:#ccc">5</span>) <span style="color:#555">// maximum 5 concurrent requests allowed</span>
public Connection serviceA() {
       Connection conn = <span style="color:#de8e30;font-weight:700">null</span>;
       conn = connectionService();
       <span style="color:#6969fa;font-weight:700">return</span> conn;
}
</pre>

<p><b>Thread pool style Bulkhead</b></p>
<p>When <span style="color:red">@Bulkhead</span> is used with <span style="color:red">@Asynchronous</span>, the thread pool isolation approach will be used. The thread pool approach allows to configure the maximum concurrent requests together with the waiting queue size. The semaphore approach only allows the concurrent number of requests configuration. <span style="color:red">@Asynchronous</span> causes an invocation to be executed by a different thread.</p>

<pre style="background:#211e1e;color:#dadada"><span style="color:#555">// maximum 5 concurrent requests allowed, maximum 8 requests allowed in the waiting queue</span>
@Asynchronous
@Bulkhead(value = <span style="color:#ccc">5</span>, waitingTaskQueue = <span style="color:#ccc">8</span>)
public Future&lt;Connection> serviceA() {
Connection conn = <span style="color:#de8e30;font-weight:700">null</span>;
conn = connectionService();
<span style="color:#6969fa;font-weight:700">return</span> CompletableFuture.completedFuture(conn);
}

</pre>

<h3>5. Fallback</h3>
<p>Most previous annotations increase the success rate in method invocation. However, they cannot completely eliminate exception. Exception should still be dealt with. Often it is useful to fall back to a different operation on a dysfunctional operation. A method can be annotated with <span style="color:red">@Fallback</span>, which means the method will have Fallback policy applied.</p>

<pre style="background:#211e1e;color:#dadada">@Retry(maxRetries <span style="color:#47b8d6">=</span> <span style="color:#ccc">1</span>)
@Fallback(StringFallbackHandler<span style="color:#47b8d6">.</span>class)
public String serviceA() {
	 <span style="color:#6969fa;font-weight:700">return</span> nameService();
}

</pre>

<p>In the above code snippet, when the method fails and retry reaches its maximum retry, the fallback operation will be performed. The method <span style="color:red">StringFallbackHandler.handle(ExecutionContext context)</span> will be invoked. The return type of <span style="color:red">StringFallbackHandler.handle(ExecutionContext context)</span> must match the return type of serviceA().</p>

<p>If a fallback method is declared on the same class as the method that specified with @Fallback. Use this following way to specify fallback.</p>

<pre style="background:#211e1e;color:#dadada">@Retry(maxRetries = <span style="color:#ccc">2</span>)
@Fallback(fallbackMethod= <span style="color:#ad9361">"fallbackForServiceB"</span>)
public String serviceB() {
	 counterForInvokingServiceB<span style="color:#47b8d6">++</span>;
	 <span style="color:#6969fa;font-weight:700">return</span> nameService();
}
private String fallbackForServiceB() {
<span style="color:#6969fa;font-weight:700">return</span> <span style="color:#ad9361">"myFallback"</span>;
}

</pre>


<p>The above code snippet means when the method failed and retry reaches its maximum retry, the method <span style="color:red">fallbackForServiceB</span> will be invoked. The return type of <span style="color:red">fallbackForServiceB</span> must be <span style="color:red">String</span> and the argument list for <span style="color:red">fallbackForServiceB</span> must be the same as <span style="color:red">ServiceB</span>.</p>
 
<p>The annotations declared by MicroProfile Fault Tolerance can be used in combination.</p>
 
<p>When reading till here, you might think. So all annotations. They are all static. Is it possible to configure them without repackaging the app?</p>

<h2>Configure MicroProfile Fault Tolerance</h2>
<p>All of the annotation parameters are configurable. This specification directly depends on <a href="https://www.eclipse.org/community/eclipse_newsletter/2017/september/article3.php">MicroProfile Config</a> to configure the parameters.</p>
 
<p>The annotation parameters can be overwritten via config properties in the naming convention of <span style="color:red">&lt;classname&gt;/&lt;methodname&gt;/&lt;annotation&gt;/&lt;parameter&gt;</span>. To override the <span style="color:red">maxDuration</span> for ServiceA, set the config property</p>
<p><code>com.acme.test.MyClient/serviceA/Retry/maxDuration=3000</code></p>
 
<p>If the parameters for a particular annotation need to be configured with the same value for a particular class, use the config property <span style="color:red">&lt;classname>/&lt;annotation&gt;/&lt;parameter&gt;</span> for configuration.</p>
<p>For an instance, use the following config property to override all <span style="color:red">maxRetries</span> for <span style="color:red">Retry</span> specified on the class <span style="color:red">MyClient</span> to 100.</p>
<p><code>com.acme.test.MyClient/Retry/maxRetries=100</code></p>
 
<p>Sometimes, the parameters need to be configured with the same value for the whole microservice.</p>
<p>For an instance, all Timeout needs to be set to 100ms. It can be cumbersome to override each occurrence of Timeout. In this circumstance, the config property <span style="color:red">&lt;annotation&gt;/&lt;parameter&gt;</span> overrides the corresponding parameter value for the specified annotation. For instance, in order to override the maxRetries for the Retry to be 30, specify the config property</p>
<p><code>Retry/maxRetries=30</code></p>


<h2>Special Feature in MicroProfile Fault Tolerance</h2>
 
<p>Have you ever thought under some situations you would like to turn off Fault Tolerance? Can you achieve this by using the current third-party Fault Tolerance libraries? Probably not.</p>
 
<p>MicroProfile Fault Tolerance offers you a switch to turn off all other annotations except Fallback via the config property <span style="color:red">MP_Fault_Tolerance_NonFallback_Enabled</span>. This feature is particular useful in some service mesh architecture e.g. <a target="_blank" href="https://istio.io">Istio</a>. Istio is a robust service mesh for microservices. The project was started by teams from Google and IBM, in partnership with the Envoy team at Lyft.</p>
<p>Istio offers Fault Tolerance aspects such as Retry, Circuit Breaker etc. Any microservice with Fault Tolerance integration will run into conflicts with Istio’s Fault Tolerance policies such as Retries and Timeout. For instance, if a microservice has a maxRetires configured to be 3 and Istio configured to be 5, 15 retries will be performed. This is not what you would expect. MicroProfile Fault Tolerance provides a way to solve this. Setting the property <span style="color:red">MP_Fault_Tolerance_NonFallback_Enabled</span> with the value of <span style="color:red">false</span> turns off all Fault Tolerance policies apart from Fallback. Therefore, the microservice with MicroProfile Fault Tolerance can utilise Istio’s Fault Tolerance without any issues.</p>
 
<p>The API and TCK jars can be found from maven central</p>
<ul>
<li><a target="_blank" href="https://mvnrepository.com/artifact/org.eclipse.microprofile.fault-tolerance/microprofile-fault-tolerance-api">https://mvnrepository.com/artifact/org.eclipse.microprofile.fault-tolerance/microprofile-fault-tolerance-api</a></li>
<li><a target="_blank" href="https://mvnrepository.com/artifact/org.eclipse.microprofile.fault-tolerance/microprofile-fault-tolerance-tck">https://mvnrepository.com/artifact/org.eclipse.microprofile.fault-tolerance/microprofile-fault-tolerance-tck</a></li>
</ul>
 
<p>The specification can be accessed from <a target="_blank" href="https://github.com/eclipse/microprofile-fault-tolerance/releases">here</a>.</p>


<h2>Troubleshooting</h2>
<p>The annotations <span style="color:red">@Asynchronous</span>, <span style="color:red">@Bulkhead</span>, <span style="color:red">@CircuitBreaker</span>, <span style="color:red">@Fallback</span>, <span style="color:red">@Retry</span> and <span style="color:red">@Timeout</span> are all interceptor bindings. All annotations apart from <span style="color:red">@Fallback</span>, can bound at the class level or method level where <span style="color:red">@Fallback</span> can only be bound at method level.</p>
 
<p>Since this specification depends on CDI and interceptors specifications, fault tolerance operations have the following restrictions:</p>
 
 <ul>
		<li>Fault tolerance interceptors bindings must applied on a bean class or bean class method otherwise it is ignored,</li>
		<li>invocation must be business method invocation as defined in <a target="_blank" href="http://docs.jboss.org/cdi/spec/1.2/cdi-spec.html#biz_method">CDI specification</a>.</li>
		<li>if a method and its containing class don’t have any fault tolerance interceptor binding, it won’t be considered as a fault tolerance operation</li>
</ul>

<h2>Where to go next?</h2>
<p>We have released Fault Tolerance 1.0 in September 2017 and plan to work through the <a target="_blank" href="https://github.com/eclipse/microprofile-fault-tolerance/issues">issues</a> on MicroProfile Fault Tolerance repo. If you would like to see new features in the next release, please log some issues there. We have weekly hangout to discuss the design issues. Please import the MicroProfile Calendar, which can be found from <a target="_blank" href="https://wiki.eclipse.org/MicroProfile">MicroProfile wiki</a>. Hope to meet you on the hangout!</p>
 
 
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/emily.jpg"
         alt="Emily Jiang" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Emily Jiang<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/emily-jiang-60803812/">LinkedIn</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/emilyfhjiang">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

