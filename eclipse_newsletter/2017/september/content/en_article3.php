<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

	<h2>Why do I bother with Configuration?</h2>
<p>Separating configuration from code is not a new concept and neither is the need for applications to be configured differently when running on different environments. In recent years the desire to package immutable applications in containers, into which external configuration can be injected, has become a best practice for microservices. There are many strategies to achieve this so that applications do not need to be repackaged when their underlying running environment changes. Microservices are designed to be moved around and run in multiple environments. How do we enable microservices to run in multiple environments without modification? The portable externalization of configuration is key.</p>

	<h2>What is Eclipse MicroProfile Config?</h2>
<p>Eclipse <a target="_blank" href="https://github.com/eclipse/microprofile-config">MicroProfile Config</a> is a solution to externalise configuration from microservices. The config properties are provided by <code>ConfigSources</code>. <code>ConfigSources</code> are ordered according to their ordinal, with a higher value taking priority over a lower one. For example, in Websphere Liberty, this means that if the same config is set in server.env (ordinal=300) and in microprofile-config.properties (ordinal=100), then the value from server.env will be used. By default, there are 3 default <code>ConfigSources</code>:</p>
	<ul>
        <li><code>System.getProperties() (ordinal=400)</code></li>
        <li><code>System.getenv() (ordinal=300)</code></li>
        <li>all META-INF/microprofile-config.properties files on the ClassPath. (default ordinal=100, separately configurable via a config_ordinal property inside each file)</li>
	</ul>

<p>This diagram demonstrates MicroProfile Config in WebSphere Liberty. The server.env contains configs with the ordinal of 300 while the files jvm.options and bootstrap.properties offer configs with the ordinal of 400. If the same config key exists in microprofile-config.properties or server.env and in jvm.options, the config value in jvm.options will be used.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/september/images/configchart.png"/></p>

<p>The default values can be specified in the file microprofile-config.properties within the application and the value can be overwritten later for each deployment. A higher ordinal number takes precedence over a lower number.</p>

<p>It is also possible to write and register custom <code>ConfigSources</code>, for example if you want application in your environments to retrieve their config from a central key/value store.</p>

<p>There are a number of Config projects which directly influenced MicroProfile Config and acted as basis for this API, such as:</p>

	<ul>
        	<li><a target="_blank" href="http://deltaspike.apache.org/documentation/configuration.html">DeltaSpike Config</a></li>
        	<li><a target="_blank" href="https://github.com/struberg/javaConfig/">Extracted parts of DeltaSpike Config</a></li>
        	<li><a target="_blank" href="http://tamaya.incubator.apache.org/">Apache Tamaya</a></li>    	
	</ul>

<p>This project started in October 2016 with 14 amazing contributors. The Config 1.0 API was released in August 2017, followed by Config 1.1 release in September 2017.</p>
<p>Microprofile Config does not contain a “reference implementation” itself but does provide the API specification, a TCK, and documentation.</p>

<p>The following Implementations are available with more on their way:</p>
    <ul>
        	<li><a target="_blank" href="https://svn.apache.org/repos/asf/geronimo/components/config/trunk">Apache Geronimo Config</a></li>
        <li><a target="_blank" href="https://svn.apache.org/repos/asf/geronimo/components/config/trunk">WebSphere Liberty</a></li>
        <li><a target="_blank" href="https://svn.apache.org/repos/asf/geronimo/components/config/trunk">Payara Server 173 and Payara Micro 173</a></li>
    </ul>

<h2>How to use MicroProfile Config?</h2>
<p>MicroProfile Config offers two ways to obtain config properties: programmatically or via CDI injection.</p>
<h3>1. Obtain Config programmatically</h3>
<p>Below is a code snippet to demonstrate how to get hold of a property named “userId”. First obtain the Config object, which contains all properties this class can access. Then look up the individual property via <code>getValue(String propertyName, Class&lt;?&gt; propertyValueType)</code></p>

<pre style="background:#211e1e;color:#dadada"><span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.Config;
<span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.ConfigProvider;
 
<span style="color:#6969fa;font-weight:700">public</span> class ProgrammticConfigDemo {
    
    <span style="color:#6969fa;font-weight:700">public</span> <span style="color:#a1a1ff">void</span> purchase() {
            Config config <span style="color:#47b8d6">=</span> ConfigProvider.getConfig();
            String userId <span style="color:#47b8d6">=</span>  config.<span style="color:#a1a1ff">getValue</span>(<span style="color:#ad9361">"userId"</span>, String.class);
            purchaseService(userId);
    }
 
    <span style="color:#6969fa;font-weight:700">private</span> <span style="color:#a1a1ff">void</span> purchaseService(String userId) {
            <span style="color:#555">// do something  </span>
    }
}

</pre>

<h3>2. Obtain Config via Injection</h3>
<h4>Individual property injection - static value</h4>
<p>Each individual property can be injected directly, shown below. The injected value is static and the value is fixed on application starting.</p>

<pre style="background:#211e1e;color:#dadada"><span style="color:#6969fa;font-weight:700">import</span> javax.inject.Inject;
<span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.inject.ConfigProperty;
 
<span style="color:#6969fa;font-weight:700">public</span> class ProgrammticConfigDemo {
    @Inject @ConfigProperty(<span style="color:#a1a1ff">name</span><span style="color:#47b8d6">=</span><span style="color:#ad9361">"userId"</span>) 
      String userId;

    <span style="color:#6969fa;font-weight:700">public</span> <span style="color:#a1a1ff">void</span> purchase() {            
                purchaseService(userId);
    }
 
    <span style="color:#6969fa;font-weight:700">private</span> <span style="color:#a1a1ff">void</span> purchaseService(String userId) {
                <span style="color:#555">// do something     </span>
    }
}

</pre>

<h4>Individual Property Injection - Dynamic</h4>

 <p>If a property is dynamic, injecting Provider<> will force the value to be retrieved just in time.</p>

<pre style="background:#211e1e;color:#dadada"><span style="color:#6969fa;font-weight:700">import</span> javax.inject.Inject;
<span style="color:#6969fa;font-weight:700">import</span> javax.inject.Provider;

<span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.inject.ConfigProperty;
 
<span style="color:#6969fa;font-weight:700">public</span> class ProgrammticConfigDemo {
    @Inject @ConfigProperty(<span style="color:#a1a1ff">name</span><span style="color:#47b8d6">=</span><span style="color:#ad9361">"userId"</span>)
      Provider&lt;String> userId;

    <span style="color:#6969fa;font-weight:700">public</span> <span style="color:#a1a1ff">void</span> purchase() {             
                purchaseService(userId.<span style="color:#a1a1ff">get</span>());
    }
 
    <span style="color:#6969fa;font-weight:700">private</span> <span style="color:#a1a1ff">void</span> purchaseService(String userId) {
                <span style="color:#555">// do something  </span>
    }
}

</pre>

<h4>Individual Optional Property Injection</h4>

<p>If a property is optional, supply a default value on ConfigProperty.</p>

<pre style="background:#211e1e;color:#dadada"><span style="color:#6969fa;font-weight:700">import</span> javax.inject.Inject;

<span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.inject.ConfigProperty;
 
<span style="color:#6969fa;font-weight:700">public</span> class ProgrammticConfigDemo {
    @Inject @ConfigProperty(<span style="color:#a1a1ff">name</span><span style="color:#47b8d6">=</span><span style="color:#ad9361">"userId"</span>, defaultValue<span style="color:#47b8d6">=</span><span style="color:#ad9361">"me"</span>)
    String userId;

    <span style="color:#6969fa;font-weight:700">public</span> <span style="color:#a1a1ff">void</span> purchase() {             
                purchaseService(userId);
    }
 
    <span style="color:#6969fa;font-weight:700">private</span> <span style="color:#a1a1ff">void</span> purchaseService(String userId) {
                <span style="color:#555">// do something  </span>
    }
}

</pre>

<h4>Config Object Injection</h4>
 <p>The config object can also be injected. Then use the getValue() method to retrieve the individual property.</p>
 
<pre style="background:#211e1e;color:#dadada"><span style="color:#6969fa;font-weight:700">import</span> javax.inject.Inject;
<span style="color:#6969fa;font-weight:700">import</span> org.eclipse.microprofile.config.Config;
 
<span style="color:#6969fa;font-weight:700">public</span> class ProgrammticConfigDemo {
    @Inject Config config;

    <span style="color:#6969fa;font-weight:700">public</span> <span style="color:#a1a1ff">void</span> purchase() {            
            String userId <span style="color:#47b8d6">=</span>  config.<span style="color:#a1a1ff">getValue</span>(<span style="color:#ad9361">"userId"</span>, String.class);
            purchaseService(userId);
    }
 
    <span style="color:#6969fa;font-weight:700">private</span> <span style="color:#a1a1ff">void</span> purchaseService(String userId) {
            <span style="color:#555">// do something     </span>
    }
}

</pre>


<p>The above example illustrates how to retrieve a mandatory property. If this property does not exist in one or more <code>ConfigSources</code>, a deployment exception will occur.</p>

<p>For an optional property, replace the call to <code>getValue()</code> with a call to <code>getOptionalValue()</code> and replace <code>@ConfigProperty(name=”userId”)</code> with <code>@ConfigProperty(name=”userId”</code>, <code>defaultValue=”me”)</code>.</p>


<h2>Where to go next?</h2>
<p>The <a target="_blank" href="http://mvnrepository.com/artifact/org.eclipse.microprofile.config/microprofile-config-api">API</a> and <a target="_blank" href="http://mvnrepository.com/artifact/org.eclipse.microprofile.config/microprofile-config-tck">TCK</a> jars can be found from maven central. The specification can be accessed from <a target="_blank" href="https://wiki.eclipse.org/MicroProfile/Config">here</a>.</p> 

<p>We have released MicroProfile Config 1.0 and Config 1.1.</p>
	        
<p>Below is the quick summary of Config 1.1 release content:</p>
	<ul>
        <li>One default method returning the property names <code>getPropertyNames()</code> was added to <code>ConfigSource</code></li> 
        <li>The default converters extended to include URL, all primitive types (int, long etc) in addition to the specified primitive type wrappers</li>
        <li>The default property name for <code>@ConfigProperty</code> has been changed to fully qualified classname (no longer lowercase the first letter of the class, as specified in Config 1.0) followed by the . and variable name.</li> 
	</ul>
	
	<p>We are working through the <a target="_blank" href="https://github.com/eclipse/microprofile-config/issues">issues</a> on MicroProfile Config <a target="_blank" href="https://github.com/eclipse/microprofile-config">repo</a>. The next big item will be to improve the dynamic aspect of Config. If you would like to see new features in the next release, please log some issues there. We have a weekly hangout to discuss the design issues. Please import the MicroProfile Calendar, which can be found from MicroProfile <a target="_blank" href="https://github.com/eclipse/microprofile-config">wiki</a> and join the hangout.</p>
	
	<p>Due to the success of this project, the <a href="https://www.eclipse.org/community/eclipse_newsletter/2017/september/article7.php">Configuration JSR</a> was proposed with me and Mark Struberg as Spec Lead. Stay tuned on the progress. </p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/emily.jpg"
        alt="Emily Jiang" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Emily Jiang<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/emily-jiang-60803812/">LinkedIn</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/emilyfhjiang">Twitter</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

