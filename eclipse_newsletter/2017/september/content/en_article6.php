<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>Just a little over a year ago, the MicroProfile initiative was born. The community first heard about it at the DevNation conference in San Francisco on the 27th June last year. During the keynote panel, the five founding members (Red Hat, IBM, Tomitribe, LJC, and Payara) announced their collaboration and their main goal - to make it easier for developers to use familiar Java EE technologies and APIs for building microservice applications.</p>

<p>The members of the panel, Mark Little (Red Hat), Martijn Verburg (LJC), Theresa Nguyen (Tomitribe), Alasdair Nottingham (IBM), and Mike Croft (Payara), gave an overview of their views on the MicroProfile initiative and how they see it fitting in to the established ecosystem of microservices. The message from the panelists was clear – they all believe that Enterprise Java is a solid foundation on which to build the next generation of applications, and the MicroProfile collaboration can make it easier and provide portability between vendor implementations.</p>

<p>A lot has happened since June 2017: MicroProfile joined the Eclipse Foundation, the 1.0 and 1.1 versions were released, new members were added, and new specs created. All of this (and more) was achieved through a lot of hard work, countless discussions, calls, meetings, and community collaborations.</p> 

<p>Now that one year has passed, it seems like a good time to check back with the panellists to see how they feel about MicroProfile and the progress that it has made since the time they all stood at the DevNation stage together.</p>

<h2>Community</h2> 

<p>The community plays a vital role in the Eclipse MicroProfile (MP) initiative. It has a lot of <a target="_blank" href="http://microprofile.io/contributors">individual contributors</a>, many of them are not only members of the Java EE community, but also on Java EE 8 related expert groups or lead JSRs. MicroProfile participation is open to any community, company, group, or individual.</p> 

<p>Martijn Verburg from the LJC has been involved in Java community collaborations and user groups for many years, so it wasn’t surprising that the community aspect of the MicroProfile initiative was what he focused on during the panel last year. One year later, he admits that the Eclipse MicroProfile plays a significant role in the Java community.</p>

<p>‘There’s been a distinct increase in co-operation between the traditional Java EE vendors’ says Martijn. ‘Typically, one vendor would lead a Java EE JSR with some but certainly not equal input from other vendors. The MicroProfile initiative has shown that work similar to JSR work (the production of MP specifications) can actually be shared amongst vendors, and because of the more open platform even individual contributors and Java User Groups who are not directly affiliated with the vendors’.</p>
<p>‘MicroProfile is still new and so there aren't many production implementations of the specifications it's put out, but already one of those specifications has been submitted as a JSR (Config), and so ideas that are rapidly prototyped in MP can quickly become standards via the JCP process and gain a larger audience’.</p>
 
<p>When asked about the biggest achievements of the Eclipse MicroProfile initiative, Mike Croft from Payara Services, who’s been directly involved in the MicroProfile development from its early days, also highlights the importance of the Java community.</p>

<p>‘It can't be overstated how difficult it is to launch a new initiative like this with community at its heart. It's pretty easy to get people on your side at the start, but we have been very fortunate to have a huge number of very dedicated people and the momentum has been steadily building even through our transition to the Eclipse Foundation’.</p> 


<h2>Innovating Enterprise Java for Developers</h2>

<p>The Eclipse MicroProfile initiative’s aim is to bring microservice architectures closer to developers who already have a great amount of Enterprise Java knowledge and skills that they can leverage as they evaluate the adoption of innovative cloud-native architectures. Mark Little, from Red Hat, is confident that MicroProfile helped the community make some good progress in this area.</p>

<p>‘If you look at the work we did with the 1.0 and 1.1 releases of Eclipse MicroProfile, and now the work the community is driving forwards for 1.2, a lot of it is based on existing efforts which have been in deployment in some places for years’ says Mark. ‘We’re finding a lot of people have been deploying Java EE implementations or components into cloud or microservices for a long time and their input has been valuable to help shape where we go with the next steps, but the overall theme has been that what exists in Java EE already has been very applicable to these new areas of deployment’.</p>

<p>Back in June last year, the DevNation panel members addressed some of the community’s worries, like Java EE not offering solutions specifically in the areas of cloud and microservices. One year later, David Blevins, the founder of Tomitribe, explains how the MicroProfile initiative is contributing to changing the Java EE development towards more cloud-native and microservices approach.</p>

<p>‘If you look at the work at or nearing completion in Eclipse MicroProfile there are at least three cloud touch points. By this I mean areas where MicroProfile is touching and enabling integration with common cloud-native technologies’ – highlights David.  </p>
<p>‘The first is MicroProfile Config which allows properties-based configuration data to be injected or looked up by applications. The key cloud-native touchpoint is it enables the environment properties found in containerized environments like Kubernetes or Docker to be one of those sources of configuration data. </p> 
<p>The second is the MicroProfile JWT support which allows caller identity to be securely passed to MicroProfile applications in the form of signed JSON Web Tokens which are verified using nothing more than a simple RSA public key and no other backend state.  A MicroProfile compliant implementation must support verifying these tokens which enables developers to leverage cloud-native security technologies such as API Gateways and OAuth 2.0 authorization servers that issue JWTs. </p> 
<p>The third are is the emerging MicroProfile Healthcheck specification which mandates standard endpoints and format of HTTP-based healthchecks to determine server status. The goal here is to enable the growing number of cloud-native tools for monitoring the health of the MicroProfile-based microservices living in the cloud. If you look at it at the macro scale, MicroProfile is shaking hands directly with Container providers, Security solutions and Monitoring systems in the cloud-native space and saying “we will work with you”. This is only just the beginning. There are efforts under way for metrics, exporting Open API JSON definitions and more’.</p>

<p>Another DevNation panel question touched upon Java EE not directly addressing containerization, including Docker and similar technologies. As it was then explained by Mike Croft, Linux containers (almost universally Docker based) and application containers work to solve different problems and they can be very complementary. However, to make the most of them, they required developers to hunt out the most appropriate libraries to interact with a "containerized" world. Things are looking more positive now, thanks to the Eclipse MicroProfile efforts.</p>

<p>‘Since that time, the MicroProfile has begun reducing this gap that developers have had to fill with a completed Config API, and almost complete APIs for Metrics, service Health Checks, Fault Tolerance, and JWT role-based access control’ says Mike.</p>
<p>‘As we look forward now, there are many more areas where MicroProfile can help developers focus on business logic, rather than infrastructure needs; with newly emerging specifications based on Open Tracing and interoperability with service mesh architectures, there are still many more ways in which MicroProfile can make developers lives easier’.</p>

<p>The panelists agree that the community needs Java EE to have a set of common standards and a common foundation to drive innovation. ‘In terms of driving innovation for future standardization, the Configuration API JSR is the first of hopefully many JSRs we’ll see come out of the MicroProfile’ explains David Blevins. ‘This API was one of the first tackled in the Eclipse MicroProfile, incubated for several months, was covered at a handful of conferences and has generally been proven ready for the next step of standardization. What cannot be missed about this is the JSR itself has been submitted by the Eclipse Foundation making it one of the first, if not the first, JSR with all IP fully owned by an open source foundation. What this gives us is not just more standards that lay a common foundation, but a way to do that that is entirely community owned and protected.  What would be left undone is to see all standards follow the same path’.</p>


<h2>Progress - One Step at a Time</h2>

<p>Although a lot of progress has already been made, not everything has been moving forward as quickly as the Eclipse MicroProfile members would have wanted. Moving to a new Foundation and bringing together a diverse and rather large community of developers meant that we had to wait a bit longer for MicroProfile 1.1. It was however successfully released on the 9th of August.</p> 

<p>In his <a target="_blank" href="https://mikecroft.io/2017/07/11/microprofile-1-year-on.html">recent article</a>, Mike Croft highlights that despite some slight delays with development, now that the first growing pains are fading away things should be speeding up: ‘Eclipse mentors and the MicroProfile community were very dedicated and work on new specs is now progressing at a very healthy pace’. All contributions and activity of the initiative can be monitored on the <a href="https://projects.eclipse.org/projects/technology.microprofile/who">Eclipse MicroProfile page</a>, while the current list of proposals can be viewed on <a target="_blank" href="http://microprofile.io/projects">microprofile.io</a>.</p>

<p>Mark Little breaks down the Eclipse MicroProfile progress into two categories:</p>

<p>‘First, there’s the various implementations that we are relying on to help drive the experience and understanding within MicroProfile; WildFly Swarm is just one of those implementations of course, but here we are seeing a lot of progress around integration of technologies such as those found within NetflixOSS or with Kubernetes and now Istio. This progress includes feedback from users and not just developers.  The second category of progress is with Eclipse MicroProfile itself and that hasn’t been as fast. However, that’s not a bad thing and it is something we all expected: from the start we’ve said that we want the work we try to agree upon in MicroProfile to be influenced by real work users and experience, and you can’t have either of those until you’ve got implementations. Therefore, naturally MicroProfile effort should lag behind the implementations to a degree’.</p>

<p>Kevin Sutter from IBM is happy to see the high level of involvement from his team and the progress that they have made so far: ‘IBM is extremely active with the MicroProfile project. We co-lead the Eclipse Project (along with Red Hat), we have also driven two of the MicroProfile specifications that are part of the MicroProfile releases - Config 1.0 and Fault Tolerance 1.0.  We are also participating in all the other specifications being developed for MicroProfile. Several other community members have also been contributing to the success of these components' deliveries’.</p>

<p>Such a level of involvement has a direct influence on how the large vendors such as IBM approach the development of their technologies. ‘The MicroProfile development feeds directly into WebSphere Liberty’ admits Kevin, ‘As soon as we near completion of a MicroProfile release in the community, we figure out a plan to get the corresponding implementations into our customer's hands.  With WebSphere Liberty's continuous delivery model, this is actually quite straight forward.  In most cases, we can have a Liberty fixpack supporting a new MicroProfile release within weeks of a community delivery’.</p> 

<h2>Java EE – The Future</h2>

<p>After the recent <a target="_blank" href="https://blogs.oracle.com/theaquarium/opening-up-java-ee">Oracle announcement</a> about moving Java EE to an open source foundation, we can expect a lot of changes coming to the Java world. At this point in time it is difficult to predict exactly what is going to happen and when, but the MicroProfile panelists feel positive about what might be coming our way.</p>

<p>‘I'm really optimistic!’ says Mike Croft, ‘A more open process and an easier way to contribute is something that a lot of the community have been asking for a long time’.</p>

<p>‘I think this is a very positive thing to do and likely sits up there alongside Sun's open sourcing of Java as one of the most significant events to happen to the wider Java ecosystem’ adds Mark Little. ‘Clearly, I also see this as beneficial to our collective MicroProfile efforts and we will have to see how both of these things will evolve over time’.</p>

<p>Kevin Sutter from IBM notices how much Oracle’s decision resonates with the MicroProfile’s aims and goals: ‘We've been asking for a more open governance model for Java EE for several years.  As a matter of fact, if this would have happened say 18 months ago, maybe there would not have been a need for MicroProfile... ‘.</p>

<p>As for how Oracle’s decision affects Eclipse MicroProfile, we will probably have to wait at least 12 months. ‘There are still a lot of unanswered questions, but <a target="_blank" href="https://www.youtube.com/watch?v=VvUfAn6te3M&feature=youtu.be&t=1658">members of the Eclipse MicroProfile are certainly aware and talking about it in the regular public meetings</a>’ says Mike Croft. ‘Something that generates further interest here is the fact that MicroProfile <a target="_blank" href="https://www.slideshare.net/dblevins1/2017-jcp-ec-configuration-jsr">filed the Config API as its first JSR not long before this announcement</a>. This may be only serendipitous but, if it's a sign of things to come, I think it's a good sign!’</p>

<p>‘Perhaps the MicroProfile initiative will merge with this new donation’ – suggests Martijn Verburg. ‘MicroProfile as a community has learned how to quickly and collaboratively build specifications, test them in the wild and modify them *before* deciding on standardisation.  Ideally that's how the future Java EE platform would work as well’ – he adds.</p>

<p>David Blevins looks at the Oracle’s decision and its possible impact on Eclipse MicroProfile from a slightly different perspective:</p>
<p>‘I think the question is better asked the other way around, how has the Eclipse MicroProfile affected opening up Java EE. I wouldn’t want to take any thunder away from Oracle’s bold and generous move, but I will say I love to see both going in the same direction. We can only get stronger and faster together. With all IP out in the open, including TCKs, the collaboration opportunities are only going to increase. When an open source project collaborates with a proprietary project, collaboration naturally only goes one way. With both being open source, I think we’ll see a new era ahead’.</p>

<p>For now however, it looks like the Eclipse MicroProfile members will continue with development as normal. As Kevin Sutter explains, ‘if the Java EE move to a foundation occurs, it will take several months. We can't wait and hope for that to complete. So, we will continue marching forward with MicroProfile. Since we have already started the process of proposing the MicroProfile Config spec as a JSR, we see the cooperation between MicroProfile and the new "foundation Java EE" becoming even easier in the future’.</p>

<p>To keep an eye on further development of the Eclipse MicroProfile initiative or to join and contribute, visit the <a target="_blank" href="http://microprofile.io/projects">Eclipse MicroProfile website</a>.</p> 

<p><i>Many thanks to Martijn Verburg, Mark Little, Mike Croft, Kevin Sutter and David Blevins for their contribution!</i></p>

<p>Watch the panel <a target="_blank" href="https://www.youtube.com/watch?time_continue=1446&v=Cf9Mz6npH3k">video</a>.</p>
<p align="center"><a target="_blank" href="https://www.youtube.com/watch?time_continue=1446&v=Cf9Mz6npH3k"><img src="/community/eclipse_newsletter/2017/september/images/devnation-panel.jpg"/></a></p>

<div class="alert alert-warning">
 <strong>Update</strong>:  As this article was being published, Oracle announced on September 12 that they, along with IBM and Red Hat, will be moving Java EE to the Eclipse Foundation. <a target="_blank" href="https://blogs.oracle.com/theaquarium/opening-up-ee-update">Read the announcement</a>.
</div>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/dominika.jpg"
        alt="Dominika Tasarz" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Dominika Tasarz<br />
            <a target="_blank" href="https://www.payara.fish/">Payara</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/dominislawa">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.payara.fish/author/dominika-tasarz">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

