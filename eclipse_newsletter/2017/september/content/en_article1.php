<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <h2>Executive Summary</h2>
<p>Within 15 months, <a target="_blank" href="https://microprofile.io/">MicroProfile</a> will have delivered a microservices platform consisting of 3 existing Java EE-related JSRs and 5 new “home grown” MicroProfile specifications. This was done while simultaneously becoming an Eclipse Foundation “<a href="https://projects.eclipse.org/proposals/eclipse-microprofile">Eclipse MicroProfile</a>” project. In addition, work continues towards its second major platform release. To learn the details, read on!</p>
   
   <h2>What is MicroProfile?</h2>
	<p>Leading into 2016, the pace of Java EE releases had been slowing while at the same time the industry was quickly moving towards a microservices architecture.  While this was happening, the Java EE community was fragmenting in how it was approaching microservice patterns and delivering incompatible microservice-oriented runtimes. To solve this cadence mismatch and fragmentation, a collection of vendors, organizations, and individuals founded MicroProfile to define Enterprise Java specifications to address microservices patterns.</p>

    <p>MicroProfile takes an “open source” approach to developing specifications by collaborating publicly as a community and rapidly iterating on specifications, putting multiple implementations of a specification in developers hands much more quickly than a traditional standards organization.  However, when a specification becomes mature enough, the intent is for it to become a standard.</p>
    
    <p>Within a period of roughly 15 months, MicroProfile will have had 3 releases and joined the Eclipse Foundation, living up to its goal of rapid, iterative progress! What follows is a quick review of what we have delivered in a bit more detail.</p>

	<h2>MicroProfile 1.0 - September, 2016</h2>
	<p>The MicroProfile community has its historical roots firmly planted within the Java EE community. With that in mind, MicroProfile released version 1.0 just before JavaOne 2016, based on Java EE JSRs. This release included CDI 1.1, JAX-RS 2.0, and JSON Processing 1.0. Having just been founded as a project, the MicroProfile community did not want to take too large a stride in defining the MicroProfile platform. Instead, it defined a small core set technologies that set a baseline for the programming model upon which additional specifications can layer on with the input of a much broader and rapidly growing community.</p>
	
   <h2>MicroProfile Joins the Eclipse Foundation - December, 2016</h2>

	<p>In late 2016, MicroProfile was re-branded to Eclipse MicroProfile when it became an Eclipse Foundation project. Within the Eclipse Foundation, MicroProfile can leverage proven Eclipse Foundation release management, legal, and governance processes while ensuring a level playing field for all contributors. Two of MicroProfile’s three releases have been delivered within the Eclipse Foundation.</p>
   
	<h2>Eclipse MicroProfile 1.1- August, 2017</h2>
	<p>With three Java EE-related JSRs forming the base of MicroProfile, the version 1.1 specification [<a target="_blank" href="https://drive.google.com/open?id=0B7PK4ZIk1mjAb1pNTWxydVpzV1U">pdf</a>] added a home-grown “Config 1.0” specification. A core microservice (and <a target="_blank" href="https://12factor.net/">12-factor</a>) requirement is to externalize the configuration of a service from the service itself. The Config 1.0 specification team gathered input from other open source configuration projects, like <a target="_blank" href="http://deltaspike.apache.org/documentation/configuration.html">Apache DeltaSpike</a> and <a target="_blank" href="http://tamaya.incubator.apache.org/">Apache Tamaya</a>), while defining the specification.</p>

	<p>The Config 1.0 Specification, JavaDocs, and TCK, and API jar are available <a target="_blank" href="https://github.com/eclipse/microprofile-config/releases/tag/1.0">here</a>. In addition, the Config 1.0 was also <a target="_blank" href="https://jcp.org/aboutJava/communityprocess/ec-public/materials/2017-08-15/2017_JCP_Configuration_API_JSR_v2.pdf">proposed</a> as a “Configuration 1.0” JSR within the JCP.</p>

	<p>Last, Eclipse MicroProfile 1.1 was also launched with a new MicroProfile logo, selected from multiple community contributions and community vote.</p>
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/september/images/microprofile_logo_med.png"/></p>
	
	<h2>MicroProfile 1.2 - September, 2017 (planned)</h2>
	<p>The MicroProfile 1.2 release packs quite a punch, with 4 additional specifications and one updated specification. They are as follows:</p>
		<ul>
            <li><a href="https://projects.eclipse.org/projects/technology.microprofile/releases/config-1.1">Config 1.1</a> - An incremental update to Configuration 1.0, adding clarifications and minor API updates.
            <li><a href="https://projects.eclipse.org/projects/technology.microprofile/releases/fault-tolerance-1.0">Fault Tolerance 1.0</a> - Build resilient microservices by applying Timeout, Retry, Circuit Breaker, Bulkhead, and Fallback policies.
            <li><a href="https://projects.eclipse.org/projects/technology.microprofile/releases/health-metrics-1.0">Health Metrics 1.0</a> - Expose metrics / telemetry in a common way across all compliant servers
            <li><a href="https://projects.eclipse.org/projects/technology.microprofile/releases/health-metrics-1.0">Health Check 1.0</a> - Ensure a compatible wire format, agreed upon semantics and possible forms of interactions between system components that need to determine the “liveliness” of computing nodes in a bigger system.
            <li><a href="https://projects.eclipse.org/projects/technology.microprofile/releases/health-metrics-1.0">JWT Propagation 1.0</a> - Defines the interoperability and container integration requirements for <a href="https://projects.eclipse.org/projects/technology.microprofile/releases/health-metrics-1.0">JSON Web Token (JWT)</a> based bearer token authentication and authorization for use with Java EE style RBAC permissions.
         </ul>   	
	
	<h2>Looking ahead</h2>
    <p>MicroProfile 2.0 is in planning, so normal disclaimers related to “forward looking statements” apply :-).</p> 
    
    <p>Expect the Java EE 7-related JSRs to be updated to a Java EE 8-related JSRs. This implies not only upgrading to CDI 2.0, JAX-RS 2.1, and JSON processing 1.1, but the potential to add new JSRs like the <a target="_blank" href="https://jcp.org/en/jsr/detail?id=367">Java API for JSON Binding (JSON-B)</a>.</p>
    
    <p>Expect updates to some of the MicroProfile “home grown” specifications delivered in MicroProfile 1.2. Exactly what those updates will be is “to be determined” because as of this article, the focus is on delivering the final touches to MicroProfile 1.2.</p>
    
    <p>Last, there is the exciting news that Oracle is <a target="_blank" href="https://jcp.org/en/jsr/detail?id=367">Opening Up Java EE</a> by moving it to an open source foundation. This has the potential to accelerate the adoption of MicroProfile specifications within the Java EE ecosystem. MicroProfile will continue to deliver specifications while Java EE is moved to a foundation, which will take time,  to not lose the momentum of delivering specifications for Enterprise Java developers.  When the foundation move is complete, we’ll work on aligning the two projects.</p>
    
    <h2>One more thing</h2>
	<p>We in the MicroProfile community would love to hear *your* input and great ideas. Please join the <a target="_blank" href="https://jcp.org/en/jsr/detail?id=367">MicroProfile Google Group</a> and share your experience and knowledge. Help us bring Microservices to Enterprise Java!!</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/john.jpg"
        alt="John Clingan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            John Clingan<br />
            <a target="_blank" href="https://www.redhat.com/">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jclingan">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://johnclingan.com/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

