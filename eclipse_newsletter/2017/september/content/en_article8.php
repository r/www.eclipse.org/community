<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/september/images/microprofile_logo_dark.jpg"/></p>

<h2>Optimising Enterprise Java for Microservice Architecture</h2>

<p>The <a href="https://projects.eclipse.org/projects/technology.microprofile">Eclipse MicroProfile</a> initiative was launched at JavaOne 2016, the brainchild of server vendors and Java user groups, in order to address the shortcomings in the Enterprise Java microservices space. The pace of Java EE releases slowed to a point where it was unable to meet the challenges of the rapidly advancing microservice trend. MicroProfile addresses this by brings together individuals, vendors and community organizations in open collaboration to deliver microservices to the EE eco-system.</p>

<h2>What is Eclipse MicroProfile?</h2>

<p>The MicroProfile specifies a collection of Java EE APIs and technologies which together form a core baseline microservice that aims to deliver application portability across multiple runtimes.</p>

<p>MicroProfile 1.0 specification includes a subset of the 30 plus Java Enterprise specifications and they are: JAX-RS 2.0 for RESTful endpoints, CDI 1.1 for extensions and dependency injection and JSON-P 1.0 for processing JSON messages. Why just these three? It’s because this represents the least amount of APIs required to build a microservice.</p>

<p>MicroProfile 1.2 was released in September 2017 with quite a few amazing features, including Configuration 1.1, Fault Tolerance, JWT, Metrics and Health Check. It is expected that version 2.0 will align all APIs to Java EE 8.</p>

<p>A number of respected server vendors provide runtimes. Those who support the MicroProfile architecture are: <a target="_blank" href="https://developer.ibm.com/wasdev/websphere-liberty/">WebSphere Liberty</a> from IBM, <a target="_blank" href="http://tomee.apache.org/">TomEE</a> from Tomitribe, <a target="_blank" href="https://www.payara.fish/">Payara</a>, RedHat’s <a target="_blank" href="http://wildfly-swarm.io/">WildFly Swarm</a> and <a target="_blank" href="https://ee.kumuluz.com/">KumuluzEE</a>.</p>

<p>Community support comes from the <a target="_blank" href="https://www.meetup.com/Londonjavacommunity/">London Java Community</a> and <a target="_blank" href="https://soujava.org.br/">SOUJava</a> plus numerous <a target="_blank" href="https://projects.eclipse.org/projects/technology.microprofile/who">individuals</a> who give their time and effort.</p>

<h2>Motivations for Eclipse MicroProfile</h2>

<p>Java EE’s maturity has resulted in a slowing release cadence which has become too slow for the microservice community. However this has not stop Java Enterprise technologies from being used to develop microservices, in fact, a lot of efforts has already gone on microservices in Java EE.</p>

<p>The space was becoming fragmented with each vendor implementing their own opinion about the right way to do microservices. It was clear that if cohesion was not sought then vendor fragmentation was the inevitable consequence. So collaboration was entered into among vendors and user groups with the guiding force to do microservices better.</p>

<p>With the Java EE full profile becoming so large, over 30 APIs at the last count, it doesn’t seem right to include all those specifications when you are building something that you call ‘micro’, especially when you are not going to use the vast majority of those APIs. A slimmer and better solution had to be found.</p>

<h2>Objectives</h2>

<p>Out of these motivating forces came four overriding objectives that all who participate in the open source project seek to pursue. Those objectives can be outlined as follows:</p>

<p>Avoid fragmentation among vendors and implementers as a way to address the need for a coherent approach to the microservice space. This results in microservice interoperability between server vendors through a common strategy to microservices.</p>

<p>Build quickly through lazy consensus to allow rapid development and iterations. This leads to a high-frequency release cadence, satisfying the communities need for a quick turn-around.</p>

<p>To create more open source technologies in the Java EE space through the framework of the Eclipse organization. This allows the organization to act as the specification lead when those technologies are moved to a JSR format.</p>

<p>And the ultimate goal is to supply the <a target="_blank" href="https://www.jcp.org/en/home/index">Java Community Process</a> (JCP) with <a target="_blank" href="https://www.jcp.org/en/jsr/overview">specification requests</a> (JSR)  backed up by judicious approaches and implementations that developers and businesses can rely upon and actually want.</p>

<p>MicroProfile attempts to understand microservices in terms of Java EE technologies and is not an attempt to compete with Oracle but to innovate around the edges and to feedback to the JSR.</p>


<h2>1. Sand-box Approach to Open Contribution</h2>

<p>Eclipse MicroProfile has a unique approach to open contribution that allows an immediate start via the MicroProfile <a target="_blank" href="https://github.com/eclipse/microprofile-sandbox">sandbox repository</a>. You simply fork the sandbox repository, create a distinct sub-directory, code your idea and submit as many pull requests as you need to probe the concepts behind your proposal.</p>

<p>Anyone can contribute in this way, even if not yet active in the community. This approach presents a zero bar to entry and allows the capture of ideas for anyone who has the inclination to contribute.</p>

<p><a target="_blank" href="https://wiki.eclipse.org/MicroProfile/FeatureInit">Progressing your idea</a> is just as frictionless. Start a new thread on the <a target="_blank" href="https://groups.google.com/forum/#!forum/microprofile">community forum</a> announcing your idea and allow a few days (72 hours) for it to garnish feedback. Consider and respond to feedback, making updates as appropriate and providing justification when feedback is not applicable. Once acceptance of your idea is granted, via lazy consensus, your contribution is migrated to its own repository where work on the specification, APIs and test suit commences.</p>

<p>If you don’t want to work alone, you can solicit support from the community by expressing your idea openly on the Google group. This is where all discussions occur. You can find others who share the same interest and are willing to collaborate with you on your idea.</p>

<h2>2. Challenges Perception that Java EE is Heavy Weight</h2>

<p>The traditional perception of the Enterprise Edition of Java is that it is heavy weight, cumbersome and slow to deploy and this was true for much of its earlier life, but that perception is being successfully challenged, thanks to the Eclipse MicroProfile effort.</p>

<p>On the MicroProfile Eclipse developer resources page, an <a target="_blank" href="https://github.com/eclipse/microprofile-conference">architectural conference application</a> key code sample can be found that you can use to test for yourselves the lightweight nature of a MicroProfile Java EE application. If you don’t have time, then watch this excellent <a target="_blank" href="https://www.youtube.com/watch?v=iG-XvoIfKtg">presentation</a>.</p>

<p>The key code sample consists of four microservices and a front-end application. In the presentation, each microservice was developed with the MicroProfile architecture and deployed on one of the four participating vendor’s server.</p>

<p>A maven plugin compiles the application as a WAR and packages it into a JAR with the server runtime. It is launched via the command line using the java -jar command.</p> <!-- Examine the table below and as you can see the JAR size is minimal and the start up time extremely short. These numbers give any other Java EE framework a run for its money.</p>

    <table class="table table-striped">
          <tr>
            <th><p>Vendor</p></th>
            <th><p>Microservice</p></th> 
            <th><p>JAR size/Mb</p></th>
            <th><p>Start Up Time/Secs</p></th>
          <tr>
            <td><p>WebSphere Liberty</p></td>
            <td><p>Session Voting</p></td> 
            <td><p>35</p></td>
            <td><p>7</p></td>
          </tr>
          <tr>
            <td><p>WildFly Swarm</p></td>
            <td><p>Session</p></td> 
            <td><p>65</p></td>
            <td><p>6</p></td>
          </tr>
          <tr>
            <td><p>Payara</p></td>
            <td><p>Session Schedule</p></td> 
            <td><p>33</p></td>
            <td><p>5</p></td>
          </tr>
          <tr>
            <td><p>TomEE</p></td>
            <td><p>Speaker</p></td> 
            <td><p>35</p></td>
            <td><p>3</p></td>
          </tr>
          <tr>
            <td><p>KumuluzEE</p></td>
            <td><p>Session Schedule</p></td> 
            <td><p>11</p></td>
            <td><p>2</p></td>
          </tr>
    </table>
    
    <div class="alert alert-warning">
 <strong>Note</strong>:  Each MicroProfile instance was launched on a different laptop machine (Windows or Mac) with distinct performance characteristic so these results should not be considered a scientific benchmark. What you should take away from this table is that all vendors performed excellently regardless of the operating system and hardware and that startup times are very short. Just imagine how much better the performance would be on a fully optimised cloud infrastructure.
</div>
    
 -->     
      <p>Each vendor’s implementation takes advantages of the server’s architecture to optimise the deployment. For example; IBM’s WebSphere Liberty uses ‘features’ to select the Java EE technology required and packages just what the application needs. WildFly Swarm uses ‘fractions’ to ‘right size’ the service ensuring that only the required APIs are included in the deployment.</p>

<p>The common feature across all vendors is the optimised way that the deployment is packaged. Just enough code is included to make it work resulting in an uber JAR of minimal size.</p>


      <h2>3. High Cadence Release Philosophy</h2>

<p>The Java EE release cycle typically takes a few years between versions, which, for a standards-driven ecosystem, is perfectly correct. Standards organizations are not meant to innovate, they exist to standardize. They collect the proven ideas once they have been fleshed out and demonstrated to work.</p>

<p>Attempting to innovate within a standards organization is thwart with pit falls and is proven to be disastrous. CMP (container managed persistence) is an example of what happens when a committee guesses what people want and gets it wrong.</p>

<p>The world of microservices is changing very fast and a multi-year release cycle is just too long for the community. MicroProfile’s goal is to improve on this by shortening cycles and making them more iterative. The idea is to try out suggestions, see what works and what doesn’t, and do it in an ‘iterate quickly, fail fast’ process. The bad ideas are flushed out quickly, leaving just the good ones.  As technologies become proven they can be submitted to the JCP for consideration in a future JSR.</p>

<p>The shortened release cycle is designed to deliver something and frequently, even if it is just one idea. This is considered more preferable than delivering a lot of ideas, two years down the line.</p>

<p>MicroProfile’s currently planned releases are publicly announced on the <a target="_blank" href="https://projects.eclipse.org/projects/technology.microprofile/governance">Eclipse MicroProfile</a> site.</p>

<h2>4. Complete Transparency at all Stages</h2>

<p>A guiding philosophy that has proven to result in solutions that the community really wants is that of total transparency. This is the approach that Eclipse MicroProfile has taken. All discussions are conducted in the open on the <a target="_blank" href="https://groups.google.com/forum/#!forum/microprofile">Google Group</a> which is where all discussions start and flourish and anyone can jump in with a comment or a new idea.</p>

<p>Bi-weekly Google Hangouts are an integral part of the process and all are invited to join the meetings. They are announced ahead of time on the community forum and <a target="_blank" href="https://docs.google.com/document/d/16v3jVkcDzVz9BVU5aGEzPVbK-a8BIx7S1gbqToVUaLs/view">meeting notes</a> are published in a Google doc for all to see.</p>

<p>Contributors are acknowledged on the <a target="_blank" href="https://projects.eclipse.org/projects/technology.microprofile/who">Eclipse Project</a> site where it is transparent who is the greatest individual and organizational contributors and which company members have been most active.</p>

<h2>5. CDI Centric Programming Model</h2>

<p>The <a target="_blank" href="http://cdi-spec.org/">Context and Dependency Injection specification</a> is one of the most powerful and feature rich specifications in the Java EE ecosystem and is often leveraged by other APIs.</p>

<p>Two of its most powerful features are interceptors and observers. Interceptors perform cross-cutting tasks that are orthogonal to business logic such as auditing, logging, and security. An essential ability for all kinds of microservice attributes developers require.</p>

<p>The baked in event notification model implements the observer pattern to provide a powerful and lightweight event notification system that can be leveraged system-wide.</p>

<p>On top of these features, you get the expected decoupling of server and client (the essence of dependency injection) plus the entire CDI specification including the ability to define a bean’s lifecycle, typesafe injection, scopes, producer methods, qualifiers and a complete service provider interface (SPI) that allows third-party integration.</p>

<p>It is clear to see why this API was chosen as a core dependency for the baseline Java EE microservice.</p>

<h2>How to Get Involved?</h2>

<p>Getting involved could not be simpler. Pop over to the <a target="_blank" href="https://groups.google.com/forum/#!forum/microprofile">Google Groups</a>, pick a thread that tweaks your interest and start reading and commenting. Do like to talk? Then jump on one of the bi-weekly Google Hangouts and express yourself. If you have an idea for a feature that’s not being discussed then why not start a thread and get some feedback. If you like to jump in at the deep end then fork the <a target="_blank" href="https://github.com/eclipse/microprofile-sandbox">microprofile-sandbox</a> repository and get coding. There is no way that you can’t get involved.</p>
      
      
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/september/images/alex.jpg"
        alt="Alex Theedom" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Alex Theedom<br />
            <a target="_blank" href="https://readlearncode.com/">Java EE Consultant/Developer</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/alextheedom">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://readlearncode.com/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

