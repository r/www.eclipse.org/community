<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


<p>As Java developers we’re often really busy with large backlogs, customer issues and countless disruptions. It can be quite daunting finding the time to learn all about things like <a target="_blank" href="http://kubernetes.io/">Kubernetes</a> and its associated tools and technologies (<a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a>, <a target="_blank" href="https://www.openshift.org/">OpenShift</a>, <a target="_blank" href="https://docs.openshift.org/latest/cli_reference/get_started_cli.html">oc</a>, <a target="_blank" href="http://docker.com/">Docker</a> and <a target="_blank" href="https://coreos.com/rkt/">Rkt</a> and standards like <a target="_blank" href="https://cncf.io/">CNCF</a> and <a target="_blank" href="https://www.opencontainers.org/">OCI</a>). Its well worth doing if you have the time mind you! We recommend you dive into Kubernetes and see where that takes you whenever you get some spare time :).</p>

<p align="center"><a target="_blank" href="https://fabric8.io/"><img class="img-responsive" src="/community/eclipse_newsletter/2017/january/images/fabric8.png" alt="fabric 8 io logo"/></a></p>

<p>On the <a target="_blank" href="https://fabric8.io/">fabric8</a> project we’ve been focused on making it easy for developers to create, build, deploy and manage microservices on top of Kubernetes for some time.</p>

<p>Up to now we’ve mostly focused on the end to end lifecycle of creating new microservices, then changing code through continuous delivery then having continuous improvement. Lately we’ve been focused more on the <i>pre-git-push</i> phase. Where you’re hacking code on your laptop and trying things out before you want to commit and push into your git repository.</p>

<p>Lots of Java developers are used to using application servers, creating deployment units (jars/wars), deploying them and running them in app servers using Maven. Then they can use Maven from the command line or easily inside their IDE to do most of the work.</p>

<p>So we figured, why not make Kubernetes look and feel like an application server to a Java developer? So you build and deploy your application from maven like you would with other maven plugins like spring-boot, tomcat, jetty, wildfly, karaf et al. Then you can get started quickly by just treating Kubernetes as a kind of application server.</p>

<p>Kubernetes is actually way more awesome than an application server; its more like an application <i>cloud</i> as:</p>
	<ul>
	<li>kubernetes can keep running multiple instances of each of your apps including automatic restarts on software or hardware failures</li>
	<li>automatic load balancing when invoking your apps</li>
	<li>each app instance is isolated as a separate process so its much easier to monitor metrics and logs</li>
	</ul>
	
<h2>Setup your maven project</h2>

<p>Do you have an existing maven project for your Java app? Maybe its a Spring Boot or WildFly Swarm app; or a flat classpath, fat jar? If you don’t have a maven app to hand, visit <a target="_blank" href="http://start.spring.io/">start.spring.io</a> and create one.</p>

<p>To enable fabric8 on your project just type this <a target="_blank" href="https://maven.fabric8.io/#fabric8:setup">fabric8:setup</a> command which adds the <a target="_blank" href="https://maven.fabric8.io/">fabric8-maven-plugin</a> to your <i>pom.xml</i> like this <a target="_blank" href="https://maven.fabric8.io/#fabric8:setup">example</a>.</p>

<pre>mvn io.fabric8:fabric8-maven-plugin:3.2.8:setup</pre>

<p>Note that all the <a target="_blank" href="https://github.com/fabric8-quickstarts/">fabric8 quickstarts</a> already have the fabric8 maven plugin enabled so you don’t need to do the above!</p>

<p>Now that we have enabled the fabric8 maven plugin for our project we can have some fun!</p>

<h2>Starting and stopping a local kubernetes cluster</h2>

<p>To develop on kubernetes you will need a kubernetes cluster; just like you need an instance of an application server to deploy your app. If someone in your organisation has provided you with a kubernetes cluster then feel free to use that; you just need to connect to it via the <a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a> too.</p>

<p>However if you don’t have any kubernetes clusters to hand its very easy to spin up your own local cluster. Using a local kubernetes cluster is really handy for development; it lets you build your app and run it locally before you do a git commit and trigger the CI / CD pipeline.</p>

<p>Before you start please make sure you have the <b><a target="_blank" href="https://maven.fabric8.io/#fabric8:cluster-start"></b> prerequisites installed</a> for your platform (basically a recent Apache Maven distro and have a working hypervisor for your platform like xhyve, hyper-v or kvm).</p>

<p>Now with the fabric8-maven-plugin you just use the goal <a target="_blank" href="https://maven.fabric8.io/#fabric8:cluster-start">fabric8:cluster-start</a> to spin up a local cluster:</p>

<pre>mvn fabric8:cluster-start</pre>

<p>Wait a few minutes for some stuff to download and then you’ve now got a local installation of a Kubernetes cluster running on your machine! No installation of Docker, VirtualBox or Vagrant is required!</p>

<p>Under the covers this downloaded <a target="_blank" href="https://github.com/fabric8io/gofabric8/">gofabric8</a> and ran <i>gofabric8</i> start which then downloaded <a target="_blank" href="https://github.com/kubernetes/minikube/">minikube</a> and <a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a> binaries and installs them into <i>~/.fabric8/bin</i></p>

<p>Want to use OpenShift instead of Kubernetes? Use <a target="_blank" href="https://maven.fabric8.io/#using-openshift">this command</a> instead:</p>

<pre>mvn fabric8:cluster-start -Dfabric8.cluster.kind=openshift</pre>

<p>You can now use the <a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a> command line to get pods and whatnot:</p>

<pre>kubectl get pod</pre>

<p>When you’re done and want to stop the kubernetes cluster just use <a target="_blank" href="https://maven.fabric8.io/#fabric8:cluster-stop">fabric8:cluster-stop</a></p>

<pre>mvn fabric8:cluster-stop</pre>

<p>you can restart it again at any time via <a target="_blank" href="https://maven.fabric8.io/#fabric8:cluster-start">fabric8:cluster-start</a></p>

<h2>Running your app</h2>

<p>The simplest way to run your app is via<a target="_blank" href="https://maven.fabric8.io/#fabric8:run">fabric8:run</a></p>
<pre>mvn fabric8:run</pre>

<p>This will build your app (compile code, run tests, generate the jar and package it up into an immutable docker image), generate the kubernetes manifest and deploy it then tail the log output so you can see how your app behaves. If you hit <i>Ctrl-C</i> it then terminates the app.</p>

<p>So that the <i>fabric8:run</i> goal works kinda like other run goals in other maven projects like <i>spring-boot</i>, <i>tomcat</i>, <i>jetty</i>, <i>karaf</i> and <i>wildfly</i>.</p>

<h2>Deploy or undeploy your app</h2>

<p>Deploying your app is a little like running <a target="_blank" href="https://maven.fabric8.io/#fabric8:run">fabric8:run</a> in the background.</p>

<p>To deploy your app into kubernetes you use the <a target="_blank" href="https://maven.fabric8.io/#fabric8:deploy">fabric8:deploy</a> goal</p>

<pre>mvn fabric8:deploy</pre>

<p>This will build your java code and run your unit tests, generate the docker image, create the kubernetes manifest and deploy them into kubernetes.</p>

<p>To remove our app from kubernetes just use the <a target="_blank" href="https://maven.fabric8.io/#fabric8:undeploy">fabric8:undeploy</a> goal</p>

<pre>mvn fabric8:undeploy</pre>

<h2>Viewing logs</h2>

<p>Once you’ve deployed your app you probably want to check its logs. So you can type:</p>

<pre>mvn fabric8:log</pre>

<p>then hit <i>Ctrl-C</i> to terminate tailing the log.</p>

<p>If you don’t want to keep tailing the log and just want to see the current log output try this:</p>

<pre>mvn fabric8:log -Dfabric8.log.follow=false</pre>

<h2>Start and stop</h2>

<p>If you have deployed your app you sometimes want to stop it running so that you can later on start it again. e.g. since its using up resources on your laptop or its invoking some other microservice that you wish to take offline.</p>

<p>To stop your application just use the <a target="_blank" href="https://maven.fabric8.io/#fabric8:stop">fabric8:stop</a> and <a target="_blank" href="https://maven.fabric8.io/#fabric8:start">fabric8:start</a> goal:</p>

<pre>mvn fabric8:stop
…
mvn fabric8:start</pre>


<p>All the <a target="_blank" href="https://maven.fabric8.io/#fabric8:start">fabric8:start</a> and <a target="_blank" href="https://maven.fabric8.io/#fabric8:stop">fabric8:stop</a> goals do is to scale your application on Kubernetes — as in set the number of expected replicas — the number of running processes of your app.</p>

<p>You can also use <a target="_blank" href="https://maven.fabric8.io/#fabric8:start">fabric8:start</a> to scale up your app to more than one pod if you wish</p>

<pre>mvn fabric8:start -Dfabric8.replicas=2</pre>

<p>You can also use the <a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a> command line too:</p>

<pre>kubectl scale — replicas=2 deployment myappname</pre>

<h2>Debugging</h2>
<p>Outside of unit tests we highly recommend you try and run your apps and do all your integration and system testing inside Kubernetes. Then you are spending most of your time using a production-like environment (rather than your laptop with its different operating system, networks and storage etc). It also means you can easily deploy multiple microservices into the same namespace and test combinations of microservices easily.</p>

<p>To debug your app, just run the <a target="_blank" href="https://maven.fabric8.io/#fabric8:debug">fabric8:debug</a> goal</p>

<pre>mvn fabric8:debug</pre>

<p>This will use the default Java remote debugging port of <i>5005</i>. You can specify a different port if you wish</p>

<pre>mvn fabric8:debug -Dfabric8.debug.port=8000</pre>

<p>Once that goal is running the debug port will be open on your laptop (<i>localhost</i>) which then connects using port forwarding to your most recent running pod for your app.</p>

<p>So you can now just start a Remote debug execution in your IDE (like you do to debug remote application server instances) and you can set breakpoints and debug your pod while its inside kubernetes.</p>

<p>e.g. in IntelliJ here’s how you add a Run/Debug execution:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/january/images/debug.png" alt="debug and run configuration"/></p>
<p align="center">add a new Remote kind of Run/Debug configuration</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/january/images/debugtitle.png" alt="debug title"/></p>
<p align="center">give the Run/Debug configuration a title</p>

<p>To see this in action check out the video below.</p>

<h2>Continuous Delivery and Releasing</h2>

<p>Once you are ready to start releasing your code run this command:</p>

<pre>mvn fabric8:import</pre>

<p>This will import your local project into fabric8.</p>

<p>If your code is not already hosted in a git repository it will create a new git repository and import your code. Then it’ll show you a link to open the project in the fabric8 console so you can add a Continuous Delivery pipeline to your project to enable a Jenkins build for full CI / CD</p>

<h2>Demo</h2>
<p>To see all of these maven goals in action check out this short video:</p>

<div class="embed-responsive embed-responsive-16by9">
  <iframe width="560" src="https://www.youtube.com/embed/CYrCW1SjpOI" frameborder="0" allowfullscreen></iframe>
</div>

<p align="center">local java development with kubernetes and docker using fabric8 maven plugin</p>

<h2>Summary</h2>

<p>So once you’ve started a cluster (<i>cluster-start</i>) the fabric8 maven goals are very similar to other application server based maven plugins; run, <i>deploy/undeploy, start/stop</i> are the main <i>goals</i> with <i>log</i> and <i>debug</i> pretty handy too.</p>

<p>Once you are familiar with those and you’ve done some development on kubernetes with the fabric8 maven plugin you might start to try out using the <a target="_blank" href="http://kubernetes.io/docs/user-guide/kubectl-overview/">kubectl</a> tool also which is handy for querying or watching the state of kubernetes, performing updates to them. e.g. to watch pods start and stop type:</p>

<pre>kubectl get pod -w</pre>

<p>Please try out the <a target="_blank" href="https://maven.fabric8.io/">fabric8 maven plugin</a> and let us know what you think via the <a target="_blank" href="http://fabric8.io/community/">community</a> or the <a target="_blank" href="https://github.com/fabric8io/fabric8-maven-plugin/issues/">issue tracker</a>! Can you think of any way to make it even more awesome? If so <a target="_blank" href="https://github.com/fabric8io/fabric8-maven-plugin/issues/">let us know</a>!</p>

<p>For a live introduction to Fabric8, attend my Devoxx US talk in San Jose, California on March 21. The talk is called <a target="_blank" href="http://cfp.devoxx.us/2017/talk/CYO-6114/develop_microservices_faster_with_an_open_source_platform_based_on_docker,_kubernetes_and_jenkins">develop microservices faster with an open source platform based on docker, kubernetes and jenkins"</a>.

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/january/images/jamess.png"
        alt="James Strachan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             James Strachan<br />
            <a target="_blank" href="http://fabric8.io/">Fabric8</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jstrachan">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.fabric8.io/@jstrachan">Blog</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
