<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><a target="_blank" href="http://www.angular2.com/">Angular 2</a>, the complete re-write of the popular AngularJS framework was released in September of 2016. In March 2017 Angular 2 will be renamed to Angular, so I’ll be using the new name in this article.</p>

<p>Initially the entry barrier into the world of Angular development was pretty high because of the need to learn and manually configure multiple tools. Even to get started with a simple application, you’d need to learn the TypeScript language (an easy job for Java developers), learn how to configure and use the TypeScript compiler, ES6 modules, a module loader (e.g. SystemJS), test runners, npm, a dev web server. To work on a real-world project, you’d also need to learn how to test and bundle your app for deployment.</p>

<p>To jumpstart the development process, the Angular team created a tool called Angular CLI (see <a target="_blank" href="https://github.com/angular/angular-cli">https://github.com/angular/angular-cli</a>), which is a command-line interface that covers all the stages of creating an Angular application from scaffolding and generating an initial app to deployment. The generated code also includes pre-configured files for unit tests and bundling with the powerful Webpack bundler.</p>

<p>While Angular CLI is still in beta, most of the developers like the ease of getting started with new projects. In this article I’ll show you how to create, bundle, and deploy an simple project with Angular CLI.</p>

<p>To get started you have to have NodeJS installed on your computer (see <a target="_blank" href="https://nodejs.org">https://nodejs.org</a>). Both Angular and Angular CLI have lots of dependencies on other packages, which are available at the NPM repository at <a target="_blank" href="https://www.npmjs.com">https://www.npmjs.com</a> (it’s similar to Maven Central). NodeJS comes with npm – a package manager that can install individual packages from npmjs.org or use the config file package.json (like pom.xml) to install all project dependencies. I’ll show you how to create the initial project (including package.json) using Angular CLI.</p>

<p>After installing NodeJS, you can start using npm in the command prompt window (or the Terminal on Mac) to install all required dependencies. Here’s the command to install Angular CLI globally on your computer (-g is for global):</p>

<pre>npm install @angular/cli -g</pre>

<p>After Angular CLI is installed, you can start using its ng command to generate various artifacts such as the new project, components, services as well as building and running the app with included Web server (webpack-dev-server). To create a new project called myproject just enter the following command:</p>

<pre>ng new myproject</pre>

<p>This command will create a new directory with the boilerplate project and install all required dependencies – thousands of files in the node_module directory of your project. This sounds like a lot of files to install, but enterprise Java developers are accustomed to this. Depending on the speed of your Internet connection, it can take anywhere from one to five minutes. This time can be substantially decreased by using the Yarn package manager (an alternative to npm), and I described how to do it in <a target="_blank" href="https://yakovfain.com/2016/11/06/angular-cli-speed-up-installing-dependencies-with-yarn/">this blog post</a>.</p>

<p>Change directory to your newly generated project, and you’ll see something like this:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/january/images/cli.png" alt="app works!"/></p>
<p align="center">Figure 1. Project structure</p>

<p>To build the dev version of this app,   run the following command:</p>

<pre>ng serve</pre>

<p>Now open your browser at <a target="_blank" href="http://localhost:4200">http://localhost:4200</a>, and you’ll see the following page rendered by the top-level component app.component.ts:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/january/images/app.png" alt="running app"/></p>
<p align="center">Figure 2. Running the app</p>

<p>Angular CLI has many other commands and you can read about them in the product documentation at <a target="_blank" href="https://github.com/angular/angular-cli">https://github.com/angular/angular-cli</a>. But I just want to give you a quick overview of different ways of building the app.</p>

<p>The command ng serve invoked the webpack-dev-server that took all required files from our project and bundled them up in memory. The size of this app was not optimized since we work in the development mode now. If you open the network tab in the Dev Tools of your browser, you’ll see that the size of this tiny app is 3MB.</p>

<p>Let’s make it smaller by requesting the production build with optimization:</p>

<pre>ng serve –prod</pre>

<p>Refresh the page at localhost:4200, and you'll see that the size of our app was reduced to 130KB. When you bundle an app with the option -prod, Anfular CLI performs the Ahead-Of-Time (AoT) compilation and doesn't include the the Angular compiler into the bundle.</p>

<div class="row">
  <div class="col col-md-3"><i class="fa fa-info-circle fa-3x" aria-hidden="true"></i></div>
  <div class="col-6 col-md-21">While using the AoT mode will lower the size of the small apps, this may not be the case for the larger ones. But the speed of rendering of the app will improve.</div>
</div>

<br/>
<p>The ng serve command is bundling the app in memory, and automatically rebuilds it as soon as you make changes in the code. But for prod deployment you need files, and the ng build command will create the directory called dist that will contain the files with your application bundles, their gzipped versions, styles, and source maps for debugging TypeScript in the browser. Try it out by running the following command:</p>

<pre>ng build –prod –aot</pre>

<p>Now you can take the content of this dist directory and copy it to your Apache, NGINX, Tomcat, or any application server of your choice. The build and deployment process can be also automated using npm scripts, Gulp, Grunt et al.</p>

<h2>Learn More</h2>
<p>The goal of this article was to give you a very high level overview of the initial generation and deployment of a very simple Angular app. To explore Angular further read its docs at <a target="_blank" href="https://angular.io/docs/ts/latest/">https://angular.io/docs/ts/latest/</a>. Get familiar with TypeScript, which is a piece of cake for any Java developer. This language supports classes, interfaces, annotations, generics, which are very similar to what you use in Java. Knowing the basic syntax of JavaScript is helpful, but not a must. Friends don’t teach friends JavaScript. They teach TypeScript.</p>

<p>I will also be speaking at the Devoxx US conference happening in San Jose, California on March 21-23. I will present two talks about Angular 2:</p> 
<ul>
	<li><a target="_blank" href="http://cfp.devoxx.us/2017/talk/CDA-0488/Angular_2_for_Java_Developers">Angular 2 for Java Developers</a></li>
	<li><a target="_blank" href="http://cfp.devoxx.us/2017/talk/TRS-9820/Reactive_programming_in_Angular_2">Reactive programming in Angular 2</a></li>
</ul>

<p>If you are attending Devoxx US and want to learn more about Angular 2, plan to attend my talks.</p>

<p>I recorded a video that shows how to generate and bundle a project with Angular CLI: <a target="_blank" href="https://www.youtube.com/watch?v=VKQEN7IyanU"/>https://www.youtube.com/watch?v=VKQEN7IyanU</a></p>


<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/january/images/yakov.jpg"
        alt="Yakov Fain" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Yakov Fain<br />
            <a target="_blank" href="http://faratasystems.com/">IT consultancy Farata Systems</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/yfain">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://yakovfain.com/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

