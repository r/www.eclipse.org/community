<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>It should come as no surprise to any developer who regularly reads articles on the web that many organisations are looking to migrate away from (or augment) their current monolithic Java application application. Rightly or wrongly, the microservice architecture has become the defacto best style for implementing web-based applications. However, with the “great power” provided by microservices comes great responsibilities and challenges.</p>

<p>Indeed, much has been written on the challenges associated with implementing microservice-based <a target="_blank" href="http://microservices.io/patterns/microservices.html">architecture</a>, modelling the <a target="_blank" href="http://blog.christianposta.com/microservices/the-hardest-part-about-microservices-data/">data</a>, and the <a target="_blank" href="https://martinfowler.com/bliki/MicroservicePrerequisites.html">operational</a> (and organisational) aspects. However, not much has been written about the challenges of testing with this new architectural style, with the exception of Toby Clemson’s excellent article “<a target="_blank" href="https://martinfowler.com/articles/microservice-testing/">Testing Strategies in a Microservices Architecture</a>”. This article aims to add to the discussion around testing Java-based microservices.</p>

<p>As part of my work with <a target="_blank" href="https://www.specto.io/">SpectoLabs</a> and <a target="_blank" href="https://opencredo.com/">OpenCredo</a> we have helped several organisations build and deploy microservice-based applications, both for greenfield prototypes and brownfield integrations and migrations. We’ve learned lots along the way, and today we are keen to share our findings in how to design, build and test microservice-based systems:</p>

<p><strong>Design the system: Determine service boundaries</strong></p>

<ol>
	<li>Identify areas of business functionality in the existing (or new) system. We often define these areas as <a target="_blank" href="https://en.wikipedia.org/wiki/Domain-driven_design">domain-driven design (DDD)</a> '<a target="_blank" href="http://martinfowler.com/bliki/BoundedContext.html">bounded contexts</a>'.</li>
	<li>This step can take some time (and will also be iterative), but the output is typically a <a target="_blank" href="https://www.infoq.com/articles/ddd-contextmapping">context map</a> which represents the first pass at defining the application service boundaries.</li>
</ol>

<p><strong>Design the service APIs: Determine service functionality</strong></p>

<ol>
	<li>Work with the relevant business owners, domain experts and the development team to define service functionality and APIs.</li>
	<li>Use <a target="_blank" href="https://en.wikipedia.org/wiki/Behavior-driven_development">behaviour-driven design (BDD)</a> technique the '<a target="_blank" href="https://inviqa.com/blog/bdd-guide#example-workshop-introducing-the-three-amigos">Three Amigos</a>'.</li>
<li>The typical outputs from this step include:</li>
	<ul>
		<li>A series of BDD-style acceptance tests that asserts component (single microservice) level requirements, for example <a target="_blank" href="https://cucumber.io/docs/reference">Cucumber Gherkin</a> syntax acceptance test scripts;</li>
		<li>An API specification, for example a <a target="_blank" href="http://swagger.io/">Swagger</a> or <a target="_blank" href="http://raml.org/">RAML</a> file, which the test scripts will operate against.</li>
	</ul>
</ol>

<p><strong>Build services outside-in</strong></p>
<ol>
	<li>Now we have our API specification and associated (service-level) business requirements we can begin building the service functionality outside-in!</li>
	<li>Following Toby Clemson’s excellent article on microservice testing, this is where we use both integration testing and unit testing (both social and solitary), frequently using a <a target="_blank" href="http://coding-is-like-cooking.info/2013/04/outside-in-development-with-double-loop-tdd/">double-loop TDD</a> approach.</li>
	<li>We often use <a target="_blank" href="http://junit.org/">JUnit</a>, <a target="_blank" href="https://github.com/RichardWarburton/lambda-behave">Lambda-behave</a>, <a target="_blank" href="http://site.mockito.org/">Mockito</a>, and <a target="_blank" href="https://specto.io/blog/2017/1/4/stubbing-http-apis-and-microservices-with-the-hoverfly-java-dsl">Hoverfly Java</a> for foundational unit testing.</li>
</ol>

<p><strong>Component test</strong></p>
<ol>
	<li>In combination with building a service outside-in we also work on component-level testing. This differs from the integration testing mentioned above, in that component testing operates via the public API and tests an entire slice of business functionality.</li> 
<li>Typically the first wave of component tests utilise the acceptance test scripts we defined in 2.3, and these assert that we have implemented the business functionality correctly within this service</li>
<li>Tooling we like here includes <a target="_blank" href="http://rest-assured.io/">REST-assured</a> and <a target="_blank" href="https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html">Spring Boot testing features</a>.</li>
<li>Test non-functional requirements (NFRs). Examples include:</li>
	<ul>
		<li>Performance testing of a series of core happy paths offered by the service e.g. <a target="_blank" href="http://jmeter.apache.org/">JMeter</a> (often triggered via the <a target="_blank" href="https://wiki.jenkins-ci.org/display/JENKINS/Performance+Plugin">Jenkins Performance Plugin</a>) or <a target="_blank" href="http://gatling.io/">Gatling</a> (often run via <a target="_blank" href="https://flood.io/">flood.io</a>).</li>
		<li>Basic security testing using a framework like Continuum Security’s <a target="_blank" href="https://github.com/continuumsecurity/bdd-security">bdd-security</a>, which includes the awesome <a target="_blank" href="https://www.owasp.org/index.php/OWASP_Zed_Attack_Proxy_Project">OWASP ZAP.</a></li>
		<li>Fault-tolerance testing, where we deterministically simulate failures using <a target="_blank" href="https://github.com/SpectoLabs/hoverfly">Hoverfly</a> and <a target="_blank" href="https://www.specto.io/blog/service-virtualization-is-so-last-year.html">associated middleware</a> (and in the past, <a target="_blank" href="https://github.com/tomakehurst/saboteur">Saboteur</a>).</li>
		<li>Visibility testing, which asserts that the service offers the expected endpoints for metrics and health checks.</li>
	</ul>
</ol>

<p><strong>Contract test - verify the component interactions</strong></p>
<ol>
	<li>Verify the proposed interaction between components.</li>
	<li>A popular approach for this in the microservice world is by using <a target="_blank" href="http://martinfowler.com/articles/consumerDrivenContracts.html">consumer-driven contracts</a>, and this can be implemented using frameworks like <a target="_blank" href="https://cloud.spring.io/spring-cloud-contract/spring-cloud-contract.html">Spring Cloud Contract</a>, <a target="_blank" href="https://github.com/DiUS/pact-jvm">Pact-JVM</a> or <a target="_blank" href="https://github.com/thoughtworks/pacto">Pacto</a>.</li>
</ol>

<p><strong>End-to-end (E2E) tests: Asserting system-level business functionality and NFRs</strong></p>

<ul>
	<li>E2E automated tests essentially assert core user journeys and application functionality (and also prevent regression).</li>
	<li>Test non-functional/cross-functional requirements, for example, asserting that all critical business journey are working, respond within a certain time, and are secure.</li>
	<li>When E2E tests touch systems that are not available then use tooling like <a target="_blank" href="https://github.com/SpectoLabs/hoverfly">Hoverfly</a> to simulate the API. Latency or failures can be injected via <a target="_blank" href="https://www.specto.io/blog/service-virtualization-is-so-last-year.html">Hoverfly middleware</a>.</li>
	<li>Outputs from this step of the process should include:</li>
	<ul>
		<li>A correctly functioning and robust system;</li>
		<li>The automated validation of the system;</li>
		<li>Happy customers!</li>
		</ul>
</ul>
 
<h2>Final Words</strong></h2>
<p>If you have enjoyed reading this article, then a more comprehensive version can be found at specto.io: “<a target="_blank" href="https://specto.io/blog/recipe-for-designing-building-testing-microservices.html">A Proposed Recipe for Designing, Building and Testing Microservices</a>”. You can also find me speaking at Devoxx US in March, focusing on microservice antipatterns, “<a target="_blank" href="http://cfp.devoxx.us/2017/talk/BDX-6295/The_Seven_(More)_Deadly_Sins_of_Microservices">The Seven (More) Deadly Sins of Microservices</a>”.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/january/images/danielbryant.jpeg"
        alt="Daniel Bryant" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Daniel Bryant<br />
            <a target="_blank" href="https://opencredo.com/">OpenCredo</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/danielbryantuk">Twitter</a></li>
           	<li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/danielbryantuk/">LinkedIn</a></li>
            <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

