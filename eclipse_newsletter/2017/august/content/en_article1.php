<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p><i>Cloudy with a chance of tanks</i></p>

<p>While it’s easier than ever to define and train deep neural networks (DNNs), understanding the learning process remains somewhat opaque. Monitoring the loss or classification error during training won’t always prevent your model from learning the wrong thing or learning a proxy for your intended classification task. To understand what we mean, consider <a target="_blank" href="http://intelligence.org/files/AIPosNegFactor.pdf">this (possibly apocryphal) story</a> [1]:</p>

<p><i>Once upon a time, the US Army wanted to use neural networks to automatically detect camouflaged enemy tanks. The researchers trained a neural net on 50 photos of camouflaged tanks in trees, and 50 photos of trees without tanks…</p>

<p>Wisely, the researchers had originally taken 200 photos, 100 photos of tanks and 100 photos of trees. They had used only 50 of each for the training set. The researchers ran the neural network on the remaining 100 photos, and without further training the neural network classified all remaining photos correctly. Success confirmed! The researchers handed the finished work to the Pentagon, which soon handed it back, complaining that in their own tests the neural network did no better than chance at discriminating photos.</p>

<p>It turned out that in the researchers’ dataset, photos of camouflaged tanks had been taken on cloudy days, while photos of plain forest had been taken on sunny days. The neural network had learned to distinguish cloudy days from sunny days, instead of distinguishing camouflaged tanks from empty forest.</i></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/ft17_argonne_1918.gif"/></p>
<p align="center"><i><small>Definitely a real image of a modern tank from the training set. Source: Wikipedia</small></i></p>

<p>Regardless of the veracity of this tale, the point is familiar to machine learning researchers: training metrics don’t always tell the whole story. And the stakes are higher than ever before: for rising applications of deep learning like autonomous vehicles, these kinds of training errors <a target="_blank" href="https://www.theverge.com/2016/6/30/12072408/tesla-autopilot-car-crash-death-autonomous-model-s">can be deadly</a> [2].</p>

<p>Fortunately, standard visualizations like <a target="_blank" href="https://arxiv.org/abs/1311.2901">partial occlusion</a> [3] and <a target="_blank" href="https://arxiv.org/abs/1312.6034">saliency maps</a> [4] provide a sanity check on the learning process. <a target="_blank" href="https://github.com/yosinski/deep-visualization-toolbox">Toolkits</a> [5] for standard neural network visualizations exist, along with tools for monitoring the <a target="_blank" href="https://www.tensorflow.org/get_started/summaries_and_tensorboard">training process</a>. They’re often tied to the deep learning framework, if not model-specific. Could a general, easy-to-setup tool for generating standard visualizations have saved these researchers from detecting sunny days instead of tanks?</p>

<h2>Eclipse Picasso</h2>
<p>Eclipse Picasso is a free open-source (<a href="https://www.eclipse.org/legal/epl-v10.html">Eclipse Public License</a> (EPL)) DNN visualization tool that gives you partial occlusion and saliency maps with minimal fuss. At Merantix, we work with a variety of neural network architectures; we developed Picasso to make it easy to see standard visualizations across our models in our various verticals: including applications in <a target="_blank" href="http://www.merantix.com/industry/automotive/">automotive</a>, such as understanding when road segmentation or object detection fail; <a target="_blank" href="http://www.merantix.com/industry/advertising/">advertisement</a>, such as understanding why certain creatives receive higher click-through rates; and <a target="_blank" href="http://www.merantix.com/industry/health/">medical imaging</a>, such as analyzing what regions in a CT or X-ray image contain irregularities.</p>

<p>Picasso is a <a target="_blank" href="http://flask.pocoo.org/">Flask</a> application that glues together a deep-learning framework with a set of default and user-defined visualizations. You can use the built-in visualizations and easily add your own. Picasso was developed to work with checkpointed Keras and Tensorflow neural networks. If you want to try it out but don’t have any trained models, we provide Tensorflow and Keras <a target="_blank" href="http://yann.lecun.com/exdb/mnist/">MNIST</a> checkpoints and a Keras <a target="_blank" href="http://www.robots.ox.ac.uk/~vgg/research/very_deep/">VGG16</a> checkpoint for you.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/august/images/overview.gif"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/overview_sm.gif"/></a></p>
<p align="center"><i><small>An overview of the application flow with the default settings. The user has loaded a Keras model trained on the MNIST dataset, and generates a partial occlusion visualization on a couple handwritten digit images. See below for an in-depth explanation of occlusion maps.</small></i></p>

<p>At Merantix, we are particularly interested in convolutional neural networks (CNNs) that take images as inputs and do classification. We developed Picasso with these parameters in mind. However, the framework is flexible enough to use on all kinds of models. While the included visualizations should be fairly robust across different NNs, you can still implement model-specific visualizations if you want to.</p>
<p>We provide a few standard visualizations out of the box:</p>
	<ol>
        <li><b><a target="_blank" href="https://arxiv.org/abs/1311.2901">Partial occlusion</a></b> - Occlude parts of the image and see how the classification changes.</li>
        <li><b><a target="_blank" href="https://arxiv.org/pdf/1312.6034.pdf">Saliency map</a></b> - Compute derivatives of class predictions with respect to the input image.</li>
        <li><b>Class Prediction</b> - Not a visualization per se, but can be a handy, simple check on the learning process.</li>
    </ol>
<p>And we have a couple more in the works! For a more in-depth exposition, see our paper on <a target="_blank" href="https://arxiv.org/abs/1705.05627">arXiv</a>.</p>

<h2>Picasso in practice</h2>
<p>Let’s attack the tank problem with Picasso’s two builtin visualizations: partial occlusion and saliency maps. In these examples, we’ll used a pre-trained VGG16 model for classification. We already know this model is pretty good at classifying tanks: can we use these visualizations to check that model is actually classifying based on the tank and not, say, the sky?</p>

<p align="center"><a href="/community/eclipse_newsletter/2017/august/images/tank_crop_occ.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/tank_crop_occ_sm.png"/></a></p>
<p align="center"><i><small>By sequentially blocking out parts of the image, we can tell which regions are more important to classification. This image was classified by the <a target="_blank" href="http://www.robots.ox.ac.uk/~vgg/research/very_deep/">VGG16</a> model, with a 94% classification probability of “tank.” Bright parts of the image correspond to higher probability of the given classification. For instance, <b>the sky regions are very bright because occluding the sky doesn’t affect the probability of this image being classified as a tank</b>. And conversely, the tank tread regions are darker because without them, it’s hard to for the model to know if it’s looking at a tank.
</small></i></p>

<p>We can see how this visualization may have helped the Army: it’s clear that when the “tanky” parts are missing (for instance, the tank treads), the model can’t successfully classify it. Interestingly, when you block some of the tread, there’s a higher probability of classifying the image as a half-track. This intuitively makes sense, because a <a target="_blank" href="https://en.wikipedia.org/wiki/Half-track">half-track has regular wheels in the front</a>.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/halftrack_occlusion.png"/></p>
<p align="center"><i><small>The model is quite sure this is a half-track — unless you block out the wheels! It’s also quite sure it’s not a tank; that is, unless you occlude the wheels. Image source: <a target="_blank" href="https://upload.wikimedia.org/wikipedia/commons/c/c8/M3_Half_Track%2C_Thunder_Over_Michigan_2006.jpg">Wikipedia</a></small></i></p>

<p>In addition to partial occlusions, we also provide saliency maps out-of-the-box. Saliency maps look at the derivative of the input image (via backpropagation) with respect to classification. A high value at a given pixel means changing this pixel should more dramatically affect the classification.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/tank_saliency.png"/></p>
<p align="center"><i><small>Saliency map for the tank. Brighter pixels indicate higher values for the derivative of “tank” with respect to that input pixel for this image. The brightest pixels appear to be in the tank region of the image, which is a good sign. Notice that with a few exceptions, the non-tank areas are largely dark — meaning changing these pixels should not make the image more or less “tanky.”</small></i></p>

<h2>Adding visualizations</h2>
<p>We wanted it to be particularly easy to integrate new visualizations. All you need to do is drop your visualization code in the <code>visualizations</code> folder and cook up an HTML template to display it.</p>

<p>See the <a target="_blank" href="https://picasso.readthedocs.io/en/latest/visualizations.html">tutorial</a> on the <code>ClassProbabilites</code> visualization for an example on how to build very simple visualization.</p>

<p align="center"><a href="/community/eclipse_newsletter/2017/august/images/picasso_visualizer.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/picasso_visualizer_sm.png"/></a></p>
<p align="center"><i><small>Giving the relative classification probabilities is about the simplest visualization you can make.</small></i></p>

<h2>Using your own Models</h2>
<p>Naturally, you’ll want to use the included visualizations with your own trained neural networks. <a target="_blank" href="https://picasso.readthedocs.io/en/latest/models.html">We’ve tried to make this as simple as possible</a>, but at the minimum you’ll need to define three methods:</p>
	<ol>
        <li><code>preprocess</code> tell the visualization how to change uploaded images into NN inputs</li>
        <li><code>postprocess</code> tell the visualization how to change flattened intermediate layers to the image dimensions (this is needed by visualizations which operate on intermediate layers, like saliency maps)</li>
        <li><code>decode_prob</code> tell the visualization how to interpret the raw output, usually an array of probabilities, by annotating with class names</li>
    </ol>
<p>How to construct these functions is detailed in this tutorial. These functions can be specified separately from the source code for the app.</p>

<p align="center"><a href="/community/eclipse_newsletter/2017/august/images/picasso_visualizer2.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/picasso_visualizer2_sm.png"/></a></p>
<p align="center"><i><small>The results for a saliency map visualization. The app is using the Keras framework with a <a target="_blank" href="http://www.robots.ox.ac.uk/~vgg/research/very_deep/">VGG16</a> model. This example comes prepackaged with the code. Since saliency maps depend on the derivative of the input layers with respect to an intermediate layer, you must tell the visualization how to reshape the output tensor back into an image with `decode_prob`.</small></i></p>

<h2>Contributing</h2>
<p>We’re very open to suggestions about how better to structure our application. And if you’d like to contribute a visualization or anything else, even better! Head over to our <a target="_blank" href="https://github.com/merantix/picasso">GitHub</a> repository for more. We’re releasing Picasso under the EPL because we intend for it to become part of the Eclipse Foundation.</p>

<h2>Acknowledgements</h2>
	<ul>
        <li><a target="_blank" href="https://github.com/Sylvus">Elias</a> and <a target="_blank" href="https://github.com/scopelf">Filippo</a> for early code contributions and finding bugs and issues.</li>
        <li><a target="_blank" href="https://github.com/JohnMcSpedon">John</a>, <a target="_blank" href="https://github.com/jwayne">Josh</a>, <a target="_blank" href="https://github.com/rrothe">Rasmus</a>, and <a target="_blank" href="https://github.com/knub">Stefan</a> for their careful code review and feedback on this article.</li>
        <li><a target="_blank" href="https://github.com/dmrd">David</a> and <a target="_blank" href="https://www.linkedin.com/in/nader-al-naji-86b14a3a/">Nader</a> for reviewing and discussion of this article.</li>
	</ul>
	
<h2>References</h2>
	<ol>
        <li>Yudkowsky, Eliezer. “Artificial Intelligence as a Positive and Negative Factor in Global Risk.” Global Catastrophic Risks, edited by Nick Bostrom and Milan M. Ćirković, 308–345. New York: Oxford University Press. 2008.</li>
       <li> “Tesla Driver Killed In Crash With Autopilot Active, NHTSA Investigating”. The Verge. N.p., 2017. Web. 11 May 2017.</li>
        <li>Zeiler, Matthew D., and Rob Fergus. “Visualizing and understanding convolutional networks.” European conference on computer vision. Springer International Publishing, 2014.</li>
        <li>Simonyan, Karen, Andrea Vedaldi, and Andrew Zisserman. “Deep inside convolutional networks: Visualising image classification models and saliency maps.” ICLR Workshop, 2014.</li>
        <li>Yosinski, Jason, et al. “Understanding neural networks through deep visualization.” Deep Learning Workshop, International Conference on Machine Learning (ICML), 2015.</li>
	</ol>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/august/images/ryan.jpeg"
        alt="Ryan Henderson" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Ryan Henderson<br />
            <a target="_blank" href="merantix.com">Merantic GmbH</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/rhsimplex">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

