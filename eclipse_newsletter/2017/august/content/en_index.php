<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2017.08.22</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxanne.jpg" />
                            <h3>Editor's Note</h3>
                           <p>I'm excited to share this month's newsletter articles with you. They are all about new and exciting projects that have chosen to make the Eclipse Foundation their home. It covers a bit of everything, including visualization tools, quantum computing, traffic flow simulation, language based extensions, and language testing. </p>
             				<p>The program schedule for EclipseCon Europe (Oct 24-26) is <a style="color:#000;" href="https://www.eclipsecon.org/europe2017/conference/schedule/session/2017-10-24">now available</a>. Start planning your conference schedule today!</p>
             				<p>For those of you specifically interested in IoT, we are launching an Eclipse IoT Newsletter next month! It'll be similar to this one, but Eclipse IoT specific. <a style="color:#000;" href="http://eepurl.com/cYn4-n">Subscribe here</a>.
             				<p>Have you downloaded Eclipse Oxygen yet? <a style="color:#000;" href="https://www.eclipse.org/oxygen">Download it now</a>.
             				<p>Happy reading!<br>
							Roxanne Joncas<br>
							<a target="_blank" style="color:#000;" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Eclipse Oxygen Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/picasso.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article1.php"><h3>Eclipse Picasso: A free open-source visualizer for Convolutional Neural Networks</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse Picasso is a free open-source DNN visualization tool that gives you partial occlusion and saliency maps with minimal fuss.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/sumo.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article2.php"><h3>#Simulation of Urban MObility - SUMO</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse SUMO (Simulation of Urban MObility) is a microscopic, inter- and multi-modal, space-continuous and time-discrete traffic flow simulation platform.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/acute.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article3.php"><h3>Eclipse aCute for C# and .NET Core</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse aCute is a language based extension that provides development tools for C# and .NET Core inside the Eclipse IDE.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/eclipsexacc.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article4.php"><h3>Eclipse Science and Open Source Software for Quantum Computing</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse XACC is an open-source hybrid programming model and reference implementation called the eXtreme-scale ACCelerator programming framework, or XACC for short.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

       <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/xpect1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/august/article5.php"><h3>Xpect: Easy Testing and Reviews for Xtext Languages</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Xpect is a framework that helps you to specify, test, and discuss Xtext languages.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Java IDE Tips &amp; Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">

                            <p>Here is this month's tip in action &#10145;<br>
                            <i><small>Click on the gif to view the original tweet and gif.</small></i></p>
                            <p>It was all going great, but now your code is formatted so badly you can't even look at it. No problem, Ctrl+Shift+F tidies it up in no time!</p>
                            <pre>Ctrl+Shift+F</pre>
                            <p>Want more tips? Follow <a target="_blank" style="color:#000;" href="https://twitter.com/eclipsejavaide">@EclipseJavaIDE</a> on Twitter.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://twitter.com/EclipseJavaIDE/status/890524728519163905"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/prettycode.gif"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->



  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/assets/public/images/Eclipse%20IDE%20Keybindings.pdf"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/june/images/eclipsekeybindings2.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse IDE Keybindings</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Always wanted to know what the default <a target="_blank" style="color:#000;" href="https://www.eclipse.org/community/eclipse_newsletter/assets/public/images/Eclipse%20IDE%20Keybindings.pdf">Eclipse IDE keyboard shortcuts</a> were? Well, here you go!</p>
                            <p>You can always personalize the shortcuts to fit your needs under <code>Preferences &rarr; General &rarr; Keys</code>. Or use the Quick Access Toolbar <code>Ctrl+3</code> or <code>⌘+3</code> and simply type <code>keys</code>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
   <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="http://eepurl.com/cYn4-n">Launching Eclipse IoT Newsletter</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://eclipse.org/org/press-release/20170814_security_bulletin.php">Potential adware plugin on Eclipse Marketplace - Eclipse Class Decompiler</a></li>
                            <li><a target="_blank" style="color:#000;" href="http://thingmonk.com/eclipse-day-talks.html">Eclipse IoT Day @ ThingMonk</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
							<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipse-microprofile-1-1-136172.html">Eclipse MicroProfile 1.1 release: Better JDK 8 compatibility</a></li>
							<li><a target="_blank" style="color:#000;" href="https://www.contact-software.com/en/news/2017/07/contact-software-takes-part-in-eclipse-iot-projects/">CONTACT Software takes part in Eclipse IoT Projects</a></li>
							<li><a target="_blank" style="color:#000;" href="https://tech.io/blog/reactive-programming-eclipse-vert-x/">Reactive Programming with Eclipse Vert.x</a></li>
						   </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.xpect">Eclipse Xpect</a>: Eclipse Xpect is a unit- and integration-testing framework to be used for Xtext-based languages.</li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.xsemantics">Eclipse Xsemantics</a>: Eclipse Xsemantics is a DSL (implemented in Xtext itself) for writing type systems, reduction rules, interpreters and general relation rules for languages implemented in Xtext.</li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
                             <li><a target="_blank" style="color:#000;" href="https://developer.ibm.com/wasdev/blog/2017/08/09/microprofile-1-1-announced/">Eclipse MicroProfile 1.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.dirigible/releases/2.8/plan">Eclipse Dirigible 2.8</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lyo/releases/2.2.0/bugs">Eclipse Lyo 2.2</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.linuxtools/releases/6.1.0/bugs">Eclipse Linux Tools 6.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.app4mc/releases/0.8.1/bugs">Eclipse APP4MC 0.8.1</a></li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.papyrus-rt/releases/1.0.0/bugs">Eclipse Papyrus for Real Time (Papyrus-RT) 1.0</a></li>
                            	</ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/august_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse events:</p>
                             <p><a target="_blank" style="color:#000;" href="https://www.meetup.com/Trondheim-Eclipse-User-Group/events/236894151/">Eclipse DemoCamp Trondheim</a><br>
                            Aug 24, 2017 | Trondheim, Norway</p>
                            <p><a target="_blank" style="color:#000;" href="http://thingmonk.com/">Eclipse IoT Day @ Thingmonk</a><br>
                            Sep 11, 2017 | London, UK</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_Day_Milano_2017">Eclipse Day Milano 2017</a><br>
                            Sep 22, 2017 | Milan, Italy</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2017/">EclipseCon Europe 2017</a><br>
                            Oct 24-26, 2017 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://www.siriuscon.org/">SiriusCon 2017</a><br>
                            Nov 9, 2017 | Paris, France</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
