<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p><a href="https://projects.eclipse.org/proposals/eclipse-sumo">Eclipse SUMO</a> (Simulation of Urban MObility) is a microscopic, inter- and multi-modal, space-continuous and time-discrete traffic flow simulation platform. The implementation of SUMO started in 2001, with a first open source release in 2002. There were two reasons for making the work available as open source under the GNU General Public License (GPL), and soon also under Eclipse Public License (EPL). The first was the wish to support the traffic simulation community with a free tool into which own algorithms can be implemented. Many other open source traffic simulations were available, but being implemented within a student thesis, they got unsupported afterwards. A major drawback – besides reinvention of the wheel – is the almost non-existing comparability of the implemented models or algorithms, and a common simulation platform is assumed to be of benefit here. The second reason for making the simulation open source was the wish to gain support from other institutions.</p>
 
<p>Included with SUMO is a plethora of supporting tools that handle tasks such as route finding, visualization, network import and emission calculation. SUMO can be enhanced with custom models and provides various APIs to remotely control the simulation.</p>

<p>Some key features of SUMO are:</p>
	<ul>
        <li>Microscopic simulation - vehicles, pedestrians and public transport are modeled explicitly</li>
        <li>Online interaction – control the simulation with TraCI (a socket based communication protocol)</li>
        <li>Simulation of multimodal traffic, i.e., vehicles, public transport and pedestrians</li>
        <li>Time schedules of traffic lights can be imported or generated automatically by SUMO</li>
        <li>No artificial limitations in network size and number of simulated vehicles</li>
        <li>Supported import formats: OpenStreetMap, VISUM, VISSIM, HERE</li>
        <li>SUMO is implemented in C++ and uses only portable libraries</li>
        <li>Cross-Platform support – SUMO is tested on Linux, macOS, and Windows</li>
	</ul>
	
<p>SUMO provides a variety of tools to setup and run transport simulations:</p>
	<ul>
        <li><b>SUMO:</b> command line simulation</li>
        <li><b>GUISIM:</b> simulation with a graphical user interface</li>
        <li><b>NETCONVERT:</b> network importer</li>
        <li><b>NETGEN:</b> abstract networks generator</li>
        <li><b>OD2TRIPS:</b> converter from O/D matrices to trips</li>
        <li><b>JTRROUTER:</b> routes generator based on turning ratios at intersections</li>
        <li><b>DUAROUTER:</b> routes generator based on a dynamic user assignment</li>
        <li><b>DFROUTER:</b> route generator with use of detector data</li>
        <li><b>MAROUTER:</b> macroscopic user assignment based on capacity functions</li>
        <li><b>NETEDIT:</b> Visual editor for street networks, traffic lights, detectors, and further network elements</li>
	</ul>
	
<p>Although the setup of a realistic city-scale transport simulation scenario requires expert knowledge and incorporates many different data sources, there are several ways to get a basic transport simulation scenario with a few simple steps. The most straightforward way is the usage of the OSM Web Wizard, which is part of the SUMO package. With the OSM Web Wizard the user can setup a simple scenario as easy as selecting the desired area on OpenStreetMap in a web browser. Besides selecting the desired area, the OSM Web Wizard also provides some options to define the traffic demand (e.g. modes of transport). From this selection, the OSM Web Wizard generates a complete SUMO simulation scenario, which will be run and displayed in the graphical user interface. Obviously, the so generated scenario is only a coarse reproduction of the real world, but it will give the interested user a first impression of what SUMO is capable of.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/august/images/sumo_webwizard.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/sumo_webwizard_sm.png"/></a></p>
<p align="center"><i>OSM Web Wizard for SUMO</i></p>

<p>Over the years, SUMO has been applied in many different contexts. Some examples are:</p>
	<ul>
        <li>Evaluate the performance of traffic lights, including the evaluation of modern algorithms up to the evaluation of weekly timing plans.</li>
        <li>Vehicle route choice has been investigated, including the development of new methods, the evaluation of eco-aware routing based on pollutant emission, and investigations on network-wide influences of autonomous route choice.</li>
        <li>SUMO was used to provide traffic forecasts for authorities of the City of Cologne during the Pope’s visit in 2005 and during the Soccer World Cup 2006.</li>
        <li>SUMO was used to support simulated in-vehicle telephony behavior for evaluating the performance of GSM-based traffic surveillance.</li>
        <li>SUMO is widely used by the V2X community for both, providing realistic vehicle traces, and for evaluating applications in an on-line loop with a network simulator.</li>
        <li>Online simulator-coupling to provide background traffic for driving simulators.</li>
        <li>Energy-consumption modelling, e.g. for electric vehicles.</li>
	</ul>
	
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/august/images/sumo_devsvn.jpg"/></p>
<p align="center"><i>SUMO GUISIM in action, showing vehicles meeting at an intersection</i></p>
	
<p>Through the variety of available models, tools, interfaces and through its open approach SUMO has become one of the leading frameworks for microscopic traffic modeling.</p> 

<h2>Learn More</h2>
<p>If you want to learn more about SUMO, please visit the <a target="_blank" href="http://sumo.dlr.de/wiki">wiki page</a>. Support for Eclipse SUMO is currently provided in a very active <a target="_blank" href="https://sourceforge.net/projects/sumo/lists/sumo-user">mailing list</a>.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/august/images/gregor.png"
        alt="Gregor Laemmel" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Gregor Laemmel<br />
            <a target="_blank" href="http://www.dlr.de/">Deutsches Zentrum für Luft- und Raumfahrt e.V. (DLR)</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/GrgrLmml">GitHub</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.researchgate.net/profile/Gregor_Laemmel">ResearchGate</a></li>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

