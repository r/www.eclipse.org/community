<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Eclipse CDT features a rich debug environment that helps developers track down bugs and performance issues in their C/C++ code. Whether using gdb or a different backend debugger, CDT Debug provides a consistent visual interface and has several powerful views that provide different insights into your code. This article highlights some of the newest and  most useful features in Eclipse CDT 9.2 edition that helps save precious time while debugging your code.</p>

<h3>Step Into Selection</h3>

<p>Typical run control allows you to navigate through your code with commands such as run, stop, step, etc. <i>‘Run to line’</i> is particularly handy when you want to get to a specific portion of code. Additionally, another great time saver is the <i>‘Step Into Selection’</i> command. This is useful while debugging a particular bit of code and you want to step into one call without having to tediously step in and out of calls made on the same line. This is particularly the case with C++ where you might have a lot of getter calls as arguments to another method. There are a number of ways to invoke this command, depending on your particular preference.</p>

<p>While debugging, select the method of interest and:</p>
<ul>
<li>Press <code style="color:blue;">CTRL+ALT</code> and click on the method (by far the quickest way to use it).</li>
<li>Right-click and select <i>‘Step Into Selection’</i> from context-menu.</li>
<li>Use keyboard shortcut <code style="color:blue;">CTRL+F5</code></li>
</ul>

<p>The example below shows while debugging the Python source code, how you can set straight into the <code style="color:blue;">_PyMem_RawStrdup</code> method while avoiding the setlocale if you just did a normal step into.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/debug_python.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/debug_python_sm.png" alt="Debugging Python Source Code"/></a></p>

<p>Step into selection means no need to step through any more tiresome setters and getters!</p>

<h3>Dynamic Printf</h3>

<p>Dynamic printf gives you the ability to insert a printf call into your program on-the-fly, without having to recompile it or restart it. Your source code remains unchanged once you’ve finished debugging. Dynamic printf is a type of software trace which helps avoids serious slowdowns of code instead of having to stop at a regular breakpoint. Dprintf can be used with printf style formatting and expressions to make more useful debug messages.</p>

<p>The following screenshots show dynamic printf being used to trace the calls to the write method in the C-Python source.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/printf_trace.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/printf_trace_sm.png" alt="Printf trace calls"/></a></p>

<ol>
<li>Right click on margin to insert a dynamic printf.</li>
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/printf_common.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/printf_common_sm.png" alt="Printf trace calls common"/></a></p>
<li>Customise dprintf properties, using printf style formatting.</li>
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/customise_dprintf.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/customise_dprintf_sm.png" alt="Customize Printf Properties"/></a></p>
<li>Dprintf can be seen in breakpoints view, messages are written to console every time the dprintf is hit.</li>
</ol>

<h3>Disassembly View</h3>

<p>This is a great feature for embedded developers, debugging drivers or for anyone operating at the assembly language level. You can quickly switch to disassembly view, which gives you a nice shading as you step through the code. A new feature in Eclipse Neon is that you can hover over registers to see their value instantly, another great time saver over having to open the registers view and find the register of interest.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/disassembly.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/disassembly_sm.png" alt="Disassembly View"/></a></p>

<h3>Memory View</h3>

<p>When you need insight into your program’s memory, CDT has a handy memory view to use. The memory view displays the bytes and can be easily rendered in different formats to support the data being inspected. New in CDT 9, the memory view comes with annotations to help the user quickly map pointers, variables, etc to the bytes in the memory view.</p>

<p>Here’s an example looking at the stack pointer while debugging the <code style="color:blue;">fileio_write</code> call in CPython. The memory view updates as you click on various levels of the stack trace in the debug view.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/memory_view.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/memory_view_sm.png" alt="Memory View"/></a></p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/memory_view2.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/memory_view2_sm.png" alt="Memory View 2"/></a></p>

<h3>Optimal Watch Expressions</h3>

<p>While debugging, although variables can be inspected, in some cases the value of the variables is not always interpreted in the optimal format. In this case, you can right click on the variable and use the <i>‘Cast To Type’</i> method to change it to a more appropriate type, for example from a void * to a char * for a String. And if you want to keep that for later, you can right click and select <i>‘Watch’</i> which will create a full expression which can be very useful.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/watch_expression.png" alt="Optimal Watch Expressions" /></p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/expressions.png" alt="Expressions" /></p>

<p>These are just some of the features available while debugging CDT. To see some of these latest debug features in actions, take a look at <a target="_blank" href="https://youtu.be/-ApILIyEeuw?t=16m11s">this talk</a> from Jonah Graham in which he debugs the CPython codebase using CDT, demonstrating the features here and more.</p>

<p>Eclipse CDT provides a rich debug interface with lots of features to help every C/C++ developer get to the heart of their issue in the most effective way.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/april/images/tracymiranda.JPG"
        alt="Tracy Miranda" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Tracy Miranda<br />
            <a target="_blank" href="https://kichwacoders.com/">Kichwa Coders</a>
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/tracymiranda?lang=en">Twitter</a></li>
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://ca.linkedin.com/in/tracymiranda">LinkedIn</a></li>
            <?php echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
