<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<h3>Introduction</h3>

<p>There is a lot of buzz these days around Docker Containers. Docker Containers
are akin to lightweight Virtual Machines whereby one can run an application in isolation from the host OS. They differ from traditional VMs in that they share the machine's system kernel and use filesystem layering to share common files. Since they share existing resources, startup is quick and they use less storage.</p>

<p>Docker Containers are created using Docker Images which are templates that define the initial filesystem contents of the Container including the OS, installed packages, libraries, configuration files, and general user files. The Docker Container is an instance of an image running a particular command or application. Running the same command or application in the same image should produce similar results (timing issues notwithstanding).</p>

<p>Images are prepared in advance and are usually based on other existing images. Modifications made to the base image result in layers being stored and when the image is used to create a container, the layers are assembled using the layered filesystem. This can be done extremely quickly and minimizes the storage required by a new image to just the filesystem delta. Base images are supplied for most of the various Linux variants and releases to build more complex images. For example, your application might require gdbserver installed so you can create an image that takes a base OS and adds the gdbserver package. How to do this will be demonstrated later in this article. That image can, in turn, be used to form other images that need more than just gdbserver installed.</p>

<p>Using Docker requires a Docker daemon be up and running either locally or remotely. An end-user specifies the location either as a locally running Unix socket (Linux) or as a TCP address. Actions are performed using the "docker" command which performs a number of various tasks via the daemon (e.g. start a container, pull an image, build an image).</p>

<p>Images are stored in a registry. Docker provides a default registry that you can use to access existing images or to store images you have created for others to share. To access an existing image you need to first pull it from a registry. Any images you create based on this image are only known to your daemon and are persistent, but if you wish to make such images accessible to others, you will need to push to the registry. The Docker default registry is free to use after you have created an id. Images placed there are by default public, but you can designate an image as private. While the Docker registry is default, a local private registry can be created to be used by the daemon (for example, to provide pre-release versions of software that are confidential).</p>

<h3>Docker Tooling in Eclipse IDE</h3>

<p>With the Eclipse Mars release, the Linux Tools project contributed the Docker Tooling plug-ins. This set of plug-ins provides access to docker command tasks with an Eclipse UI, making it easier to use. The Docker Tooling perspective includes an Explorer View which shows a tree view of Docker daemon connections, an Images View which shows a table of images that have been pulled by a connection, and a Containers View which shows containers that have been created by the active connection.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/dockertoolingperspective.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/dockertoolingperspective_sm.png" alt="Docker Tooling Perspective"/></a></p>

<h3>Launching C/C++ Executables</h3>

<p>In the CDT, an optional feature was added to allow launching and debugging of C/C++ projects in a Docker Container. At present, building is not yet supported but bug:<a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=513589">513589</a> has been opened and includes prototype changes to the master branch. This means that the executable must be compatible for the target OS of the Container (i.e. Elf format and dynamic library functionality compatible with Container's level of base OS).</p>

<p>To install the feature, click on Help &rarr; Install New Software... and choose the Neon release repository (http://download.eclipse.org/releases/neon). Under the Programming Languages category, choose the C/C++ Docker Container Launch Support feature.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/cdtlaunchinstall.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cdtlaunchinstall_sm.png" alt="C/C++ Docker Launch Support"/></a></p>

<p>Launching uses the Linux Tools Docker Tooling plug-ins and so they will also be installed if not already present in your Eclipse IDE.</p>

<p>The launch uses volume mounting to make the C/C&plus;&plus; executable accessible to the Container. Otherwise, one would have to make a Docker Image with the C/C&plus;&plus; executable in the filesystem. This would take a lot more time and require a new Docker Image whenever the executable gets built. Mounting on the other hand allows the same image to be used with each container run getting the updated executable. It should be noted that when running the Docker daemon locally, we are actually mounting the data, but when the daemon runs remotely, this is not possible so empty volumes are created and data is copied from the host to the volumes. The result is the same and this is done automatically by the plug-ins.</p>

<p>Running a C/C++ executable can be done two ways:</p>
<ul>
	<li>Right-click on the binary and select "Run as &rarr; C/C++ Container Application".</li>
	<li>From the Run Configurations dialog, create a new C/C++ Container Application launch configuration.</li> 
	</ul>

<p>Option 1 will default the Docker Connection to the first known connection and will either use the default image set in Window &rarr; Preferences &rarr; C/C++ &rarr; Docker Container Launch or else use an arbitrary image which is first in the list of images. After starting, a launch configuration is created which you can then modify via the Run Configurations dialog.</p>

<p>Option 2 allows you to specify all settings before performing the run via the launch configuration Container tab.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/launchconfigcontainertab.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/launchconfigcontainertab_sm.png" alt="C/C++ Docker Launch Configuration"/></a></p>

<p>From the tab, you can add additional host directories that would be needed to run the executable. The directory containing the executable is already mounted by default. Other options include: keeping the container after launch (by default it is removed), allowing stdin support (if console input is required), and running the executable in privileged mode (needed for certain operations such as using strace).</p>

<p>Each launch creates a new Docker Container. If you choose to keep the container after launch (useful if you want to simply run it again or view the console output at a later date), you can find all these containers in the Containers View using a label filter. By default, all C/C++ launches will add a label to the Docker Container configuration that can be filtered in the Containers View. To filter, use the menu pull-down and select Configure Labels Filter. From this dialog, add a CDTLaunch entry (no value is required). You can optionally add a CDTProject label if you want to only look for launches for a particular project.</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/configurefilterlabels.png" alt="Filter Label Configuration" /></p>

<p>Launching an executable will result in a console being created. Using Window &rarr; Preferences &rarr; Docker you can specify if a timestamp should be prefixed to all output or not. A timestamp is useful if you want to run multiple times and want to compare results.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/cdtdockerlaunchconsole.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cdtdockerlaunchconsole_sm.png" alt="C/C++ Docker Launch Console"/></a></p>

<h3>Debugging a C/C++ Executable in a Container</h3>

<p>Debugging an executable is similar to running one except that the selected image must have gdbserver pre-installed. Base images do not typically have gdbserver installed so the following method can be used to create such an image:</p>

<ul>
<li>From the Docker Images View select a desired base image (e.g. fedora:21).</li>
<li>Right-click on the selected image and choose "Run".</li>
<li>In the Run Image Wizard enter the /bin/sh command to run - be sure to select using a pseudo-TTY and stdin support from the additional options then hit "Finish".</li>
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/dockerrunshell.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/dockerrunshell_sm.png" alt="Run Shell"/></a></p>
<li>In the Terminal View, manually install the appropriate gdbserver package for the OS.</li>
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/installgdbserver.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/installgdbserver_sm.png" alt="Install GDB Server"/></a></p>
<li>Find the container you just created in the Docker Containers View.</li>
<li>Select the container, right-click and select "Commit".</li>
<li>Save the Container with gdbserver installed as whatever image name you like.</li>
<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/commitcontainer.png" alt="Commit Container" /></p>
</ul>

<p>Now you have an image that is able to run gdbserver to debug the executable. To debug a C/C++ executable in a container you should create a C/C++ Container Application launch configuration from the Debug Configurations dialog as opposed to using Debug as &rarr; C/C++ Container Application. This is needed the first time because you will need to specify the privileged option. As of Docker 1.10 or higher, gdbserver needs to run privileged to avoid an error due to security.  This problem will be fixed in Eclipse Oxygen whereby the gdbserver launch will internally set security options to bypass requiring user intervention.</p>

<p>Debugging works the same as debugging a remote executable, only the remote setup is done for you automatically and the source is already locally accessible.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/april/images/cdtdockerdebug.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cdtdockerdebug_sm.png" alt="Debugging a C/C++ Executable in Container"/></a></p>
	
<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/april/images/jeffheadshot.jpg"
        alt="Jeff Johnston" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
             Jeff Johnston, Principal Engineer<br />
            <a target="_blank" href="https://www.redhat.com/">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

