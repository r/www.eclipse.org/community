<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Eclipse CDT provides a feature-rich environment for editing C++ code. To do this, it needs to have a deep level of understanding of the code being edited.</p>

<h3>What supporting C++ involves</h3>

<p>To obtain such a deep level of understanding, CDT needs to perform many of the same stages of processing on the code that a compiler does:</p>

<ul>
<li>The code is first run through a <b>lexer</b> which converts the code into a token stream.</li>
<li>The lexed tokens are then <b>preprocessed</b>, which involves expanding macros and dropping sections disabled by conditional compilation.</li>
<li>The preprocessed tokens are <b>parsed</b> into an abstract syntax tree (AST).</li>
<li><b>Semantic analysis</b> is performed on the AST, which creates links between parts of the AST (for example, recognizing that a name in one part of the AST refers to an entity - like a variable or function - declared in another part). In C++, semantic analysis is a complex process that involves procedures like name lookup, overload resolution, and template instantiation.</li>
</ul>

<p>That's basically everything a compiler does short of optimization and code generation - and CDT can do all this.</p>

<p>In addition, unlike a compiler which compiles C++ code one translation unit at a time, CDT needs to support some features that require a <i>global</i> view of the entire codebase, such as answering the question "where are all the places this function is called in the codebase?" - and it needs to be able to answer such questions <i>efficiently</i>. To this end, CDT maintains an <b>index</b>, which can be thought of as a queryable database containing semantic information about the entire project.</p>

<p>CDT leverages the semantic model of the code that it builds to provide a wealth of editing features, such as showing errors and warnings as you type, semantic highlighting, navigation (such as Open Declaration), search, and code completion, among others.</p>

<p>Adding support for a C++ language feature can involve all of the above: adding support to the lexer, preprocessor, parser, and semantic analysis; storing new types of information in the index; and integrating the feature into our various editor capabilities (for example, a new language feature may require a new semantic highlighting, or a new type of element in the Outline View).</p>

<h3>Evolution of the C++ language</h3>

<p>The C++ language isn't static, such that you add support for it once and you're done with it - it is constantly evolving. Standardized by <a target="_blank" href="http://open-std.org/JTC1/SC22/WG21/">a committee of the International Standards Organization</a>, C++ had its initial language standard published in 1998, called C++98. A significant new version of it was published in 2011 (C++11), and since then the committee has been making 3-year releases of the standard: C++14, C++17, and so on.</p>

<p>Let's examine the state of CDT's support for these various language versions.</p>

<h4>C++98</h4>

<p>C++98 has been published for a while, and CDT has supported all of its major features for a while. Over the years, the quality of CDT's support has improved, with a long tail of edge cases being fixed over time, such that these days, the remaining bug reports CDT receives about C++98 features is about very obscure edge cases.</p>

<p>Here is a screenshot illustrating CDT performing overload resolution to provide accurate code completion:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cxx98_overload.png" alt="C++98" /></p>

<p>An example of a C++98 edge case that CDT still does not support, is the use of #include statements that are not at global scope; however, there is a <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=315964#c7">plan</a> for supporting that as well.</p>

<h4>C++11</h4>

<p>Adding C++11 support to CDT was a more recent endeavor. By this point, all major C++11 features are supported as well, though we will sometimes get bug reports about not-so-obscure edge cases.</p>

<p>Here's a screenshot of CDT using a C++11 library function, and using C++11's auto to deduce a type:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cxx11_auto.png" alt="C++11" /></p>

<h4>C++14</h4>

<p>C++14 is the most recent C++ language standard published to date. CDT's support for C++14 is a work in progress: two major features, <a target="_blank" href="http://en.cppreference.com/w/cpp/language/variable_template">variable templates</a> and C++14 extensions to constexpr, are already supported, while work on others, such as <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=408470">return type deduction</a> is ongoing.</p>

<p>Here's a screenshot of CDT performing C++14 constexpr evaluation. You can see from the hover that CDT has calculated that the result of the factorial(5) call is 120:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/april/images/cxx14_constexpr.png" alt="C++14" /></p>

<h3>Non-standard extensions</h3>

<p>In addition to standard language features, compilers sometimes support non-standard extensions. CDT doesn't (and can't possibly) support all such extensions for all compilers, but we do have some support. For example, since the majority of our users use the <a target="_blank" href="http://gcc.gnu.org/">GCC compiler</a>, we support a number of popular GNU extensions, including <a target="_blank" href="https://gcc.gnu.org/onlinedocs/gcc/Statement-Exprs.html">statement-expressions</a> and <a target="_blank" href="https://gcc.gnu.org/onlinedocs/gcc/Labels-as-Values.html">label reference expressions</a>.</p>

<p>With a large backlog of standard C++ features to implement, adding support for new non-standard extensions is not a priority, but we do welcome patches in cases where there is significant community interest in an extension.</p>

<h3>Encouraging experimentation</h3>

<p>In addition to the official releases of the C++ language standard like C++11 and C++14, the C++ standards committee has a mechanism for encouraging experimentation with language features that are still in development: <a target="_blank" href="http://en.cppreference.com/w/cpp/experimental">Technical Specifications</a>. These are specifications of new features that are not ready for final standardization yet but are published with the hope that implementers will implement them and users will use them, and provide feedback that may guide final standardization.</p>

<p>CDT aims to play its part in enabling such experimentation, by supporting features published as Technical Specifications. For example, CDT plans to support the <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=492682">Concepts Technical Specification</a> without waiting for the feature's official standardization.</p>

<h3>Leveraging the community</h3>

<p>The current level of C++ language support in CDT wouldn't be possible without the efforts of our community of contributors to complement the work of CDT's committers. Community contributions in the area of language support have ranged from bug fixes to entirely new features.</p>

<p>A notable community partner for CDT has been the <a target="_blank" href="https://ifs.hsr.ch/index.php?L=4">Institute for Software</a> (IFS), an academic group at the Swiss university <a target="_blank" href="https://www.hsr.ch/Home.home.0.html?&L=4">HSR</a>. Led by Professor Peter Sommerlad, the group uses CDT to teach programming and C++ to students, and they develop <a target="_blank" href="https://www.cevelop.com/">Cevelop</a>, a C++ IDE that extends CDT with a number of useful plugins. Among numerous other contributions, the implementations of the two main C++14 features CDT supports so far (variable templates and C++14 constexpr) started as term projects by students at IFS.</p>

<p>CDT wouldn't be what it is without our volunteer contributors. <a target="_blank" href="https://wiki.eclipse.org/CDT/contributing">New contributors</a> are always welcome!</p>

<h3>The future: C++17 and beyond</h3>

<p>The C++ language is evolving rapidly, with a new release, C++17, expected later this year, and more to follow every three years. Even with the help of the community, keeping up with the pace of development of new C++ language features is challenging for CDT.</p>

<p>As a result, there have been discussions about re-architecting CDT to leverage the internals of an existing compiler to understand C++ code, rather than rolling its own support. This wasn't done when CDT was initially designed because, at the time, open-source compilers weren't written with use by IDEs in mind. Since then, <a target="_blank" href="https://clang.llvm.org/">clang</a> has come along, a C++ compiler designed from the ground up for extensibility and use by tools.</p>

<p>Specifically, there is an <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/april/article5.php">exploratory effort</a> underway to make use of clang's internals via the <a target="_blank" href="https://github.com/Microsoft/language-server-protocol/">Language Server Protocol</a> and <a target="_blank" href="http://clang-developers.42468.n3.nabble.com/ClangD-tt4055124.html">ClangD</a>. While this would be a long-term project, once it's done CDT would effectively be able to gain support for new language features as soon as clang does - and clang has been pretty good at keeping pace with C++ standardization, with its latest trunk <a target="_blank" href="http://clang.llvm.org/cxx_status.html">already supporting</a> all of the features in the draft C++17 standard.</p>

<div class="bottomitem">
  <h3>About the Author</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        
        <div class="col-sm-16">
          <p class="author-name">
             Nathan Ridge
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/HighCommander4/">GitHub</a></li>
            <?php // echo $og; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
