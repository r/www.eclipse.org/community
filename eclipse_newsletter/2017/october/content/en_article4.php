<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
  <p><a target="_blank" href="https://marketplace.eclipse.org/content/ferret">Ferret</a> is a conceptually simple software exploration tool that seeks to summarize how a program element relates to the rest of the software system. Ferret was motivated by research into how and why programmers become <i>lost</i> or <i>disoriented</i> during software development activities. Two contributing factors include losing the context of how the programmer came to examine an element, and managing the mental workload required to combine the results of IDE searches to answer the questions that the programmer really wants to answer.</p>
  
<p>Ferret is implemented as a single view (below) that presents a densely-packed set of answers to dozens of questions about some element. On selecting some element in the UI (shown in label 1), Ferret starts digging. The questions (label 2) are grouped by type (label 3) as to whether they relate to questions about where or how an element is declared, to inheritance, or relationships between classes or within a class. Ferret includes the number of answers found (label 4); questions with no answers are hidden by default. Ferret supports drilling down into the answers by clustering along different attributes (label 4).In the example below, the instantiators of a type, which includes instantiators of subtypes, have been clustered by the subtype instantiated (label 6).</p>
  
  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/ferretlabels.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/ferretlabels_sm.png"/></a></p>
  	
  	<p>Ferret understands that software systems are more than just Java files, and supports bridging program elements across other sources of information such as type references in XML and plain-text manifests, method references in dynamic traces, revision history, and more. As these equivalences may not be 1–1 or onto, Ferret captures and propagates the <i>fidelity</i> of the equivalence (label 7).</p>

	<p>Results may be clustered by common attributes, such as by the defining package or project, or using the Eclipse package naming conventions to separate API types from internal types. Irrelevant questions or answers may be removed from the view.</p>

  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/ferret.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/ferret_sm.png"/></a></p>
  	
  	<p>As answers may spawn further questions, Ferret supports cascading queries in-place. In the following example, I wished to examine the instantiators of an EGitCredentialsProvider. I discovered that it was created in a method on an activator and wanted to confirm that the method was called as part of the bundle activation. Cascading queries can save having to jump between files, and preserves the context of the trail that I was investigating.</p>
  	
  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/ferretquery.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/ferretquery_sm.png"/></a></p>
  	
  	<p><u>One word of caution</u>: Ferret can bite off more than it can chew, such as might happen when set on java.lang.String or some other widely-used type. In such circumstances, simply closing the Ferret view should allow it to recover; you can also use the “Stop” button in the Progress view.</p>
  	
<p>Disorientation isn’t unique to software development, and understanding the causes and mitigating factors is essential to applications that help people make sense of complex domains. If you're coming to EclipseCon Europe, come to <a target="_blank" href="https://www.eclipsecon.org/europe2017/session/avoid-disorientation-ferret-powerful-search-tool-eclipse-ide">my talk</a> (Wednesday, October 25 at 11:15) where I will talk about Ferret and dive into disorientation and its causes. The Ferret <a target="_blank" href="https://github.com/briandealwis/ferret/blob/master/README.md">README</a> links to research papers describing the approach in more depth and with pointers to related research.</p>

<p>Ferret is open source and licensed under the EPL. The source is available on <a target="_blank" href="https://github.com/briandealwis/ferret">GitHub</a>, and prebuilt binaries can be installed via the <a target="_blank" href="https://marketplace.eclipse.org/content/ferret">Eclipse Marketplace</a>.</p>
  	
  	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2016/february/images/brian.png"
        alt="Brian de Alwis" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Brian de Alwis<br />
            <a target="_blank" href="http://manumitting.com/">Manumitting Technologies Inc</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/delaweeza?lang=en">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/briandealwis">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

