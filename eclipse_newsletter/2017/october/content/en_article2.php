<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
<p>Smart cities swarming with autonomous vehicles while intelligent robots work away at every corner. This is the direction technology is taking us. At the heart of these trends are the embedded systems: the hardware, the integrated circuits, the silicon chips. And the enabling technology for these embedded systems are of course the embedded tools, something we know a lot about in the Eclipse community.</p> 
<p>The Eclipse C/C++ development tools (CDT) is the industry standard C/C++ IDE powering the design and development of embedded systems worldwide. This year it celebrates its <a target="_blank" href="https://cdtdoug.ca/happy-15th-anniversary-cdt/">15th anniversary</a>. And in those 15 years, thanks to its highly customizable nature and industrial strength, Eclipse CDT has been adopted and customized by every silicon vendor and more out there. Eclipse CDT has been a dominant driver in the birth of highly-effective, open source embedded software tools that in turn have enabled both hardware and software innovations in this space. Here are just some of the CDT-based products out there being used by developers around the world to build powerful embedded systems.</p> 
  
  	<a target="_blank" href="/community/eclipse_newsletter/2017/october/images/embeddedtools.png"><p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/embeddedtools_sm.png"/></a></p>

<p>Building on Eclipse CDT’s success, we have seen more and more valuable embedded tools follow the same open-source, collaborative model. <a target="_blank" href="http://tracecompass.org/">Eclipse Trace Compass</a> provides tooling for interpreting the behaviour of complex systems by viewing or analysing any types of logs or traces.</p> 

  	<a target="_blank" href="/community/eclipse_newsletter/2017/october/images/tracecompass.png"><p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/tracecompass_sm.png"/></a></p>
  	
  	<p>The Eclipse Linux Tools project provides a collection of useful plug-ins for functionality such as profiling and code coverage. Eclipse Linux Tools is about developing Linux for any target, especially embedded ones, so don’t misunderstand the name - most of these plug-ins work on Windows too.</p> 
<p>There are lots of ongoing interesting developments in the CDT and related projects space. One of the most exciting new features is <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/april/article1.php">Docker tooling support</a>, including the upcoming support to <a target="_blank" href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=513589">build C/C++ projects in a Docker container</a>.</p> 
<p>In addition to the CDT ecosystem, there are a number of other embedded tools hosted in the Eclipse community. <a target="_blank" href="https://www.eclipse.org/app4mc/">Eclipse APP4MC</a> is a platform for engineering embedded multi- and many-core software systems. With the megatrend that is the internet-of-things, <a target="_blank" href="https://www.eclipse.org/vorto/">Eclipse Vorto</a> provides a way to create and manage technology agnostic, abstract device descriptions, so called information models.</p>
  	
  	
 <h2>Challenges for Embedded Tools</h2>
<p>Going forward embedded tools face theses three main challenges:</p>
<p><b>1. Tool Complexity</b> - As complexity of the underlying systems-on-chip grow, so do the requirements for the tools – there is a great deal of functionality to contend with for the tools of the future. Multicore debugging is just one example of very complex interactions that have to be presented to users in an easy to understand, fast and robust way.</p> 
<p><b>2. Sophisticated Users</b> – today’s users are increasingly sophisticated. They have specific notions of what tools should look like and how they should work. Often embedded tools suffer from usability issues as they have not spent enough effort focusing on the end-users needs and optimizing key task flows.</p>
<p><b>3. Integration Demands</b> – an extra burden is put on developers to get integration right. Today tools cannot exist in a vacuum and must play nicely with the existing systems out there, whether they are other developer tools or devops tools for build systems and revision control. Dealing with integration issues complicates the lives of tool developers as these requirements often arise further down the project life cycle and require a high level of adaptability.</p>

<h2>Future of Embedded Tools</h2>

<p>EclipseCon Europe provides the setting for developers to work together to overcome these challenges in practice. The <a target="_blank" href="https://www.eclipsecon.org/europe2017/embedded">Embedded Tools Summit</a> provides the unconference for sharing ideas, discussing complexity, user needs and integration demands, seeing demos of tools in action and thoughts on the future.</p>
 
  	
<p>At the main conference there are several talks following the embedded theme:</p>
<ol>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/docker-container-buildrundebug-support-cc-projects">Docker Container Build/Run/Debug Support for C/C++ Projects</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/eclipse-app4mc-embedded-multicore-optimization">Eclipse APP4MC - embedded multicore optimization</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/taming-complex-chip-designs-beautiful-diagrams">Taming complex chip designs with beautiful diagrams</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/update-remotely-embedded-devices-using-eclipse-hawkbit-and-swupdate">Update Remotely Embedded Devices using Eclipse hawkBit and SWUpdate.</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/cdt-where-are-we-where-do-we-go-here">CDT: Where are we? Where do we go from here?</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/make-your-ideas-fly-developing-software-quadcopters-eclipse">Make your ideas fly - Developing software for quadcopters with Eclipse</a></li>
        <li><a target="_blank" href="https://www.eclipsecon.org/europe2017/session/what-about-common-debug-protocol">What about a Common Debug Protocol?</a></li>
 </ol>	

<p>If you are able to, it is well worth it to join us as we handle the challenges of embedded tools and drive forward the next generation of embedded tools.</p>  
<p>Embedded tools are building systems for societal-scale applications, and as a result we need new ways to support this large-scale innovation. Open source is a proven model for innovating across established companies, institutions and start-ups. Open source provides a way to free developers form the day-to-day drudgery and help elevate them to the next level of problem solving. Eclipse CDT is one of the best examples of that and sets the tone for the next generation of innovative embedded system tools that will build our future worlds.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/october/images/tracy.jpg"
        alt="Tracy Miranda" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Tracy Miranda<br />
            <a target="_blank" href="https://kichwacoders.com/">Kichwa Coders</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/tracymiranda">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://kichwacoders.com/blog/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

