<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p><a target="_blank" href="http://junit.org/junit5/">JUnit 5</a> is out the door as the next generation test framework. It is a fundamentally redesigned version of the most widely used testing library in Java. JUnit 4.0 was first released over a decade ago after the introduction of annotations in Java 5. The world of Java and testing has evolved a lot since then. JUnit 4 was a big ball of mud with a single junit.jar to be used by test developers, testing framework developers, IDE developers, and build tool developers. Over the time, these developers have been accessing internals and duplicating code from JUnit 4 to get things done. This had made it very difficult to maintain and enhance the JUnit framework. So, to take advantage of the new features like lambda expressions from Java 8 and to support the advanced testing needs, JUnit 5 is now available as a modular and extensible test framework for the modern era.</p>
  
  
    <p align="center">	<a target="_blank" href="/community/eclipse_newsletter/2017/october/images/junituserguide.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/junituserguide_sm.png"/></a></p>
    	<p align="center"><small>Source: (JUnit 5 User Guide) <a target="_blank" href="http://junit.org/junit5/docs/current/user-guide/#dependency-diagram">http://junit.org/junit5/docs/current/user-guide/#dependency-diagram</a></small></p>
    	
<h2>How is JUnit 5 different?</h2>
<p>JUnit 5 is composed of several different modules from three different sub-projects.</p>

<p><b>JUnit 5 = JUnit Platform + JUnit Jupiter + JUnit Vintage</b></p>

<ul>
<li><b>JUnit Platform</b> defines Launcher APIs which are used by IDEs and build tools to launch the framework. It also defines TestEngine APIs which are used to develop testing frameworks that run on the platform.</li> 
<li><b>JUnit Jupiter</b> is the combination of the new programming model and extension model for writing tests and extensions in JUnit 5. It also provides a TestEngine for running Jupiter based tests on the platform.</li>
<li><b>JUnit Vintage</b> provides a TestEngine for running JUnit 3 and JUnit 4 based tests on the platform.</li>
</ul>

<h3>Give JUnit 5 a spin</h3>
<p>To give JUnit 5 a spin, you have the tooling support in the Eclipse IDE ready at your disposal. <a href="https://www.eclipse.org/downloads/eclipse-packages/">Download Eclipse Oxygen.1a (4.7.1a)</a> now and try it out yourself!</p>
<p>Here is a sneak peek into the major interesting features of JUnit Jupiter with Eclipse support for JUnit 5.</p>
    	
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/junitwordle.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/junitwordle_sm.png"/></a></p><br/>
    	
<h2>What can I do with JUnit 5?</h2>    	

<h3>Create a new JUnit Jupiter test</h3>

<p>Create a new JUnit Jupiter test via <b>New JUnit Test Case</b> wizard:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/junit_test.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/junit_test_sm.png"/></a></p><br/>

<p>On this page, you can specify the lifecycle method stubs to be generated for this test case. It also lets you select a class under test and on the next page, you can select the methods from this class to generate test stubs.</p>

<h3>Add JUnit 5 library to the build path</h3>

<ul>
<li>The new JUnit Test Case wizard offers to add it while creating a new JUnit Jupiter test:</li>
</ul>
  	
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/junittestcase.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/junittestcase_sm.png"/></a></p><br/>

<ul>
<li><b>Quick Fix (Ctrl+1)</b> proposal on <code>@Test</code>, <code>@TestFactory</code>, <code>@ParameterizedTest</code>, and <code>@RepeatedTest</code> annotations</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/jtestclass.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testclass_sm.png"/></a></p><br/>

<ul>
<li>Add JUnit library in <b>Java Build Path</b> dialog:</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/buildpath.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/buildpath_sm.png"/></a></p><br/>

<h3>JUnit Jupiter test case</h3>
<p>Here's a simple JUnit Jupiter test case.</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testcase_personjava.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testcase_personjava_sm.png"/></a></p><br/>

<p>In JUnit Jupiter, test classes and methods can have any access modifier (other than private).</p> 

<p>Here is a comparison of the annotations in JUnit 4 and JUnit Jupiter:</p>

 <table class="table table-striped">
          <tr>
            <th><p>JUnit 4</p></th>
            <th><p>JUnit Jupiter</p></th> 
          <tr>
            <td><p>@org.junit.Test</p></td>
            <td><p>@org.junit.jupiter.api.Test<br>
(<i>No expected</i> and <i>timeout</i> attributes)</p></td> 
          </tr>
           <tr>
            <td><p>@BeforeClass</p></td>
            <td><p>@BeforeAll</p></td> 
          </tr>
           <tr>
            <td><p>@AfterClass</p></td>
            <td><p>@AfterAll</p></td> 
          </tr>
           <tr>
            <td><p>@Before</p></td>
            <td><p>@BeforeEach</p></td> 
          </tr>
           <tr>
            <td><p>@After</p></td>
            <td><p>@AfterEach</p></td> 
          </tr>
           <tr>
            <td><p>@Ignore</p></td>
            <td><p>@Disabled</p></td> 
          </tr>
    </table>
    
<h3>JUnit Jupiter test method</h3> 
<p>Create a JUnit Jupiter test method in the Eclipse IDE with the new <b>test_jupiter</b> template:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/test_jupiter.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/test_jupiter_sm.png"/></a></p><br/>

<h3>Assertions and Assumptions</h3>
<p>JUnit Jupiter provides assertions and assumptions as static methods in</p> 
<p><code>org.junit.jupiter.api.Assertions</code> class and <code>org.junit.jupiter.api.Assumptions</code> class respectively.</p> 

<ul>
<li>You can view all the failures from grouped assertions in the <b>Result Comparison</b> dialog which can be opened from the Failure Trace section of JUnit view:</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/failuretrace.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/failuretrace_sm.png"/></a></p><br/>

<ul>
<li>You can view the number of tests with assumption failures on hover in JUnit view:</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/junitview.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/junitview_sm.png"/></a></p><br/>

<ul>
<li>JUnit Jupiter’s <code>Assertions</code>, <code>Assumptions</code>, <code>DynamicContainer</code> and <code>DynamicTest</code> classes are now added to <b>Eclipse Favorites</b> by default:</li>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/eclipsefavs.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/eclipsefavs_sm.png"/></a></p>
<br/>

<p>This allows you to quickly import static methods in your code from these favorite classes via <b>Content Assist (Ctrl + Space)</b> and <b>Quick Fix (Ctrl + 1)</b>.</p>

<p>You can also configure the number of static member imports needed in your code before type.* is used in the <b>Organize Imports</b> Preferences:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/organizeimports.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/organizeimports_sm.png"/></a></p><br/>
</ul>

<h3>Custom display names</h3>
<p>JUnit Jupiter’s <code>@DisplayName</code> annotation allows you to provide custom display names for test classes and test methods which can have spaces, special characters, and even emojis. In Eclipse, you can see the custom test names in JUnit view. You can use the <b>Go to File</b> action or you can just double-click on the custom test name to navigate to the corresponding test from JUnit view: </p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/displayname.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/displayname_sm.png"/></a></p><br/>

<h3>Nested test classes</h3>
<p>In JUnit Jupiter, non-static nested classes (i.e. inner classes) can serve as <code>@Nested</code> test classes for logical grouping of test cases. In Eclipse, you can (re-)run a single nested test class by using the <b>Run</b> action in JUnit view or Outline view. You can even right-click on a nested test class name in the editor and use the <b>Run As &#8594; JUnit Test</b> action:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/runtest.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/runtest_sm.png"/></a></p><br/>

<h3>Test interfaces and default methods</h3>
<p>JUnit Jupiter introduces the concept of a <b>test interface</b> which allows you to pre-implement some test code as default methods in interfaces. These <b>default test methods</b> can be inherited by implementing test classes, thereby enabling multiple inheritance in tests. In Eclipse, you can use the <b>Go to File</b> action or double-click on a test method in JUnit view to navigate to the corresponding default method inherited by the test class:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testmethod.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testmethod_sm.png"/></a></p><br/>

<h3>Tagging and filtering</h3>
<p>	You can tag test classes and test methods with your own identifiers using the <code>@Tag</code> annotation in JUnit Jupiter. These tags can later be used to filter test execution. In Eclipse, you can provide tags to be included in or excluded from a test run via the <b>Configure Tags</b> dialog in its JUnit launch configuration:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testtags.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testtags_sm.png"/></a></p><br/>

<h3>Meta-annotations and composed annotations</h3>
<p>Annotations in JUnit Jupiter can be used as <b>meta-annotations</b>. This allows you to create a custom <b>composed annotation</b> that will automatically inherit the semantics of its meta-annotations. For example, you can use <code>@Test</code> and <code>@Tag(“performance”)</code> annotations as meta-annotations and create a composed annotation named <code>@PerformaceTest</code> which can be used as a replacement of the other two annotations on tests:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/performance.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/performance_sm.png"/></a></p><br/>

<h3>Dynamic tests</h3>
<p>JUnit Jupiter introduces a concept of dynamic tests which are generated at runtime by a factory method annotated with <code>@TestFactory</code> annotation. The test factory method returns <code>DynamicNode</code> instances (<code>DynamicContainer</code> and <code>DynamicTest</code>). A <b>dynamic container</b> is composed of a display name and a list of dynamic nodes. A <b>dynamic test</b> is composed of a display name and an Executable. Note that there are no lifecycle callbacks for individual dynamic tests. Dynamic containers can be used for dynamic nesting of nodes:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/dynamicdemo.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/dynamicdemo_sm.png"/></a></p><br/>

<p>In your Eclipse workspace, you can create a test factory method with the new <b>test_factory</b> template:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testfactory.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testfactory_sm.png"/></a></p><br/>

<h3>Repeated tests</h3>

<p>You can repeat a test in JUnit Jupiter by annotating the test method with <code>@RepeatedTest</code> annotation and specifying the number of repetitions. You can optionally specify a custom display name for each repetition and use <code>RepetitionInfo</code> as the test method parameter to get information about the current repetition:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/repeatedtestresult.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/repeatedtest_sm.png"/></a></p><br/>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/repeatedtest.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/repeatedtestresult_sm.png"/></a></p><br/>

<h3>Parameterized tests</h3>
<p align="center"><p>You can run a test multiple times with different arguments by annotating the test method with <code>@ParameterizedTest</code> annotation and declaring at least one source to provide the arguments. You can optionally specify a custom display name for each run. JUnit Jupiter provides various source annotations which can be seen in this example:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/parametrizedtest.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/parametrizedtest_sm.png"/></a></p><br/>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/parametrizedtestresults.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/parametrizedtestresults_sm.png"/></a></p>

<h3>Dependency injection</h3>
<p><b>Dependency injection</b> in JUnit Jupiter allows test constructors and methods to have parameters. These parameters must be dynamically resolved at runtime by a registered parameter resolver. <code>ParameterResolver</code> is the extension API to provide parameter resolvers which can be registered via <code>@ExtendWith</code> annotation. This is where Jupiter’s programming model meets its extension model. JUnit Jupiter provides some built-in resolvers which are registered automatically:</p>

<ul>
<li><b>TestInfo</b> (resolved by <code>TestInfoParameterResolver</code>) to access information about the currently executing test:</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testinfo.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testinfo_sm.png"/></a></p><br/>

<ul>
<li><b>TestReporter</b> (resolved by <code>TestReporterParameterResolver</code>) to publish additional data about the current test run which can be seen in the <b>Console view</b> in the Eclipse IDE:</li>
</ul>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testreporter.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testreporter_sm.png"/></a></p><br/>

<p>In the Eclipse IDE, the <b>Test Method Selection</b> dialog in JUnit launch configuration now shows the method parameter types also:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/testinfodemo.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/testinfodemo_sm.png"/></a></p><br/>

<h3>JUnit Jupiter extension model</h3>
<p>JUnit Jupiter’s extension model provides various extension APIs as small interfaces in <code>org.junit.jupiter.api.extension</code> package which can be implemented by extension providers. You can register one or more of these extensions declaratively on a test class, test method, or composed annotation via <code>@ExtendWith</code> annotation. JUnit Jupiter also supports global extension registration via Java’s <code>ServiceLoader</code> mechanism:</p>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/extendwith.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/extendwith_sm.png"/></a></p>

<h2>Note</h2>
<ul>
<li>If you are using an Eclipse workspace where you were running your JUnit 5 tests via <code>@RunWith(JUnitPlatform.class)</code> in the Eclipse IDE without JUnit 5 support then you will have JUnit 4 as the test runner in their launch configurations. Before executing these tests in the Eclipse IDE with JUnit 5 support, you should either change their test runner to JUnit 5 or delete them so that new launch configurations are created with JUnit 5 test runner while running the tests:</li>

<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/note1.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/note1_sm.png"/></a></p><br/>

<li>We do not support running tests in a setup where an old Eclipse build (not having JUnit 5 support) is using a new Eclipse build (having JUnit 5 support) as target. Also, developers who have the JDT JUnit runtime bundles (<code>org.eclipse.jdt.junit.runtime</code>, <code>org.eclipse.jdt.junit4.runtime</code>) checked out and pull the latest changes will run into the above issue. You are expected to use a new Eclipse build for the development.</li>
</ul>

<h2>Resources &amp; References</h2>

<p>Eclipse Support for JUnit 5</p>
<ul>
	<li><a target="_blank" href="https://www.eclipse.org/downloads/eclipse-packages">Download Eclipse Oxygen.1a (4.7.1a)</a></li>
	<li><a target="_blank" href="http://download.eclipse.org/releases/oxygen/">Software site repository to update your Eclipse installation</a></li>
	<li><a target="_blank" href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=JDT&component=UI">Report bugs/enhancements in Eclipse support for JUnit 5</a></li>
</ul>

<p>Learn more about the <a target="_blank" href="http://junit.org/junit5">JUnit 5 Project</a></p>
<ul>
	<li><a target="_blank" href="http://junit.org/junit5/docs/current/user-guide">User Guide</a></li>
	<li><a target="_blank" href="http://junit.org/junit5/docs/current/api">Javadoc</a></li>
	<li><a target="_blank" href="https://github.com/junit-team/junit5">GitHub</a></li>
	<li><a target="_blank" href="https://gitter.im/junit-team/junit5">Gitter</a></li>
	<li><a target="_blank" href="http://stackoverflow.com/questions/tagged/junit5">Q&A</a></li>
</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/october/images/noopur.jpg"
        alt="Noopur Gupta" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Noopur Gupta<br />
            <a target="_blank" href="https://www.ibm.com/">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/noopur2507">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/noopur2507/">LinkedIn</a>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

