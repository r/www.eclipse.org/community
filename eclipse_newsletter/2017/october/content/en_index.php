<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2017.10.18</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxannenew.png" />
                            <h3>Editor's Note</h3>
                            <p><a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/europe2017/">EclipseCon Europe 2017</a> is upon us! It's our biggest event of the year and it starts next week, on October 24 in Germany. There's so much to learn at the conference. We asked some speakers to write about topics they will be presenting to give those of you who can't attend a taste of the content. <a style="color:#000;" target="_blank" href="https://www.eclipsecon.org/europe2017/program/sessions/accepted">View talk list</a>.</p>
                            <p>Read on to learn all you need to know about JUnit 5, e4 on JavaFX, scripting with EASE, embedded tools and Eclipse CDT, the Ferret search tool, and software ethics. We will be filming the EclipseCon Europe talks, so stay tuned for those.</p>
							<p>We launched our yearly donation campaign this month. <i>Great news, you can now donate via credit card without PayPal!</i> Power the Eclipse Community by <a style="color:#000;" target="_blank" href="https://www.eclipse.org/donate/">donating today</a>.</p>
							<p>The first issue of the quarterly Eclipse IoT Newsletter was published! <a style="color:#000;" target="_blank" href="http://mailchi.mp/dea07dc3737b/eclipse-iot-newsletter-october-2017">Read it now</a> and <a style="color:#000;" target="_blank" href="http://eepurl.com/cYn4-n">subscribe</a> to receive future IoT newsletters directly in your inbox.</p>
							<p>Did you know? JavaEE made the Eclipse Foundation it's new home! <a style="color:#000;" target="_blank" href="https://mmilinkov.wordpress.com/2017/09/12/java-ee-moves-to-the-eclipse-foundation/">Learn more</a>.</p>             				
             				<p>Tweet us #EclipseCon you'll be at EclipseCon Europe next week!<br>
							Roxanne Joncas<br>
							<a target="_blank" style="color:#000;" href="https://twitter.com/roxannejoncas">@roxannejoncas</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article5.php"><h3>Embracing JUnit 5 with Eclipse</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>JUnit 5 is out the door as the next generation test framework! Here are some tips and examples to help you work with it.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article1.php"><h3>e4 on JavaFX</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Develop applications and write 100% framework free business components with e4 on JavaFX.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article3.php"><h3>Scripting Eclipse – just rapid prototyping?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Scripting is powerful. Learn more about scripting itself and about the Eclipse Advanced Scripting Environment (EASE).</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article2.php"><h3>15 Years of Open Source Embedded Tools at Eclipse</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse CDT is the industry standard C/C++ IDE powering the design and development of embedded systems worldwide.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article4.php"><h3>Avoid disorientation with Ferret, a powerful search tool</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Discover Ferret, a conceptually simple software exploration tool that seeks to summarize how a program element relates to the rest of the software system.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article6.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/october/article6.php"><h3>Why Software Ethics Matter</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Software is impacting every area of our lives and will be even more omnipresent in the future. How can we be more ethical about it all?</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a href="http://eclipse.org/donate"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/donate.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
      
          
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://www.youtube.com/eclipsefdn"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/videoofthemonth.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
					      <tr>
						    <td valign="top" class="textContent">
                            <p>Each month, we will post the most popular YouTube video from the <a target="_blank" style="color:#000;" href="https://www.youtube.com/eclipsefdn">Eclipse Foundation YouTube Channel</a> here.</p>
                          </td>
                         </tr>
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://youtu.be/tzJODTjYuWI"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/openhabvid.jpg"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      <tr>
                           <td valign="top" class="textContent">
                             <p><a target="_blank" style="color:#000;" href="https://www.youtube.com/eclipsefdn">Subscribe</a> to our channel and <a target="_blank" style="color:#000;" href="mailto:youtube@eclipse.org?subject=YouTube%20Video%20Suggestion">share</a> videos about Eclipse technology with us.</p>
                             </td>

                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
   <!-- // MODULE ROW -->
      
    
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Java IDE Tips &amp; Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">

                            <p>Here is this month's tip in action &#10145;<br>
                            <p style="color:blue">How to see the difference between expected and actual results in grouped assertions in JUnit 5.</p> 
                            <p><i><small>Click on the gif to view the original tweet and gif.</small></i></p>
                            
                            <p>Want more tips? Follow <a target="_blank" style="color:#000;" href="https://twitter.com/eclipsejavaide">@EclipseJavaIDE</a> on Twitter.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://twitter.com/EclipseJavaIDE/status/919915440041840641"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/junit5.gif"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://mmilinkov.wordpress.com/2017/09/30/on-naming/">On Naming Eclipse Enterprise for Java (EE4J)</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://ianskerrett.wordpress.com/2017/09/29/annual-donation-campaign-end-user-support-for-the-eclipse-foundation/">Annual Donation Campaign: End User Support for the Eclipse Foundation</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://eclipse.org/org/press-release/20170928_open_iot_challenge_4.php">Eclipse IoT Announces Fourth Edition of the Open IoT Challenge</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://waynebeaton.wordpress.com/2017/09/22/java-9-support-for-eclipse-ide-oxygen-edition/">Eclipse IDE Now Supports Java 9</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/20170925criticalbug.php">Special Notice for Eclipse IDE Users on macOS 10.13 in non-English mode</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="http://toem.de/index.php/blog/166-toem-joins-the-eclipse-foundation">toem joins the Eclipse Foundation</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/articles/eclipse-collections">The Java Evolution of Eclipse Collections</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2017/10/EE4JNeedsU">Eclipse Issue Open Call for Enterprise Java Participation</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/java-ee-8-interview-josh-juneau-137847.html">“We should use Java EE 8 in present tense and EE4J when speaking of the next release”</a></li>
								<li><a target="_blank" style="color:#000;" href="https://skymind.ai/press/eclipse">Skymind joins Eclipse Foundation, contributes Deeplearning4j suite</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.theregister.co.uk/2017/10/03/java_ee_goes_eclipse_now_what/">Introducing EE4J – Java EE's fling with the Eclipse Foundation</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoworld.com/article/3230364/java/under-eclipse-java-ee-compliance-testing-goes-open-source.html">Under Eclipse, changes to Java EE begin</a></li>
								<li><a target="_blank" style="color:#000;" href="https://ianskerrett.wordpress.com/2017/10/02/5-trends-to-watch-in-the-java-ecosystem/">5 Trends to Watch in the Java Ecosystem</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/ee4j-java-ee-eclipse-foundation-137668.html">Introducing EE4J: Java EE takes first steps into Eclipse Foundation</a></li>
								<li><a target="_blank" style="color:#000;" href="http://www.iotevolutionworld.com/fog/articles/434570-cloudera-joins-eclipse-foundation-open-source-iot-community.htm">Cloudera Joins Eclipse Foundation Open Source IoT Community</a></li>
							 </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j/charter">Eclipse EE4J</a>: Eclipse Enterprise for Java (EE4J) is an open source initiative to create standard APIs, implementations of those APIs, and technology compatibility kits for Java runtimes that enable development, deployment, and management of server-side and cloud-native applications.  EE4J is based on the Java™ Platform, Enterprise Edition (Java EE) standards, and uses Java EE 8 as the baseline for creating new standards.</li>
                            	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/deeplearning4j">Eclipse Deeplearning4j</a>: enables developers and large organizations to build deep learning applications, covering the whole deep learning workflow from data preprocessing through distributed training and hyperparameter optimization and production-grade deployment.</li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
                             	<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/eclipse/news/4.7.1a/">Eclipse Project 4.7.1a</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jgit/releases/4.9.0">Eclipse JGit: Java implementation of Git 4.9</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egit/releases/4.9.0">Eclipse EGit: Git Integration for Eclipse 4.9</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.ease/releases/0.5.0/bugs">Eclipse Advanced Scripting Environment (EASE) 0.5</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://medium.com/@donraab/nine-features-in-eclipse-collections-9-0-a2ca97dfdf74">Eclipse Collections 9.0</a></li>
                             	<li><a target="_blank" style="color:#000;" href="http://planetorion.org/news/2017/10/announcing-orion-16/">Eclipse Orion 16.0</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/PDT/NewIn51">Eclipse PHP Development Tools 5.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/science.january/releases/2.1.0">Eclipse January 2.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.tracecompass/releases/3.1.0/bugs">Eclipse Trace Compass 3.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/science.triquetrum/releases/0.2.0">Eclipse Triquetrum 0.2</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.kapua/releases/0.3.0">Eclipse Kapua 0.3</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.kura/releases/3.1.0">Eclipse Kura 3.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.om2m/releases/1.1.0">Eclipse OM2M 1.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4j/releases/0.3.0/bugs">Eclipse LSP4J 0.3</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4e/releases/0.3.0/bugs">Eclipse LSP4E 0.3</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://bugs.eclipse.org/bugs/buglist.cgi?list_id=16708633&product=ECP&query_format=advanced&target_milestone=1.14.0">Eclipse EMF Client Platform 1.14</a></li>
								<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.elk/releases/0.3.0">Eclipse Layout Kernel 0.3</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/rt.rap/releases/3.3.0">Eclipse Remote Application Platform (RAP) 3.3</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/science.eavp/releases/0.2">Eclipse Advanced Visualization Project 0.2</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.sirius/releases/5.1.0">Eclipse Sirius	5.1</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/rt.vertx/releases/3.5.0">Eclipse Vert.x 3.5</a></li>
							 	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.tmf.xtext/releases/2.13.0">Eclipse Xtext 2.13</a></li>
                            	</ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2017/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/oct_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2017/">EclipseCon Europe 2017</a><br>
                            Oct 24-26, 2017 | Ludwigsburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://www.siriuscon.org/">SiriusCon 2017</a><br>
                            Nov 9, 2017 | Paris, France</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
