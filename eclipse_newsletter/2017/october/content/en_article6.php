<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
<p>Software is impacting every area of our lives, and will be even more omnipresent in the future. We have seen on many occasions during the last few years how a small glitch in software can have unprecedented consequences, from data leaks to people being harassed, harmed or killed.</p>

<p>As software practitioners and as members of a community, we have a responsibility towards our users, our fellow developers, humankind and the world.</p>

<p>In this article we will take the opportunity to consider the ethical implications of software development and production, why it is important, and how we can do it better for the world and for ourselves while still being pragmatic.</p>

<h2>Why do ethics matter?</h2>

<h3>About ethics</h3>

<p>Ethics is a <a target="_blank" href="https://en.wikipedia.org/wiki/Ethics">major branch of philosophy</a>. In simple terms, it is the study of values and customs of a person or a group. In even simpler terms: the philosophy on how to act. It usually implies concepts of right and wrong, and is all about the consequences of what we do, and how we do it. Examples of ethical paradigms are consequentialism (judge on the consequences whatever the means), deontology (judge the means, whatever the consequences), or virtue (if you're a good person you will do good).</p>

<p>Likewise, politics is the study of the relative distribution of power, authority and privileges in human societies. Software has introduced a major shift in the economics, social, information and political fields, and the choices we make in the design, implementation and dissemination of software products has a direct impact on them. <b>Developing software is a political act</b>.</p>

<h3>Software is eating the world</h3>

<p>We use software every day. States and big companies have digitalised a great part of our data: our habits, through phones, computers, purchases, network connections and bank transactions, are known and stored somewhere.</p>

<p>Software enables us to fly, and can on occasions cause <a target="_blank" href="https://www.theguardian.com/technology/2015/may/20/airbus-issues-alert-software-bug-fatal-plane-crash">planes to crash</a>. Software makes us live longer and better lives, and sometimes <a target="_blank" href="https://en.wikipedia.org/wiki/Therac-25">kills us whilst trying to do so</a>. Software enables us to hear and see the whole world, but is sometimes used to deceive us. And if war comes upon us, then we'll probably be killed by some software-engineered weapons.</p>

<h2>Community Ethics</h2>

<h3>Ethics and code of conduct: state of practice</h3>

<p><a target="_blank" href="https://ethical.software/Communities/examples.html">Most organisations</a> now have a <a target="_blank" href="https://en.wikipedia.org/wiki/Ethical_code">code of conduct and/or a code of ethics</a>. Code of ethics is for business and decision-making, code of conduct is for actions and behaviour. Big companies even have a variety of them for their contractors, colleagues, users, and providers.</p>

<p>Still, most companies involved in scandals during last years had a code of ethics. Experience demonstrates that the presence of an ethics code is just not enough, it needs to be enforced. It is however a good thing to have because it sets a standard, and it lays down healthy foundations for further reflection and action.</p>

<p>All major forges and open-source communities (e.g. <a target="_blank" href="https://www.apache.org/foundation/policies/conduct.html">Apache Foundation</a>, <a target="_blank" href="https://github.com/blog/2039-adopting-the-open-code-of-conduct">GitHub</a>, <a target="_blank" href="https://eclipse.org/org/documents/Community_Code_of_Conduct.php">Eclipse Foundation</a>, <a target="_blank" href="https://en.wikipedia.org/wiki/Debian_Social_Contract">Debian</a>) have a code of conduct and enforce it. There is a common concern about understanding and application of an ethics code and in most cases of breach there are prompt reactions.</p>

<h3>Ethics at the Eclipse Foundation</h3>

<p>The Eclipse Foundation has a <a target="_blank" href="http://www.eclipse.org/org/foundation/">transparent structure</a>, with a <a target="_blank" href="https://fosdem.org/2017/schedule/event/corporate_shenanigans/">specific concern on its independence</a>. The board, staff, EMO's, and project leaders listen and interact frequently, e.g. on the mailing lists. IP policy is not only for lawers, it's also about fairness. The Eclipse Foundation has a <a target="_blank" href="https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf">very strict policy</a> regarding IP and licencing.</p>

<p>There is a <a target="_blank" href="http://www.eclipse.org/org/documents/Community_Code_of_Conduct.php">code of conduct</a> and numerous initiatives [<a target="_blank" href="https://theaaaaaa.wordpress.com/2017/01/24/diversity-at-eclipse/">1</a>, <a target="_blank" href="https://www.eclipsecon.org/europe2016/sites/default/files/slides/ECE_diversity7habits_0.pdf">2</a>, <a target="_blank" href="https://www.eclipsecon.org/europe2016/sites/default/files/slides/Ethics_in_SW_Dev.pdf">3</a>] on diversity in workshops, eclipse days and conferences, and a dedicated channel (#diversity) on the <a target="_blank" href="https://mattermost.eclipse.org/">foundation's Mattermost server</a>.</p>

<h3>The flaws of meritocracy</h3>

<p>Meritocracy is a beloved concept in open-source communities. But although it seems to be remarkably efficient (see the Debian project, and generally speaking all major free software organisations), it is not necessarily ethical. Many voices [ <a target="_blank" href="http://geekfeminism.wikia.com/wiki/Meritocracy">1</a>, <a target="_blank" href="https://www.theatlantic.com/business/archive/2015/12/meritocracy/418074/">2</a> ], and studies have shown that organizational cultures that value meritocracy <a target="_blank" href="http://asq.sagepub.com/content/55/4/543.short">often result in greater inequality</a>. People with "merit" are also often <a target="_blank" href="https://modelviewculture.com/pieces/the-dehumanizing-myth-of-the-meritocracy">excused for their bad behavior</a> in public spaces based on the value of their technical contributions.</p>

<h2>Personal ethics</h2>

<h3>What we do</h3>

<p>All communities start from a group of individuals: that is us. As software practitioners we face ethical dilemma and questionnable situations. We already did, and we probably will again.

<p>A good step towards an increased consciousness is to do our <a target="_blank" href="https://medium.freecodecamp.org/the-code-im-still-ashamed-of-e4c021dff55e">own mea culpa</a>, in order to review our behaviour and actions and check that they are aligned with our own beliefs.

<p>What can we do to prepare ourselves to make the right decisions when it happens? Get to know where our personal limits are, question and understand the consequences of decisions and actions, know the <a target="_blank" href="https://ethical.software/">existing initiatives</a>, and exercise our judgement.

<h3>Building ethical software</h3>

<p>How do we, as committers of a project and actors of its community, make our product more ethical? There are a couple of easy steps:

<ul>
    <li>Adopting a code of conduct: there are ready-to-use codes available, like the <a target="_blank" href="https://www.contributor-covenant.org/">Contributor Covenant</a>. GitHub also proposes a <a target="_blank" href="https://help.github.com/articles/adding-a-code-of-conduct-to-your-project/">default template</a> when creating a new project.</li>
    <li>Enforcing the code of conduct, and providing easy means for people to raise ethical issues.</li>
    <li>Setting up a transparent and publicly available governance model for the project.</li>
    <li>Nurishing quality- and ethical- initiatives within the projects, whatever they may be.</li>
</ul>

    <p>Other more time-consuming aspects may include software quality attributes, like reliability, maintainability or usability, community-related commitment to diversity and support people, user data protection.</p>

<h2>Use Cases</h2>

<h3>The Diesel gate</h3>

<p>A <a target="_blank" href="https://en.wikipedia.org/wiki/Diesel_emissions_scandal">big institutionalised lie</a> set up to bypass state environmental regulation, and widespread across the whole automotive industry. The European authorities knew it, the states knew it, the companies knew it. But as Segolene Royal (the former french head of the ministry for the environment) said during an <a target="_blank" href="https://www.youtube.com/watch?v=ZiIoRq3e9L8">official commission</a>, "we cannot impede our national companies".</p>

<p>Meanwhile, outdoor air pollution kills <a target="_blank" href="http://www.who.int/mediacentre/factsheets/fs313/en/">3M people per year worldwide</a>.</p>

<h3>Whistleblowers</h3>

<p>It’s difficult to get a man to understand something when his salary depends on him not understanding it. [<a target="_blank" href="https://en.wikipedia.org/wiki/Upton_Sinclair">Upton Sinclair</a>]</p>

<p>Whistleblowers make a choice to make public interest disclosures about illegal activity or activity in violation of company policy against an employer. It requires courage and preparation, and most of the time leads to losing one's job. Regarding the recent events [<a target="_blank" href="https://www.theguardian.com/environment/2017/sep/15/eu-report-on-weedkiller-safety-copied-text-from-monsanto-study">1</a>, <a target="_blank" href="https://www.theguardian.com/news/series/panama-papers">2</a>, <a target="_blank" href="https://en.wikipedia.org/wiki/Luxembourg_Leaks">3</a>] whistleblowers, more than anyone else, have set the battle against corruption and environmental and health diseases.</p>

<p>There is a growing interest in whistleblowers and public disclosure -- as a sidenote, some job interviews now include questions related to whistleblowing, and they expect the candidate to do so (observed in the UK).</p>

<h2>Conclusion</h2>

<p>Most of what we call ethics is unwritten. Instead, like many cultural rules, they're mostly implicit. Not everything is black or white: between things we know we would never do and things that we consider ok, there is a large grey zone. And that's where issues happen. The devil lies in this grey zone, and reducing its size is our best chance to take the right action next time a dilemma happens.</p>

<p>Everything will start from discussions, common interest and debate. What do you think will be the next step? Should we write a manifesto for our corporation, like the Hypocrate's oath for medecine? Should projects be assessed regarding their ethics and a suitable label attached, like sustainable trade does? <b>What should we do &mdash; what will you do? We need to act now.</b></p>

<p>When no single body is at fault it is clearly everybody's duty. Let's state it plainly: <b>We do care</b>.</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/october/images/boris.jpg"
        alt="Boris Baldassari" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Boris Baldassari<br />
            <a target="_blank" href="http://castalia.solutions/">Castalia Solutions</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/borisbaldassari">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/borisbaldassari/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

