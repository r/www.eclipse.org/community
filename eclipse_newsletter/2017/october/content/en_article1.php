<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <h2>What is it?</h2>
<p>Many people are familiar with Eclipse RCP 3.x and most of them are also familiar with Eclipse 4 RCP, sometimes also called Eclipse 4 Application Platform (EAP), that brought modern development concepts to Rich Client developers:</p>

<ul>
<li>A model representing the application state comparable to the browser DOM</li>
<li>DependencyInjection (DI) or Inversion of Control (IoC) as the new programming model to implement components that are not coupled to the application container they are running in</li>
</ul>

<p>A less known fact is that when we designed the EAP, we took extra care to lead by example and not couple our new framework with specific UI technologies (ie SWT). Instead, we introduced the concept of a rendering engine that allows other projects/vendors/companies to provide implementations for other UI toolkits.</p>

<p>There have been people implementing a renderer for Vaadin, while others have taken e4 to swing, but none of them reached the maturity required to be used by enterprises class applications - except one: the JavaFX renderer developed as part of the <a target="_blank" href="http://efxclipse.org/">e(fx)clipse project</a>.</p>

<p>Today there are many commerical applications built on top of it from applications in the financial industry to medical applications. Another area JavaFX and hence <b>e4 on JavaFX</b> has gained traction, is the public sector.</p>

There are multiple reasons to favor <b>e4 on JavaFX</b> over e4 on SWT, but the most important one is that one can theme/design JavaFX applications in a pixel perfect way in much less time compared to SWT and even Swing. With UX getting more and more attention even used "only" in the back office, JavaFX is the natural choice for most applications that decided to stay on the desktop as a rich client.

In general <b>e4 on JavaFX</b> can be used to build any application, but in reality it is primarily suited for medium sized to large projects with a lot of views, screens, …​ The framework is designed to help with saving and restoring state, provides a command and handler story and a sane service story that is fully integrated in the the DI programming model that fosters a state of the art application and component design.

<h2>What’s the difference?</h2>
<p>The basic EAP already provides many concepts required to develop applications with minimal dependencies on the application framework, but e4 on JavaFX closes even those minimal gaps, allowing you to write 100% framework free business components.</p>

<p>The following examples highlight what that means:</p>

<p>If you want to publish information into EAP so that others can retrieve this information later by just writing @Inject in their components, you are forced to code something like this:</p>
  
<pre style="background:#fff;color:#000">@<span style="color:#ff7800">Inject</span> <span style="color:#ff7800">IEclipseContext</span> context;

<span style="color:#ff7800">void</span> publish(<span style="color:#ff7800">Person</span> p) {
    context<span style="color:#ff7800">.</span>modify( <span style="color:#409b1c">"selectedPerson"</span>, p );
}
</pre>

<p>This introduces a compile-time dependency to the EAP-Framework through the use of IEclipseContext. In contrast to that, e4 on JavaFX enhances the DI container, allowing you to write the following:</p>

<pre style="background:#fff;color:#000">@<span style="color:#ff7800">Inject</span> @ContextValue(<span style="color:#409b1c">"selectedPerson"</span>)
<span style="color:#ff7800">Consumer&lt;<span style="color:#ff7800">Person</span>></span> publisher;

<span style="color:#ff7800">void</span> publish(<span style="color:#ff7800">Person</span> p) {
    publisher<span style="color:#ff7800">.</span>accept( p );
}
</pre>

<h2>What can you build?</h2>
<p>It’s up to your imagination, but to show you that it is not only business UIs that can be built on top of it, we have started working on a alternate showcase IDE, built on top of Eclipse JDT:</p>

	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/screen_orig_light.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/screen_orig_light_sm.png"/></a></p>
  
  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/screen_orig_dark.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/screen_orig_dark_sm.png"/></a></p>
  
  <p>Having an IDE with a dark theme is not only a really cool and fancy new feature, the interesting part is how long it takes to create such a theme. It turns out that the following ~50 lines of CSS are sufficient. The most important ones are:</p>

<pre style="background:#fff;color:#000">.root {
    <span style="color:#ff7800">-</span>fx<span style="color:#ff7800">-</span>base<span style="color:#ff7800">:</span> #4d5052;
    <span style="color:#ff7800">-</span>fx<span style="color:#ff7800">-</span>background<span style="color:#ff7800">:</span> #4d5052;
    <span style="color:#ff7800">-</span>fx<span style="color:#ff7800">-</span>control<span style="color:#ff7800">-</span>inner<span style="color:#ff7800">-</span>background<span style="color:#ff7800">:</span> #4d5052;
    <span style="color:#ff7800">-</span>fx<span style="color:#ff7800">-</span>text<span style="color:#ff7800">-</span>base<span style="color:#ff7800">-</span>color<span style="color:#ff7800">:</span> #c7c7c7;

    <span style="color:#ff7800">-</span>content<span style="color:#ff7800">-</span>assist<span style="color:#ff7800">-</span>extra<span style="color:#ff7800">-</span><span style="color:#3b5bb5">1</span><span style="color:#ff7800">-</span>color<span style="color:#ff7800">:</span> rgb(<span style="color:#3b5bb5">150</span>,<span style="color:#3b5bb5">150</span>,<span style="color:#3b5bb5">150</span>);
    <span style="color:#ff7800">-</span>content<span style="color:#ff7800">-</span>assist<span style="color:#ff7800">-</span>extra<span style="color:#ff7800">-</span><span style="color:#3b5bb5">1</span><span style="color:#ff7800">-</span>color<span style="color:#ff7800">-</span>hover<span style="color:#ff7800">:</span> rgb(<span style="color:#3b5bb5">80</span>,<span style="color:#3b5bb5">80</span>,<span style="color:#3b5bb5">80</span>);
}

<span style="color:#8c868f">/* ... stripped some lines ... */</span>

.styled<span style="color:#ff7800">-</span>text<span style="color:#ff7800">-</span>area .list<span style="color:#ff7800">-</span>view {
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>code<span style="color:#ff7800">:</span> #b8c4d1;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>operator<span style="color:#ff7800">:</span> #b8c4d1;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>bracket<span style="color:#ff7800">:</span> #b8c4d1;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>keyword<span style="color:#ff7800">:</span> #d78b40;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>string<span style="color:#ff7800">:</span>  #7c986c;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>number<span style="color:#ff7800">:</span>  #b6c8ad;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>doc<span style="color:#ff7800">:</span> #<span style="color:#3b5bb5">929292</span>;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>api<span style="color:#ff7800">-</span>doc<span style="color:#ff7800">:</span> #74a567;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>buitin<span style="color:#ff7800">-</span>type<span style="color:#ff7800">:</span> rgb(<span style="color:#3b5bb5">255</span>,<span style="color:#3b5bb5">235</span>,<span style="color:#3b5bb5">121</span>);
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>annotation<span style="color:#ff7800">:</span> rgb(<span style="color:#3b5bb5">200</span>, <span style="color:#3b5bb5">200</span>, <span style="color:#3b5bb5">200</span>);

    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>markup<span style="color:#ff7800">-</span>doc<span style="color:#ff7800">:</span> #<span style="color:#3b5bb5">929292</span>;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>markup<span style="color:#ff7800">-</span>property<span style="color:#ff7800">-</span>name<span style="color:#ff7800">:</span> #b8c4d1;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>markup<span style="color:#ff7800">-</span>property<span style="color:#ff7800">-</span>value<span style="color:#ff7800">:</span> #7c986c;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>markup<span style="color:#ff7800">-</span>tag<span style="color:#ff7800">:</span> #e9c063;
    <span style="color:#ff7800">-</span>source<span style="color:#ff7800">-</span>editor<span style="color:#ff7800">-</span>markup<span style="color:#ff7800">-</span>extra<span style="color:#ff7800">:</span> rgb(<span style="color:#3b5bb5">190</span>, <span style="color:#3b5bb5">190</span>, <span style="color:#3b5bb5">190</span>);
}

.content<span style="color:#ff7800">-</span>proposal<span style="color:#ff7800">-</span>doc {
    <span style="color:#ff7800">-</span>fx<span style="color:#ff7800">-</span>background<span style="color:#ff7800">-</span>color<span style="color:#ff7800">:</span> #4d5052;
}
</pre>

<h2>What’s next?</h2>
<p>As outlined above, <b>e4 on JavaFX</b> is enterprise ready and we are actively maintaing it, add new features and APIs and fix bugs in both our own and the upstream code base.</p>

<p>One of the bigger pieces we have been working on for a while, is to allow developers to choose their favorite IDE when working with <b>e4 on JavaFX</b> applications.</p>

<p>At JavaOne 2017 we showcased the current state of Netbeans and IntelliJ IDEA support:</p>


  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/e4intellijidea.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/e4intellijidea_sm.png" alt="e4 intellij"/></a></p>

  	<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/october/images/e4netbeans.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/e4netbeans_sm.png" alt="e4 netbeans"/></a></p>

<p>A key point to support other IDEs is that we had to implement a completely different development model compared to the MANIFEST-first approach taken by PDE and maven-tycho.</p>

<p>In order to achieve this, we have switched to a maven-first approach by leveraging the fairly new bnd-maven-plugin that is part of the bnd-ToolSuite.</p>

<p>We are still supporting MANIFEST-first development styles, but we encourage our user base to favor the maven-first style, because it works in any modern IDE (Eclipse, Netbeans, IntelliJ, VS-Code) and even makes your application a better OSGi citizen.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/october/images/tom.jpg"
        alt="Tom Schindl" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Tom Schindl<br />
            <a target="_blank" href="http://bestsolution.at/">BestSolution</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/tomsontom">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://tomsondev.bestsolution.at">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

