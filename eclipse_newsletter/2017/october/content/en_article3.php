<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
	<p>When I started my work on scripting support for the Eclipse platform I had three major goals in mind:</p>
	<ul>
        <li>First inexperienced programmers should get a simple language not forcing them to deal with complex data types or object orientation.</li> 
        <li>On the other hand the language should allow for complex structures and object-oriented design if users do want this.</li> 
        <li>Finally, interaction with the running IDE should be possible.</li>
	</ul>
	
<p>EclipseScript already used Rhino as an engine and added a static Document Object Model (DOM) to it which allowed interaction with some workbench elements. Eclipse Monkey took the concept a step further and allowed to extend the DOM via extension points. Having a DOM that can be extended during runtime was just the logical next step. After having a first prototype ready the <a target="_blank" href="http://eclipse.org/ease">Eclipse Advanced Scripting Environment (EASE)</a> was born.</p>

<p>To get started simply install a script engine from our <a target="_blank" href="https://www.eclipse.org/ease/download/">p2 site</a>. Switch to the Scripting Perspective and type print(„Hello world“) in the <i>Rhino Script Shell</i> view.</p> 

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/rhinoshell.png"/></p>
	
<p>The Rhino Shell accepts JavaScript code, allows to access JDK classes as well as the whole Eclipse API of your current installation. In case you want more convenience, have a look at the Modules Explorer View to the right and browse through the available functions. To try them out simply drop a command to the shell. While EASE ships with some predefined modules we encourage you to <a target="_blank" href="https://wiki.eclipse.org/EASE/Module_Contribution">write your own</a> (no knowledge of EASE required).</p>	
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/moduleexplorer.png"/></p>
	
<p>Interacting with given Eclipse APIs allows to explore their functionality in an easy way. Simply use  a script shell and call any method you are interested in. Oh, did I mention that you have code completion support even for Java classes?</p>

<p>More script demos and tutorials are available on our <a target="_blank" href="http://git.eclipse.org/c/ease/org.eclipse.ease.scripts.git/tree">scripting repository</a>.</p>

<h2>Automation and UI Integration</h2>
<p>Scripts provide a convenient way of automating recurring tasks. I do use scripts to generate new workspace projects based on given templates, update plugin version numbers or to run certain external builds. Once your script library grows you need to organize them in a library. EASE preferences allow to provide library locations and accumulate existing scripts into a nice view. But wouldn't it be much nicer to integrate your scripts directly into the IDE?</p>

<p>EASE scripts do understand magic keywords in <a target="_blank" href="https://wiki.eclipse.org/EASE/Scripts">header comments</a>. Some of them allow to attach a script to toolbars, view menus, and popups. But keywords can do even more. Scripts may trigger on startup or shutdown, they may react on resource changes or even on events to the event bus. Do you have ideas for other keywords? EASE allows to define your own...<p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/automationease.png"/></p>
	
	<h2>Unit Testing</h2>
	
	<p>Now we got this nice script language, so why not use it for writing simple, expressive regression tests? <a target="_blank" href="https://en.wikipedia.org/wiki/Behavior_Driven_Development">Behavior driven testing</a> is an exciting topic nowadays. EASE does not support Gherkin style feature descriptions (yet), but with the help of modules your users are able to write unit tests without having lots of programming background. Further such tests will be simple to read and understand. A first test may look as simple as that:</p>

<pre>startTest(„sum 2 values“);
assertEquals(5, thisIsYourSumFunction(2, 3));
endTest();
</pre>

<p>Given the right set of custom modules you may provide your own DSL for test developers. Then write expressive tests for web services, IoT devices, or the C++ project you are working on in CDT. EASE provides nice frontends to run and debug your tests. Of course support for automation and Jenkins integration is also available.</p>
	
	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/script_unittest.png"/></p>
	
	<p>With our latest release we extended the unit test framework to support python scripts too. Next to the simple, functional syntax we introduced OO testing similar to Junit. For python we integrated support for <a target="_blank" href="https://docs.python.org/3/library/unittest.html">pyunit tests</a>.</p>

<h2>The Quest for the Right Script Language</h2>
<p>Scripting is powerful. Some claim that JavaScript is not. We do not intend to start religious wars on the best scripting language, so we simply support them all. Starting out with Rhino we added more and more interpreters over the years: Jython, Groovy, Jruby, Beanshell, Nashorn and finally full python support via Py4J.</p>

<p>Using Py4J you may interact with any local python interpreter on your system. This allows to use the powers of numpy and scipy for scientific calculations. If python alone were not enough we also integrated support for <a target="_blank" href="http://jupyter.org/">Jupyter</a> notebooks right in the Eclipse IDE.</p>
	
	<a target="_blank" href="/community/eclipse_newsletter/2017/october/images/jupyter.png"><p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/october/images/jupyter_sm.png"/></a></p>
	
	
	<p>Interested? If you're at EclipseCon Europe (Oct 24-26, 2017) join our <a target="_blank" href="https://www.eclipsecon.org/europe2017/session/threesome-made-heaven-ease-python-jupyter-tutorial">extended python tutorial</a>. We do also have an <a target="_blank" href="https://www.eclipsecon.org/europe2017/session/and-people-said-let-there-be-scripts">introductory talk</a> where we demo the main features of EASE.</p>

	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/october/images/christian.jpeg"
        alt="Christian Pontesegger" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Christian Pontesegger<br />
            <a target="_blank" href="https://www.infineon.com/">Infineon Technologies AG</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/pontesegger">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

