<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "Location Matters";
  $pageKeywords = "eclipse, newsletter, geolocation, locationtech, geospacial,";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "In this newsletter, fill your boots with everything geospacial and learn what LocationTech is.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "Devoxx US";
  $sponsorLink = "https://devoxx.us/";
  $sponsorImage = "/community/eclipse_newsletter/assets/public/images/devoxxus_small.png";

  // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;