<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  <img align="left" class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geotrellis.png" alt="geotrellis logo"/>
  
  <p><a target="_blank" href="http://geotrellis.io/">LocationTech GeoTrellis</a> is a library based on the Apache Spark project that enables low latency, distributed processing of geospatial data, particularly for imagery and other raster data.</p>

<p>GeoTrellis has made significant progress in the past year. There are three accomplishments that stand in the forefront: (1) graduating from incubation with 1.0 release through LocationTech in December 2016, (2) the development of the feature-rich Python binding project called GeoPySpark, and (3) the support of Raster Foundry, a web application for processing imagery at scale with an emphasis on user friendly UI/UX. <a target="_blank" href="https://www.azavea.com/">Azavea</a> is pleased to share more about these endeavors and several additional achievements of the past year.</p> 

<h2>Project Background</h2>

<p>The core competency of GeoTrellis is raster data processing using the techniques of <a target="_blank" href="https://en.wikipedia.org/wiki/Map_algebra">map algebra</a>. In addition to raster support, the library includes some support for working with vector and <a target="_blank" href="https://en.wikipedia.org/wiki/Point_cloud">point cloud</a> data.</p>

<h3>1.0, 1.1, 1.2 Releases</h3>

<p>GeoTrellis is released under an Apache 2.0 license. It joined the LocationTech working group as an incubating project in 2013, and the 1.0 release was a significant milestone. In addition to being an indicator of the project’s maturity and reliability, the 1.0 release signified the promotion to a top level LocationTech project and graduation from incubation. In addition to the usual incubation tasks concerning intellectual property review and project governance, our key objective for the GeoTrellis contributors was to <a target="_blank" href="https://www.locationtech.org/projects/technology.geotrellis/who">expand the community of contributors</a> beyond the original core developers at Azavea, and we believe this is one of our most significant accomplishments over the past few years. In addition to higher level project goals, the 1.0 release brought many additional features, including:</p>

		<ul>
    			<li>Streaming GeoTiff support</li>
            <li>Windowed GeoTiff reading on S3 and Hadoop</li>
            <li>HBase and Cassandra support</li>
            <li>A new Collections API that enables users to bypass Spark in some cases</li>
            <li>Experimental support for Vector Tiles, GeoWave integration, and GeoMesa integration</li>
            <li>Expanded documentation moved to <a target="_blank" href="http://geotrellis.readthedocs.io/en/latest/">ReadTheDocs</a>. This greatly improves usability, readability, and searchability</li>
		</ul>
		
		
<p>Following the 1.0 release, GeoTrellis 1.1 was released in June 2017 and 1.2 is scheduled to be released in November 2017. We are following the <a target="_blank" href="http://semver.org/">SemVer</a> versioning semantics guidelines and aim to have three released per year.</p> 

<p>The 1.1 release introduced feature support for Spark-enabled cost distance calculations, expanded the non-Spark feature support through the implementation of a new Collections API, and support for conforming delaunay triangulation in order to ingest LiDAR point clouds. A <a target="_blank" href="https://en.wikipedia.org/wiki/Point_cloud">point cloud</a> is a set of data points with X,Y,Z coordinates and can be useful for terrain and volumetric analysis. This work was the result of a successful relationship through a private contract that was used to support open source work. This led to the <a target="_blank" href="https://github.com/geotrellis/geotrellis-pointcloud">GeoTrellis point cloud subproject</a> and the development of an <a target="_blank" href="https://pointcloud.geotrellis.io/">interactive demo</a>.</p> 

  
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/volumetric.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/volumetric_sm.png"/></a></p>
<p align="center"><small>Screenshot of the demo showing analysis of volumetric change calculations</small></p>

<h2>Python Bindings: GeoPySpark</h2>

<p>GeoTrellis is written in Scala. Scala provides many performance benefits and enables GeoTrellis to fully leverage the power of the Spark and Hadoop ecosystem. However, Python is a more commonly used language for many developers working with geospatial data. We created Python bindings for GeoTrellis to increase the project’s accessibility and released the project as <a target="_blank" href="https://www.azavea.com/blog/2017/09/19/introducing-geopyspark-a-python-binding-of-geotrellis/">GeoPySpark</a> in September 2017.</p>

<p>Much of the functionality of GeoPySpark is handled by another library, <a target="_blank" href="https://spark.apache.org/docs/0.9.0/python-programming-guide.html">PySpark</a>. <a target="_blank" href="https://www.py4j.org/">Py4J</a> is a tool that Spark (and in turn GeoPySpark) utilizes to enable Scala object and classes and their methods to be accessed in Python.</p>

<p>In addition to expanding the user base through Python support, GeoPySpark was designed to facilitate iterative workflows in a <a target="_blank" href="http://jupyter.org/">Jupyter Notebook</a>. GeoPySpark can be installed using ‘pip’ or accessed through a <a target="_blank" href="https://www.azavea.com/blog/2017/09/29/how-to-run-geopyspark-in-a-geonotebook-with-docker/">Docker container</a> that includes all of the necessary dependencies. Users can develop workflows and iterate on algorithmic design on a single machine in a Jupyter Notebook environment and easily scale the workflow to a national or global dataset by leveraging cluster computing. Azavea worked with the team at <a target="_blank" href="https://www.kitware.com/">Kitware</a> that created <a target="_blank" href="https://github.com/OpenGeoscience/geonotebook">GeoNotebook</a> - a tool that provides an embedded map inside of a Jupyter Notebook - to develop interactive workflows.</p>

   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/jupyter_sf.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/jupyter_sf_sm.png"/></a></p>
<p align="center"><small>Screenshot of GeoPySpark being used in a GeoNotebook</small></p>

<h2>Raster Foundry</h2>

<p>In addition to developing new libraries and tools that can use GeoTrellis, a new end-user application, Raster Foundry, is under development for earth observation analysis. The goal for <a target="_blank" href="https://www.rasterfoundry.com/">Raster Foundry</a> is to enable users to find, combine, and analyze earth imagery at any scale and share it on the web as well as leverage <a target="_blank" href="https://www.azavea.com/blog/2017/05/30/deep-learning-on-aerial-imagery/">deep learning</a> for object detection.</p>

<p>The development Raster Foundry is partially <a target="_blank" href="https://www.azavea.com/blog/2016/09/26/raster-foundry-model-lab-phase-ii-sbir/">supported</a> by two federal research grants from the US Department of Energy (DE-SC0013134) and NASA (NNX16CS04C).</p>

   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/wildfire_bc.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/wildfire_bc_sm.png"/></a></p>
   
   
<h2>Project Maturity</h2>

<p>Documentation is an essential component of any open source project. We have made a significant effort this past year to increase the quantity, quality, and searchability of the <a target="_blank" href="https://docs.geotrellis.io/en/latest/">GeoTrellis documentation</a>. The GeoTrellis documentation is hosted using <a target="_blank" href="https://readthedocs.org/">ReadTheDocs</a> - a tool designed to support usable documentation.</p>

<p>In addition to documentation, we are focused on the creation of example workbooks and tutorials to demonstrate core concepts. This work has begun with GeoPySpark and the tutorials can be found on <a target="_blank" href="https://github.com/geodocker/geodocker-jupyter-geopyspark/tree/master/notebooks">GeoDocker</a>.</p>

<p>We are also excited to share the work of <a target="_blank" href="https://www.marinetraffic.com/en/p/density-maps">MarineTraffic</a> that is using GeoTrellis for their shipping density calculations.</p> 
   

<h2>Looking Ahead</h2>

<p>We are currently in the process of focusing efforts on 1.3 and the 2.0 release that we anticipate for the first half of 2018. We believe that GeoTrellis contains much of the core functionality we envisioned for the project. This does not mean it is done or feature complete, and much work remains to make GeoTrellis more usable and accessible. GeoPySpark is a good start for reaching Python developers, but we still need to continue improving the documentation, tutorials, and demonstrations for both core GeoTrellis and GeoPySpark. There are also several features and optimizations that are on the roadmap for the coming year:</p>

    <ul>
        <li><a target="_blank" href="http://www.cogeo.org/">Cloud Optimized GeoTiffs</a> (COGs) is a format for internal organization of GeoTiff files that enables efficient retrieval of data subsets in cloud workflows. We have <a target="_blank" href="https://github.com/locationtech/geotrellis/issues/2284">plans</a> to make COGs a core component of GeoTrellis</li>
        <li>Bringing in additional features from GeoTrellis into GeoPySpark</li>
        <li>Map Algebra Modeling Language (MAML) is a declarative structure that describes a sequences of map algebra operations. This structure can be evaluated against a given collection of datasets to compute a result. Critically the evaluation logic is not specified in MAML, only the semantic meaning of the operations. This separation allows for multiple interpreters to exist that operate in different computational contexts. This has the potential to expose GeoTrellis processing power to custom interpreters.</li>
        <li>Improve GeoTrellis job performance and reduce resource requirements by optimizing data access patterns based on query and job structure</li>
        <li>Improve query performance for large spatiotemporal layers by implementing more advanced space filling curve indexing techniques, based on the approach used in GeoWave</li>
        <li>Expose support for <a target="_blank" href="http://www.opengeospatial.org/standards/wcs">WCS</a> OGC standard</li>
    </ul>
    
	<h2>Resources</h2>
	<ul>
        <li><a target="_blank" href="https://gitter.im/geotrellis/geotrellis">Gitter</a> (The fastest way to get an answer about GeoTrellis)</li>
        <li><a target="_blank" href="https://twitter.com/geotrellis">Twitter</a> (Track updates and share what you are working on!)</li>
        <li><a target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2014/march/article4.php">Introducing GeoTrellis</a> (March 2014 Eclipse Newsletter)</li>
        <li><a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2014/december/article4.php">GeoTrellis Adapts to Climate Change and Spark</a> (December 2014 Eclipse Newsletter)</li>
        <li><a target="_blank" href="https://github.com/locationtech/geotrellis">GeoTrellis on GitHub</a></li>
        <li><a target="_blank" href="https://docs.geotrellis.io/en/latest/">GeoTrellis documentation</a></li>
        <li><a target="_blank" href="https://github.com/locationtech-labs/geopyspark">GeoPySpark on GitHub</a></li>
        <li><a target="_blank" href="http://www.rasterfoundry.com/">Raster Foundry website</a></li>
       <li><a target="_blank" href="https://github.com/raster-foundry/raster-foundry"> RasterFoundry on GitHub</a></li>
       <li><a target="_blank" href="https://www.azavea.com/?s=geotrellis">Azavea blog articles on GeoTrellis</a></li>
	</ul>
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/ross.jpg"
        alt="Ross Bernet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Ross Bernet<br />
            <a target="_blank" href="https://www.azavea.com/">Azavea</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/rosszb">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.rasterfoundry.com/@ross.bernet">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

