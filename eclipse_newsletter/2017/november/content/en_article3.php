<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
  <h2>Location Intelligence at Sea and at Scale</h2>

<p>The data services company exactEarth uses a constellation of satellites in space to collect more than 25 million <a target="_blank" href="https://en.wikipedia.org/wiki/Automatic_identification_system">Automatic Identification System</a> (AIS) messages per day. This volume continues to increase as more of their 60+ exactView Real-Time powered by Harris satellites come online, because maritime vessels use the AIS system to report their position, identity, and additional navigation information. From this rich source of data about maritime activity, we can derive insights on shipping tonnage for capital and energy markets, the impact of weather on insurance markets and commercial supply chains, fuel consumption for clean energy applications, and much more.</p>
<p>That’s where the Eclipse Locationtech working group’s <a target="_blank" href="http://www.geomesa.org/">GeoMesa</a> comes in. GeoMesa is a robust cloud-native toolchain for processing a data stream in near-real time, visualizing that stream, storing it in a ‘big data’ solution, and analyzing it. For example, GeoMesa can <a target="_blank" href="https://cloud.google.com/blog/big-data/2017/01/scaling-exactearths-satellite-vessel-tracking-service-using-geomesa-on-google-cloud-platform">generate heat maps of AIS traffic</a>:</p>

    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geomesa3.png"/></p>


<h2>LocationTech GeoMesa</h2>

<p>The Eclipse Newsletter article <a target="_blank" href="http://www.eclipse.org/community/eclipse_newsletter/2014/march/article3.php">GeoMesa: Scaling up Geospatial Analysis</a> gave an overview of GeoMesa as an open-source spatio-temporal database backed by popular Hadoop-based databases for back-end storage. These databases let GeoMesa scale out horizontally to store billions and even trillions of spatial records. To ease developer adoption and use, GeoMesa integrates projects in the broader geospatial Java ecosystem such as GeoTools and GeoServer.</p>

<p>GeoMesa can also take advantage of Apache Spark, a computing framework that makes it easier to build advanced analytics. While using Spark requires no Hadoop infrastructure, it has become popular on that platform because it makes it so much easier to do custom analytics without worrying about the Map and Reduce steps that often had to be hand-coded in early Hadoop application development. Spark offers additional capabilities using optional modules such as the MLib machine learning library, the GraphX graph processing library, a streaming library, and the Spark SQL library.</p>

<p>The Spark SQL library provides a way to store data in table-like structures called DataFrames that can then be queried with SQL. This lets people take advantage of new classes of big data with a straightforward, interpreted language that many of these people have known for years.</p>
 
<p>While SQL can do math with retrieved data to calculate figures such as sums, averages, and differences among retrieved values, SQL implementations usually offer no spatial data types and functions. So, while you might have the right data to help you determine where your next retail outlet should be, vanilla SQL lacks the calculation horsepower that you would need to calculate the distance and area figures that make the best use of that data. Add-in packages for relational databases such as the PostGIS module for the PostgreSQL database and Oracle Spatial and Graph extend the SQL language for use with those particular databases, but until recently no such extensions existed for Spark SQL.</p>

<h2>GeoMesa and Spark SQL</h2>

<p>To address this lack of geospatial support in Spark SQL, the GeoMesa team added extensions to support geospatial data types such as points, linestrings, and polygons. To work with these new types, the team used <a target="_blank" href="http://github.com/locationtech/jts">LocationTech JTS</a> to implement a <a target="_blank" href="http://www.geomesa.org/documentation/user/spark/sparksql_functions.html">long list of new geospatial functions</a> that you can now call from Spark SQL when working with GeoMesa. GeoMesa also hooks into Spark’s Catalyst query optimizer to rewrite queries and push spatial filtering down to the underlying distributed database, making execution more efficient.</p>
<p>Users and analysts can access the power of Spark SQL by writing SQL, Scala, or Python to execute their queries. This brings this ability to perform complex geospatial analytics tasks to a broader user base. Using these with Notebook servers such as Apache Zeppelin makes it easier to rapidly prototype analytics and powerful visualizations.</p>

<h2>Exploring Hurricane Harvey</h2>

<p>To demonstrate the spatial SQL extensions, let’s use them to learn about how Hurricane Harvey affected the supply chain of vessels visiting ports in the Gulf of Mexico.</p>
<p>First, we use GeoMesa to load position reports for the region and the time period (August 25th to September 3rd, 2017) into a Spark dataframe named ‘vessels’. The first query on this dataframe will find which vessels are in which ports:</p>
<pre>
SELECT vessel_id, port_name, vessel_type
FROM ports JOIN vessels
ON st_contains(port_geometry, vessel_position)
AS vesselsJoinPorts
</pre>

<p>Without GeoMesa’s extensions, the port_geometry and vessel_position spatial columns and the st_contains spatial function would be unavailable. GeoMesa also offers options to partition data spatially and to cache dataframes in memory to accelerate such spatial joins.</p>

<p>Next, we can aggregate statistics for each port and for each vessel type:</p>

<pre>
SELECT first(port_name), first(vessel_type), count(distinct vessel_id) FROM vesselsJoinPorts
GROUP BY port_name, vessel_type
</pre>

<h2>Visualizing before, during and after Harvey</h2>

<p>Repeating similar queries with date ranges before, during and after the hurricane, we can compute statistics that analyze the hurricane’s effect on shipping. Within a <a target="_blank" href="https://zeppelin.apache.org/">Zeppelin Notebook</a> the AIS data can be queried and aggregated using Spark SQL and also natively plotted in the same environment. This makes it possible to generate figures like this one:</p>

<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geomesa5.png"/></p>
  	 
<p>This shows the density of unique cargo and tanker vessels for Port Houston, Texas across a the specified time range. The obvious lull of activity marks the region of time surrounding hurricane Harvey, and we can easily see how long vessel traffic was significantly affected for this port as well as the rate at which it recovered.</p>

<p>We can see the same story visually by creating heatmaps near Houston, Texas for date ranges before, during, and after the hurricane. Below we see that activity in the hurricane’s date range is restricted primarily to the port—that is, the ships are in port and moored.</p>

    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geomesa_houston.png"/></p>
   <p align="center"><small>The above heatmaps show cargo and tanker vessel  traffic near the Port of Houston before, during, and after Hurricane Harvey.</small></p>
   
<p>These are just two examples of how this toolset lets us explore the hurricane’s effect on different sectors and how long these sectors’ shipping pipelines may take to recover. GeoMesa’s Spark SQL capabilities let us easily reach insights based on millions of records using SQL queries rather than writing a complex, distributed analytic.</p>

<p>Similar applications abound for IoT and other tracking applications. The growing number of players in the booming field of Location Intelligence are providing new kinds of spatio-temporal data every day, and LocationTech GeoMesa continues to provide new tools to gain insight from large-scale combinations of this data.</p>

<p>Additional information:</p>
	<ul>
        <li><a target="_blank" href="http://www.exactearth.com/">exactEarth</a></li>
        <li><a target="_blank" href="http://www.geomesa.org/">GeoMesa.org</a></li>
        <li><a target="_blank" href="https://gitter.im/locationtech/geomesa">LocationTech GeoMesa Gitter</a></li>
        <li><a target="_blank" href="https://github.com/locationtech/geomesa">GeoMesa on GitHub</a></li>
    </ul>

<div class="bottomitem">
  <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/atallahhezbor.jpg"
        alt="Bob DuCharme" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Atallah Hezbor<br />
            <a target="_blank" href="http://www.ccri.com/">CCRi</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ccr_inc">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.ccri.com/blog/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/company/1400064/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
   <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/jimhughes.jpg"
        alt="Jim Hughes" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Jim Hughes<br />
            <a target="_blank" href="http://www.ccri.com/">CCRi</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
</div>

