<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
  
  <p><i>Geospatial data versioning and management available through LocationTech!</i></p>

<p><a target="_blank" href="http://geogig.org/">LocationTech GeoGig</a> is an open source tool for versioning of geospatial data. GeoGig’s versioning capabilities empower geospatial data production workflows. It allows data edits to be tracked and stored in a history, accessible to users at any time. GeoGig also provides new and innovative workflows for decentralized data management, enabling multiple users to edit, review, and publish updated geographic data in customized workflows. GeoGig draws its inspiration from <a target="_blank" href="https://git-scm.com/">Git</a>.</p>

  <h2>GeoGig 1.2.0 Release</h2>
  
  <img align="right" class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geogig.png"/>
 <p>GeoGig 1.2.0 has three major changes;</p>
	<ol>
        <li>Support for the latest release of GeoServer (GeoServer 2.12)</li>
        <li>Improved performance for data sharing functionality including Clone/Fetch/Pull/Push operations</li>
        <li>Numerous usability improvements to support the upcoming GeoGig plugin for QGIS</li>
	</ol>
<p>For GeoServer 2.12 and newer, you must use GeoGig version 1.2.0 or newer.</p>
<p>For GeoServer 2.11 and older, you must use GeoGig version 1.1.1.</p>


<h2>Support for GeoServer 2.12 (WebAPI Refresh)</h2>

<p>To provide GeoGig users with the latest functionality in GeoServer, GeoGig 1.2 has added support for GeoServer 2.12. One of the big changes in GeoServer 2.12 was to replace Restlet with Spring MVC. This necessitated GeoGig to also replace Restlet with Spring MVC.</p>

<p>We made the new WebAPI exactly equivalent to the old WebAPI, so all the requests and responses are exactly the same - this means that web client applications (like QGIS and GeoServer) do not need to make <b><i>any</i></b> changes to connect to the updated WebAPI. We also made some improvements to the API documentation <a target="_blank" href="http://geogig.org/docs/interaction/web-api.html">here</a>.</p>

<p>GeoServer 2.12 offers lots of new features and improvements including map styling enhancements like style based layer groups and options for KML Placemark placement; <a target="_blank" href="http://blog.geoserver.org/2017/10/17/geoserver-2-12-0-released/">http://blog.geoserver.org/2017/10/17/geoserver-2-12-0-released/</a></p>

<h2>Improved performance for Replication - Clone/Fetch/Pull/Push</h2>

<p>For this release, we have concentrated on improving how changes are shared among people (replication). Replication supports robust data production workflows that are either in-house, or distributed among different organizations or different locations. Replication operations like clone, fetch, push, and pull enables each data version to be tracked, synched, and merged across repositories and shared amongst many (or a few) people. We have greatly improved the speed of these data sharing operations to make collaboration even easier and noticeably faster - sometime several orders of magnitude faster!</p>  
  
  
  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/geodata.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/geodata_sm.png"/></a></p>
  <p align="center"><i>Figure 1: In this replication workflow, data editors in three locations (left) update data and share changes with one another before pushing updates to the production database. From there, quality checks (QA) are completed (with revisions sent between QA and data editors as needed), and the approved data is published via one-way replication for public view (right).</i></p>
  
  <h2>Usability improvements to support the GeoGig plugin for QGIS</h2>

<p>This release supports the upcoming GeoGig plugin for QGIS, which gives a powerful and user-friendly GUI front-end to GeoGig.  We've made some improvements - bug fixes and performance increases - to make this plugin as useable as possible. The GeoGig plugin for QGIS will be released in early 2018.</p>
  
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/streetmap.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/streetmap_sm.png"/></a></p>
 	<p align="center"><i>Figure 2: A preview of the GeoGig QGIS plugin. This view highlights a ‘diff’ layer exported from GeoGig - a layer that  shows geometric changes between two points in the data history (a ‘before’ and ‘after’ view).</i></p>
 	
 	<h2>Getting Started with GeoGig</h2>

<p>GeoGig 1.2.0 downloads and more information are available from <a target="_blank" href="https://www.locationtech.org/projects/technology.geogig/downloads">LocationTech</a>. The GeoGig website also has a <a target="_blank" href="http://geogig.org/#install">Getting Started guide</a> and detailed <a target="_blank" href="http://geogig.org/docs/index.html">documentation</a>. GeoGig is an Open Source project, so anyone can <a target="_blank" href="https://github.com/locationtech/geogig/blob/master/doc/technical/source/developers.rst">contribute to the project</a>. Boundless Spatial is proud to have contributed 100% of the code and documentation for the GeoGig 1.2.0 release.</p>

<p>For more information about how versioned geospatial data editing can be used in your geospatial solutions, please contact LocationTech at <a href="mailto:info@locationtech.org?subject=GeoGig">info@locationtech.org</a>.</p> 
<h3>Bundles</h3>
<p><b>LocationTech supported:</b></p>
    <ul>
    		<li>Command line app from LocationTech: <a target="_blank" href="http://download.locationtech.org/geogig/geogig-1.2.0.zip">geogig-1.2.0.zip</a></li>
    </ul>

<p><b>Plugins built on Geogig:</b></p>
	<ul>
        <li><a target="_blank" href="https://github.com/locationtech/geogig/releases/download/v1.2.0/geogig-plugins-osm-1.2.0.zip">OpenStreetmap data 1.2.0 plugin</a></li>
        <li><a target="_blank" href="https://github.com/locationtech/geogig/releases/download/v1.2.0/geoserver-2.12-SNAPSHOT-geogig-plugin.zip">GeoServer 2.12.x plugin</a></li>
        <li><a target="_blank" href="https://github.com/locationtech/geogig/releases/download/v1.2.0/geoserver-2.13-SNAPSHOT-geogig-plugin.zip">GeoServer 2.13.x plugin</a></li>
	</ul> 
	
<p><b>NOTE:</b> If you are looking for a plugin for GeoServer 2.11 or older, you must use <a target="_blank" href="https://github.com/locationtech/geogig/releases/tag/v1.1.1">version 1.1.1 of GeoGig</a>.</p>

 	
 	
 
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/hannah.jpg"
        alt="Hannah Bristol" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Hannah Bristol<br />
            <a target="_blank" href="https://boundlessgeo.com/">Boundless</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/hannah-bristol-gis/">LinkedIn</a>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

