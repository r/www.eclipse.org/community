<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
 
 <p>Eclipse collaborative working groups explore new open source fields, helping our community build bridges to new industries. LocationTech is exploring the application of open source mapping and location technologies. These technologies are experiencing rapid growth as they are realized across our new IT landscape.</p>
 
 
  <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/jts_web.png"/></p>
  
  <h2>JTS Topology Suite</h2>

<p>There are two fundamental concepts a location-based technology stack needs to provide: the shape of items in the world, and where those items are located.</p>

<p>The JTS Topology Suite project tackles the first issue. It is a “geometry” library written in Java. JTS is a little bit more, it is <b>the geometry library</b> for the open source spatial industry - with ports to JavaScript, C++ and several other widely used languages.</p>

<p>This project acts as cornerstone of the open source spatial community. The project has been recognized for its importance by the presentation of the OSGeo Sol Katz Award to project lead Martin Davis in 2011. Worldwide hundreds of projects use JTS, either directly in the JVM, or indirectly through JavaScript or C++ ports. At LocationTech, JTS is used by our cloud processing solutions (GeoMesa, GeoTrellis, GeoWave), distributed data control (GeoGig), and desktop applications (uDig), and the Spatial4J library.</p>
  
  
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/jts_1.png"/></p>
 
 <p>To explain what the fuss is about, it helps to explore what goes into a “geometry library”. While many projects (including Java 2D and SWT) have a data structure for shapes, JTS functions as a topology suite implementing spatial data types, spatial relationships between geometries, and spatial operations for processing geometries.</p>
 
 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/jts_2.png"/></p>
 <p align="center"><small>Spatial operations including Voronoi, convex hull, buffer and simplification</small></p>
 
 <p>The JTS Library is focused on planar linear geometry (i.e. shapes defined by straight-line segments on a flat surface).  It provides an  extensive framework for algorithm development and exploration. The core data structures are Point, Line and Polygon along with geometry collections. These are supplemented with indexes, edge graphs, a pluggable precision model, and perhaps most importantly point-set-theory predicates and overlays.</p>

<p>JTS works with standards, providing an implementation of the the industry standard “Simple Feature for SQL” geometry data structures and operations. The project also provides support for standard spatial text (WKT), binary (WKB), GeoJSON and GML formats.</p>

<p>A key success factor for JTS is over a decade of extensive testing. Many of the algorithms employed are intensive running into the limitations of computational stability. A battery of hundreds of tests have been built up over time. The tests ensure that robustness and stability is maintained across releases, which is essential to such a widely-used library.</p>

<p>One of the joys of working on a spatial library is the ability to see a picture of what you are working on. Tests are defined as XML files, captured with the visual JTS TestBuilder tool shown below.</p>

   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/jts_testbuilder.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/jts_testbuilder_sm.png"/></a></p>
    <p align="center"><small>JTS TestBuilder</small></p>
    
    <h2>JTS Topology Suite 1.15 at Eclipse Foundation</h2>
    <p>JTS is an established project, with an initial 1.0 release in 2002. This November, JTS has completed the Eclipse incubation process with the release of JTS 1.15.</p>

	<p>Key features of the JTS 1.15 release:</p>
	<ul>
        <li>Initial LocationTech release with focus on organization and packaging, migration from svn to git, maven build chain and breakdown into modules</li>
        <li>New dual license: Eclipse Public License and Eclipse Distribution License</li>
        <li>K-Nearest Neighbor search for STR-Tree</li>
        <li>Improved handling of Quadtree queries</li>
        <li>Intersects now supports GeometryCollection</li>
        <li>JTSTestRunner command-line application</li>
    </ul>

<p>We would like to thank the the community, our mentors Gunnar Wagenknecht and Wayne Beaton, and the Eclipse Staff for their support.</p>

    
 <h2>The Future is Spatial</h2>

<p>We have some great plans for the year ahead, and welcome your interest and collaboration. In the short term we are pursuing greater collaboration with the downstream JSTS (JavaScript) and GEOS (C++) teams. We are also excited to outline a JTS 2.0 focused on supporting multiple geometry interfaces, and more extensive operations infrastructure to collaborate with our fellow LocationTech Spatial4J project.</p>

<p>For more information visit the <a target=="_blank" href="https://www.locationtech.org/projects/technology.jts">JTS Project Page</a> or catch up with the recent <a target=="_blank" href="https://www.slideshare.net/jgarnett/state-of-jts-2017">State of JTS 2017</a> presentation. We look forward to doing great work in the year ahead, please join us!</p>

<p>Additional information:</p>
	<ul>	
		<li><a target=="_blank" href="https://www.locationtech.org/projects/technology.jts">LocationTech JTS Topology Suite</a></li>
		<li><a target=="_blank" href="https://www.slideshare.net/jgarnett/state-of-jts-2017">State of JTS 2017</a></li>
 	</ul>
 
  
 
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/jody.jpeg"
        alt="Jody Garnett" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jody Garnett<br />
            <a target="_blank" href="https://boundlessgeo.com/">Boundless</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jodygarnett">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://boundlessgeo.com/author/jgarnett/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

