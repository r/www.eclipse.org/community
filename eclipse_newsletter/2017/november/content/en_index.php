<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">
      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2017.11.22</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/roxannenew.png" />
                            <h3>Editor's Note</h3>
                            <p>There is an assumption that up to 80% of all objects and activities are linked to a specific and unique location, permanently or at any given time. This implies that a majority of all - big and small - data has a spatial or spatial-temporal component. Something that is of great value for asset management, remote sensing, IoT, and autonomous cars, just to name a few. No wonder that geospatial technology has become the must-have in many commercial and industrial applications.</p>
							
							<p><a style="color:#000;" target="_blank" href="https://www.locationtech.org/">LocationTech</a>, an Eclipse Foundation Working Group for spatially aware technology, is the home for Big (and smaller) Geospatial Data processing and visualization components and development tools. Within LocationTech, a variety of stakeholders collaborate on projects that deal with the various challenges presented by the 'Big Data V's'.</p>
							
                            <p>This issue of the Eclipse Newsletter is dedicated to some of the latest Big Geospatial Data development projects available. We kick off with an article about the open source geospatial toolkit, JTS 1.15. We then explore LocationTech GeoTrellis before moving on to two case studies: 1) exactEarth and GeoMesa being used for Hurricane Harvey and 2) Spacial Data and Precision Agriculture. Finally, we learn what's new in GeoGig 1.2 and what FOSS4G NA is.</p>
                            
                            
                            <p>You are welcome to join the conversation, participate, and benefit regardless of background - industry, government, science, or academia, etc.</p>
                            
                            <p>We'd like to thank the Eclipse LocationTech community for contributing these articles. It was a great pleasure to work with each author and the community as a whole.</p>
                            
                            <p>Next year, on May 14-17, LocationTech will co-host <a style="color:#000;" target="_blank" href="http://2018.foss4g-na.org/">FOSS4G NA</a>, the annual North American open spatial conference in St. Louis, Missouri. The Call for Papers is now open!</p>
                            
                            <p>Sit back, relax, and read.</p>
                            
                            <p>Roxanne Joncas (Editor)<br>
                            Thea Aldrich &amp; Marc Vloemans (LocationTech Guest Editors)</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article1.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article1.php"><h3>Geospatial starts with Geometry</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>A cornerstone of the open source geospatial toolkit, JTS 1.15 brings spatial data types, spatial relationships, and spatial operations for processing geometries to the Eclipse Foundation.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article2.php"><h3>LocationTech GeoTrellis - What's New</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>GeoTrellis continues to advance the field of distributed processing of geospatial imagery and raster data.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article3.php"><h3>Maritime Location Intelligence with exactEarth data and LocationTech GeoMesa</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>The CCRI team shows how exactEarth’s maritime dataset was used with GeoMesa to explore shipping traffic during Hurricane Harvey.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article4.php"><h3>Spatial Data and Precision Agriculture</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Witness the power of LocationTech software when used as a tool for precision agriculture and large scale farming.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

	  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article5.php"><h3>LocationTech GeoGig 1.2.0 now released!</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>GeoGig 1.2 delivers improved usability with QGIS, better Replication and support for GeoServer 2.12.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article6.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="http://www.eclipse.org/community/eclipse_newsletter/2017/november/article6.php"><h3>What is FOSS4G NA 2018</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Get a sneak peek at the premier 2018 North American conference and tradeshow for all things open source geospatial comes to St. Louis, MO.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><a target="_blank" href="https://www.locationtech.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/locationtech.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></a></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
  
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
    
  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Featured Article | Phaser Editor</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">

                            <p>This month, we're featuring a special article about <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/november/phasereditor.php">Phaser Editor</a>.</p>
                            <p>Discover the Eclipse-based IDE for HTML5 games creation and create a video game of your own!</p>
                            <p>If you want to start in the game development world, as a hobby or as a career, HTML5 games are simple to start with and have a promising future. Phaser is one of the most popular HTML5 game dev frameworks. Phaser Editor is a tool made specifically for Phaser that is continuously is growing and maturing. The fact that it is made with Eclipse technology allows us to reuse a lot of powerful features, third-party plugins. It also allows us to enter into the Eclipse community as a serious game development alternative.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2017/november/phasereditor.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/phaser_dino.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->  
      
          
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://www.youtube.com/eclipsefdn"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/october/images/videoofthemonth.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
					      <tr>
						    <td valign="top" class="textContent">
                            <p>Each month, we will post a popular or new YouTube video from the <a target="_blank" style="color:#000;" href="https://www.youtube.com/eclipsefdn">Eclipse Foundation YouTube Channel</a> here.</p>
                          </td>
                         </tr>
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://youtu.be/cnSMhgKApOg"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/mikestateoftheunion.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      <tr>
                           <td valign="top" class="textContent">
                             <p><a target="_blank" style="color:#000;" href="https://www.youtube.com/eclipsefdn">Subscribe</a> to our channel and <a target="_blank" style="color:#000;" href="mailto:youtube@eclipse.org?subject=YouTube%20Video%20Suggestion">share</a> videos about Eclipse technology with us.</p>
                             </td>

                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
   <!-- // MODULE ROW -->
      
    
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Java IDE Tips &amp; Tricks</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">

                            <p>Here is this month's tip in action &#10145;<br>
                            <p style="color:blue">Enjoy the Edit &rarr; Block Selection (Alt+Shift+A) while working on your Java 9 module-info.java</p> 
                            <p><i><small>Click on the gif to view the original tweet and gif.</small></i></p>
                            
                            <p>Want more tips? Follow <a target="_blank" style="color:#000;" href="https://twitter.com/eclipsejavaide">@EclipseJavaIDE</a> on Twitter.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                        <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://twitter.com/EclipseJavaIDE/status/931124796502536192"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/java9.gif"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
       <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
							 <a target="_blank" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/virtualmeetup.png"
                            width="<?php print $col_2_img; ?>" class="img-responsive"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
					      <tr>
						    <td valign="top" class="textContent">
                            <p>The <a target="_blank" style="color:#000;" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/">Virtual Eclipse Community MeetUp</a> is back with new LIVE webinars. One or two meetups will be hosted every month. They will air live on YouTube, where you can join and chat with other viewers and ask questions to the speaker.</p>
                            <ul>
                            <li>December 13: <a target="_blank" style="color:#000;" href="https://www.meetup.com/Virtual-Eclipse-Community-MeetUp/events/245130795/">Building form-based UIs with EMF Forms</a> with Jonas Helming</li>
                            </ul>
                            <p>Join as a Member to receive updates about future MeetUps. If you have topic suggestions for future meetups. Comment on the MeetUp or email us <a target="_blank" style="color:#000;" href="mailto:marketing@eclipse.org?Subject=Eclipse%20Virtual%20Eclipse%20Meetup%20Topic">Email us</a>.
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
   <!-- // MODULE ROW -->
      

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // --> <!--
                        The table cell imageContent has padding
                          applied to the bottom as part of the framework,
                          ensuring images space correctly in Android Gmail.
                                                        -->
                      <table border="0" cellpadding="0"
                        cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="imageContent">
                          <p align="center"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/august/images/blackbar.png"
                            width="<?php print $col_1_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_1_img; ?>;" /></p>
                          </td>
                          </tr>

                       </table> <!-- // CONTENT TABLE -->
                      <div style="clear: both;"></div>
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
  <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table background="#f1f1f1" border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Announcements</h3>
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://mmilinkov.wordpress.com/2017/11/15/help-pick-the-new-name-for-java-ee/">Help Pick the New Name for Java EE</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2017/news/eclipsecon-ludwigsburg-great-partners">EclipseCon + Ludwigsburg = Great Partners</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://eclipse.org/org/press-release/20171019_industry40_testbed.php">New Industry 4.0 Open Testbed Addresses Performance Monitoring and Management in Manufacturing</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://www.eclipse.org/org/press-release/20171018_scienceopensource.php">Eclipse Science Advances Open Source Technology for Scientific Research</a></li>
                            		<li><a target="_blank" style="color:#000;" href="https://eclipse.org/org/foundation/minutes.php#board">Eclipse Foundation Board Minutes</a></li>
                           </ul>
                           <p>Have Eclipse project or member news to share with the community? <a target="_blank" style="color:#000;" href="mailto:news@eclipse.org?Subject=Eclipse%20Community%20News">Email us</a>.</p>
                          </td>
                        </tr>

                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">

                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Eclipse Community News</h3>
							<ul>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipse-microprofile-interview-jax-london-138988.html">Think of Eclipse MicroProfile as two things: The project and the technology set</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/skymind-deeplearning4j-eclipse-138872.html">Skymind's Deeplearning4j, the Eclipse Foundation, and scientific computing in the JVM</a></li>
								<li><a target="_blank" style="color:#000;" href="https://www.infoq.com/news/2017/11/jax-london-microprofile">Accelerating the adoption of Java microservices with Eclipse MicroProfile</a></li>
								<li><a target="_blank" style="color:#000;" href="https://sdtimes.com/industry-spotlight-java-ee-finally-gets-rebooted">Industry Spotlight: Java EE finally gets rebooted</a></li>
								<li><a target="_blank" style="color:#000;" href="https://jaxenter.com/eclipsecon-key-takeaways-138344.html">EclipseCon Europe 2017 key takeaways: Java 9, EE4J, Eclipse MicroProfile and more</a></li>
								<li><a target="_blank" style="color:#000;" href="http://www.businesswire.com/news/home/20171019005551/en/Renesas-Electronics-Unveils-Integrated-Software-Development-Environment">Renesas Electronics Unveils Integrated Software Development Environment for ADAS and Automated Driving</a></li>
								<li><a target="_blank" style="color:#000;" href="https://dzone.com/articles/15-productivity-tips-for-eclipse-java-ide-users">15 Productivity Tips for Eclipse Java IDE Users</a></li>
							</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
    <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-tyrus">Eclipse Tyrus</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-openmq">Eclipse OpenMQ</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-glassfish-tools">Eclipse GlassFish Tools</li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-grizzly">Eclipse Grizzly</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-jersey">Eclipse Jersey</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-restful-web-services-api-java">Eclipse RESTful Web Services API for Java</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-message-service-api-java">Eclipse Message Service API for Java</a></li>                            	

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-websocket-api-java">Eclipse WebSocket API for Java</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-mojarra">Eclipse Mojarra</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-json-processing">Eclipse JSON Processing</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-cognicrypt">Eclipse CogniCrypt</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-kuksa">Eclipse Kuksa</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-cyclone">Eclipse Cyclone</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-pax-lang">Eclipse PAX Lang</a></li>

                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-crossmeter">Eclipse CROSSMETER</a></li>
                            	</ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                             <ul>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.dirigible/releases/3.0">Eclipse Dirigible 3.0</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.emf.diffmerge/releases/0.9.0/plan">Eclipse EMF Diff/Merge 0.9</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://www.locationtech.org/projects/technology.jts/releases/1.15/plan">LocationTech JTS Topology Suite 1.15</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4e/releases/0.4.0/plan">Eclipse LSP4E 0.4</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/science.texlipse/releases/2.0.0/plan">Eclipse TeXlipse 2.0</a></li>
                             	<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.nebula/releases/1.4.0/bugs">Eclipse Nebula - Supplemental Widgets for SWT 1.4</a></li>
                             	</ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->


                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://events.eclipse.org/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2017/november/images/nov_events.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Here is the list of upcoming Eclipse events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_Day_Lyon_2017">Eclipse Day Lyon</a><br>
                            Nov 28, 2017 | Lyon, France</p>
                             <p><a target="_blank" style="color:#000;" href="https://www.eventbrite.de/e/eclipse-democamp-december-2017-tickets-38379630508">DemoCamp Munich</a><br>
                            Dec 4, 2017 | Munich, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="https://wiki.eclipse.org/Eclipse_DemoCamp_December_2017/Hamburg">DemoCamp Hamburg</a><br>
                            Dec 11, 2017 | Hamburg, Germany</p>
                            <p><a target="_blank" style="color:#000;" href="http://2018.foss4g-na.org/">FOSS4G NA</a><br>
                            May 14-17, 2018 | St. Louis, MO, USA</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->
