<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>This year’s North American edition of the <b>F</b>ree and <b>O</b>pen <b>S</b>ource <b>S</b>oftware for(4) <b>G</b>eospatial (<a target="_blank" href="https://2018.foss4g-na.org/">FOSS4G</a>) conference is taking place in St. Louis, Missouri from May 14-17, 2018. The program committee this year is excited to announce our <a target="_blank" href="https://2018.foss4g-na.org/cfp">Call for Proposals</a> has opened, and on behalf of the <a target="_blank" href="https://2018.foss4g-na.org/about-pc">conference committee</a>, I invite you to be a part of this one-of-a-kind event.</p>

 <h2>What is FOSS4G NA?</h2>

<p>A collaborative effort between <a target="_blank" href="http://locationtech.org/">LocationTech</a> and <a target="_blank" href="http://osgeo.org/">OSGeo</a>, powered by community volunteers: FOSS4G NA is an annual conference for people who work with, or are interested in working with, free and open source geospatial software. It brings together a mix of developers, users, decision makers, and observers from a broad spectrum of organizations and fields of operation for three days of workshops, presentations, and discussions.</p>
 

 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/f4g_event.jpg"/></p>
<p align="center"><small>Scenes from FOSS4G NA 2016. Photos by Jody Garnett &amp; Anne Jacko, Eclipse Foundation</small></p>


<h2>About Our Program</h2>
<p>FOSS4G NA’s conference committee is volunteer-powered: we are a geographically-dispersed team that comes from a variety of backgrounds. Working together, we look forward to seeing this FOSS4G NA 2018 present a wide range of content that showcases the best of our community's breadth and diversity. From software developers to educators, climate scientists to designers: the community of people who use &amp; contribute to free &amp; open source geospatial software is vast. In 2018, we aim to show that as members of this community, we each bring something unique to the mix, and FOSS4G is the common thread that binds us together.</p>

<p>A non-exhaustive list of topics we plan to feature on the schedule includes:</p>
    <ul>
        <li>Software Projects</li>
        <li>Business</li>
        <li>Government</li>
       	<li>Nonprofit &amp; Humanitarian Work</li>
        <li>Education &amp; Academia</li>
        <li>The Sciences</li>
        <li>Data Visualization</li>
        <li>New &amp; Emerging Technology</li>
        <li>&hellip;and more!</li>
	</ul>
<p>You can see a detailed topic list along with more information on <a target="_blank" href="https://2018.foss4g-na.org/cfp">our CFP</a>.</p>

<h2>Join Us In St. Louis</h2>

<p>As North America’s largest gathering of students & professionals who share the common interest of free & open source geospatial tools, FOSS4G NA is going to be an amazing event. If you’re interested in speaking, I encourage you to <a target="_blank" href="https://2018.foss4g-na.org/cfp">submit your talk or workshop</a> proposal today. If you or your company would like to know more about partnering or sponsoring this event, <a target="_blank" href="https://2018.foss4g-na.org/prospectus">learn more from the sponsor prospectus</a>.</p> 

<p>See you in St. Louis!</p>

<p>Sara Safavi<br>
Program Chair, FOSS4G NA 2018</p>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/sara.jpg"
        alt="Sara Safavi" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Sara Safavi<br />
            <a target="_blank" href="http://planet.com/">Planet</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/sarasomewhere">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.sarasafavi.com/">Blog</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/sarasafavi">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

