<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>Today, web browsers are everywhere: phones, TVs, laptops, PC, video game consoles. Browsers are getting so sophisticated and powerful that users can do video conferences, play videos, access social networks, browse realistic maps, create content offline, and play games!</p>

<p>This latest browser revolution has a name: <a target="_blank" href="https://en.wikipedia.org/wiki/HTML5">HTML5</a>, the last standard that is implemented by major browsers on all the platforms. HTML5 is a browser API that allows developers to create apps that provide a richer experience to the users.</p>

<p>One of the exciting things you can do with HTML5 is create video games. Traditionally, playing rich games in a browser was only possible by installing third-party plugins like the Flash Player, Java Applets, or the Unity Web Player, however, these technologies are being deprecated in all major browsers and replaced by HTML5.<p>

<p>New standards like <a target="_blank" href="https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/">Progressive Web App</a> (from Google and recently adopted by <a target="_blank" href="https://hacks.mozilla.org/2017/10/progressive-web-apps-firefox-android/">Firefox</a>) are going to remove the boundaries between the browser apps and native apps, and web games will get more of the space that is filled by native games in desktop and mobile devices today.</p>

<p>HTML5 games are getting special attention on traditional online games portals, but also in social networks giants like Facebook (see <a target="_blank" href="https://techcrunch.com/2016/11/29/messenger-instant-games/">Facebook Instant Games</a>) are revolutionizing the way games are played and distributed, therefore giving more relevance to the social component.</p>

<p>In this article, we present Phaser Editor, a tool to create HTML5 video games.</p>

<h2>Phaser Editor</h2>

<p><a target="_blank" href="http://phasereditor.boniatillo.com/">Phaser Editor</a> is an Eclipse-based IDE to create HTML5/JavaScript video games. The games use the popular <a target="_blank" href="https://phaser.io/">Phaser</a> game development framework, a third-party project of <a target="_blank" href="https://www.twitter.com/photonstorm">Richard Davey</a> at <a target="_blank" href="http://www.photonstorm.com/">PhotonStorm</a>.</p>

<p>Phaser provides a powerful set of functions that cover the main areas of game development: assets loading, physics, animations, particles, camera, tilemaps, input handling, sound. In few lines of code the user can develop an idea, which makes Phaser a great option for game dev starters.  Bigger games can be implemented by larger teams following Object Oriented Programming patterns.</p>

<h3>A Phaser game that shows an image</h3>
<pre style="background:#fff;color:#000"><span style="color:#ff7800">var</span> game = new Phaser.Game(<span style="color:#3b5bb5">800</span>, <span style="color:#3b5bb5">600</span>, Phaser.AUTO, <span style="color:#409b1c">'phaser-example'</span>, 
{ preload<span style="color:#ff7800">: </span>preload, create<span style="color:#ff7800">: </span>create });

<span style="color:#ff7800">function</span> <span style="color:#3b5bb5">preload</span>() {
game.load.image(<span style="color:#409b1c">'einstein'</span>, <span style="color:#409b1c">'assets/pics/einstein.png'</span>);
}

<span style="color:#ff7800">function</span> <span style="color:#3b5bb5">create</span>() {
game.add.sprite(<span style="color:#3b5bb5">0</span>, <span style="color:#3b5bb5">0</span>, <span style="color:#409b1c">'einstein'</span>);
}
</pre>

<p>Phaser Editor provides code editors and visual tools that accelerate the development of Phaser games: texture packer, assets manager, previewers, scenes and prefabs maker, animations editor, JavaScript editors, documentation navigators, and others. These tools use the Phaser formats or generate plain Phaser code, so no limits are set to what the user can do directly in code, by calling the Phaser API.</p>

<p>Because the editor is based on Eclipse technology, many features are available by default, like multiple platforms support, Git tooling, flexible (layout) and customizable (perspectives) workbench, projects and workspace, preferences UI, key bindings, automatic updates, plugins marketplace, UI themes, offline help, embedded browser, refactorings, search, just to name a few.</p>

<p>The editor can be downloaded from the Phaser Editor <a target="_blank" href="http://phasereditor.boniatillo.com/blog/downloads">Downloads</a> page. Follow these steps:</p>
    <ol>
        <li>Pick the one of your operating system,</li>
        <li>download it,</li>
        <li>unpack it,</li>
        <li>run the <b>Phaser Editor</b> executable (it does not require any special installation).</li>
    </ol>

<h3>Start by creating a project</h3>

<p>To start a game it is very simple, there are two project wizards to create a game: the <b>Phaser Project</b> and the <b>Phaser Example Project</b> wizards. Phaser beginners should start with the <b>Phaser Example Project</b> wizard, it shows a list of more than 600 official examples, and some others created to showcase Phaser Editor features. The user can select one example and create a project with it.</p>

<p>The Phaser Project wizard (File &rarr; New &rarr; Phaser Project) shows a couple of parameters like the language (JavaScript  or TypeScript), the size of the game, the inclusion of demo assets, the game structure: simple, with one or with multiple scenes.</p>

<p>For the first time keep all parameters by default, but check the <b>Include demo asset</b>, to get an already working scene.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/new_phase_project.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/new_phase_project_sm.png"/></a></p>
<p align="center"><small>Wizard to create a game project</small></p>

<p>Press <b>Finish</b>, it will create a new project and open the scene <b>Level.canvas</b>:</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/workspace.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/workspace_sm.png"/></a></p>
<p align="center"><small>Scene created by default</small></p>

<p>Learn more in the <a target="_blank" href="http://phasereditor.boniatillo.com/docs/first_steps.html">First Steps section</a> of the Phaser Editor docs.</p>

<h3>Texture packing</h3>

<p>Texture packing is an essential task in game development. It is the process of merging different images into one big image together with a description file that includes the information of the original image positions and names. This is named the texture atlas or texture map.</p>

<p>In Phaser Editor, there is a texture packer that support different algorithms, specially the <code>MAX_RECTS</code> algorithms, that allows an efficient packing.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/textures.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/textures_sm.png"/></a></p>
<p align="center"><small>The Texture Atlas Generator editor</small></p>

<p>Learn more in the <a target="_blank" href="http://phasereditor.boniatillo.com/docs/texture_packer.html">Texture Packing Editor section</a> of the Phaser Editor docs.</p>

<h3>Assets management</h3>

<p>In Phaser there are two ways to load the assets, one by one or by loading an asset pack. The asset pack is a manifest file (with a JSON format) where all assets are declared. Each asset pack entry is grouped in a section and contains all the parameters of the asset. Phaser Editor provides an editor for this asset pack file, so the user can load the assets quickly and use them in other parts of Phaser Editor, like the scenes maker and the Preview window.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/declare_assets.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/declare_assets_sm.png"/></a></p>
<p align="center"><small>The Assets Pack editor</small></p>

<p>Phaser supports many asset types: image, texture atlas, sprite-sheet, audio, video, tile-maps, and others. The asset pack editor supports all of them and provides the documentation for all the parameters of the assets. This documentation is extracted automatically from the Phaser docs, so it helps the user to learn the Phaser API.</p>

<p>A great featured provided by the Phaser Editor is the refactoring of assets. It uses the Eclipse Refactoring feature to replace textures of the assets or move the assets to different sections. These refactoring operations allow the user to start the game with demo assets and replace them with the final art, at any moment.</p>

  <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/remove_asset.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/remove_asset_sm.png"/></a></p>
<p align="center"><small>Refactoring dialog to remove an asset</small></p>

<p>The asset packs with the assets, the scenes, and prefabs, are shown in the <b>Assets</b> view. This view is very important because the user can browse the assets, preview them, delete them, rename them, replace their textures or drop them into scenes, or search all the scenes (using the Eclipse Search feature) that references a particular asset.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/assets.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/assets_sm.png"/></a></p>
<p align="center"><small>Asset navigator</small></p>

<p>Learn more about the <a target="_blank" href="http://phasereditor.boniatillo.com/docs/assets_manager.html">Assets Pack editor and the Assets view</a> in Phaser Editor docs.</p>

<p>The assets can be previewed by hovering the mouse over it in the Assets view or the Asset Pack editor. However there is a Preview view where the user can drop an asset for a detailed preview. The common is to drop there the sprite-sheet and atlas assets. The user can drag and drop the texture frames from the Preview window to the scenes. In the case of the sprite-sheet preview, the user can select some frames and animate them.</p>

 <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/dino_preview.png"/></p>
<p align="center"><small>In a sprite-sheet preview, the frames can be animated</small></p>

<p>Learn more about the <a target="_blank" href="http://phasereditor.boniatillo.com/docs/preview_window.html">Preview window</a> in the Phaser Editor docs.</p>

<h3>Scenes creation</h3>

<p>The scenes are the most important feature of Phaser Editor. The scene editor (or Canvas Editor) allow the user to create levels with visual tools. The objects of the scene are created by dropping the assets on it, and can be edited in the Property Grid editors.</p>

<p>The scenes are compiled into a readable JavaScript code (or TypeScript code), so it does not require any special or third-party plugin to load the scenes into the game. The editor stage is made with JavaFX, technology that is provided to Eclipse by the <a target="_blank" href="https://www.eclipse.org/efxclipse">e(fx)clipse project</a>. In a future we are going to evaluate a stage made with the JavaFX WebView, so more complex objects can be created like particles, by running a Phaser game instance inside the editor.</p>

<p>There are three type of scenes: State, Sprite and Group. The Sprite and Group scenes are prefabs and can be used in other scenes, which makes easier to reuse objects and create complex games. In addition, the Sprite prefab can be set to different types: Sprite, Button, TileSprite, and Text.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/asset_navigator.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/asset_navigator_sm.png"/></a></p>
<p align="center"><small>Different parts of the scene editor</small></p>

<p>The Property Grid contains some advanced editors, like the animations editor. The animations are edited in a dedicated dialog where the user can create new animations, select the animation's frames and play it.</p>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/animations_editor.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/animations_editor_sm.png"/></a></p>
<p align="center"><small>Animations dialog</small></p>

<p>Learn more about the <a target="_blank" href="http://phasereditor.boniatillo.com/docs/canvas.html">Scene Editor</a> in the Phaser Editor docs.</p>

<h3>JavaScript programming</h3>

<p>Phaser is a JavaScript framework, and the games are coded in this language. In addition are provided the TypeScript definitions of the API, so users can use this language too. Phaser Editor provides code generators for JavaScript 5, JavaScript 6 and TypeScript, however, only JavaScript 5 is supported by the JavaScript editor, that is provided by the <a target="_blank" href="https://www.eclipse.org/webtools/jsdt/">JSDT project</a> of the <a target="_blank" href="https://www.eclipse.org/webtools">Eclipse Web Tools</a>. JavaScript 6 and TypeScript can be edited by installing third party plugins, specially the <a target="_blank" href="http://phasereditor.boniatillo.com/blog/2017/09/how-to-install-typescript-ide-in-phaser-editor-v142">TypeScript IDE</a> from <a target="_blank" href="https://github.com/angelozerr/typescript.java">Angelo Zerr</a>. In future versions of Phaser Editor we are going to include TypeScript IDE as the built-in solution, to replace the current JSDT editor.</p>

<p>The JavaScript editor provides some nice features dedicated to Phaser development:</p>

	<ul>
        <li>Phaser API autocompletion</li>	
        <li>Assets key autocompletion and previsualization</li>
        <li>Assets filename autocompletion and previsualization</li>
        <li>Animations constants autocompletion and previsualization</li>
        <li>Code templates</li>
	</ul>

 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/november/images/phaser_proposals.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/november/images/phaser_proposals_sm.png"/></a></p>
<p align="center"><small>Assets keys proposals</small></p>

<h2>Conclusion</h2>

<p>If you want to start in the game development world, as a hobby or as a career, HTML5 games are simple to start with and have a promising future. Phaser is one of the most popular HTML5 game dev frameworks and Phaser Editor is a tool just made for Phaser, and is growing and maturing. The fact that is made with Eclipse technology allow us to reuse many powerful features and third-party plugins, and enter into the Eclipse community as a serious game development alternative.</p>

<p>For more information on the editor features, take a look to the <a target="_blank" href="http://phasereditor.boniatillo.com/docs">online documentation</a>. In <a target="_blank" href="https://gamedevacademy.org/make-a-mario-style-platformer-with-the-phaser-editor/">this tutorial on the Zenva Academy</a> you can learn how to create a Mario-like game (<a target="_blank" href="http://phasereditor.boniatillo.com/demos/zenva-tuto-platformer-1/final-demo/">play the demo</a>).</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/november/images/arian.png"
        alt="Arian Fornaris" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Arian Fornaris<br />
            <a target="_blank" href="http://phasereditor.boniatillo.com/">Phaser Editor</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/PhaserEditor2D">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.facebook.com/PhaserEditor2D">Facebook</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/PhaserEditor2D/PhaserEditor">GitHub</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>

