<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <p>In a world moving at an ever faster pace, Capella is an open and field-proven approach to address increasing complexity in systems architecture and solve the limitations of traditional engineering practices that are primarily driven by documents.</p>
<p>Launched in 2007 by Thales, this initiative resulted in the definition of Arcadia, an engineering method ensuring effective and rigorous co-engineering, and in the development of the <a target="_blank" href="https://polarsys.org/capella/">Capella</a> modeling workbench.</p> 
<p>In this article, we will explain the challenges that systems engineers face when designing and mastering the architectures for increasingly complex systems. Then we will present the main benefits of a Model-Based Systems Engineering (MBSE) approach to take these challenges. Finally, we will introduce Capella which implements a field-proven MBSE method.</p>
 
 <h2>Exponential increases in complexity</h2>
<p>Across all industry domains, the increasing sophistication of new services and products lead to customers demanding ever more interconnected, ready-to-use, safe, environmentally friendly, and low cost systems.</p>
<p>Many examples of these challenges are clearly seen in domains such as:</p>
	<ul>
        <li>transportation (autonomous vehicles, connected devices, low emission, etc.),</li> 
        <li>energy (grids, efficiency, long-term impacts, etc.),</li>
        <li>aerospace (lower entry costs, delay reductions, etc.).</li>
	</ul> 
	
<p>Similar challenges are easily found in many other domains as well.</p>
   
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/tangle.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/tangle_sm.png"/></a></p>
    
    
<p>The race to continuously deliver more and more value is forcing industries to innovate at an even faster pace.</p>
<p>As new systems rely on more and more components being coupled together, this leads to even more complexity: wider and deeper functional breakdowns, increasing functional dependencies and interfaces, more complex architectural patterns, and increased variability.</p>
<p>Designing and assembling such complex architectures necessarily relies on multidisciplinary activities that require long-term collaboration between numerous experts, in a variety of domains (mechanics, thermic, software, telecommunications, safety, etc.).</p>
<p>The future is already here: disruptive technologies such as the “Internet of Things”, 3D printing, and new materials also change the kind of services that systems deliver as well as the way of developing them.</p>
<p>All of this means that systems engineers have to implement a new approach that will enable them to cope with these exponentially increasing levels of complexity.</p>

<h2>Systems engineering challenges</h2>
<p>The mission of systems engineers is to ensure all the subsystems, equipment, and software work together to achieve the correct objectives. More complexity means that they have to adapt their practices and introduce additional rigor in order to undertake several key challenges.</p>
<p>The first challenge is to understand the system’s objectives. This means understanding customer needs and the value added by the new system. With large and complex systems, the number of requirements can be significant and they are dependent on all the other interconnected systems. In addition, the more components the system has, the harder it is to identify all the emergent properties and how they will contribute to (or harm) the target services provided by the whole system.</p>
<p>It is therefore logical to speak a common language that can be used by different stakeholders to communicate about the many interdependent properties.</p>

        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/treepeople.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/treepeople_sm.png"/></a></p>

<p>Throughout the project's life cycle, from the needs definition phase to the deconstruction of the system, including the design, development, integration, verification, deployment and maintenance phases, systems engineers have to collaborate together, with the different domain experts, as well as with the other stakeholders, including subcontractors and the final customers.</p>
<p>This implies sharing a consolidated and consistent vision of the whole system.</p> 
<p>Once the architectural drivers have been identified and shared, systems architects must focus on the early evaluation of the envisioned solution according to multiple factors (performance, cost, maintainability, mass, durability, etc).</p> 
<p>Being able to evaluate architectures early is crucial in order to find the best architectural compromises.</p>

<h2>The promises of a Model-Based approach</h2>
<p>The objective of a Model-Based approach is to move from a set of heterogeneous office-based documents to a coherent, integrated and computational description of a system.</p>
<p>It consists in using a formal digital format to specify, design, analyze and verify systems. This format (the concepts with their properties and relationships) is defined by a standard language (a metamodel) that enables the implementation of workbenches providing modeling services such as edition, visualization, transformation, comparison, storage, etc.</p>

<p>In a Model-Based Systems Engineering (MBSE) approach, the model describes the system and its components, across the whole spectrum from a business needs perspective, to the physical implementation, including the logical decomposition of the system. To elaborate the model, systems architects can perform functional and structural analyses, derive interfaces and work on dynamic aspects (scenarios, state-machines).</p>
<p>Having both a functional view of the system as well as traceability right up to the physical components ensures the integrity of the system’s description and the full traceability throughout the systems engineering process. This reduces inconsistencies and facilitates assembly by allowing early identification of any incompatibilities between components.</p> 
<p>A MBSE approach can also be associated to a graphical notation, so that the system is visually described by multiple consistent views that can be dedicated to a broad range of specific concerns (performance, security, resilience, cost, risk, etc). These views, which can be graphical or tabular, are synchronized with the model which ensures the consistency of the whole system’s description.</p>
<p>Beyond the facilities offered to create a consistent system, another major benefit of MBSE is the possibility to automate many engineering activities, thanks to the digital format of the models including:</p>

	<ul>
    		<li>consistency validation,</li>
        <li>derivation of architecture alternatives,</li>
        <li>trade-offs evaluation,</li>
        <li>production of deliverables,</li>
        <li>exchanges with other engineering tools,</li>
        <li>reuse of architectures between several projects.</li>
	</ul>


    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/msbe.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/mbse_sm.png"/></a></p>
    
    <p>With MBSE, systems engineers can explore more alternatives faster, address a broad range of multi-disciplinary concerns in a consistent way, and successfully achieve the correct objectives.</p>
    
    
<h2>The Capella solution</h2>
<p>Thales initiated the development of Arcadia and Capella with these challenges in mind, implementing short experimentations loops to capture system engineers’ feedback. With Arcadia being its DNA, the Capella workbench provides methodological guidance, including an engineering workflow with clear modeling objectives, and an advanced set of productivity tools.</p>
    
    
        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_sm.jpg"/></a></p>

 <p>This open MBSE solution is extensible. It can be adapted to specific engineering approaches and integrated with other engineering tools. It can also be enhanced with ready-to-use add-ons to facilitate new project launches, collaboration between remote teams, the generation of documents, etc.</p>
<p>Successfully deployed across various domains in all the Thales Business Units worldwide, Capella is a field-proven solution. It helps to manage systems complexity, primarily during the development phases, but also during the production and operations phases.</p>

<p>As a way of extending its adoption beyond Thales, in 2014 Capella was made open source as part of <a target="_blank" href="http://polarsys.org/">PolarSys</a>, an Eclipse Foundation Working Group that is dedicated to embedded systems development.</p> 
<p>A Capella Industry Consortium has also been created. It gathers MBSE actors looking to collaborate in a vendor neutral way and with open governance. By participating in this consortium, members (companies developing complex systems and software, suppliers providing solutions in addition to Capella, academics) can share their vision on how Capella could evolve and pool their respective investment efforts.</p>

<h2>Conclusion</h2>
<p>Capella is a concrete illustration of an open and field-proven approach that can solve major concrete industrial challenges.</p>
<p>Its creators decided to capitalize on Eclipse Modeling and its ecosystem to implement the adaptable and extensible workbench that could both support the MBSE approach it required and fit the specific needs of the company’s business units.</p>
<p>Capella also benefits from the PolarSys Working Group initiative that allows organizations to combine the best practices of open source development with the possibility to foster industry collaborations and build a strong ecosystem.</p>

<h2>References</h2>
    <ul>
    <li><a target="_blank" href="https://www.polarsys.org/capella/arcadia.html">Arcadia method</a></li>
    <li><a target="_blank" href="https://www.polarsys.org/capella/features.html">Capella MBSE tool</a></li>
    <li><a target="_blank" href="https://www.polarsys.org/capella/community.html#case-studies">Industrial case studies</a></li>
    <li><a target="_blank" href="http://www.incose.org/AboutSE/sevision">Incose SE Vision 2025</a></li> 
	</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/fred.jpg"
        alt="Frederic Madiot" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Frédéric Madiot<br />
            <a target="_blank" href="http://obeo.fr/">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fmadiot">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/fmadiot/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>