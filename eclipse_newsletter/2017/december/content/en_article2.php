<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   

<p>As the state-of-the-art engineering tool vendors did not provide the appropriate offer, Thales decided in 2007 to develop its own Model-Based Systems Engineering (MBSE) method and workbench. This initiative resulted in Arcadia and Capella.</p>
<p>To reduce the complexity and the cost of developing such a custom modeling tool, Thales worked with Obeo to create a new technology that could facilitate the creation and the maintenance of graphical modeling workbenches. This technology, <a href="http://www.eclipse.org/sirius">Eclipse Sirius</a>, is available in open source since 2013.</p>
<p>Completely generic, Sirius leverages the Eclipse modeling technologies, including EMF for the model management and GMF for the graphical representation. It makes it possible to equip teams who have to deal with complex architectures on specific domains.</p>
<p>It is particularly adapted for users that have defined a DSL (Domain Specific Language) and need graphical representations to better elaborate and analyze a system and improve the communication with other team members, partners or customers.</p>
    
    <h2>Capella, a Sirius-based workbench</h2>
<p>Thanks to Sirius, Thales developed Capella, an innovative visual MBSE workbench offering advanced graphical editing capabilities:</p>
<ul>
    <li>specific diagrams dedicated to the kind of representations defined by Arcadia method (dataflow, scenario, functional chain, breakdown, modes & states, data model, component wiring, allocation)</li>
    <li>a consistent colour scheme (all function-related elements are green, and all component-related elements are blue)</li>
    <li>complexity management (display of computed exchanges between functions, automated contextual diagrams, filters to automatically hide or show some elements, etc.)</li>
    <li>model validation and quick fixes organized in categories (Integrity, design, completeness, etc.)</li>
</ul> 
    
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella_filters.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_filters_sm.png"/></a></p>
<p align="center"><small><a target="_blank" href="https://polarsys.org/capella/images/features/filters.png">Capella Filters</a></small></p>

<p>Sirius can also be used, through the Capella Studio environment, to develop additional graphical representations dedicated to specific engineering concerns, called viewpoint:</p>
<ul>
    <li>electrical power systems</li>
    <li>power and thermal performance depending on flight phase consumption</li>
    <li>redundancy rules, failure scenarios and propagation</li>
    <li>reconfiguration issues</li>
    <li>early identification of spatial arrangement constraints impacting the architecture</li>
</ul>

    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/specific_viewpoint.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/specific_viewpoint_sm.png"/></a></p>
<p align="center"><small><a target="_blank" href="https://polarsys.org/capella/images/features/multi_vp.png">Specific Viewpoint</a></small></p>

<h2>How does it work?</h2>
<p>The graphical editors can be easily configured with a minimal technical knowledge.</p>
<p>The Sirius runtime uses a description of the modeling workbench which defines the complete structure of the model editors, their behavior, and all the edition and navigation tools. This description can be seen as a mapping between the DSL concepts (the elements defined by the Ecore model) and the graphical workbench (the diagrams, the tables, the tools, etc).</p>


    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/viewpoint.png"/></p>
<p align="center"><small>The definition of a Sirius modeling workbench</small></p>


    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/diagram_editor_sirius.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/diagram_editor_sirius_sm.png"/></a></p>
<p align="center"><small>A diagram editor created with Sirius</small></p>

<p>In addition, to help the users master the potential complexity of their models, Sirius provides several mechanisms to focus on elements of interest:</p>
<ul>
    <li><b>Conditional styles</b>: change the graphical rendering of objects depending on their properties. For example: changing the background color to red when an attribute exceeds a threshold, varying the size of the shape according to the value of an attribute, etc.</li>
    <li><b>Layers and filters</b>: display or hide elements of the diagram according to specific conditions.</li>
    <li><b>Validation and quick-fixes</b>: the model can be validated by specific rules. The problems are listed on the Problems View and the corresponding objects are graphically marked. For some problems, quick-fixes can be suggested to correct the model.</li>
</ul>

<p>Sirius also provides the notion of "Modeling Project", an integration with the Project Explorer. It handles the synchronization of workspace resources, and in-memory instances. It also manages the sharing of command stacks and editing domains across editors.</p>
<p>The editors’ description is dynamically interpreted to materialize the modeling workbench within the Eclipse IDE.</p> 
<p>No code generation is involved: the editors’ description is dynamically interpreted to materialize the modeling workbench within the Eclipse IDE and the specifier of the workbench receives instant feedback while adapting the description. Once completed, the modeling workbench can be deployed as a standard Eclipse plugin. Thanks to this short feedback loop, a workbench or its specialization can be created in a matter of hours.</p>

<h2>Integration with other technologies</h2>
<p>Sirius integrates with technologies such as <a target="_blank" href="https://www.eclipse.org/emf/compare/">EMF Compare</a> (to compare models), <a target="_blank" href="https://www.eclipse.org/acceleo/">Acceleo</a> (to generate text), or <a href="http://www.m2doc.org/">M2Doc</a> (to generate MS Word documents).</p>
<p>Sirius can also be integrated with other technologies such as <a target="_blank" href="https://www.eclipse.org/Xtext/">Eclipse Xtext</a> to provide <a target="_blank" href="https://www.obeodesigner.com/en/white-paper-xtext-sirius">complementary textual editors</a>. With Xtext, models are loaded and saved using their textual syntax, enabling concurrent use of diagram, table and textual editors.</p> 


    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/sirius_xtext.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/sirius_xtext_sm.png"/></a></p>
<p align="center"><small>Sirius and Xtext editing the same model</small></p>
 
 <p>To enable teams working simultaneously with Sirius-based modeling tools, Obeo has developed <a target="_blank" href="https://www.obeodesigner.com/en/collaborative-features">Obeo Designer Team</a>, an add-on that integrates Sirius with <a href="https://www.eclipse.org/cdo/">Eclipse CDO</a> to allow several users to collaborate live and fluently on the same model, without losing time managing conflicts.</p>
 
 <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/--aF29vPWS8" frameborder="0" allowfullscreen></iframe>
</div>
<p align="center"><small>Obeo Designer Team principles</small></p>

<h2>Sirius ecosystem and roadmap</h2>
<p>Since the creation of Capella, many other modeling workbenches have already been implemented with Sirius in various fields and use-cases. You can find some noteworthy examples on the public <a href="http://www.eclipse.org/sirius/gallery.html">Sirius gallery</a>.</p>
<p>Brand new case-studies were also presented during SiriusCon, last november in Paris. You can discover the presentations here <a target="_blank" href="https://www.siriuscon.org/">https://www.siriuscon.org/</a></p>
<p>At SiriusCon, I also presented Sirius roadmap: from origins, up to what’s actually cooking.</p>


 <p align="center"><a target="_blank" href="https://www.slideshare.net/melbats/siriuscon-2017-sirius-oadmap?ref=https://www.siriuscon.org/"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/roadmap_sirius.png"/></a></p>
 
 <p>If you need additional features to be integrated to Sirius, know that Sirius is continuously evolving according to rich collaborations with the ecosystem, in an Open Innovation mode. It is in this spirit that we invite you to share the needs you would like Sirius to fulfill by completing <a href="http://bit.ly/SiriusCommunitySurvey">this short survey</a>.</p>
 <p>You are welcome on board!</p>
 
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/melanie.jpg"
        alt="Melanie Bats" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Mélanie Bats<br />
            <a target="_blank" href="http://obeo.fr/">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/melaniebats">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="http://melb.enix.org/">Blog</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>