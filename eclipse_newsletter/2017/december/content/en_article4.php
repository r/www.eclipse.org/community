<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>Capella Studio is the IDE (integrated development environment) to develop viewpoints and add-ons for Capella. It is based on Kitalpha which is an IDE for the development of model-based workbenches. Capella Studio integrates Capella in order to access to all the Capella development artefacts (e.g. metamodels, diagrams, code), to enrich and customize them.</p>
   
 <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella_studio.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_studio_sm.png"/></a></p>

    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/architecture_capellas.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/architecture_capellas_sm.png"/></a></p>


<p>Capella Studio offers the capability to create Capella add-ons in a standard way with Java and the Eclipse Modeling Framework (EMF). To develop complex and coherent extensions, Kitalpha implements the notion of viewpoint, in reference to the ISO/IEC-42010 standard. Capella is then enriched with new types of data, diagrams, and services, to address new engineering domains such performance or safety. Capella Studio eases the development of viewpoints with the technique of DSL. A set of complementary textual editors enable to define meta-model extensions, editors (contributions to properties view), diagrams (contributions to Sirius diagram specifications), services (e.g., transformation rules), and process support through an activity explorer. A generation approach hides most of the technical implementation details. In a very short time, a new viewpoint is deployable and seamlessly executable in Capella.</p>
<p>The following add-ons and viewpoints are already downloadable on the <a target="_blank" href="https://polarsys.org/capella/download.html">Capella website</a>.</p>

<h2>System to Subsystem Transition</h2>
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/chart_capellas.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/chart_capellas.png"/></a></p>

<p>This add-on is a System to Subsystem Transition. It initializes a new Capella project from an existing model. This transition extracts suitable model information from a selected Capella source component and ensures top/down propagation of updates.</p>
<p>The automated and iterative transition between system and subsystems is a great help for managing several levels of engineering. The contract and model of the subsystems are computed from the system architecture. Ideally, subsystems stakeholders are involved in co-engineering activities at system level before the transition occurs.</p>

<h2>XHTML Documentation Generation</h2>
<p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/documentation.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/documentation.jpg"/></a></p>

<p>This add-on enables the end-user to generate an HTML website from a Capella project.</p>
<p>Sharing models with all stakeholders is essential in model-based systems engineering. Publishing and sharing HTML versions of models helps make models THE reference of all engineering activities.</p>

<h2>Requirements Viewpoint</h2>
    <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/studio_example.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/studio_example.png"/></a></p>

<p>This add-on provides a means to import a set of requirements from a ReqIF file (Requirement Interchange Format / OMG Standard).</p>
<p>The import is iterative (diff/merge based) and a set of tools is provided to link the model elements to the requirements.</p>

<h2>Basic Viewpoints</h2>
   <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella_chart.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_chart.jpg"/></a></p>

<p>This add-on provides three simple viewpoints which allow annotating a Capella model with some non-functional properties such as “Mass”, “Price” or “Performance”.</p>

<div class="bottomitem">
 <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
			<img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/benoit.jpg"
        alt="Benoit Langlois" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Beno&icirc;t Langlois<br />
            <a target="_blank" href="https://www.thalesgroup.com/en">Thales Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/LangloisBenoit">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/benoit-langlois-52398ab/">LinkedIn</a></li>
          </ul>
        </div>
      </div>
     </div>  
   </div>
   <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
                  <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jean Barata<br />
            <a target="_blank" href="https://www.thalesgroup.com/en">Thales Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/jean-barata-8864a05/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>
