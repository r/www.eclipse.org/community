<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <p>Capella is a model-based engineering solution that leverages the Eclipse platform to implement an adaptable and extensible workbench. Natively supporting an innovative Model-Based Systems Engineering (MBSE) approach based on both industrial experimentations and system engineers’ feedback, it can be customized to fit the specific needs of many industrial domains.</p>
<p>Made open source in 2014, a vibrant ecosystem as grown with Capella: companies developing complex systems and softwares, suppliers providing offers on the top of Capella, academics with a MBSE topic for research need.</p>
<p>Beyond the Capella workbench solution, the members of this new ecosystem identified the need for a place where they could:</p>

<ul>
	<li>foster collaborations,</li>
	<li>share investment and best practices,</li>
	<li>leverage Open Innovation to co-develop new MBSE capabilities.</li>
</ul>
   
   <p>This is precisely the aim of the Eclipse Working Group initiative: to allow organizations to combine the best practices of open source development with the possibility to foster industry collaborations and build a strong ecosystem.</p>
<p>The Capella Industry Consortium (IC) has therefore been created as a new Working Group to develop the Capella ecosystem with vendor-neutral and open governance. The scope of this Working Group focuses on the Capella tool, the add-ons and the underlying technologies.</p>
<p>Five organizations are currently part of  the Capella IC: Thales, Artal, Obeo, PRFC and Tecnalia.</p>

<h2>Goals</h2>
<p>Five main collaboration axis have been identified for Capella IC.</p>

   
    <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capellaic.png"/></p>

<h3>Knowledge sharing</h3>
<p>Members can exchange about their experiences and needs:</p>
<ul>
    <li>Share MBSE experiences and case studies among consortium members to enable broad adoption of Capella and Arcadia in operational projects.</li>
    <li>Establish a technology watch service to help members leverage innovation in Capella and its ecosystem.</li>
    <li>Keep up-to-date with new and noteworthy, future improvements, success stories, and user feedback.</li>
    <li>Organize private exchange workshops between members.</li>
</ul>

<h3>Joint development financing</h3>
<p>Members coordinate investments from consortium members in the different aspects of the overall MBSE solution to reduce development time, risk, and costs, and to maximize ROI.</p>

<h3>Product Management</h3>
<p>Members cooperate on the product roadmap:</p>
<ul>
    <li>Capture, consolidate, and manage requirements from the different consortium members and application domains.</li>
    <li>Define development priorities based on consortium members requirements/needs.</li>
    <li>Discuss the roadmap among users and contributors.</li>
    <li>Operate a transparent process supported by tools.</li>
</ul>

<h3>Promotion</h3>
<p>Members promote the use of Capella as a leading industrial MBSE solution and provide materials to explain the value of Capella and Arcadia to executives and managers.</p>

<h3>Community Development</h3>
<p>Members provide a focal point for organizing collaboration and creating partnerships among suppliers, end-users, and research/academia.</p>

<h2>Membership Classes</h2>

<p>The membership classes of the Capella IC are designed to reflect the varied interests of the members. The membership class of each Capella IC member is confirmed once per year.</p>

<h3>Driver Member</h3>
<p>Driver members want to influence the development of Capella and be an integral part of the group of organizations that govern the development of the open source MBSE solution. They invest a substantial amount of resources to sustain the Capella IC activities and contribute to the development of the open source technologies.</p>

<p>Typical Drivers members are organizations that use MBSE solutions at the core of their development process and who consider Capella as a strategic asset.</p>

<h3>Participant Member</h3>
<p>Participant members participate in the development of the Capella ecosystem. They contribute to the development and adoption of Capella according to their roles, such as the development of viewpoints for tools providers, participation at conferences, or providing requirements or user experiences for end-users.</p>

<h3>Research/Academia Member</h3>
<p>Research/academia members are universities and research organizations that want to participate in the development of the overall MBSE solution based on Capella. Their contributions take the form of addressing various relevant research topics, using Capella for teaching/training purposes, and contributing to the overall development of the ecosystem.</p>

<h2>Links</h2>
<ul>
	<li><a target="_blank" href="https://www.polarsys.org/capella/industry-consortium.html">Capella IC webpage</a></li>
	<li><a target="_blank" href="https://www.eclipse.org/org/workinggroups/capellaic_charter.php">Capella IC charter</a></li>
</ul>

<div class="bottomitem">
 <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/etienne.jpg"
        alt="Etienne Juliot" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Etienne Juliot<br />
            <a target="_blank" href="http://obeo.fr/">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ejuliot">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/etiennejuliot/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
   <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/benoit.jpg"
        alt="Benoit Langlois" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Beno&icirc;t Langlois<br />
            <a target="_blank" href="https://www.thalesgroup.com/en">Thales Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/LangloisBenoit">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/benoit-langlois-52398ab/">LinkedIn</a></li>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>
