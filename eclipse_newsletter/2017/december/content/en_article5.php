<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   
   <p>When <a target="_blank" href="https://polarsys.org/capella/">Capella</a> was made open source in 2014, feedback was collected from early users and adopters. The feedback showed several topics were important to the open source community.</p> 
<p>First and foremost, users wanted to learn how to efficiently collaborate on a single Capella model and minimize configuration and version management problems.</p> 
<p>Second, users wanted to generate their documentation efficiently. How could they import and export excel data, generate HTML reports and rich documents?</p>
<p>In order to meet users’ demands, Obeo and Thales developed a number of Capella add-ons, including two key extensions that are described in this article, Team for Capella and M2Doc.</p> 

<h2>Team for Capella</h2>
<p><b>Team for Capella</b>, a commercial offer proposed by Obeo, is a tool to achieve real-time collaboration on Capella models with fine-grained lock management. Users starting out with Capella, quickly find themselves facing the problem of sharing files between multiple users who are collaborating on the same model. Whatever strategy are used to address the problem they inevitably cost time and effort, as users have to wait for others to complete their work or manage comparison and merge processes.</p>
<p>To remedy this waste of time, Team for Capella provides a shared repository that allows users to simultaneously edit the same model.</p> 
<p>The scope of Team for Capella is to manage collaboration on Capella models. It is not a tool intended to manage versions and configurations, only a tool that supports collaboration.</p>
   
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/team_capella.png"/></p>
 
 <p>The actual collaboration process is quite simple. Once a user modifies a model, a green lock is displayed on the user’s screen indicating that the element is now locked. Simultaneously, other users will see a red lock appear on their screens indicating that somebody else is modifying that element. However, these other users can still modify objects that aren’t locked. When the original user saves the model, other users are instantly notified of the changes and can refresh their diagrams to see the latest changes.</p>
 
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/redlock.png"/></p>
     
     <p>Furthermore, if a user intends to work on a package or a dedicated portion of the model and doesn’t want anyone interfering on it for a period of time, then that user can lock the whole portion of the model and work on it individually.</p>
     
      <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VRN-DcIKfog" frameborder="0" allowfullscreen></iframe>
</div>
     
     <p>Administration tools are also available for administrators to manage locks, back-ups, and so on.</p>
<p>The release date of Team for Capella 1.2 is expected for January 2018 and will be compatible with Capella 1.2. Each year, Obeo releases at least one major and one minor version of Team for Capella.</p>

<h2>M2Doc</h2>
<p>Developed by Obeo, <b>M2Doc</b> is a recent open source document generation solution that works with Capella. It provides Capella users with the ability to generate MS Word™ files from their models.</p> 
<p>Let’s start by asking ourselves why users produce documentation. They do for two main reasons.</p>
<ol>
	<li>First, they need to deliver information to people who will not use Capella models.</li>
	<li>Then, they have to fulfil certain company obligations  that require the production of many documents.</li> 
</ol> 
<p>Document generation can also be useful in many other circumstances, like facilitating model reviews for example.</p>
<p>In any case, the generated documents have to reflect the current state of the model(s) being used so that they remain the sole reference point.</p>
 
 <p>With M2Doc, users can generate the documents they need by applying their own templates to their models. M2Doc users can also apply the same template to different models, as well as different templates to the same model in order to generate different types of documentation, with each template being used to address a particular aspect of the model. Templates and generated documents use the MS Word™ docx (OOXML) format, which is the only format currently supported.</p>
     
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/m2doc.jpg"/></p>
         
         
   <p>Three roles are involved with the documents generation process.</p>
   
   <p>First, the <b>end user</b> simply uses M2Doc to generate documents after having worked on the model. End users may need to enter and maintain “user text” in some generated documents where text cannot be generated by the models. M2Doc supports this kind of <b>incremental generation</b>.</p>
     <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/generate_doc.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/generate_doc_sm.png"/></a></p>
     
     <p>Second, the <b>template author</b> creates M2Doc templates in the OOXML format (Office Open XML), and for that he will use AQL (Acceleo Query Language) services. This role requires some knowledge of Capella metamodels. The template author also has to test and debug templates before making them available to end users. M2Doc supports this work and helps with developing templates by providing accurate and relevant error detection.</p>
             
     <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/interface.png"/></p>
     
     <p>Finally, a Capella expert may sometimes be needed to provide reusable AQL services to template authors. These services can capture complex navigations and logic, and facilitate the creation of robust templates. Capella Experts require advanced knowledge of Capella metamodels as well as a good working knowledge of java development. No prior knowledge of AQL syntax is required to create AQL services.</p>
<p>M2Doc is an easy and smooth user experience for both the end user and the template author. Its syntax is quite simple and the tool provides a lot of support in terms of template creation. Maintenance is efficient and it includes powerful and reliable error reporting. Errors are accurately indicated in the file at the location of the error. Last but not least, M2Doc offers high production performance, generating hundreds of pages of documentation in just a few seconds.</p>

<p>The latest version of M2Doc 1.0 is available on its <a target="_blank" href="www.m2doc.org">new official website</a>, and is compatible with the current Capella 1.1.2 as well as the next 1.2 release.</p>
     
      <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/v0WcLpi6a0U" frameborder="0" allowfullscreen></iframe>
</div>

<h2>Other add-ons</h2>
<p>As explained in the introduction above, other add-ons exist to cover other user needs, including:</p>
<ul>
<li>The <b>Requirements</b> add-on, which enables the import of a set of requirements from a ReqIF file.</li> 
<li>The <b>System to subsystem transition</b> add-on, which initializes a new Capella project from an existing model. This transition extracts suitable model information from a selected Capella Component and manages top/down update propagation. The automated and iterative transition between system and subsystems is a great help for managing several levels of engineering. The contract and model of the subsystems are computed from the system. Ideally, subsystems stakeholders are involved in co-engineering activities at system level before the transition occurs.</li>
<li>The <b>XHTML Document Generation</b> add-on enables the generation of an HTML website from a Capella project. Sharing models with all stakeholders is essential in model-based systems engineering. Publishing and sharing HTML versions of models helps make models the reference in all engineering activities.</li>
</ul>

<h2>Links</h2>
<ul>
<li><a target="_blank" href="https://www.polarsys.org/capella/">Capella website</a></li>
<li><a target="_blank" href="https://www.obeo.fr/en/capella-professional-offer#collaboration">Team for Capella</a></li>
<li><a target="_blank" href="http://www.m2doc.org/">M2Doc</a></li>
</ul>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/laurent.jpeg"
        alt="Laurent Delaigue" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Laurent Delaigue<br />
            <a target="_blank" href="http://obeo.fr/">Obeo</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/laurentdelaigue">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/laurent-delaigue-aa79a0146/">LinkedIn</a></li>
           <?php //echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>