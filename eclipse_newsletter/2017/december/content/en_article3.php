<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
    
<p><a target="_blank" href="https://polarsys.org/capella/">Arcadia and Capella</a> are two distinct solutions that work together for the implementation of model-based systems engineering (MBSE). As illustrated in Figure 1, Arcadia (which stands for <b>ARC</b>hitecture <b>A</b>nd <b>D</b>esign <b>I</b>ntegrated <b>A</b>pproach) covers the methodology and the high-level conceptual ontology and viewpoints.</p> 
<p>Capella elaborates on these with more detail to provide a comprehensive set of diagrams and notation elements. Arcadia may be implemented with other tools, but Capella has been purposely-built to provide the notation and diagrams needed to create models that exactly fit with Arcadia’s approach.</p> 

        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/arcadia_capella.png"/></p>
<p align="center"><small>Figure 1 - Scope split between Arcadia and Capella</small></p>

<h2>Arcadia principles and modeling language</h2>
<p>The Arcadia definition is driven by a few structuring principles:</p>
<ul>
    <li>Extended functional analysis to define both need and solution behaviour;</li> 
    <li>Separation of need analysis and solution architecture definition;</li> 
    <li>Separation of operational need analysis and definition of system contribution to this need (system need analysis);</li> 
    <li>Separation of functional/behavioural description, and structural decomposition;</li> 
    <li>Differentiation between the structuring of behavioural/logical components and physical hosting components.</li> 
</ul>
<p>These principles favour separation of concerns, so as to adapt to different life cycles, provides capabilities for impact analysis, and allows efficient management of architecture alternatives, reuse, etc.</p>

<h2>Major concepts of the Arcadia method and framework</h2>
<p>The essentials of Arcadia are summarized in the following <a target="_blank" href="http://polarsys.org/capella/resources/Datasheet_Arcadia.pdf">PDF document</a>.</p>

<p>Arcadia architectural descriptions rely on very common concepts: functional analysis, structural analysis, interfaces, and behaviour modeling. However, these concepts are refined in five engineering perspectives structuring the model according to major engineering activities and concerns, each one dealing with specific engineering issues and outputs. These are listed below.</p>

        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/arcadia_pespective.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/arcadia_pespective_sm.png"/></a></p>
<p align="center"><small>Figure 2 - The five Arcadia perspectives and their architectural concerns</small></p>

<h2>Capella</h2>
<p>The scope and target of a method have direct consequences on the associated tooling. Arcadia does not cover the full spectrum of design activities: its focus is primarily on architectural design, excluding, for example, low-level behavioural modeling or simulation. The original audience for the Arcadia/Capella solution was primarily systems engineers with diverse backgrounds and skills.</p> 
 <p>At one end of the spectrum, standard or universal languages such as SysML target a wide variety of domains and modeling intentions. At the other end, specialized modeling languages have reduced coverage and more focused intentions, as they are intended to provide solutions for particular domains. These characteristics make them more likely to provide richer semantics and enhanced formalism.</p>
 
 <p>Capella is neither a SysML profile nor a domain-specific language (DSL). The core meta-model of the Capella notation has been strongly inspired by SysML and the diagrams provided are very similar. However, when considering the SysML language as a reference, the meta-model of Capella is simultaneously simplified, modified and enriched.</p>
<ul>
    <li>Simplified or modified: whenever SysML concepts were more complex than necessary to model architectures, they were either excluded (many low-level behaviour modeling constructs are absent) or simplified (components, parts, instances);</li>
    <li>Enriched: Arcadia implements an architectural framework, where description languages such as SysML do not; the Capella tool implements this framework in its meta-model.</li>
</ul> 

<p>Capella is an original solution in the landscape of modeling workbenches for several reasons including the tight coupling between the method and the tool, the availability of multiple productivity tools, the artefacts allowing to master design complexity and the fact that it is an open source solution.</p>

<h2>Tight coupling between the method and the tool</h2>
<p>Its tight coupling with the Arcadia method is one of the key aspects of Capella. In addition to having its concepts directly aligned on the Arcadia ones, three features strongly enforce the implementation of the method in Capella models, as illustrated below.</p>
<p>1. All projects are initialized with a model structure which reflects the Arcadia engineering perspectives, as illustrated in Figure 3.</p>


        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_projects.png"/></p>
<p align="center"><small>Figure 3 - Capella default project structure</small></p>

<p>2. The graphical aspect of elements in all edition view and diagrams is aligned with the Arcadia ones. For example, green is dedicated to functional analysis, blue is dedicated to structural elements and red is dedicated to interfaces (as illustrated in Figure 4). This enforced colouring policy greatly contributes to the readability of diagrams for readers having little modeling expertise;</p>

        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella_architectures.jpg"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_architectures_sm.jpg"/></a></p>
<p align="center"><small>Figure 4 – Example of Capella architecture diagram</small></p>

<p>3. A method explorer is the key interaction interface for Capella models, as illustrated in Figure 5. The customizable explorer lists all major modeling activities for each perspective, proposes shortcuts to the most suitable graphical representations, and provides an index for all existing diagrams for each activity. This explorer is of course a great help for beginners, eliminating the blank page syndrome. But beyond that, it is a powerful tool to navigate in Capella models.</p>

        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/user_interface.png"/></p>
<p align="center"><small>Figure 5 - Capella screenshot indicating features of the user interface</small></p>

 <h2>Productivity tools and complexity mastering</h2>
<p>Productivity or automation tools not only accelerate day-to-day modeling activities. Together with model validation (Figure 6), they also improve the consistency and correctness of models by reducing human mistakes. Other productivity tools of Capella include automated and iterative model transitions from one Arcadia perspective to another, brushing of layouts between diagrams, automated and iterative transition between system and subsystem modeling, generation of interfaces, etc.</p>


        <p align="center"><a target="_blank" href="/community/eclipse_newsletter/2017/december/images/capella_features.png"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/capella_features_sm.png"/></a></p>
<p align="center"><small>Figure 6 - Capella screenshot indicating features of the user interface</small></p>

<p>In addition to bringing the required rigour in engineering practices, one of the main rationales for the deployment of model-based systems engineering is to be able to cope with the growing complexity of systems. It is mandatory for a modeling tool to provide concrete help to master this complexity.</p>
 
<p>This starts with reducing the incidental complexity. By simplifying the underlying modeling concepts (when compared to SysML for example), Capella minimizes the learning curve and improves the readability of models. While this is necessary, it is not sufficient and providing mechanisms to concretely help visualize and navigate models is essential. Capella features powerful means to automatically compute and maintain graphical simplifications of the architecture, not only providing end-users with very valuable views but also allowing them to implement multiple workflows seamlessly (top-down, bottom-up, etc.).</p>
  
<h2>Scalability and applicability</h2>
<p>While being important, the usage of a specific solution to implement is only an aspect of model-based systems engineering implementation. Definition and monitoring of modeling goals (or more precisely, of engineering goals to achieve through a modeling approach) is a key success factor.</p>  
<p>Arcadia can be used to implement a fully model-based systems engineering process, but it can also add value to more traditional processes through careful scaling of the tasks undertaken and the models created. Pragmatic solutions often prove their value by assisting engineering teams in formalizing system requirements for areas at risk. Getting rid of a pain is often a stronger motivation than pursuing benefits.</p> 
<p>Model-based systems engineering is not only about modeling architectures. It interleaves model artefacts with traditional requirements-based artefacts, as shown in Figure 7. This allows engineering teams to continue using established processes, but with model content to enhance existing documents, where they add value.</p>

        <p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2017/december/images/systems_engineering.png"/></p>
<p align="center"><small>Figure 7 - Model-supported systems engineering</small></p>

<h2>Summary</h2> 
<p>The journey towards model-based systems engineering is not straightforward. To equip engineering teams with the know-how and practical experience to implement such a paradigm shift is a major challenge.</p>
<p>By developing process-based tools and techniques, the Arcadia and Capella teams have put into the hands of the engineering community a comprehensive toolkit with which to model their problem space and system architecture, whilst reducing the need for specialized training and years of theoretical learning. The open source approach has seen collaboration between organizations and the development of a thriving ecosystem of tools.</p>

<h2>Links</h2>
<ul>
    <li><a target="_blank" href="http://polarsys.org/capella/download.html">Download and try the latest Capella version</a></li>
    <li>Ask questions in the <a target="_blank" href="https://polarsys.org/forums/index.php/f/13/">Capella forum</a> or the <a target="_blank" href="https://polarsys.org/forums/index.php/f/12/">Arcadia forum</a> on Polarsys website</li>
    <li><a target="_blank" href="http://polarsys.org/capella/community.html">Send a specific request to the Capella Industry Consortium</a></li>
    <li><a target="_blank" href="http://www.ppi-int.com/systems-engineering/SYEN11">Original article</a></li>
    <li>Get more information, download returns of experience (ArianeGroup, Continental Automotive and Areva NP) and tutorials on the <a target="_blank" href="http://polarsys.org/capella/index.html">Capella website</a></li>
</ul>

<div class="bottomitem">
 <h3>About the Authors</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/stephane.jpg"
        alt="Stephane Bonnet" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            St&eacute;phane Bonnet<br />
            <a target="_blank" href="https://www.thalesgroup.com/">Thales Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/capella_arcadia">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/groups/8605600">LinkedIn</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
     <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/joesilmon.jpg"
        alt="Joe Silmon" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Joe Silmon<br />
            <a target="_blank" href="https://www.thalesgroup.com/">Thales Group</a>
          </p>
        </div>
      </div>
     </div>  
     <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2017/december/images/jeanlucvoirin.jpg"
        alt="Jean-Luc Voirin" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Jean-Luc Voirin<br />
            <a target="_blank" href="https://www.thalesgroup.com/">Thales Group</a>
          </p>
        </div>
      </div>
     </div>  
   </div>
   </div>
   </div>
</div>