<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.29.03</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2019/january/images/me.png" />
                            <h3>Editor's Note</h3>
                             <p>The <a href="https://marketplace.eclipse.org/">Eclipse Marketplace</a> is home to a huge variety of solutions from the Eclipse ecosystem. The Marketplace has enjoyed extraordinary success over the years and continues to be "the" place to find great plugins to make the Eclipse IDE experience your own. Our authors in this issue highlight some reasons why our community loves the Marketplace, the reasons why the Marketplace is good for the Eclipse user community, and explore an IDE extension that helps you detect and fix quality issues as you code. We currently have almost 34 million solutions installed directly from the Eclipse IDE. Explore the great articles below, but also be sure to check out the Marketplace to explore the coolest and most functional plugins for yourself!!</p>
                            <p></p>
								<p> &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/goodness.php">Eclipse Marketplace - Goodness for All</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/sonarlint.php">SonarLint - Fix Issues Before They Exist</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/love_marketplace.php">Why I Love the Eclipse Marketplace</a><br>
                            <p>We are proud to announce that we are holding a 2019 <a href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2019/">Eclipse IoT Day in Santa Clara</a>, co-located with <a href="http://bit.ly/2OkIp4P">IoT World</a>, the leading global IoT conference and exhibition. Check out our speakers, talks, agenda, and register to learn the latest on open source and open standards for building IoT solutions.</p>
                            <p>The <a href="https://blogs.eclipse.org/post/jameka-woodberry/thats-wrap-folks-thank-you-participating-our-2019-iot-developer-survey">IoT</a> & <a href="https://blogs.eclipse.org/post/thabang-mashologu/thank-you-taking-jakarta-ee-2019-developer-survey">Jakarta EE</a> Developer Surveys have closed! We look forward to sharing the results of both surveys in April!</p>
                            <p>Stay awesome and happy reading!</p>
                            <p>Jameka Woodberry</p>
                            <p><a style="color:#000;" target="_blank" href="https://twitter.com/marketing_jamy">@Marketing_Jamy</a></p>

                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

  <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/goodness.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/todd_front.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/goodness.php"><h3>Eclipse Marketplace - Goodness for All</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/sonarlint.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/sonarlint_front.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/sonarlint.php"><h3>SonarLint - Fix Issues Before They Exist</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="center" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/love_marketplace.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/love.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/march/love_marketplace.php"><h3>Why I Love the Eclipse Marketplace</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-dartboard">Eclipse Dartboard</a> provides development tools for Dartboard and Flutter in the Eclipse IDE. It is s a recent programming language. Originally proposed by a team at Google in 2010, its main purposes were to be a flexible, but structured language for the web. Its syntax was (and still is) very close to other languages (namely Java or C#) which makes it easy for newcomers to start using Dartboard.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-kiso">Eclipse Kiso</a> was designed with the idea that it can be adapted easily to most IoT devices, with generic device drivers and network stack. It enables fast development cycle of connecting IoT devices to network, hence fast time to market of products.</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4e">Eclipse LSP4E</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.wildwebdeveloper">Eclipse Wild Web Developer</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.linuxtools">Eclipse Linux Tools</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jgit">Eclipse JGit: Java implementation of Git</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egit">Eclipse EGit: Git Integration for Eclipse</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/webtools">Eclipse Web Tools Platform Project</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.papyrus">Eclipse Papyrus</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.scout">Eclipse Scout</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2019/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/iot-day-santa-clara.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://bit.ly/2OkIp4P"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/iot_world.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                            <p><a target="_blank" style="color:#000;" href="https://www.devoxx.fr/">Devoxx France</a><br>
                           April 17-19, 2019 | Paris, France
                           <p><a target="_blank" style="color:#000;" href="https://jax.de/">JAX 2019</a><br>
                           May 6-10, 2019 | Mainz, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.redhat.com/it/summit/2019">Red Hat Summit</a><br>
                           May 7-9, 2019 | Boston, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.redhat.com/it/summit/2019">Bosch Connected World</a><br>
                           May 15-16, 2019 | Berlin, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->