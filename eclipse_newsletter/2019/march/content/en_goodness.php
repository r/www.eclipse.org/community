<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <br>
   <p>2019 marks the 15th year of the Eclipse Marketplace. Who knew? Probably almost no one since most of you hasn't been at Eclipse that long. While there may be a number of you that remember the <a href="https://ianskerrett.wordpress.com/2009/12/08/eclipse-marketplace-is-now-live/"> announcement at the end of 2009</a> regarding the Foundation's takeover of the marketplace, it actually existed as EPIC for five years before that!</p>
	<p>So what is this marketplace and why does it matter?</p>
	<p>The benefits of having a marketplace must be staggering since every technology platform now has something similar. But what are they? And who is benefitting? 15 years seems like a good time to think about that once again.</p>
	<br>
	<h4> Why the Marketplace is good for the Eclipse user community</h4>
	<p><b>Enables new features!</b> This is the fundamental reason for the marketplace as it helps users find key additions. It even includes support for 'long-tail' technology stacks - ones that won't be "most installed" but users still want to find tools for, say Vue.js in Eclipse.</p>
	<p> <a href="https://en.wikipedia.org/wiki/The_Wisdom_of_Crowds">Wisdom of crowds</a> moves the best plug-ins towards the top of the "Most Installed" list and helps showcase the solutions that the community finds most valuable. For many, this can provide a helpful stand-in as a "quality" metric. For example, Subversive and Subclipse have been dueling in the top ten list for years with their different support of the Subversion version control system. Does it matter which is better? Nope! Let users choose what is best for them.</p>
	<p><b>Easier to upgrade!</b> to a new Eclipse release or installation using "My Marketplace > Favorites" to install all your favorites at once! You can even share the link to your favorites with others.</p>
	<br>
	<h4> Why the Marketplace is good for the Eclipse Foundation and its members</h4>
	<p><b> A venue to showcase products</b> is important to let the Eclipse ecosystem grow. Users need to be able to find and evaluate solutions for solutions to succeed! No doubt the marketplace is important to us here at Genuitec.</p>
	<p><b> Permits fast addition of new capabilities</b> letting developers try something new and see if there is interest like we did with our DevStyle plug-in or allow a company to release a major re-architecture of tooling like the very popular Spring Tools 4 release.</p>
	<p><b> Enables a path to contribution</b> by allowing long-time users to engage at a small level with a plug-in for what motivates them and grow as their interests align into potential Eclipse projects.</p>
	<br>
	<h4>Meet the New Boss, Same as the Old Boss...</h4>
	<p>In fifteen years you'd likely expect a lot of change, and there has been. Although the number of plug-ins has been in the same general range (1000-2000) over many years, thousands more have come and gone over that time as the Eclipse projects evolved to add features or the original plug-in authors moved on for some reason. But even in all this churning, we can discern that there are a few timeless rules that plug-ins have followed to become long-term, popular additions to Eclipse.</p>
	<p><b>Make It Work.</b> There are a number of plug-ins that make Eclipse work with platforms or tools that were important years ago and remain so. Popular long-lived examples in this category are plug-ins that support the <a href="https://marketplace.eclipse.org/search/site/Spring">Spring Framework</a> and the <a href="https://marketplace.eclipse.org/search/site/subversion">Subversion</a> source control system.</p>
	<p>But the plug-ins that support long-tail technologies, which are not immensely popular but are interesting and beloved, fall into this category as well. And it is in these plug-ins that we often see the year-over-year churn as new technologies are invented, supported, tested and perhaps eventually abandoned for another even newer technology.</p>
	<p><b>Make It Right.</b> The categories of code linters, testers and validators have always been both large and popular with Eclipse developers because, in the end, we just want our software to work.</p>
	<p><b>Make It Pretty.</b> Pretty might include Eclipse itself, the code you're looking at, or your user's experience with your application. But in all these instances appearance matters and it has always mattered to the plug-in developers in the marketplace. That's why plug-ins that add support for new Eclipse <a href="https://marketplace.eclipse.org/search/site/theme">themes,</a> <a href="https://marketplace.eclipse.org/search/site/formatter">code formatters</a>, and <a href="https://marketplace.eclipse.org/search/site/angular%2520react%2520vue">web frameworks</a> have remained popular over time.</p>
	<p>Just to see how well these rules hold, here's a list of the top-ten marketplace entries with their monthly installation counts (as I'm writing this in late March 2019), along with an indicator of its type: Work, Right, or Pretty.</p>
	<br>
	<table class="table table-bordered">
		  <thead>
		    <tr>
		      <th scope="col">Title</th>
		      <th scope="col">Rank</th>
		      <th scope="col">Count</th>
		      <th scope="col">Type</th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/darkest-dark-theme-devstyle">Darkest Dark Theme with DevStyle</a></th>
		      <td>1</td>
		      <td>42,361</td>
		      <td>Pretty</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/subclipse">Subclipse</a></th>
		      <td>2</td>
		      <td>30,861</td>
		      <td>Work</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/spring-tools-4-spring-boot-aka-spring-tool-suite-4">Spring Tools 4 - for Spring Boot (aka Spring Tool Suite 4)</a></th>
		      <td>3</td>
		      <td>28,720</td>
		      <td>Work, Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/codemix-3">CodeMix 3</a></th>
		      <td>4</td>
		      <td>24,146</td>
		      <td>Work, Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/spring-tools-3-add-aka-spring-tool-suite-3">Spring Tools 3 Add-On (aka Spring Tool Suite 3)</a></th>
		      <td>5</td>
		      <td>20,878</td>
		      <td>Work, Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/pydev-python-ide-eclipse">PyDev - Python IDE for Eclipse</a></th>
		      <td>6</td>
		      <td>20,433</td>
		      <td>Work, Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/sonarlint">SonarLint</a></th>
		      <td>7</td>
		      <td>18,775</td>
		      <td>Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/enhanced-class-decompiler">Enhanced Class Decompiler</a></th>
		      <td>8</td>
		      <td>17,949</td>
		      <td>Work</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/testng-eclipse">TestNG for Eclipse</a></th>
		      <td>9</td>
		      <td>15,624</td>
		      <td>Right</td>
		    </tr>
		    <tr>
		      <th scope="row"><a href="https://marketplace.eclipse.org/content/subversive-svn-team-provider">Subversive - SVN Team Provider</a></th>
		      <td>10</td>
		      <td>14,195</td>
		      <td>Work</td>
		    </tr>
		  </tbody>
		</table>
	<br>
	<p>One final lesson we can learn from the marketplace is the lesson of value-based longevity. The marketplace was created out of need shortly after the foundation itself, and its current lifespan (and the <a href="https://en.wikipedia.org/wiki/Lindy_effect">Lindy Effect</a>) indicate that it will endure just as long as the Eclipse Foundation does.</p>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/march/images/todd220.jpg"
        alt="Timothy Webb" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Todd Williams<br />
            <a target="_blank" href="https://www.genuitec.com/">Genuitec,LLC</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/toddewilliams">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.genuitec.com/community/blog/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>