<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Let me count the ways:</p>
	<ol>
		<li><b>Saves Time</b>: Saves me from futile web searches for Eclipse-specific plugins.</li>
		<br>
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/march/images/time.png"></p>
		<br>
		<li><b>Benefit from the Wisdom of the Crowd</b>: Plugin popularity is very useful when having to decide between different plugins serving the same purpose.</li>
		<br>
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/march/images/crowd.png"></p>
		<br>
		<li><b>Easy to share</b>: from the Marketplace Client, I can see what plugins I have installed, and visit the corresponding Marketplace webpage. Send that URL to a friend and they can install it too. Much better than scrambling to find the corresponding p2 site.</li>
		<br>
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/march/images/share.png"></p>
		<br>
		<li><b>Provisioning</b>: Setting up a new Eclipse install in a VM is easy since my favorites are saved to the Eclipse Foundation servers (via the Eclipse User Storage Service).</li>
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 50%" src="/community/eclipse_newsletter/2019/march/images/provision.png"></p>
		<br>
		<li><b>Discovery</b>: I've discovered several new plugins through its highlighted plugins.
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/march/images/discovery.png"></p>
		<br>
		<li><b>Analytics</b>: As a publisher, it provides analytics on monthly installations and a simplified installation process especially for Eclipse version-specific add-ons.</li>
		<br>
		<p align="left"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/march/images/analytics.png"></p>
		<br>
	</ol>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/march/images/brian.png"
        alt="Brian de Alwis" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Brian de Alwis<br />
            <a target="_blank" href="http://manumitting.com/people/bsd/">Manumitting Technologies</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/delaweeza">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>