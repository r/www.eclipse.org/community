<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<br>
	<p align="center"><img class="img-responsive" style="max-width: 70%" src="/community/eclipse_newsletter/2019/march/images/sonarlint.png"></p>
	<p><a href="https://www.sonarlint.org/?utm_source=ecl_news&utm_content=product">SonarLint</a> is an IDE extension that helps you detect and fix quality issues as you write code. Like a spell checker, SonarLint squiggles flaws so that they can be fixed before committing code.</p>
	<br>
	<h4>Why it matters</h4>
	<p>Code Quality is an integral part of any software pipeline nowadays. It's about preventing bugs from impacting end users, preventing security vulnerabilities from making it to the open world, and also easing the maintainability of your code. Static Code Analysis plays an essential role here.</p>
	<p>Static code analysis typically happens as part of a Continuous Integration (CI) pipeline. All standard CI engines (e.g. Jenkins, Travis CI, Azure DevOps etc.) allow for many different build/test/analysis tools to be part of the pipeline. But that means the code must be committed into the repository and submitted to the CI server before it can be analysed.</p>
	<p>At SonarSource, we've been writing code analyzers for more than a decade. And along the journey of offering CI-friendly tools (SonarQube and SonarCloud, enabling Continuous Code Quality across more than 25 languages), we rapidly wondered: what if we could provide code quality feedback to developers earlier in the process? We envisioned a spell-checker type tool that would instantaneously report quality issues when you write code! That's how SonarLint was born.</p>
	<br>
	<h4>SonarLint to the rescue</h4>
	<p><a href="https://www.sonarlint.org/?utm_source=ecl_news&utm_content=product">SonarLint</a> is an IDE extension that helps you detect and fix quality issues as you write code. It is open source, totally free and supports multiple IDE flavors. For Eclipse, you can get it directly from the Eclipse Marketplace, and it will then detect new bugs and quality issues as you code (in Java, JavaScript, PHP and Python).</p>
	<p>Getting started with SonarLint in Eclipse is very simple: you install it from the <a href="https://marketplace.eclipse.org/content/sonarlint">Eclipse Marketplace</a>, keep on coding, and SonarLint will let you know whenever it sees a bug/vulnerability in the file being edited.</p>
	<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.sonarlint.org/eclipse/eclipse.mp4" allowfullscreen></iframe>
	</div>
	<br>
	<p>SonarLint provides a fully integrated experience. When an issue is found, it is reported and explained in-line:</p>
	<p align="center"><img class="img-responsive" style="max-width: 100%" src="/community/eclipse_newsletter/2019/march/images/sonarlint_2.png"></p>
	<br>
	<p>A dedicated view also gives you the big picture on all issues in the file::</p>
	<p align="center"><img class="img-responsive" style="max-width: 100%" src="/community/eclipse_newsletter/2019/march/images/sonarlint_3.png"></p>
	<p>And in case you wish to understand more about the rule being violated, detailed documentation is available right in Eclipse. In fact, let's take a closer look at how SonarLint can really serve as a great learning tool to discover coding best practices.</p>
	<br>
	<h4>Learn from your mistakes</h4>
	<p>Over ten years of building code analyzers, we've developed a solid quality model split between 3 domains:</p>
	<ul>
		<li>Reliability: avoiding bugs and undefined behavior</li>
		<li>Security: avoid vulnerabilities, breaches and attacks</li>
		<li>Maintainability: Ease code updates and increase developer velocity</li>
	</ul>
	<p>When SonarLint reports an issue, it will always tell you if it's a bug (reliability), a vulnerability (security) or a code smell (maintainability). This allows you to rapidly understand the risks involved, and provides a true learning opportunity with the rule description:</p>
	<p align="center"><img class="img-responsive" style="max-width: 100%" src="/community/eclipse_newsletter/2019/march/images/sonarlint_4.png"></p>
	<p>The content there is a constant opportunity to learn more about common coding pitfalls along with tricky issues that you've possibly never considered. Each rule comes with its own detailed description, examples and even references. You'll often have fun digging into the specifics of an issue.</p>
	<p>To top it all off, SonarLint provides Issues Locations when needed: guiding you through the different steps and different data manipulation, that lead to a bug.</p>
	<br>
	<p align="center"><img class="img-responsive" style="max-width: 100%" src="/community/eclipse_newsletter/2019/march/images/sonarlint_5.png"></p>
	<p>Such in-code insights, together with rich rule descriptions, let you gain a profound understanding of how your code might behave, while continuously improving your coding skills.</p>
	<br>
	<h4>The start of a journey</h4>
	<p>There's much more to say about SonarLint, and this post is just a starting point. It's the start of a Continuous Code Quality journey, where you'll discover how static code analysis can be simple and yet powerful in its positive impact and learning opportunity.</p>
	<p>Throughout that journey you'll also discover that SonarLint offers additional features to always stay in control (e.g. configuring active rules, excluding files), and also to share the good vibes with your team (<a href="https://www.sonarlint.org/bring-your-team-on-board?utm_source=ecl_news&utm_content=team">Connecting with SonarQube or SonarCloud</a>, to share a common team definition of code quality and expand it to more coding languages and setups).</p>
	<p>We're confident you'll fully enjoy the ride, and by all means give us feedback! Our products are <a href="https://www.sonarsource.com/why-us/open-source/?utm_source=ecl_news&utm_content=products">open</a>, our static analysis rules are <a href="https://rules.sonarsource.com/?utm_source=ecl_news&utm_content=rules">open</a>, and our community is open:<a href="https://community.sonarsource.com"> community.sonarsource.com</a>.</p>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/march/images/nicolas.png"
        alt="Nicolas Bontoux" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Nicolas Bontoux<br />
            <a target="_blank" href="https://www.sonarsource.com/">Sonarsource</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/nicoallgood">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.sonarsource.com">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>