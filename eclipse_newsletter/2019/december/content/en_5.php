<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Automating Program Repair</h2>
<p>
  <a href="https://projects.eclipse.org/projects/technology.repairnator">Eclipse Repairnator</a> is
  a bot for automated program repair. It constantly monitors software bugs discovered during
  continuous integration (CI) and tries to fix them. If it succeeds in synthesizing a valid patch,
  Repairnator proposes the patch to the development team as a pull request.
</p>
<h3>From Research to Repair Bots</h3>
<p>A decade ago, a new research field called &ldquo;automated program repair&rdquo; was
  invented. The main idea is that some bugs can be automatically corrected. For example, the
  following patch, which can be automatically generated, fixes an if-statement with an incorrect
  condition.</p>
<p>- if (x &lt; 10)</p>
<p>+ if (x &lt;= 10)</p>
<p>  foo();</p>
<p>A program repair bot is an artificial agent that tries to synthesize source code
  patches. It analyzes bugs and produces patches to help human developers with software maintenance.
  The vision is that program repair bots can partially replace human developers for bug-fixing
  tasks. </p>
<h3>Automated Repair and Continuous Integration</h3>
<p>With CI, a server compiles code and runs all tests for each commit made in the version
  control system for a software project &mdash; Git, for example. In CI terms, there is a build for
  each commit.</p>
<p>A build contains the:</p>
<ul>
  <li>Information about the source code snapshot used, such as a reference to a Git commit</li>
  <li>Results of compilation and test execution &mdash; fail or success</li>
  <li>Execution trace log</li>
</ul>
<p>A build fails if compilation fails, or if at least one test case fails. Approximately
  one out of four builds fails, and test failure is the most common cause. So, it makes sense to
  automatically fix CI build failures.</p>
<h3>How Repairnator Works</h3>
<p>In 2017-2018, we designed, implemented, and started using Repairnator. Repairnator
  specializes in repairing build failures during CI, focusing on failures due to broken tests.
  Figure 1 provides an overview of how the Repairnator bot works.</p>
<p>
  <strong>Figure 1: The Repairnator workflow</strong>
</p>
<p><img src="images/5_1.png"/></p>
<p>The primary inputs of Repairnator are continuous integration builds, triggered by
  developer commits in GitHub projects. This is shown in (a) and (b) in Figure 1.</p>
<p>The outputs of Repairnator are two-fold. It:</p>
<ul>
  <li>Automatically produces patches for repairing builds, if any (g)</li>
  <li>Collects valuable data for future program repair research (h) and (k)</li>
</ul>
<p>Repairnator monitors all continuous activity from GitHub projects (c). </p>
<p>The CI builds are provided as input to a three-stage pipeline:</p>
<ul>
  <li>Collecting and analyzing CI build logs (e)</li>
  <li>Locally reproducing the build failures that occurred during CI (f)</li>
  <li>Running different program repair prototypes from the latest academic research</li>
</ul>
<p>When a patch is found, a bug fix is made in the form of a pull request. Developers then
  follow their usual process of bug fix code reviews and eventually merge the Repairnator patches
  into the main build.</p>
<h3>Where We&rsquo;re Headed</h3>
<p>
  As a next step, we&rsquo;re aiming to take Repairnator out of academia. To achieve this goal,
  we&rsquo;ve created the<a href="https://projects.eclipse.org/projects/technology.repairnator">
    Eclipse Repairnator</a> project.
</p>
<p>The goal of Eclipse Repairnator is to be the open source reference platform for
  automated repair. All kinds of repair will be considered, including test failure repair,
  compilation error repair, and static warning repair.</p>
<h3>Get More Information</h3>
<p>
  Eclipse Repairnator is a welcoming and original community, rooted in academia and industry. We
  warmly welcome everyone who would like to join the community. You can say hello<a
    href="https://github.com/eclipse/repairnator/issues/798"
  > here</a>.
</p>
<p>You can also refer to the following resources:</p>
<ul>
  <li><a href="https://hal.inria.fr/hal-01691496/file/SEIP_63_Camera-Ready-no-copyright.pdf">How to
      Design a Program Repair Bot? Insights from the Repairnator Project</a></li>
  <li><a href="https://ubiquity.acm.org/article.cfm?id=3349589">Repairnator patches programs
      automatically</a></li>
</ul>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/december/images/martin.png" alt="Martin Monperrus"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Martin Monperrus</p>
          <p>Professor, <br>KTH Royal Institute of Technology</p>
        </div>
      </div>
    </div>
  </div>
</div>