<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Simulating Urban Traffic for Modeling and Analysis</h2>
<p>
  <a href="https://projects.eclipse.org/user/8509">Robert Hilbrich</a> provides an overview of the<a
    href="https://projects.eclipse.org/projects/technology.sumo"
  > Eclipse Simulation of Urban Mobility (SUMO)</a> project, the different ways it can be used, and
  its relationship to the openMobility Working Group.
</p>
<p><img src="images/3_1.jpg"></p>
<p>
  <strong>Q. What should people know about the Eclipse SUMO project?</strong>
</p>
<p>A. They should know that this is a production-ready software toolset that allows them
  to quickly and easily create a digital twin of a city or an urban area so they can simulate
  real-world traffic &mdash; whether that&rsquo;s motorized vehicle traffic, rail traffic, bicycle
  traffic, pedestrian traffic, or even ship traffic. These simulations can be used to identify
  traffic patterns, test traffic management ideas and algorithms, and determine how traffic flows
  can be improved.</p>
<p>
  <strong>Q.  Where did the Eclipse SUMO project originate?</strong>
</p>
<p>A. The project started at the German Aerospace Center in Berlin, where I work and where
  we do considerable research into transportation systems and transportation engineering.</p>
<p>Almost 20 years ago, we started working towards building a digital twin for a city, but
  computers were still quite slow then. Today, computers can build really powerful digital models of
  cities and we see a lot of happy customers that are using the SUMO tool suite.</p>
<p>
  <strong>Q. Can you describe some of the ways people are using Eclipse SUMO? </strong>
</p>
<p>A. The SUMO tool suite is being used in many different ways, but here are just three
  examples to give you an idea:</p>
<ul>
  <li>The Oak Ridge National Laboratory in Tennessee is looking into running SUMO on their number
    one super computer to simulate the entire area at the Dallas/Fort Worth International Airport.
    Their goal is to understand the traffic patterns around the airport so they can suggest
    improvements that will help people get to the airport quicker, ensure parking is not
    overcrowded, and enable people to get to their planes on time.</li>
  <li>The city of Konstanz in southern Germany is relying on a SUMO simulation to understand and
    optimize parking in the city center. With the simulation, they can see the effects of different
    types of parking spots &mdash; on-street parking, off-street parking, and parking garages
    &mdash; on traffic congestion in the surrounding areas.</li>
  <li>Several municipalities are looking into how traffic lights and autonomous vehicles can
    communicate with one another to streamline traffic flows. So, the traffic light would tell your
    car the ideal speed to drive at to avoid red lights. And your car would tell the next set of
    traffic lights when it will be arriving so the traffic light can optimize its light control
    schedule and maybe hold the green light a little longer if you&rsquo;re nearly there. Car speed
    and traffic light cycles can also be optimized to reduce fuel consumption and idling times at
    lights, and to give preference to electric vehicles as an incentive for the public to buy them.</li>
</ul>
<p>On these types of projects, we typically see collaboration between municipalities,
  traffic light developers, and autonomous vehicle manufacturers. </p>
<p><img src="images/3_2.jpg"></p>
<p>
  <strong>Q. The SUMO tool suite sounds very sophisticated. Is it difficult to get started using it?</strong>
</p>
<p>A. Not at all. The project SUMO tool suite comes with a web wizard that lets you create
  a digital twin of your city in just seven clicks. All you need to get started is data that
  describes the road network in the city. This data is available for free through the OpenStreetMap
  project, but can also be obtained from other external data sources. The software uses the road
  network data to create a SUMO simulation that looks like your city in about 20 seconds.</p>
<p>At this point, the traffic in the simulation is random so you need to add traffic
  demand data. Getting this data can be a bit trickier, but some larger cities already use traffic
  demand models to plan public transportation and build road networks, and they&rsquo;re often happy
  to share that data. Other cities use induction loops in their road networks to count the number of
  moving vehicles, and you can use these counts to build a traffic demand model.</p>
<p>
  <strong>Q. Why is it important that Eclipse SUMO is open source? </strong>
</p>
<p>A. It is very important because it helps us reach a broad audience of researchers and
  industries that can add cutting-edge research and new algorithms to our software so the traffic
  simulations will perform even better in the future than they do today.</p>
<p>Also, the industry wants to avoid vendor lock-in so having a software platform
  that&rsquo;s not linked to a particular vendor, is of very high quality, and very rich in
  functionality has been the cornerstone of the success for the SUMO in the past.</p>
<p>
  <strong>Q. What is the relationship between Eclipse SUMO and the openMobility Working Group hosted
    by the Eclipse Foundation?</strong>
</p>
<p>A. We had reached the point where there was a lot of industry interest in Eclipse SUMO.
  A number of players were using SUMO to build projects and others were integrating SUMO into their
  commercial offerings. As a result, we were getting a lot of questions about how SUMO can be used
  to build business, whether it would still exist in five years, and what the roadmap looks like.</p>
<p>
  We created the<a href="https://www.eclipse.org/org/workinggroups/openmobility_charter.php">
    openMobility Working Group</a> so we would have a roundtable where we can bring together key
  customers and key users with a vested interest in the future of SUMO in a consortium that can
  steer and influence its direction.
</p>
<p>So, the Eclipse SUMO project is where all the coding happens. And, everything that is
  not code related &mdash; strategy, marketing, roadmaps, information dissemination &mdash; is
  coordinated and steered by the openMobility Working Group.</p>
<p>Today, the openMobility Working Group consists of research partners and industry
  partners, including Bosch and AVL, which is a big player in the car simulator world. The Working
  Group is currently focused on the Eclipse SUMO project, but in the near future, we are expecting
  to add the Eclipse MOSAIC project to our portfolio. MOSAIC will provide a framework for building a
  &ldquo;bridge&rdquo; between SUMO and other simulators. SUMO is rarely used on its own, so
  it&rsquo;s very important that it can be easily coupled with other simulators to precisely model
  autonomous vehicles or add 5G communications in the digital model.</p>
<p>
  <strong>Q. How do you see SUMO advancing in the short term and the longer term?</strong>
</p>
<p>A. We are working on a number of areas right now.</p>
<p>In the short term, we&rsquo;re really trying to integrate more precise models for
  pedestrian dynamics. They&rsquo;re called social force models and we&rsquo;re working to add these
  models into SUMO to make pedestrian movements much more realistic. </p>
<p>We&rsquo;re also working to incorporate railroad traffic and having trains behave more
  realistically.</p>
<p>And, we are working on fleet management. If you think about all of the new forms of
  public transportation, such as Uber and Lyft, and the demand-response mechanisms behind them, you
  can see that SUMO is the perfect tool suite to assist in optimizing the way these vehicles drive,
  where they pick up passengers, and the routes they take.</p>
<p>
  <strong>Q. How can people get involved and learn more?</strong>
</p>
<p>
  A. I would like to invite everyone who is interested in the future of SUMO to<a
    href="https://openmobility.eclipse.org/working-group/become-a-member/"
  > join the openMobility Working Group</a> to ensure the features they need are delivered with the
  right priority.
</p>
<p>
  We also host a SUMO User Conference every year in Berlin. The next conference is in May 2020, and
  people can learn more about it<a
    href="https://www.dlr.de/ts/en/desktopdefault.aspx/tabid-3930/20125_read-58718/"
  > </a><a href="https://sumo.dlr.de/2020">here</a>.
</p>
<p><img src="images/3_3.jpg"></p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/december/images/robert.png" alt="Robert Hilbrich"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name">Robert Hilbrich</p>
          <p>Group Manager<br>Deutsches Zentrum fuer Luft- und Raumfahrt e.V. (DLR)</p>
        </div>
      </div>
    </div>
  </div>
</div>