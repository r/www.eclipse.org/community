<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.19.12</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/thabang.png" />
                          <h3>Editor's Note</h3>
                         <p>The Eclipse Foundation’s community has had an amazing year! As 2019
                          ends, I wanted to say thank you to the many Eclipse committers, project
                          leaders, and contributors who drive our community’s success. In this
                          issue, we are highlighting some exciting projects that are addressing
                          real-world challenges software developers face today.</p>
                        <p>Here they are in alphabetical order:</p>
                        <ul>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/2.php">Eclipse Deeplearning4j</a>: Adam Gibson explains how this project aims to
                            take deep learning and AI applications out of the theoretical, academic
                            world and into the real world where they can be applied in useful and
                            meaningful ways across industries.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/4.php">Eclipse Iceoryx</a>: Michael P&ouml;hnl looks at what this project&rsquo;s
                            innovative approach to enabling virtually limitless data transmissions
                            at constant time means when huge volumes of data must be transferred
                            between different parts of a system.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/5.php">Eclipse Repairnator</a>: Martin Monperrus introduces a bot for automated
                            program repair and describes how it works.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/6.php">Eclipse Steady</a>: Henrik Plate takes an in-depth look at this
                            code-centric approach to mitigating known vulnerabilities in open source
                            components.</li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/3.php">Eclipse SUMO</a>: Robert Hilbrich answers key questions about this
                            simulation tool for urban traffic modeling and analysis.</li>
                        </ul>
                        <p>In addition to these new articles, you can find our usual lists
                          so you can stay up to date with what we&rsquo;re doing here at the Eclipse
                          Foundation:</p>
                        <ul>
                          <li><a href="#new_project_proposals">New Project Proposals</a></li>
                          <li><a href="#new_project_releases">New Project Releases</a></li>
                          <li><a href="#upcoming_events">Upcoming Eclipse Events</a></li>
                        </ul>
                        <p>
                          Finally, the Eclipse IDE 2019-12 release is now available for download! To
                          learn what’s <a href="https://www.eclipse.org/eclipseide/2019-12/noteworthy/">new</a> with 76 contributing projects, see the 2019-12 release in
                          action, and to get involved, head over to <a
                            href="https://www.eclipse.org/eclipseide/"
                          >https://www.eclipse.org/eclipseide/</a>. Congratulations and thank you to
                          everyone who contributed to delivering another successful quarterly
                          release on time.
                        </p>
                        <p>
                          We&rsquo;ll be back in January with a look at projects to watch for in
                          2020. Watch your inbox or bookmark our<a
                            href="https://www.eclipse.org/community/eclipse_newsletter/"
                          > newsletter page</a> to ensure you don&rsquo;t miss it.&nbsp;
                        </p>
                        <p>Happy Reading!</p>
                        <p>Thabang Mashologu</p>
                        <p><a href="https://twitter.com/t_mashologu">@t_mashologu</a></p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/2.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/2.php"><h3>Eclipse Deeplearning4j</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/3.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/3.php"><h3>Eclipse SUMO</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/4.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/4.php"><h3>Eclipse iceoryx</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/5.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/5.php"><h3>Eclipse Repairnator</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/6.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/december/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/december/6.php"><h3>Eclipse Steady</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                      <!-- FLEXIBLE CONTAINER // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_proposals" style="margin-bottom:10px;">New Project Proposals</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            	<li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-sentinel">Eclipse zenoh</a> provides a stack that unifies data in motion, data in-use and data at rest. It carefully blends traditional pub/sub with geo-distributed storages, queries and computations, while retaining a level of time and space efficiency that is well beyond any of the mainstream stacks.</li>
                            	<li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/jakarta-ee-starter">XML.LS</a> implements most of the language server protocol features to provide XML editing support while also exposing extension points for 3rd party adopters to provide custom features via a plugin system.</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
	<li>The <a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-open-vsx-registry">Eclipse Free BIRD Tools</a> project aims to provide visual tooling to support the development, testing, and understanding of the artefacts produced by the European Central Banks BIRD project.</li>
	<li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/proposals/eclipse-embedded-cdt-cc-development-tools">Eclipse SmartMDSD</a> will focus on model-driven tooling for service-oriented and component-based robotics software development</li>
</ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 id="new_project_releases" style="margin-bottom:10px;">New Project Releases</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul dir="ltr">
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/polarsys.kitalpha">Eclipse Kitalpha 1.4.0 </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/polarsys.capella">Eclipse Capella 1.4.0&nbsp;</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.collections">Eclipse Collections 10.1.0&nbsp;</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.mosaic">Eclipse MOSAIC Creation</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.emf.diffmerge">Eclipse EMF Diff/Merge 0.12.0 </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.cyclonedds">Eclipse Cyclone DDS 0.5.0 (Eusebius)&nbsp;</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.californium">Eclipse Californium (Cf) CoAP Framework 2.0.0</a>&nbsp;</li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.sirius">Eclipse Sirius 6.3.0</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/iot.ditto">Eclipse Ditto 1.0.0</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/ecd.openvsx">Eclipse Open VSX Registry </a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/technology.passage">Eclipse Passage 0.7.0</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/modeling.modisco">Eclipse MoDisco Termination</a></li>
                              <li><a style="color:#000;" target="_blank" href="https://projects.eclipse.org/projects/tools.dartboard">Eclipse Dartboard 0.</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table border="0" cellpadding="0"
                        cellspacing="0"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 id="upcoming_events">Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>

                            <p>Upcoming industry events and workshops where our members will play a key role. Hope to see you there!</p>

                            <p dir="ltr"><a style="color:#000;" href="https://fosdem.org/2020/">FOSDEM</a><br />
Jan 31-Feb 2 2020 | Brussels, Belgium</p>

<p dir="ltr"><a style="color:#000;" href="https://www.sigs-datacom.de/order/payment/oop2020_en.php?_ga=2.47394720.624396651.1574374423-1714592911.1574111120">OOP Munich</a><br />
Feb 2-Feb 7, 2020 | Munich, Germant</p>

<p dir="ltr"><a style="color:#000;" href="https://bosch-connected-world.com/attend/">Bosch ConnectedWorld</a><br />
Feb 9-10, 2020 | Berlin, Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://bosch-connected-world.com/hackathon/">Bosch ConnectedExperience</a><br />
Feb 17-19, 2020 | Berlin,Germany</p>

<p dir="ltr"><a style="color:#000;" href="https://www.huaweicloud.com/HDC.Cloud.html">Huawei Developer Conference</a><br />
Feb 11-12, 2020 | Shenzhen, China</p>

<p dir="ltr"><a style="color:#000;" href="https://devnexus.com/">Devnexus</a><br />
Feb 18-21, 2020 | Atlanta, GA</p>

<p dir="ltr"><a style="color:#000;" href="https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/">KubeCon Europe</a><br />
March 30 - April 2, 2020 | Amsterdam, The Netherlands</p>

<p dir="ltr"><a style="color:#000;" href="https://tmt.knect365.com/iot-world/">IoT World</a><br />
Apr 6-9, 2020 | San Jose, CA</p>

<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 id="contact_us" style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->