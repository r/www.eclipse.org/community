<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>It&rsquo;s been a week since I started as the Jakarta EE Developer Advocate at the
  Eclipse Foundation!</p>
<p>As you may have noticed, I am involved in almost any committee around Jakarta EE and
  enterprise Java in general and my new role has some implications for these engagements. I have
  listed them below and tried to give a reasonable explanation for each of them.</p>
<h2>EE4J PMC</h2>
<p>I have been a member of the EE4J PMC since its inception back in 2017, and for
  practical reasons served as the PMC Lead the entire time. According to the charter, we are
  supposed to rotate the leadership among the non-foundation staff members of the PMC. In order to
  minimize overhead, the PMC decided to stick with me as the lead until otherwise decided.</p>
<p>If the PMC wants me to continue in the PMC Lead position, the &ldquo;non-Foundation
  staff&rdquo; phrase will have to be removed from the charter. This has been put on the agenda for
  the PMC meeting on November 5th, so then we will know&hellip;</p>
<h2>Jakarta EE Working Group Steering Committee</h2>
<p>I have withdrawn from my elected Committer Representative seat in the Steering Group as
  this seat should not be held by anyone from the Eclipse Foundation. This position is currently up
  for election (hint hint: if you want to be involved, nominate yourself&hellip;).</p>
<h2>Jakarta EE Working Group Specification Committee</h2>
<p>The position I have in the Specification Committee is the PMC Representative. It is up
  to the PMC whether I should continue or withdraw. This will also be handled at the next PMC
  meeting on November 5th.</p>
<h2>Java Community Process (JCP) Executive Committee</h2>
<p>I have withdrawn from my Associate Seat at the JCP since the Eclipse Foundation is
  already on the committee. However, I will still be lurking around here as I will be the alternate
  representative for the Eclipse Foundation.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/ivar.png" alt="Ivar Grimstad"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Ivar Grimstad</p>
          <p>
            Jakarta EE Developer Advocate<br> Eclipse Foundation
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ivar_grimstad">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>