<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  <a href="https://ee.kumuluz.com/">KumuluzEE</a>
</p>
<p>
  HollowJARs and UeberJARs / FatJARs deployments are possible. Commercial (but fully optional)
  support is <a href="https://ee.kumuluz.com/support/">available</a>.
</p>
<p>
  <a href="https://helidon.io/#/">helidon</a>
</p>
<p>Services are deployed as HollowJARs and custom strategies (via main method). GraalVM integration
  is available. Sponsored by Oracle.</p>
<p>
  <a href="https://quarkus.io/">quarkus</a>
</p>
<p>Supports SkimmedJARs (executable FatJARs with strictly separated infrastructure) and comes with
  GraalVM integration. Sponsored by RedHat.</p>
<p>
  <a href="https://thorntail.io/">thorntail</a>
</p>
<p>
  Services can be deployed as Uberjars / FatJARs or as HollowJARs. Sponsored by RedHat. Eventually
  might by replaced by <a href="https://quarkus.io/">quarkus</a>.
</p>
<p>
  <a href="https://hammock-project.github.io/">Hammock</a>
</p>
<p>
  Services are shipping as executable JARs (FatJARs). Sponsored by <a
    href="https://github.com/johnament"
  >John Amment</a> :-)
</p>
<p>
  <a href="https://github.com/fujitsu/launcher">Fujitsu Launcher</a>
</p>
<p>Services ship as ThinWARs, HollowJARs or FatJARs / UeberJARs. Sponsored by Fujitsu.</p>
<p>
  <a href="https://openwebbeans.apache.org/meecrowave/">meecrowave</a>
</p>
<p>Ships with core MicroProfile specs (JAX-RS, CDI, JSON-P, JSON-B). Additional MicroProfile APIs
  can be integrated. HollowJAR and custom deployment strategies are available (via main method).</p>
<p>
  <a href="https://openliberty.io/">openliberty</a>
</p>
<p>
  Full stack Jakarta EE server with MicroProfile support. ThinWAR and UeberJARs / FatJARs are
  supported. Sponsored by IBM. Commmercial support is <a
    href="https://www.ibm.com/us-en/marketplace/elite-support-for-open-liberty/faq"
  >available</a>.
</p>
<p>
  <a href="https://www.payara.fish/">Payara Server and Payara Micro</a>
</p>
<p>
  Full stack Jakarta EE application server with MicroProfile support. ThinWAR and HolloJAR
  deployments are possible. Commercial support is <a
    href="https://www.payara.fish/support/payara-enterprise/"
  >available</a>
</p>
<p>
  <a href="https://wildfly.org/">WildFly</a>
</p>
<p>
  Jakarta EE server with partial MicroProfile support. Additional MicroProfile APIs are available
  via project <a href="https://smallrye.io/">smallrye</a> WildFly is sponsored by RedHat. Commercial
  support is available from RedHat.
</p>
<p>
  <a href="http://tomee.apache.org/">TomEE</a>
</p>
<p>
  Fullstack Jakarta EE and MicroProfile server. An Apache project. Services are deployable as
  ThinWARs and FatJars. Commercial support is <a href="https://www.tomitribe.com/services/">available</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/adam.png" alt="Adam Bien"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Adam Bien</p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/AdamBien">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>