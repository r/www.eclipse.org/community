<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>The latest news and updates from the Jakarta EE community.</p>
<h2>Jakarta EE a Major Focus at EclipseCon</h2>
<p>EclipseCon Europe was a great success with several events reinforcing the incredibly
  strong community interest and engagement in the future of Jakarta EE. Many of the discussions were
  focused on the community&rsquo;s feedback on Jakarta EE 8 and their thoughts and ideas for Jakarta
  EE 9 and Jakarta EE 10. And all of the cloud native sessions were very well attended.&nbsp;</p>
<p>At the Community Day Town Hall, Eclipse Foundation Executive Director, Mike
  Milinkovich, addressed a number of questions related to the roadmap and technical direction for
  Jakarta EE, including its relationship with Eclipse MicroProfile. While the two are very related
  technologies, Mike pointed out they do need to remain separate at this point as they are being
  developed very differently. However it is needed to further formalize the process that is used for
  Eclipse MicroProfile specification development.</p>
<p>
  To help attendees understand how to combine Jakarta EE and Eclipse MicroProfile, community member
  Edwin Derks and two of his colleagues presented a workshop that offered attendees hands-on
  experience building microservices with the two technologies. For more information about their
  workshop,<a
    href="https://www.cloudnativesolutions.guru/jakarta-ee/building-microservices-with-jakarta-ee-and-microprofile-eclipsecon-2019/"
  > click here</a>.
</p>
<p>
  To access the full suite of recordings from EclipseCon Europe,<a
    href="https://www.youtube.com/watch?v=WaL9CdrawAE&amp;list=PLy7t4z5SYNaT_yo5Dhajb9i-Pf0LbQ3z8"
  > go to YouTube</a>.
</p>
<h2>Jakarta EE 9 Delivery Plan Due Dec 9th</h2>
<p>The Jakarta EE Working Group, and the Steering Committee in particular, are actively
  working on the roadmap for Jakarta EE 9. The platform project team has been asked to provide a
  Jakarta EE 9 delivery plan based on the Steering Committee&rsquo;s requirements by December 9.</p>
<h2>Ivar Grimstad&rsquo;s Role Evolves</h2>
<p>Ivar Grimstad, our first Jakarta EE Developer Advocate, explains what his new role at
  the Eclipse Foundation means for his participation in Jakarta EE Working Groups and other
  committees.&nbsp;</p>
<p>
  <a href="https://blogs.eclipse.org/post/ivar-grimstad/signing-%E2%80%A6">Read Ivar&rsquo;s blog
    for details.</a>
</p>
<h2>Arjan Tijms Joins Jakarta EE Steering Committee</h2>
<p>Arjan Tijms has been acclaimed as the Elected Committer Representative on the Jakarta
  EE Steering Committee, filling the position vacated by Ivar Grimstad.</p>
<p>The seats on the Marketing and Brand Committees will remain vacant until the next
  election period as there are currently no nominees.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/shabnam.png" alt="Shabnam Mayel"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Shabnam Mayel</p>
          <p>Senior Marketing Manager<br>
          Eclipse Foundation</p>
        </div>
      </div>
    </div>
  </div>
</div>
