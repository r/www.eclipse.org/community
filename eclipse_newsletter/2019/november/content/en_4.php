<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  Maybe you&#39;ve already heard about <a href="https://microprofile.io/">Eclipse MicroProfile</a>,
  or maybe you don&#39;t know what benefits it offers you in your current project. Perhaps you
  don&#39;t see the relationship with Java EE/Jakarta EE - or how you can use it with Payara Server
  or Payara Micro.
</p>
<p>In this blog, I&#39;ll give you a short overview of all of the above questions so that
  you can start using MicroProfile in your next project on the Payara Platform.</p>
<h2>What is MicroProfile?</h2>
<p>Starting when Oracle was not clear about their plans for Java EE 8, in a period of time
  where the term &quot;cloud-native&quot; became a reality since Netflix finished their first
  large-scale migration, it was an appropriate time for launching a new initiative.</p>
<p>The main goal of MicroProfile is &quot;Optimizing Enterprise Java for a Microservices
  Architecture&quot;. It does this by building on top of a few Java EE specifications, namely CDI,
  JAX-RS and JSON (JSON-B and JSON-P), some standardized specifications which are common in a
  microservice architecture.</p>
<p>The main differences with Java EE are that the initiative is entirely open-source,
  hosted by the Eclipse Foundation, there is no reference implementation but is supported by many
  vendors, each having their compatible implementation, fast-paced with several releases a year and
  breaking changes can occur.</p>
<p>For the moment, the umbrella spec (similar to what Java EE is) contains the following
  specifications:</p>
<p>
  <strong>Configuration</strong> - Externalize and manage your configuration parameters outside your
  micro-services.
</p>
<p>
  <strong>Fault Tolerance</strong> - All about bulkheads, timeouts, circuit breakers, retries, etc.
</p>
<p>
  <strong>JWT Propagation</strong> - Propagate security across your micro-services.
</p>
<p>
  <strong>Metrics</strong> - Gather and create operational and business measurements.
</p>
<p>
  <strong>Health Checks </strong>- Verify the health of your micro-services.
</p>
<p>
  <strong>Open API</strong> - Generate OpenAPI-compliant API documentation.
</p>
<p>
  <strong>Open Tracing</strong> - Trace the flow of requests as they traverse your micro-services.
</p>
<p>
  <strong>Rest Client </strong>- Invoke RESTful services in a type-safe manner.
</p>
<p>
  You can have a look at the <a href="https://microprofile.io/">website</a> and read more on each of
  the individual specifications on the GitHub repositories.
</p>
<h2>How do I Use MicroProfile with Payara Platform?</h2>
<p>
  Using MicroProfile with Payara Server and Payara Micro is very simple. Since both the Payara
  Platform runtimes are MicroProfile compliant, they have all the necessary dependencies on board to
  run your MicroProfile application.<br /> <br /> You only need to include the MicroProfile API POM
  into your maven project:
</p>
<pre><code>&lt;dependency&gt;
  &lt;groupId&gt;org.eclipse.microprofile&lt;/groupId&gt;
  &lt;artifactId&gt;microprofile&lt;/artifactId&gt;
  &lt;version&gt;2.2&lt;/version&gt;
  &lt;type&gt;pom&lt;/type&gt;
  &lt;scope&gt;provided&lt;/scope&gt;
&lt;/dependency&gt;
</code></pre>

<p>and you have access to all MicroProfile constructs. As you can see, it is very similar
  to bringing the Java EE dependencies in your project.</p>
<p>This is in contrast with some other runtimes where you need to define each
  specification separately and add a specification implementation to your application before you can
  use it.</p>
<p>This way, you can use the concepts you are familiar with from Java EE and gradually
  include the features of MicroProfile in your project. Since they are all included within Payara
  Server and Payara Micro, you can just start using them without worrying about how they need to be
  &#39;accessed&#39; or &#39;configured&#39;.</p>
<h2>Why Should I Use MicroProfile with Payara Platform?</h2>
<p>Payara Server and Payara Micro are just two of the many compatible runtimes, but there
  are some good reasons for using the Payara Platform with MicroProfile, including:</p>
<ul>
  <li>The implementation of the MicroProfile specification is done in a Payara-specific way. This
    will guarantee faster execution and also we can achieve deep integration with other parts of the
    product (keep reading to learn about extensions we have.)</li>
  <li>It combines the Jakarta EE and MicroProfile specifications in a seamless way so that you just
    can choose what to use and in which combination for your specific project.</li>
  <li>You can build an executable jar of your application but for cloud environments, the hollow jar
    approach of Payara Micro gives you a much more efficient structured Container image.</li>
  <li>There are several extensions to the MicroProfile specifications implemented within Payara</li>
  <li>Payara has several additional MicroProfile Config sources so that you can define configuration
    values through the Payara Web console or asadmin commands at different levels (for example,
    server-wide or application-specific).</li>
  <li>The OpenTracing information is used by the monitoring service to detect and report slow
    response to a request.</li>
  <li>The OpenTracing is extended to Servlet and JAX-WS calls.</li>
  <li>JWT propagation is deeply integrated into the security system of the Java EE system. This
    means that JWT tokens also can be used for authentication and authorization of requests within
    the Java EE / Jakarta EE area.</li>
  <li>The deployment status of your applications are integrated within the Health endpoints and can
    be used in cloud environments to detect the readiness of your application, also with Health 1.0.</li>
  <li>Support for &#39;namespaces&#39; within the JWT claims so that some additional sources, like
    Auth0 can be used as a token provider.</li>
  <li>Export some of the JMX bean values as Metrics through a configuration file.</li>
</ul>
<h2>Does MicroProfile Replace Java EE/Jakarta EE?</h2>
<p>
  Since the development of MicroProfile started when the innovations into Java EE slowed down, one
  can ask the question if it replaces or will replace Java EE or Jakarta EE?<br /> <br /> The answer
  to this question is very clear, no! As already mentioned, some of the basic building blocks of
  MicroProfile are some Java EE specifications. So you can see it more as the &#39;micro
  profile&#39; of Java EE. Although not officially, it is a grouping of frameworks that you use very
  often together when you create microservices.
</p>
<p>With the transition of Java EE to Jakarta EE, it will become easier to innovate within
  Jakarta EE as it is now a completely open-source product. There are already voices that propose to
  merge MicroProfile into Jakarta EE. So it could become a &#39;Jakarta EE micro profile&#39; but
  that is something that only the future knows at this moment as there are some hurdles to take
  (like differences in backward compatibility, number of releases, and project governance, for
  starters).</p>
<p>MicroProfile will not replace Jakarta EE but it will certainly be a companion in many
  projects where you combine Jakarta EE and MicroProfile. This is already the case today since you
  can, in most runtimes, such as the Payara Platform, combine it with other specifications like JPA
  when you need to access a database, or JMS when using Message Queues.</p>
<h2>Payara Platform with MicroProfile Makes Microservices Easier</h2>
<p>With MicroProfile, you have some additional tools in your box to make it even easier to
  create your microservices based on Java EE or Jakarta EE.</p>
<p>Payara Server and Payara Micro have all the code for implementing the MicroProfile
  specifications already on board. You just need to include the API in your project and you can
  enjoy the deep integration within the server or runtime without the need for any other
  configuration.</p>
<p>This makes it easy for the developer to gradually include more features of MicroProfile
  in their application or make use of the extensions we have foreseen.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/rudy.png" alt="Rudy De Busscher"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Rudy De Busscher</p>
          <p>Service Team Member<br>
          Payara</p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank"
              href="https://twitter.com/rdebusscher"
            >Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>