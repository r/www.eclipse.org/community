<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In a <a href="https://blog.hargrave.io/2019/05/jakarta-ee-and-package-renaming.html">previous post</a>,
  I laid out my thinking on how to approach the package renaming problem which the Jakarta EE
  community now faces. Regardless of whether the community chooses big bang or incremental, there
  are still existing artifacts in the world using the Java EE package names that the community will
  need to use together with the new Jakarta EE package names.
</p>
<p>
  Tools are always important to take the drudgery away from developers. So I have put together a <a
    href="https://github.com/bjhargrave/transformer"
  >tool</a> prototype which can be used to transform binaries such as individual class files and
  complete JARs and WARs to rename uses of the Java EE package names to their new Jakarta EE package
  names.
</p>
<p>The tools is rule driven which is nice since the Jakarta EE community still needs to
  define the actual package renames for Jakarta EE 9. The rules also allow the users to control
  which class files in a JAR/WAR are transformed. Different users may want different rules depending
  upon their specific needs. And the tool can be used for any package renaming challenge, not just
  the specific Jakarta EE package renames.</p>
<p>
  The tools provides an API allowing it to be embedded in a runtime to dynamically <a
    href="https://github.com/bjhargrave/transformer/blob/5b2ba77f4647cce275567219e4e97cd7b41ef0b1/src/main/java/dev/hargrave/transformer/Transformer.java#L231"
  >transform class files</a> during the class loader definition process. The API also supports <a
    href="https://github.com/bjhargrave/transformer/blob/5b2ba77f4647cce275567219e4e97cd7b41ef0b1/src/main/java/dev/hargrave/transformer/Transformer.java#L158"
  >transforming JAR files</a>. A <a href="https://github.com/bjhargrave/transformer#command-line">CLI</a>
  is also provided to allow use from the command line. Ultimately, the tool can be packaged as
  Gradle and Maven plugins to incorporate in a broader tool chain.
</p>
<p>
  Given that the tool is prototype, and there is much work to be done in the Jakarta EE community
  regarding the package renames, I have started a <a
    href="https://github.com/bjhargrave/transformer/labels/enhancement"
  >list of TODOs</a> in the project&#39; issues for known work items.
</p>
<p>Please try out the tool and let me know what you think. I am hoping that tooling such
  as this will ease the community cost of dealing with the package renames in Jakarta EE.</p>
<p>PS. Package renaming in source code is also something the community will need to deal
  with. But most IDEs are pretty good at this sort of thing, so I think there is probably sufficient
  tooling in existence for handling the package renames in source code.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-16">
      <div class="row">
        <div class="col-sm-6">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/bj.png" alt="BJ Hargrave"
          />
        </div>
        <div class="col-sm-18">
          <p class="author-name">BJ Hargrave</p>
          <p>Senior Technical Staff Member<br>IBM</p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/bjhargrave">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>