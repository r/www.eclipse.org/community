<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In one of my previous blog&nbsp;<a
    href="https://rieckpil.de/howto-bootstrap-your-first-jakarta-ee-8-application/"
  >posts</a>, I showed you what you need to create your first<strong>&nbsp;Jakarta EE 8 project</strong>.
  Manually creating a Maven project for a Jakarta EE application&nbsp;<strong>from</strong>&nbsp;<strong>scratch</strong>&nbsp;is&nbsp;<strong>cumbersome</strong>.
  Especially for new developers, the folder and file setup might be overwhelming. To overcome this
  manual process and to quickly bootstrap a new Jakarta EE 8 Maven project in seconds, I&rsquo;ve
  created a&nbsp;<strong>Maven</strong>&nbsp;<strong>archetype</strong>. This includes all the
  necessary ingredients (Java 11, MicroProfile, JUnit, Mockito, etc.) to kickstart your new project.
</p>
<p>Read this blog post to understand how to use this archetype and bootstrap your new Jakarta EE 8
  projects in seconds.</p>
<h2>Bootstrap a new project with a Maven archetype</h2>
<p>
  First, you need Maven (at least version 3) available on your machine. You can download Maven&nbsp;<a
    href="https://maven.apache.org/download.cgi"
  >here</a>&nbsp;and just have to put the Maven binary on your path. Next, open a terminal window
  and use the following command to create the project:
</p>
<pre><code>mvn archetype:generate \
  -DarchetypeGroupId=de.rieckpil.archetypes \
  -DarchetypeArtifactId=jakartaee8 \
  -DarchetypeVersion=1.0.0 \
  -DgroupId=de.rieckpil.blog \
  -DartifactId=jakartaee-8-project \
  -DinteractiveMode=false
</code></pre>
<p>
  <strong>HINT:</strong>&nbsp;Double-click on the code above to get the raw result to copy and paste
  it without any additional formatting.
</p>
<p>
  You don&rsquo;t need to download anything in advance, as this command will download everything
  from&nbsp;<a href="https://search.maven.org/search?q=g:de.rieckpil.archetypes">Sonatype</a>.
  It&rsquo;s important to specify the
  correct&nbsp;archetypeGroupId,&nbsp;archetypeArtifactId,&nbsp;archetypeVersion&nbsp;like in the
  example above.
</p>
<p>Both the&nbsp;groupId&nbsp;and the&nbsp;artifactId&nbsp;are free to choose. As a basic
  convention, I&rsquo;ll use the&nbsp;artifactId&nbsp;for the folder name and
  the&nbsp;groupId&nbsp;for the initial package structure.</p>
<p>The&nbsp;interactiveMode&nbsp;attribute is optional but speeds up the process. Otherwise, you
  need to further confirm the creation of the project.</p>
<p>So the basic usage of this Maven archetype is the following:</p>
<pre><code>mvn archetype:generate -DarchetypeGroupId=de.rieckpil.archetypes \
  -DarchetypeArtifactId=jakartaee8 \
  -DarchetypeVersion=1.0.0\
  -DgroupId=&lt;your project Group Id&gt; \
  -DartifactId=&lt;your project artifact Id&gt;
</code></pre>
<h2>Components of the pre-defined Jakarta EE 8 project</h2>
<p>
  For the initial components of this Jakarta EE 8 project, I try to keep it to the bare
  minimum.&nbsp; Alongside the Jakarta EE and MicroProfile specifications,&nbsp;<strong>JUnit 5</strong>&nbsp;and&nbsp;<strong>Mocktio&nbsp;</strong>are
  available for efficient testing. Without further configurations, the&nbsp;pom.xml&nbsp;of the
  project looks like the following:
</p>
<pre><code>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"&gt;
 &lt;modelVersion&gt;4.0.0&lt;/modelVersion&gt;
 &lt;groupId&gt;de.rieckpil.blog&lt;/groupId&gt;
 &lt;artifactId&gt;jakartaee-8-project&lt;/artifactId&gt;
 &lt;version&gt;1.0-SNAPSHOT&lt;/version&gt;
 &lt;packaging&gt;war&lt;/packaging&gt;

 &lt;properties&gt;
 &lt;maven.compiler.source&gt;11&lt;/maven.compiler.source&gt;
 &lt;maven.compiler.target&gt;11&lt;/maven.compiler.target&gt;
 &lt;failOnMissingWebXml&gt;false&lt;/failOnMissingWebXml&gt;
 &lt;project.build.sourceEncoding&gt;UTF-8&lt;/project.build.sourceEncoding&gt;
 &lt;project.reporting.outputEncoding&gt;UTF-8&lt;/project.reporting.outputEncoding&gt;
 &lt;jakarta.jakartaee-api.version&gt;8.0.0&lt;/jakarta.jakartaee-api.version&gt;
 &lt;microprofile.version&gt;3.0&lt;/microprofile.version&gt;
 &lt;mockito-core.version&gt;3.1.0&lt;/mockito-core.version&gt;
 &lt;junit-jupiter.version&gt;5.5.0&lt;/junit-jupiter.version&gt;
 &lt;/properties&gt;

 &lt;dependencies&gt;
 &lt;dependency&gt;
 &lt;groupId&gt;jakarta.platform&lt;/groupId&gt;
 &lt;artifactId&gt;jakarta.jakartaee-api&lt;/artifactId&gt;
 &lt;version&gt;${jakarta.jakartaee-api.version}&lt;/version&gt;
 &lt;scope&gt;provided&lt;/scope&gt;
 &lt;/dependency&gt;
 &lt;dependency&gt;
 &lt;groupId&gt;org.eclipse.microprofile&lt;/groupId&gt;
 &lt;artifactId&gt;microprofile&lt;/artifactId&gt;
 &lt;version&gt;${microprofile.version}&lt;/version&gt;
 &lt;type&gt;pom&lt;/type&gt;
 &lt;scope&gt;provided&lt;/scope&gt;
 &lt;/dependency&gt;
 &lt;dependency&gt;
 &lt;groupId&gt;org.junit.jupiter&lt;/groupId&gt;
 &lt;artifactId&gt;junit-jupiter&lt;/artifactId&gt;
 &lt;version&gt;${junit-jupiter.version}&lt;/version&gt;
 &lt;scope&gt;test&lt;/scope&gt;
 &lt;/dependency&gt;
 &lt;dependency&gt;
 &lt;groupId&gt;org.mockito&lt;/groupId&gt;
 &lt;artifactId&gt;mockito-core&lt;/artifactId&gt;
 &lt;version&gt;${mockito-core.version}&lt;/version&gt;
 &lt;scope&gt;test&lt;/scope&gt;
 &lt;/dependency&gt;
 &lt;/dependencies&gt;

 &lt;build&gt;
 &lt;finalName&gt;jakartaee-8-project&lt;/finalName&gt;
 &lt;plugins&gt;
 &lt;plugin&gt;
 &lt;artifactId&gt;maven-compiler-plugin&lt;/artifactId&gt;
 &lt;version&gt;3.8.1&lt;/version&gt;
 &lt;/plugin&gt;
 &lt;plugin&gt;
 &lt;artifactId&gt;maven-surefire-plugin&lt;/artifactId&gt;
 &lt;version&gt;2.22.2&lt;/version&gt;
 &lt;/plugin&gt;
 &lt;/plugins&gt;
 &lt;/build&gt;
&lt;/project&gt;
</code></pre>
<p>Furthermore, the project includes a
  basic&nbsp;persistence.xml,&nbsp;microprofile-config.properties,&nbsp;beans.xml&nbsp;file to start
  using JPA, CDI and MicroProfile Config.</p>
<p>In addition a first JAX-RS resource is available to ensure the package structure is created and
  the application is up- and running:</p>
<pre><code>@ApplicationPath("resources")
public class JAXRSConfiguration extends Application {

}
</code></pre>

<pre><code>@Path("sample")
public class SampleResource {

  @Inject
  @ConfigProperty(name = "message")
  private String message;

  @GET
  public Response message() {
    return Response.ok(message).build();
  }

}
</code></pre>
<h2>Build and deploy the application</h2>
<p>
  As a default deployment target, you&rsquo;ll get&nbsp;<strong>Open Liberty</strong>. The project
  includes a&nbsp;Dockerfile&nbsp;to create a Docker container of your application running on Open
  Liberty:
</p>
<pre><code>FROM open-liberty:kernel-java11
COPY --chown=1001:0  target/jakartaee-8-project.war /config/dropins/
COPY --chown=1001:0  server.xml /config
</code></pre>
<p>The configuration for Open Liberty is basic, but enough and can be extended or reconfigured at
  any time:</p>
<pre><code>&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;server description="new server"&gt;

 &lt;featureManager&gt;
 &lt;feature&gt;javaee-8.0&lt;/feature&gt;
 &lt;feature&gt;microProfile-3.0&lt;/feature&gt;
 &lt;/featureManager&gt;

 &lt;httpEndpoint id="defaultHttpEndpoint" httpPort="9080" httpsPort="9443"/&gt;

 &lt;quickStartSecurity userName="duke" userPassword="dukeduke"/&gt;
&lt;/server&gt;
</code></pre>
<p>
  <strong>HINT</strong>: For production usage, make sure to further adjust the security settings
  properly.
</p>
<p>To serve the application from the root path, I&rsquo;m including a&nbsp;ibm-web-ext.xml&nbsp;file
  with to configure the context root path:</p>
<pre><code>&lt;web-ext
 xmlns="http://websphere.ibm.com/xml/ns/javaee"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://websphere.ibm.com/xml/ns/javaee http://websphere.ibm.com/xml/ns/javaee/ibm-web-ext_1_0.xsd"
 version="1.0"&gt;
 &lt;context-root uri="/"/&gt;
&lt;/web-ext&gt;
</code></pre>
<p>Last but not least, you can build and deploy the application with a script for convenience.
  It&rsquo;s available for both Windows&nbsp;buildAndRun.bat) and Mac/Linux (buildAndRun.sh). Once
  you execute it, Maven will create the&nbsp;.war&nbsp;file and a new Docker container is spawned.</p>
<p>
  Once the application is up- and running, you can visit&nbsp;<a
    href="http://localhost:9080/resources/sample"
  >http://localhost:9080/resources/sample</a>&nbsp;and should get a greeting.
</p>
<p>In total, these are the required commands to have your next Jakarta EE 8 Maven project up in
  seconds:</p>
<pre><code>mvn archetype:generate -DarchetypeGroupId=de.rieckpil.archetypes -DarchetypeArtifactId=jakartaee8 -DarchetypeVersion=1.0.0 -DgroupId=de.rieckpil.blog -DartifactId=jakartaee-8-microservice -DinteractiveMode=false
cd jakartaee-8-microservice
./buildAndRun.sh
</code></pre>
<h2>Live demo of this Jakarta EE 8 archetype</h2>
<p><a class="eclipsefdn-video" href="https://www.youtube.com/watch?v=aiDcgEZY21I"></a></p>
<p>If you are looking for further Maven archetypes to bootstrap a Java EE 8 project with or without
  JSF setup, have a look at the following tutorials:</p>
<ul>
  <li><a href="https://rieckpil.de/howto-bootstrap-a-jsf-2-3-maven-project-in-seconds/">#HOWTO:
      Bootstrap a JSF 2.3 Maven project in seconds</a></li>
  <li><a
    href="https://rieckpil.de/howto-bootstrap-a-java-ee-8-and-microprofile-2-0-maven-project-in-seconds/"
  >#HOWTO: Bootstrap a Java EE 8 and MicroProfile 2.0.1 Maven project in seconds</a></li>
</ul>
<p>
  You can find the archetype on&nbsp;<a href="https://github.com/rieckpil/custom-maven-archetypes">GitHub</a>.
</p>
<p>Have fun creating new Jakarta EE 8 projects with Maven,</p>
<p>Phil</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/philip.png" alt="Philip Riecks"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Philip Riecks</p>
          <p>
            Java Developer <br> Immobilien Scout 24
          </p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank"
              href="https://twitter.com/rieckpil"
            >Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>