<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  At JavaOne 2017 Oracle announced that they would start the difficult process of moving Java EE to
  the Eclipse Software Foundation. This has been a massive effort on behalf of Eclipse, Oracle and
  many others and we are getting close to having a specification process and a Jakarta EE 8
  platform. We are looking forward to being able to certify Open Liberty to it soon. While that is
  excellent news, on Friday last week Mike Milinkovich from Eclipse <a
    href="https://eclipse-foundation.blog/2019/05/03/jakarta-ee-java-trademarks/"
  >informed the community</a> that Eclipse and Oracle could not come to an agreement that would
  allow Jakarta EE to evolve using the existing javax package prefix. This has caused a flurry of
  discussion on Twitter, from panic, confusion, and in some cases outright FUD.
</p>
<p>
  To say that everyone is disappointed with this outcome would be a massive understatement of how
  people feel. Yes this is disappointing, but this is not the end of the world. First of all,
  despite what some people are implying, Java EE applications are not suddenly broken today, when
  they were working a week ago. Similarly, your Spring apps are not going to be broken (yes, the <a
    href="https://twitter.com/phillip_webb/status/1124384920925655040"
  >Spring Framework has 2545 Java EE imports</a>, let alone all the upstream dependencies). It just
  means that we will have a constraint on how Jakarta EE evolves to add new function.
</p>
<p>We have got a lot of experience with managing migration in the Open Liberty team. We have a zero
  migration promise for Open Liberty which is why we are the only application server that supports
  Java EE 7 and 8 in the same release stream. This means that if you are on Open Liberty, your
  existing applications are totally shielded from any class name changes in Jakarta EE 9. We do this
  through our versioned feature which provide the exact API and runtime required by the
  specification as it was originally defined. We are optimistic about for the future because we have
  been doing this with Liberty since it was created in 2012.</p>
<p>
  The question for the community is &quot;how we should move forward from here?&quot; It seems that
  many in the Jakarta EE spec group at Eclipse are leaning towards quickly renaming everything in a
  Jakarta EE 9 release. There are advantages and disadvantages to this approach, but it appears
  favoured by <a href="https://www.tomitribe.com/blog/jakarta-ee-a-new-hope/">David Blevins</a>, <a
    href="https://developer.ibm.com/announcements/jakarta-ee-has-landed/"
  >Ian Robinson, Kevin Sutter</a>, <a href="https://blog.payara.fish/jakarta-ee-8-and-beyond">Steve
    Millidge</a>. While I can see the value of just doing a rename now (after all, it is better pull
  a band aid off fast than slow), I think it would be a mistake if at the same time we do not invest
  in making the migration from Java EE package names to Jakarta EE package names cost nothing.
  Something in Liberty we call &quot;zero migration&quot;.
</p>
<p>Jakarta EE will only succeed if developers have a seamless transition from Java EE to Jakarta EE.
  I think there are four aspects to pulling off zero migration with a rename:</p>
<ol>
  <li>Existing application binaries need to continue to work without change.</li>
  <li>Existing application source needs to continue to work without change.</li>
  <li>Tools must be provided to quickly and easily change the import statements for Java source.</li>
  <li>Applications that are making use of the new APIs must be able to call binaries that have not
    been updated.</li>
</ol>
<p>The first two are trivial to do: Java class files have a constant pool that contains all the
  referenced class and method references. Updating the constant pool when the class is loaded will
  be technically easy, cheap at runtime, and safe. We are literally talking about changing
  javax.servlet to jakarta.servlet, no method changes.</p>
<p>The third one is also relatively simple; as long as class names do not change switching import
  statements from javax.servlet.* to jakarta.servlet.* is easy to automate.</p>
<p>The last one is the most difficult because you have existing binaries using the javax.servlet
  package and new source using the jakarta.servlet package. Normally this would produce a
  compilation error because you cannot pass a jakarta.servlet class somewhere that takes a
  javax.servlet class. In theory we could reuse the approach used to support existing apps and apply
  it at compile time to the downstream dependencies, but this will depend on the build tools being
  able to support this behaviour. You could add something to the Maven build to run prior to
  compilation to make sure this works, but that might be too much work for some users to
  contemplate, and perhaps is not close enough to zero migration.</p>
<p>
  I think if the Jakarta EE community pulls together to deliver this kind of zero migration approach
  prior to making any break, the future will be bright for Jakarta EE. The discussion has already
  started on the jakarta-platform-dev mail list kicked off by <a
    href="https://www.eclipse.org/lists/jakartaee-platform-dev/msg00029.html"
  >David Blevins</a>. If you are not a member you can join now on <a
    href="https://accounts.eclipse.org/mailing-list/jakartaee-platform-dev"
  >eclipse.org</a>. I am also happy to hear your thought via <a href="https://twitter.com/nottycode">twitter</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/alasdair.png" alt="Alasdair Nottingham"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Alasdair Nottingham</p>
          <p>Lead Architect, Open Liberty<br>
          IBM</p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-small btn-warning" target="_blank"
              href="https://twitter.com/nottycode"
            >Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>