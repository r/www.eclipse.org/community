<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In the last <a href="https://kodnito.com/posts/deploy-jakarta-ee-application-to-kubernetes/">tutorial</a>,
  we learned how to deploy our Jakarta EE application locally and created our own Docker registry
  where we deployed our Docker images and today we will learn how to deploy our Jakarta EE
  application to Google Cloud Platform (GCP) with Kubernetes.
</p>
<p>
  Go to <a href="https://cloud.google.com/">https://cloud.google.com/</a> and create an account, you
  will get $300 credit and it&#39;s valid for one year.
</p>
<p>
  To follow this tutorial, you will need Docker, Kubernetes, Maven and <a
    href="https://cloud.google.com/sdk/install"
  >Google Cloud SDK</a>.
</p>
<p>
  <a class="eclipsefdn-video" href="https://youtu.be/fiP5GZvD6yE"></a>
</p>
<h2>Create Google Kubernetes Engine Cluster.</h2>
<p>
  Click in the top menu bar where it says <strong>My first project</strong> and create a new
  project, I will call my project <strong>jakartaee-cloud-project </strong>and when it&#39;s created
  select it.
</p>
<p>
  <img src="images/6_1.png"/>
</p>

<p>
  <img src="images/6_2.png"/>
</p>
<p>
  <img src="images/6_3.png"/>
</p>
<p>1. In the menu, click on Kubernetes Engines and then on Clusters.</p>
<p>
  <img src="images/6_4.png"/>
</p>

<p>2. Click on Create Cluster, choose the default values for now, this will take few minutes.</p>
<h2>Google Cloud SDK</h2>
<p>Install the Google Cloud SDK and run the following command in the terminal.</p>
<pre><code>$&gt; gcloud init
</code></pre>
<p>Next install the kubectl command by using the following command:</p>
<pre><code>$&gt; gcloud components install kubectl
</code></pre>
<p>To see a list of components that are available, run the following command:</p>
<pre><code>$&gt; gcloud components list
</code></pre>
<p>Now we need to connect our local gcloud CLI with the cluster, you will find the correct command
  for your project when you click on the Connect button.</p>
<p>
  <img src="images/6_5.png"/>
</p>
<pre><code>$&gt; gcloud container clusters get-credentials standard-cluster-1 --zone us-central1-a --project
  zippy-starlight-256807
</code></pre>
<p>Output:</p>
<p>
  Fetching cluster endpoint and auth data.<br /> kubeconfig entry generated for standard-cluster-1.
</p>
<p>I will use my custom maven archetype to generate the Jakarta EE application.</p>
<p>Type the following command in your terminal:</p>
<pre><code>$&gt; mvn archetype:generate -DarchetypeGroupId=com.kodnito \
-DarchetypeArtifactId=kodnito-jakartaee-archetype \
-DarchetypeVersion=1.0.5 -DgroupId=com.kodnito \
-DartifactId=jakartaee-google-cloud -Dversion=1.0-SNAPSHOT
</code></pre>

<p>Now open the project in your IDE or editor of your choice.</p>
<p>Open deployment and make it look like this:</p>

<pre><code>
kind: Service
apiVersion: v1
metadata:
  name: jakartaee-google-cloud
  labels:
    app: jakartaee-google-cloud
spec:
  type: NodePort
  selector:
    app: jakartaee-google-cloud
  ports:
  - port: 8080
    targetPort: 8080
    name: http
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: jakartaee-google-cloud
spec:
  replicas: 2
  selector:
    matchLabels:
      app: jakartaee-google-cloud
  template:
    metadata:
      labels:
        app: jakartaee-google-cloud
        version: v1
    spec:
      containers:
        - name: jakartaee-google-cloud
          image:
  gcr.io/zippy-starlight-256807/jakartaee-google-cloud:1.0
          imagePullPolicy: Always
          ports:
            - containerPort: 8080
          readinessProbe:
            httpGet:
              path: /health
              port: 8080
            initialDelaySeconds: 45
          livenessProbe:
            httpGet:
              path: /health
              port: 8080
            initialDelaySeconds: 45
      restartPolicy: Always
</code></pre>

<p>This image: gcr.io/zippy-starlight-256807/jakartaee-google-cloud:1.0 part is the important one.</p>

<p>Run the following command to build the war file:</p>

<pre><code>$&gt; mvn clean package
</code></pre>

<p>The following command will build the image from the Dockerfile.</p>

<pre><code>$&gt; docker build -t jakartaee-google-cloud:1.0 .
</code></pre>

<p>Next command is used to tag the image so it points to our registry.</p>

<pre><code>$&gt; docker tag jakartaee-google-cloud:1.0
  gcr.io/zippy-starlight-256807/jakartaee-google-cloud:1.0
</code></pre>

<p>Run the following command to authenticate to Container registry.</p>

<pre><code>$&gt; gcloud auth configure-docker</code></pre>

<p>Now we can push the image.</p>

<pre><code>$&gt; docker push gcr.io/zippy-starlight-256807t/jakartaee-google-cloud:1.0</code></pre>

<p>Now it&#39;s time to deploy, use the following command to create the deployment.</p>

<pre><code>$&gt; kubectl apply -f deployment.yml</code></pre>

<p>Now run the following command to see if the deployment was created.</p>

<pre><code>$&gt; kubectl get deployments</code></pre>

<pre><code>NAME                     READY UP-TO-DATE  AVAILABLE   AGE
jakartaee-google-cloud   2/2   2           2           70s
</code></pre>

<p>The following command will list all the services.</p>

<pre><code>$&gt; kubectl get services</code></pre>

<pre><code>
NAME                     TYPE CLUSTER-IP  EXTERNAL-IP   PORT(S)           AGE
jakartaee-google-cloud   NodePort         10.12.4.69    8080:31719/TCP    109s
kubernetes               ClusterIP        10.12.0.1     443/TCP           15m
</code></pre>

<p>Your cluster is not ready yet because it is not publicly accessible, so in the root of the
  project create a file called jakartaee-ingress-deployment.yml and add the following:</p>

<pre><code>apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: jakartaee-google-cloud-ingress
spec:
  backend:
    serviceName: jakartaee-google-cloud
    servicePort: 8080
</code></pre>
<p>This file defines an Ingress resource that directs traffic to our service.</p>
<p>Ingress is a Kubernetes resources that defines rules and configurations for HTTP(S) routes
  outside the cluster to services within the cluster.</p>

<p>Now create the deployment:</p>

<pre><code>$&gt; kubectl apply -f jakartaee-ingress-deployment.yml
</code></pre>

<p>With the following command we will se the IP</p>

<pre><code>$&gt; kubectl get ingress jakartaee-google-cloud-ingress
</code></pre>

<pre><code>
NAME                             HOSTS ADDRESS           PORTS AGE
jakartaee-google-cloud-ingress   *     107.178.255.164   80    3m35s
</code></pre>

<p>
  Now open your browser and point it to <a href="about:blank">http://IP/jakartaee-google-cloud/api/hello</a>
</p>

<p>
  <img src="images/6_6.png"/>
</p>

<p>
  You can find the source code on <a
    href="https://github.com/cicekhayri/deploy-jakartaee-google-cloud-platform"
  >GitHub</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/hayri.png" alt="Hayri Cicek"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Hayri Cicek</p>
        </div>
      </div>
    </div>
  </div>
</div>