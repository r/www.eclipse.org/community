<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>A curated collection of some of the most relevant and timely blogs and tutorials by
  Eclipse community members.</p>
<h2>Hot Topic: Jakarta EE Package Naming</h2>
<p>When it was announced in May 2019 that the javax package namespace can&rsquo;t be
  modified by the Eclipse Foundation community, the great debate about how to handle this very
  challenging constraint started in earnest.</p>
<p>Here are two very interesting blogs on this hot topic:</p>
<ul>
  <li>Alasdair Nottingham advocates investing in a &ldquo;zero migration&rdquo; approach to package
    renaming based on four necessary, but not always easy, requirements. <a href="/community/eclipse_newsletter/2019/november/7.php">Read Alasdair&rsquo;s
    blog</a>.</li>
  <li>BJ Hargrave explains how a rules-based tool he has prototyped can be used to rename Jakarta EE
    packages in binaries. <a href="/community/eclipse_newsletter/2019/november/3.php">Read BJ&rsquo;s blog</a>.</li>
</ul>
<h2>Tutorials and Technical Overviews</h2>
<p>With their dedication to open source collaboration, it&rsquo;s no surprise that Jakarta
  EE community members are quick to share their technical expertise for the greater good of the
  broader community.</p>
<p>Here are three technical explanations we think you&rsquo;ll find extremely helpful as
  you delve deeper into programming with Jakarta EE:</p>
<ul>
  <li>Philip Rieck explains how to bootstrap a Jakarta EE Maven project with Java 11 in seconds.
    <a href="/community/eclipse_newsletter/2019/november/1.php">Learn more</a>.</li>
  <li>Hayri Cicek demonstrates how to deploy a Jakarta EE application on Google Cloud Platform with
    Kubernetes. <a href="/community/eclipse_newsletter/2019/november/5.php">Learn more</a>.</li>
  <li>Adam Bien provides an overview of MicroProfile runtimes. <a href="/community/eclipse_newsletter/2019/november/2.php">Learn more</a>.</li>
</ul>
<h2>Great Reads: Our Free Cloud Native Java E-Book</h2>
<p>
  If you haven&rsquo;t already downloaded our free e-book,<a
    href="https://jakarta-4753786.hs-sites.com/cloud-native-java-eBook"
  > Fulfilling the Vision for Open Source, Cloud Native Java</a>, take a minute to do that now.
</p>
<p>This e-book includes insights from some of the leading voices in enterprise Java and
  the Jakarta EE Working Group. It explores all aspects of cloud native Java, from the many benefits
  it gives developers today to the vision and priorities for its long-term evolution.</p>
<p>
  <a href="https://jakarta-4753786.hs-sites.com/cloud-native-java-eBook">Download the e-book today</a>.
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/shabnam.png" alt="Shabnam Mayel"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Shabnam Mayel</p>
          <p>Senior Marketing Manager<br>
          Eclipse Foundation</p>
        </div>
      </div>
    </div>
  </div>
</div>