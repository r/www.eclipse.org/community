<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>Combining MicroProfile, Jakarta EE, and Payara Platform</h2>
<p>Eclipse MicroProfile makes it easy to optimize resources for a microservices
  architecture. Jakarta EE drives cloud native innovation for modern enterprises. And, the Payara
  application server is compatible with both.</p>
<p>Combining these three open source solutions has numerous benefits for developers. The
  key is to understand how they fit together.</p>
<p>Payara&rsquo;s Rudy De Busscher <a href="/community/eclipse_newsletter/2019/november/4.php">explains how to make it all work</a> so you can
  start using MicroProfile in your next project.</p>

  <div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/november/images/shabnam.png" alt="Shabnam Mayel"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Shabnam Mayel</p>
          <p>Senior Marketing Manager<br>
          Eclipse Foundation</p>
        </div>
      </div>
    </div>
  </div>
</div>