<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>During our times, there lives a very popular and a matter-of-fact person, whose thoughts are balanced, philosophical and well-grounded. Lately though, on looking around, the person finds all the young kids next block having fun with sophisticated gadgets which never existed in earlier times and a desire is born in our protagonist's mind to acquire some of the better, newer gadgets. However, as we all know, our protagonist is a highly-regarded old-school decision maker and doesn't want the name acquired so far to get tainted by being seen in the wrong company - be it of people or of gadgets. After a period of deep thought, the protagonist comes up with this brilliant idea of creating a new temporary identical persona - a persona which will adapt all new and noteworthy latest and greatest gadgets to allow the friends to play with those. Depending on the friendly advices of the near and dear ones about how the persona behaved, our protagonist will adapt these gadgets and the associated behaviors to make those a permanent characteristic.</p>
	<p>Phew! That was my miserable attempt on literary creativity; before I make Shakespeare turn in his grave, let me get a little technical.</p>
	<p>The protagonist here is of course our good old Java, as you might have guessed already. As you all know, Java is planning to add multiple features in the next few releases. Given that the Java releases a new version every six months now, the duration is not good enough to release features with timely, user-driven feedback. Hence, in the previous version of Java, which is 11, it released an infrastructure, or "persona" if you may, called the "preview feature" support. Preview feature support helps users to try out new features which are "previewed" and provide feedback to Java developers. Depending on the user pulse, the preview feature might be adapted with little or a lot of changes, or even discarded. This facility provides new and exciting features to be incorporated in Java only after proper experimentation and user feedback driven improvements, thus continuing to keep Java sacrosanct while acquiring new features aka "gadgets".</p>
	<p> Though "preview feature" was available in the previous release, the "enablement", happened, for all practical purposes with the release of current version of Java, 12, since the first experimental feature came out in Java 12. Let us start with this experimental feature and then figure out how to enable it.</p>
	<p>Consider the following source code:</p>
<pre>
public class Y {
       enum Day {

             MON, TUE, WED, THUR, FRI, SAT, SUN
       };

       public String getDay(Day today) {

             String day = null;
 
             switch(today) {
                    case MON:
                    case TUE:
                           day = "Blues";
                           break;
                    case WED:
                           day = "Hectic";
                           break;
                    case THUR:
                    case FRI:
                           day = "Getting better";
                           break;
                    case SAT:
                    case SUN:
                           day = "Life!";
                           break;
             };

             return day;
       }
}
</pre>	

	<p>The code listing above shows how we assign a local string variable based on specific values of enum using a switch statement. For this we use the fall-through of case statements and breaks wherever required and then in the end the value in the local variable is returned. If the whole point is to assign a variable, can we make switch statement itself assign a value to the variable "day"? Hold on, <em>statements</em> do not have values, only expressions do. Hmm..  Then why don't we have "<span style="color:purple"><strong>switch</strong></span>" expression as well which has the capability of returning a value? That's exactly the Java 12 gives you. To provide switch expressions, Java 12 overloaded the "<span style="color:purple"><strong>break</strong></span>" statement to allow to return a value, for e.g. <span style="color:purple"><strong>break</strong></span> <span style="color:blue">"Blues"</span> where the "Blues" is a value and not a label to jump to. That throws up the question for some - How do we figure out whether the right-hand part of break statement is a label or a value - the "value" in the break is valid only in switch expressions - the Eclipse Java Compiler will flag an error if there was an attempt to code otherwise.</p>
<pre>
public class X {

       enum Day {
             MON, TUE, WED, THUR, FRI, SAT, SUN
       };

       public String getDay(Day today) {
             String day = switch(today) {
                    case MON:
                    case TUE:
                           break "Blues";
                    case WED:
                           break "Hectic";
                    case THUR:
                    case FRI:
                           break "Getting Better";
                    case SAT:
                    case SUN:
                           break "Life!";
             };
             return day;
       }
}
</pre>
	<p>That was only the first iteration. Let us try to see if there are further improvements: The overdose of "<span style="color:purple"><strong>case</strong></span>" statements is an immediate eye-sore for someone who is new to Java and/or who is using some of the newer languages. Java 12 proposes to ease this by allowing multiple labels in a "<span style="color:purple"><strong>case</strong></span>" statement thus allowing <span style="color:purple"><strong>case</strong></span> <span style="color:blue"><em>MON</em></span>, <span style="color:blue"><em>TUE</em></span> instead, for e.g. And now we have the following:</p>
<pre>
public class X {
 
       enum Day {
             MON, TUE, WED, THUR, FRI, SAT, SUN
       };

       @SuppressWarnings("preview")
       public String getDay(Day today) {
             String day = switch(today) {
                    case MON, TUE:
                           break "Blues";
                    case WED:
                           break "Hectic";
                    case THUR, FRI:
                           break "Getting Better";
                    case SAT, SUN:
                           break "Life!";
             };
             return day;
       }
}
</pre>
	<p>No one is ever satisfied - the same goes for the above as well. Folks felt that the presence of multiple "<span style="color:purple">break</span>" statements is just unnecessary ceremony which can be made implicit. Hence Java 12 overloaded the "->" operator to provide the more concise syntax with the final code transformation as shown below:</p>
<pre>
public class X {

       enum Day {
             MON, TUE, WED, THUR, FRI, SAT, SUN
       };

       public String getDay (Day today) {
 
             return switch(today) {
                    case MON, TUE -> "Blues";
                    case WED       -> "Hectic";
                    case THUR, FRI -> "Getting Better";
                    default        -> "Life!";
             };
       }
}
</pre>
	<p>To try out, just copy and paste the code listing above into Eclipse 4.12 or above.</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/image005.jpg"></p>
	<p>This will show the "preview" disabled error. Use the quick-fix of "Enable preview features..." to enable. Subsequently, a <code>&#64;SuppressWarnings</code>(<span style="color:blue">"preview"</span>) can be added above getDay() to suppress the "preview feature used" warning as well. Essentially, after the preview is enabled, Java Compiler section of (project specific, if you may) configuration will look like the following:</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/image006.jpg"></p>
	<p>And a final runnable program, complete with <code>SuppressWarnings</code>  directive and a <em>main()</em> is given below:</p>
<pre>
public class X {
 
       enum Day {
             MON, TUE, WED, THUR, FRI, SAT, SUN
       };
 
       @SuppressWarnings("preview")
       public String getDay(Day today) {
 
             return switch(today) {
                    case MON, TUE  -> "Blues";
                    case WED       -> "Hectic";
                    case THUR, FRI -> "Getting Better";
                    default        -> "Life!";
             };
       }
      
       public static void main(String[] args) {
             System.out.println(new X().getDay(Day.MON));
       }
}
</pre>
	<p>There's just one more detail before we can run a program in preview mode, we need to tell the JVM that the class file is generated with preview enabled and hence the running should also be in the preview enabled mode. This is automatically taken care if you are running within Eclipse; However, if you are planning to use command line JVM to run this class, use "--enable-preview" in the command line.</p>
	<p>Before we forget, the fundamental motivation of preview features is to understand user experience and get feedback. So please do rant, complain, appreciate in the <a href="https://mail.openjdk.java.net/mailman/listinfo/jdk-dev" target="_blank">JDK mailing lists</a>, or for Eclipse Java Development feature requests or modifications do <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi" target="_blank">submit bugs or enhancement requests</a>.</p>
	<p>Friends, <strong>Switch</strong> to <strong>Eclipse</strong> with <strong>Java 12, enable preview</strong> and express yourself more using switch <strong>expressions</strong>, and give feedback to keep Java evergreen! That will be, with due apologies to Neil Armstrong, "<strong>A small step for Java users, to enable a giant step for Java</strong>"!</p>
	
	
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/june/images/manoj.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Manoj Palat<br>
            <a target="_blank" href="https://www.ibm.com">Committer in Eclipse Java Developments tools</a>
          </p>
          <ul class="author-link list-inline">
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>