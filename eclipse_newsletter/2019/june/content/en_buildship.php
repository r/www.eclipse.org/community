<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>If you work with Eclipse you are probably familiar with Buildship, the Eclipse plugins for Gradle. The Buildship 3.1 release allows you to <a href="https://github.com/eclipse/buildship/issues/265">run tasks upon project synchronization</a> and auto build, the two most highly voted issues on Github. In this post, we're going to summarize why this is an essential feature for many and how can you make use of it.</p>	
<h3>Extending project synchronization</h3>
	<p>The Buildship project synchronization functionality imports the Gradle projects into the workspace and configures them to work with the Java toolchain. That - of course - is just the tip of the iceberg. There are many other tools and frameworks out there, and Buildship can't provide configuration for all. Since 3.0, there's a public API to add new integrations, but that requires writing Eclipse plugins. Most users only want to run custom tasks to generate or update configuration files. Running the tasks manually after each change can be frustrating and error-prone. Automatically running the tasks upon project synchronization helps the developers to stay in the flow.</p>
<h3>How it works</h3>	
	<p>To use the new feature, you'll need Buildship 3.1 and a project using Gradle 5.4 and above. In Gradle 5.4 we introduced a new attribute in the <code>eclipse</code> plugin:</p>
<pre>
plugins {
    id 'eclipse'
}

task generateCustomConfig {
    doLast {
          println "Generating custom configuration..."
    }
}

eclipse {
    synchronizationTasks generateCustomConfig
}
</pre>
	<p>That's it. When you import or synchronize the project, you'll see the tasks being executed.</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/taskexec.png"></p>
	<p>Note, that the synchronization task is declared with a task reference. You can actually use different types here: strings specifying the task paths, a list of tasks, and more. Essentially, you can use any <a href="https://docs.gradle.org/current/javadoc/org/gradle/api/Task.html#dependencies" target="_blank">task dependency types</a>.</p>
<h3>Executing tasks during Eclipse build</h3>
	<p>Along with the feature above, we also added the option to run tasks every time the user changes a resource in the workspace, and the Eclipse build is triggered. This feature is useful to execute small code generator and validator tasks. The syntax is very similar:</p>
<pre>
plugins {
    id 'eclipse'
}

task generateCode {
    doLast {
        println 'Generating some code...'
    }
}

eclipse {
    autoBuildTasks generateCode
}
</pre>
<h3>Conclusion</h3>
	<p>This new feature will enable a lot of developers and build authors to provide a smoother work experience within the IDE. Let us know what you think about it and how would you make use if in your own setup.</p>
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/june/images/donat.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Donat Csikos<br>
            <a target="_blank" href="https://www.gradle.org">Senior Software Engineer</a>
          </p>
          <ul class="author-link list-inline">
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>