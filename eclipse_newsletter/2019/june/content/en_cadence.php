<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Eclipse Photon was released in the last week of June 2018. It marked the end of thirteen yearly main releases that were followed up by two (and later three) service releases. At the same time, it marked the beginning of a new way of doing simultaneous releases, the so-called <a href="https://en.wikipedia.org/wiki/Rolling_release" target="_blank">rolling release</a>.</p>
	<p>The first quarterly release, named Eclipse IDE 2018-09, went live on September 19, 2018. It was followed by Eclipse IDE 2018-12 and Eclipse 2019-03 on December 19, 2018, and March 20, 2019, respectively. On June 19, 2019, the Eclipse IDE 2019-06 was released, marking the end of the first full year of quarterly releases. So it's a good time to discuss two of the most frequently asked questions related to the new release cadence.</p>
<h3>Why rolling releases?</h3>
	<p>With the dawn of continuous integration and continuous delivery, rolling releases became popular across the software industry, especially with Linux distributions. Rolling releases replace a single yearly "big bang" release with multiple incremental releases that happen more frequently. The incremental approach reduces the risk of integration problems after extended periods of development. At the same time, one or more maintenance branches are replaced with a single master branch, reducing the maintenance overhead.</p>
	<p>In the case of the Eclipse IDE simultaneous release, the frequency of the releases did not change, but the nature of them did. There are still four releases per year in March, June, September, and December. Instead of one yearly official release in June and two or three service releases, now every release is an official full-fledged release.</p>
	<p>To spread the releases evenly across the year, a thirteen-week release cadence was adapted. This led to two slight changes in the release schedule: both the June and September release now happen one week earlier than in the last years.</p>
<h3>Why are there no code names anymore?</h3>
	<p>Code names (like Galileo, Juno, Luna, Neon, Photon) for the yearly release of the Eclipse IDE were always a hot topic. The community was asked every year to help find the next code name. On the corresponding Bugzilla issues, a lot of potential names were discussed extensively. Some people just liked the fact that it gave the Eclipse IDE summer release a cool name, but the name also influenced the design of the splash screen, logos, and the release website.</p>
	<p>Especially for people that were new to the Eclipse community, the code name posed a mental disconnect or indirection between the release name and the release date. To illustrate the problem:</p>
	<ul>
		<li>Do you know how old the Galileo release is? Was it released in 2009 or 2008?</li>
		<li>Which one is older: Neon or Luna?</li>
		<li>Is Oxygen the latest release?</li>
	</ul>
	<p>In contrast, everyone knows immediately when the Eclipse IDE 2018-09 release happened.</p>
	<p>The release names were also easily confused with a product or project name. For example, Eclipse Galileo was perceived as a different product than Eclipse Europa.</p>
	<p>Discussions about the release name started in 2016 (see bug <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=493490" target="_blank">493490</a>). After deciding that the code name would be dropped, it was not easy to find a new release identifier (see bug <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=532220" target="_blank">532220</a>), but eventually, the format Eclipse IDE &lt;yyyy&gt;-&lt;mm&gt; was chosen.</p>
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/june/images/fred.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Frederic Gurr<br>
            <a target="_blank" href="https://www.eclipse.org">Release Engineer</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fr3dg">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>