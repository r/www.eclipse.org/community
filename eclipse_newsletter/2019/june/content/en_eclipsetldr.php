<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>TL;DR: Try the new Eclipse IDE for Web and JavaScript developers package (<a href="https://www.eclipse.org/downloads/packages/" target="_blank">from usual installer or direct download</a>). it's worth it!</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/package.png"></p>
	<p>If you've used the Eclipse IDE to develop modern web applications with newer HTML, CSS, JavaScript in the past, you probably sometimes had the feeling that things could be much smoother, that Eclipse IDE was a bit late compared to other IDEs when in come to the web front-end or JavaScript backend development story. Well, be happy, things are changing, for the best!</p>
<h3>The move towards Language Servers and Debug Adapters for Web development in Eclipse IDE</h3>
	<p>There are a bunch of good reasons, some technical ones and some related to community priorities and organization that could explain very well this former relatively bad state; but fortunately there are now newer approaches of doing development tools that allow to get rid of such issues, reducing greatly the development effort while providing a more up-to-date, productive, unified and fluent experience to end-users.</p>
	<p>As you may have read in <a href="https://www.eclipse.org/community/eclipse_newsletter/2017/may/" target="_blank">some</a> <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/january/article5.php" target="_blank">previous</a> <a href="https://www.eclipse.org/community/eclipse_newsletter/2017/august/article3.php" target="_blank">Eclipse</a> <a href="https://www.eclipse.org/community/eclipse_newsletter/2018/march/jdtls.php" target="_blank">newsletters</a>, the Language Server Protocol and Debug Adapter Protocol now offers a very efficient and factorized way to implement language support in different IDEs. Visual Studio Code itself includes very good language servers for HTML, CSS and JSon; some good language servers exist in the Eclipse ecosystem for <a href="https://github.com/redhat-developer/yaml-language-server/" target="_blank">Yaml</a>, <a href="https://github.com/theia-ide/typescript-language-server" target="_blank">JavaScript, TypeScript</a> and <a href="https://github.com/angelozerr/lsp4xml" target="_blank">XML</a>. The simple yet working idea is to adopt such Language Servers as the fundation of the Web development stack in Eclipse IDE to Language Servers, in place of legacy and relatively hard to maintain Eclipse IDE-specific support for those languages.</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/wwd.jpg"></p>
	<p>Adopting those language servers is the essence of the <a href="https://marketplace.eclipse.org/content/eclipse-wild-web-developer-web-development-eclipse-ide" >Eclipse Wild Web Developer project</a>. Consuming the language servers listed above, it has become one of the most productive too to handle the following workflows:</p>
		<ul>
			<li>Developing a static HTML+CSS web page</li>
			<li>Developing and debugging a JavaScript/Node.js application</li>
		</ul>
	<p><a class="eclipsefdn-video" href="https://youtu.be/mTnELEDgi8U"></a></p>
		<ul>
			<li>Developing a TypeScript and/or Angular web application</li>
			<li>Editing some JSON, YAML, XML configuration files (supports schema)</li>
			<li>Editing Kubernetes configuration files</li>
		</ul>
<h3>Revamping the Eclipse IDE package</h3>
	<p>As Wild Web Developer easily provided more value for the most common cases than legacy technologies, we switched the Eclipse IDE for Web and JavaScript developers package to adopt it. This combined with other great features of the IDE provided by other projects in the community can allow to combine those technologies to enable some even more powerful workflows, leveraging typical Eclipse IDE features like</p>
		<ul>
			<li>"Auto-refresh" feature of the internal Web browser to see updates in HTML file on save</li>
		</ul>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/auto-refresh.gif"></p>
		<ul>
			<li>Integration of a terminal and Launch Configuration to easily manage automated builds and executions</li>
		</ul>
	<p><a class="eclipsefdn-video" href="https://youtu.be/qZQojJUsN4o"></a></p>
		<ul>
			<li>Integration of Remote Filesytem Explorer and and Server Adapters to easily deploy content</li>
			<li>Easy team collaboration with EGit and Mylyn</li>
			<li>Strong customization and extensibility capability to tailor the IDE to your needs</li>
		</ul>
	<p>With this set of good old features added to a more powerful and nicer edition and debug of Web files, the Eclipse IDE now shine again as a great ready-to-use IDE for Web and JavaScript development. It can now be considered for many use-cases by Web or Node.js developer as a viable or better alternative to other IDEs.</p>
	<p>If you're already using Eclipse IDE, just adding Wild Web Developer enables you to become a productive Web developer without having to use a separate IDE for that.</p>
<h3>Conclusion</h3>
	<p>Use <em>Eclipse IDE for Web and JavaScript developers</em>! Use Eclipse Wild Web Developer plugin! <strong>It just works</strong>!</p>
	<p align="left"><img class="img-responsive" width="100%" src="/community/eclipse_newsletter/2019/june/images/package.png"></p>
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/june/images/mickael.jpg"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	Mickael Istria<br>
            <a target="_blank" href="https://www.eclipse.org">Eclipse Developer</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mickaelistria">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>