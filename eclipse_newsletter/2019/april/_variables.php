<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

$pageTitle = "Science @ the Eclipse Foundation";
$pageKeywords = "eclipse, newsletter, eclipse science, eclipse xacc, eclipse ICE, eclipse, eclipse SWTchart, April, 2019, eclipse foundation, eclipse iot, iot developer survey survey, iot world, JAX 2019, Bosch Connected World, eclipse iot day santa clara";
$pageAuthor = "Christopher Guindon";
$pageDescription = "This month, the newsletter will highlight how the Eclipse Science Working Group is bringing industry, academia, and government together to develop reusable open source software for scientific research. And, by the looks of it, the result of their hard work is undeniable.Read the exciting articles in this month's Eclipse Newsletter!";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "";
  $sponsorLink = "";
  $sponsorImage = "";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;