<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Single-Source Pythonic Programming for Quantum-Accelerated Computing with Eclipse XACC";
  $pageKeywords = "eclipse, newsletter, eclipse science, science, eclipse foundation, Eclipse XAAC, Zachary Parks, Alex McCaskey, April, 2019, eclipse project, eclipse community, open source, opensource";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "With the advent of potential quantum acceleration, a major focus is being placed on how we can provide high-level programming models and extensible, open-source software to allow for the offloading of select computational tasks to attached quantum accelerators. Luckily, research being done at Oak Ridge National Laboratory on how to provide this important software infrastructure has resulted in the production of Eclipse XACC.";

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");