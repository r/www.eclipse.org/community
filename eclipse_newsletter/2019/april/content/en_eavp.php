<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<br>
	<p>The Eclipse Advanced Visualization Project (EAVP)[1] is a versatile and powerful way to embed visualizations, especially scientific visualizations, inside of an Eclipse workbench. Since its release in 2016, EAVP has relied on Open Service Gateway Initiative services devoted to individual rendering capabilities to provide other programs with its visualizations. These services were fairly heavyweight-covering everything from file parsing, to publishing information about the visualized data, to rendering the visualizations themselves-and were tied strongly to the Eclipse architecture. This provided the motivation for refactoring the project to a web-based microservice architecture, with which a visualization server could provide only those services it needed and would be able to stream visualizations to other programs over HTML.</p>
	<br>
	<p>The high-level goal for this architecture is to provide visualizations to other arbitrary programs, from Eclipse workbenches to Vaadin applications to simple HTML web pages, which need only to provide the file to be visualized to the EAVP instance's URL. EAVP will be deployed as a series of .war files, one per service, to be deployed to the user's web server of choice. Generic requests for visualization will also be handled by the base service, which will forward the input to a series of other services that can collectively create a visualization for it. These services will fall into one of several diverse categories, each handling a different step of the visualization process.</p>
	<br>
	<p>The first kind of service in the pipeline is a file parsing service. Rather than rewrite file parsers for every different kind of visualization, there will be one file parser service per file type that is responsible for creating an internal EAVP data type containing that file's contents. After the base service receives this internal representation back from a parser service, it will then forward it to the second kind of service-the visualization service. The visualization services will  accept input in the format of the internal EAVP data type, simplifying and standardizing the visualization engine's acceptance of data to visualize. The visualization service will then be responsible for rendering a visual representation of the data and ultimately producing content that can be embedded directly into HTML to display the created visualization. These visualizations could be as simple as an image or as complex as HTML to post to a Vaadin client that provides interactive access to the visualization.</p>
	<br>
	<p>Tying into these other services will be the Resource Description Framework (RDF) service, which handles communication with an RDF data store. As a preprocessing step before attempting to handle requests to themselves, file parsing and visualization services will be able to calculate a unique identifier (such as a hash) for their input and call back to the base service with it. The base service will then check whether a data store service is registered and pass the unique identifier and requesting service to it. If the RDF store already contains the end product of the service, which would be an internal EAVP data representation object for file parser services or some domain specific visualization object such as an image file for a visualization service, then the base service will alert the other service to stop work and pass the cached result on as normal. This allows the system to avoid having to parse or visualize the exact same data multiple times in cases where a user is, for example, repeatedly switching between visualizing two files to compare them. If the RDF store does not already possess the data, then once the file parser or visualization service is finished the base service will forward the data to the RDF service for storage.</p>
	<br>
	<p>One final service that bears mentioning is the state service. To provide user interaction to otherwise static visualizations like images, the state service allows for storing the information about the state of the visualization as the user performs operations on it. For example, if the user clicks an image of a graph, the data point clicked can be calculated by the visualization service and then be stored with a session ID and the service name through the RDF service. Then when another call is made to the state service to retrieve that state for a session and service, that data point can be returned. This would allow the programmer to respond to the user's actions with the data from the visualization, such as creating a pop-up with the exact value of a data point the user clicked on an image. Clients that provide examples of how to make these API calls for an EAVP visualization will be provided for easy out-of-the-box use in other programs.</p>
	<br>
	<p>Collectively, the EAVP microservices will provide a full suite for embedding arbitrary visualizations within other programs over HTML. A wide variety of services are planned, from plots to maps, all of which will be easy for programmers to obtain and provide interactions for through a simple and standardized API. EAVP's source code is currently available through its GitHub repository[2].</p>
	<br>
	<p><a href="https://projects.eclipse.org/proposals/eclipse-advanced-visualization-project">[1] https://projects.eclipse.org/proposals/eclipse-advanced-visualization-project</a></p>
	<p><a href="https://github.com/eclipse/eavp">[2] https://github.com/eclipse/eavp</a></p>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/april/images/Robert.jpg"
        alt="Robert Smith" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Robert Smith<br />
            <a target="_blank" href="https://www.ornl.gov/">Oak Ridge National Laboratory</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/SmithRWORNL">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>