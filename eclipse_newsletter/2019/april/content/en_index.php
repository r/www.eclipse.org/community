<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.30.04</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2019/january/images/me.png" />
                            <h3>Editor's Note</h3>
                             <p>The goal of the <a href="https://science.eclipse.org/">Eclipse Science Working Group</a> is to bring industry, academia, and government together to develop reusable open source software for scientific research. And, by the looks of it, the result of their hard work is undeniable.</p>
                            <p>Take, for example, <a href="https://www.eclipse.org/ice/">Eclipse ICE</a>; its latest version, 3.0 ("ICE III"), is a full redesign and nearly full re-implementation of the entire ICE platform that uses workflow aggregation and microservices to combine the core ICE framework with other workflow management systems. Speaking of improvements, <a href="https://projects.eclipse.org/projects/science.xacc">Eclipse XACC's</a> recent updates have enabled an expressive, single-source Pythonic approach for productive programming workflows on near-term quantum computers. Read more about Eclipse ICE and Eclipse XACC in this issue!</p>
                            <p>We're also checking up on <a href="https://projects.eclipse.org/projects/science.eavp">Eclipse Advanced Visualization Project</a> and <a href="https://projects.eclipse.org/projects/science.swtchart">Eclipse SWTChart</a> and see how they are doing. Explore the great articles below, but also be sure to check out the Eclipse Science Working Group to explore other projects!</p>
								<p> &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/ice.php">The Future of Scientific Workflows and What That Means for Eclipse ICE</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/xacc.php">Single-Source Pythonic Programming for Quantum-Accelerated Computing with Eclipse XACC</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/eavp.php">Microservice Architecture for the Eclipse Advanced Visualization Project</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/SWTChart.php">Eclipse SWTChart</a><br>
							<p>There are a few events coming up soon. Check them out here and register to learn the latest on open source, open standards for building IoT solutions and cloud-native Java:</p>
                            <ul>
 							 <li><a href="https://jax.de/">JAX 2019</a></li>
  							 <li><a href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2019/">Eclipse IoT Day Santa Clara</a>, co-located with <a href="http://bit.ly/2Olde9p">IoT World</a></li>
  							 <li><a href="https://bosch-connected-world.com">Bosch Connected World</a></li>
							</ul>
                            <p>We are proud to announce that our <a href="https://iot.eclipse.org/iot-developer-surveys/">2019 IoT Developer Survey results</a> are now available! Check out the findings to learn more about the key trends that are happening in this ever-changing environment and don't forget to read the blogs of <a href="https://eclipse-foundation.blog/2019/04/17/2019-iot-developer-survey/">Mike Milinkovich</a> and <a href="https://blogs.eclipse.org/post/fr%C3%A9d%C3%A9ric-desbiens/iot-developer-survey-2019-key-trends">Frederic Desbiens</a> on their thoughts and reasoning behind the key results!</p>
                            <p>While you're at it, make sure to check out our <a href="https://iot.eclipse.org/iot-developer-surveys/">previous surveys</a>.</p>
                            <p>Happy reading!</p>
                            <p>Jameka Woodberry</p>
                            <p><a style="color:#000;" target="_blank" href="https://twitter.com/marketing_jamy">@Marketing_Jamy</a></p>

                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/ice.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/april/images/ice.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/ice.php"><h3>The Future of Scientific Workflows and What That Means for Eclipse ICE</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/xacc.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/april/images/xacc.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/xacc.php"><h3>Single-Source Pythonic Programming for Quantum-Accelerated Computing with Eclipse XACC</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/eavp.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/april/images/eavp.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/eavp.php"><h3>Microservice Architecture for the Eclipse Advanced Visualization Project</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/SWTChart.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/april/images/swt.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/april/SWTChart.php"><h3>Eclipse SWTChart</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/jakarta-ee-examples">Jakarta EE Examples</a> was created to provide Jakarta EE examples that can be used freely without the need to add any license to the given examples when copying.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-tempest">Eclipse Tempest</a> will start by providing an IDE-agnostic library for tools required for cloud application development.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-kiso">Eclipse Kiso</a> was designed from scratch as a software development kit for IoT devices and has already been used and verified on a handful of existing products in the market. Eclipse Kiso's reusability, robustness and portability is a key factor which enables fast development and quick time to market for almost all kinds of IoT "Things" product development.</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.mosquitto">Eclipse Mosquitto</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.kura">Eclipse Kura</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.tycho">Eclipse Tycho</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.app4mc">Eclipse APP4MC</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.passage">Eclipse Passage</a></li>
                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.openj9">Eclipse OpenJ9</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2019/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/march/images/iot-day-santa-clara.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="http://bit.ly/2Olde9p"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/april/images/iotworld.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                           <p><a target="_blank" style="color:#000;" href="https://jax.de/">JAX 2019</a><br>
                           May 6-10, 2019 | Mainz, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.redhat.com/it/summit/2019">Red Hat Summit</a><br>
                           May 7-9, 2019 | Boston, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://iot.eclipse.org/eclipse-iot-day-santa-clara-2019/">Eclipse IoT Day Santa Clara</a><br>
                           May 13, 2019 | Santa Clara, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.dlr.de/ts/en/desktopdefault.aspx/tabid-3930/20125_read-53575/">Eclipse SUMO User Conference 2019</a><br>
                           May 13-15, 2019 | Berlin-Adlershof, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="http://bit.ly/2Olde9p">IoT World</a><br>
                           May 13-16, 2019 | Santa Clara, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://bosch-connected-world.com/">Bosch Connected World</a><br>
                           May 15-16, 2019 | Berlin, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://events.linuxfoundation.org/events/kubecon-cloudnativecon-europe-2019/">KubeCon Europe 2019</a><br>
                           May 20-23, 2019 | Barcelona, Spain
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->