<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>With almost 40 Eclipse Internet of Things (IoT) projects to choose from, developers have the
  flexibility to mix and match the specific projects that are ideal for their IoT application
  strategies and goals. But, all of this flexibility and choice comes with some heavy costs for
  developers.</p>
<p>Here are just a few of the many reasons the Eclipse IoT community needs to focus on creating
  packages that make it faster and easier for IoT application developers to find the open source
  code they need and jump-start application development.</p>
<h2>Open Source Projects Are Critical for IoT</h2>
<p>The fact that there are around 40 contributed Eclipse IoT projects highlights how important
  developers believe open source code is for IoT applications. Now, it&rsquo;s time for the Eclipse
  IoT community to help developers make better use of the projects that have been submitted so far,
  drive widespread adoption of Eclipse IoT projects, and encourage additional submissions.</p>
<p>With open source IoT projects, developers can avoid tying their applications to a single
  vendor&rsquo;s IoT technology and strategy. While vendor-specific solutions might give developers
  what they need today, locking in to the vendor&rsquo;s approach over the long-term can be
  restrictive and risky. APIs can change, prices can increase, and roadmaps may no longer align with
  the way the application needs to evolve.</p>
<p>Locking in to a specific vendor also typically locks developers into cloud-based solutions. But,
  many IoT solutions require on-premises deployments where each application component runs in the
  same environment as the end devices. Manufacturing facilities that leverage IoT applications are a
  prime example. It&rsquo;s not unusual for these facilities to rely exclusively on LAN connections
  to enable IoT applications. Developers must have access to IoT technologies their customers can
  deploy anywhere, and fully operate and control themselves.</p>
<p>Development time and budget also come into play. It often doesn&rsquo;t make sense for developers
  to create all of the application functionality themselves. For example, most IoT applications
  include a connectivity layer that provides data from sensors and devices to a back-end system for
  analysis. This functionality is mandatory, but it&rsquo;s not a differentiator or a unique selling
  point. When developers can reuse open source code for this type of lower level functionality, they
  can focus on the higher-level layers where they add more value while saving time and money.</p>
<h2>There Is No Grand Design to Today&rsquo;s IoT Projects</h2>
<p>Today, there is no alignment among the contributed projects. It&rsquo;s simply a collection of
  IoT projects that were started by individuals who were working to solve a particular problem and
  wanted to share the results of their efforts with others.</p>
<p>The person who started one project usually had no involvement with, or knowledge of, other IoT
  projects. Projects are in varying states of development, and they target a wide variety of issues.
  They are not integrated with one another, and not necessarily even compatible for use in the same
  application. In some cases, multiple projects solve similar problems, but within the context of a
  specific application, so each one solves the problem in a different way.</p>
<h2>Unless You&rsquo;re Already an IoT Expert, It&rsquo;s Overwhelming</h2>
<p>Depending on the developer&rsquo;s level of expertise and experience with IoT applications, it
  can be overwhelming to sort through a long list of projects whose purpose they don&rsquo;t
  understand and can&rsquo;t quickly grasp. The unfortunate reality is that only highly experienced
  IoT application developers will be able to quickly and easily determine which projects fit into
  which layer of the application architecture and which projects are the best choice for a
  particular functionality.</p>
<p>A large part of the problem is that projects are primarily described by the technical
  capabilities they provide. This makes it difficult for developers who are new to the IoT
  application domain to determine how and where each project fits into an IoT application
  architecture &mdash; despite their programming knowledge and previous experience. It&rsquo;s not
  that IoT applications are harder to understand, they just require a different set of knowledge.</p>
<p>For example, a developer with a strong background in enterprise applications will already have a
  good understanding of vertical scalability. However, if that same developer is now tasked with
  developing a cloud-based IoT application, it may be difficult for them to immediately understand
  how horizontal scalability can be achieved in the context of cloud-related IoT projects because
  that&rsquo;s not part of their past experience.</p>
<p>The Eclipse IoT homepage provides a couple of ways to filter and sort projects, but again,
  developers must already understand every level in an IoT architecture and know what they&rsquo;re
  looking for before they start. There&rsquo;s no point in filtering by devices, gateways, or cloud
  stacks if you don&rsquo;t know whether or why you would need the functionality. For example, to
  find the right gateway project, a developer must already know:</p>
<ul>
  <li>The constraints that using a gateway typically imposes</li>
  <li>Why a gateway is the right choice for the application versus implementing the same
    functionality on the back end in the cloud</li>
</ul>
<p>
  Today, there&rsquo;s no real shortcut around the need to take a deep dive into the documentation
  for projects to better understand what they really do and whether they&rsquo;re a good fit for a
  particular IoT application. The two <a href="https://iot.eclipse.org/testbeds/">Open Testbeds</a>
  can be used as examples of how particular IoT projects can be combined in IoT applications. And,
  it&rsquo;s worth searching the web to see whether anyone else has developed a similar IoT
  application using Eclipse IoT projects, but that&rsquo;s about it.
</p>
<h2>Integration Is a Big Task</h2>
<p>Once individual IoT projects are selected, integrating them is another significant and
  time-consuming task. Developers must determine the external behaviors that are exposed in each
  project, learn the APIs, and figure how much mapping is required between projects. More often than
  not, developers must also understand the internal workings of projects to successfully integrate
  them.&nbsp;</p>
<p>So, while developers can use Eclipse IoT projects to save the time required to implement the
  functionality themselves, they will almost certainly spend considerable time and effort on
  integration tasks.</p>
<h2>Packages Significantly Lower Barrier to Entry</h2>
<p>The challenges of getting started with Eclipse IoT projects today create a high barrier to entry
  and won&rsquo;t help us improve adoption of open source IoT technologies.</p>
<p>
  The good news is the Eclipse IoT community has recognized the challenges with the current
  situation and has started a <a href="https://projects.eclipse.org/projects/iot.packages">packages
    project</a> that will simplify developers&rsquo; experience and make it much easier to choose
  and use Eclipse IoT projects.
</p>
<h2>Get More Information</h2>
<p>
  For a closer look at the packages project and our goals for improving Eclipse IoT, be sure to read
  <a href="/community/eclipse_newsletter/2019/october/a_close_look.php">Jens
    Reimann&rsquo;s article</a> in this newsletter.
</p>
<p>And, as always, I would encourage you to get involved and share your ideas and expertise so
  together we can realize the potential of open source IoT projects at the Eclipse Foundation.&nbsp;</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/october/images/kai.jpg" alt="Kai Hudalla"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Kai Hudalla</p>
          <p>Chief Software Architect<br>
          Bosch Software Innovations GmbH</p>
        </div>
      </div>
    </div>
  </div>
</div>