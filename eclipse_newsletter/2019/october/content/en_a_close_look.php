<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>
  In this article, I&rsquo;ll explain how the<a
    href="https://projects.eclipse.org/projects/iot.packages"
  > Eclipse IoT Packages project</a> aims to address the many challenges with Eclipse IoT projects
  to benefit IoT application developers and enterprises that deploy IoT applications. If you would
  like to learn more about the challenges with Eclipse IoT projects, read the <a
    href="/community/eclipse_newsletter/2019/october/why_eclipse_iot.php"
  >accompanying article</a> by my community colleague, Kai Hudalla.
</p>
<p>
  The overall goal of the Eclipse IoT Packages project is to make it easier for people to choose and
  combine the<a href="https://iot.eclipse.org/projects/"> Eclipse IoT projects</a> that are offered
  today as individual building blocks. We want developers to be able to determine, within 15
  minutes, whether a set of Eclipse IoT projects is relevant to their application and offers the
  right benefits so they can quickly decide whether they should invest more time exploring those
  projects.
</p>
<h2>Use Case Packages Showcase Project Potential</h2>
<p>
  To achieve this goal, the<a href="https://projects.eclipse.org/projects/iot.packages/who"> IoT
    Packages project team</a> is developing use cases that demonstrate how two or more Eclipse IoT
  projects can be used together to deliver particular functionality or address a particular
  challenge. Each use case will use Kubernetes as the base and will include:
</p>
<ul>
  <li>A document that explains what developers can accomplish with the package and describes the
    basic steps to start using it</li>
  <li>The Helm charts needed to deploy the Eclipse IoT projects in the package on the Kubernetes
    platform</li>
</ul>
<p>Developers can simply download the use case package, install it, run it, and execute a few tests
  to determine whether the package meets their needs. It&rsquo;s a &ldquo;Hello World!&rdquo;
  experience for this particular combination of Eclipse IoT projects. A use case package
  doesn&rsquo;t give developers a complex set up for IoT application development, but it does give
  them a working set up that&rsquo;s ideal for basic project evaluation.</p>
<p>To ensure the use cases are as relevant and helpful to as many developers as possible,
  we&rsquo;re working to develop scenarios that are specific enough that people can easily
  understand what they achieve, but general enough to show they can be used in different ways. For
  example, we won&rsquo;t focus on developing use cases that solve a very specific problem, or that
  apply only to a single vertical industry.&nbsp;</p>
<h2>Package Zero Is the First Step</h2>
<p>The first package we&rsquo;re working on is a good example of what we&rsquo;re trying to achieve.
  This package, which has the working name Package Zero, includes:</p>
<ul>
  <li><a href="https://www.eclipse.org/hono/">Eclipse Hono</a> for device connectivity</li>
  <li><a href="https://www.eclipse.org/ditto/">Eclipse Ditto</a> for managing and organizing digital
    representations of physical objects &mdash;digital twins</li>
  <li><a href="https://www.eclipse.org/hawkbit/">Eclipse hawkBit</a> for rolling out and managing
    software updates to IoT devices</li>
</ul>
<p>It&rsquo;s easy to see that this combination of functionality can be used across any number of
  IoT applications in almost any industry. We know we have Eclipse IoT community members with
  expertise and experience using and combining these projects, so it was a natural package to start
  with.&nbsp;</p>
<p>Over time, we may also be able to provide packages of fully integrated Eclipse IoT projects. But
  providing use cases is our first major step toward helping IoT application developers quickly and
  easily select the right combination of Eclipse IoT projects for their goals.</p>
<h2>Success Relies on Community Involvement</h2>
<p>While Package Zero is being developed by the Eclipse IoT Packages project team, we don&rsquo;t
  see packages as something that only we create for the community to consume. In the spirit of open
  source, and building on the work of others to extend and improve what already exists, we very much
  welcome package ideas and contributions from all members of the Eclipse IoT ecosystem.</p>
<p>
  Contributing to the project is pretty simple. A new package must showcase your vision for
  combining two or more Eclipse IoT projects, and it must include documentation that describes the
  value this combination provides. To contribute an Eclipse IoT package, you also need to be an<a
    href="https://www.eclipse.org/membership/become_a_member/committer.php"
  > Eclipse committer</a>. If you&rsquo;re not currently a committer, this may be the right time to
  become one.
</p>
<p>Once an Eclipse IoT package is released, we&rsquo;re also very interested in ideas for expanding
  it with one or more additional IoT projects. We&rsquo;d love to see IoT use cases strengthened in
  this way and we strongly encourage you to keep that possibility in mind as you use Eclipse IoT
  projects and work with use cases such as Package Zero and those that follow.</p>
<p>Here are just three of the additional benefits we will realize when the Eclipse IoT community
  strongly engages and participates in creating and building up IoT use case packages:&nbsp;&nbsp;</p>
<ul>
  <li>Developers who are new to the world of IoT applications can browse a broad selection of
    Eclipse IoT use cases to get ideas for IoT applications and educate themselves about
    what&rsquo;s possible. For example, they may discover a use case that enables an IoT application
    that&rsquo;s ideal for their enterprise employer. Or, they may find a use case that solves a
    problem they didn&rsquo;t realize they were likely to encounter when developing their IoT
    application.</li>
  <li>Developers can look for use cases that jumpstart their development efforts to save time and
    money. For example, they may find a use case that provides 80 percent of the functionality they
    need, so they only need to invest significant time in the remaining 20 percent. In the ideal
    scenario, they then contribute the final 20 percent back to the Eclipse IoT community to improve
    the original use case for others.</li>
  <li>Developers and enterprises that specialize in IoT application development and deployment, or a
    specific area of IoT, can establish themselves as IoT community and industry thought leaders by
    contributing advanced and innovative use cases, and by improving existing use cases.</li>
</ul>
<h2>More Information to Come</h2>
<p>
  As we get closer to the release of our first use case package, we&rsquo;ll be updating the <a
    href="https://eclipse.org/packages"
  >Eclipse IoT Packages project web page</a> with more information and adding content to the GitHub
  repository. You&rsquo;re also very welcome to use the GitHub issue tracker to provide feedback,
  make comments, or ask questions.
</p>
<p>When you&rsquo;re exploring the information and resources that we make available, be aware that
  we&rsquo;re trying to build the Eclipse IoT Packages project in an open environment, so everyone
  can see what we&rsquo;re doing. Obviously, that comes with challenges. We welcome your comments
  and honest feedback and we encourage you to make suggestions, but please keep in mind that this is
  very much a work in progress and we&rsquo;re still in the early stages of the project.&nbsp;</p>
<p>
  In the meantime, if you would like more background about how the Eclipse IoT Packages project
  started and the IoT projects in our first package, read my blog about our <a
    href="https://dentrassi.de/2019/09/10/from-building-blocks-to-iot-solutions/"
  >evolution from building blocks to IoT solutions</a>.&nbsp;
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/october/images/jens.jpg" alt="Jens Reimann"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Jens Reimann</p>
          <p>
            Principal Software Engineer, Messaging & IoT<br> Red Hat
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ctron">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>