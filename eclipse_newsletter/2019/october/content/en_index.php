<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.21.10</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img width="200" class="float-left margin-right-20 margin-bottom-25" src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/thabang.jpg" />
                          <h3>Editor's Note</h3>
                          <p>In this month&rsquo;s newsletter, we&rsquo;re focusing on
                          <a href="https://iot.eclipse.org/">Eclipse IoT</a> and the exciting advances our community is making in IoT and
                          edge technologies.</p>
                        <p>
                          I&rsquo;ll start with our articles about the<a
                            href="https://projects.eclipse.org/projects/iot.packages"
                          > Eclipse IoT Packages project</a>. To give you the complete perspective
                          on the project, we asked two of our community authors to tell the story
                          from a different perspective:
                        </p>
                        <ul>
                        <li>To explain why <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/why_eclipse_iot.php"
                          >Eclipse IoT needs packages</a>, Kai Hudalla reviews the challenges that
                          make it hard to use Eclipse IoT projects today.</li>
                        <li>To provide <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/a_close_look.php"
                          >a closer look at the Eclipse IoT Packages project</a>, Jens Reimann
                          explains how use cases will make it easier to choose and combine<a
                            href="https://iot.eclipse.org/projects/"
                          > Eclipse IoT projects</a>.</li>
                        </ul>
                        <p>
                          We&rsquo;re also introducing the <a
                            href="https://www.eclipse.org/org/workinggroups/eclipse_edge_charter.php"
                          >Edge Working Group</a>, a new open industry collaboration that will drive
                          the evolution and broad adoption of edge computing and related
                          technologies. <a href="https://iofog.org/">Eclipse ioFog</a> Project Lead,<a
                            href="https://projects.eclipse.org/user/8025"
                          > Kilton Hopkins</a>, explains why the Eclipse Foundation is focusing on
                          edge computing, the relationship between edge and IoT, and the motivations
                          behind creating the new working group.&nbsp;
                        </p>
                        <p>
                          For insight into the diversity and trends revealed in our IoT project
                          releases, check out Fr&eacute;d&eacute;ric Desbiens&rsquo; article
                          explaining <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/eclipse_iot_releases.php"
                          >why our release history is a roadmap to the future</a>.
                        </p>
                        <h2>EclipseCon Kicks Off</h2>
                        <p>
                          By the time you read this, <a href="https://www.eclipsecon.org/europe2019">EclipseCon
                            Europe 2019</a> will be underway. As our community&rsquo;s biggest
                          gathering of the year, EclipseCon Europe brings together developers,
                          architects, and open source business leaders from around the world.
                          We&rsquo;re excited to share our recent innovations around cloud native
                          Java, the Eclipse IDE and cloud development tools, IoT and edge computing,
                          automotive development, and much more. Stay tuned for community updates
                          and news from the conference.
                        </p>
                        <h2>Check Out Our Short Takes</h2>
                        <p>
                          Finally, this month&rsquo;s newsletter also includes a new section called
                          <a
                            href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/short_takes.php"
                          >Short Takes</a>. Short Takes highlights late-breaking news from the
                          Eclipse Foundation and provides important reminders and links for upcoming
                          events, so be sure to check it out.
                        </p>
                        <p>
                          Happy reading!<br /> Thabang Mashologu<br>
                          Follow <a href="https://twitter.com/t_mashologu">@t_mashologu</a>
                        </p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/why_eclipse_iot.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/1.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/why_eclipse_iot.php"><h3>Why Eclipse IoT Needs Packages</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/a_close_look.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/2.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/a_close_look.php"><h3>A Closer Look at the Eclipse IoT Packages Project</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/discover.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/3.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/discover.php"><h3>Discover The Edge Working Group</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/eclipse_iot_releases.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/4.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/eclipse_iot_releases.php"><h3>Eclipse IoT Releases: Why They’re a Roadmap to the Future</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/short_takes.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/october/images/5.jpg"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/october/short_takes.php"><h3>Short Takes</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 style="margin-bottom:10px;">New Project Proposals</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-jkube">Eclipse JKube</a> provides plugins and libraries for the Java build tools for building and deploying applications for Kubernetes.</li>
                                <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-supervisory-control-toolkit">Eclipse Supervisory Control Toolkit</a> creates a toolkit for the development of supervisory controllers.</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                                <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-cloe">Eclipse Cloe</a> provides simulation middleware and simulation-engine bindings for connecting simulation engines to the "software under test".</li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 style="margin-bottom:10px;">New Project Releases</h3>
                      <p>The combination of the quarterly simultaneous release and the Jakarta EE 8 release have made this a busy month for releases.</p>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a href="https://projects.eclipse.org/projects/iot.hono">Eclipse Hono </a></li>
                            	<li><a href="https://projects.eclipse.org/projects/ecd.orion">Eclipse Orion </a></li>
                            	<li><a href="https://projects.eclipse.org/projects/iot.kapua">Eclipse Kapua</a></li>
                            	<li><a href="https://projects.eclipse.org/projects/technology.sumo">Eclipse SUMO 1.3.1&nbsp;</a></li>
                            	<li><a href="https://projects.eclipse.org/projects/technology.openj9">Eclipse OpenJ9 0.17.0&nbsp;</a></li>
                            	<li><a href="https://projects.eclipse.org/projects/science.triquetrum">Eclipse Triquetrum 0.3.0&nbsp;</a></li>
                            	<li><a href="https://projects.eclipse.org/projects/ecd.che">Eclipse Che 7.0.0&nbsp;</a></li>
                            	<li><a href="https://projects.eclipse.org/projects/science.xacc">Eclipse XACC 1.0.0&nbsp;</a></li>
                              <li><a href="https://projects.eclipse.org/projects/technology.steady">Eclipse Steady </a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a href="https://projects.eclipse.org/projects/technology.recommenders">Eclipse Code Recommenders </a></li>
                              <li><a href="https://projects.eclipse.org/projects/technology.packaging.mpc">Eclipse Marketplace Client 1.8.0</a></li>
                              <li><a href="https://projects.eclipse.org/projects/modeling.m2t.jet">Java Emitter Templates (JET2)</a></li>
                              <li><a href="https://projects.eclipse.org/projects/science.january">Eclipse January 2.3.0&nbsp;</a></li>
                              <li><a href="https://projects.eclipse.org/projects/tools.wildwebdeveloper">Eclipse Wild Web Developer 0.5.0&nbsp;</a></li>
                              <li><a href="https://projects.eclipse.org/projects/locationtech.proj4j">LocationTech Proj4J 1.1.0&nbsp;</a></li>
                              <li><a href="https://projects.eclipse.org/projects/iot.4diac">Eclipse 4diac 1.11.0&nbsp;</a></li>
                              <li><a href="https://projects.eclipse.org/projects/modeling.papyrus-xtuml">Eclipse Papyrus for xtUML</a></li>
                            </ul>

                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2019"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/ece.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2019">EclipseCon Europe 2019</a><br>
                           October 21-24, 2019 | Ludwigsburg, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://devoxx.be/">Devoxx Belgium</a><br>
                           November 4-8, 2019 | Antwerp, Belgium</p>
                           <p><a target="_blank" style="color:#000;" href="http://www.embeddedconference.se/app/netattm/attendee/page/82448">Embedded Conference Scandinavia</a><br>
                           November 5-6, 2019 | Stockholm, Sweden</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.eclipse.org/4diac/en_eve.php?event=2019_4days">4days of 4diac</a><br />
                            November 11-14, 2019 | Linz, Austria</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.continuouslifecycle.de/">Continuous Lifecycle 2019</a><br>
                           November 12-15, 2019 | Mannheim, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/">KubeCon North America 2019</a><br>
                           November 18-21, 2019 | San Diego, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.edgecomputingworld.com/?gclid=EAIaIQobChMIsNTb0K-j5QIVDfDACh23jQdkEAAYASAAEgL9dvD_BwE">Edge Computing World</a><br />
                            December 9-12, 2019 | Silicon Valley, CA</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/clouds.htm">Jakarta EE, Eclipse MicroProfile and Clouds</a><br>
                           December 11, 2019 | Munich Airports</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/streaming.htm">Streaming Architectures with Jakarta EE and Eclipse MicroProfile</a><br>
                           December 13, 2019 | Munich Airports</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/microservices.htm">Jakarta EE and Eclipse MicroProfile Microservices</a><br>
                           December 19, 2019 | Munich Airports</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->