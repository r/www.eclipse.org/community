<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>In Eclipse IoT, each project release is a meaningful milestone. Many Eclipse IoT projects spend
  years in incubation before they&rsquo;re released. And they all follow the rigorous Eclipse
  development process, so they don&rsquo;t play games with version numbers. Looking back at recent
  Eclipse IoT releases, and ahead at upcoming releases, reveals several important trends that give
  us a glimpse into the future of IoT.</p>
<h2>A Comprehensive and Diverse Toolkit for IoT</h2>
<p>The Eclipse IoT Working Group currently hosts 38 projects. Some are very well-known and ship with
  most current Linux distributions. Others are less visible because they address very specific
  niches. All of them are a testament to the innovation and collaboration driving our open source
  community.</p>
<p>Since April 2019, the Eclipse IoT projects listed below were released at least once. The list
  includes projects that focus on industrial automation, edge computing, gateways, protocols, and
  vertical use cases. These are all important areas of IoT application development and showcase the
  sheer diversity of initiatives in our comprehensive IoT toolkit.</p>
<ul>
  <li><a href="https://www.eclipse.org/4diac/">Eclipse 4diac</a> (v. 1.1.11): An open source
    infrastructure for distributed industrial process measurement and control systems based on the
    IEC 61499 standard. Its main components are an IDE and a runtime environment.</li>
  <li><a href="https://iofog.org/">Eclipse ioFog</a> (v1.1.0 and v1.2.0): Streamlines container
    orchestration at the edge by providing a clean API, a powerful command-line interface and, with
    v1.2.0, Kubernetes integration.</li>
  <li><a href="https://www.eclipse.org/kura/">Eclipse Kura</a> (v4.1.0): A popular Java/OSGi
    framework that demonstrates the continued relevancy of the service gateway model, where the
    gateway functions as an aggregator and a controller.</li>
  <li><a href="https://github.com/eclipse/milo">Eclipse Milo</a> (v0.3.0 to v0.3.3): A Java-based
    implementation of the OPC Unified Architecture (OPC UA) outlined in the IEC 62541 standard. This
    project&rsquo;s strong momentum confirms how relevant it is to industrial IoT.</li>
  <li><a href="https://mosquitto.org/">Eclipse Mosquitto</a> (v1.6 to v1.6.7): The Eclipse Mosquitto
    broker now supports version five of the ubiquitous MQTT protocol, validating the results of our
    2019 IoT Developer Survey, which indicated the protocol is more widely used than ever.</li>
  <li><a href="https://volttron.org/">Eclipse VOLTTRON</a> (v6.0): A useful platform for improving
    building system performance and creating a more flexible and reliable power grid. This project
    targets a specific use case, proving that Eclipse IoT is not just about IoT building blocks.</li>
</ul>
<h2>Targeting IoT Technology Needs Across Industries</h2>
<p>The months ahead will be very busy for our community. The Eclipse IoT projects listed below have
  recently announced plans for new releases or initiated a release review. The projects in this
  release roster highlight current trends, such as digital twins, as well as popular use cases such
  as contactless payments and connected cars.</p>
<ul>
  <li><a href="https://www.eclipse.org/californium/">Eclipse Californium</a></li>
  <li><a href="https://www.eclipse.org/ditto/">Eclipse Ditto</a></li>
  <li><a href="https://www.eclipse.org/hono/">Eclipse Hono</a></li>
  <li><a href="https://iofog.org/">Eclipse ioFog</a></li>
  <li><a href="https://www.eclipse.org/kapua/">Eclipse Kapua</a></li>
  <li><a href="https://projects.eclipse.org/proposals/eclipse-keyple">Eclipse Keyple</a></li>
  <li><a href="https://www.eclipse.org/kuksa/">Eclipse Kuksa</a></li>
  <li><a href="https://mosquitto.org/">Eclipse Mosquitto</a></li>
  <li><a href="https://eclipse.org/vorto/">Eclipse Vorto</a></li>
</ul>
<p>Going forward, you can expect the Eclipse IoT Working Group to address the main IoT challenges
  related to constrained devices, edge nodes, and the cloud with high-quality and
  commercially-friendly open source code.</p>
<p>While our community is strong, we are always looking for new contributors and new projects.
  Please drop us a line at iot@eclipse.org if you want to contribute your time, your code, or both.</p>
<p>We hope you will join us for the ride!</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/october/images/frederic.jpg" alt="Frédéric Desbiens"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Frédéric Desbiens</p>
          <p>
            Program Manager, IoT and Edge Computing<br> Eclipse Foundation
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/BlueberryCoder">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>