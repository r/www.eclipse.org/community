<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<h2>IoT Adopter Survey Needs Your Input</h2>
<p>Your input is needed to gain a deeper understanding of the IoT industry landscape!</p>
<p>The 2019 IoT Commercial Adoption Survey is live and ready for input by organizations that use IoT
  platforms.</p>
<p>
  Launched by the<a href="https://iot.eclipse.org/"> Eclipse IoT Working Group</a>, the <a
    href="https://www.surveymonkey.com/r/newsl-iotcomm"
  >2019 IoT Commercial Adoption Survey</a> will help IoT industry stakeholders get a better
  understanding of the IoT ecosystem, the state of commercial solutions, and insights into the
  requirements, priorities, and challenges faced by organizations who are implementing and using
  commercial IoT solutions and services.
</p>
<p>If your organization uses an IoT platform, your input is needed and we want to hear from you.
  Take just a few minutes to complete the survey.</p>
<p>Survey responses will be collected until Monday, October 28 and the results will be published in
  early December.&nbsp;</p>
<p>Thank you in advance for your participation!</p>
<p>
  <a class="btn btn-secondary" href="https://www.surveymonkey.com/r/newsl-iotcomm">Take the survey now!</a>
</p>
<h2>IoT Adopter Logo Campaign Launched</h2>
<p>We need your help to identify IoT adopters; companies that have purchased commercial IoT
  solutions from our members.</p>
<p>
  One of the biggest challenges in the open source software world is identifying who is using the
  technologies contributed by the development community. The Eclipse Foundation&rsquo;s IoT Working
  Group has launched a campaign to identify the adopters of Eclipse IoT open source projects.
  Companies who have adopted commercial IoT solutions &mdash; whether or not they are working group
  members &mdash; can be listed as adopters, here:<a href="https://iot.eclipse.org/adopters/">
    https://iot.eclipse.org/adopters/</a>&nbsp;
</p>
<p>
  We are hoping your customers will be interested in showing their support for open source. All they
  have to do to add their organization&rsquo;s logo to the list of adopters is create a<a
    href="https://github.com/EclipseFdn/iot.eclipse.org/issues/new?template=adopter_request.md"
  > GitHub issue</a> and the Eclipse Foundation&rsquo;s team will take it from there.
</p>
<p>Your support of this initiative will help demonstrate the commercial relevance of open source IoT
  projects. And your customer&rsquo;s logo on the Eclipse IoT adopters page will show their support
  for open source innovation.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/october/images/thabang.jpg" alt="Thabang Mashologu"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Thabang Mashologu</p>
          <p>Vice President, Marketing<br>Eclipse Foundation</p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/t_mashologu">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>