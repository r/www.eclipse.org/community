<?php
/**
 * *****************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 * Christopher Guindon (Eclipse Foundation)
 * *****************************************************************************
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<p>
        Eclipse ioFog Project Lead,<a href="https://projects.eclipse.org/user/8025"> Kilton Hopkins</a>,
        explains why the Eclipse Foundation is focusing on edge computing, the relationship between
        edge and IoT, and the motivations behind creating the <a
          href="/org/workinggroups/eclipse_edge_charter.php"
        >Edge Working Group</a>, and the difficult challenges that working group would tackle.&nbsp;
      </p>
      <hr>
      <h4>Q. Why is now the right time for the Eclipse Foundation to create an Edge Working Group?</h4>
      <p>A. Edge computing has really started to be acknowledged in the industry as technology
        that&rsquo;s independent of IoT and independent of cloud.</p>
      <p>Several IoT projects at the Eclipse Foundation were already starting to consider edge
        computing, so it was natural to move these projects out from under the IoT umbrella to a new
        edge umbrella. By creating a dedicated, standalone Edge Working Group, these projects will
        be able to receive the focused support the market opportunity warrants, both within the
        field of IoT as well as other domains.&nbsp;</p>
        <hr>
      <h4>
        Q. Why was it important for the Eclipse Foundation to separate the Edge Working Group from
        its<a href="https://iot.eclipse.org/"> open source IoT initiatives</a>?
      </h4>
      <p>A. The Eclipse Foundation is doing the same thing for edge computing as it did for IoT.
        We&rsquo;re drawing a circle around edge computing to acknowledge that it has a certain set
        of requirements, challenges, and industry-wide concerns that are different from those found
        in all other sectors. We&rsquo;re saying that edge computing is not only on the rise,
        it&rsquo;s here to stay. There&rsquo;s enough going on in the industry that it&rsquo;s time
        to address the particular challenges associated with edge computing in a very focused
        way.&nbsp;</p>
      <hr>
      <h4>Q. Beyond IoT, what are some of the applications for edge computing?</h4>
      <p>A. Artificial intelligence is a big one. Imagine an AI application that analyzes images
        from thousands of security cameras installed around an airport. The cost to constantly
        stream terabytes of data to a cloud datacenter for processing and analysis would blow a
        monthly data budget in a day. The only solution is to move the AI application &mdash; and
        all of the associated compute capabilities &mdash; to the airport so the data only has to
        flow over the wires and Wi-Fi already installed at the airport to be analyzed.</p>
      <p>Autonomous vehicles are another very important application for edge computing. In this
        case, the latency that occurs when data is sent from the vehicle to the cloud and back again
        creates huge safety issues. The average latency using the cloud is about 250 milliseconds.
        But, vehicle safety relies on sub-one-millisecond latency. The only way to respond 250 times
        faster is to move the compute power and software inside the vehicle.</p>
      <hr>
      <h4>Q. What are some of the key projects the Edge Working Group will encompass?</h4>
      <p>
        A. The two flagship projects are<a href="https://projects.eclipse.org/projects/iot.iofog">
          Eclipse ioFog</a> and<a href="https://projects.eclipse.org/projects/iot.fog05"> Eclipse
          fog05</a>. These projects were previously growing under Eclipse IoT and there&rsquo;s
        already a community around them. So, this is not a working group that&rsquo;s saying, well,
        let&rsquo;s take a look at edge computing and see what we come up with. The Edge Working
        Group is starting with established projects that are already deployed in the field.
      </p>
      <p>
        This is an extremely important point because the Edge Working Group is building on traction
        that&rsquo;s already happening in the industry. This is not a theoretical place where people
        postulate on the future. Rather, it&rsquo;s the home of fully functional code that is
        forming the foundation for edge computing standards for infrastructure. Plus, the working
        group will leverage a proven <a href="/org/workinggroups/about.php">governance
          model</a> that fosters open innovation at scale.&nbsp;
      </p>
      <hr>
      <h4>Q. What are the overarching goals of the Edge Working Group?</h4>
      <p>A. Our goal is to bring edgeops &mdash; devops for the edge &mdash; to the world so
        developers can write software and get it out to the edge so it can run where it needs to,
        whether that&rsquo;s on an MRI machine in a hospital, in a vehicle that&rsquo;s doing some
        lightweight autonomous driving, or on cameras that are sensing animal movements in an
        automated agriculture application. We also want to ensure developers can mass-deploy their
        edge computing technology, for example to an entire fleet of vehicles, once the technology
        is ready for production.</p>
      <p>We&rsquo;re focused on understanding and resolving the challenges that make the edge so
        hard and so different from the cloud. What architectures are needed? What should we tackle
        first? What are some standards and edgeops technologies that developers can use to implement
        these capabilities in their enterprise systems?</p>
      <hr>
      <h4>Q. Why are the technical challenges at the edge so much harder than those in the cloud?</h4>
      <p>A. There are four main reasons edge computing is really hard and very different from the
        cloud.&nbsp;</p>
      <p>Hardware. The hardware at the edge is heterogenous, typically provides a much lower set of
        capabilities, and has far less RAM than hardware that runs in the cloud. The performance
        difference between datacenter servers and edge devices can easily be a factor of hundreds.
        And, you typically can&rsquo;t install the same software across edge device types without
        recompiling for a different processor type. In a datacenter, you can run the same software
        on any vendor&rsquo;s server hardware.</p>
      <p>Connectivity. Connectivity at the edge is often low bandwidth, vulnerable to weather
        events, and prone to dropping out. Imagine a sandstorm knocking out connectivity to the oil
        pumps you&rsquo;re monitoring for several hours. You have to think differently about how
        devices communicate at the edge because you don&rsquo;t have the luxury of a 10-gig fiber
        connection like you do in a datacenter.&nbsp;</p>
      <p>Power. At the edge, you may have to deal with a device that runs on a battery and relies on
        a tiny solar panel to recharge so you can&rsquo;t use more than 50 percent of the available
        power in a given day. In a datacenter, you have almost infinite power and sophisticated air
        conditioning to keep servers cool.</p>
      <p>Security. Edge devices will run enterprise-level software, but they&rsquo;re vulnerable to
        theft and damage. Often, all it takes is a ladder and screwdriver so it&rsquo;s fairly easy
        for people to act in a malicious way before they can be stopped. This level of vulnerability
        doesn&rsquo;t exist in datacenters where even employees don&rsquo;t have access to all areas
        of the facility.</p>
      <hr>
      <h4>Q. What kind of initial interest have seen in joining the Edge Working Group?</h4>
      <p>A. The semiconductor and telecom verticals come to mind as real areas of opportunity where
        we have seen a lot of interest expressed.&nbsp;</p>
      <p>For chipmakers, the Edge Working Group provides access to a developer ecosystem that will
        expose the powerful capabilities in their AI accelerator silicon products. These days, chips
        are really only as valuable as the user community that&rsquo;s developing software for them.
        Edge computing applications are an ideal way to showcase the speed their products provide
        for AI and data analytics processing at the edge.&nbsp;</p>
      <p>Telecom vendors&rsquo; reasons for joining are also very straightforward. If you have the
        5G cell tower of the future and you know your telecom carrier customers are going to be
        looking for edge computing to enable 5G applications, you want to stay close to where people
        are building edge computing infrastructure. You can ensure your products align with edge
        computing standards, and you have an opportunity to help drive those standards.</p>
      <hr>
      <h4>Q. What do you say to developers who are intimidated by the idea of developing
        applications for the edge?&nbsp;</h4>
      <p>A. Our goal is to ensure that everything&nbsp; that grows out of the Eclipse Edge Working
        Group will be focused on developers first. We know developers can&rsquo;t afford to learn
        completely new things so our goal is to let people use their existing skills and devops
        tools, write in their preferred languages, reuse their existing cloud and datacenter
        applications, and make it all work seamlessly with the edge.</p>
      <p>Chances are, if developers have software running in the cloud or in datacenters,
        they&rsquo;re already 80 percent of the way there and we can help them push that software
        out to the edge.</p>
      <hr>
      <h4>Q. How can interested developers and organizations get involved?</h4>
      <p>
        A. If you want to get in on the ground floor, start by
        reviewing the <a href="/org/workinggroups/eclipse_edge_charter.php">charter</a>
        and the <a
          href="/org/workinggroups/wpga/eclipse_edge_working_group_participation_agreement.pdf"
        >Edge Working Group Participation Agreement (WPGA)</a>, or email us at
        membership@eclipse.org. Individually, developers can join the <a
          href="https://accounts.eclipse.org/mailing-list/edge-wg"
        >Edge WG</a> mailing list where we&rsquo;ll be sharing the progress of the working group.
        Edge is the way of the future, so join us now and take your seat at the table!
      </p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/october/images/kilton.jpg" alt="Kilton Hopkins"
          />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Kilton Hopkins</p>
          <p>
            CEO<br> Edgeworx, Inc
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ctron">Twitter</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>