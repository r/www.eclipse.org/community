<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.22.08</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/gabriela.jpg" />
                       		<h3>Editor's Note</h3>
							<p>The moment we've all been waiting for ever since Oracle announced their decision to contribute Java EE to the Eclipse Foundation is almost upon us. A lot of work and dedication has been put into delivering the <strong>Jakarta EE 8 release</strong>, but the most important accomplishment so far has been the collaboration between the members of the <a href="https://jakarta.ee/about/">Jakarta EE Working Group</a>. If active participation from a diverse community represents the best way to drive the vendor-neutral, open innovation necessary to modernize enterprise Java, then Jakarta EE is the poster child for what teamwork can accomplish.</p>
							<p>This newsletter issue puts a spotlight on the present and future of Jakarta EE, including Tanja Obradovic's thoughts on the journey to Jakarta EE 8, why you should attend <a href="https://jakartaone.org/">JakartaOne Livestream</a>, an interview with Ivar Grimstad about his role as the Eclipse Foundation's first Jakarta EE Developer Advocate and more. </p>
								<p> &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8release.php">Jakarta EE 8 Release @ Eclipse Foundation</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaee8.php">Jakarta EE 8: Past, Present, and Future</a><br>
                            		 &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8now.php">Jakarta EE 8 - Where are we?</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaonelivestream.php">Why You Should Attend JakartaOne Livestream!</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/ivargrimstad.php">Meet Ivar Grimstad, the Eclipse Foundation's First Jakarta EE Developer Advocate</a><br>
                           <p>To learn more about the ongoing collaborative efforts to build tomorrow's enterprise Java platform for the cloud, check out the <a href="https://jakartablogs.ee/">Jakarta Blogs</a>.</p>
                           <p>By the way, have you booked your ticket to <a href="https://www.eclipsecon.org/europe2019">EclipseCon Europe 2019</a> yet? Don't forget to register for the <a href="https://www.eclipsecon.org/europe2019/eclipse-community-day">Community Day</a> happening on October 21, which is jam-packed with peer-to-peer interaction and community-organized meetings that are ideal for Eclipse Working Groups, Eclipse projects, and similar groups that form the Eclipse community. Plus, there's also a Community Evening planned for you, where like-minded attendees can share ideas, experiences and have fun! That said, in order to make this event a success, we need your help. What would you like the Community Day and Evening to be all about? Check out <a href="https://wiki.eclipse.org/Cloud_Native_Community_Events_2019">this wiki</a> first, then make sure to go over what we did last year. And don't forget to <a href="https://www.eclipsecon.org/europe2019/registration">register</a> for the Community Day and/or Community Evening! </p>
                           <p>Last but not least, I'd like to take this opportunity to inform you that the Jakarta EE 8 release will be accompanied by a downloadable <strong>Cloud Native Java eBook</strong>. If you want to learn more about what cloud native Java really means to developers, what the cloud native Java future looks like, where Jakarta EE is headed and which technologies should be part of your toolkit for developing cloud native Java applications, stay tuned for our upcoming eBook.</p>
                           <p>Happy reading!</p>
                           <p><strong>PS</strong>: Are you interested in the current state and future of <strong>Jakarta EE</strong>? Would you like to explore other related technologies that should be part of your toolkit for developing cloud native Java applications? Then JakartaOne Livestream is for you! The agenda for this one-day virtual conference, which takes place <strong>September 10, 2019</strong>, has been published! Take a peek at the plan for the day here and register today.</p>
                           <p>Gabriela Motroc</p>
                           <p><a style="color:#000;" target="_blank" href="https://twitter.com/GabrielaMotroc">@GabrielaMotroc</a></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8release.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/august/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8release.php"><h3>Jakarta EE 8 Release @ Eclipse Foundation</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaee8.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/august/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaee8.php"><h3>Jakarta EE 8: Past, Present, and Future</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8now.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/august/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakarta8now.php"><h3>Jakarta EE 8 - Where are we?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaonelivestream.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/august/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/jakartaonelivestream.php"><h3>Why You Should Attend JakartaOne Livestream</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/ivargrimstad.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/august/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/august/ivargrimstad.php"><h3>Meet Ivar Grimstad, the Eclipse Foundation's First Jakarta EE Developer Advocate</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-emf.cloud">Eclipse EMF.cloud </a>provides tools and components that facilitate the adoption of EMF in cloud-based deployment scenarios.</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.theia">Eclipse Theia</a></li>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.efxclipse">Eclipse e(fx)clipse</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2019"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/ece.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p><a target="_blank" style="color:#000;" href="https://pnnl.cvent.com/events/volttron-users-meeting/event-summary-e0c04080a09448a696c1c50833cc08c4.aspx">Eclipse VOLTTRON Users Meeting</a><br>
                           August 29-30, 2019 | Richland, WA, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://jakartaone.org/">JakartaOne Livestream</a><br>
                           September 10, 2019 | Virtual Conference</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.oracle.com/code-one/">Oracle Code One</a><br>
                           September 16-19, 2019 | San Francisco, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.bitkom.org/EN">Bitkom Forum Open Source 2019</a><br>
                           September 17, 2019 | Erfurt, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2019">EclipseCon Europe 2019</a><br>
                           October 21-24, 2019 | Ludwigsburg, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://devoxx.be/">Devoxx Belgium</a><br>
                           November 4-8, 2019 | Antwerp, Belgium</p>
                           <p><a target="_blank" style="color:#000;" href="http://www.embeddedconference.se/app/netattm/attendee/page/82448">Embedded Conference Scandinavia</a><br>
                           November 5-6, 2019 | Stockholm, Sweden</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.continuouslifecycle.de/">Continuous Lifecycle 2019</a><br>
                           November 12-15, 2019 | Mannheim, Germany</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->