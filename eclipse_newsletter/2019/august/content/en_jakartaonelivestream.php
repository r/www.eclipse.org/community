<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>If you are a server-side Java developer, you should be attending <a href="https://jakartaone.org/">JakartaOne Livestream</a>. As the name implies, it is a virtual conference to cover all things <a href="https://jakarta.ee/">Jakarta EE</a>. To boot, the conference is entirely free. It will take place on September 10th - the same date Jakarta EE 8 is to be released. As I will cover in greater detail here, the conference content will include what Jakarta EE truly means, Jakarta EE 8, Eclipse MicroProfile, as well as future specifications to come for Jakarta EE. Hopefully, this will be the first of many JakartaOne conferences to come.</p>
	<p>We have been very fortunate to have an excellent <a href="https://jakartaone.org/program-committee">program committee</a> with Adam Bien, Josh Juneau, Arun Gupta, Ivar Grimstad, and Tanja Obradovic (I am part of the program committee too). We have had some excellent content submissions that have enabled us to build a great program. In fact, the content has been so great that we will try to schedule some of the talks we could not accept as <a href="https://www.meetup.com/jakartatechtalks_">Jakarta Tech Talks</a> in the coming months.</p>
	<p>Special thanks are in order for Edwin Derks, Sebastian Daschner and Victor Orozco for being very good sports and agreeing to be backup speakers at the conference.</p>
<h4>The Content</h4>
	<p>The conference will run from 7 AM to 1AM. That should provide decent coverage to most of the globe for at least part of the day. The following is a high overview of the program.</p>
		<ul>
			<li>The opening keynotes will be provided by Eclipse Foundation executive director <em>Mike Milinkovich</em> and the father of Java <em>James Gosling</em>. Mike will discuss Jakarta EE itself while James will put Jakarta EE in the context of the broader Java ecosystem.</li>
			<li>Vendors such as <em>IBM, Oracle, Payara, Tomitribe, and Fujitsu</em> will voice their support of Jakarta EE in the industry keynote.</li>
			<li>IBM WebSphere lead architect Kevin Sutter will provide a high-level overview of <em>Jakarta EE in Jakarta for DummEEs</em>.</li>
			<li>Java Champion and Java EE Guardian Josh Juneau will provide a high-level overview of <em>Jakarta EE 8 features</em>. Josh will also briefly cover what the future might bring for Jakarta EE.</li>
			<li>Java Champion Ivar Grimstad will provide a state of the <em>union for MicroProfile</em>, including a feature tour and roadmap.</li>
			<li>Java Champion <em>Adam Bien</em> will deliver a slide-free live coding session that uses Jakarta EE and Eclipse MicroProfile together.</li>
			<li>Java EE veteran Richard Monson-Haefel will discuss <em>how the community can help contribute and move Jakarta EE forward</em>.</li>
			<li>Folks leading some early Jakarta EE specifications will share <em>what they have in the works beyond Jakarta EE 8</em>. Specifications covered include Jakarta NoSQL (led by Java Champion Otavio Santana), Jakarta Concurrency (led by Payara CEO Steve Millidge), Jakarta JSON Binding (led by JCP star specification lead Dmitry Kornilov), Jakarta REST (led by Java EE Guardian Markus Karg), Jakarta Security (led by Java EE Guardian Arjan Tijms), Jakarta Messaging (led by Tomitribe founder David Blevins) and Jakarta Faces (led by Java EE Guardian Arjan Tijms).</li>
			<li>Jakarta EE key stakeholders in the steering committee will hold an <em>open panel</em>. This is your opportunity to interact with Jakarta EE stakeholders directly in real-time.</li>
			<li>There will be sessions on innovative ecosystem open source projects like <em>Quarkus and Helidon</em>.</li>
			<li>I will show the many ways <em>Jakarta EE applications can run in the cloud</em> (in my case on Azure).</li>
		</ul>
	<p>You can view the <a href="https://jakartaone.org/">full program</a> on the JakartaOne Livestream website.</p>
<h4>Join Us!</h4>
	<p>I sincerely hope you will join us on September 10th and also tell your friends and colleagues. There is a lot of important information for all server-side Java developers that the conference is in a unique position to deliver. Note that while you can attend for free, you do need to <a href="https://www.crowdcast.io/e/nztuljys">pre-register</a> to view the sessions on September 10th. </p>
	<p>If you can't attend on September 10th, no worries. The sessions will be publicly available for free at a later point in time. Finally, don't forget to follow the <a href="https://twitter.com/JakartaOneConf">official conference Twitter handle</a> to get updates between now and then.</p>
	
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\august\images\reza.png"
        alt="Reza Rahman" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><br />
            Reza Rahman<br />
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/reza_rahman">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>