<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>As I am marking one year at the Eclipse Foundation as the Jakarta EE Program Manager, I am looking forward to the major milestone we are all eagerly waiting for - the Jakarta EE 8 release. We now have an official release date: September 10th, 2019! It feels like the whole (very large) Java EE community and enthusiasts have been waiting for this way too long! There were many ups and downs since Oracle announced they would be contributing Java EE to the Eclipse Foundation and a lot of work has been put into planning and making sure we have all the groundwork done for Jakarta EE 8 and beyond.</p>
	<p>So let's look back at everything that has been done so far: October 2017: Oracle announces the contribution of Java EE to the Eclipse Foundation. <a href="https://jakarta.ee/about/">Jakarta EE Working Group</a> is established with the goal to</p>
	<ul>
		<li>deliver more frequent releases</li>
		<li>lower barriers to participation</li>
		<li>develop the community</li>
		<li>manage the Jakarta EE brand on behalf of the community</li>
	</ul>
	<p>The following milestones were reached over the course of one year:</p>
	<ul>
		<li>The code is contributed</li>
		<li><a href="https://projects.eclipse.org/projects/ee4j.glassfish/downloads">Eclipse GlassFish 5.1</a> Java EE 8 certified is released</li>
		<li>Eclipse Foundation Specification Process is available
			<ul>
				<li><a href="https://www.eclipse.org/projects/efsp/">EFSP v1.2</a></li>
			</ul>
		</li>
		<li>Customization of EFSP for the needs of Jakarta EE resulted in Jakarta EE Specification Process
			<ul>
				<li><a href="https://jakarta.ee/about/jesp/">JESP v1.2</a></li>
			</ul>
		</li>
		<li>Community engagement
			<ul>
				<li><a href="https://drive.google.com/drive/folders/1kJFh6EIpOyAWP3JkymYZXOK311WM2HXm">Public Community drive</a></li>
				<li><a href="https://www.meetup.com/jakartatechtalks_/">Jakarta Tech Talks</a></li>
				<li>Monthly Jakarta EE Update calls</li>
				<li>Monthly email updates</li>
			</ul>
		</li>
		<li>We conducted two Jakarta EE Developer Surveys. Here are the survey findings: 2018 <a href="https://jakarta.ee/documents/insights/2018-jakarta-ee-developer-survey.pdf">Jakarta EE Developer Survey</a> and <a href="https://jakarta.ee/documents/insights/2019-jakarta-ee-developer-survey.pdf">2019 Jakarta EE Developer Survey</a></li>
		<li>The working group has continuously and progressively worked on refining the delivery plan for Jakarta EE 8 </li>
	</ul>
	<p>We are also working on ensuring the Compatibility Trademark Guidelines and License agreements are in place: <a href="https://jakarta.ee/legal/trademark_guidelines/">Jakarta EE Trademark Guidelines and License Agreements</a>.</p>
	<p>More progress:<br>
	<a href="https://docs.google.com/document/d/1Et3LtK-2SUuAoOV56t8R8fKnRWhbWqg9SLgm-VhbDPY/edit">TCK process</a> - link to be available shortly, finished July 10th, 2019<br>
	<a href="https://drive.google.com/open?id=15biGAEDzLJUelMMJj2ppRc4hPIXq-wQsDElbVF7AYos">JESP Operations Guide</a>,  finished July 10th, 2019<br>
	<a href="https://wiki.eclipse.org/How_to_Prepare_API_Projects_to_Jakarta_EE_8_Release">Jakarta EE 8 Release Guide</a>, finished July 10th, 2019</p>
	<p>With all the above laid out and defined, and with a clear understanding of the Jakarta EE rights to Java trademarks link to <a href="https://blogs.eclipse.org/post/mike-milinkovich/update-jakarta-ee-rights-java-trademarks">Mike Milinkovich's blog</a> (May 2019), the Jakarta EE Working Group has been extremely busy making sure we have the Jakarta EE 8 release ready for the community.</p>
	
	<p>The Jakarta EE 8 release will</p>
	<ul>
		<li>Be fully compatible with Java EE 8 specifications</li>
		<li>The Jakarta EE 8 specifications will be fully transparent and will follow the Jakarta EE Specification Process</li>
		<li>Include the same APIs and Javadoc using the same javax namespace</li>
		<li>Provide Jakarta EE 8 TCKs under an open source license based on and fully compatible with the Java EE 8 TCKs.</li>
		<li>Include a Jakarta EE 8 Platform specification that will describe the same platform integration requirements as the Java EE 8 Platform specification.</li>
		<li>Reference multiple compatible implementations of the Jakarta EE 8 Platform when the Jakarta EE 8 specifications are released.</li>
		<li>Provide a compatibility and branding process for demonstrating that implementations are Jakarta EE 8 compatible.</li>
	</ul>
	
	<p>Jakarta EE at a glance</p>
	<ul>
		<li>There has been a strong commitment from the Jakarta EE Working Group to
			<ul>
				<li>Deliver Jakarta EE 8</li>
				<li>Keep evolving Jakarta EE and deliver new versions</li>
				<li>Further plans are being evolved by the Jakarta EE community via the <a href="https://projects.eclipse.org/projects/ee4j.jakartaee-platform">Jakarta EE Platform Project</a></li>
			</ul>
		</li>
		<li>The ongoing evolution of Jakarta EE is the only way to ensure that developers and software vendors can continue to meet the modern enterprise's need for cloud-based applications that resolve key business challenges.</li>
	</ul>
	<p>As you can see, a lot of work has been put into Jakarta EE already and a lot more is ahead of us! While the current focus is on the Jakarta EE 8 release, we are looking into making sure all steps are taken so that we can grow the community and direct our attention to innovation in future releases. On a personal level, I am looking forward to seeing the evolution of Jakarta EE and witnessing Java's dominance in the cloud native era!</p>	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\august\images\tanja.png"
        alt="Tanja Obradovic" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><br />
            Tanja Obradovic<br />
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/TanjaEclipse">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>