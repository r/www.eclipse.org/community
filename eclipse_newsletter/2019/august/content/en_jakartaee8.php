<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>In this article, we're going to explain what Jakarta EE 8 is all about. But before we go there, let's look at a little bit of history.</p>
<h4>The Past</h4>
	<p>Java was released in 1996, along with an application server called the Kiva Enterprise Server, shortly after being acquired by the internet giant of that time, Netscape. At the same time, a company called WebLogic, shortly after it was acquired by BEA, had been working towards a similar concept of an "application server", using, of course, totally different APIs.</p>
	<p>Together with Sun Microsystems, the initial APIs (JDBC, EJB, Servlet and JSP) for an extensive common framework were being designed mostly based on these two servers. In late 1999, the first version of this framework came out: the somewhat confusingly-named J2EE 1.2, which was implemented by the aforementioned Kiva, now called iPlanet Application Server (iAS) and WebLogic.</p>
	<p>J2EE was a (somewhat) open standard, meaning it could be licensed by other parties, who could then provide certified implementations of it. Later on, these parties could even join in the design of the specifications through a program called the JCP; Java Community Process, which throughout its timespan became increasingly more open.</p>
	<p>J2EE 1.2 made quite an impact on the market, and long before a wide range of companies had products out that were compatible with it, including IBM WebSphere 4, BEA WebLogic 6.1, Oracle 9i AS, Tmax Soft JEUS 3.0, Borland AS 4.5, NEC WebOTX 4.2, and many more. J2EE evolved to 1.3 in 2001, and 1.4 in 2003 with some 19 implementations, among which OSS implementations JBoss AS, JOn AS, and Apache Geronimo. It is worth mentioning that a certain amount of partial J2EE implementations existed as well, like Tomcat, Jetty, Resin, and IronFlare's Orion.</p>
	<p>During this period, we also saw several bigger acquisitions; Red Hat acquired JBoss, and Oracle acquired both IronFlare and BEA.</p>
	<p>Despite the many implementations, J2EE 1.4 was hitting a roadblock; one in the form of a wall of complexity. In mid-2006, J2EE was more or less rebooted in the form of Java EE 5, which strongly focused on developer friendliness and convention over configuration. However, the number of companies with a compatible implementation shrunk, and the number of original implementations was even lower, which basically consisted of GlassFish, WebSphere, WebLogic, JBoss AS, JOn AS, Geronimo, JEUS, and NetWeaver. Java EE 5 evolved into Java EE 6 in late 2009, which introduced many of the "modern" APIs, specifically JAX-RS and CDI, and the long-awaited reboot of JSF, aptly named JSF 2.</p>
	<p>Around that same time, however, Sun was acquired by Oracle, which strongly changed the dynamic in the Java EE ecosystem. Since Oracle had already acquired BEA (WebLogic) and IronFlare (Orion), it also owned GlassFish now, and in addition, a mayer extra force was removed from the power equation in the JCP. With Apache leaving the JCP and Geronimo and JOn AS silently fading away, the Java EE market was essentially reduced to the three major players Oracle, IBM, and Red Hat, plus the much, much smaller Tomitribe, which picked up some of the pieces left behind by Geronimo for a new AS called TomEE.</p>
	<p>While a relatively minor release compared to 5 and 6, Java EE 7, which was released in 2013, finally revealed the contours of what a modern Java EE platform was all about; a core bean model with composable interceptors, validation services, and extension points. Java EE 8 was supposed to bring this model largely to its conclusion, but then things slowed down considerably. Java EE 8 was released with a much-reduced scope, and Oracle announced in late 2017 that they would be transferring Java EE to the Eclipse Foundation.</p>

<h4>The Present</h4>
	<p>The transfer, which has now been in progress for about two years, includes quite a number of steps:</p>
		<ul>
			<li>Vetting and cleaning the <a href="https://github.com/javaee">existing source code</a></li>
			<li>The actual transfer of the code, including issues</li>
			<li><a href="https://jenkins.eclipse.org/">Setting up jobs</a> to build everything at <a href="https://ci.eclipse.org">Eclipse infrastructure</a></li>
			<li>Changing over the Maven coordinates of all components to <a href="https://mvnrepository.com/artifact/jakarta">jakarta.*</a></li>
			<li>Releasing <a href="https://projects.eclipse.org/projects/ee4j.glassfish/downloads">Eclipse GlassFish 5.1</a>, a version fully built from the transferred source at Eclipse that passed the existing Java EE 8 TCKs (and <a href="https://www.oracle.com/technetwork/java/javaee/overview/compatibility-jsp-136984.html">officially certifying it</a>)</li>
			<li>Designing a new Jakarta and Eclipse spec and certification process to replace the JCP, and <a href="https://eclipse.org/legal/tck.php">a new spec license</a></li>
			<li>Transfering the <a href="https://github.com/eclipse-ee4j/jakartaee-tck">Java EE TCK source</a></li>
			<li>Building from the transferred TCK sources <a href="https://download.eclipse.org/ee4j/jakartaee-tck/jakartaee8-eftl/promoted/">a series of TCK binaries</a> under the new spec license</li>
			<li>Transforming all projects constituting Java EE 8 APIs to spec projects and <a href="https://waynebeaton.wordpress.com/2019/04/04/renaming-java-ee-specifications-for-jakarta-ee/">replacing Oracle trademarked terms</a>, or terms otherwise negotiated between Oracle and Eclipse not to be used anymore with the new terms in place</li>
			<li>Adding new <a href="https://waynebeaton.wordpress.com/2019/04/08/specification-scope-in-jakarta-ee/">scope statements</a> to the spec projects</li>
			<li><a href="https://jenkins.eclipse.org/es/job/2_security-run-tck-against-staged-build/">Running the newly-build and relicensed TCKs</a> against the updated APIs, and file certification requests for Jakarta EE 8 against staged builds</li>
			<li>Releasing the staged API jars</li>
		</ul>
	<p>When this is all done and dusted, we have a Jakarta EE 8 API release that has an identical signature to the Java EE 8 API where it originates from, but which is completely built, licensed, tested, and certified via/by the Eclipse Foundation and its processes.</p>
	<p>At the moment of writing, the second-to-last step included in the above-mentioned list is in full swing, and we anticipate to finish the work soon, which should be before the end of August 2019. It's expected that application servers such as WildFly, Open Liberty and others will be Jakarta EE 8 certified relatively soon thereafter.</p>
	<p>As briefly mentioned above, Jakarta EE 8 will bring about a new set of names for all the well known APIs. For these new names, we have collectively (= spec committee and committers) decided to favor small names over longer names, especially to avoid obscure abbreviations. Long-time users of Java EE are, of course, used to abbreviations like JMS, JTA, JCA, JPA, JSF, EJB, etc, etc, but for newcomers, this has been often cited as a barrier to entry. As we studied the names, we found that they were actually quite inconsistent. I.e. why is JMS a "Service", JTA an "API" and JCA an "Architecture"? Almost always, these words are essentially just some kind of filler words that don't really bring any added value.</p>
	<p>For the sake of this discussion, we'll split the new names into three tiers:</p>
	<p><strong>Tier 1: One word</strong></p>
		<ol>
			<li>Jakarta Servlet</li>
			<li>Jakarta Faces*</li>
			<li>Jakarta WebSocket</li>
			<li>Jakarta Concurrency</li>
			<li>Jakarta Interceptors</li>
			<li>Jakarta Authentication</li>
			<li>Jakarta Authorization</li>
			<li>Jakarta Security</li>
			<li>Jakarta Messaging</li>
			<li>Jakarta Persistence</li>
			<li>Jakarta Transactions</li>
			<li>Jakarta Batch</li>
			<li>Jakarta Mail</li>
			<li>Jakarta Connectors</li>
			<li>Jakarta Annotations</li>
			<li>Jakarta Activation</li>
			<li>Jakarta NoSQL</li>
		</ol>
	<p><strong>Tier 2: Two words</strong></p>
		<ol>
			<li>Jakarta Bean Validation</li>
			<li>Jakarta Expression Language</li>
			<li>Jakarta Enterprise Beans</li>
			<li>Jakarta XML Binding</li>
			<li>Jakarta JSON Binding</li>
			<li>Jakarta JSON Processing</li>
			<li>Jakarta Server Pages</li>
		</ol>		
	<p><strong>Tier 3: Three words or more</strong></p>
		<ol>		
			<li>Jakarta XML Web Services</li>
			<li>Jakarta RESTful Web Services</li>
			<li>Jakarta Standard Tag Library</li>
			<li>Jakarta Contexts and Dependency Injection</li>
		</ol>
	<p><em>(* due to a small timing issue, this is temporarily called Java Server Faces, but the intent is to rectify this soon)</em></p>
	
<h4>The Future</h4>
	<p>A somewhat thorny issue remains open, and that's renaming all Java API packages from javax.* to jakarta.*. E.g. <em>javax.servlet.http.HttpServletRequest</em> will become <em>jakarta.servlet.http.HttpServletRequest</em>.</p>
	<p>This is obviously a change that will impact existing code, so a good strategy for this should be in place. There's a plethora of different options, including taking the opportunity to also rename oddly-named packages such as javax.security.auth.message not to just jakarta.security.auth.message.*, but directly to e.g. jakarta.authentication, matching the (new) spec name. Other options being considered are a so-called big bang rename (rename everything in one go), or rename incrementally (rename the entire API only when anything in that API needs to be updated). A decision still has to be made, but from various polls, opinion pieces and discussions, it seems the following is the preferred combination of options:</p>
	<ul>
		<li>Big bang (<em>rename all APIs in one go</em>)</li>
		<li>javax.* to jakarta.* only (<em>not touching any other part of the package name</em>)</li>
		<li>Jakarta EE 9 only about rename (<em>no other new features, which would be the topic of EE 10 then</em>)</li>	
	</ul>
	<p>Another open issue is the transfer of spec documents. With all the work that has been going on, this still has not happened but is expected to happen within a reasonable time. Meanwhile, Jakarta EE 8 is therefore released without the "real" spec documents, but instead with a so-called boilerplate spec document. This document essentially contains just the license and scope and not much else. The Javadoc of the APIs, which are part of the specification as well, is, of course, released.</p>
	<p>What we're all looking forward to is to finally have the transfer process behind us, and start working on the next generation of Jakarta EE. If the above-sketched path for the rename is followed, that would be Jakarta EE 10, or maybe for the occasion, this could be called Jakarta EE X.</p>
	<p>Some of the plans of what might show up in this elusive X version will be disclosed at the <a href="http://blog.rahmannet.net/2019/08/why-you-should-attend-jakartaone.html">JakartaOne LiveStream</a>, so if you're interested in the future of Jakarta EE, be sure to attend that!</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\august\images\arjan.png"
        alt="Arjan Tijms" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><br />
            Arjan Tijms<br />
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/arjan_tijms">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>