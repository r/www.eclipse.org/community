<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin6.png"></p>
<h3>In the past</h3>
	<p>When Java EE was nearing a release, the final stages were a mystery to most of us.  Sure, many of us participated in the various JSRs and the creation of the Specifications that fed into the Java EE Platform.  But, what was actually happening during these final stages of the release at Oracle (and Sun before them)?  We all knew there had to be some final polishing of the Specifications themselves.  And, there was the packaging of the various artifacts -- APIs, Javadocs, and Specifications.  And, of course, there was the final TCK testing of Glassfish.  But, were they 10% there, or 65%, or closer to 95%?  It was all kind of a mystery to those of us outside of Oracle.</p>
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin1.png"></p>
<h3>Jump to Jakarta EE</h3>
	<p>As most of you should know by now, <a href="https://www.infoq.com/podcasts/milinkovich-jakarta-ee/">Java EE has been transitioning to the Eclipse Foundation under the Jakarta EE brand</a>.  Everything we're doing at Eclipse in support of Jakarta EE is in the open.  All of the processes, all of the code, all of the testcases, all of our trials and tribulations...  everything has been done in the open.  I will admit that sometimes this openness has its challenges.  Getting a group of industry leaders to all move in the same direction takes some effort.  But, the power of this openness is becoming so evident now that we are nearing the completion of Jakarta EE 8.  Let me give you some insights.</p>
<h3>Specifications</h3>			
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin2.png"></p>
	<p>I don't want to bore you with all of the processes.  But, I do want you to be aware that we do have some methods to our madness.  We have the overall <a href="https://jakarta.ee/about/jesp/">Jakarta EE Specification Process</a>, which is a derivative of the <a href="https://www.eclipse.org/projects/efsp/">Eclipse Foundation Specification Process</a>.  This is the driving force which provides the basis of the Specifications produced for Jakarta EE 8.  We also need to keep in mind that Jakarta EE 8 will be functionally equivalent to Java EE 8.</p>
	<p>We will be reproducing the Jakarta EE 8 Platform and Web Profile Specifications in their entirety.  All of the text, images, and references in the original Java EE Platform and Web Profile Specifications are in the updated Jakarta EE 8 equivalents &mdash; except for the required <a href="https://jakarta.ee/legal/acronym_guidelines/">trademark modifications as required by Oracle</a>.  You can easily view and/or build the current state of these platform Specifications via the <a href="https://github.com/eclipse-ee4j/jakartaee-platform/tree/master/specification">Jakarta EE Platform github repository.</a></p>
	<p>Outside of the Platform and Web Profile Specifications, the individual component Specifications will consist of skeletal specification document combined with the javadoc of the API.  Although not as complete as the JSR Specifications we are used to, they do provide a solid definition of the APIs.  The Eclipse Foundation is in the process of securing the copyrights of these individual component Specifications.  A more complete set of readable Specifications should be available post Jakarta EE 8.</p>
<h3>Specification Reviews</h3>	
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin3.png"></p>
	<p>You can see our current progress of creating all of the necessary Specifications and the associated artifacts via the PRs at our <a href="https://github.com/jakartaee/specifications/pulls"><em>specifications</em> github repo</a>.  We are tracking progress via labels -- draft (still being reviewed), final (done pre-reviewing, ready for the official ballot), ballot (out for official vote), and approved (ready to be posted).  As of this writing, we have 13 Specifications out for the official ballot.  Since we have a goal of completing Jakarta EE 8 by mid-September, this status will be changing daily.</p>
	<p>Here again, this shows the power of doing everything in the open.  Anybody can monitor our progress (or lack thereof).  Anybody can help with the review process and provide comments and suggestions.  If there are problems with the process, people can open issues and we can address them at the appropriate time.</p>
<h3>Technical Artifacts</h3>
	<p>Let's look a bit closer at one of the component Specifications that was recently approved for ballot &mdash; <a href="https://github.com/jakartaee/specifications/pull/37">Jakarta Messaging</a>.  (There is also a <a href="https://github.com/jakartaee/specifications/pull/58">corresponding PR for the javadoc</a>, but let's focus on the Specification PR for this discussion.)</p>
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin4.png"></p>
	<p>In the checklists posted to this PR, you'll notice several items that need to be provided to indicate that a Specification is ready for ballot.</p>
		<ul>
			<li>Specification, in both pdf and html formats</li>
			<li>Javadoc, separate PR</li>
			<li>OSSRH staging repository for API, javadoc, and sources jar files</li>
			<li>TCK binary</li>
			<li>TCK user's guide</li>
			<li>Compatible Implementation</li>
			<li>Certification Request</li>
			<li>TCK results</li>
			<li>And a few meta files to help with the organization and publishing of these artifacts once they are approved</li>
		</ul>
	<p>Not a light task, by any means.  But, we need to ensure that the content of Jakarta EE is robust and ready for consumption by implementers.  Some of this process is also in place to help ensure the proper flow of IP (Intellectual Property) from contributors to consumers.  But that's a topic that requires a separate blog post.</p>
<h3>So, what's left?</h3>	
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/august/images/kevin5.png"></p>
	<p>We are making fantastic progress!  We've got a great group of individuals all driving towards this single goal of producing Jakarta EE 8 by mid September.  How can you help with achieving this goal?  You can follow our progress by monitoring our PRs as outlined earlier.  You can cheer us on.  You can review the PRs.  You can participate in our <a href="https://accounts.eclipse.org/mailing-list">various mailing lists (look for jakarta)</a>.  And, finally, you can help us celebrate when we announce Jakarta EE 8!</p>	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\august\images\kevin.jpg"
        alt="Kevin Sutter" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><br />
            Kevin Sutter<br />
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kwsutter">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>