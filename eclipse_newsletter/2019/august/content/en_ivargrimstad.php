<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p><strong>Starting in October, you will be the Eclipse Foundation's first Jakarta EE Developer Advocate. What does this role entail?</strong></p>
	<p><strong>Ivar Grimstad</strong>: The role of the Jakarta EE Developer Advocate involves activities that help increase the awareness, participation, and adoption of Jakarta EE and related technologies. In plain words, this means creating and spreading content that helps developers adopt the technologies. It will involve a lot of networking, both online as well as in person at conferences and meetups. It is important to gain the trust of developers and thought leaders in the community and get the message out about everything that goes on around Jakarta EE.</p>
	<p><strong>How do you plan to help the Jakarta EE platform move forward? </strong></p>
	<p><strong>Ivar Grimstad</strong>: I plan on continuing the work I am doing in the Jakarta EE Working Group and the EE4J projects. Since the major part of my role as the Jakarta EE Developer Advocate will be to communicate with developers, I will help channel their experiences, feedback, and requirements back to the Jakarta EE Working Group in order to help focus on the technology that developers are asking for. Being a communication liaison between the working group and the developer community.</p>
	<p><strong>What are your short-term goals as a Jakarta EE Developer Advocate?</strong></p> 
	<p><strong>Ivar Grimstad</strong>: My short-term goals as the Jakarta EE Developer Advocate will be to establish this role in the community. One factor of this particular role is that it is vendor-neutral, which is what distinguishes it from the various vendors' own developer advocates. However, I think it is important to cooperate with the vendor developer advocates, as well as developer advocates in other communities.</p>
	<p><strong>What do you think makes a great Developer Advocate?</strong></p> 
	<p><strong>Ivar Grimstad</strong>: A great developer advocate is someone who is able to communicate with developers on their terms. Deep knowledge of technology is vital. The key is to be talking the "language" of the developers since they are the ones making the technical decisions, or at least deeply influence the decisions. It used to be the CTOs and CIOs that chose technology on a corporate level, but in the cloud native landscape, today these decisions are often made by the development teams, i.e. the developers. And a developer will see right through a sales pitch or marketing lingo. A great developer advocate is able to have a healthy and productive dialogue within the developer community as well as in communities around competing technologies.</p>
	<p><strong>Do you plan to stay involved in the Jakarta EE development community?</strong></p> 
	<p><strong>Ivar Grimstad</strong>: Yes, definitely! As a developer advocate, I really need to have deep knowledge of various technologies. And there is no better way to stay up-to-date with technology than to be involved in developing it. I will contribute to specifications and implementations, as well as the work which involves adjusting the processes to meet the constantly changing demands.</p>
	<p><strong>Let's discuss the vision for the technical future of Jakarta EE. In your opinion, what are the most important areas to evolve?</strong></p>
	<p><strong>Ivar Grimstad</strong>: I think Jakarta EE is a great set of technologies. That said, some of the parts of the platform are maybe not that relevant for modern cloud native applications. These technologies need to be pruned, deprecated or made optional. This will need to happen some way or the other even if it compromises backward compatibility. It does not make sense for a new vendor that wants to be certified as Jakarta EE compatible to implement a bunch of technologies that are not going to be used anyway. Another area that needs to be addressed is the relationship with Eclipse MicroProfile.</p>
	<p><strong>Jakarta EE is community-driven, which means that the role of the community is more important than ever. How would you convince the community to help design the future of Jakarta EE?</strong></p>
	<p><strong>Ivar Grimstad</strong>: I think of it more as enabling than convincing. There is such a strong community around Jakarta EE and we just have to continue making the threshold for participation as low as possible. Communication is key, and that is an area we have not yet succeeded in the establishing of Jakarta EE. We need to get the message out there about what is going on, what we need help with and how to participate.</p>
	<p><strong>With the upcoming release of Jakarta EE 8, what will you do as the Developer Advocate to drive adoption of Jakarta EE 8 compatible products?</strong></p>
	<p><strong>Ivar Grimstad</strong>: I will be out there in the community, talking about the technologies, writing articles and tutorials, running workshops and writing code samples. I will do this in a vendor-neutral way by using the various products and comparing them in an objective way. Where possible, I want to cooperate with the vendors' developer advocates in order to help with the promotion of their products.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\august\images\ivar.png"
        alt="Ivar Grimstad" />
        </div>
        <div class="col-sm-16">
          <p class="author-name"><br />
            Ivar Grimstad<br />
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ivar_grimstad">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
 </div>