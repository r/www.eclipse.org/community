<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

$pageTitle = "Jakarta EE 8 Preview";
$pageKeywords = "eclipse, newsletter, Jakarta EE, jakarta ee 8 release, jakarta ee 8, open source, java, java ee, jakartaone, jakartaone livestream";
$pageAuthor = "Christopher Guindon";
$pageDescription = "This newsletter issue puts a spotlight on Jakarta EE 8 and beyond, including Tanja Obradovic's thoughts on the journey to Jakarta EE 8, why you should attend JakartaOne Livestream, an interview with Ivar Grimstad about his role as the Eclipse Foundation's first Jakarta EE Developer Advocate and more.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "";
  $sponsorLink = "";
  $sponsorImage = "";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;