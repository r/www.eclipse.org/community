<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "2019 in Focus";
  $pageKeywords = "eclipse, newsletter, Launch, Our 4 priorities for 2019, 2019, Tools, Automotive, IoT, Jakarta, Jakarta EE, EFSP, JCP, Java Community, process, OpenADx, EF Specification process, edge computing";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "This month, the newsletter gives you a little taste of what you can expect in 2019";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "";
  $sponsorLink = "";
  $sponsorImage = "";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;