<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>As we start 2019 I would like to discuss where we expect the staff of the Eclipse Foundation to focus our time and energy throughout the year. As the platform for open collaboration we provide the infrastructure, governance, licensing, and community development support that our projects need to be successful. The "the community is the capacity" is an axiom of open source. But unfortunately, the staff of the Eclipse Foundation is not infinite. So we need to think about some key areas where to focus our energies. After discussion with the staff and the Board, we've come up with the following list of key focus areas for the year.</p>
	<ol>
		<li>Cloud Native Java: <a href="https://jakarta.ee/">Jakarta EE</a>, <a href="https://microprofile.io/"> Eclipse MicroProfile</a>, <a href="https://www.eclipse.org/jetty/">Eclipse Jetty</a>, <a href="https://vertx.io/">Eclipse Vert.x</a>. There is a long list of important and rapidly growing projects and communities at the Eclipse Foundation. All are focused on making Java runtimes and frameworks relevant for developers building modern cloud-based applications that are based on paradigms like reactive and microservices. The unique value proposition that the Eclipse community has is the mix of technologies that support the bleeding edge, while at the same time providing a migration path for enterprises' existing assets. Our objectives for 2019 is to support these projects' rapid growth in community and adoption.</li> 
		<br>
		<li>Internet of Things: <a href="https://iot.eclipse.org/">Eclipse IoT</a> has been an area of rapid growth for the Eclipse community since its inception. With runtimes, protocols, and frameworks that span from the smallest of embedded devices, through IoT and Edge gateways, and into highly scalable cloud infrastructure, Eclipse IoT delivers technologies that matter to Internet of Things.</li>
		<br>
		<li>Automotive: The automotive ecosystem is no longer simply about getting aluminum and steel boxes to move faster and more economically using internal combustion engines. Connected cars, electrification, seamless mobility, advanced driving assistance, and autonomous driving are all megatrends that will shape the future of automotive. Eclipse projects and working groups like Eclipse openPASS and Eclipse openMobility are building some key technologies for this industry. Technologies like simulation and test environments for autonomous driving systems, managing automotive measurement data, and building tooling platforms for ADAS are all focus areas for the Eclipse community in automotive.</li>
		<br>
		<li>Developer Tools: The Eclipse Foundation's roots are in developer tools, and we remain very strong in this area. From the <a href="https://www.eclipse.org/eclipseide/">Eclipse Java IDE</a> to the Capella systems engineering environment, highly capable desktop tools are a core strength. In 2019 we anticipate a lot of growth in our web-based tooling projects like <a href="https://www.eclipse.org/che/">Eclipse Che</a> and <a href="https://www.theia-ide.org/">Eclipse Theia</a>. These are the next-generation tooling platforms that will continue the Eclipse community's contribution to meeting the needs of developers for years to come.</li>
	</ol>
	<p>Of course, we're still offering infrastructure, governance, licensing, and community development support to all of our projects. That isn't changing.  What is changing is that, as the areas of interest of our projects continue to expand, we are explicitly identifying through these core focus areas where we will exert strategic energies to help those projects be successful. We expect these core focus areas to evolve and expand over time.</p>
	<p>It's going to be a tremendously busy and exciting year!</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/january/images/mike.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Mike Milinkovich<br />
            <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://mmilinkov.wordpress.com/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>