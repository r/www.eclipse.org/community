<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>As most of you are aware, Oracle has contributed the Java EE specification to the Eclipse Foundation. The enterprise Java community decided to rename the Java EE specification to Jakarta EE. Part of this huge transition to open source is changing the specification process. The famous Java Community Process (JCP) is going to be replaced by the Eclipse Foundation Specification Process (EFSP), which will be better suited for vendor neutrality, transparency, and all other attributes associated with open source. So what exactly is the difference?</p>
	<p>To learn more about the new process, please refer to the <a href="https://www.eclipse.org/projects/efsp/">EFSP v1.0</a>, and <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/january/specification_process.php">Wayne Beaton's article</a> in this newsletter.</p>
	<p>There are many differences between the Eclipse Foundation Specification Process (EFSP) and the Java Community Process (JCP), but let's focus on my top 5!</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/january/images/jcp_2.png"></p>
	<p><strong>Code First</strong>: While the JCP proposed to have a specification document created first, the EFSP will be firstly based on the hands-on experimenting and coding, as a way of proof that something is worthy of documenting in a specification. Once the code is in good shape, we can proceed with the creation of a specification document that will support the code and not the other way around.</p>
	<p><strong>Collaborative</strong>: The EFSP is defined and managed by the Jakarta EE Working Group members, which is governed as a vendor-neutral group and will be used by the wider Jakarta community for a specification creation and implementation. We ensure a level playing field for everyone in the Working Group to participate in the specification creation via collaboration. Collaboration of this type also ensures the sharing of knowledge and the growth of the developers participating in the process.</p>
	<p><strong>Documents and TCKs are open source</strong>: The key benefits of the EFSP are producing documents and open source TCKs. This means the following for the community: Transparency, Openness, Shared Burden, and Vendor Neutrality. You can refer to the <a href="https://blogs.eclipse.org/post/tanja-obradovic/java-ee-tck-now-open-sourced-eclipse-foundation">open source TCK blog</a> for more information. Opening the specification to the community and having them influence the technical direction as well as provide feedback enables a large pool of people to get involved, which ultimately results in better quality! Transparency, openness, and vendor neutrality were not part of the JCP. </p>
	<p><strong>Compatible Implementations</strong>: The JCP required that each specification version has a corresponding Reference Implementation. The EFSP will require at least one Compatible Implementation of a specification, however, we are welcoming and encouraging other implementations of the specification and are avoiding singling out or favoring particular implementations or a vendor. We are hoping this will further encourage adoption and involvement of the community.</p>
	<p><strong>Self-certification</strong>: For the certification process for the EFSP, we utilize a self-serve model, thus lowering the costs and effort involved in carrying out certifications. The Eclipse Foundation will provide all the necessary information and instructions for the certification and make them available for download. There is an explicit requirement for all test results to be made publicly available so that verification can be carried out.</p>
	<p>specification processes' are, by nature, involved, detailed, and fairly complex. Care has been taken to ensure the overhead associated with engaging in the specification process is no more significant than it needs to be. But, as we further progress and learn, we will tweak the process based on these learnings.</p>
	<p>We hope you'll get involved!</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/january/images/tanja.jpg"
        alt="Farah Papaioannou" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Tanja Obradovic<br />
            <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/TanjaEclipse">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>