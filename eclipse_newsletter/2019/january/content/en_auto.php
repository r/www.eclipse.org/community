<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Worldwide the automotive industry is changing at an unprecedented rate. Emerging automated, connected and electric (ACE) automotive technologies enable new mobility services in a shared (S) economy. These innovations are, in turn, changing the way both carmakers and consumers derive value from vehicles through new revenue models and environment-vehicle-driver interaction.</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/january/images/autonomousdriving.png"></p>
	<p>At the heart of these developments is an increased focus on software-driven solutions. Not surprisingly, the car industry's focus and investments on software and solutions will continue to rise rapidly, reaching &#0036;82.01 billion in 2020, according to Frost &amp; Sullivan. This implies a fundamental shift in the core competencies of automotive players throughout the supply chain. To avoid becoming locked-in by their traditional roots in mechanical engineering and being bypassed by new entrants like Tesla and Waymo, they need to become software centered, fast.</p>
	<p>Unfortunately, the speed of innovation in software development is faster than in traditional car engineering and developers have become a scarce commodity. To realize the ACES vision and keep up with customer demands, the automotive OEMs realize they will need to achieve and sustain innovation at a digital scale and pace. No wonder automakers have begun to understand that, to stay in the race, they are obliged to pool resources and work smarter together. Industry-wide collaborations like AutoSar and GENIVI have already paved the way, but open source software development - as proven in the IT sector - is the most viable option to collaborate and innovate.</p>
	<p>Many carmakers and their suppliers have already recognized this fact and joined efforts at Eclipse Foundation, e.g. Daimler, Toyota, BMW, Volkswagen, Audi, Bosch, and Siemens.</p>
	<br>
	<h4>Eclipse Foundation and the car of the future</h4>
	<p>The Eclipse Foundation is well positioned to provide a platform for the automotive industry to collaborate pre-competitively on new technology. It has a proven track record of enabling open collaboration and innovation earned over 15 years. Our collaborative projects have resulted in over 162 million lines of code, representing an &#0036;8.7 billion shared investment.</p>
	<p>Since the center of gravity for automotive innovation has shifted towards cloud computing, Big Data, artificial intelligence and machine learning, it makes good business sense to tap into the Foundation's accumulated body of knowledge and experience. Thereby jumping the learning curve, freeing up scarce resources, increase operational agility and speed up the time-to-market of new offerings.</p>
	<p>Companies compete with differentiated products and services. They derive business value from developing differentiating features, which are introduced to the market ahead of competitors to reap premium prices and grow. Other potential investments only attribute to the company's bottom line if they incur cost savings and thereby increase the gross margins. These savings relate to the development and maintenance of commodity features as well as back-office activities. To separate value add from cost efficiency an organization's activities can be placed respectively above and below the so-called 'value line'.</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/january/images/auto_2.PNG"></p>
	<p>Working with other industry players - even your fiercest competitors - on technology frees up scarce organizational resources to focus on delivering differentiating features faster. By pooling the development effort associated with back-end or commodity capabilities and activities open source participants can save on headcount, HR, and infrastructure. While overall development costs are lowered, at the same time business risk is mitigated by accelerated market adoption of technologies and standards.</p> 
	<br>
	<h4>How the Foundation provides service</h4>
	<p>Automotive Working Groups foster an open, member-driven environment leveraging the proven Eclipse Working Group model. They provide an open and vendor-neutral governance framework for individuals and organizations to engage in collaborative development. Combined with efficient development processes and rigorous intellectual property services, the end result is clean code that can be readily built into commercial products.</p>
	<p>Working Groups help member organizations from across the mobility ecosystem to collaborate on developing core frameworks, tools, and systems for interoperability, simulation, testing, validation, and certification. Thus enabling software developers to create the automotive software stacks of the future, regardless of individual technology choices by suppliers involved.</p>
	<p>Besides the fact that the automotive OEM's and suppliers have to collaborate in new ways and across traditional supply chains, they need to reach out and tap into other industry sectors. Increasingly 'innovation happens elsewhere' in domains such as Cloud, AI/ML and IoT. Where new technologies reside which are crucial to autonomous driving and the connected car. Eclipse Foundation hosts over 300 individual software projects and other Working Groups dedicated to domains such as Research, Science, IoT, and Geospatial. To benefit our members we encourage cross-Working Group and cross-project collaboration.</p>
	<br>
	<h4>Automotive Working Groups and projects</h4>
	<p>A wide variety of companies, NGO's, Research &amp; Science, Government, individuals already participate and new ones are coming on board rapidly. Meanwhile, the scope of technologies involved ranges from frontend in-vehicle all the way to the backend in the cloud. Already auto OEMs like Audi, BMW, Daimler, Toyota, and Volkswagen already use the proven WG model to pool their resources. While the likes of DLR, T&#0252;v-S&#0252;d and various others add to the mix.</p>
	<p>As a means to deliver the Eclipse Foundation services to the projects and members, the concept of Working Group (WG) has been implemented. These services are provided through these WG's and include; co-funding, IP management, community development, branding, specifications, and certification. While openness and transparency of the processes involved are safeguarded through established governance practices, thus satisfying important requirements regarding pre-competitive development, vendor neutrality. The various components of the Eclipse Governance structure include; its bylaws, the Working Group Charter, the Eclipse Public License (EPL 2.), its Anti-trust and IP policies and - last but not least - the Eclipse Development Process.</p>
	<p>There are a number of WG's 'on the road'. The OpenPASS WG is motivated by the need of advanced driver assistance systems and partially automated driving functions to assess safety effects in traffic. The application of open source provides - through deliverables as core frameworks and modules - the much-needed reliability and transparency of results obtained by the simulation. WG members include Audi, BMW, Daimler, Mueller BBM, Siemens, Peak Solutions and ITK Engineering.</p>
	<p>Within the OpenMDM WG companies foster and support an open and innovative ecosystem. The focus is on providing tools and systems, qualification kits and adapters for validated, standardized and vendor independent management of measurement data in accordance with the ASAM ODS standard. WG members include TATA, Daimler, Itemis, Siemens, Canoo and HighQSoft.</p>
	<p>Another interesting and pivotal development project (presently part of the Eclipse Research Initiative) for the automotive industry is <a href="https://www.eclipse.org/kuksa/">Eclipse Kuksa</a>. This cross-domain project unifies technologies across the vehicle, IoT, cloud, and security domains in order to provide an open source ecosystem to developers while addressing the challenges of the electrified and connected vehicle era.</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/january/images/auto_3.PNG"></p>
	<br>
	<h4>New initiatives</h4>
	<p>For 2019 a number of new Automotive Working Groups are in the making. One will drive the evolution and broad adoption of mobility modeling and simulation technologies. It will foster the development of a framework for the detailed simulation of the movement of vehicles, people, and their communications. The project around which the collaboration will be organized is the <a href="https://projects.eclipse.org/projects/technology.sumo">Eclipse Sumo</a> project as championed by the German Aerospace Agency DLR.</p>
	<p>Another initiative aims to overcome the incompatibility between the various toolchains in use. This is an industry-wide issue, which slows down development and costs precious money and other resources. To increase interoperability the goals are to deliver an industry-wide definition of the autonomous driving toolchain and deliver a reference architecture defining the interoperability of interesting in-scope technologies.</p>
	<p>Furthermore, the impact of AI on the car of the future is certainly not to be overlooked. Therefore, in the short term, the Foundation envisions a collaborative platform for assessment and certification of AI for ADAS/Autonomous Driving.</p>
	<br>
	<h4>Join the Movement</h4>
	<p> The Eclipse Foundation has a strategic focus on one of the most exciting and rapidly evolving industry sectors around. By giving the automotive supply chain a home and platform for open innovation and pre-competitive collaboration to develop the car of the future. An increasing amount of new projects is being on-boarded, thus creating a comprehensive, state of the art technology suite. By providing each project a dedicated home at one of the growing number of automotive Working Groups essential requirements regarding openness, business-friendly licensing, IP-cleanliness, vendor-neutrality and pre-competitiveness are satisfied.</p>
	<p>If you are a developer and you would like to get involved in one of the projects, or if you are an organization interested in becoming part of our automotive ecosystem, please contact <a href="mailto:ralph.mueller@eclipse-foundation.org">ralph.mueller@eclipse-foundation.org</a> or <a href="mailto:gael.blondelle@eclipse-foundation.org">gael.blondelle@eclipse-foundation.org</a></p>
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/january/images/Marc.JPG"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Marc Vloemans<br />
            <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/MarcVloemans">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>