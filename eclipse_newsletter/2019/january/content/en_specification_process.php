<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The <a href="https://www.eclipse.org/projects/efsp/">Eclipse Foundation Specification Process</a> (EFSP) defines a general framework for developing specifications in open source at the Eclipse Foundation. At the heart of the EFSP is the notion of an open source project and, much like an open source software project, an open source specification project is concerned with creating various artifacts in an open and transparent manner. In the case of a specification project, however, at least one of the artifacts that's produced is a specification document that describes how software should be implemented. </p>
	<p>The EFSP extends the <a href="https://www.eclipse.org/projects/dev_process">Eclipse Development Process</a> (EDP). The EDP defines the governance of open source projects at the Eclipse Foundation. This EDP describes, for example, our open source "rules of engagement" (open, transparent, meritocratic), how open source projects are structured, roles and relationships, and our review process around releases. The EFSP adds a few extra checks and balances.</p>
	<p>To support specification development, we've extended our open source project model to include a specific notion of a specification project. At a basic level, designation as a specification project is a flag that indicates that the open source project that is concerned with producing one or more specifications. In practical terms, unlike regular Eclipse open source projects, specification projects are explicitly aligned with exactly one Eclipse Foundation working group; the specification committee designated by the <a href="https://www.eclipse.org/org/workinggroups/">working group</a> plays a key role in the governance of the project. </p>
	<p>A full discussion of working groups is out of scope; the short version is that while open source projects provide a means for individuals to work together, working groups provide a means for companies to work together. A specification committee represents the interests of a working group in the specification process.</p>
	<p>One of the roles of the specification committee is to ensure that specification work stays in-scope. Scope has always been an important concept for Eclipse open source projects: all new Eclipse open source projects are required declare their scope as part of their proposal; that scope must be approved by the Eclipse Foundation's Executive Director and the membership at large. All Eclipse projects are required to ensure that their work remains within the defined scope (and a change in scope requires a formal community review).</p>
	<p>Scope is especially important for specification projects. So much so, that our regular processes are extended to require specification committee approval of the scope before a project can even be created, as well as approval of release plans and all corresponding reviews to ensure that work done by the project falls within the defined scope. The specification committee must also approve of any changes in scope.</p>
	<p>Like regular Eclipse open source projects, a <a href="https://www.eclipse.org/projects/efsp/#efsp-projects">Specification Project</a> starts life as a Proposal with a description, scope, list of committers, and more; goes through an iterative development cycle that produces one or more Milestone builds; and then engages in a <a href="https://www.eclipse.org/projects/handbook/#release">release process</a>.</p>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="/community/eclipse_newsletter/2019/january/images/lifecycle.png"></p>
	<p>The specification committee needs to approve of the creation of a specification project from a proposal by taking a role in the <a href="https://www.eclipse.org/projects/efsp/#efsp-reviews-creation">creation review</a>. Following successful creation and provisioning of project resources, the specification project begins development. During the development cycle, the project team must produce at least one milestone build of the specification's content (documentation and technical artifacts) to solicit feedback, and at least one of the milestone builds must serve as a trigger to engage in a <a href="https://www.eclipse.org/projects/efsp/#efsp-reviews-progress">progress review</a>.</p>
	<p>Progress reviews, a new addition to the EDP introduced with the <a href="https://www.eclipse.org/projects/dev_process/development_process_2018/">2018 version</a>, are roughly equivalent to <a href="https://www.eclipse.org/projects/efsp/#efsp-reviews-release">release reviews</a>, but have the intent of ensuring that the specification project is progressing in a manner that will ultimately result in a successful release. A specification committee and project leadership may compel a specification project to engage in additional progress reviews.</p>
	<p>For a progress review to conclude successfully, the Project Management Committee (PMC), and Eclipse Management Organization (EMO) must validate that the work done to date has been in-scope, that the project team is following the EDP and EFSP, and that the Eclipse Foundation's Intellectual Property Policy is being correctly implemented.</p>
	<p>At the end of every release cycle, the project team must produce a release candidate build that we label as a specification version and then engage in a release review. For a release review, the PMC, EMO, and specification committee all engage in the same sorts of activities that they do for a progress review, but with the understanding that approval results in the ratification of the specification and promotion to an official status.</p>
	<p>Following a successful release review, the final release version of the specification artifacts is considered ratified and morph into what the process refers to as a final specification. It is the final specification that must be used to build compatible implementations.</p>
	<p>Following a successful first release (and every subsequent release thereafter), and before engaging in any further development of the specification, the project team must assemble and present their plan for review by the specification committee via <a href="https://www.eclipse.org/projects/efsp/#efsp-reviews-plan">plan review</a>. The notion of a plan review is specific to the EFSP (since plan reviews are not part of the EDP, no formal involvement from the PMC is required). A plan review provides the Specification Committee with an opportunity to ensure that the plan for the next specification version is in-scope, fits within the overall vision of the working group, and is otherwise charting a path to eventual ratification and release. After the Plan is approved, the Project Team engages in Development as before.</p>
	<p>We've crafted the EFSP to provide just enough process to satisfy the legal requirements that come from doing specification work while honoring the open and transparent nature of open source development defined by the EDP. In the coming months, we'll apply the EFSP to the Jakarta EE specifications currently hosted under the Eclipse EE4J Top-Level Project. As part of that process, we'll customize the process for the Jakarta EE working group and-as we learn more by applying the process-tweak it a bit. Specification work in open source is a relatively new concept; it's an exciting time.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/january/images/wayne.jpg"
        alt="Wayne Beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Wayne Beaton<br />
            <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>