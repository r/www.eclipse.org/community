<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<h4>Executive Summary</h4>
		<p>The ITEA PANORAMA project boosts design efficiency for heterogeneous automotive and aerospace systems. This open source project provides an environment for collaboration amongst diverse hardware and software technologies and teams, especially at the early stages of design. It supports efficient design decisions by defining evolving standards, tools and best practices for the exchange of non-functional, formal models.</p>
		<p>The main task of the project is to extend the scope of current system level approaches by enhancing existing abstract performance meta-models to be suitable for heterogeneous hardware, and heterogeneous function domains. We will stand on the shoulder of giants by building on the meta-model developed in the research projects AMALTHEA and AMALTHEA4public [2] that resulted in Eclipse APP4MC [3]. In addition, the project will consider results from other projects such as TIMMO, Timmo2USE, and ARAMiS I &amp; II. This way, the enhanced meta-model will be a common and open standard to support development by diverse parties across organizations.</p>
		<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/pano1.png"></p>
		<p>PANORAMA will also provide paths for integration and transition to the project technology. Existing modelling approaches will be respected and transformations into the existing meta-models will be offered. To ensure broad acceptance and justify the investment by the industry, the meta-model will need to be suitable for many use cases, ideally across the complete development cycle. This will result in a meta-model that is rather wide and generic. In addition, during product development, performance models are usually enriched and grow. These facts impose three important conditions:</p>
			<ul>
				<li>PANORAMA's approach must integrate well with existing ecosystems</li>
				<li>All information must be stored only once and at a single location</li>
				<li>Guidance will need to be provided to efficiently address specific design tasks with the right abstraction level of the performance model</li>
			</ul>
		<p>As a result, the project will need to ensure that</p>
			<ul>
				<li>the co-existence of models proposed here with established forms of information storage and system specification such as AUTOSAR, AUTOSAR adaptive, SysML, AADL, and EAST-ADL will be considered, and industrial practice is cross-checked</li>
				<li>the co-existence of tools deployed in design, both open source and commercial, is sought-after</li>
				<li>use case specific "views" on the performance models are provided that allow the developer to focus on specific design tasks at hand. These views must be suitable for exchange between parties.</li>
			</ul>
		<p>Use cases addressed by PANORAMA's methodology are, for example, assessment of different hardware architectures for a given software, assessment of deployment alternatives in a system, or guidance for optimization of system-level design decisions by visualization of analysis results.</p>
		<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/pano2.png"></p>
		<p>To address these use cases, in addition to the underlying modelling approach, static and dynamic analysis approaches will be provided. For instance, dynamic analysis based on performance simulation is one path we will take, paying attention to combining strengths of flexible and open solutions (such as SystemC) with established and mature commercial simulators. In the context of static analysis, we intend to provide analysis methods to enable checking system properties, such as performance, worst-case timing, and schedulability conditions, energy usages, path coverage, fault isolation, and security.</p>
		<p>As lessons learned from related projects and industrial practice, two further aspects will be considered to pave the way for methods and tools provided by PANORAMA:</p>
			<ul>
				<li>Closed source only and non-adaptable solutions are prohibitive in the heterogeneous era; flexibility for tools and methods is required to cope with fast-paced hardware trends. PANORAMA's solutions will be freely available and highly adaptable, for example as add-ons for Eclipse APP4MC [4]. Solutions provided by the project will be completed with commercial tools that offer the maturity and context required for industrial deployment.</li>
				<li>Documentation of meta-models often lacks formality; to eliminate confusion at the outset, our meta-model solutions are accompanied by reference implementations that clearly define their semantics and provide guidance for usage.</li>
			</ul>
		<p>To summarize, PANORAMA will provide comprehensive modelling tooling to enable effective integration of heterogeneous systems and tools, early in the process. The project is designed for engineers by engineers and we welcome your input as we embark on our next major stage in development. For more information, visit www.eclipse.org/APP4MC</p>
	<h4>Resources</h4>
		<ol>
			<li><a href="https://panorama-research.org">https://panorama-research.org</a></li>
			<li><a href="https://itea3.org/project/success-story/amalthea-and-amalthea4public-success-story.html">https://itea3.org/project/success-story/amalthea-and-amalthea4public-success-story.html</a></li>
			<li><a href="https://www.eclipse.org/app4mc/">https://www.eclipse.org/app4mc/</a></li>
			<li><a href="https://projects.eclipse.org/projects/technology.app4mc/developer">https://projects.eclipse.org/projects/technology.app4mc/developer</a></li>
		</ol>
<div class="bottomitem">
 <h3>Authors of Article</h3>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Author</th>
    </tr>
  </thead>
  <tbody>
    <tr>
  		<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/luk.png"></th>
		<td>Lukas Krawczyk is a Ph.D. student and computer science researcher in the Institute for the Digital Transformation of Application and Living Domains at Dortmund University of Applied Sciences. Broadly, his research focuses on analyzing the timing behaviour and optimizing the deployment of automotive software for embedded multi- and many-core systems. Since 2011, Lukas is involved in a series of international ITEA projects, such as AMALTHEA, AMALTHEA4public, APPSTACLE, and PANORAMA. He became a committer for Eclipse APP4MC in 2015, Eclipse Kuksa in 2017, and has mentored six Google Summer of Code (GSoC) projects on behalf of the Eclipse Foundation. Since 2019, Lukas is co-project-leader and workpackage 4 leader for the research project PANORAMA.</td>
    </tr>
     <tr>
     	<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/jor.png"></th>
		<td>Joerg Tessmer is a project manager within the Cross Automotive Platform division at the Robert Bosch GmbH. He holds a Diploma in Automation and Electrical Engineering from the University of Applied Science in Giessen-Friedberg. He has more than 15 years of experience in tool development and rollout of model based design approaches for embedded SW engineering. He is project leader for several European Research projects in the automotive domain related to resource optimization of Embedded Systems and connected vehicle ecosystem. Currently he leads the European research Project Panorama.</td>
    </tr>
   <tr>
   		<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/har.png"></th>
		<td>Harald Mackamul is working as senior expert at Bosch Corporate Research. With many years of experience in the area of development tools, the recent focus is on the development of embedded multicore systems. In the European projects AMALTHEA and AMALTHEA4public he was technical project lead and responsible for the implementation and integration of the AMALTHEA platform. He is project lead and committer of the Eclipse project APP4MC and work package 1 leader for the research project PANORAMA.</td>
   </tr>
  </tbody>
</table>
</div>

<div class="bottomitem" style="background-color:#fff">
 <table class="table">
   <tbody>
    <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/itea.png"></th>
      <td>
      	<ul>
      		<li>Eclipse Project: <a href="eclipse.org/app4mc">eclipse.org/app4mc</a></li>
      		<li>Mailing-List: <a href="mailto:app4mc-dev@eclipse.org">app4mc-dev@eclipse.org</a></li>
      	</ul>
      </td>
      <td><img width="100px" src="/community/eclipse_newsletter/2019/july/images/app.png"></td>
    </tr>
  </tbody>
</table>
</div>