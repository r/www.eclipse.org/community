<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Eclipse SCAVA is an open source platform for automatically analyzing the source code, bug tracking systems, and communication channels of open source software projects. SCAVA provides techniques and tools for extracting knowledge from existing open source components and to use such knowledge to support the selection and reuse of existing software to develop new systems and to provide developers with real-time recommendations that are relevant to their current development tasks. Eclipse SCAVA  has been created out of the EU-funded CROSSMINER project under the H2020 Programme.</p>
	<p>As shown in Figure 1, Eclipse SCAVA fits conceptually between the developer and all the different and heterogeneous data sources that one needs to interact with when understanding and using existing open source components.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/scava1.png"></p>
	<p align="center"><em><strong>Figure 1: Overview of Eclipse Scava</strong></em></p>
	<p>Recommender systems typically implement four main activities i.e., data preprocessing, capturing context, producing recommendations, presenting recommendations. In this respect, both the Eclipse-based IDE and the Web-based dashboards make use of data produced by the mining tools working in the back-end of the Eclipse SCAVA infrastructure.</p>
	<p>The developer context is used as a query sent to the knowledge base that answers with recommendations that are relevant with respect to the developer contexts. Machine learning techniques are used to infer knowledge underpinning the creation of relevant real-time recommendations. The knowledge base infers more insight from raw data produced by the different mining tools, which include the following:</p>
		<ul>
			<li><strong>Source code miners</strong> to extract and store actionable knowledge from the source code of a collection of open source projects;</li>
			<li><strong>NLP miners</strong> to extract quality metrics related to the communication channels, and bug tracking systems of open source projects projects by using Natural Language Processing and text mining techniques;</li>
			<li><strong>Configuration miners</strong> to gather and analyze system configuration artifacts and data to provide an integrated DevOps-level view of a considered open source project; and</li>
			<li><strong>Cross-project miners</strong> to infer cross-project relationships and additional knowledge underpinning the provision of real-time recommendations.</li>
		</ul>
	<p>Additionally, Eclipse SCAVA provides the means to simplify the development of bespoke analysis and knowledge extraction tools by delivering a framework that will shield engineers from technological issues and allow them to concentrate on the core analysis tasks instead.</p>
<h3>The SCAVA Eclipse-based IDE and Web-Based Dashboards</h3>
	<p>The Eclipse SCAVA technical offering consists of several components that can be either singularly used or developers can benefit from integrated ways of using them via an Eclipse-based IDE and Web-based dashboards. Recommendation examples that Eclipse SCAVA is able to produce are shown below.</p>
<h4>Recommending Additional Libraries</h4>
	<p>As shown in Figure 2, depending on the set of third-party libraries currently being used by the software system being developed, Eclipse SCAVA can recommend additional libraries that should be included. Such recommendations are based on the similarity of the considered software system under development with respect to already existing and analyzed libraries.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/scava2.png"></p>
	<p align="center"><em><strong>Figure 2: Suggesting additional libraries</strong></em></p>
<h4>Recommending API Documentation</h4>
	<p>Depending on the set of selected libraries, the system shows API documentation and Q&amp;A posts that can help developers understand how to use the selected libraries. Figure 3 shows a list of ranked StackOverflow posts that are recommended with respect to their relevance to the selected source code directly in the editor.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/scava3.png"></p>
	<p align="center"><em><strong>Fig 3: Recommending Stack Overflow posts</strong></em></p>
<h4>Recommending API Function Calls and Usage Patterns</h4>
	<p>Eclipse SCAVA is also able to recommend API function calls that the developer might need to add to the method being developed. For instance, Figure 4 contains a representative example where the findBoekrekeningen method queries the available entities and retrieves those of type Boekrekening. To this end, the Criteria API library is used. The situation shown in Figure 4 is when the development is in an early stage and the developer already used some methods of the chosen API to develop the required functionality. However, they are not sure how to proceed from this point. In such cases, different sources of information may be consulted, such as StackOverflow, video tutorials, API documentation, etc. For instance, by providing the first three statements of the findBoekrekeningen method declaration, Eclipse SCAVA can provide developers with recommendations consisting of a list of API method calls that should be used next, and code snippets that could support developers in completing the method definition with the framed code in Figure 4. </p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/scava4.jpg"></p>
	<p align="center"><em><strong>Fig 4: Recommending API function calls</strong></em></p>
<h4>Web-Based Dashboards</h4>
	<p>Eclipse SCAVA dashboards present up to date, high level, and quantitative panoramic views of analyzed projects.  They include specific metrics for tracking key performance aspects and show details like who and how they are contributing to a given project.  Dashboard panels show summary, aggregated and evolutionary data, statistical analysis, faceted views, etc. for each data source. The interface allows drilling down into the data combining different filters (time ranges, projects, repositories, contributors). NLP analysis tools can also be applied to retrieve all the sentiments related to the analyzed projects. For instance, Figure 5 shows an interactive Wheel of Emotions that can be used to filter the sentiments shown in the dashboard.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/scava5.png"></p>
	<p align="center"><em><strong>Fig. 5: Evolution in time of Sentiments in Issues</strong></em></p>
<h3>References</h3>
	<p>Eclipse SCAVA has been created out of the EU funded CROSSMINER project <a href="https://www.crossminer.org/">(https://www.crossminer.org/)</a> which has been supported under the Horizon 2020 Programme. For details about the installation and usage of ECLIPSE SCAVA, please refer to <a href="https://scava-docs.readthedocs.io">https://scava-docs.readthedocs.io</a>.</p>
	
	
<div class="bottomitem">
 <h3>Authors of Article</h3>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Author</th>
    </tr>
  </thead>
  <tbody>
    <tr>
  		<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/scava6.png"></th>
		<td>Davide Di Ruscio is an Associate Professor at the Department of Information Engineering Computer Science and Mathematics of the University of L'Aquila. His main research interests are related to several aspects of Software Engineering, Open Source Software, and Model-Driven Engineering (MDE) including domain specific modelling languages, model transformation, model differencing, model evolution, and coupled evolution. He has been in the PC and involved in the organization of several workshops and conferences, and reviewer of many journals like IEEE Transactions on Software Engineering, Science of Computer Programming, Software and Systems Modeling, and Journal of Systems and Software. He is a member of the steering committee of the International Conference on Model Transformation (ICMT), of the Software Language Engineering (SLE) conference, of the Seminar Series on Advanced Techniques &amp; Tools for Software Evolution (SATTOSE), of the Workshop on Modelling in Software Engineering at ICSE (MiSE) and of the International Workshop on Robotics Software Engineering (RoSE). Davide is in the editorial board of the International Journal on Software and Systems Modeling (SoSyM), of the Journal of Object Technology, and of the IET Software journal. Since 2006 he has been working on different European and Italian research projects by contributing the application of MDE concepts and tools in several application domains (e.g., service-based software systems, autonomous systems, open-source software systems, and hybrid polystore systems). Currently, Davide is the technical director of the H2020 CROSSMINER project <a href="https://www.crossminer.org/">https://www.crossminer.org/</a>.  More information is available at <a href="http://people.disim.univaq.it/diruscio">http://people.disim.univaq.it/diruscio</a>.</td>
    </tr>
  </tbody>
</table>
</div>
<div class="bottomitem" style="background-color:#fff">
 <table class="table">
   <tbody>
    <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/flag.png"></th>
      <td>
      	<ul>
      		<li>Eclipse Project: <a href="eclipse.org/scava">eclipse.org/scava</a></li>
      		<li>Mailing-List: <a href="mailto:scava-dev@eclipse.org">scava-dev@eclipse.org</a></li>
      		<li>Website:<a href="crossminer.org"> crossminer.org</a></li>
      		<li>Twitter:<a href="twitter.com/crossminer"> @crossminer</a></li>
      	</ul>
      </td>
      <td><img width="100px" src="/community/eclipse_newsletter/2019/july/images/scava7.png"></td>
    </tr>
  </tbody>
</table>
</div>