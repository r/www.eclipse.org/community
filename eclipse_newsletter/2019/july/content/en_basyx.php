<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The Eclipse BaSyx project is the open source result of the German research project "BaSys 4.0", which is funded by the Ministry for Education and Research (grant no. 01IS16022). Drawing inspiration from Autosar, BaSyx is a middleware in the sense that it sits on top of existing communication standards used in industrial automation, providing an abstraction/compatibility layer to industrial applications. While the project has wrapped up, its successor, "BaSys 4.2", kicked off this month and will conclude in three years.</p>
<h4>What is Industry 4.0?</h4>	
	<p>Industry 4.0 refers to the current digitization of Industrial automation as a 4th industrial revolution after mechanization, electrification, and computerization, respectively. Other names used are Industrial IoT or just Industrial Internet. Industry 4.0 is not a single concept, but a collection of concepts and objectives:</p>
		<ul>
			<li>Striving for a flexible, reconfigurable production that enables quick changes and small lot sizes.</li>
			<li>Open, highly interconnected automation systems that allow for data access across several layers of the automation pyramid, and even factories.</li>
			<li>A general higher infusion of modern information technology concepts in automation (like Big Data).</li>
			<li>A reflectance of the ongoing paradigm shift from embedded systems to cyber-physical systems.</li>
		</ul>
	<p>However, addressing these objectives is difficult, if not impossible, with the typical IT infrastructure as it is found in today's shop floors. It lacks the required flexibility, interoperability, and abstraction. With BaSyx, we intend to remedy this situation by providing a service-oriented architecture through a standardized interface and a virtual bus (called Virtual Automation Bus) on top of the existing network infrastructure. That way, both modern (e.g. OPC UA), as well as legacy systems, are supported. The universal interface in a BaSyx network is the Asset Administration Shell, which is the digital representative of a real-life object. Altogether, this approach is supposed to enable a service-based production.</p>
<h4>Asset Administration Shells</h4>
	<p>The <em>Asset Administration Shell</em> (AAS) is a concept currently standardized by the German Industry. The core idea is that <em>every</em> asset in the production process, e.g. a machine, a production line, a worker, or a product, has such an administration shell, which</p>
		<ul>
			<li>contains and/or points to every digital information available for that asset, and</li>
			<li>allows accessing capabilities of an asset via service calls</li>
		</ul>
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/july/images/componant.png"></p>
	<p>An asset, together with its AAS, is called an Industry 4.0 component. An AAS is organized into sub-models, which contain data and services of a specific aspect of an asset. Some examples of possible submodels are</p>
		<ul>
			<li>The geometric properties of an asset</li>
			<li>The topology of an asset composed of sub-assets, e.g. a production line</li>
			<li>Formulas and/or simulation models describing the physical process implemented by the asset</li>
			<li>User documentation</li>
		</ul>
	<p>Users are able to define their own submodels to custom tailor the asset administration shells to their application. Additionally, there is a set of suggested submodels provided by BaSyx. For example, each asset can have a service submodel, containing all provided services. This service submodel can then be used to enable a service-based production by providing a unified means of accessing the manufacturing capabilities of an asset.</p>
	<p>The AAS is the central point of access in the BaSyx Middleware. For example, if an application wants to use a certain capability of a certain device, it calls the corresponding service of the device's AAS. Explorative approaches are also supported, e.g. an application can browse through the services listed in the AAS, or search for a specific one. In addition, via a directory service, an application can search for administration shells that provide certain services.</p>
	<p>The BaSyx middleware features an open source implementation of a distributed Asset Administration Shell. That means the information provided by an AAS can reside in different places in the network; e.g. different submodels might have different locations, or extensive history data of several AAS might be put on a dedicated server.</p>
	<p>The connection between an application and an AAS, between an AAS and a remote submodel, and between an AAS and a device is established in the background via the <em>Virtual Automation Bus</em>.</p>
<h4>The Virtual Automation Bus</h4>
	<p>There are many different protocols in the manufacturing domain. There are relatively new approaches, e.g. OPC UA, but also legacy protocols like Profibus. To be able to integrate all devices within a plant with each other, there has to be an integration of this heterogeneous communication system. The Virtual Automation Bus (VAB) is inspired by the Virtual Function Bus in Autosar. It provides end-to-end communication, possibly bridging several protocols. Thus, it allows accessing properties and operations provided by the VAB participants. Additionally, it enables the connection between legacy devices and state-of-the-art applications.</p>
	<p>To be able to provide end-to-end communication between devices potentially using different protocols, two key components are used. A Directory Service, with a known and statically configured address, allows the registration and lookup of VAB members through their unique ID. A lookup operation returns the path to the asset, i.e. its address within the VAB. Since various protocols can be used, it is possible that this address does not directly point to the asset but instead to the second VAB key component, the Gateway. Gateways perform the translation between the different protocols using an intermediate language.</p>
	<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/july/images/table.png"></p>
	<p>This intermediate language is very similar to the CRUD approach known from persistent storage. Additionally, it is extended to allow natively invoking operations to support a service-based production.</p>
	<p>The following primitives are supported by the VAB: Create, Delete, Read, Update and Invoke. These primitives are mapped to existing protocols, e.g. HTTP/REST. For example, an Invoke primitive translates to an HTTP-POST while a Get primitive is translated to an HTTP-GET.</p>
<h4>Legacy Support</h4>
	<p>Starting in green field is very rare; typically, there is a mixture of old, non-Industry 4.0 ready devices, and newer device, i.e. a brown field. Thus, it is necessary to provide a migration path for older devices.</p>
	<p>BaSyx supports the integration of legacy devices through the concept of cut-in devices. These cut-in devices provide a mapping of the VAB primitives to the legacy protocol. For example, a mapping between a native protocol message and the desired property can be created statically.</p>
<h4>Conclusion</h4>
	<p>With its Asset Administration Shell approach combined with the Virtual Automation Bus concept, BaSyx intends to enable the Industry 4.0 scenarios envisioned today. In the BaSyx GIT repo, you will find SDKs for Java, C++, and C#, with the Java version being the most advanced at the moment.</p>
	<p>You can find more technical information in the <a href="https://wiki.eclipse.org/BaSyx" target="_blank">BaSyx Wiki</a>. More information about the research project BaSys 4.0 and our project partners is available on <a href="http://www.basys40.de/" target="_blank">http://www.basys40.de/</a> (in German).</p>
<div class="bottomitem">
 <h3>Authors of Article</h3>
 <table class="table table-bordered">
  <tbody>
     <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/bas1.png"></th>
		<td>Frank Schnicke is an engineer at the Fraunhofer Institute for Experimental Software Engineering IESE. His research focuses on Industry 4.0 software architectures that enable changeability in industrial plants. Additionally, he is coordinating the implementation of the Eclipse BaSyx reference implementation.</td>
    </tr>
   <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/bas2.jpg"></th>
		<td>Dr. Markus Damm is a researcher at the Fraunhofer Institute for Experimental Software Engineering IESE. His work focuses on Software Architectures and Simulation. He is coordinating the BaSys 4.0 research project, which is funded by the German Ministry of Education and Research, and provides its results open source via Eclipse BaSyx.</td>
   </tr>
     <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/bas3.png"></th>
		<td>Dr. Thomas Kuhn is the head of the Embedded Systems division at the Fraunhofer Institute for Experimental Software Engineering IESE. His focus is the development of reliable adaptive embedded systems. He is the technical lead of the BaSys 4.0 research project and Eclipse BaSyx project lead.</td>
     </tr>
  </tbody>
</table>
</div>

<div class="bottomitem" style="background-color:#fff">
 <table class="table">
   <tbody>
    <tr>
      <th scope="row"><img width="150px" src="/community/eclipse_newsletter/2019/july/images/bas4.png"></th>
      <td>
      	<ul>
      		<li>Eclipse Project: <a href="https://www.eclipse.org/basyx/">eclipse.org/basyx</a></li>
      		<li>Mailing List: <a href="mailto:basyx-dev@eclipse.org">basyx-dev@eclipse.org</a></li>
      		<li>Website: <a href="https://www.basys40.de/">basys40.de</a></li>
      	</ul>
      <td><img width="100px" src="/community/eclipse_newsletter/2019/july/images/bas5.png"></td>
    </tr>
   <tr>
   	<td colspan="3">Eclipse BaSyx has its origins in the BaSys 4.0 project that is funded by the german Bundesministerium fur Bildung und Forschung (BMBF)</td>
   </tr>
  </tbody>
</table>
</div>