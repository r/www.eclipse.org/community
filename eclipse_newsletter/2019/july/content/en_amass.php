<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>This newsletter represents the continuation of the one published last year<sup>1</sup>, were we presented the toolchain developed by the EU ECSEL AMASS (Architecture-driven, Multi-concern and Seamless Assurance and Certification of Cyber-Physical Systems) project, integrated under the umbrella of the PolarSys OpenCert project. In a nutshell, AMASS toolchain aims at increasing the efficiency of assurance and certification activities and  lowering certification costs in the face of rapidly-changing product features and market needs. The purpose of this article is to report the final achievements of the project and also to call for users and stakeholders in order to extend the ecosystem and the community around it. In what follows, we go over the <a href="https://docs.google.com/document/d/1C9l8I3TeNH4F0YJiX5zY1zv2m7xIZW_L5eoG0B8DEGE/edit?ts=5d276b85#heading=h.rir2gx7f3i4r">context and problem</a>, <a href="https://docs.google.com/document/d/1C9l8I3TeNH4F0YJiX5zY1zv2m7xIZW_L5eoG0B8DEGE/edit?ts=5d276b85#heading=h.qteb38o3xfni">derived scientific objectives</a>, the achieved <a href="https://docs.google.com/document/d/1C9l8I3TeNH4F0YJiX5zY1zv2m7xIZW_L5eoG0B8DEGE/edit?ts=5d276b85#heading=h.gandds999yye">tangible results</a>, the <a href="https://docs.google.com/document/d/1C9l8I3TeNH4F0YJiX5zY1zv2m7xIZW_L5eoG0B8DEGE/edit?ts=5d276b85#heading=h.f0z65o3jby15">demonstrators and the used evaluation framework</a>, and, last but not least, the main identified <a href="https://docs.google.com/document/d/1C9l8I3TeNH4F0YJiX5zY1zv2m7xIZW_L5eoG0B8DEGE/edit?ts=5d276b85#heading=h.1if4g0fnsgvp">roles and stakeholders</a>.</p>
<h3>Context and Problem:</h3>
	<p>Embedded systems have significantly increased in technical complexity towards open, interconnected cyber-physical systems (CPS), exacerbating the problem of ensuring rolesseveral dependability concerns such as safety and security. Unlike practices in electrical and mechanical equipment engineering, CPS do not have a set of standardized and harmonized practices for assurance and certification that ensures safe, secure, and reliable operation with typical software and hardware architectures. As a result, the CPS community often finds it difficult to apply existing certification guidance. Ultimately, the pace of assurance and certification will be determined by the ability of the industry and the certification and assessment authorities to overcome technical, regulatory, and operational challenges. Another key difficulty appears when trying to reuse CPS products between projects and even from one application domain to another. Product evolutions become costly and time-consuming because they entail regenerating the entire body of evidence or their certification can be constrained by different standards. This may imply that the full assurance and certification process is applied as for a new product, thus reducing the return on investment of such reuse decision.</p>
<h3>Derived Scientific Technical Objectives:</h3>
	<p>AMASS has defined four Scientific and Technical Objectives (STOs):</p>
		<ul>
			<li><strong>Architecture-Driven Assurance </strong>: Explicit integration of assurance and certification activities with the CPS development activities, including specification and design. It provides support for system components composition in accordance with the domain best practices, guaranteeing that emerging behavior does not interfere with the whole system assurance.</li>
			<li><strong>Multi-concern Assurance </strong>: Tool-supported methodology for the development of assurance cases, co-assessment, and contract-based assurance, which addresses multiple system characteristics (mainly safety and security, but also other dependability aspects such as availability, robustness, and reliability).</li>
			<li><strong>Seamless Interoperability </strong>: Open and generically applicable approach to ensure the interoperability between the tools used in the modelling, analysis, and development of CPS, among other possible engineering activities; in particular, interoperability from an assurance and certification-specific perspective, and collaborative work among the stakeholders of the assurance and certification of CPS.</li>
			<li><strong>Cross/Intra-Domain Reuse </strong>: Consistent assistance for intra- and-cross-domain reuse, or cross-concern, based on a conceptual framework to specify and manage assurance and certification assets.</li>
		</ul>
<h3>AMASS Tangible Results</h3>
	<p>As summarized in Figure 1, the AMASS Project created and consolidated the first European assurance and certification open tool platform (which implements the AMASS reference tool architecture), and an ecosystem and self-sustainable community spanning the largest CPS vertical markets.</p>
	<p>Since the inception of the AMASS project, the consortium agreed to deliver the AMASS Tool Platform results in open source. This ensures both the usability of the platform to fulfill AMASS objectives and the interoperability between potential specializations of AMASS.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass1.png"></p>
	<p align="center"><em><strong>Figure 1: AMASS Tangible Outcomes</strong></em></p>
	<p>AMASS has proposed the Common Assurance and Certification Metamodel (CACM)<sup>2</sup> where concepts about process and system assurance are included. The CACM provides a means to integrate the domain-specific languages resulting from the OPENCOSS<sup>3</sup>, SafeCer<sup>4</sup> , and CHESS<sup>5</sup> projects and allows for the integration and extension of their conceptual, modelling, and methodological frameworks. Based on this CACM, the AMASS Reference Tool architecture (ARTA) has been developed (point 1 in Figure1). </p>
	<p>The AMASS Reference Tool Architecture represents a virtual entity that embodies a common set of tool interfaces/adaptors, working methods, tool usage methodologies, and protocols that is expected to allow any stakeholder of the assurance and certification/qualification activities to seamlessly integrate their activities (e.g., product engineering, external/independent assessment, component/parts supply) into tool chains adapted to the specific needs of the targeted CPS markets. During the AMASS Project's life, the ARTA architecture has been implemented, thus creating the AMASS Open Platform, which is available at <a href="https://polarsys.org/opencert/downloads/">https://polarsys.org/opencert/downloads/</a>.</p>
	<p>The internal and external tools integrated into and available for the AMASS Platform are shown in Figure 2, grouped with the aforementioned STOs. The STO Seamless Interoperability is not explicitly added as it is provided by the internal platform tools.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass2.png"></p>
	<p align="center"><em><strong>Figure 2: AMASS Platform Tools ecosystem</strong></em></p>
	<p>The internal tools are highlighted in blue, whereas the external ones are depicted in green. The internal tools are all available in Open Source and integrated into the AMASS Open Platform:</p>
		<ul>
			<li><strong>PolarSys OpenCert<sup>6</sup></strong> is the core of the AMASS Open Source platform. OpenCert, which was created by the members of the OPENCOSS research project<sup>2</sup>, supports evidence management, assurance case specification and part of the compliance management functionality (see Figure 3). It also includes new functionality implemented during the AMASS project, such as argument fragments generation starting from the information available in the system model (Polarsys CHESS) and tool interoperability with the OSLC technology<sup>7</sup>.</li>
		</ul>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass3.png"></p>
	<p align="center"><em><strong>Figure 3: Compliance management in OpenCert</strong></em></p>
	<p>The <strong>PolarSys CHESS<sup>8</sup></strong> toolset, which was created by the CHESS research project<sup>4</sup> and continued by SafeCer<sup>3</sup>, adds support for Architecture-Driven Assurance. The CHESS toolset leverages another important Eclipse project, the Papyrus<sup>9</sup> platform for UML (Unified Modelling Language) design and profiles, including system modelling with SysML (Systems Modelling Language) and MARTE (Modeling and Analysis of Real-time and Embedded systems). In particular, CHESS provides support for:</p>
		<ul>
			<li>Contract-based modelling and analysis via integration with xSAP<sup>10</sup> and OCRA<sup>11</sup> tools.</li>
			<li>Dependability modelling and analysis.</li>
			<li>Performance analysis through MARTE based modelling and integration with MAST<sup>12</sup> tool.</li>
			<li>Code generation facilities.</li>
		</ul>
	<p>In the context of the AMASS project, the support for contract-based and dependability modelling and analysis has been extended and brand new functionalities have been provided (e.g. support for architectural patterns, documentation generation, dedicated views to support the modelling activities and to check the analysis results) to enable the architecture-driven assurance process depicted in Figure 4.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass4.png"></p>
	<p align="center"><em><strong>Figure 4: CHESS process for architecture-driven assurance</strong></em></p>
		<ul>
			<li>EPF Composer<sup>13</sup>; this pre-existing Eclipse project, created by IBM more than a decade ago and brought back to life thanks to the AMASS contribution<sup>14</sup>, which is already used in the context of SafeCer, is a key component used to model the standards (a), the processes (b), the basic compliance (c) during the planning (Figure 5). In addition, from process models and standards models created in EPF Composer, users can semi-automatically generate process-based arguments aimed at offering justification for compliance as well as enabling compliance checking.</li>
		</ul>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass5.png"></p>
	<p align="center"><em><strong>Figure 5: Standards, processes and compliance modelling in EPF Composer<sup>15</sup></strong></em></p>	
		<ul>
			<li><strong>BVR Tool<sup>16</sup></strong> is a series of Eclipse plugins that implement the BVR metamodel, a language for enabling variability management in the context of safety-critical systems engineering. The BVR Tool supports feature modelling, resolution, realization, and derivation of specific family members.</li>
		</ul>
	<p>In AMASS, as part of the solutions for Cross-domain and Intra-Domain reuse, BVR Tool has been integrated with EPF Composer, CHESS toolset and OpenCert enabling the variability management at the process (Figure 6), product, assurance case level. </p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass6.png"></p>
	<p align="center"><em><strong>Figure 6: Managing variability at process level via the integration of EPF Composer and BVR Tool</strong></em></p>
	<p><strong>Capra<sup>17</sup></strong> is a dedicated traceability management tool that allows the creation, management, visualisation, and analysis of trace links within Eclipse. Capra is highly configurable and is used and extended in AMASS to allow the creation of traceability links between different artifacts and in particular to manage the links between assurance and system design/analysis related entities (e.g. see Figure 7), as logically depicted in Figure 4.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass7.png"></p>
	<p align="center"><em><strong>Figure 7: Traceability View</strong></em></p>
		<ul>
			<li><strong>CDO<sup>18</sup></strong> is both a development-time model repository and a runtime persistence framework. It is used to store all the data in AMASS.</li>
		</ul>
	<p>Apart from the internal tools, which integrate directly with the AMASS platform, external tools can be used via standardized, mostly open interfaces. For instance, WEFACT, a requirements-based workflow engine that couples to various tools, which perform the workflow activities, as depicted in Figure 8, uses EPF Composer's interfaces to import process models, representing the processes as prescribed by functional safety or cybersecurity standards. Among other features, WEFACT is capable of creating and managing requirements, plus those which represent the rules imposed by standards. These can then be exported and imported into the CHESS toolset.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass8.png"></p>
	<p align="center"><em><strong>Figure 8: WEFACT user interface example after importing a process model</strong></em></p>
	<p>The Enterprise Architect-based tool called MORETO ("Requirements tool") is another external tool which benefits from standardized interfaces. </p>
	<p>Whereas each Eclipse project has its own lifecycle, according to the Eclipse Development Process and its own website, the advantage of the AMASS Open Platform is that it synchronizes several different projects and contributions in consistent versions that can be downloaded from the Polarsys OpenCert Tools Platform website<sup>19</sup>.</p>
<h3>Demonstrators and Evaluation Framework</h3>
	<p>AMASS demonstrators are the result of the case studies implementation for several meaningful CPS industry segments. Partners have focused on modelling standards depending on their domain (industrial automation, automotive, railway, avionics, space, and air traffic), establishing an assurance project, and using the appropriate tools based on their exploitation objectives.</p>
	<p>The AMASS Evaluation Framework, based on the Goal-Question-Metric (GQM) approach, includes a set of quality metrics to quantify the benefits at the levels of architecture driven assurance, multi-concern assurance, seamless interoperability, and cross/intra domain reuse. This framework has been used in the context of reference case studies. More specifically, a benchmarking exercise has been conducted aimed at comparing results achieved thanks to the AMASS platform with those achieved via former state of practice. </p>
	<p>The AMASS benchmarking has been driven by project goals, including gain for design efficiency of complex CPS by reducing their assurance and certification/qualification effort; and reuse of assurance results (qualified or certified before the rise of technology innovation and sustainable impact in CPS industry).</p>
	<p>Evaluation results from the final AMASS Platform Prototype show the fulfillment of overall AMASS goals, which are set to improve the current situation in CPS design technologies:</p>
		<ul>
			<li><strong>G1</strong> to demonstrate a potential gain for design <strong>efficiency</strong> of complex CPS by reducing their assurance and certification/qualification effort by 50%.</li>
			<li><strong>G2</strong> to demonstrate  potential <strong>reuse</strong> of assurance results (qualified or certified before), leading to 40% of cost reductions for component/product (re)certification/qualification activities.</li>
			<li><strong>G3</strong> to demonstrate a potential rise of technology innovation led by a 35% reduction of assurance and certification/qualification <strong>risks</strong> of new CPS products. The reduction of risks can be "invested" into the risky adoption of new technologies, for which there was no space without the reduction.</li>
			<li><strong>G4</strong> to demonstrate a potential sustainable impact in CPS industry by increasing the <strong>harmonization</strong> and <strong>interoperability</strong> of assurance and certification/qualification tool technologies by 60%.</li>
		</ul>
<h3>Roles and Stakeholders</h3>	
	<p>The profiles of AMASS Platform users can be grouped as shown in Figure 9.</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/amass9.png"></p>
	<p align="center"><em><strong>Figure 9: AMASS Tool Platform Roles, taken from D2.5 User guidance and methodological framework<sup>20</sup> </strong></em></p>
	<p>In addition, potential stakeholders of the Open Source community are as follows: Tools Architects, Eclipse modelling experts, AMASS open platform developers, Researchers, Journalists, Industry / Decision makers, and general-purpose Open Source contributors.</p>
	
	
<h3>Resources</h3>
	<ol>
		<li><a href="https://www.eclipse.org/community/eclipse_newsletter/2018/july/amass.php">https://www.eclipse.org/community/eclipse_newsletter/2018/july/amass.php</a></li>
		<li>AMASS D2.4 Reference architecture (c), June 2018</li>
		<li>OPENCOSS research project<a href="http://opencoss-project.eu/"> http://opencoss-project.eu/</a></li>
		<li>SafeCer project (Certification of Software-intensive Systems with Reusable Components <a href="http://cordis.europa.eu/project/rcn/103721_en.html">http://cordis.europa.eu/project/rcn/103721_en.html</a> and <a href="http://cordis.europa.eu/project/rcn/105610_en.html">http://cordis.europa.eu/project/rcn/105610_en.html</a></li>
		<li> CHESS research project <a href="http://www.chess-project.org">http://www.chess-project.org</a></li>
		<li>OpenCert project page <a href="http://www.polarsys.org/projects/polarsys.opencert">http://www.polarsys.org/projects/polarsys.opencert</a></li>
		<li><a href=" http://trc-research.github.io/spec/km/"> http://trc-research.github.io/spec/km/</a></li>
		<li>PolarSys CHESS project<a href="http://www.polarsys.org/chess/start.html">http://www.polarsys.org/chess/start.html</a></li>
		<li>Papyrus project<a href=" http://www.eclipse.org/papyrus/"> http://www.eclipse.org/papyrus/</a></li>
		<li><a href="https://xsap.fbk.eu/">https://xsap.fbk.eu/</a></li>
		<li><a href="https://ocra.fbk.eu/">https://ocra.fbk.eu/</a></li>	
		<li><a href="https://mast.unican.es/">https://mast.unican.es/</a></li>	
		<li>Eclipse Process Framework Project (EPF) <a href="http://eclipse.org/epf/">http://eclipse.org/epf/</a></li>	
		<li> EclipseCon-2018, <a href="https://www.eclipsecon.org/france2018/session/get-epf-composer-back-future-trip-galileo-photon-after-11-years">https://www.eclipsecon.org/france2018/session/get-epf-composer-back-future-trip-galileo-photon-after-11-years</a></li>	
		<li>Ul Muram F., Gallina B., Kanwal S. (2019) A Tool-Supported Model-Based Method for Facilitating the EN50129-Compliant Safety Approval Process. In: Collart-Dutilleul S., Lecomte T., Romanovsky A. (eds) Reliability, Safety, and Security of Railway Systems. Modelling, Analysis, Verification, and Certification. RSSRail 2019. Lecture Notes in Computer Science, vol 11495. Springer, Cham</li>	
		<li> BVR Tool<a href="http://www.amass-ecsel.eu/content/bvr-tool-amass">http://www.amass-ecsel.eu/content/bvr-tool-amass</a></li>
		<li>Capra traceability framework<a href="http://projects.eclipse.org/projects/modeling.capra">http://projects.eclipse.org/projects/modeling.capra</a></li>	
		<li>CDO - Connected Data Objects <a href="http://www.eclipse.org/cdo/">http://www.eclipse.org/cdo/</a></li>
		<li> Polarsys OpenCert Tools Platform website<a href="http://www.polarsys.org/opencert/">http://www.polarsys.org/opencert/</a></li>
		<li>AMASS Deliverable D2.5 AMASS User guidance and Methodological framework, November 2018<a href="https://www.amass-ecsel.eu/sites/amass.drupal.pulsartecnalia.com/files/D2.5_User-guidance-and-methodological-framework_AMASS_Final.pdf"> https://www.amass-ecsel.eu/sites/amass.drupal.pulsartecnalia.com/files/D2.5_User-guidance-and-methodological-framework_AMASS_Final.pdf</a></li>				
	</ol>
<div class="bottomitem">
 <h3>Authors of Article</h3>
 <table class="table table-bordered">
  <tbody>
    <tr>
  		<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass10.png"></th>
		<td>Dr. Barbara Gallina is Associate Professor of Dependable Software Engineering at Malardalen University (Sweden), where she leads the Certifiable Evidences and Justification Engineering group. Within EU-funded projects, she has played various technical leadership roles at task/work-package and global project level. Within AMASS, a large EU-ECSEL funded project, she played various roles: technical manager at the global level, work package leader, task leader, and land coordinator. She has been member of several program committees related to dependability such as ISSRE, SafeComp, EDCC, LADC, COMPSAC-SEPT, AdaEurope, RSS-Rail, QUORS, WoSoCER, SASSUR, ReSACI, ISSA. Dr. Gallina is the author of over 100 articles in the area of dependable software engineering and certification. Page: <a href="http://www.es.mdh.se/staff/308-Barbara_Gallina">http://www.es.mdh.se/staff/308-Barbara_Gallina</a></td>
    </tr>
    <tr>
    	<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass11.png"></th>
    	<td>Jose Luis de la Vara is a Senior Researcher (Ramon y Cajal Fellow) at the Computing Systems Department of the University of Castilla-La Mancha (Spain), where he is also a member of the "Laboratory of User Interaction and Software Engineering" research group. Within the overall area of Systems and Software Engineering, his main research interests include Requirements Engineering, Model-Driven Engineering, Safety-Critical Systems, and Empirical Software Engineering. He has been work package and task leader in the AMASS project.</td>
    </tr>
    <tr>
    	<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass12.png"></th>
    	<td>Thomas Gruber received his degree in electrical and telecommunications engineering at the Technical University Vienna in 1982. After six years at Siemens Austria, he joined the AIT in 1988. He has over 30 years of experience in the area of railway safety engineering; as Senior Engineer and project manager he was and is responsible for and contributing to several ECSEL research projects (AMASS, AQUAS and IoSense) and a customer project regarding project planning tools for a railway interlocking system. His main research areas are safety and hazard analysis methods, safety critical / fault tolerant system architectures, functional safety and cybersecurity standards, safety certification across industry domains, and inter-dependence of safety and security in critical systems. He is author and co-author of numerous publications and is holding lectures on test engineering and test management in the course "safety and systems engineering" at the University of Applied Sciences Campus Vienna.</td>
    </tr>
     <tr>
  		<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass13.png"></th>
		<td>Stefano Puri is Senior Consultant in the Innovative &amp; Technological Services Unit at Intecs (Italy). He is involved in several international industrial and research projects, with a role of technical leadership for the definition and usage of model driven methodologies and the development of supporting tools for the design of high-integrity software systems. He has many years of experience on the development of model based tools based on the Eclipse platform and he has deep experience in the OMG standards both as an end user and as technology developer. He received his Computer Science degree from the Pisa University, Italy.</td>
    </tr>
    <tr>
    	<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass14.png"></th>
    	<td>Isaac Moreno Asenjo, is bachelor of Science in Physics in the Universidad Complutense de Madrid since 1996.Currently, he is working as a system engineer in the SW &amp; Ground department in Thales Alenia Space, Spain. He has experience in specification, integration and management of satellite regenerative systems, and ground control systems, gained through multiple European institutional programs and commercial projects involving multi-spot satellite telecom systems. Previously, he worked in terrestrial BB multiservice Access Nodes and fixed switched telecommunication systems. He also actively collaborated in the specification of the second generation DVB-RCS standard.</td>
    </tr>
    <tr>
    	<th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/amass15.png"></th>
		<td>Alejandra Ruiz holds a Ph.D. degree in Telecommunications and Computer Engineering, (2015, U. of Deusto), an MSc in Advanced Artificial Intelligence (2012, UNED) and the degree in Telecommunication Engineering (2005, University of Deusto). She joined TECNALIA in 2007 and is a Senior Research Engineer in the Trustech Area. She currently leads the area of Modular Assurance and Certification of Safety-critical Systems, with particular focus on automotive, aerospace, railway and medical device industries. She has been the leading the AMASS project (Architecture-driven, Multi-concern and Seamless Assurance and Certification of Cyber-Physical Systems) and is the main contributor in these areas for European projects such as RECOMP (Reduced Certification Costs for Trusted Multicore Platforms), OPENCOSS (Open Platform for EvolutioNary Certification of Safety-critical Systems) SafeAdapt (Safe Adaptive Software for Fully Electric Vehicles) and EMC2 (Embedded Multi-Core systems for Mixed Criticality applications in dynamic and changeable real-time environments). </td>
    </tr>
  </tbody>
</table>
</div>

<div class="bottomitem" style="background-color:#fff">
 <table class="table">
   <tbody>
    <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/plane1.png"><br><br><img width="100px" src="/community/eclipse_newsletter/2019/july/images/flag.png"></th>
      <td>
      	<ul>
      		<li>Eclipse Project: <a href="polarsys.org/opencert">polarsys.org/opencert</a></li>
      		<li>Mailing-List: <a href="mailto:opencert-dev@eclipse.org">opencert-dev@eclipse.org</a></li>
      		<li>Website:<a href="amass-ecsel.eu">amass-ecsel.eu</a></li>
      		<li>Twitter:<a href="twitter.com/AMASSproject">@AMASSproject</a></li>
      	</ul>
      </td>
      <td><img width="100px" src="/community/eclipse_newsletter/2019/july/images/opencert.png"></td>
    </tr>
    <tr>
   	<td colspan="3">This project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No 692474</td>
   </tr>
  </tbody>
</table>
</div>