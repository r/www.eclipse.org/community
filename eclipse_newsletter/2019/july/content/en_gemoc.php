<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>GEMOC is primarily an open and international research initiative on language engineering, including the development of domain-specific languages (DSLs) and their coordination for the construction of complex software-intensive systems. It brings together researchers and practitioners from both industry and academia, and aims to develop new tools and methods integrated within the so-called Eclipse GEMOC Studio. At the Eclipse Foundation, GEMOC takes many forms that we introduce in this article,  along with the main objectives, the organization, and the current and future challenges.</p>	
<h3>1. What Is GEMOC?</h3>
	<p>GEMOC is an open and international <strong><em>Initiative&sup1;</em></strong> created in 2013, which aims to coordinate and disseminate the research results regarding the support of the development and coordinated use of various modeling languages that lead to the concept of the globalization of modeling languages&sup2;. The various GEMOC partners develop and experiment techniques, frameworks, and environments to facilitate the creation, integration, and automated processing of heterogeneous modeling languages. All the results are seamlessly integrated into the GEMOC <strong><em>Studio&sup3;</em></strong>, an Eclipse RCP on top of the Eclipse Modeling package.</p>
	<p>The community of the GEMOC Initiative has reached a certain level of maturity that required an organization to sustain the GEMOC Studio as a research platform, but also continue to support collaboration between academia and industry and foster industrial transfer and innovation. For such a purpose, in 2018, we created the GEMOC <strong><em>Research Consortium&#8308;</em></strong>, which supports the corresponding international initiative to develop, coordinate, and disseminate Research &amp; Transfer efforts on the use and globalization of modeling languages. In particular, the GEMOC RC:</p>
		<ul>
			<li>hosts the GEMOC Studio and ensures the constant availability of the research platform through adequate integration testing and continuous integration,</li>
			<li>coordinates and operates the co-funding of the GEMOC Studio for sustainability,</li>
			<li>plans and coordinates evolutionary initiatives among the stakeholders through the use of mailing lists, forums, and bug trackers,</li>
			<li>shares the benefits of pilot projects, and fosters exchanges between academia and industry, as well as collaboration on research projects.</li>
		</ul>
<h3>2. How To Use GEMOC?</h3>
	<p>To model systems, it is first necessary to engineer the modeling languages fitting for the modeling tasks at hand. In the case of behavioral modeling, defining the execution semantics of said languages is of great importance to enable the simulation and debugging of conforming models early in the development process. And when multiple heterogeneous modeling languages are jointly used to define the different aspects of a system, it is essential to compose these modeling languages by relating their concepts and their semantics, in order to enable the joint simulation and debugging of coordinated heterogeneous behavioral models conforming to these modeling languages.</p>
	<p>To address all these concerns, the GEMOC Studio provides generic components through Eclipse technologies for the development, integration, and use of heterogeneous executable modeling languages. This includes, among others:</p>
		<ul>
			<li>metaprogramming approaches and associated execution engines to design and run the execution semantics of executable modeling languages,</li>
			<li>efficient and domain-specific execution trace management services, to enable the dynamic verification and validation of behavioral models,</li>
			<li>model animation services, to graphically visualize an ongoing execution,</li>
			<li>advanced debugging facilities such as forward and backward debugging (i.e. omniscient debugging), timeline, etc.,</li>
			<li>coordination facilities to support concurrent and coordinated execution of heterogeneous models,</li>
			<li>an extensible framework for easily adding new execution engines and runtime services.</li>
		</ul>
	<p>The GEMOC Studio is also provided as an Eclipse product, which eases the diffusion of the various technologies aforementioned through an integrated studio that also includes documentation, examples, and tutorials. The GEMOC Studio offers two workbenches (accessible with specific perspectives):</p>
		<ul>
			<li>A Language Workbench to be used by language designers to build and compose new executable modeling languages, which can be automatically deployed in the Modeling Workbench</li>
			<li>A Modeling Workbench to be used by domain designers to create, execute, and coordinate models conforming to executable modeling languages.</li>
		</ul>
<h3>3. GEMOC: Who's It For?</h3>
	<p>The GEMOC Initiative brings together researchers and practitioners, both from academia and industry, who want to collaborate on the topic of language engineering and the globalization of modeling languages. By joining the GEMOC initiative, they have the opportunity to share, discuss, exchange ideas and collaborate with other members, taking advantage of the research framework proposed by the research consortium, as well as infrastructure and collaboration tools. These tools include mailing lists, model repositories, an integrated studio as a research platform, a continuous integration, etc.</p>
	<p>In this context, researchers may conduct research and experiments in language engineering leading to prototypes integrated into the GEMOC Studio. Practitioners may develop demonstrators and conduct pilot projects using the GEMOC Studio. Fig. 2 shows how various professionals can use  GEMOC Studio to their benefit. A <em>GEMOC core developer</em> maintains the studio framework that can be used by a <em>contributor</em> to extend the studio with new metaprogramming approaches, or generative approaches automating the development of DSL tooling. A <em>language engineer</em> can then build languages using these metaprogramming and generative approaches, and a <em>modeler</em> can use languages to create, debug, execute, and validate models. Lastly, a toolmaker can provide tools that can interact with the execution of models through the framework API, which can then be used by modelers (eg. for early dynamic validation).</p>
	<p align="center"><img class="img-responsive" width="80%" src="/community/eclipse_newsletter/2019/july/images/levels.png"></p>
	<p align="center"><em>Fig. 1: Roles in the development and use of the GEMOC Studio</em></p>
	<p>The GEMOC Studio is also of interest for educational purposes. It is already used in several universities to teach students about the state of the art language engineering principles and cutting-edge technologies.</p>
<h3>4. GEMOC at Eclipse</h3>
	<h4>The GEMOC Research Consortium</h4>
		<p>Collaboration around open source, when it comes to research, generally takes place in two phases: prototyping then industrialization. The second phase is pretty well known. It is led by the <strong>SME</strong> <em>(Small &amp; Medium Enterprise)</em> in collaboration with the <strong>industrial company</strong>. During this phase, the SME <strong>industrializes</strong> a product or delivers some services (Fig.1#1) which answer to the <strong>requirements</strong> injected by an industrial.</p>
		<p>The first phase happens when the answer to the industrial needs cannot be fulfilled directly by an SME. In such a case, a prototyping phase is required and the <strong>researchers</strong> are involved. During this phase, the researchers demonstrate the <strong>innovation</strong> (Fig.1#2) and validate the prototype by comparing the results to the <strong>requirements</strong>. If the industrial company is satisfied with the resulting prototype, this phase is completed by a <strong>technology transfer</strong> (Fig. 1#3): the researcher assists and supports SMEs during the industrialization phase.</p>
		<p>In this context, the role of the Open Source Software (OSS) takes on its full meaning: each of these actors (industry, researchers, SMEs) can collaborate on the basis of a shared OSS, which protects them against most of the intellectual property issues.</p>
		<p align="center"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/july/images/triangle.png"></p>
		<p align="center"><em>Fig. 2: Open Source: the catalyst among researchers, SMEs, and industries</em></p>
		<p>At Eclipse, existing Industrial Consortia (IC) are well suited to cover the industrialization phase but are not well suited to cover the prototyping phase: Eclipse Industrial Consortia provide a vendor-neutral governance structure that allows organizations to freely collaborate to develop new industry platforms&#8309;. However, during the prototyping phase, the actors do not build a product or a platform, they evaluate and validate some options based on a research platform.</p>
		<p>For this reason, the Eclipse Foundation has defined the concept of <strong>Research Consortium (RC)</strong>. They are dedicated to  supporting and promoting  a research platform, excluding the industrialization of this platform&#8310;. It was important for the Foundation to support this type of collaboration without adding industrial constraints, which are not well suited to a research consortium (e.g., RC has by default an IP due diligence of Type A).</p>
		<p>The GEMOC Research Consortium is the first Research Consortium (RC) at Eclipse, available under the umbrella of the working groups. While it is free for any partner to join an RC, projects hosted in this context must have a TRL level below or equal to 5. This means that the GEMOC RC hosts advanced research prototypes that should be used only for inconsequential examples, e.g, academic use cases, demonstrators, and industrial pilot projects. The GEMOC RC explicitly excludes the objective of increasing the maturity level of these projects to make them ready for operational deployment (TRL&gt;&#0061;6). If members of the RC need to industrialize a technology hosted in the GEMOC RC, they are invited to fork the project and host it at their convenience, including through a dedicated Eclipse project, or part of an existing Industry Consortium.</p>
	<h4>The GEMOC Studio Development Infrastructure</h4>
		<p>The GEMOC Studio is open and aims to integrate various Eclipse and third-party technologies, with potentially different levels of maturity, and coming with different scenarios. The GEMOC Eclipse project is split into several GitHub repositories and organized to leverage automation in a continuous integration process while trying to limit individual workload. It uses a multibranch pipeline on Jenkins that allows compiling all repositories at once for a given branch. The project capitalizes on system tests (using <em>swtBot</em>) that simulate typical user scenarios. This gives acceptable confidence, covering the toolchain from the end user point of view. The architecture of the build process is designed in such a way that it can be easily cloned by external groups on their own infrastructure so they can get similar confidence in their developments before considering submitting into the GEMOC Studio. This process allows continuous delivery of the GEMOC Studio. Any GEMOC Studio contributor or user may ask for an intermediate release by snapshotting the current status (for example, before submitting a conference paper) or wait for the annual release. Release notes are handled semi-automatically through the systematic use of GitHub pull requests that populate the changelog. The GEMOC Studio also provides a discovery service (based on <em>amalgam</em>) that highlights third-party components or examples that cannot be directly hosted in GEMOC Studio.</p>
<h3>5. GEMOC: The Road Ahead</h3>
	<p>Our immediate challenge is to properly implement the newly created Research Consortium, and to sustain both the initiative and the Studio. Although it inherits from the already existing community and organization of the initiative, we need to develop it in accordance to the scientific breakthroughs currently explored by the various members, and according to the available financial resources. Hence, we solicit future projects relying on the GEMOC Studio to contribute to the Research Consortium.</p>
	<p>Various scientific challenges are currently explored through different projects&#8311;. Ongoing work includes co-simulation, model debugging and testing, live modeling and modeling in the web. Discussions about the next generations of the modeling frameworks are also underway and aim to identify the new opportunities and challenges with regard to growing scenarios such as data-driven applications and smart systems.</p>
	<p>The community defines the long term perspectives. To collaboratively define the roadmap, various scientific and technological events are regularly organized in conjunction with both EclipseCon and scientific conferences (e.g. MODELS and SPLASH&#8312;). They bring together the partners of the GEMOC community to briefly recap the recent achievements, to discuss the current challenges and the coming roadmap, to address challenging tasks on the GEMOC Studio, and to welcome the newcomers.</p>
<h4>References</h4>
	<ol>
		<li><a href="http://gemoc.org" target="_blank">http://gemoc.org</a></li>
		<li>Benoit Combemale, Julien Deantoni, Benoit Baudry, Robert B. France, Jean-Marc J&#233;z&#233;quel, et al.. Globalizing Modeling Languages. Computer, Institute of Electrical and Electronics Engineers, 2014, pp.10-13. <a href="https://hal.inria.fr/hal-00994551" target="_blank">https://hal.inria.fr/hal-00994551</a></li>
		<li><a href="https://projects.eclipse.org/projects/modeling.gemoc" target="_blank">https://projects.eclipse.org/projects/modeling.gemoc</a></li>
		<li><a href="https://www.eclipse.org/org/workinggroups/gemoc_rc_charter.php" target="_blank">https://www.eclipse.org/org/workinggroups/gemoc_rc_charter.php</a></li>
		<li><a href="https://www.eclipse.org/org/workinggroups/about.php#wg-neutral" target="_blank">https://www.eclipse.org/org/workinggroups/about.php#wg-neutral</a></li>
		<li><a href="https://www.eclipse.org/org/workinggroups/gemoc_rc_charter.php" target="_blank">https://www.eclipse.org/org/workinggroups/gemoc_rc_charter.php</a></li>
		<li><a href="http://gemoc.org/projects" target="_blank">http://gemoc.org/projects</a></li>
		<li><a href="http://gemoc.org/events" target="_blank">http://gemoc.org/events</a></li>
	</ol>
	
<div class="bottomitem">
 <h3>Authors of Article</h3>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Author</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc1.png"></th>
		<td>
		<p><a href="http://combemale.fr">Benoit Combemale</a> is Full Professor of Software Engineering at the University of Toulouse, and a Research Scientist at Inria. His research interests are in the field of software engineering, including Model-Driven Engineering, Software Language Engineering and Validation &amp; Verification; mostly in the context of (smart) Cyber-Physical Systems and Internet of Things. He is also teaching worldwide in various engineering schools and universities. </p>
		<p>Prof. Combemale received his Habilitation in Computer Science from the University of Rennes 1 in 2015, and his Ph.D. in Computer Science from the University of Toulouse in 2008. Before joining the University of Toulouse, he was an Associate Professor at the University of Rennes 1, and has been visiting professor at McGill University and Colorado State University.</p><p> Prof. Combemale co-authored 3 books, and 100+ journal and conference publications in the fields of software engineering. He also edited 2 books and various special issues in scientific journals. He is chairing the Steering Committee of the conference series SLE, a deputy editor-in-chief of the journal JOT, and a member of the editorial boards of the journals SoSyM, COLA, and SCP. He has been the program chair of SLE 2014 and ECMFA 2019, and general chair of MODELS 2016 and SLE 2017. He also serves as program committee member for various conferences and workshops in software engineering. Prof. Combemale coordinated and participated in many collaborative projects, and bilateral collaborations with industries. He is also a founding member of the GEMOC initiative, and currently lead the steering committee of the Eclipse Research Consortium GEMOC.</p></td>
    </tr>
     <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc2.png"></th>
	<td>Julien Deantoni is an associate professor in computer sciences at the University Cote d'Azur. After studies in electronics and micro informatics, he obtained a PhD focused on the modeling and analysis of control systems, and had a post doc position at INRIA in France. He is currently a member of the I3S/Inria Kairos team. His research focuses on the join use of Model Driven Engineering and Formal Methods for System Engineering. More information at <a href="http://www.i3s.unice.fr/~deantoni/">http://www.i3s.unice.fr/~deantoni/</a></td>
    </tr>
   <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc3.png"></th>
		<td>Erwan Bousse is an associate professor at the University of Nantes since 2018, specialized in software engineering. He has obtained his PhD in France in 2015 at the University of Rennes 1 for his work on execution traces and omniscient debugging of executable models. He is a member of the LS2N laboratory, and his current research interests include software language engineering, domain-specific languages, model execution, and model debugging. He is one of the core developers of the GEMOC Studio.</td>
   </tr>
     <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc4.png"></th>
		<td>
		<p>Didier Vojtisek works for the French National Institute for Research in Computer Science and Control (Inria) since november 2001 as a research engineer in the Development and Experimentation Department (SED). His main role is to develop software and promote of good software engineering practices in Inria research teams. Didier was first assigned in the Triskell team and in collaboration with Vertecs team. Since 2013, the Triskell team is now named DiverSE.</p>
		<p>Didier is specialized in research and development in model-driven engineering concepts, tools and applications. He is the main architect of Kermeta, an open source tool for building Model Driven solutions. He  actively contributes to the newest software of the team relative to modeling and language engineering. Didier co-authored the "Engineering Modeling Languages - Turning Domain  Knowledge into Tools" book (CRC Press 2016).</p>
		</td>
     </tr>
     <tr>
      <th scope="row"><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc5.png"></th>
		<td>Philippe Krief is the Research Relations Director of the Eclipse Foundation Europe. He received his Ph.D. in Computer Science from University Paris VIII, France in 1990. Before joining the Eclipse Foundation staff, he was Senior Architect on Embedded System developments, Eclipse committer, Agile development evangelist on the Rational Collaborative platform, and R&amp;D Development Manager for IBM. He has a passion for Eclipse since its beginning, early 2000 when he was involved in the early versions based on VisualAge Micro Edition, which supported the development of embedded Java applications and cross-system development. Philippe is the dissemination and community development manager of the AGILE project (<a href="www.agile-iot.eu">www.agile-iot.eu</a>), co-founded by the Horizon 2020 programme of the European Union.</td>
    </tr>
  </tbody>
</table>
</div>

<div class="bottomitem" style="background-color:#fff">
 <table class="table">
   <tbody>
    <tr>
      <th scope="row"><img width="150px" src="/community/eclipse_newsletter/2019/july/images/gemoc6.png"></th>
      <td>
      	<ul>
      		<li>Eclipse Project: <a href="https://eclipse.org/gemoc">eclipse.org/gemoc</a></li>
      		<li>Mailing List: <a href="mailto:gemoc-dev@eclipse.org">gemoc-dev@eclipse.org</a></li>
      		<li>Website: <a href="https://gemoc.org">gemoc.org</a></li>
      		<li>Twitter: <a href="https://twitter.com/gemocinitiative">@gemocinitiative</a></li>
      	</ul>
      </td>
      <td><img width="100px" src="/community/eclipse_newsletter/2019/july/images/gemoc7.png"></td>
    </tr>
   <tr>
   	<td colspan="3">This project has received funding from the European Union's Horizon 2020 research and innovation programme under grant agreement No 787034</td>
   </tr>
  </tbody>
</table>
</div>
