<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

$pageTitle = "Research@Eclipse";
$pageKeywords = "eclipse, newsletter, eclipse research, basyx, crossminer, gemoc, panorama, scava, amass, agile iot, brainiot";
$pageAuthor = "Christopher Guindon";
$pageDescription = "We are very proud to offer you an introduction to most of the research projects we're involved in. You will find many areas of interest such as Internet of Things with AGILE, BRAIN-IoT and BaSys projects, Development Tools with PDP4E and CROSSMINER, Modeling with AMASS and GEMOC, and Automotive and Aerospace with PANORAMA.";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "";
  $sponsorLink = "";
  $sponsorImage = "";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;