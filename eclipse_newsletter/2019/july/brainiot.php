<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "BRAIN-IoT: Model-based Framework for the Development, Deployment and Management of Distributed IoT Applications";
  $pageKeywords = "eclipse, newsletter, iot, eclipse iot,  Brain-IoT, iot applications, eclipse research";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = " BRAIN-IoT focuses on the realization of a framework and methodologies for fast and safe implementation of intelligent IoT applications leveraging on Component-Based Development and Event-Driven dynamic deployment techniques, similar to Function-as-a-Service (FaaS) paradigm.";

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");