<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>In January 2019, GlassFish 5.1 was released as the first project anticipating Jakarta EE 8, although still based on Java EE 8. In the Jakarta EE Specification Committee, where I represent the community via the committer seat, we plan the next steps towards Jakarta EE 8 and beyond.</p>
	<p>Both the project names are about to change for most <a href="https://waynebeaton.wordpress.com/2019/04/04/renaming-java-ee-specifications-for-jakarta-ee/" target="_blank">specifications</a> and likely with Jakarta EE 9, a significant portion is going to see refactoring and name change not just on the outside, but also in the specifications and APIs themselves. The <a href="https://projects.eclipse.org/projects/ee4j.jakartaee-platform" target="_blank">Jakarta EE Platform project</a> is going to work on those changes advised by the Specification Committee and other bodies like the EE4J PMC. </p>
	<p>All assets developed by Oracle have been transferred to the Eclipse Foundation, and while others, especially Context and Dependency Injection (CDI) or Bean Validation, which are being developed and maintained by Red Hat under the JBoss ecosystem, they are also likely to become part of the Jakarta EE codebase at some point rather than just changing the name of the specifications or package names. The Java Batch Standard, Spec Lead IBM has already proposed a <a href="https://projects.eclipse.org/proposals/jakarta-batch" target="_blank">Jakarta Batch project</a>. So, what's planned for Batch could also follow for CDI or Bean Validation and maybe others, if they considered joining the Jakarta EE platform someday, for example, JCache. Other downstream specifications like the Java Portlet Spec that build on Java EE will have to face a change of dependencies and the question how to evolve if building on top of Jakarta EE means it also has to change and likely move away from the current standardization at the Java Community Process.</p>
	<p>While it changed a bit since GlassFish 5.1 was released, right before its publication the commit statistics for the GlassFish project showed that not only large vendors like obviously, Oracle had contributed to GlassFish but nearly 50% of commits then came from "contributors", meaning they are not affiliated with one of the major vendors but individual committers like myself or others.</p>
	<p align="left"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/may/images/graph1.PNG"></p>
	<p><em>GlassFish Commits by Company when Glassfish 5.1 was published (January 2019)</em></p>
	<p>Some specifications led by individuals and smaller companies like the MVC Spec and its implementation Eclipse Krazo show that individual committers and small vendors make up the lion share in this case:</p>
	<p align="left"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/may/images/graph2.PNG"></p>
	<p><em>Commits on the Krazo project by individuals over the last three months (till April 2019)</em></p>
	<p>Looking at the affiliation of the Krazo contributors confirms this picture.</p>
	<p align="left"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/may/images/graph3.PNG"></p>
	<p><em>Commits on the Krazo project by supporting organization over the last three months (till April 2019)</em></p>
	<p>So, while bodies like the Jakarta EE Working Groups only have one or the other individual representing committers, many projects are driven by individual committers or users of the technologies rather than vendors only.</p>
	<p>For the new Jakarta EE specification, all TCKs and Compatibility Test Suites should not only provide transparent results but also be open source themselves. This, along with a number of other aspects, is a sticking point for Jakarta EE due to the cooperation of legal departments at Oracle, the Eclipse Foundation, and other corporations like Red Hat, IBM, Pivotal and others. </p>
	<p>The TCK project shows that despite rumors and prejudice that Jakarta EE would be dead and Spring or Spring Boot killed it, the very contributor to the Spring Framework or Spring Boot, Pivotal also contributed nearly 30% to the Jakarta EE TCK. </p>
	<p align="left"><img class="img-responsive" width="60%" src="/community/eclipse_newsletter/2019/may/images/graph4.PNG"></p>
	<p><em>Commits on the Jakarta TCK project by supporting organization over the last three months (till April 2019)</em></p>
	<p>So while there is obviously a lot done by large vendors like Oracle, the power of the community shows up in more and more projects whether it is competitors or current users of Java EE who want to improve Jakarta EE. For example, 12 projects were counted under the Jakarta EE 8 umbrella with only Oracle leading compared to 17 which have others leading or co-leading. Compared to Java EE, even up to eight (which had only three specs without complete Oracle leadership) means a much stronger involvement from the community at large.</p>

<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/werner.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Werner Keil<br>
            <a target="_blank" href="http://www.catmedia.us/">Creative Arts and Technologies</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/wernerkeil">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>