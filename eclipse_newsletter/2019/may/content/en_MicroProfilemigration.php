<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The goal of MicroProfile.IO is to optimise Java EE for a micro-service architecture. It is based on some of the Java EE specifications and standardise a few technologies from the micro-services space.</p>
	<p>However, some of the microprofile implementations are completely different 'servers', like the KumuluzEE server. So how can you migrate easily from your favorite Java EE server to a MicroProfile implementation?</p>
	<p>This session shows you an overview of what MicroProfile is and how it relates to Java EE. It then demonstrates with a few examples how you can adjust your Java EE application to incorporate some of the MicroProfile.IO specifications and how you can transform your Java EE application to a MicroProfile one using Thorntail, Liberty, Payara Micro and KumuluzEE.</p>
	<p><a class="eclipsefdn-video" href="https://www.youtube.com/watch?v=kaBs8_gpc9Y"></a></p>
    
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/may/images/rudy.webp"
        alt="Rudy De Busscher" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Rudy De Busscher<br />
            <a target="_blank" href="https://www.payara.fish">Payara</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/rdebusscher">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>