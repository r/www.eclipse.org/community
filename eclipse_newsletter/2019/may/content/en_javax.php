<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>This week Mike Milinkovich <a href="https://eclipse-foundation.blog/2019/05/03/jakarta-ee-java-trademarks/" target="_blank">announced</a> that Oracle and the Eclipse Foundation agreed that the javax package cannot be evolved by Jakarta EE community and Java trademarks cannot be used in Jakarta EE specifications. How critical is it to Jakarta EE community? Can Jakarta EE survive without javax namespace? Here are my thoughts about it.</p>
	<p>For sure, it would be much better to evolve existing APIs in javax packages without any restrictions. But we have what we have and I am not going to do complaining and blaming here. There were reasons to do it this way. On the other hand, finally, the uncertainties with Java trademarks are gone. It's very important because Jakarta EE progress is not blocked anymore. Javax package restrictions draw a clear line separating Java EE and Jakarta EE, separating past and future. Javax becomes a legacy. All innovations will belong to jakarta namespace. Now it's clear that javax packages cannot be modified and community needs to find a way how to evolve specifications. And there are some options how it can be done. We need to maximize compatibility with Jakarta EE 8 for future versions without stifling innovation. It's what Jakarta EE Spec Committee agreed on.</p>
	<p>In my opinion the best would be to make a big-bang javax->jakarta package rename to untie the community hands and allow modifications and APIs evolution. It will break the backwards compatibility which was always an important part of Java EE platform and we should not forget about it. There are thousands of Java EE applications and it won't be wise to remove backwards compatibility requirements. A good option is to create a special backwards compatibility profile in Jakarta EE platform. This profile should contain a frozen Java EE 8 APIs and will allow to run Java EE 8 applications on future versions of Jakara EE Platform. This profile can be optional to allow new potencial Jakarta EE vendors concentrate only on innovations, but I am sure that all big players such Oracle and IBM will support it anyway.</p>
	<p>This kind of solution is not new for the industry. For example, in 2006 Sony released PlayStation 3 system which used new architecture and was not compatible with the previous PlayStation 2 system on the hardware level. They solved it by adding PS2 chip to the first versions of PS3 consoles allowed to run PS2 games on it.</p>
	<p>Another sample is a process of changing CPU in Apple Macintosh computers from PowerPC to Intel. They provided a simulator allowing running PowerPC application on new Intel based computers for some limited time until most of the applications migrated to the new platform.</p>
	<p>How the backwards compatibility can be implemented technically? One of the obvious and straightforward solutions is supplying two implementations: one is supporting javax and and another supporting jakarta. Javax implementations are already exist as part of Java EE 8. Jakarta implementations can be created by forking javax implementations and make necessary modifications.</p>
	<p>Another way is patching application binaries at runtime or build time. Runtime solution can be accomplished using JavaAgent and build time via tooling and build plugins.</p>
	<p>To simplify migration to jakarta package on the source level, the community may think of creating IDE plugins replacing javax with jakarta the clever way with one click.</p>
	<p>I am working in different Jakarta EE committees and see the intention of participants to remain committed to Jakarta EE work, finish Jakarta EE 8 and push it forward to provide a robust, compatible and innovative enterprise platform.</p>
			
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\february\images\Glassfish_img_1.PNG"
        alt="Dmitry Kornilov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Dmitry Kornilov<br />
            <a target="_blank" href="https://www.oracle.com/index.html">Oracle</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/m0mus">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://dmitrykornilov.net/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>