<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The agreement between the Eclipse Foundation and Oracle regarding rights to Java trademarks has been signed! This is truly an important milestone for Jakarta EE since we will now be able to move forward with it.</p>
	<p>As outlined in Mike Milinkovich's <a href="https://blogs.eclipse.org/post/mike-milinkovich/update-jakarta-ee-rights-java-trademarks" target="_blank">recent blog</a> on Jakarta EE rights to Java trademarks, there are two major areas of impact on the Jakarta EE projects:</p>
	<ul>
		<li>Java Trademarks</li>
		<li>The javax.* Namespace</li>
	</ul>

<h3>Java Trademarks</h3>
	<p>One part of the agreement focuses on the use of Java trademarks. The implications for Jakarta EE is that we have to rename the specifications and the specification projects.</p>
	<p>This work is ongoing and is tracked in our specification renaming board on <a href="https://github.com/orgs/eclipse-ee4j/projects/11" target="_blank">GitHub</a></p>
	<p>The EE4J PMC has published the following Naming Standard for <a href="https://www.eclipse.org/ee4j/news/?date=2019-04-23">Jakarta EE Specifications</a> in order to comply with the trademark agreement.</p>
	
<h3>The javax Namespace</h3>
	<p>The main topic of the agreement is around the use of the javax namespace. The agreement permits Jakarta EE specifications to use the javax namespace as is only.Changes to the API must be made in another namespace.</p>
	<p>While the name changes can be considered cosmetic modifications, the restrictions on the use of the javax.* namespace come with some technical challenges.</p>
	<p>For example, how are we going to preserve backwards compatibility for applications written using the javax.* namespace?</p>
	<p>The Jakarta EE Specifications Committee has come up with the following guiding principle for Jakarta EE.next: <em>"Maximize compatibility with Jakarta EE 8 for future versions without stifling innovation."</em></p>
	<p>With the restrictions on the use of the javax.* namespace, it is necessary to transition the Jakarta EE specifications to a new namespace.</p>
	<p>The Jakarta EE Specification Committee has decided that the new namespace will be jakarta.*.</p>
	<p>How and when this transition should happen is now the primary decision for the Jakarta EE community to make. There are several possible ways forward and the forum for these discussions is the <a href="https://accounts.eclipse.org/mailing-list/jakartaee-platform-dev" target="_blank">Jakarta EE Platform mailing list</a>.</p>
	<p>Please make sure that you subscribe to the mailing list and join in on the discussion. We hope that we will be able to reach some form of consensus within a month that can be presented to the Specification Committee for approval.</p>

<h3>An Opportunity</h3>
	<p>While the restrictions on the use of Java trademarks and the javax.* namespace impose both practical as well as technical challenges, it is also an opportunity to perform some housekeeping.</p>
	<p>By renaming the specifications, we can get a uniform, homogeneous naming structure for the specifications that makes more sense and is easier on the tongue than the existing one. By having clear and concise names, we may even get rid of the need for abbreviations and acronyms.</p>
	<p>The shift from javax.* to jakarta.* opens up the possibility to differentiate the stable (or legacy) specifications that have played their role from the ones currently being developed.</p>
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2018/november/images/ivar.png"
        alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         	 Ivar Grimstad<br>
            <a target="_blank" href="https://www.cybercom.com/">Principal Consultant at Cybercom Group</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/ivar_grimstad">Twitter</a><br>
           <?php // echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>