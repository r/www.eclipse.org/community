<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>This may come as a bit of a surprise, but as of May 2019, there are no Jakarta EE specification projects. Clearly, we do have a great many projects engaged in work related to the Jakarta EE specifications-APIs and TCKs specifically-but none of these projects are specification projects as defined by the Eclipse Foundation Specification Process (EFSP), and so none of them are configured to work on actual specifications.</p>
	<p>When we first started the effort to build Jakarta EE, we didn't have a specification process. In the absence of a specification process, we did what we do well: we created Eclipse open source projects as places to work on the artifacts related to the specifications. Up until this point, however, all we've been working with are the technical artifacts associated with specifications and not the specifications themselves. Or put another way, we don't currently have any specification documents, a necessary ingredient in the execution of a specification process.</p>
	<p>The EFSP defines specification as a "... collection of Application Programming Interface (API) definitions, descriptions of semantic behavior, data formats, protocols, and/or other referenced specifications, along with its TCK, intended to enable the development and testing of independent Compatible Implementations." We have the APIs and TCKs, but do not currently have documents that describe the "semantic behavior, data formats, protocols," etc.; it's time to start working on these specification documents. To do this, we're going to turn the existing projects doing specification-related work into specification projects.</p>
	<p>In the months since we started this effort, we've created the EFSP and have moved past the other roadblocks set in the way of making progress, and it's now time to start the work of creating the specifications that will become Jakarta EE 8. To do this, we need to change the nature of the existing projects. Per the Eclipse Development Process (EDP), we do this by engaging in a restructuring review. While we're at it, we're going to use this opportunity to change the placeholder names and placeholder scope statements that we established when we first created this projects.</p>
	
<h3>Specification and Specification Project Names</h3>	
	<p>Why do we need to change the specification project names? In short, we need to change the names of the specification projects, because the names that we have are terrible. When we started this effort, we knew that we might have to do something with the project names, so we applied a very simple pattern. Today, we have twenty four "Eclipse Project for..." projects (e.g. "Eclipse Project for Servlet"), along with a small handful for projects with slightly better names, like "Eclipse Project for Stable Jakarta EE APIs" and "Eclipse Jakarta EE Platform" that can also benefit from some adjustment.</p>
	<p>Before we address project names, however, we need to first focus our attention on the names of the specifications themselves. Why do we need to change the specification names? To start, it's just good form to leverage the Jakarta brand. But, more critically, many of the existing specification names use trademarked terms that make it either very challenging or impossible to use those names without violating trademark rules. </p>
	<p>With this in mind, we've come up with a naming pattern that we believe can serve as a good starting point for discussion. The naming rules are relatively simple:</p>
	<ul>
		<li>Replace "Java" with "Jakarta" (e.g. "Java Servlet" becomes "Jakarta Servlet");</li>
		<li>Add a space in cases where names are mashed together (e.g. "JavaMail" becomes "Jakarta Mail");</li>
		<li>Add "Jakarta" when it is missing (e.g. "Expression Language" becomes "Jakarta Expression Language"); and</li>
		<li>Rework names to consistently start with "Jakarta" ("Enterprise JavaBeans" becomes "Jakarta Enterprise Beans" and  "Java Message Service" becomes "Jakarta Message Service").</li>
	</ul>
	<p>This presents us with an opportunity to add even more consistency to the various specification names. Some, for example, are more wordy or descriptive than others; some include the term "API" in the name, and others don't; etc.</p>
	<p>We'll have to sort out what we're going to do with the Eclipse Project for Stable Jakarta EE Specifications, which provides a home for a small handful of specifications which are not expected to change. We'll also have to sort out what we're going to do about projects like Eclipse Mojarra, Eclipse Metro, and Eclipse Project for JavaMail which combine APIs and implementations for some specifications; we may end up having to create new specification projects as homes for development of the corresponding specification documents (regardless of how this ends up manifesting as a specification project, we're still going to need specification names).</p>
	<p>The EE4J PMC has created a <a href="https://github.com/orgs/eclipse-ee4j/projects/11" target="_blank">project board</a> to track this work.</p>
	
<h3>Specification Project Scope</h3>	
	<p>With the EFSP a single open source specification project has a dedicated project team of committers to create and maintain one or more specifications. The cycle of creation and maintenance extends across multiple versions of the specification, and so while individual members may come and go, the team remains and it is that team that is responsible for every version of that specification that is created.</p>
	<p>The first step in managing how intellectual property rights flow through a specification is to define the range of the work encompassed by the specification. Per the Eclipse IP Policy, this range of work (referred to as the scope) needs to be well-defined and captured. Once defined, the scope is effectively locked down forever (changes to the scope are possible but rare, and must be carefully managed; the scope of a specification can be tweaked and changed, but doing so requires approval from the Jakarta EE Working Group's Specification Committee).</p>
	<p>As a general rule, a scope statement should not be too precise. Rather, it should describe the intention of the specification in broad terms. Think of the scope statement as an executive summary or "elevator pitch".</p>
	<p style="margin-left: 40px;"><em>Elevator pitch: You have fifteen seconds before the elevator doors open on your floor; tell me about the problem your specification addresses.</em></p>
	<p>The scope statement must answer the question: what does an implementation of this specification do? The scope statement must be aspirational rather than attempt to capture any particular state at any particular point-in-time. A scope statement must not focus on the work planned for any particular version of the specification, but rather, define the problem space that the specification is intended to address.</p>
	<p>For example:</p>
	<p style="margin-left: 40px;"><em>Jakarta Batch provides describes a means for executing and managing batch processes in Jakarta EE applications.</em></p>
	<p>and</p>
	<p style="margin-left: 40px;"><em>Jakarta Messaging describes a means for Jakarta EE applications to create, send, and receive messages via loosely coupled, reliable asynchronous communication services.</em></p>
	<p>For the scope statement, we can assume that the reader has a rudimentary understanding of the field. It's reasonable, for example, to expect the reader to understand what "batch processes" means.</p>
	<p>Note that the two examples presented above are just examples of form. The respective project teams are currently working with their communities to sort out the final form.</p>
	<p>The scope is "sticky" for the entire lifetime of the specification: it spans versions. The plan for any particular development cycle must describe work that is in scope; and at the checkpoint (progress and release) reviews, the project team must be prepared to demonstrate that the behavior described by the specifications (and tested by the corresponding TCK) cleanly falls within the scope.</p>
	<p>In addition to the specification scope which is required by the Eclipse Intellectual Property (IP) Policy and EFSP, the specification project that owns and maintains the specification needs a project scope. The specification project scope is pretty straightforward: a particular specification project defines and maintains a specification.</p>
	<p>For example:</p>
	<p style="margin-left: 40px;">The Jakarta Batch project defines and maintains the Jakarta Batch specification and related artifacts.</p>
	<p>Like the specification scope, the project scope should be aspirational. In this regard, the specification project is responsible for the particular specification in perpetuity. Further the related artifacts, like APIs and TCKs can be in scope without actually being managed by the project right now.</p>
	<p>Today, for example, most of the TCKs for the Jakarta EE specifications are rolled into the Jakarta EE TCK project. But, over time, this single monster TCK project may be broken up and individual TCKs moved to corresponding specification projects. Or not. The point is that regardless of where the technical artifacts are currently maintained, they may one day be part of the specification project, so they are in scope.</p>
	<p>We might consider, for example, changing the project scope of the Jakarta EE Stable APIs to something simple like:</p>
	<p style="margin-left: 40px;">Jakarta EE Stable APIs provides a home for stable (legacy) Jakarta EE specifications and related artifacts which are no longer actively developed.</p>
	<p>But, all this talk about specification projects aside, our initial focus needs to be on describing the scope of the specifications themselves. With that in mind, the EE4J PMC has created a <a href="https://github.com/orgs/eclipse-ee4j/projects/10" target="_blank">project board</a> with issues to track this work and we're going to ask the project teams to start working with their communities to put these scope statements together.</p>
	
<h3>Restructuring Specification Projects</h3>	
	<p>The EDP defines a collection of reviews that mark key lifecycle milestones for Eclipse Projects. All Eclipse projects start with a creation review and end with a termination review; in between these two bookend reviews, a project will engage in (many) progress and release reviews. Restructuring reviews are a little outside of these normal sorts of lifecycle events. Restructuring reviews are intended as a means of informing the community and key stakeholders of planned significant changes in the project. Since we're making very fundamental changes to the nature of existing projects, a restructuring review is an appropriate means of informing the community and preserving a record of the activity for posterity.</p>
	<p>Starting in late May 2019, the Eclipse Management Organization (EMO) will schedule restructuring reviews for our Jakarta EE specification projects, batching them in small groups as the information becomes available, starting with a smallish batch of three to five projects.</p>
	<p>To declare success on the restructuring review, we need:</p>
	<ul>
		<li>One week of community review;</li>
		<li>Approval from the PMC; and</li>
		<li>Super-majority approval from the Specification Committee.</li>
	</ul>
	<p>All reviews include a week long period of community review which is intended as a final opportunity for adopters, users, and others who operate downstream from the project to have a say in the process. The community review provides an opportunity for key stakeholders in the process (including Eclipse Foundation member companies) review the planned changes, and a last opportunity to protest and fail a review or request changes. EDP reviews rarely fail; stakeholders and others with any sort of interest in the project can, and should, engage early in the process. Projects are expected to operate in an open and transparent manner, so getting involved early in the process to make your concerns known is easy.</p>
	<p>The role of the PMC in the review process is to ensure that the project team is following the rules. Specifically, as part of the PMC's approval process, they will check that the project team is following the EDP and the Eclipse IP Policy. A project is expected to work in a transparent and open manner, so the PMC will check, for example, that the community has been given sufficient opportunity to engage with the project and that their concerns have been addressed. It should be pointed out that it is completely valid for a project team to say "no" to a request; it is, on the other hand, poor form for a project team to completely ignore a request. The PMC will look for this sort of thing. </p>
	<p>Per the EFSP, super-majority approval from the specification committee is required for key lifecycle events of a specification project. Super-majority approval is required to create a new specification project, complete a progress or release review, or change an existing specification project's scope. The EFSP is, however, (effectively) silent on the topic of restructuring reviews (restructuring reviews are a bit of a "catch all" for handling corner cases). In such cases, we fall back on the spirit of the rules. While, strictly speaking, we are not creating new specification projects-but rather are turning existing projects into specification projects-we are still (at least in spirit) creating a means to do real specification work, so we'll follow the process outlined by the EFSP for creating a new specification project. This requires a seven day ballot with a super-majority of positive votes to pass.</p>
	<p>The EMO will declare success on the review when all of these requirements have been met, will then apply the required changes to project metadata (turn the projects into specification projects, change the name, and change the scope), and will engage the respective project teams with the Eclipse Foundation's webmaster team to create Git repositories to manage the specification documents.</p>
	
<h3>Completing the Specification Work</h3>
	<p>Completing these restructuring reviews and setting up the Jakarta EE specification projects to be able to work on specification documents is an important step on the path that we laid out when we started this process. We should be able to get through this part of the process relatively quickly and get onto the real work of producing complete specifications.To create what the EFSP refers to as a final specification-the form of the specification from which Compatible Implementations can be created-each project team must engage in a successful release review. Like the restructuring reviews, these release can be run in small batches as project teams complete the work. We'll talk more about this process in a future installment.</p>
	<p>After we've pushed out final specifications for all of the specifications, including the platform specification, we can declare success on Jakarta EE 8!</p>
	
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\may\images\wayne.jpg"
        alt="Wayne Beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Wayne Beatonh<br />
            <a target="_blank" href="https://www.eclipse.org">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>