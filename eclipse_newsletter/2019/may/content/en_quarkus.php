<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>A primary value of Eclipse MicroProfile is the ability to write portable microservices that can run on multiple MicroProfile implementations. This offers developers a superb selection of runtimes to choose from and decide on one (or more) based on technical or business requirements. In case you haven't heard, there is a new MicroProfile implementation in town called <a href="https://quarkus.io" target="_blank">Quarkus</a>!</p>	
	<p>Quarkus is a MicroProfile implementation that focuses on efficiently running Java applications in containers in general and Kubernetes in particular. These environments are highly dynamic in nature. Microservice containers come and go to respond to changes in traffic and to address continuous deployment demands. Quickly starting microservices improve operational agility and efficient memory utilization matters when running microservices on Kubernetes nodes in order to minimize cost. Quarkus excels at both.</p>
	<p>To put it in perspective, a traditional Java microservice starts in many seconds and utilizes 100's of megabytes of memory. A MicroProfile application compiled to a native binary with Quarkus starts in 10's of milliseconds and consumes only 10's of megabytes of memory. For example, a simple Java JAX-RS "hello world" application can start and respond to the first request in 14 milliseconds while consuming only 13MB of memory. Not just 13MB of Java heap, that's the *entire* application footprint resident in memory (<a href="https://en.wikipedia.org/wiki/Resident_set_size" target="_blank">RSS</a>)! Quarkus opens up MicroProfile application development to a whole new set of use cases that were previously unattainable. When MicroProfile launched in 2016, who thought it could be used in serverless applications? It's Java, after all, right? With Quarkus, it can!</p>
	<p>MicroProfile applications do not have to be compiled to native code to run more efficiently. Quarkus uses ahead-of-time (AOT) compilation to optimize the uber jar so that it starts very quickly and with less RAM than a traditional microservice running on the JVM. The following chart compares a microservice built with Quarkus running on the OpenJDK, running natively, and even compares it to a traditional Java microservice.</p>
	<p align="left"><img class="img-responsive" src="/community/eclipse_newsletter/2019/may/images/graphjohn.png"></p>
	<p>Getting started with Quarkus is easy with the <a href="https://quarkus.io/get-started/" target="_blank">Getting Started Guide</a>, or choose from a large and growing selection of <a href="https://quarkus.io/guides/" target="_blank">hands-on guides</a> to learn how to use the broad set of Quarkus capabilities. </p>
	<p>A <a href="https://microprofile.io/2019/04/22/quarkus-microprofile-cheat-sheet/" target="_blank">handy cheat sheet</a> is available that maps MicroProfile specifications to hands-on guides and maven dependencies. <a href="https://quarkus.io/guides/gradle-tooling" target="_blank">Gradle is supported</a> too. Quarkus supports MicroProfile 2.2, the Reactive Streams Operators specification and even the draft Reactive Messaging specification. Keep in mind that Quarkus is still beta so there may still be some compatibility issues.</p>
	<p>This super-quick 5-minute <a href="https://www.youtube.com/watch?v=DYcEQs-9sb0" target="_blank">Introduction to Quarkus</a> video is worth a watch. It shows application generation, the zero-config Live Reload feature, and culminates in running 100 JAX-RS microservice instances running on Kubernetes (OpenShift) <em>on a laptop</em>!</p>
	<p>In conclusion, MicroProfile offers a lot of implementation options for developers to choose from. For those looking to deploy MicroProfile applications using containers or Kubernetes, then Quarkus is worth a look. Here's a hat tip to the <a href="https://www.graalvm.org" target="_blank"></a>GraalVM team which makes native compilation possible!</p>
	<p>Resources:</p>
	<ul>
		<li><a href="https://quarkus.io" target="_blank">Website</a></li>
		<li><a href="https://quarkusio.zulipchat.com/" target="_blank">Chat room</a></li>
		<li><a href="https://twitter.com/quarkusio" target="_blank">Twitter</a></li>
	</ul>


<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/may/images/john.png"
        alt="Andy McCright" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
         John Clingan <br />
            <a target="_blank" href="https://www.redhat.com/en">Red Hat</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/jclingan">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>