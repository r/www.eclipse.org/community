<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The results of the 2019 Jakarta EE Developer Survey are out. Almost 1,800 Java developers from around the world have spoken. Taken together with the engagement and response to my recent posts on the future of Jakarta EE (see my latest blog <a href="https://eclipse-foundation.blog/2019/05/08/jakarta-ee-8-faq/" target="_blank">here</a>), the survey makes clear the developer community is focused on charting a new course for a cloud native future, beginning with delivering Jakarta EE 8. The Java ecosystem has a strong desire to see Jakarta EE, as the successor to Java EE, continue to evolve to support microservices, containers, and multi-cloud portability.</p>
	<p>Organized by the <a href="https://jakarta.ee/" target="_blank">Jakarta EE Working Group</a>, the survey was conducted over three weeks in March 2019. Just like last year (see the 2018 results <a href="https://jakarta.ee/documents/insights/2018-jakarta-ee-developer-survey.pdf" target="_blank">here</a>), Jakarta EE member companies promoted the survey in partnership with the London Java Community, Java User Groups, and other community stakeholders. Thank you to everyone who took the time to participate. Access the full findings of the survey <a href="https://jakarta.ee/documents/insights/2019-jakarta-ee-developer-survey.pdf" target="_blank">here</a>.</p>
	<p>Some of the highlights from this year's survey include:</p>
	<ul>
		<li>The top three community priorities for Jakarta EE are: better support for microservices, native integration with Kubernetes (tied at 61 percent), followed by production quality reference implementations (37 percent). To move mission-critical Java EE applications and workloads to the cloud, developers will need specifications, tools, and products backed by a diverse vendor community. Jakarta EE Working Group members have committed to deliver multiple compatible implementations of the Jakarta EE 8 Platform when the Jakarta EE 8 specifications are released.</li>
		<li>With a third of developers reporting they are currently building cloud native architectures and another 30 percent planning to within the next year, cloud native is critically important today and will continue to be so;</li>
		<li>The number of Java applications running in the cloud is projected to substantially increase, with 32 percent of respondents expecting that they will be running nearly two-thirds of their Java applications in the cloud within the next two years;</li>
		<li>Microservices dominates as the architecture approach to implementing Java in the cloud, according to 43 percent of respondents;</li>
		<li>Spring/Spring Boot again leads as the framework chosen by most developers for building cloud native applications in Java;</li>
		<li>Eclipse Microprofile's adoption has surged, with usage growing from 13 percent in 2018 to 28 percent today;</li>
		<li>Java continues to dominate when it comes to deploying applications in production environments. It comes as no surprise that most companies are committed to protecting their past strategic investments in Java.</li>
	</ul>
	<p>Once again, thanks to everyone who completed the survey and to the community members for their help with the promotion.</p>
	<p>Let me know what you think about this year's survey findings. We are open to suggestions on how we can improve the survey in the future, so please feel free to share your feedback.</p>
<div class="bottomitem">
 <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\may\images\mike.jpg"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Mike Milinkovich<br />
            <a target="_blank" href="https://www.eclipse.org">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/mmilinkov">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://eclipse-foundation.blog/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>