<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	
	<p>MicroProfile Rest Client is a type-safe client API for invoking RESTful services. Version 1.2 improves the developer experience and enhances integration with other MicroProfile technologies such as CDI, Fault Tolerance, OpenTracing, etc. MicroProfile Rest Client 1.2 is part of MicroProfile 2.2, which is available in <a href="https://openliberty.io/blog/2019/03/28/microprofile22-liberty-19003.html" target="_blank">Open Liberty 19.0.0.3</a>.</p>
	<p>Previous releases of MicroProfile Rest Client lacked integration with CDI, Fault Tolerance, OpenTracing, etc. making it difficult to add things like timeouts, retries, circuit breakers, etc. to client interfaces. MicroProfile Rest Client 1.2 improves integration with other MicroProfile technologies but also improves the developer experience so that you can specify the base URL directly in the <code>@RegisterRestClient</code> annotation in the interface and specify connect and read timeouts in a portable fashion.</p>
	<p>Sending HTTP headers or propagating them from a JAX-RS request has been a complicated problem with prior releases, but not anymore. Now you can specify HTTP headers to send or propagate using the new <code>@ClientHeaderParam annotation</code>, MicroProfile Config properties, or by implementing a new <code>ClientHeadersFactory</code> interface.</p>
	<p>To enable the MicroProfile Rest Client feature in your <code>server.xml</code>:</p>
<pre>
&lt;featureManager&gt;
  &lt;feature&gt;mpRestClient-1.2&lt;/feature&gt;
&lt;/featureManager&gt;
</pre>
	<p>Here is an example of specifying the URI in the annotation on the interface:</p>
<pre>
package io.openliberty.rest.client;
...
@RegisterRestClient(baseUri="http://localhost:9080/myBaseUri")
public interface ClientWithURI {
...
</pre>
	<p>This can still be overridden by specifying a MicroProfile Config property like this:</p>
<pre>
io.openliberty.rest.client.ClientWithURI/mp-rest/uri=http://localhost:9080/myOverriddenBaseUri
</pre>
<br>
<hr>
<br>
	<p>If the MicroProfile Rest Client interface is invoked from within a JAX-RS resource class, then it is possible to propagate HTTP headers from the incoming request to the outgoing Rest Client request by specifying the headers to propagate using this MP Config property:</p>
<pre>
org.eclipse.microprofile.rest.client.propagateHeaders=Authorization,MyCustomHeader
</pre>
	<p>It is also possible to specify or generate a header value using annotations like this:</p>
<pre>
@ClientHeaderParam(name="HeaderOrigin", value="MPRestClientInOpenLiberty")
@ClientHeaderParam(name="MyHeader", value="value1")
@Path("/somePath")
public interface MyClient {

    @GET
    @ClientHeaderParam(name="MyHeader", value="{computeValue}")
    MyResponse getSomeResponse();

    default String computeValue() {
        return "value2";
    }
...
</pre>
	<p>In this example, the client would send two newly-created HTTP headers when the <code>getSomeResponse()</code> method is invoked:</p>
<pre>
 HeaderOrigin: MPRestClientInOpenLiberty
 MyHeader: value2
</pre>
	<p>Header values can be computed by surrounding the method name in curly-braces. The only methods that can be specified to compute a header value are default interface methods in the same client interface or public static methods (which must be fully-qualified).</p>	
	<p>In the case where the same header is specified at the interface level and the method level, the value specified at the method level is used. Thus <code>MyHeader</code> is set to <code>value2</code> rather than <code>value1</code>.</p>
	<p>If the mpFaultTolerance-2.0 feature has been specified, then the functionality of the Rest Client interfaces can be augmented with handy Fault Tolerance APIs, for example:</p>
<pre>
public interface MyFaultTolerantClient {

    @GET
    @Retry(retryOn={WebApplicationException.class}, maxRetries = 3)
    String method1();

    @PUT
    @Timeout(value=3, unit=ChronoUnit.SECONDS)
    boolean method2(MyEntity entity);

    @POST
    @Fallback(fallbackMethod="queueForLater")
    boolean method3(MyEntity entity);

    default boolean queueForLater(MyEntity entity) {
        return addToRetryQueue(entity);
    }
}
</pre>

	<p>In this example, <code>method1</code> retries the request up to three times in the event that a <code>WebApplicationException</code> occurs. The <code>@Timeout</code> annotation on <code>method2</code> automatically sets the connect and read timeouts for the request to 3 seconds, and if the request takes longer than 3 seconds, the Fault Tolerance implementation interrupts the request. Lastly, if an exception occurs when invoking method3, the Fault Tolerance implementation invokes the <code>queueForLater</code> method.</p>
	<p>To try it out, take a look at <a href="https://openliberty.io/blog/2019/03/28/microprofile22-liberty-19003.html" target="_blank">Open Liberty 19.0.0.3</a>, which provides a full implementation of MicroProfile 2.2, including MicroProfile Rest Client 1.2.</p>
	<p>For more details, take a look at the <a href="https://openliberty.io/docs/ref/microprofile/2.2/" target="_blank">MicroProfile Javadoc</a>.</p>
	<p>To learn to use MicroProfile Rest Client, see the <a href="https://openliberty.io/guides/microprofile-rest-client.html" target="_blank">Consuming RESTful services with template interfaces</a> guide.</p>
	    
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/may/images/andy.jpg"
        alt="Andy McCright" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
           Andy McCright <br />
            <a target="_blank" href="https://www.ibm.com/ca-en">IBM</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/andrewmccright">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>