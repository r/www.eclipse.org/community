<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.31.05</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                              <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/gabriela.jpg" />
                            <h3>Editor's Note</h3>
							<p>The Jakarta community has already made great progress this year and work is underway to deliver the Jakarta EE 8 release. But before we dive deeper into this Jakarta newsletter issue, let's take a moment to look at the 2019 Jakarta EE developer survey results, which canvassed nearly 1,800 Java developers about their adoption of Jakarta EE and trends in Java programming. The findings indicate that cloud native is critically important with a third of developers currently building cloud native architectures and another 30 percent planning to within the next year.</p>
							<p>Access the full findings of the 2019 Java Community Developer Survey <a href="https://jakarta.ee/documents/insights/2019-jakarta-ee-developer-survey.pdf" target="_blank">here</a>.</p>
							<p>This issue puts a spotlight on the present and future of Jakarta EE, including thoughts on the agreement between the Eclipse Foundation and Oracle on Jakarta EE and the use of Java trademarks and the javax namespace, how the current challenges can be transformed into opportunities, as well as the next steps towards Jakarta EE 8 and beyond.</p>
							<p>You'll also have the chance to read about Quarkus, a next-generation Kubernetes native Java framework, learn how to easily migrate from your favorite Java EE server to an Eclipse MicroProfile implementation and discover how MicroProfile Rest Client 1.2 improves integration with other MicroProfile technologies.</p>	
								<p> &#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/EEsecurity.php">EE Security in Relation to JASPIC, JACC and LoginModules/Realms</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfilemigration.php">Gradual Migration from Java EE to MicroProfile</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfileRestClient.php">Improved developer experience and MicroProfile integration for MicroProfile Rest Client 1.2</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/quarkus.php">Going Native with Eclipse MicroProfile and Quarkus</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEE.php">Jakarta EE, what's in it for the Community?</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartafuture.php">Jakarta Going Forward</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/javax.php">Thoughts about Jakarta EE Future without 'javax'</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEsurvey.php">The Cloud Native Imperative - Results from the 2019 Jakarta EE Developer Survey</a><br>
                            		&#10140; <a style="color:#000;" target="_blank" href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEspecification.php">Jakarta EE and Specification Projects</a><br>
							<p>To learn more about the ongoing collaborative efforts to build tomorrow's enterprise Java platform for the cloud, check out the <a href="https://jakartablogs.ee/" target="_blank">Jakarta Blogs</a>.</p>
							<p>Have you attended a Jakarta Tech Talk yet? The <a href="https://www.meetup.com/jakartatechtalks_/" target="_blank">Jakarta Tech Talks</a> virtual meetup series is dedicated to the discovery and discussion of Jakarta EE and broader Cloud Native Java technologies. Become a member on the <a href="https://www.meetup.com/jakartatechtalks_/" target="_blank">Meetup page</a> to stay updated on talks to come.</p>
                            <p>PS: It's that time of the year again: the Call for Papers (CFP) is now open for EclipseCon Europe 2019! Please visit the CFP page <a href="https://www.eclipsecon.org/europe2019/cfp">here</a> for information on how to submit your talks. This year, the early-bird submission deadline is July 1 and the final submission deadline is July 15. If you have an idea for a talk that will educate and inspire the Eclipse community, we would love to hear from you!</p>
                            <p>Happy reading!</p>
                            <p>Gabriela Motroc</p>
                            <p><a style="color:#000;" target="_blank" href="https://twitter.com/GabrielaMotroc">@GabrielaMotroc</a></p>

                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/EEsecurity.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/1.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/EEsecurity.php"><h3> EE Security in Relation to JASPIC, JACC and LoginModules/Realms</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfilemigration.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/2.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfilemigration.php"><h3>Gradual Migration from Java EE to MicroProfile</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfileRestClient.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/3.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/MicroProfileRestClient.php"><h3>Improved developer experience and MicroProfile integration for MicroProfile Rest Client 1.2</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/quarkus.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/4.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/quarkus.php"><h3>Going Native with Eclipse MicroProfile and Quarkus</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEE.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/5.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEE.php"><h3>Jakarta EE, what's in it for the Community?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartafuture.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/6.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartafuture.php"><h3>Jakarta Going Forward</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/javax.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/7.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/javax.php"><h3>Thoughts about Jakarta EE Future without 'javax'</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                      <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEsurvey.php""><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/8.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                          <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEsurvey.php""><h3>The Cloud Native Imperative - Results from the 2019 Jakarta EE Developer Survey</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->
      
        <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEspecification.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/9.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/may/jakartaEEspecification.php"><h3>Jakarta EE and Specification Projects</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                     </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Proposals</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-shellwax">Eclipse ShellWax</a> aims to provide a rich editor for shell script files using existing language server for the purpose. The goal of the project is to be thin wrapper around language server and shell interpreters as provided by the operating system.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-codewind">Eclipse Codewind</a> aims to provide tools to help build high-quality cloud-native applications for Kubernetes, regardless of which IDE or language you use.</li>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/jakarta-batch">Jakarta Batch</a> was created as part of the process of transitioning Java EE 8 technologies to the Eclipse Foundation as described in The Eclipse Enterprise for Java Project Top Level Project Charter.</li>
                            </ul>
                            <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>New Project Releases</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.sumo">Eclipse SUMO</a></li>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.wildwebdeveloper">Eclipse Wild Web Developer</a></li>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.buildship">Eclipse Buildship</a></li>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/locationtech.proj4j">LocationTech Proj4J</a></li>
	                            <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/rt.ebr">Eclipse Bundle Recipes</a></li>
                            </ul>
                            <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2019"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/ece.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://wiki.eclipse.org/Eclipse_IoT_Day_Paris_Saclay_2019"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/saclay.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p>Here is the list of upcoming Eclipse and Eclipse related events:</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.eventbrite.com/e/eclipse-iot-day-paris-saclay-2019-tickets-60845532626">IoT Day Paris</a><br>
                           June 4, 2019 | Paris, France</p>
                           <p><a target="_blank" style="color:#000;" href="https://tmt.knect365.com/iot-world-europe/">IoT World Europe Summit</a><br>
                           June 12-13, 2019 | London, UK</p>
                           <p><a target="_blank" style="color:#000;" href="https://devopsconference.de/">DevOpsCon</a><br>
                           June 11-14, 2019 | Berlin, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://iotcon.de/en">IoT conference</a><br>
                           June 17-19, 2019 | Munich, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.comcastlabsconnect.com/open-source-2019">Comcast Labs Open Source Conference</a><br>
                           June 20, 2019 | Denver, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.java-forum-stuttgart.de/de/Home.html">Java Forum Stuttgart</a><br>
                           July 4, 2019 | Stuttgart, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://conferences.oreilly.com/oscon/oscon-or"> OSCON</a><br>
                           July 15-18, 2019 | Portland Oregon, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2019">EclipseCon Europe 2019</a><br>
                           October 21-24, 2019 | Ludwigsburg, Germany
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->