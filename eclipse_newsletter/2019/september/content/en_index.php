<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="<?php print $email_body_width; ?>" id="emailBody">

      <!-- MODULE ROW // -->
      <!--
        To move or duplicate any of the design patterns
          in this email, simply move or copy the entire
          MODULE ROW section for each content block.
      -->

      <tr class="banner_Container">
        <td align="center" valign="top">

          <?php if (isset($path_to_index)): ?>
            <p style="text-align:right;"><a href="<?php print $path_to_index; ?>">Read in your browser</a></p>
          <?php endif; ?>

          <!-- CENTERING TABLE // -->
           <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer">
            <tr>
              <td background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Banner.png" id="bannerTop"
                class="flexibleContainerCell textContent">
                <p id="date">Eclipse Newsletter - 2019.26.09</p> <br />

                <!-- PAGE TITLE -->

                <h2><?php print $pageTitle; ?></h2>

                <!-- PAGE TITLE -->

              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr class="ImageText_TwoTier">
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td align="center" valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="85%"
                        class="flexibleContainer marginLeft text_TwoTier">
                        <tr>
                          <td valign="top" class="textContent">
                            <img class="float-left margin-right-20" src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/shabnam.png" />
                          <h3>Editor's Note</h3>
                          <p>Cloud native Java is top of mind for many of us in the
                          Eclipse Foundation community these days. It was a hot topic at the recent
                          inaugural JakartaOne Livestream event with at least three presentations
                          focusing on Eclipse MicroProfile&rsquo;s powerful capabilities for
                          designing, building, and deploying enterprise Java microservices.</p>
                        <p>The ongoing excitement around MicroProfile and its role in
                          bringing microservices to enterprise Java environments is evident in the
                          articles we&rsquo;re sharing with you in this edition of the newsletter.</p>
                        <p>In this issue of the newsletter, our community authors explain
                          the pros and cons of different microservice programming models and how to
                          use MicroProfile to incorporate key features, such as observability and
                          fault tolerance, into microservices architectures. They share information
                          about opportunities for Spring Boot developers to leverage Quarkus to
                          access MicroProfile capabilities and to combine MicroProfile and Jakarta
                          EE on the same application server. One author even takes a peek into the
                          future with a look at where MicroProfile is headed. We wrap up the issue
                          with highlights from the JakartaOne Livestream event.</p>
                        <p>We&rsquo;re sure you&rsquo;ll agree we have a stellar line-up
                          of interesting and informative content from some of the leading minds in
                          our community:</p>
                        <ul>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile.php">Building Cloud Native Microservices. Which Programming Model Should I
                            Use?</a></li>
                          <li><a href="https://www.tomitribe.com/blog/microprofile-how-it-has-evolved-and-where-its-headed/">MicroProfile: How It Has Evolved and Where It&rsquo;s Headed</a></li>
                          <li><a href="https://microprofile.io/2019/05/02/introduction-observability-with-microprofile/">Introduction: Observability With MicroProfile</a></li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/autowire_microprofile.php">Autowire MicroProfile Into Spring With Quarkus</a></li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile_powered_application_servers.php">MicroProfile-Powered Application Servers</a></li>
                          <li><a href="https://blog.payara.fish/microprofile-fault-tolerance-2.0">Eclipse MicroProfile Fault Tolerance 2.0</a></li>
                          <li><a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/jakartaone.php">JakartaOne Livestream Wrap-up</a></li>
                        </ul>
                        <p>Don&rsquo;t Forget&hellip; EclipseCon Is Fast Approaching</p>
                        <p>
                          <a href="https://www.eclipsecon.org/europe2019">EclipseCon Europe</a> is
                          just around the corner (Oct 21-24 in Ludwigsburg, Germany), so if you
                          haven&rsquo;t already registered, be sure to <a
                            href="https://www.eclipsecon.org/europe2019/registration"
                          >do that now</a>.
                        </p>
                        <p>
                          We&rsquo;re particularly excited about <a
                            href="https://www.eclipsecon.org/europe2019/eclipse-community-day"
                          >Community Day</a> on October 21. This event is a must for everyone
                          who&rsquo;s interested in our cloud native projects. You&rsquo;ll have
                          great opportunities to get together with other community members to share
                          ideas, learn from their experiences, and participate in workshops. This
                          year, we&rsquo;ll be discussing potential collaboration between Jakarta EE
                          and MicroProfile, among other topics, so you won&rsquo;t want to miss it.
                          We&rsquo;ve also planned an evening event so you&rsquo;re sure to have
                          lots of fun!&nbsp;
                        </p>
                        <p>
                          Check the <a
                            href="https://wiki.eclipse.org/Cloud_Native_Community_Events_2019"
                          >EclipseCon Community Day wiki</a> for more information about what&rsquo;s
                          planned so far and to add your ideas for additional discussion topics.
                        </p>
                        <p>Congrats to the Jakarta EE Community</p>
                        <p>
                          As a final note, I just wanted to emphasize how extremely proud everyone
                          at the Eclipse Foundation was when our executive director, Mike
                          Milinkovich, accepted the Duke&rsquo;s Choice Award on behalf of the
                          Jakarta EE community at the Oracle Code One conference in mid-September.
                          Read more about the significance of this recognition for our community <a
                            href="https://blogs.eclipse.org/post/thabang-mashologu/eclipse-community-continues-deliver-open-source-commitment"
                          >here</a>.
                        </p>
                        <p>The Duke&rsquo;s Choice Awards celebrate invaluable innovation
                          in Java-based technologies and contributions to Java, and our Jakarta EE
                          Working Group was recognized for its outstanding open source contributions
                          to the Java ecosystem. Duke&rsquo;s Choice Award winners are selected by
                          members of Oracle&rsquo;s Java Platform Group, which makes this
                          recognition a particularly meaningful achievement.</p>
                        <p>Enjoy the articles!&nbsp;</p>
                        <p>Shabnam Mayel</p>
                      </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

 <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <hr>
          <h3 style="text-align: left;">Articles</h3>
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                           <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/autowire_microprofile.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_autowire_microprofile.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/autowire_microprofile.php"><h3>Autowire MicroProfile Into Spring With Quarkus</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile_powered_application_servers.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_microprofile_powered_application_servers.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                          <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile_powered_application_servers.php"><h3>MicroProfile-Powered Application Servers</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->



   <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_microprofile.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/microprofile.php"><h3>Building Cloud Native Microservices? Which Programming Model Should I Use?</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/jakartaone.php"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_jakartaone.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.eclipse.org/community/eclipse_newsletter/2019/september/jakartaone.php"><h3>JakartaOne Livestream Wrap up</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://www.tomitribe.com/blog/microprofile-how-it-has-evolved-and-where-its-headed/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_microprofile_how.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://www.tomitribe.com/blog/microprofile-how-it-has-evolved-and-where-its-headed/"><h3>MicroProfile: How It Has Evolved and Where It’s Headed</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://microprofile.io/2019/05/02/introduction-observability-with-microprofile/"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_introduction.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://microprofile.io/2019/05/02/introduction-observability-with-microprofile/"><h3>Introduction: Observability With MicroProfile</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                       <tr>
                          <td valign="top" class="imageContentLast">
                            <a href="https://blog.payara.fish/microprofile-fault-tolerance-2.0"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/september/images/card_eclipse_microprofile.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                         <tr>
                          <td valign="top" class="textContent">
                            <a href="https://blog.payara.fish/microprofile-fault-tolerance-2.0"><h3>Eclipse MicroProfile Fault Tolerance 2.0</h3></a>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p></p>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <!-- CONTENT TABLE // -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 style="margin-bottom:10px;">New Project Proposals</h3>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                            		<li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-jkube">Eclipse JKube</a> provides plugins and libraries for the Java build tools for building and deploying applications for Kubernetes.</li>
                                <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-iot-packages">Eclipse IoT Packages</a> provides deployment instructions and scripts for deploying packages of Eclipse IoT projects to Kubernetes based platforms using Helm charts.</li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                                <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/proposals/eclipse-vulnerability-assessment-tool">Eclipse Vulnerability Assessment Tool</a> analyses Java and Python applications to identify, assess and mitigate the use of open-source dependencies with known vulnerabilities.</li>
                            </ul>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>Interested in more project activity? <a target="_blank" style="color:#000;" href="http://www.eclipse.org/projects/project_activity.php">Read on!</a>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

      <!-- MODULE ROW // -->
      <tr>
        <td align="left" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="left" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <h3 style="margin-bottom:10px;">New Project Releases</h3>
                      <p>The combination of the quarterly simultaneous release and the Jakarta EE 8 release have made this a busy month for releases.</p>
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.papyrus/releases/4.5.0">Eclipse Papyrus 4.5.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4e/releases/0.11.0">Eclipse LSP4E 0.11.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.dirigible/releases/3.5">Eclipse Dirigible 3.5</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ecd.theia/releases/0.10.0">Eclipse Theia 0.10.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/eclipse/releases/4.13.0">Eclipse Project 4.13.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.batch/releases/1.0">Jakarta Batch 1.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.bean-validation/releases/2.0">Jakarta Bean Validation 2.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.ca/releases/1.3.5">Jakarta Annotations 1.3.5</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.cdi/releases/2.0">Jakarta Contexts and Dependency Injection 2.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.cu/releases/1.1.2">Jakarta Concurrency 1.1.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.ejb/releases/3.2.4">Jakarta Enterprise Beans 3.2.4</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.el/releases/3.0.3">Jakarta Expression Language 3.0.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.es/releases/1.0.2">Jakarta Security 1.0.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.faces/releases/2.3">Jakarta Server Faces 2.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.interceptors/releases/1.2.4">Jakarta Interceptors 1.2.4</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jacc/releases/1.6.2">Jakarta Authorization 1.6.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jakartaee-platform/releases/8.0">Jakarta EE Platform 8.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jakartaee-stable/releases/1.0.1">Jakarta Stable APIs 1.0.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jaspic/releases/1.1.3">Jakarta Authentication 1.1.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jaxrs/releases/2.1.6">Jakarta RESTful Web Services 2.1.6</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jaxws/releases/2.3.3">Jakarta XML Web Services 2.3.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jca/releases/1.7.3">Jakarta Connectors 1.7.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jersey/releases/2.29.1">Eclipse Jersey 2.29.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jms/releases/2.0.3">Jakarta Messaging 2.0.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jpa/releases/2.2.3">Jakarta Persistence 2.2.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jsonb/releases/1.0.2">Jakarta JSON Binding 1.0.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jsonp/releases/1.1.6">Jakarta JSON Processing 1.1.6</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jsp/releases/2.3.5">Jakarta Server Pages 2.3.5</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jstl/releases/1.2.4">Jakarta Standard Tag Library 1.2.4</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.jta/releases/1.3.3">Jakarta Transactions 1.3.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.javamail/releases/1.6.4">Jakarta Mail 1.6.4</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.servlet/releases/4.0.3">Jakarta Servlet 4.0.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/ee4j.websocket/releases/1.1.2">Jakarta WebSocket 1.1.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/iot.keyple/releases/0.7.0-0">Eclipse Keyple 0.7.0 (Java 19/07)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/locationtech.geowave/releases/1.0.0">LocationTech GeoWave 1.0.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.capra/releases/0.7.1">Eclipse Capra 0.7.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.ecp/releases/1.22.0">Eclipse EMF Client 1.22.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.emf.cdo/releases/4.8.0">Eclipse CDO 4.8.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.emf.emf/releases/2.19.0">Eclipse EMF 2.19.0</a></li>
                            </ul>
                          </td>
                         </tr>
                      </table> <!-- // CONTENT TABLE --> <!-- CONTENT TABLE // -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <ul>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.emfcompare/releases/3.3.8">Eclipse EMF Compare 3.3.8</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.graphiti/releases/0.16.1">Eclipse Graphiti 0.16.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.ocl/releases/2019-09-6.9.0">Eclipse OCL 2019-09 (6.9.0)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mdt.xsd/releases/2.19.0">Eclipse XSD 2.19.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mmt.atl/releases/4.1.0">Eclipse ATL 4.1.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mmt.qvt-oml/releases/2019-09-3.10.0">Eclipse QVT-OML 2019-09 (3.10.0)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.mmt.qvtd/releases/2019-09-0.20.0">Eclipse QVTd 2019-09 (0.20.0)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.tmf.xtext/releases/2.19.0">Eclipse Xtext 2.19.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/modeling.viatra/releases/2.2.1">Eclipse VIATRA 2.2.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.ease/releases/0.7.0">Eclipse Advanced Scripting Environment 0.7.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.egit/releases/5.5.0">Eclipse Git Team Provider 5.5.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.handly/releases/1.2.1">Eclipse Handly 1.2.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jgit/releases/5.5.0">Eclipse JGit 5.5.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.jubula/releases/8.0.0-2019-09">Eclipse Jubula 8.0.0 (2019-09)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.lsp4j/releases/0.8.0">Eclipse LSP4J 0.8.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.m2e/releases/1.13">Eclipse m2eclipse 1.13</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.microprofile/releases/metrics-2.1">Eclipse Microprofile Metrics 2.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.nebula.nattable/releases/1.6.0">Eclipse NatTable 1.6.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.openj9/releases/0.16.0">Eclipse OpenJ9 0.16.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.packager/releases/0.16.0">Eclipse Packager 0.16.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.packaging.mpc/releases/1.8.0">Eclipse Marketplace Client Project 1.8.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.passage/releases/0.6.0">Eclipse Passage 0.6.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.reddeer/releases/2.7.0">Eclipse RedDeer 2.7.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.sw360.antenna/releases/1.0">Eclipse SW360antenna 1.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/technology.tm4e/releases/0.3.4">Eclipse TM4E 0.3.4</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.buildship/releases/3.1.2">Eclipse Buildship 3.1.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.cdt/releases/9.9.0">Eclipse C/C++ Development Tools 9.9.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.cdt.tcf/releases/1.7.1">Eclipse Target Communication Framework 1.7.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.corrosion/releases/0.4.2">Eclipse Corrosion 0.4.2</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.dartboard/releases/0.1">Eclipse Dartboard 0.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.gef/releases/5.2.0-2019-09">Eclipse Graphical Editing Framework 5.2.0 (2019-09)</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.linuxtools/releases/7.4.0">Eclipse Linux Tools 7.4.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.mat/releases/1.9.1">Eclipse Memory Analyzer 1.9.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.pdt/releases/6.3">Eclipse PHP Development Tools 6.3</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.ptp/releases/9.3.1">Eclipse Parallel Tools Platform 9.3.1</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.tracecompass/releases/5.1.0">Eclipse Trace Compass 5.1.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/tools.wildwebdeveloper/releases/0.5.0">Eclipse Wild Web Developer 0.5.0</a></li>
                              <li><a target="_blank" style="color:#000;" href="https://projects.eclipse.org/projects/webtools/releases/3.15-2019-09">Eclipse Web Tools Platform Project 3.15 (2019-09)</a></li>
                            </ul>

                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
                <p>View all the project releases <a target="_blank" style="color:#000;" href="https://www.eclipse.org/projects/tools/reviews.php">here!</a>.</p>
                <hr>
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->


      <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <table border="0" cellpadding="0" cellspacing="0"
            width="100%">
            <tr>
              <td align="center" valign="top">
                <!-- FLEXIBLE CONTAINER // -->
                <table border="0" cellpadding="0" cellspacing="0"
                  width="<?php print $email_body_width; ?>" class="flexibleContainer">
                  <tr>
                    <td valign="top" width="<?php print $email_body_width; ?>"
                      class="flexibleContainerCell">
                      <!-- CONTENT TABLE // -->
                      <table align="Left" border="0" cellpadding="0"
                        cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3>Upcoming Eclipse Events</h3>
                          </td>
                        </tr>
                        <tr>
                          <td valign="top" class="textContent">
                            <p>Eclipse events are being hosted all over the world! Get involved by attending
                            or organizing an event. <a target="_blank" style="color:#000;" href="http://events.eclipse.org/">View all events</a>.</p>
                          </td>
                        </tr>
                     <tr>
                          <td valign="top" class="imageContentLast">
                            <a target="_blank" href="https://www.eclipsecon.org/europe2019"><img
                            src="<?php print $path; ?>/community/eclipse_newsletter/2019/may/images/ece.png"
                            width="<?php print $col_2_img; ?>" class="flexibleImage"
                            style="max-width: <?php print $col_2_img; ?>;" /></a>
                          </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->
                      <table align="Right" border="0"
                        cellpadding="0" cellspacing="0" width="<?php print $col_2_table; ?>"
                        class="flexibleContainer">
                        <tr>
                          <td valign="top" class="textContent">
                           <p><a target="_blank" style="color:#000;" href="https://jaxlondon.com">Jax London</a><br>
                           October 7-10, 2019 | London, UK</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.eclipsecon.org/europe2019">EclipseCon Europe 2019</a><br>
                           October 21-24, 2019 | Ludwigsburg, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://devoxx.be/">Devoxx Belgium</a><br>
                           November 4-8, 2019 | Antwerp, Belgium</p>
                           <p><a target="_blank" style="color:#000;" href="http://www.embeddedconference.se/app/netattm/attendee/page/82448">Embedded Conference Scandinavia</a><br>
                           November 5-6, 2019 | Stockholm, Sweden</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.continuouslifecycle.de/">Continuous Lifecycle 2019</a><br>
                           November 12-15, 2019 | Mannheim, Germany</p>
                           <p><a target="_blank" style="color:#000;" href="https://events.linuxfoundation.org/events/kubecon-cloudnativecon-north-america-2019/">KubeCon North America 2019</a><br>
                           November 18-21, 2019 | San Diego, USA</p>
                           <p><a target="_blank" style="color:#000;" href="https://www.edgecomputingworld.com/">Edge Computing World 2019</a><br>
                           December 9-12, 2019 | Mountain View, USA</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/clouds.htm">Jakarta EE, Eclipse MicroProfile and Clouds</a><br>
                           December 11, 2019 | Munich Airports</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/streaming.htm">Streaming Architectures with Jakarta EE and Eclipse MicroProfile</a><br>
                           December 13, 2019 | Munich Airports</p>
                           <p><a target="_blank" style="color:#000;" href="http://workshops.adam-bien.com/microservices.htm">Jakarta EE and Eclipse MicroProfile Microservices</a><br>
                           December 19, 2019 | Munich Airports</p>
                          </td>
                        </tr>
                        <tr>
                        	<td valign="top" class="textContent">
                        		<p>Are you hosting an Eclipse event? Do you know about an Eclipse event happening in your community? Email us the details <a style="color:#000;" href="mailto:events@eclipse.org?Subject=Eclipse%20Event%20Listing">events@eclipse.org</a>!</p>
                       		 </td>
                        </tr>
                      </table> <!-- // CONTENT TABLE -->

                    </td>
                  </tr>
                </table> <!-- // FLEXIBLE CONTAINER -->
              </td>
            </tr>
          </table> <!-- // CENTERING TABLE -->
        </td>
      </tr>
      <!-- // MODULE ROW -->

     <!-- MODULE ROW // -->
      <tr>
        <td align="center" valign="top">
          <!-- CENTERING TABLE // -->
          <!--
              The centering table keeps the content
                tables centered in the emailBody table,
                in case its width is set to 100%.
            -->
          <table background="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/footer.png" border="0" cellpadding="0" cellspacing="0"
            width="100%" class="flexibleContainer" id="bannerBottom">
            <tr class="ThreeColumns">
              <td valign="top" class="imageContentLast"
                style="position: relative; width: inherit;">
                <div
                  style="width: 160px; margin: 0 auto; margin-top: 30px;">
                  <a href="http://www.eclipse.org/"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/Eclipse_Logo.png"
                    width="100%" class="flexibleImage"
                    style="height: auto; max-width: 160px;" /></a>
                </div>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Contact Us</h3>
                <a href="mailto:newsletter@eclipse.org"
                style="text-decoration: none; color:#ffffff;">
                <p id="emailFooter">newsletter@eclipse.org</p></a>
              </td>
            </tr>

            <tr class="ThreeColumns">
              <td valign="top" class="textContent followUs"
                style="position: relative; width: inherit; margin-top: 12%;">
                <h3 style="padding-top: 25px;">Follow Us</h3>
                <div id="iconSocial">
                  <a href="https://twitter.com/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Twitter.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a> <a
                    href="https://www.facebook.com/eclipse.org"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Facebook.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>  <a
                    href="https://www.youtube.com/user/EclipseFdn"><img
                    src="<?php print $path; ?>/community/eclipse_newsletter/assets/public/images/icon_Youtube.png"
                    width="23" class="flexibleImage"
                    style="max-width: 23px !important;" /></a>
                </div>
              </td>
            </tr>
          </table>
        <!-- // CENTERING TABLE -->
        </td>
      </tr>

    </table> <!-- // EMAIL CONTAINER -->