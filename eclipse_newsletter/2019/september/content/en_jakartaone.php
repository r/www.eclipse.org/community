<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>September 10, 2019 was a big day in Jakarta EE global circles. We released the first
  version of Jakarta EE, and we held the first JakartaOne Livestream conference. It was a very
  ambitious undertaking, but what an experience it was!</p>
<p>The intention of the JakartaOne Livestream conference was to mark an important
  milestone, the release of the first vendor-neutral, Java EE 8-compatible release of Jakarta EE
  following the new Jakarta EE Specification Process.&nbsp;</p>
<p>Almost two years after Oracle announced it would contribute Java EE to the Eclipse
  Foundation, we finally have a base-level release that will enable developers to easily and
  seamlessly move existing Java EE 8-certified products to the Jakarta EE 8 release. The release is
  the result of cross-community collaboration at the Eclipse Foundation. Jakarta EE 8 uses the same
  APIs and javax namespace as Java EE 8, its Jakarta EE 8 technology compatibility kits (TCKs) are
  fully compatible with Java EE 8 TCKs, and a compatibility and branding process is available to
  certify compatible products.</p>
<h2>An Enthusiastic Response From the Start</h2>
<p>It was very obvious from the beginning that this event would be bigger than we
  expected. Here&rsquo;s why. We approached a few, well-respected leaders in the Java EE community
  to join the JakartaOne Livestream Program Committee. What a thrill that was! Reza Rahman
  graciously accepted the role of Program Committee Chair, and was supported by an equally eager and
  interested group of committee members: Adam Bien, Ivar Grimstad, Arun Gupta, Josh Juneau , and
  from the Eclipse Foundation, Tanja Obradovic. It was a great pleasure working with this team.</p>
<p>We started working on the conference in early June. Because all members had busy
  schedules and summer vacations, we made the most of the time we had to develop a plan and put out
  a call for papers (CFP) for the conference.</p>
<p>
  The response to our CFP was another indicator of the wider community&rsquo;s interest in an event
  such as this one. We received more than 50 high-quality submissions, making it a difficult task to
  select the best ones. With Reza&rsquo;s help, we settled on the sessions listed here at<a
    href="https://jakartaone.org/"
  > jakartaone.org</a> along with links to the session recordings.&nbsp;&nbsp;&nbsp;
</p>
<h2>Excellent Participation, Interesting Sessions</h2>
<p>Participants enjoyed a great mixture of introductory and overview sessions, including
  sessions on particular specifications, cloud native topics, keynotes from Mike Milinkovich and
  James Gosling, as well as industry keynotes from Jakarta EE Working Group Steering Committee
  members IBM, Fujitsu, Oracle, Payara, Red Hat, and Tomitribe, along with demos and panel
  discussions.&nbsp;</p>
<p>We selected 16 submissions, which resulted in 18 hours of program material including
  keynote addresses and panel discussions. Adam Bien was the event MC and I was available to help
  out.&nbsp;</p>
<h2>A Resounding Success</h2>
<p>You may call it beginner&#39;s luck, but on the day of the event, we had well over
  1,350 registered attendees, and a number of questions on the chat were almost immediately answered
  by others in the community.</p>
<p>The positive spirit and sense of the community coming together were overwhelming,
  illustrating the true power of open source. With very few technical difficulties, sessions ran
  smoothly one after another, and even when we did have issues, Reza Rahman was available to save
  the day yet again with an additional Q&amp;A session.</p>
<p>
  <img class="img-responsive" src="images/jakartaone.jpg" />
</p>
<p>Leaving the best for last, in terms of running the event, the work of the Eclipse
  Foundation team was impressive, to say the least. I could not ask for better support than working
  with Stephanie Swart, Laura Tran, and Shabnam Mayel. Their level of dedication, professionalism,
  and readiness to improvise to deal with issues that arose was amazing. And this was the first
  conference any of us has worked on! Let&rsquo;s take a deep breath and enjoy the success for
  another moment before we start working on the next Jakarta EE release.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/august/images/tanja.png"
        alt="Tanja Obradovic" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Tanja Obradovic</p>
         <p>Jakarta EE Program Manager</p>
        </div>
      </div>
     </div>
   </div>
 </div>