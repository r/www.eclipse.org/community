<?php
/**
 * Copyright (c) 2019 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>MicroProfile was invented to provide specifications for building microservices that are the right
  size for scalable environments, such as clouds. It includes specifications for exposing health
  checks, metrics, and telemetry data as well as specifications that handle configuration and
  inter-service communications. Several standalone MicroProfile implementations are available.
  However, because MicroProfile is basically a set of specifications, it doesn&rsquo;t enforce a
  specific approach for implementation. This opens up possibilities for other kinds of
  implementations.</p>
<h2>Microservice Capabilities</h2>
<p>One possibility is to implement MicroProfile specifications in a traditional <strong>application server</strong>,
  along with the Jakarta EE specifications it already contains. Like MicroProfile, Jakarta EE is a
  set of specifications that can be implemented in the same application server as other
  specifications. Although MicroProfile and Jakarta EE are separate frameworks, they function as one
  entity when implemented in the same application server.</p>
<p>
  <img src="/community/eclipse_newsletter/2019/september/images/microprofile_powered_application_servers_01.png" style="height: 216px; width: 308px" />
</p>
<p>Adding MicroProfile specifications allows the application server to run as a microservice in a
  cloud. Traditionally, Jakarta EE has not provided specifications for these features. When
  MicroProfile is available in the same running instance of the application server, these metrics
  are provided by endpoints exposed by MicroProfile. Load balancers, orchestrators, and monitoring
  tools need this kind of data from the running application server instances to determine whether
  scaling up or down is required.</p>
<h2>Conclusion</h2>
<p>In my daily job as a developer, I work with application servers such as Payara. Payara is just
  one of several application servers that currently implement MicroProfile specifications. When a
  project to adopt Docker and deploy it in a containerized environment began, we started adopting
  MicroProfile specifications to create a microservices architecture. The architecture is evolving
  gradually as the specifications mature with every release of MicroProfile that&rsquo;s added to
  the application server.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-4">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/september/images/edwin.png" alt="Edwin Derks"
          />
        </div>
        <div class="col-sm-20">
          <p class="author-name">
            Edwin Derks
          </p>
          <p>Software Architect at Ordina JTech</p>
        </div>
      </div>
    </div>
  </div>
</div>
