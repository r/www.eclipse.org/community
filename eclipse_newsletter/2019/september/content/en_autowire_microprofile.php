<?php
/**
 * Copyright (c) 2019 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Eric Poirier (Eclipse Foundation) - Initial implementation
 *
 * SPDX-License-Identifier: EPL-2.0
 */
// This file must be included
if (basename(__FILE__) == basename($_SERVER['PHP_SELF'])) {
  exit();
}
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<p>When developing Java microservices, Eclipse MicroProfile and Spring Boot are often thought of as
  separate and distinct APIs. Developers default to their mental muscle memory by leveraging APIs
  they use on a daily basis. Learning new frameworks and runtimes can require a major time
  investment. This article aims to ease Spring developers&rsquo; introduction to some popular
  MicroProfile APIs by enabling them to use the Spring APIs they already know while benefiting from
  significant new capabilities offered by Quarkus.</p>
<p>More specifically, this article describes the scope, and some details, of the Spring APIs that
  Quarkus supports so Spring developers have a grasp of the foundation they can build on with
  MicroProfile APIs. The article then covers MicroProfile APIs that Spring developers will find
  helpful when developing microservices. Only a subset of MicroProfile APIs are covered.&nbsp;</p>
<p>
  Why Quarkus? Live coding is one reason because any change is automatically reloaded whether
  you&rsquo;re using MicroProfile, Spring, or a different Java API. Just run <code>mvn quarkus:dev</code>.
  That&#39;s it. A second compelling reason is because the<a
    href="https://github.com/jclingan/quarkus-spring-microprofile"
  > example project</a>&#39;s Person service, which compiles Spring, MicroProfile, and Java
  Persistence API (JPA) APIs into a native binary format, starts in 0.055 seconds, and uses
  approximately 90 MB of resident set size (RSS) RAM after reaching the application&rsquo;s RESTful
  endpoints. Simply run <code>mvn package -Pnative</code> to compile to a native binary format. That&#39;s it.
</p>
<p>This article does not go into detailed comparisons, but should help Spring developers understand
  how Spring and MicroProfile APIs can be combined with Quarkus.</p>
<h2>Containers and Kubernetes</h2>
<p>To keep length down, this article describes Kubernetes support only at a high level. But it is
  important to discuss. One of Quarkus&rsquo; key value propositions is &quot;Kubernetes Native
  Java,&quot; where the goal is to minimize memory footprint and reduce startup time. The reduced
  memory footprint helps to increase the density of applications that share hardware, reducing
  overall costs.</p>
<p>
  Quarkus also<a href="https://quarkus.io/guides/kubernetes-resources"> supports auto-generation</a>
  of Kubernetes resources, and<a href="https://quarkus.io/guides/"> guides</a> for deploying Quarkus
  applications on Kubernetes and OpenShift are available. In addition, Dockerfile.jvm Java virtual
  machine (JVM) and Dockerfile.native native binary packages are automatically generated for
  container creation.
</p>
<p>Last, given that Quarkus considers Kubernetes to be a target deployment environment, it forgoes
  using Java frameworks when inherent Kubernetes capabilities are available. Table 1 briefly maps
  the Java frameworks typically used by Spring developers to Kubernetes built-in capabilities.</p>
<p><strong>Table 1: Java Framework to Kubernetes Mapping</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>
        <p>Capability</p>
      </th>
      <th>
        <p>Traditional Spring Boot</p>
      </th>
      <th>
        <p>Kubernetes</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>Service discovery</p>
      </td>
      <td>
        <p>Eureka</p>
      </td>
      <td>
        <p>DNS</p>
      </td>
    </tr>
    <tr>
      <td>
        <p>Configuration</p>
      </td>
      <td>
        <p>Spring Cloud Config</p>
      </td>
      <td>
        <p>ConfigMaps</p>
        <p>Secrets</p>
      </td>
    </tr>
    <tr>
      <td>
        <p>Load balancing</p>
      </td>
      <td>
        <p>Ribbon (client side)</p>
      </td>
      <td>
        <p>Service</p>
        <p>ReplicationController</p>
        <p>(server side)</p>
      </td>
    </tr>
  </tbody>
</table>
<h2>Compiling and Running the Example Code</h2>
<p>
  This article is accompanied by an<a href="https://github.com/jclingan/quarkus-spring-microprofile">
    example project</a> that combines Spring and MicroProfile APIs in the same project and the same
  Java class. The code can be compiled and run from the command line. For instructions, see
  README.md.
</p>
<h2>Spring Framework APIs</h2>
<p>This section describes dependency injection and the web framework for Spring Framework, including
  the Spring Data JPA and user-defined queries.</p>
<h3>Dependency Injection</h3>
<p>
  Quarkus supports many<a href="https://quarkus.io/guides/cdi-reference"> Contexts and Dependency
    Injection (CDI)</a> and Spring Dependency Injection (DI) APIs. MicroProfile, Java EE, and
  Jakarta EE developers will be very familiar with CDI. Spring developers can use the <strong>Quarkus
  Extension for Spring DI API</strong> for Spring DI compatibility. Table 2 provides a sample of the
  supported Spring DI API features.
</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses CDI
  and Spring DI APIs. The Quarkus<a href="https://quarkus.io/guides/spring-di-guide"> Spring DI
    Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 2: Sample of Supported Spring DI API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>
        <p>Supported Spring DI</p>
        <p>Feature</p>
      </th>
      <th>
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>Constructor Injection</p>
      </td>
      <td>
        <pre><code>
public PersonSpringController(
    PersonSpringRepository personRepository,&nbsp; // injected&nbsp;
    PersonSpringMPService personService) { // injected
    this.personRepository = personRepository;
    this.personService = personService;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>Field Injection</p>
        <p>@Autowired</p>
        <p>@Value</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <pre><code>
@Autowired
@RestClient
SalutationMicroProfileRestClient salutationRestClient;

@Value(&quot;${fallbackSalutation}&quot;)
String fallbackSalutation;
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Bean</p>
        <p>@Configuration</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <pre><code>
@Configuration
public class AppConfiguration {
    @Bean(name = &quot;capitalizeFunction&quot;)
    public StringFunction capitalizer() {
        return String::toUpperCase;
    }
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>@Component</td>
      <td>
        <pre><code>
@Component(&quot;noopFunction&quot;)
public class NoOpSingleStringFunction implements StringFunction {
    @Override
    public String apply(String s) {
        return s;
    }
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>@Service</td>
      <td>
        <pre><code>
@Service
public class MessageProducer {
    @Value(&quot;${greeting.message}&quot;)
    String message;
    public String getPrefix() {
        return message;
    }
}
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h3>Web Framework</h3>
<p>MicroProfile developers will be comfortable with Quarkus support for JAX-RS, MicroProfile Rest
  Client, JSON-P, and JSON-B as the core web programming model. Spring developers may be surprised
  to learn that Quarkus has recently added Spring Web API support, specifically around Spring
  REST-related APIs. As with Spring DI, the goal of Spring Web API support is to help Spring
  developers feel comfortable using Spring Web APIs and MicroProfile APIs together. Table 3 provides
  a sample of the supported Spring Web API features.</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses
  Spring Web and MicroProfile Rest Client APIs. The Quarkus<a
    href="https://quarkus.io/guides/spring-web-guide"
  > Spring Web Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 3: Sample of Supported Spring Web API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>
        <p>Supported Spring Web</p>
        <p>Feature</p>
      </th>
      <th>
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>@RestController</p>
        <p>@RequestMapping</p>
      </td>
      <td>
        <pre><code>
@RestController</p>
@RequestMapping(&quot;/person&quot;)
public class PersonSpringController {
    ...
    ...
    ...
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@GetMapping</p>
        <p>@PostMapping</p>
        <p>@PutMapping</p>
        <p>@DeleteMapping</p>
        <p>@PatchMapping</p>
        <p>@RequestParam</p>
        <p>@RequestHeader</p>
        <p>@MatrixVariable</p>
        <p>@PathVariable</p>
        <p>@CookieValue</p>
        <p>@RequestBody</p>
        <p>@ResponseStatus</p>
        <p>@ExceptionHandler</p>
        <p>@RestControllerAdvice (partial)</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <pre><code>
@GetMapping(path = &quot;/greet/{id}&quot;,
    produces = &quot;text/plain&quot;)

public String greetPerson(
    @PathVariable(name = &quot;id&quot;) long id) {
        ...
        ...
        ...
    }
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h4>Spring Data JPA</h4>
<p>MicroProfile developers will be comfortable with Quarkus JPA support using Hibernate Object
  Relational Mapping (ORM). Spring developers, have no fear! Quarkus supports commonly used Spring
  Data JPA annotations and types. Table 4 provides a sample of the supported Spring Data JPA API
  features.&nbsp;</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses
  Spring Data JPA repository APIs. The Quarkus<a
    href="https://quarkus.io/guides/spring-data-jpa-guide"
  > Spring Data JPA Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 4: Sample of Supported Spring Data JPA API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>
        <p>Supported Spring Data JPA</p>
        <p>Feature</p>
      </th>
      <th>
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>CrudRepository</p>
      </td>
      <td>
        <pre><code>
public interface PersonSpringRepository
    extends CrudRepository&lt;Person, Long&gt; {
        List&lt;Person&gt; findByAge(int age);
    }
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>Repository</p>
        <p>JpaRepository</p>
        <p>PagingAndSortingRepository</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <pre><code>
public class PersonRepository extends
    Repository&lt;Person, Long&gt; {
        Person save(Person entity);
        Optional&lt;Person&gt; findById(Person entity);
    }
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>Repository fragments</p>
      </td>
      <td>
        <pre><code>
public interface PersonRepository extends JpaRepository&lt;Person, Long&gt;, PersonFragment {
    ...
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>Derived query methods</p>
        <p>&nbsp;</p>
        <h4>&nbsp;</h4>
      </td>
      <td>
        <pre><code>
public interface PersonRepository extends CrudRepository&lt;Person, Long&gt; {
    List&lt;Person&gt; findByName(String name);
    Person findByNameBySsn(String ssn);
    Optional&lt;Person&gt;
    findByNameBySsnIgnoreCase(String ssn);
    Boolean existsBookByYearOfBirthBetween(Integer start, Integer end);
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <h4>User-defined queries</h4>
      </td>
      <td>
        <pre><code>
public interface MovieRepository extends CrudRepository&lt;Movie, Long&gt; {
    Movie findFirstByOrderByDurationDesc();
    @Query(&quot;select m from Movie m where m.rating = ?1&quot;)
    Iterator&lt;Movie&gt; findByRating(String rating);
    @Query(&quot;from Movie where title = ?1&quot;)
    Movie findByTitle(String title);
}
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h2>MicroProfile APIs</h2>
<p>This section describes the MicroProfile Fault Tolerance, Service Health, and Metrics APIs.</p>
<h3>Fault Tolerance</h3>
<p>Fault tolerance patterns are critical to prevent cascading failures and to create a reliable
  microservice architecture. Hystrix circuit-breaking has been a go-to fault tolerance pattern for
  Spring developers for quite a while. However, Hystrix is now in maintenance mode while the
  MicroProfile Fault Tolerance API is in active development and developers have been using it in
  production for years now. Quarkus recommends using MicroProfile Fault Tolerance APIs to improve
  service reliability. Table 5 provides a sample of the MicroProfile Fault Tolerance API features.</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses the
  MicroProfile Fault Tolerance API, the @Timeout and @Fallback features in particular. The Quarkus<a
    href="https://quarkus.io/guides/fault-tolerance-guide"
  > Fault Tolerance Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 5: Sample of MicroProfile Fault Tolerance API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th width="25%">
        <p>MicroProfile Fault Tolerance API</p>
        <p>Feature</p>
      </th width="25%">
      <th>
        <p>Description</p>
      </td>
      <th width="50%">
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>@Asynchronous</p>
      </td>
      <td>
        <p>Execute logic on a separate thread</p>
      </td>
      <td>
        <pre><code>
@Asynchronous
@Retry
public Future&lt;String&gt;
getSalutation() {
    ...
    return future;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Bulkhead</p>
      </td>
      <td>
        <p>Limit number of concurrent requests</p>
      </td>
      <td>
        <pre><code>
@Bulkhead(5)
public void fiveConcurrent() {
    makeRemoteCall(); //...
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@CircuitBreaker</p>
      </td>
      <td>
        <p>Gracefully handle faults and fault recovery</p>
      </td>
      <td>
        <pre><code>
@CircuitBreaker(delay=500
    failureRatio = .75,
    requestVolumeThreshold = 20,
    successThreshold = 5)
@Fallback(fallbackMethod = &quot;fallback&quot;)
public String getSalutation() {
    makeRemoteCall(); //...
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Fallback</p>
      </td>
      <td>
        <p>Alternative logic called upon failure</p>
      </td>
      <td>
        <pre><code>
@Timeout(500) // milliseconds
@Fallback(fallbackMethod = &quot;fallback&quot;)
public String getSalutation() {
    makeRemoteCall(); //...
}
public String fallback() {
    return &quot;hello&quot;;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Retry</p>
      </td>
      <td>
        <p>Retry a request</p>
      </td>
      <td>
        <pre><code>
@Retry(maxRetries=3)
public String getSalutation() {
    makeRemoteCall(); //...
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Timeout</p>
      </td>
      <td>
        <p>Wait period before assuming failure</p>
      </td>
      <td>
        <pre><code>
@Timeout(value = 500 )
@Fallback(fallbackMethod = &quot;fallback&quot;)
public String getSalutation() {
    makeRemoteCall(); //...
}
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h3>Service Health</h3>
<p>Platforms such as Kubernetes use probes to check container health. Spring developers use a custom
  HealthIndicator and Spring Boot Actuator to expose service health to the underlying platform. With
  Quarkus, Spring developers can use the MicroProfile Health API to expose service health. A default
  liveness check is provided, but developers can include custom liveness and readiness checks as
  well. Table 6 provides a sample of the MicroProfile Health API features.</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses the
  MicroProfile Health API to expose application readiness. The Quarkus<a
    href="https://quarkus.io/guides/health-guide"
  > Health Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 6: Sample of MicroProfile Health API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th width="20%">
        <p>MicroProfile Health</p>
        <p>Feature</p>
      </th>
      <th width="30%">
        <p>Description</p>
      </th>
      <th width="50%">
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>@Liveness</p>
      </td>
      <td>
        <p>Platform reboots unhealthy containerized applications</p> &nbsp;
        <p>Endpoint:</p>
        <p>host:8080/health/live</p>
      </td>
      <td>
        <pre><code>
@Liveness</p>
public class MyHC implements HealthCheck {
    public HealthCheckResponse call() {
        ...
        return HealthCheckResponse
    .named(&quot;myHCProbe&quot;)
    .status(ready ? true:false)
    .withData(&quot;mydata&quot;, data)
    .build();
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Readiness</p>
      </td>
      <td>
        <p>Platform does not direct traffic to containerized applications that are not ready</p>
        <p>&nbsp;</p>
        <p>Endpoint:</p>
        <p>host:8080/health/ready</p>
      </td>
      <td>
        <pre><code>
@Readiness</p>
public class MyHC implements HealthCheck {
    public HealthCheckResponse call() {
        ...
        return HealthCheckResponse
    .named(&quot;myHCProbe&quot;)
    .status(live ? true:false)
    .withData(&quot;mydata&quot;, data)
    .build();
}
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h3>Metrics</h3>
<p>Applications expose metrics for operational reasons, such as performance service level agreements
  (SLAs), and non-operational reasons, such as business SLAs. Spring developers typically use Spring
  Boot Actuator and Micrometer to expose metrics. Quarkus uses MicroProfile Metrics to expose base
  (JVM and operating system), Vendor (Quarkus), and application metrics. The MicroProfile Metrics
  API requires implementations to support JavaScript Object Notation (JSON) and OpenMetrics
  (Prometheus) output formats. Table 7 provides a sample of the MicroProfile Metrics API features.</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses the
  MicroProfile Metrics API to expose application metrics. The Quarkus<a
    href="https://quarkus.io/guides/metrics-guide"
  > Metrics Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 7: Sample of MicroProfile Metrics API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th width="20%">
        <p>MicroProfile Metrics</p>
        <p>Feature</p>
      </th>
      <th width="30%">
        <p>Description</p>
      </th>
      <th width="50%">
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>@Counted</p>
      </td>
      <td>
        <p>Denotes a counter which counts the invocations of the annotated object</p>
      </td>
      <td>
        <pre><code>
@Counted(name = &quot;fallbackCounter&quot;,
    displayName = &quot;Fallback Counter&quot;,
    description = &quot;Fallback Counter&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@ConcurrentGauge</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <p>Denotes a gauge which counts the parallel invocations of the annotated object</p>
      </td>
      <td>
        <pre><code>
@ConcurrentGuage(</p>
    name = &quot;fallbackConcurrentGauge&quot;,
    displayName=&quot;Fallback Concurrent&quot;,
    description=&quot;Fallback Concurrent&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Gauge</p>
      </td>
      <td>
        <p>Denotes a gauge, which samples the</p>
        <p>value of the annotated object</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <pre><code>
@Metered(name = &quot;FallbackGauge&quot;,
    displayName=&quot;Fallback Gauge&quot;,
description=&quot;Fallback frequency&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Metered</p>
      </td>
      <td>
        <p>Denotes a meter, which tracks the frequency of invocations of the annotated object</p>
      </td>
      <td>
        <pre><code>
@Metered(name = &quot;MeteredFallback&quot;,
    displayName=&quot;Metered Fallback&quot;,
    description=&quot;Fallback frequency&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Metric</p>
      </td>
      <td>
        <p>An annotation that contains the metadata</p>
        <p>information when requesting a metric to</p>
        <p>be injected or produced</p>
      </td>
      <td>
        <pre><code>
@Metric
@Metered(name = &quot;MeteredFallback&quot;,
    displayName=&quot;Metered Fallback&quot;,
    description=&quot;Fallback frequency&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@Timed</p>
        <p>&nbsp;</p>
      </td>
      <td>
        <p>Denotes a timer, which tracks duration of</p>
        <p>the annotated object</p>
      </td>
      <td>
        <pre><code>
@Timed(name = &quot;TimedFallback&quot;,
    displayName=&quot;Timed Fallback&quot;,
    description=&quot;Time for fallback to complete&quot;)
public String salutationFallback() {
    return fallbackSalutation;
}
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td colspan="3">
        <strong>Metrics Endpoints</strong>
      </td>
    </tr>
    <tr>
      <td>
        <p>Application metrics</p>
      </td>
      <td colspan="2">
        <p>http://localhost:8080/metrics/application</p>
      </td>
    </tr>
    <tr>
      <td>
        <p>Base metrics</p>
      </td>
      <td colspan="2">
        <p>http://localhost:8080/metrics/base</p>
      </td>
    </tr>
    <tr>
      <td>
        <p>Vendor metrics</p>
      </td>
      <td colspan="2">
        <p>http://localhost:8080/metrics/vendor</p>
      </td>
    </tr>
    <tr>
      <td>
        <p>All metrics</p>
      </td>
      <td colspan="2">
        <p>http://localhost:8080/metrics</p>
      </td>
    </tr>
  </tbody>
</table>
<h2>MicroProfile Rest Client</h2>
<p>Microservices often expose RESTful endpoints, requiring a client API to consume a RESTful
  endpoint. Spring developers typically use a RestTemplate to consume RESTful endpoints. Quarkus
  supports the MicroProfile Rest Client API to do the same. Table 8 provides a sample of
  MicroProfile Rest Client API features.</p>
<p>
  The<a href="https://github.com/jclingan/quarkus-spring-microprofile"> example project</a> uses the
  MicroProfile Rest Client API to consume RESTful endpoints. The Quarkus<a
    href="https://quarkus.io/guides/rest-client-guide"
  > Rest Client Guide</a> provides greater detail and additional examples.
</p>
<p><strong>Table 8: Sample of MicroProfile Rest Client API Features</strong></p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th width="20%">
        <p>MicroProfile</p>
        <p>Rest Client</p>
        <p>Feature</p>
      </th>
      <th width="30%">
        <p>Description</p>
      </th>
      <th width="50%">
        <p>Examples</p>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <p>@RegisterRestClient</p>
      </td>
      <td>
        <p>Register a typed Java interface as a REST client</p>
      </td>
      <td>
        <pre><code>
border=&quot;1&quot;</code>
        </pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>@RestClient</p>
      </td>
      <td>
        <p>Decorate instance injection of a typed REST client interface</p>
      </td>
      <td>
        <pre><code>
@Autowired // or @Inject
@RestClient
MyRestClient restClient;
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>Invocation</p>
      </td>
      <td>
        <p>Invoke REST endpoint</p>
      </td>
      <td>
        <pre><code>
System.out.println(
    restClient.getSalutation());
</code>
</pre>
      </td>
    </tr>
    <tr>
      <td>
        <p>mp-rest/url</p>
      </td>
      <td>
        <p>Specify REST endpoint</p>
      </td>
      <td>
        <pre><code>
application.properties:
org.example.MyRestClient/mp-rest/url=http://localhost:8081/myendpoint
</code>
</pre>
      </td>
    </tr>
  </tbody>
</table>
<h2>Summary</h2>
<p>This article provided an overview, primarily for Spring developers, of using Spring APIs and
  MicroProfile APIs with Quarkus. Spring developers can now use some of the APIs they know and love,
  along with MicroProfile APIs, to live-code Java microservices then compile them into a native
  binary format to save hundreds of megabytes of RAM while starting in milliseconds.</p>
<p>
  Note:<a href="https://quarkus.io/guides/"> Quarkus guides</a> provide more detail about Spring and
  MicroProfile API support, and much, much more!
</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-24">
      <div class="row">
        <div class="col-sm-4">
          <img class="img-responsive"
            src="/community/eclipse_newsletter/2019/september/images/john.png" alt="John Clingan"
          />
        </div>
        <div class="col-sm-20">
          <p class="author-name">
            John Clingan
          </p>
          <p>Senior Principal Product Manager at Red Hat, Inc.</p>
          <ul class="author-link list-inline">
            <li><a class="btn btn-xs btn-warning" target="_blank"
              href="https://www.linkedin.com/in/jclingan"
            >Linkedin</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>