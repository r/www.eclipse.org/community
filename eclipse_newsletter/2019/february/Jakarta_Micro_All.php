<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  $App = new App ();
  require_once ('_variables.php');

  // Begin: page-specific settings. Change these.
  $pageTitle = "Java EE, Jakarta EE, MicroProfile, or Maybe All of Them";
  $pageKeywords = "eclipse, newsletter, microprofile, jakarta EE, java EE, February, 2019, open source";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "It seems that more and more enterprise technology is emerging that is based on Java EE. There are a lot of options to choose from, between Java EE (now referred to as Jakarta EE), MicroProfile, and combinations of their APIs. If we look at available application containers, the number of possibilities is even higher. Which platforms, particular standards, and runtimes should enterprise developers base their applications on in year 2019?";

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  $script_name = $App->getScriptName();

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/community/eclipse_newsletter/_includes/_generate_page_article.php");