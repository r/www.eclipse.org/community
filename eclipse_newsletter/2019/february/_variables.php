<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/

  $pageTitle = "Jakarta EE - What's New in 2019?";
  $pageKeywords = "eclipse, newsletter, Jakarta EE, new in 2019, February, Jakarta,Java Community, microprofile, journey, Jakarta EE 8, Adoption, GlassFish 5.1, Jakarta EE 9, Starter";
  $pageAuthor = "Christopher Guindon";
  $pageDescription = "A glimpse into what is new with Jakarta EE for 2019";

  // Make it TRUE if you want the sponsor to be displayed
  $displayNewsletterSponsor = FALSE;

  // Sponsors variables for this month's articles
  $sponsorName = "";
  $sponsorLink = "";
  $sponsorImage = "";

    // Set the breadcrumb title for the parent of sub pages
  $breadcrumbParentTitle = $pageTitle;