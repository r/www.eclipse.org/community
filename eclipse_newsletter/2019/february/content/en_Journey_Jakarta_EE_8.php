<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>At Java One last year, Oracle announced that they had made the monumental decision to open source Java EE and move it to the Eclipse Foundation. As Oracle Code One (the successor conference to Java One) comes around I thought it would be good to reflect on where we are and how far we still have to go.</p>
	<p>For the Payara team this has been a very interesting year. Payara has become strategic members of the Eclipse Foundation in order to drive both MicroProfile and Jakarta EE forward. I personally have become a director of the Eclipse Foundation and many of our team members have become committers and project leads on many of the Jakarta EE projects. As a team, we have had to rapidly become familiar with the Eclipse way of working and in working in a multi-vendor collaborative organisation with committees, paperwork, global conference calls at odd hours, as well as tracking and responding on many different mailing lists. These are the necessary evils that come with organisational collaboration, and at times can be frustrating and seem fruitless. On the flip side, I have also found all the organisations involved are open and welcoming and everyone involved is acting in good faith for the best interests of developers and end users that want a free, open and standards-based platform on which to develop business applications. We have all been feeling our way in this process but things are now gathering pace rapidly.</p>
	<p>As I write this, many of the milestones which seemed a long way off just six months ago are now close to completion.</p>
	<p>The TCK code is now open source and just needs a few supplemental pieces of code to make it through the donation process before we can build and execute. This code will become the basis for testing products like the Payara Platform and ensure they are compatible with Jakarta EE 8. Making this code open source is a huge deal, as this code previously could not be accessed without a legal agreement with Oracle. Having the TCK and platform CTS open source will allow many more vendors to enter the market as well as allow everybody to review and improve the test suites to ensure better compatibility.</p>
	<p>All the GlassFish code has now been donated, and nightly builds of GlassFish are being generated using the Eclipse Foundation's Jenkins Infrastructure. Currently, many of the components outside of core GlassFish e.g. Tyrus etc. are still being pulled from the old Oracle maven repositories but slowly these will be moved over to Eclipse EE4J project artifacts. On October 21st the aim is to have all the EE4J projects configured with CI/CD pipelines on <a href="https://jenkins.eclipse.org/">https://jenkins.eclipse.org/</a>.</p>
	<p>The Payara team have been building many of these Jenkins pipelines for the deadline. By November 5th, all projects hope to have artifacts released to maven and the final release candidate of Eclipse GlassFish 5 can reference these artifacts within its build with the aim of having a Java EE certified Eclipse GlassFish release by December 14th.</p>
	<p>On the business and standards side of all this the Jakarta EE working group has been created and has attracted many organisations to join - outside of what you would think as the usual Java EE vendors. The working group committees have been established and the specification committee has worked hard to create a draft of a new specification process suitable for Jakarta EE and potentially other working groups at the Eclipse Foundation. This draft should be released for community review very soon. Payara, along with other working group members, have committed to funding the working group for the next three years to ensure longevity for Jakarta EE and certainty to the Eclipse Foundation so that they can invest in building up their team to support, engage and drive Jakarta EE forward.</p>
	<p>We are rapidly nearing the end of the transition of Java EE to Jakarta EE and the Eclipse Foundation. Now is the time to look beyond the transition to the future evolution of Jakarta EE. All the building blocks are now slotting into place which will allow Jakarta EE to create new, and evolve existing APIs, so that Jakarta EE can retain its place as the premier open source, open standards, multi-vendor platform for building Enterprise applications now and in the future. That is why Payara has committed to help fund Jakarta EE in the future and why we intend Payara Server and Payara Micro to become Jakarta EE 8 (and in the future, Jakarta EE 9) compatible.</p>
	
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\february\images\journey_jakarta_ee_8_img_1.PNG"
        alt="Steve Millidge" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Steve Millidge<br />
            <a target="_blank" href="https://www.payara.fish/">Payara</a></p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/l33tj4v4">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.payara.fish/author/steve-millidge">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>