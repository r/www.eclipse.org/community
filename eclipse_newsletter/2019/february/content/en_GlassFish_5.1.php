<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<br>	
	<p>I am very excited to bring you some great news. Today Eclipse GlassFish 5.1 has finally been released and available</p>
<p>on Maven Central:</p>
	<ol>
		<li><a href="https://repo1.maven.org/maven2/org/glassfish/main/distributions/glassfish/5.1.0/glassfish-5.1.0.zip">Eclipse GlassFish 5.1 - Full Profile</a></li>
		<li><a href="https://repo1.maven.org/maven2/org/glassfish/main/distributions/web/5.1.0/web-5.1.0.zip">Eclipse GlassFish 5.1 - Web Profile</a></li>
	</ol>
<p>or can be downloaded from Eclipse Foundation website:</p>
	<ol>
		<li><a href="https://www.eclipse.org/downloads/download.php?file=/glassfish/web-5.1.0.zip">Eclipse GlassFish 5.1 - Web Profile</a></li>
		<li><a href="https://www.eclipse.org/downloads/download.php?file=/glassfish/glassfish-5.1.0.zip">Eclipse GlassFish 5.1 - Full Platform</a></li>
	</ol>
<p>A huge milestone has been reached. Eclipse GlassFish 5.1 is a pure Eclipse release. All components formerly supplied by Oracle have been transferred to the Eclipse Foundation from Oracle Java EE repositories, have passed the Eclipse release review, and have been released to Maven Central with new licensing terms. Eclipse GlassFish 5.1 has passed all CTS/TCK tests (run on Oracle infrastructure) and has been certified as Java EE 8 compatible.</p>
<p>CTS tests results (copied from Oracle infra):</p>
<p>The most interesting part however is arguably how the existing specs and APIs in Java EE 8 are going to be evolved and how that brings us to a Jakarta EE 9. It is here that things start to look a bit less clear at the moment. In order to evolve these, at least two things need to happen:</p>
	<ol>
		<li><a href="https://jenkins.eclipse.org/jakartaee-tck/job/oracle-javaeects-publish-reports/6/junit-reports-with-handlebars/testSuitesOverview.html">Full Profile</a></li>
		<li><a href="https://jenkins.eclipse.org/jakartaee-tck/job/oracle-javaeects-publish-reports-web/4/junit-reports-with-handlebars/testSuitesOverview.html">Web Profile</a></li>
	</ol>
<p>This release doesn't contain any new features. The feature set is the same as in Oracle GlassFish 5.0.1. The main goal was to demonstrate that GlassFish and all other components transferred from Oracle to Eclipse are buildable, functional and usable.</p>
<p>Another significant change is the license. It's the first version of GlassFish released under EPL 2.0 + GPL 2.0 with the GNU classpath extension.</p>
<p>And the third change is the modification of Maven coordinates of GlassFish components. To distinguish Jakarta APIs from Java EE APIs we changed their Maven coordinates from javax to jakarta and released new versions. The full list of components used in GlassFish 5.1 can be found <a href="https://wiki.eclipse.org/Eclipse_GlassFish_5.1_Components_Release_Tracker">here</a>.</p>
<p>In addition to delivering the software we have also learned:</p>
	<ol>
		<li>How to use Eclipse Development Process to elect committers, submit projects for release reviews, etc.</li>
		<li>How to use Eclipse build infrastructure to setup build jobs, release components to staging repository and to Maven Central</li>
		<li>How to communicate with different project teams and work together to achieve shared goals</li>
		<li>And much more...</li>
	</ol>
<p>The next step for the Jakarta EE community is to complete the Jakarta EE specification process, create Jakarta EE specifications that correspond to all the Java EE specifications and approve all these specifications through the specification process.  That process will no longer require a Reference Implementation, but it will require at least one Compatible Implementation. We hope the community will ensure that Eclipse GlassFish is Jakarta EE 8 compatible and will remain a compatible implementation as the Jakarta EE specification evolves in the future.</p>
<p>It took us more than a year to deliver this release. A huge amount work has been done and we wouldn't have completed it without community help and support. I would like to say thanks to all people who participated in this release.</p>

<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\february\images\Glassfish_img_1.PNG"
        alt="Dmitry Kornilov" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Dmitry Kornilov<br />
            <a target="_blank" href="https://www.oracle.com/index.html">Oracle</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/m0mus">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://dmitrykornilov.net/">Blog</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>