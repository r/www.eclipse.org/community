<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>The <a href="https://www.eclipse.org/projects/efsp/">Eclipse Foundation Specification Process</a> (EFSP), which extends the <a href="https://www.eclipse.org/projects/dev_process/"> Eclipse Development Process</a> (EDP), defines a blueprint for collaborating on specification development in open source.  </p>
	<p>Committers are the ones who tend to develop most of the content. Committers have the ability to push their own contributions into their project's source code repositories and decide whether or not to accept contributions from others. They are the ones that decide what goes into particular builds, and how content is distributed. Committers hold the real power in open source, and with that awesome power comes awesome responsibility. Committers serve the community as the ringmasters of collaboration.</p>
	<p>Committers are members of what the EFSP calls the specification team, the first community that a committer serves. Committers on the specification team need to get along and ensure that they are collectively working towards the same goals. Collaboration within the specification team and the broader communities starts with transparency.</p>
	<p><strong>Be Transparent.</strong> The entire open source world shares a common definition of transparency. While there is some variety in degree, transparency is pretty simple to understand: specification teams must make sure that everybody can watch and understand what they're doing. To this end, a specification team must use a publicly accessible source code repository that's easy to find, use public facing issue tracking software, post their release plans where the community can find them, and capture meeting minutes in public forums (e.g. mailing lists with archives).</p>
	<p>Individuals and groups engaged in producing compatible implementations form a community. The specification team's committers need to work closely with implementers to ensure that the specification is meeting their needs and that the specification can indeed be implemented. The specification team needs the implementers: the EFSP requires that a version of a specification have a least one compatible implementation (under an open source license) before it can be declared final and official. It's expected that implementation teams will interact regularly (via open channels) with the specification team to provide feedback and ask questions.</p>
	<p>The best thing that a specification team can do to help implementers is to be open.</p>
	<p><strong>Be Open.</strong> For a lot of open source communities, "transparent" and "open" mean the same thing (e.g. "open book"), but the EDP regards these terms as being quite different. For Eclipse projects, "open" means open to new ideas, or open to participation. The rules for participating in an Eclipse open source project must be the same for everybody: it's not enough to just accept a few patches, an Eclipse open source have to be open to new ideas. It's hard to be open. In short, the specification team has to let others in and give up absolute control.</p>
	<p>It's worth noting that representatives from groups developing compatible implementations are very likely committers on the specification team themselves and the specification committee.</p>
	<p>This is the desired state; in fact, it's expected that committers will actively court implementers to contribute and, after demonstrating adequate merit, join the team.</p>
	<p>Specifications have dependencies on other specifications. When one specification "depends-on", "uses", "includes", etc. another specification, it's natural and expected for the specification teams to work together to ensure that their products work together. Specifications that group together other specifications (referred to as "profiles" and "platforms" by the EFSP) provide a coherent overall story and so have a vested interest in ensuring that individual specification teams are working toward common goals. This is, of course, not easy: navigating the concerns of multiple consumers is always a challenge.</p>
	<p>Coordinating the work of multiple specification teams requires collaborative planning.</p>
	<p><strong>Have a plan.</strong> It's easy to lapse into a pattern of just letting development happen, but like any process (especially a specification development process), having some method to the madness is critical. A specification team must make sure that their project employs a development methodology and that somebody owns the process (e.g. a project lead). Having a plan helps developers know where they can contribute the most value and makes it easier for other specification teams, implementers, and users to implement their own plans.</p>
	<p>Specifications that are closely related, or tightly coupled will likely share team members. Again, this is the desired state; specification teams are expected to actively court contributions from their consumers and work to turn those contributors into committers.</p>
	<p>A specification needs implementers, and implementers need users. By extension, therefore, a specification needs users. For users, open source specification development breaks with tradition and opens up the process of developing specifications. Users can, and should, contribute to specifications and-just like with implementers-the specification project team should actively court their engagement and work to turn that engagement into contribution.</p>
	<p>Specification teams must ensure that developers from all communities are welcome.</p>
	<p><strong>Keep the "Playing Field Level".</strong> This doesn't necessarily mean that a project team must let just anybody join the project, but rather that they must ensure that the same set of rules apply to everybody (the playing field may be level, but developers still have to earn your way onto the field). Some projects implement meritocracy, for example, by requiring that developers make some number of contributions to demonstrate that they understand the code, rules, and culture of the project before inviting them to join the team. Specification teams must make sure that the process for adding new developers to their team are well known and that the process is operated transparently (e.g. a public vote).</p>
	<p>It's easy to get overwhelmed when you try to think of managing interactions across all of these communities. In practice, however, there is considerable overlap across these communities and much of the collaboration occurs naturally. But real collaboration requires investments in time and energy from everybody involved. </p>
	<p>The specification team's job equal parts developing specifications, building consensus in the community, and facilitating collaboration. We're all in this together.</p>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2019/january/images/wayne.jpg"
        alt="Wayne Beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Wayne Beaton<br />
            <a target="_blank" href="https://www.eclipse.org/">Eclipse Foundation</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>