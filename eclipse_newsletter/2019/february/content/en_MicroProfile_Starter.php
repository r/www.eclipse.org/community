<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <h3>MicroProfile</h3>
	<p>For those who did not come across Eclipse MicroProfile yet, let me give you a quick introduction. In 2016, a group of vendors and individuals started an initiative to optimize Enterprise Java for a microservices architecture.</p>
	<p>It builds on top of a few Java EE specifications, CDI and JAX-RS mainly, and defines some specifications towards resilience, distributed, reactive and serviceability. Maybe specification is a too heavily weighted word - but there are some standards described so an application can run on any of the runtimes which are provided by the vendors.</p>
	<h3>MicroProfile Starter</h3>
	<p>Although most vendors already have an online tool of some sort to generate a project that incorporates MicroProfile, the MicroProfile Starter has additional features:</p>
		<ul>
		<li>It is not geared towards a runtime of a specific vendor. This will strengthen the image that MicroProfile is a collaborative initiative. It also highlights the standardization factor, that a MicroProfile-based application can be used on any of the implementations.</li>
		<li>It also generates examples for the specifications, getting you a quick start in the process of learning the features of each of these specifications.</li>
		<li>How to communicate with different project teams and work together to achieve shared goals</li>
		<li>And much more...</li>
		</ul>
	<p>So, the MicroProfile Starter is a good starting place if you want to learn some of the features within one of the specifications.</p>
	<h3>Using the Starter</h3>
	<p align="center"><img class="img-responsive" style="max-width: 60%" src="\community\eclipse_newsletter\2019\february\images\microprofile-starter.png"></p>
	<ol>
		<li>Visit <a href="https://start.microprofile.io/">start.microprofile.io.</a></li>
		<li>Enter your groupID and artifactID into the text boxes (or leave default values).</li>
		<li>Choose MicroProfile version from the drop-down menu.</li>
		<li>The server implementations available for the chosen MicroProfile version will appear in the MicroProfile Server drop-down menu. If you don't see an implementation you would like to use, try selecting a different MicroProfile version.</li>
		<li>Choose your Bean Discovery Mode (or leave default value).</li>
		<li>Check the boxes for the specifications for which you want to receive code examples.</li>
		<li>Click the download button to save the generated zipped file.</li>
		</ol>
	<h3>Beta</h3>
	<p>The tool is marked as Beta for the moment but works just fine, The "Beta" notation is there since the application will evolve and expand in the future. Some of the things you will see in later versions include:</p>
	<ul>
		<li>More examples of the different specifications.</li>
		<li>Support for Gradle.</li>
		<li>Other ideas from the community which you can submit on the Github issue page.</li>
		</ul>
	<p>And of course, the tool will also incorporate the new versions, runtimes, and specifications when they become available.</p>
	<h3>Conclusion</h3>
	<p>We hope that the MicroProfile Starter application will be the ideal starting point for your journey with MicroProfile. Give it a try - Payara Micro is included as a MicroProfile Server for most MicroProfile versions - and keep an eye on the tool for the future enhancements.</p>
	<p>Not using Payara Micro yet? Get started here:</p>
	<p><a class="btn btn-small btn-warning" target="_blank" href="https://blog.payara.fish/cs/c/?cta_guid=f1ff138e-af85-4bf9-8edf-cdd4f14055b1&placement_guid=363ebe49-5f9f-44fd-bae1-f7b47d43d873&portal_id=334594&canon=https%3A%2F%2Fblog.payara.fish%2Fmicroprofile-starter-launched&redirect_url=APefjpHZHmL6IgiS2-vTcspkn49pGA7wexXo5SdbbysYTUhHZ0nQQixFIiNWubKGKTMQEVH6zuVHQ8QaGG9jLywpCvuxZA5v08aeDzHJQqaW7VMWdRRR8gRhVg6X0_V2IAsTE6N3tOIyLgDXHeJQvJsVy1A9tR9q6hcmYyzbHmhqMw9xYDRA24hCF1mPGg-a5lyKd8FJzCd7HL0yoExfiIMA6xFlz86a2sGn-7uvc4P8vvsLj6TZ4DGFHDUOLiJ8mYANmqlcCzvX&click=7f556038-3e3b-45c9-8e82-62bff0578b22&hsutk=bddc206a116b7bf3df0e44321d22eb56&pageId=7564292599&__hstc=229474563.bddc206a116b7bf3df0e44321d22eb56.1550488558658.1550488558658.1550488558658.1&__hssc=229474563.2.1550599676531&__hsfp=4241547375">Download Payara Micro</a></p>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community\eclipse_newsletter\2019\february\images\Rudy.PNG"
        alt="Rudy De Busscher " />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Rudy De Busscher <br />
            <a target="_blank" href="https://www.payara.fish/">Payara</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/rdebusscher">Twitter</a></li>
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://blog.payara.fish/author/rudy-de-busscher">Blog</a></li>
          </ul>
        </div>
      </div>
     </div>
   </div>
</div>