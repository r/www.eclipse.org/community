<?php
/*******************************************************************************
 * Copyright (c) 2015, 2016 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Eric Poirier (Eclipse Foundation) - Initial implementation
*    Christopher Guindon (Eclipse Foundation)
*******************************************************************************/
// This file must be included
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
?>

   <h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<p>Now that Jakarta EE's official launch is upon us, you could expect that developers, whether or not they are on a Java EE project, are looking forward to this moment. At least, I certainly am! However, I have spoken to a few acquaintances and colleagues that are currently on a Java EE 7 or Java EE 8 project. What surprised me is that not all of them are actually busing themselves with plans for adopting Jakarta EE. How is that possible and why aren't they? Let's see what their responses were, placed in three categories.</p>
	<br>
	<h4>Jakarta EE...is that really a thing?</h4>
	<p>It may sound unlikely, but some don't even seem to have the knowledge that there is a new platform as a successor for Java EE on the rise. Meanwhile, they may be busing themselves with fixing issues, optimizing their business logic and perform maintenance, instead of planning to adopt the next level of their platform. Of course, this can be a valid situation, but what could they gain if they started adopting Jakarta EE?</p>
	<br>
	<h4>Java EE 7/8 works pretty well for us. Why hurry?</h4>
	<p>Apparently, Java EE 7 or 8 is already such a feature-rich platform for some developers that they are just not in a hurry to adopt Jakarta EE. Although it is a perfectly valid choice when people are content with their current situation, maybe diving into the upcoming features of Jakarta EE will spark their interest in adopting them?</p>
	<br>
	<h4>...I was just planning to adopt a competitor</h4>
	<p>The fact that the focus on the Java EE platform was low for a couple of years has some developers believe that competitors are ahead of the Jakarta EE platform. Although this situation cannot be denied as we look at the current state of the platform, it also cannot be confirmed and is likely just a misinterpretation.</p>
	<p>As we speak, talks are being given at conferences by various developers and spokespeople of the Jakarta EE platform or brand that prove how modern, feature-rich and even cloud-native the platform is. Please consider using their knowledge of the evolution of the platform to your advantage before you make the choice of moving to a competitor.</p>
	<br>
	<h4>What is your plan for adopting Jakarta EE?</h4>
	<p>If you are reading this, I'm curious if you are in one of these situations, or are just completely new to the platform. Therefore, I would like to know how <strong>you</strong> feel about adopting the Jakarta EE platform. Do you have any doubts or pitfalls in the process? Or maybe you are fully into it and want to share the progress you are making? Whatever your situation is, please share your case with us!</p>
	<br>
<div class="bottomitem">
  <h3>About the Author</h3>
  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="\community\eclipse_newsletter\2019\february\images\adopting_jakarta_ee_img_1.PNG"
        alt="Mike Milinkovich" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
            Edwin Derks<br />
            <a target="_blank" href="https://www.linkedin.com/in/edwin-derks/">Independent Developer Advocate for Jakarta EE</a>
            <br>
            <a target="_blank" href="https://ordina-jtech.github.io/">Software Architect</a>
          </p>
          <ul class="author-link list-inline">
          <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/edwinderks">Twitter</a></li>
           <?php echo $og; ?>
          </ul>
        </div>
      </div>
     </div>  
   </div>
</div>