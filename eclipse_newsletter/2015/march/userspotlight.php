<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Stephan Eberle";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2015/march">March 2015</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2015/march/images/stephan.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I'm leading the technical operations of itemis France, the French subsidiary of itemis in Paris. Our mission is to bring model-driven tools, Eclipse technology and itemis expertise and 
					products to the French market and help out on the German market when needed. We drive the Artop and Sphinx projects and continuously implement and contribute new features to both of them. 
					Since 2012, we have also been very active in the development of connected embedded systems and thereby became users of our own methods and tools. The most exiting project has been to design 
					a full smart meter device for Saudi Arabian market. We have used mbeddr to design the embedded device software and Sphinx to build a calibration tool for the smart meter metrology in a 
					model-driven way. In addition to that, we maintain the Eclipse Automotive IDE package and co-lead the EATOP project that provides an implementation of the system engineering standard EAST-ADL 
					based on EMF and Sphinx.</p>
					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I've started to use Eclipse with a strong focus on modeling almost 11 years ago, right after having joined Bosch in Stuttgart. By that time, Bosch had the idea to leverage Eclipse to progressively 
					replace their former company-internal tools used in automotive software engineering by a modern integrated development environment that can be extended and customized according to their needs. 
					Some years later, I got an offer to move to Paris and take a leading role in the development of Eclipse-based design tools supporting the AUTOSAR standard. Out of that, the Artop project and later 
					also the Sphinx project have been created. I joined itemis in 2011 and have continued to develop Artop and Sphinx with a small team of very experienced developers. The Smart meter project gave me 
					the opportunity to get in touch with another and still relatively new piece of Eclipse - mbeddr. Our use case was the first and so far biggest industrial challenge for this tool. We provided a 
					lot of feedback from the ground floor and the itemis team in Stuttgart did a really great job in incorporating it entirely and quickly. This has resulted in a major boost for mbeddr in terms of 
					industrial strength and maturity and contributed to making Eclipse a provider of cutting-edge technologies for the development of embedded systems and IoT devices.</p>
					
			<h3>Name five plugins you use and recommend:</h3>
					<ul>
						<li><a target="_blank" href="http://mbeddr.com/">mbeddr</a>: Not really an Eclipse plug-in as it is based on JetBrains' MPS platform, but still an Eclipse project and available under EPL. Anyone 
					who needs to develop software for some micro-controller or embedded PC device using C programming language MUST have a look at this tool. It's a game-changing technology and provides unparalleled 
					ways to achieve things like comprehensive and modular architectures, software reuse and platform-based development, hardware-independent testing, and incremental integration and commissioning on 
					the target system. Offering all this at close to zero overhead in the resulting C code, mbeddr is a door opener for significant productivity gains and cost reductions in the design of hardware near 
					software with tough resource and real-time constraints.</li>
						<li><a href="https://www.eclipse.org/sphinx/">Sphinx</a>: A production-quality modeling tool platform based on Eclipse and EMF. It enables users to realize powerful model-driven tool chains 
					supporting arbitrary sets of public or company-internal modeling standards. It is true that Sphinx is very much inspired by the needs of car makers, but keep in mind that Sphinx as such is 
					general purpose. You can therefore see it as an excellent opportunity to benefit from the latest advances in terms of model-driven tool development emerging from the automotive industry. 
					In the Mars release for instance, we are about to add many exiting new features to Sphinx such as a scripting capability using dynamic MWE workflows written in Xtend or Java, a very lightweight 
					approach to model validation by just writing Java or Xtend code and providing all required extra information through @Check annotations, an integration of the powerful EMF-IncQuery framework 
					to support super-fast queries and proxy resolution on big models, or a model search capability that is fully integrated with the Eclipse Search UI.</li>
						<li><a target="_blank" href="https://www.artop.org/">Artop</a>: AUTOSAR design tools + open source = Artop; Artop is based on Sphinx and implements the metamodel behind the automotive 
					architecture and design standard AUTOSAR along with many useful services on top of it. We also use a lot of other Eclipse technologies (mostly modeling technologies) in Artop. Just recently, 
					we have leveraged EMF-IncQuery to make queries and resolution of references on large AUTOSAR models up to 50 times faster than before.</li>
						<li><a href="http://eclipse.org/xtend/">Xtend</a>: A very interesting alternative to "traditional" Java development. Provides many powerful but still easy to grasp language extensions which allow for more compact, 
					more elegant and better readable code. Integrates really well with regular Java, JDT and PDT in Eclipse, and also with Maven/Tycho. We use it very heavily for model to model transformation and 
					code generation and also more and more for general purpose programming.</li>
						<li><a href="http://marketplace.eclipse.org/content/anyedit-tools">AnyEdit</a>: Offers quite a lot of handy IDE extensions like sorting, comparison with external file or clipboard content, 
					conversion of line endings, case, html/entities, camel case/underscores, and much more. Basically things that make a developer's life easier and could also have been provided by the Eclipse platform itself.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>I most like spending time with my family and friends, running, indoor and outdoor climbing, and playing piano. With my kids, I have recently built a small robot that plays the tower of hanoi game 
					using some FischerTechnik and a Raspberry Pi. We have used mbeddr to implement the control software for that robot and make it as comprehensive as possible. Finally, we took the robot as an extra 
					show case for mbeddr with us to the booth at the Embedded World 2015 exhibition in Nuremberg that the Eclipse Foundation had organized. That was a lot of fun.</p>
					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="http://fr.linkedin.com/in/seberle">Linkedin</li>
					<li><a target="_blank" href="http://www.xing.com/profile/Stephan_Eberle">Xing</li>
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>