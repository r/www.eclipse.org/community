<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <img
      src="/community/eclipse_newsletter/2015/march/images/artop_logo.jpg"
      alt="" /><br />
    <p>As most of our tech-savvy readers will know, a modern car is
      actually a complex network of around one hundred computers on
      wheels. And in the future, that network will itself be a node on
      wheels in an even more complex network of other cars and further
      road-side and backbone nodes - all of those nodes being
      implemented by different companies. It is obvious, that automotive
      companies need comprehensive software engineering know-how.</p>

    <h2>A bit of history</h2>
    <p>Flashback to the year 2003. Major players in the automotive
      industry recognized that they were facing serious challenges in
      software engineering. At that time, the software that runs in the
      electronic control units (ECU) in the car was often very tightly
      coupled to the hardware and the specific implementation of the
      basic software (the operating system of an ECU). It was often
      really hard to separate the actual control software from the basic
      software. And even then, there was no real standard for the basic
      software (BSW), reducing portability even further.</p>

    <p>
      So major players got together and founded the <a target="_blank"
        href="http://www.autosar.org/">AUTOSAR partnership</a> to
      provide a common standard to address these problems. In more than
      10 years of its existence, AUTOSAR provided a software engineering
      standard for the automotive domain, that standardizes three major
      fields: The basic software that runs the ECUs, resulting in a
      definition of a common "operating system", an exchange format that
      standardizes the description of software engineering artefacts to
      facilitate cross-company exchange and standardized functional
      interfaces.
    </p>

    <p>
      In the beginning, a lot of the business cased argumentation
      focused on the standardization of the BSW, but the tooling
      departments and companies recognized the importance of the common
      exchange model. After some isolated experiments, the first
      companies got together in 2008 to form the "<a target="_blank"
        href="https://www.artop.org/">Artop (AUTOSAR tool platform)</a>"
      community. While AUTOSAR was based on "Cooperate on standards,
      compete on implementation", the Open Source idea was already more
      established on the tooling side and it was recognized that a
      common implementation of the exchange format would be the most
      feasible approach and that <a
        href="http://www.eclipse.org/modeling/emf/">Eclipse EMF</a>
      provides a very good basis for automotive tooling.
    </p>

    <h2>What is Artop?</h2>
    <p>Today, the Artop community is providing a common implementation
      of the complex AUTOSAR-metamodel, which supports all kind of
      artefacts like software architecture, network definitions,
      hardware configurations and even feature-modelling and variant
      specifications. On top of plain EMF, AUTOSAR has defined some
      intricate concepts, such as splitting a model element over several
      files which are also supported by Artop.</p>

    <p>Artop is now being used as the basic platform for a number of
      commercial as well as in-house tools and is a perfect basis for
      tool integrations. Without Artop, the introduction of AUTOSAR
      tooling would have come with a much higher cost for many
      companies.</p>

    <p>Although the Artop members are strong supporters of the Eclipse
      foundation (many are Eclipse members), Artop has to be a separate
      community with restricted access to due the IP regulations of
      AUTOSAR.</p>
    <p>
      Artop also recognized that they had created a rich
      AUTOSAR-independent framework for model management that supported
      a lot of use cases that are not restricted to AUTOSAR alone. Quite
      some effort was spent to factor out the project that is now known
      as the Eclipse project "<a href="https://www.eclipse.org/sphinx/">Sphinx</a>".
      Sphinx itself was already adopted by other projects like EATOP,
      Amalthea and a number of in-house projects and is actively
      maintained by automotive companies.
    </p>

    <p>Artop is one of the reasons why Eclipse has become a strong tool
      platform in the automotive domain. And it is one of the early
      prominent examples of what can be achieved, if you don't only
      cooperate on standardization, but also on implementation.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/march/images/andreas.jpg"
        alt="andreas graf" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Andreas Graf<br />
        <a target="_blank" href="http://www.itemis.com/">itemis AG</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/grafandreas">Twitter</a></li>
        <li><a target="_blank"
          href="http://de.linkedin.com/pub/andreas-graf/b/788/100/en">Linkedin</a></li>
          <!--<li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

