<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <strong>Building an eclipse-based open source development platform,
      extendable by the community</strong>
    <p>
      <i>A major goal of automotive domain is more and more automated
        driving that may even end up driving autonomously. Consequently,
        the complexity of driver assistance systems further increases to
        the point that new hardware architectures like multi- and
        many-core platforms have to be considered. Current development
        tools can no longer handle this complexity. Therefore, upcoming
        development tools need to be more efficient and designed to meet
        future requirements.</i>
    </p>

    <p>
      <img
        src="/community/eclipse_newsletter/2015/march/images/assistance_systems.jpg"
        class="img-responsive" alt="" />
    </p>
    <br />
    <p>
      <i>Assistance systems to exculpate the driver (Source: BOSCH)</i>
    </p>

    <p>While multi-core architectures have already been introduced to
      the development of embedded systems, there is still a need for
      more powerful hardware platforms to meet future challenges. For
      instance, many-core systems provide a further increase in the
      number of cores to reach more computational power for control
      units. However, these architectures bring in additional
      challenges, because of the increasing number of cores causes more
      complex network architectures on a chip. Therefore, compared to
      multi-core systems the underlying architecture (network-on-chip)
      differs significantly. Description capabilities of existing
      hardware as well as software models have to be extended to support
      these new system architectures. Furthermore, introducing extended
      models, new tools and techniques are required to make effectively
      use of the resulting level of parallelism, as well as the
      performance of the new architecture.</p>

    <p>For developing automotive control units, a variety of tools is
      used. Thereby each tool has a specific view on the system or is
      used in one or more particular steps of development. In each case,
      specific information needs to be processed and possibly changed.
      The modified data is often the basis for further development
      steps, such that a chain of tools is formed. For the transition
      from one tool to another, developers must often transfer the data
      manually, due to different data formats.</p>

    <h2>The AMALTHEA Project</h2>
    <p>Therefore, the goal of the EU ITEA 2 AMALTHEA project was to
      reduce this effort. Together with 14 partners from Finland,
      Turkey, and Germany, we developed a platform on basis of Eclipse
      that includes a common data model and offers multi-core support.
      The project started in July 2011 and was successfully completed in
      April 2014.</p>

    <p>Data models of the AMALTHEA Platform contain all the information
      that are necessary for the complete development process, so that
      the data is generated at a central location and can be used and
      exchanged via well-defined interfaces between different tools. The
      data includes both, software and hardware descriptions, which were
      extended to elements for supporting multi-core aspects.</p>

    <p>To be able to also considering variants in automotive systems in
      the development process of a system, in AMALTHEA we developed a
      method for the combined description of software and hardware
      variants. Thereby, we can check dependencies between software and
      hardware components immediately by this method and thus can
      prevent errors during the selection of a specific variant.</p>

    <p>A special focus in the development of the tool chain was on
      providing open interfaces, such that different tools, whether open
      source, commercial or corporate-owned, can be integrated. To
      achieve these goals, we use Eclipse as the basis for the
      development environment, which provides a plug-in mechanism and
      thus an easy integration with other tools.</p>


    <p>
      <a
        href="/community/eclipse_newsletter/2015/march/images/almathea_eclipse.jpg"><img
        src="/community/eclipse_newsletter/2015/march/images/almathea_eclipse.jpg"
        class="img-responsive" alt="" /></a>
    </p>
    <br />

    <p>The picture above depicts the different AMALTHEA models and
      open-source basic tools, which have been developed during the
      project. These tools were used to build up a tool chain, which is
      100% open-source without any commercial tools. This allows
      everyone to try out an AMALTHEA tool chain, which is built with
      the AMALTHEA Platform. You can find more information about the
      AMALTHEA Common Platform Models in the Help pages of the AMALTHEA
      Platform.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/march/images/tool_platform.png"><img
        src="/community/eclipse_newsletter/2015/march/images/tool_platform.png"
        class="img-responsive" alt="" /></a>
    </p>
    <br />


    <h2>What's next?</h2>
    <p>The next step to support not only embedded multi- but also
      many-core system development requires further extensions as well
      as enhancements of the AMALTHEA common data model and development
      methods. Therefore, the new AMALTHEA4public project targets to
      provide both a methodology and a platform for the model-based
      development of embedded multi- and many-core systems.
      AMALTHEA4public will benefit from prior publicly funded projects
      by integrating their outcomes into the resulting development
      environment. Of course, these project results might need adaption
      to fit to our overall goals. For instance, we will base our work
      on the open-source development platform of the 2014 successfully
      completed project AMALTHEA. However, this platform will need to be
      extended to include not only methods for systems engineering, but
      also for verification and test generation as well as analyzes of
      product lines, which are issues focused by AMALTHEA4public. A
      further important aspect related to these methods is the platform
      extension for a continuous support of many-core systems.</p>

    <p>Although initial focus of AMALTHEA4public is the development of
      embedded systems for automotive industry, results will not be
      limited thereto. To ensure this goal, capabilities and functions
      of the resulting platform will be demonstrated using real world
      applications not only from the automotive industry, but also from
      further domains like ICT and automation.</p>

    <p>The EU ITEA2 AMALTHEA4public project has started in September
      2014 and involves 21 partners from Germany, Sweden, Spain and
      Turkey. These partners cover nearly all steps of the value chain,
      i.e. automotive suppliers, tool vendors, consulting and research.
      In addition, an Advisory Board has been established, including
      amongst others different vehicle manufacturers to evaluate and
      support the transfer to industry results from a user perspective.</p>
    <p>The AMALTHEA4public consortium will be supported by its partner
      Eclipse Foundation Europe GmbH to publish the results as
      open-source Eclipse project and form a community around the
      project. For this purpose, AMALHTEA4public includes an extra work
      package “Maintenance and Community”. Goal of this work package is
      passing the three phases required to become an Eclipse project:</p>

    <ol>
      <li>Project proposal and creation</li>
      <li>Incubation phase</li>
      <li>Graduating from incubation</li>
    </ol>

    <p>Tackling the route to an Eclipse project, AMALTHEA4public
      consortium aims to ensure long-term maintenance and development of
      the platform. Finally, the platform shall be established as a
      de-facto standard for the development of (automotive) embedded
      multi- and many-core systems.</p>

    <h2>Try it out and give feedback</h2>
    <p>In addition to the feedback from the Advisory Board we want to
      provide the results for a wide range of users, therefore we
      published the development environment under the Eclipse Public
      License. In April 2014, we published Version 1.0 of the platform
      on the AMALTHEA Homepage (www.amalthea-project.org), which
      includes all results of the AMALTHEA Project. In order to get more
      Feedback we also integrate a forum to discuss topics and
      realizations of the platform and its models.</p>

    <h3>Links</h3>
    <ul>
      <li><a target="_blank"
        href="http://www.amalthea-project.org/index.php/downloads">Platform
          download</a></li>
      <li><a target="_blank"
        href="http://www.amalthea-project.org/index.php/forum">AMALTHEA
          Forum</a></li>
      <li><a target="_blank"
        href="https://itea3.org/project/amalthea.html">AMALTHEA Itea 2
          website</a></li>
      <li><a target="_blank"
        href="https://itea3.org/project/amalthea4public.html">AMALTHEA4public
          Itea 2 website</a></li>
    </ul>


    <img
      src="/community/eclipse_newsletter/2015/march/images/fmer_germany.jpg"
      alt="" /><img
      src="/community/eclipse_newsletter/2015/march/images/itea2.jpg"
      width="200" alt="" />


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/march/images/christopher-brink.jpg"
        width="90" alt="christopher brink" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Christopher Brink<br />
        <a target="_blank" href="http://www.uni-paderborn.de/en/">Heinz
          Nixdorf Institute, <br>
        <br>University of Paderborn
        </a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Blog</a></li>-->
        <li><a target="_blank"
          href="https://www.xing.com/profile/Christopher_Brink2">Xing</a></li>
        <!--<li><a target="_blank" href="https://www.linkedin.com/pub/max-rydahl-andersen">Lindedin</a></li>
            <?php echo $pageTitle; ?>og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/march/images/passbild.png"
        width="90" alt="jan jatzkowski" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jan Jatzkowski<br />
        <a target="_blank" href="http://www.uni-paderborn.de/en/">C-Lab,
          University of Paderborn</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Blog</a></li>-->
        <li><a target="_blank"
          href="https://www.xing.com/profile/Jan_Jatzkowski2">Xing</a></li>
        <!--<li><a target="_blank" href="https://www.linkedin.com/pub/max-rydahl-andersen">Lindedin</a></li>
            $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


