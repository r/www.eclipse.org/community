<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <img
      src="/community/eclipse_newsletter/2015/march/images/eclipse_automotive.png" alt="" /><br />
    <br />
    <p>
      If you have been following the <a
        href="http://eclipse.org/membership/">Eclipse memberships</a>,
      you might have noticed that a lot of automotive companies have
      become members in the past few years. Why is that? Automotive
      engineering has changed a lot in the last decade. Most of the
      innovation today is in the software of the vehicle, so software
      engineering is a major part in the development process. In the
      early days of the shift to software, development departments often
      claimed that their core competencies where in the development of a
      function, and not in the tooling. So some tried to use only
      "standard" COTS tools, but that approach is limited. As soon as
      you develop new innovative functions with new innovative methods,
      standard tools will lag behind and you will need something
      specific to support your new ideas and methods. Custom tooling
      actually is an important expertise and Eclipse is the perfect
      platform for both developing custom tooling as well as integrating
      with commercial tools.
    </p>

    <h2>Key Eclipse technologies</h2>
    <p>Eclipse provides key technologies for building tooling for the
      automotive software domain. Modern electronic control unit (ECU)
      development is a complex activity that requires a lot of different
      artifacts like software architecture, network definition,
      diagnosis information, and basic software configuration.</p>

    <p>The Eclipse Modeling provides a rich infrastructure to manage the
      software engineering data. Here are some of the main projects and
      frameworks used for automotive:</p>
    <ul>
      <li><a href="http://www.eclipse.org/modeling/emf/">Eclipse
          Modeling Framework (EMF)</a>: provides a very good basis to
        implement the data model for many of those artefacts and
        integrate them with existing systems. This has been recognized
        by the Artop project, that provides an implementation of the
        AUTOSAR meta-model based on EMF. It also provides easy
        integration of any other data models that are based on XML for
        example. Standard formats like Fibex can be easily used with the
        powerful frameworks that build on EMF, as well as in-house data
        models that are being developed from scratch.</li>
      <li><a href="https://www.eclipse.org/sphinx/">Sphinx</a>: provides
        a lot of helpful model management functions on top of that.</li>
      <li><a href="https://eclipse.org/Xtext/">Xtext</a>: is well
        integrated with EMF and provide means to process existing text
        based formats and adds a lot of functionality to support
        comfortable editing of those configuration files to the end
        user.</li>
      <li><a href="https://eclipse.org/cdo/">CDO</a>: allows EMF models
        to be easily stored in 3-tier architectures with commercial
        SQL-databases, if required by company IT policy.</li>
      <li>A number of other data backends (EMFStore, Teneo, etc.).</li>
    </ul>

    <p>
      On top of that, there is a variety of projects for creating great
      user interfaces for these models. Xtext is very popular, since its
      textual domain specific languages approach resonates well with a
      technical audience. <a href="http://eclipse.org/ecp/emfforms/">EMF
        Forms</a>, <a href="https://eclipse.org/eef/">Extended Editing
        Framework (EEF)</a> and others cater for the needs of the
      form-based target audience and with <a
        href="http://eclipse.org/rap/">Remote Application Platform (RAP)</a>
      you can even take your application to the web. And the latest
      addition to the family, the <a href="https://eclipse.org/sirius/">Sirius
        project</a>, reduces the effort for creating graphical/diagram
      editors significantly.
    </p>

    <p>
      Well integrated with that are a number of Eclipse projects to
      process that data. For the actual ECU, the data has to be
      transformed in formats like AUTOSAR, configuration files, or
      actual C code. Users can choose from a variety of Model-To-Model-
      and Model-To-Text transformation tools like QVTO, <a
        href="https://eclipse.org/atl/">ATL</a>, <a
        href="https://eclipse.org/acceleo/">Acceleo</a>, <a
        href="http://eclipse.org/xtend/">Xtend</a>, etc. that are well
      suited for the development of specific tools and tool
      integrations. The projects listed are only a fragment of the
      entire rich infrastructure available.
    </p>

    <h2>Why build on Eclipse?</h2>
    <p>The business case for building automotive tools on the Eclipse
      platform is obvious: There is hardly any other infrastructure
      project that provides so many components that are essential for
      the implementation of automotive tools. The competitive advantage
      is not in building that infrastructure, but in the software
      engineering innovations built on top of that. Instead of investing
      in proprietary tools to store, process, compare, edit and
      integrate engineering data, the budget can be spent on the parts
      that add value to your tool chain.</p>

    <p>
      Apart from the technical advantages, the Eclipse Foundation and <a
        href="https://www.eclipse.org/legal/epl-v10.html">Eclipse Public
        License</a> are also key factors. The automotive industry (as
      many other industry) pay attention to the legal aspects of using
      open software. The Foundation with its IP process and the EPL make
      the usage of Eclipse commercially attractive. That also has the
      effect that we see more and more automotive companies contributing
      to the Eclipse projects directly or indirectly.
    </p>

    <p>A lot of in-house tools and tool chain integration in the
      automotive domain are already based on Eclipse. The current strong
      activities in the Eclipse platform projects for the Mars release
      show that Eclipse is a stable platform as well as actively
      maintained. And the Eclipse Foundation is an attractive
      organization for initiatives that want to cooperate in a
      well-established legal framework - the automotive openMDM working
      group for measured data management is the latest example.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/march/images/andreas.jpg"
        alt="andreas graf" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Andreas Graf<br />
        <a target="_blank" href="http://www.itemis.com/">itemis AG</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/grafandreas">Twitter</a></li>
        <li><a target="_blank"
          href="http://de.linkedin.com/pub/andreas-graf/b/788/100/en">Linkedin</a></li>
        <!--<li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

