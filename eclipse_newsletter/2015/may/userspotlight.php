<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Sebastian Zarnekow";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2015/may">May 2015</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2015/may/images/szarnekow.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I work as a Software Engineer and Consultant for itemis in Kiel, a nice place to live and work near the Baltic Sea, in Northern Germany. Here, I'm part of a great team that works on various Eclipse 
					technologies, primarily <a target="_blank" href="http://xtext.org">Xtext</a> and <a target="_blank" href="http://xtend-lang.org">Xtend</a>. These are by far the most exciting technologies that I've built 
					so far. Xtext is a framework to develop domain specific languages and even grown-up programming languages. Xtend started as a language to implement code generators and evolved to a very powerful language 
					that embraces functional programming on the Java virtual machine. The kicker is that Xtend is built with Xtext and we use Xtend nowadays to develop Xtext itself. It's one of these dogfooding projects that 
					passionate developers often strive for.</p> 
					<p>Besides working on the technology itself, I help customers adopt these powerful tools and tailor them to their specific needs. It's always great to work together 
					with users to unleash the full power of different technologies and frameworks. Other than that, I speak at conferences to spread the word further so that even more developers will benefit from our tools.</p>
					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I used one of the first versions of Eclipse back in 2002, I guess. At that time, we usually used Borland's JBuilder which was by far the clunkiest editor one could imagine. Back then, it was really impressive 
					to see how the Eclipse IDE matured with each release into one of the best development tools of that time. Before I joined itemis, almost 7 years ago, I had a short intermezzo with native Windows development. 
					Even in these dark ages, I used custom Eclipse plugins to improve the development experience for our team. We used code generation and Xtext was a natural choice to sport that approach. In 2008, I joined itemis 
					and eventually became an Eclipse committer. Since then, I've used the IDE actively, while also trying to improve it.</p>			
									
			<h3>What features are you most looking forward to in Eclipse Mars?</h3>
					<ul>
						<li><b>Automated Error Reporting</b>: I really like the quality initiative for Eclipse Mars. The automated error reporting is a crucial part of that from a plugin developer's perspective. Nothing is more 
					important than user feedback. Thanks to the error reporting, users have a seamless way to provide feedback if something's not working as expected. I think this is really one of the most valuable additions to 
					Eclipse, especially in the testing phase before the release. All the feedback can be used to deliver a rock solid version for Mars.</li>
						<li><b>Eclipse Installer</b>: The pluggable nature of Eclipse is both a strength and a weakness at once. It was always cumbersome to provide a consistent IDE and still allow the installation of custom user 
					specific plugins. The Eclipse installer and the underlying Oomph technology really improves on that. It has never been easier to keep an up-to-date installation with custom preferences applied. Specifically, 
					the shared user specific settings across different installations are a real time-saver.</li>
						<li><b>Java Development Tools with Improved Null Analysis</b>: The Eclipse JDT is already one of the most powerful tools for Java developers out there, but with Eclipse Mars, it's going to ship with a great 
					enhancement. Earlier versions already had support for null pointer analysis, but so far this was limited to the local code base. Eclipse Mars will also support sophisticated null analysis for external libraries. 
					I'm really looking forward to the enhanced compiler feedback as it will also improve the quality of the code that every Java developer writes.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>In my spare time, I try to do something completely different to keep a proper balance. It's not always prone to success, but as often as possible I spend time with friends and family. Also I like to run. This is 
					the time when I embrace the quiet of nature. Besides sports, I have a passion for good food and cooking. I love to try new recipes or to improvise in the kitchen. For me, that's a kind of creativity that I 
					really embrace.</p>
					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://twitter.com/szarnekow">Twitter @szarnekow</li>
					<li><a target="_blank" href="https://plus.google.com/+SebastianZarnekow/">Google+</li>
					<li><a target="_blank" href="http://zarnekow.blogspot.com">Blog</li>
					<li><a target="_blank" href="https://github.com/szarnekow">GitHub</li> 
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>