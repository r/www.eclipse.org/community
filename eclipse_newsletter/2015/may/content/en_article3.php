<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>
      As many already know <a href="https://eclipse.org/m2e">M2Eclipse</a>
      is the official <a target="_blank" href="https://maven.apache.org">Apache
        Maven</a> integration for Eclipse. We have been working on
      M2Eclipse for over 10 years now: we made our first commit in
      December, 2005 at Codehaus and brought M2Eclipse to the Eclipse
      Foundation with Igor Fedorenko as the lead in November, 2010.
      Since then we&#39;ve had six major releases and have been a
      standard part of the Eclipse release train since Indigo in 2011.
    </p>

    <p>Historically, trying to integrate Maven with Eclipse has been
      quite a challenge. We have Eclipse being an OSGi-based system,
      having its own internal builders, having its own
      classloader/classpath management, and using JDT for compilation
      with its own way of tracking incremental build state. Over time we
      have made adjustments on the Maven side, patched and collaborated
      with existing tools, have created new APIs to try and bridge the
      gap and for the most part I think we&#39;ve succeeded.</p>

    <p>
      A critical integration path is how existing Maven plugins work
      inside M2Eclipse. This is actually more challenging than
      integrating Maven itself because there are literally thousands of
      Maven plugins that work in all sorts of weird and wonderful ways.
      Not all Maven plugins are created equal, and many of them do not
      play very well inside an embedded, incremental environment like
      Eclipse. We have tried to make Maven plugin integration easier in
      M2Eclipse by creating the <a target="_blank"
        href="https://github.com/eclipse/m2e-core/tree/master/org.eclipse.m2e.core/src/org/eclipse/m2e/core/project/configurator">Configurator
        API</a> and the <a target="_blank"
        href="https://github.com/takari/io.takari.incrementalbuild">Takari
        Incremental API</a>.
    </p>

    <p>
      The Configurator API is a general mechanism for configuring
      Eclipse projects in the workspace from configuration that is
      present in a Maven POM. A simple example of how this works is the
      way in which we set the
      <code>source/target</code>
      compiler options for JDT in Eclipse from the configuration in the
      <code>maven-compiler-plugin</code>
      . Any M2Eclipse extension, or what we call connectors, can use
      this API to manipulate the state of the Eclipse project and we
      have <a target="_blank"
        href="https://github.com/takari/m2e-discovery-catalog">connectors
        for many tools like Antlr, JavaCC, PMD, and Checkstyle</a>.
    </p>

    <p>
      The <a target="_blank"
        href="https://github.com/takari/io.takari.incrementalbuild">Takari
        Incremental API</a> is an attempt to alleviate the burden of
      having to write a connector for many classes of Maven plugins. If
      you are generating content like sources, resources from
      annotations, JARs, or anything similar to what happens in the
      default Maven lifecycle and you use the Takari Incremental API
      then you don&#39;t have to write a connector to integrate with
      M2Eclipse. If you adhere to the Takari Incremental API you can use
      the <a target="_blank"
        href="https://repository.takari.io/content/sites/m2e.extras/takari-team/0.1.0/N/LATEST/">Takari
        TEAM Connector</a> and the integration with M2Eclipse will be
      handled for you. For an example of how the Takari Incremental API
      is used, you can take a look at the <a target="_blank"
        href="https://github.com/takari/takari-lifecycle">Takari
        Lifecycle Plugin</a>. We hope that over time developers will use
      the Takari Incremental API for their Maven plugins which will
      provide better performance and the best integration with
      M2Eclipse.
    </p>

    <p>
      We are also very proud of our development support for Maven core,
      Maven plugins, and Maven extensions. We have been working on this
      support for nearly 8 years and it&#39;s no trivial effort. We can
      easily run/debug through the entire graph of M2Eclipse
      dependencies from within the Eclipse workspace. We can develop
      Maven plugins or Maven extensions from within the workspace
      running with the version of Maven in the workspace. It means that
      we never have to install anything in the local Maven repository in
      order to work on new features of Maven, new plugins or extensions.
      It all just works inside M2Eclipse with no need for remote
      debugging. If you develop Maven plugins then I highly recommend
      you take a look at the video we put together demonstrating these
      development capabilities. It really is a huge time saver and
      alleviates most of the frustration while trying to develop for
      Maven.
    </p>

    <p>
      <iframe width="640" height="360"
        src="https://www.youtube.com/embed/mM111lL8cro"
        allowfullscreen></iframe>
    </p>
    <br />

    <p>
      Along the way we&#39;ve gained some really great core
      contributors. Working on M2Eclipse is not easy as you have to
      understand how Eclipse works and how Maven works. Two fairly
      sizable ecosystems in their own right, and getting them to work
      together is not always straight forward. <a target="_blank"
        href="https://twitter.com/fbricon">Fred Bricon</a> has been with
      us for many years, is the lead of the <a
        href="https://www.eclipse.org/m2e-wtp/">m2e-wtp</a> component,
      and works on integrating M2Eclipse and WTP into <a target="_blank"
        href="http://tools.jboss.org">JBoss Tools</a> for Red Hat. Fred
      has made some great usability improvements like radically speeding
      up project imports, and automatically keeping project
      configurations up-to-date so that users don&#39;t have to
      manually. Anton Tanasenko joined us last year and has made
      numerous improvements including revamping the Maven plugin
      configuration content assist in the POM editor.
    </p>

    <p>
      We also have some exciting changes happening in Maven and some of
      those changes have made their way into some experiments on the
      M2Eclipse side. Recently <a target="_blank"
        href="http://takari.io">Takari</a> released <a target="_blank"
        href="https://github.com/takari/polyglot-maven">Polyglot for
        Maven</a> with DSLs for Ruby, Groovy, Scala, Clojure and POM
      markups for YAML and Atom. Fred Bricon is excited about Polyglot
      for Maven and <a target="_blank"
        href="https://github.com/jbosstools/m2e-polyglot-poc">created a
        prototype for integrating it with M2Eclipse</a>. The <a
        target="_blank" href="https://github.com/jruby/jruby">JRuby</a>
      team also recently switched over to using Polyglot for Maven with
      the Ruby DSL! You can check that out on <a target="_blank"
        href="https://github.com/jruby/jruby">JRuby master at Github</a>.
    </p>

    <p>
      We are also excited about the new <a
        href="https://projects.eclipse.org/projects/tools.andmore">Eclipse
        Andmore</a> which integrates the <a target="_blank"
        href="http://rgladwell.github.io/m2e-android/">M2Eclise Android</a>
      connector and the <a target="_blank"
        href="https://github.com/simpligility/android-maven-plugin">Android
        Maven Plugin</a>. Dave Carver, Ricardo Gladwell, and Manfred
      Moser are really doing admirable work on their respective projects
      and we&#39;re starting to see it all come together in the Andmore
      project which is great to see.
    </p>

    <p>
      If you ever have any questions, or want to know more about the
      M2Eclipse project please visit the <a
        href="http://eclipse.org/m2e/m2e-community.html">community page</a>
      on our website!
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/may/images/jasonvanzyl.jpg"
        alt="jasonvanzyl" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jason van Zyl<br />
        <a target="_blank" href="http://takari.io">Takari, Inc.</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/jvanzyl">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/113247990055413254822/posts">Google
            +</a></li>
        <!--<li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">

    <div class="sideitem">
      <h6>About the Author</h6>
      <img class="author-picture"
        src="/community/eclipse_newsletter/2015/may/images/jasonvanzyl.jpg"
        alt="jasonvanzyl" height="90" />
      <p class="author-name">
        Jason van Zyl<br />
        <a target="_blank" href="http://takari.io">Takari, Inc.</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/jvanzyl">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/113247990055413254822/posts">Google
            +</a></li>
        <!--<li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
    </div>
  </div>
</div>
