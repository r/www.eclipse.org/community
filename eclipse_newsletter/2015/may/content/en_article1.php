<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <img
      src="/community/eclipse_newsletter/2015/may/images/gradlephant.png"
      width="200" alt="" /><br />

    <p>
      Automated building of software has become an essential part of
      Continuous Integration (CI) and Continuous Delivery (CD) of
      software. CI and CD have become vitally important as SaaS software
      and omnichannel requirements such as Android, IOS, responsive web,
      kiosk, API and many other platforms, channels, and business become
      a normal part of the software industry. Gone are the days when
      software could be delivered in 6 month waterfall increments and
      rewritten in massive "2.0" efforts. In addition, the emergence of
      containers like <a target="_blank" href="https://www.docker.com/">Docker</a>
      enable more language platform diversity as the developer begins to
      control their stack and just deliver microservices.
    </p>

    <p>In short, the modern software environment is based on agile
      sprints and fragmentation across lots of channels, platforms,
      languages, and business contexts. The demand is ever increasing in
      size and complexity while continuously increasing the frequency of
      shipping high quality code.</p>

    <p>
      The ever increasing complexity of code has led to a great deal of
      unhappiness in the software industry including broken builds, bug
      regressions, code freezes, and deat hmarches. <a target="_blank"
        href="https://gradle.org/">Gradle</a>'s mission is to reverse
      this negative trend, hence our slogan: Build Happiness.
    </p>

    <p>In modern software, the build (the key step of compiling,
      assembling, testing, integrating and packaging software for
      delivery) has expanded to include a wider scope of tasks like
      automated testing and integration and in some cases even
      deployment. Thus the requirements for a build system have become
      increasingly steep.</p>

    <p>Modern build systems must now manage multiple contexts:</p>

    <ul>
      <li><b>Local Builds</b>: Software being built locally to verify
        the code changes before sharing them with the rest of the team.</li>

      <li><b>Continuous Integration Server Builds</b>: Software being
        built remotely on continuous integration servers to verify that
        the incoming changes are not introducing regressions.</li>

      <li><b>Staging Server Builds</b>: Software being built remotely on
        staging servers to ensure the quality gates are met before the
        software enters the next phase of the release cycle.</li>
    </ul>

    <p>Covering such a wide and very context-sensitive set of
      requirements with a single source of build information is a key
      requirement for today's build systems. Besides being able to
      describe these requirements in an expressive and concise manner,
      the build must also execute quickly, be highly reproducible, and
      provide progress and error information.</p>

    <p>Most build systems were built before the complexity and sheer
      size of today's code bases and are not designed with polyglot,
      multi-platform, multi-project, multi-channel, lean/agile mindsets.
      Gradle is the next-generation build system designed to meet the
      requirements of industry trends like Mobile First, Continuous
      Delivery, and Polyglot unlike most of the other build systems.
      Furthermore, Gradle is backed by Gradle Inc., as well as a
      significant industry alliance composed of some of the best
      software engineering companies in the world including Linkedin,
      Netflix, Google, and Unity3D.</p>

    <p>This new industry has increased both the complexity and the value
      of defining, implementing, and running builds; the result is the
      ascendence of the modern Build Master. He or she might or might
      not have the background of a developer, but this person will
      definitely be at home when it comes to automation, integration,
      and delivery. In non-trivial setups, the developer just consumes
      builds provided by the build master. The developer needs to be
      able to run builds and these builds must run fast, be
      reproducible, and provide valuable progress and error reporting.</p>

    <p>Since today's software development happens primarily in IDEs, it
      is not surprising that developers expect their builds to be
      integrated into their IDE. They want the IDE project setup to be
      derived from the build, they want to run their build from within
      the IDE, they want to see build progress and potential build
      errors in the IDE, and they want to make module-specific changes
      to the build, like adjusting dependency versions, meta
      information, and so on. Build masters also want IDE integration so
      they can write, maintain, and extend their builds from within the
      IDE. They need code assistance, debugging support, documentation,
      and more to keep a grip on their builds.</p>

    <p>
      This is where Gradle, Eclipse, and <a
        href="https://projects.eclipse.org/projects/tools.buildship">Buildship</a>
      come into play. Gradle is the strongest build system on the market
      for demanding enterprise environments. Eclipse continues to be the
      strongest IDE on the market for enterprise software development.
      Buildship connects the Eclipse IDE world with the Gradle Build
      System world.
    </p>

    <p>Buildship's vision is to make Eclipse more powerful by increasing
      what can be done from within Eclipse. Imagine not having to go to
      the command line to run and debug builds, not having to manually
      start and stop servers used for integration testing, and not
      having to wade through textual progress logs. Buildship aims to
      give you all the views, wizards, and knobs to control your build
      from within Eclipse - and to give you all this in a deeply
      integrated fashion.</p>

    <p>Buildship 1.0 will be released together with Eclipse Mars in
      June. This first official version of Buildship focuses on
      developers who run Gradle builds. Project Import Wizard, Task
      View, Task and Test Execution, and Build Execution Progress View
      are all part of Buildship 1.0.</p>

    <p>Future versions of Buildship will give you more insight into your
      build through visual and navigable information about the
      dependencies, plugins, components, and more. Buildship will also
      address the needs of build masters, providing assistance in
      writing and maintaining Gradle builds, plugins, and distributions.</p>

    <p>Buildship 1.0 is only the beginning of this journey. With each
      future version of Buildship and Eclipse, our vision will become
      more reality. And build happiness will propagate.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/may/images/etienne.jpg"
        width="90" alt="etienne studer" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Etienne Studer<br />
        <a target="_blank" href="https://gradle.org/">Gradle Inc.</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/etiennestuder">Twitter</a></li>
        <!--<li><a target="_blank" href=""></a></li>
            <li><a target="_blank" href=""></a></li>
            $og-->
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/may/images/miko.jpg"
        width="90" alt="miko matsumura" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Miko Matsumura<br />
        <a target="_blank" href="https://gradle.org/">Gradle Inc.</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/mikojava">Twitter</a></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

