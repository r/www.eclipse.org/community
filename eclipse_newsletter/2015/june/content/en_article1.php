<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Eclipse Mars, the tenth simultaneous release of Eclipse open
      source projects, was delivered to the community by the Eclipse
      project teams on June 24, 2015. The Mars release is the expression
      of the effort of 379 committers and 351 contributors working on
      approximately 65 million lines of code for seventy nine separate
      open source projects.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/june/images/mars_stats.png"><img
        src="/community/eclipse_newsletter/2015/june/images/mars_stats.png"
        alt="graph release metrics" class="img-responsive" /></a>
    </p>
    <br />

    <p>For various reasons, four projects dropped off this year, but
      seven new projects joined, resulting in net three new projects
      added over last year's Luna release. The Mars release welcomes
      SWTBot, RCP Testing Tool, e(fx)clipse, Thym Cordova Tools, Trace
      Compass, Oomph, and Lua Development Tools as first time
      participants in the simultaneous release.</p>
    <p>The Mars release includes a lot of updates to the Eclipse
      Platform. Significant work went into improving the fidelity of the
      the Dark Theme; these improvements drove improved support for
      general styling via CSS, and new icons that render much better
      against the dark background (and other colours). For Linux users,
      GTK3 support is much improved, and for Mac users, the application
      layout is now standard and ready to be dropped into the
      Applications folder. The Perspective customization dialog, which
      dropped out of the 4.x stream, is back, and a nested/hierarchical
      view of projects is now supported by the Project Explorer. With
      Mars, all Eclipse packages are configured to automatically
      periodically check for updates; the use of improved compression
      techniques vastly improves the performance and footprint of the
      update process.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/june/images/dark_theme_new_pde.png"><img
        src="/community/eclipse_newsletter/2015/june/images/dark_theme_new_pde.png"
        alt="dark theme" class="img-responsive" /></a>
    </p>
    <p>Dark theme improvements in the Plug-in Editor.</p>

    <p>For Java developers, Eclipse Mars includes numerous improvements
      for Java 8 and lambda support, including several new quick assists
      (e.g. "Add inferred lambda parameter types" and the ability to
      convert method references to and from lambda expressions). The
      performance of the Mars Eclipse Compiler for Java (ECJ) performs
      significantly better than the Luna release on generics-heavy code.
      Java developers will also benefit from improved flow analysis for
      loops and a new code formatter. The recommendations engine, which
      provides content assistance based on common usage patterns is now
      turned on by default, and features new subtype aware constructor
      and types completion.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/june/images/convert-to-method-reference.png"><img
        src="/community/eclipse_newsletter/2015/june/images/convert-to-method-reference.png"
        alt="quick fixes" class="img-responsive" /></a>
    </p>
    <p>New Quick Fixes include the ability to convert to
      and from a method reference.</p>

    <p>Though not strictly part of the Mars release, a technology
      preview of Java 9 support is available in the Eclipse Marketplace.</p>
    <p>The Mars release also equips open source project developers with
    some important tools that will help improve their Eclipse software.
    The new Error Reporter helps users easily provide feedback to
    Eclipse project teams when their environment encounters an unhandled
    or unexpected error. The Error Reporter manifests a very slick user
    experience that stays out of the user's way, inconspicuously opening
    a tray notification in the lower right hand corner of the screen
    that is equally easy to use to report an error or just ignore on
    those occasions when the user's train of thought just can't be
    interrupted. Users don't need to create an account, or provide any
    identifying information to make the report: it's quick and easy, and
    immensely helpful for our open source project developers.
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/june/images/error_reporter.png"><img
        src="/community/eclipse_newsletter/2015/june/images/error_reporter.png"
        alt="error reporting" class="img-responsive" /></a>
    </p>
    <p>The Error Reporter pops up in the lower right
      corner.</p>

    <p>Paired with the Error Reporter is the UI Responsiveness Monitor,
      which automatically generates an error record when the UI thread's
      responsiveness drops below a configurable threshold. This helps
      Eclipse open source software developers identify and ultimately
      fix performance issues and improve the overall user experience.</p>
    <p>Mars includes new tools for Docker that let the user manage and
      control their Docker images and containers; tools for building
      Cordova applications; and tools for creating, managing and running
      Gradle builds from within the development environment.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/june/images/docker.png"><img
        src="/community/eclipse_newsletter/2015/june/images/docker.png"
        alt="control docker" class="img-responsive" /></a>
    </p>
    <p>Control Docker images and containers directly from
      within Eclipse.</p>

    <p>One of the more exciting things included with Eclipse Mars a new
      installer. It may seem a little strange to some that Eclipse has
      never used actual install technology, opting for years to
      distribute binaries as compressed "packages" can that need to be
      extracted onto the user's workstation. Those packages are still
      the primary means of distributing Eclipse, but the new installer
      is offered as an alternative with the Mars release. The installer
      in simple mode does the sorts of things that installers do:
      download and assemble the necessary bits, and add an icon to the
      desktop (on Windows only).</p>
    <p>The installer's advanced mode is where the real power lies. With
      advanced mode, a user can realize a complete development
      environment including all the Eclipse plug-ins they need, and a
      fully provisioned workspace that includes source code from Git
      repositories, tasks from issue trackers, and a target environment
      for builds. With this, a developer can provision a complete
      development environment and be ready to start compiling, running,
      and testing in just a few minutes. The current implementation
      supports the creation of environments that get developers started
      to work on building contributions for Eclipse open source
      projects, as well as a handful of other open source projects.
      Tools are provided by the Oomph project to assist with the
      creation of configurations for other environments.</p>
    <p>The Preferences Recorder enables the user to define preferences
      that they want to have propagated to all of the the workspaces on
      their machine. Preferences are recorded as they are set; every
      time a workspace is opened, the recorded preferences are checked
      and updated where necessary.</p>
    <p>These are some of the high points in Eclipse Mars, specifically
      for users. User interface and performance improvements should
      positively impact the way that users work. Mars has a lot to offer
      for Java developers, with Java 8 functionality and performance
      improvements, the power of the code recommendations engine turned
      on by default, and tools for building, maintaining, and running
      Gradle builds. I'm particularly excited about the potential in the
      new install technology to help users assemble all the pieces that
      they need to get started quickly.</p>
    <p>Explore Mars!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2014/july/images/Wayne.jpg"
        width="75" alt="wayne beaton" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Wayne Beaton<br />
        <a href="http://www.eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="http://waynebeaton.wordpress.com/">Blog</a>
        </li>
        <li><a target="_blank" href="https://twitter.com/waynebeaton">Twitter</a></li>
        <!-- <li><a target="_blank" href="https://twitter.com/waynebeaton"></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
