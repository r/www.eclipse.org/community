<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>Eclipse Mars has arrived, and with it comes a brand new Docker
      tooling for it.</p>

    <h2>Goals</h2>
    <p>We wanted to have a way to easily start/stop and deploy Docker
      containers directly from Eclipse.</p>

    <p>We wanted something that would run on all three major platforms:
      Windows, Linux and OS X.</p>

    <p>We wanted something that would work together with existing Docker
      command line tools, but also utilized provide better overview and
      easier/faster access to common operations, from a visual
      perspective.</p>

    <p>We wanted it to be released with Eclipse Mars.</p>

    <p>...and that is what we got now.</p>

    <p>This article runs through how to get it installed, the main
      features and what the future plans are.</p>

    <h2>Where do I get it ?</h2>

    <p>
      With Eclipse Mars released, you can get it from the Eclipse Mars
      updatesite, the feature is named
      <code>Docker Tooling</code>
      .
    </p>

    <p>
      If you want to try the latest greatest builds you can use Linux
      Tools project nightly builds update site at <a
        href="http://download.eclipse.org/linuxtools/updates-docker-nightly/">http://download.eclipse.org/linuxtools/updates-docker-nightly/</a>
    </p>

    <p>
      To use the plugins, it is assumed that Docker is already
      installed. You can see <a target="_blank"
        href="https://docs.docker.com/installation/#installation">Docker's
        Installation</a> guide on how to do this on various platforms.
    </p>


    <h2>Views and Perspectives</h2>
    <p>Once you have installed the Docker tooling, you will get access
      to three new views:</p>

    <p>
      <b>Docker Explorer</b><br> a tree view listing all connected
      Docker instances, with image and containers.
    </p>

    <p>
      <b>Docker Containers</b><br> a table view listing containers for
      selected Docker connection.
    </p>

    <p>
      <b>Docker Images</b><br> a table view listing images available in
      the selected Docker connection.
    </p>

    <p>The easiest way to get to see these are by opening the Docker
      Tooling perspective.</p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_tooling_perspective.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_tooling_perspective.png"
      alt="Docker Tooling Perspective" class="img-responsive" /></a><br /> <br />
    <p>
      In the screen above, the Docker tooling are connected to a locally
      running Docker deamon named
      <code>boot2docker</code>
      .
    </p>

    <h2>Connect</h2>
    <p>
      To configure this you click the
      <code>Add Connection...</code>
      button in the
      <code>Docker Explorer</code>
      view.
    </p>

    <p>This will start a wizard that will try to detect your default
      Docker connection setup, dependent on your operating system.</p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/add_docker_connection.png"><img
      src="/community/eclipse_newsletter/2015/june/images/add_docker_connection.png"
      alt="add connection wizard" class="img-responsive" /></a><br /> <br />


    <p>
      In Linux it will use standard unix sockets and if on Windows or
      OSX, it will look for the following environment variables:
      <code>DOCKER_HOST</code>
      ,
      <code>DOCKER_TLS_VERIFY</code>
      and
      <code>DOCKER_CERT_PATH</code>
      .
    </p>

    <p>
      If neither of these are detectable, you can click
      <code>Use custom connection settings</code>
      and provide the connection info.
    </p>

    <p>When you have the connection working you can get started using
      Docker images.</p>

    <h2>Pull individual images</h2>
    <p>
      To pull an image, you use the
      <code>Pull Image</code>
      in the Docker Image view.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_pull_image.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_pull_image.png"
      alt="pull image" class="img-responsive" /></a><br /> <br />


    <p>
      Here, I'm simply pulling the <a target="_blank"
        href="https://registry.hub.docker.com/u/jboss/wildfly/">jboss/wildfly</a>
      image, a image amongst many available at <a target="_blank"
        href="https://hub.docker.com/account/signup/">http://hub.docker.com</a>.
    </p>


    <h2>Run</h2>

    <p>To run the image, the easiest way is to right-click on the image
      in the Docker Explorer.</p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_explorer_run.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_explorer_run.png"
      alt="docker explorer run" class="img-responsive" /></a><br /> <br />

    <p>
      Here, I've initially filtered the list to just show images
      matching
      <code>wildfly</code>
      and then using right-click to choose the
      <code>Run Image...</code>
      action.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_run_image.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_run_image.png"
      alt="docker run" class="img-responsive" /></a><br /> <br />

    <p>
      From within this dialog you can also search in Docker Hub for
      other images by clicking
      <code>Search...</code>
      .
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_search.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_search.png"
      alt="docker search" class="img-responsive" /></a><br /> <br />

    <p>In this example, I'm only going to focus on running using the
      defaults, but in the Run Image wizard you can also configure
      ports, links, volumes, environment variables etc.</p>

    <p>By default, we enable interactive and tty mode to allow you to
      interact with the docker container in the console (i.e. if the
      image asks for input).</p>

    <p>
      When you click
      <code>Finish</code>
      , the container will start and show output in a Console and the
      Docker Containers view will show which ports are used.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_run_console.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_run_console.png"
      alt="docker run console" class="img-responsive" /></a><br /> <br />

    <p>In here, the port at 8080 (the web server) is mapped to 32768 on
      the docker daemon.</p>

    <p>
      To show this I just need to goto <a
        href="http://dockerhost:32768/">http://dockerhost:32768</a> to
      access it.
      <code>dockerhost</code>
      is the IP of the docker daemon.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_browser.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_browser.png"
      alt="docker pull image" class="img-responsive" /></a><br /> <br />

    <h2>Build</h2>

    <p>
      If you have a
      <code>Dockerfile</code>
      you can build it via the
      <code>hammer</code>
      icon on the Image view. This will start the Build wizard.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_build.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_build.png"
      alt="docker build" class="img-responsive" /></a><br /> <br />

    <p>
      Once built, the image will show up and be possible to use for
      <code>running</code>
      .
    </p>

    <h2>Properties</h2>
    <p>You can view properties for all the various parts: connection,
      image and container, including getting a tree view of what docker
      inspect would show.</p>

    <a
      href="/community/eclipse_newsletter/2015/june/images/docker_properties.png"><img
      src="/community/eclipse_newsletter/2015/june/images/docker_properties.png"
      alt="docker properties" class="img-responsive" /></a><br /> <br />

    <h2>Future</h2>
    <p>For Eclipse Mars we added all of the above base features and you
      can use it in your day-to-day work with Docker.</p>

    <p>For Eclipse Mars SR1, we will work on getting some of the rough
      edges fixed, like 'Run' and 'Build' should be available in the
      context menu and not only in the views menu.</p>

    <p>Work also started in Eclipse CDT to support using Docker images
      to build binaries for an OS other than the one you are running on.
      The vision for this would allow running on Windows or Mac, but
      target native deployment on multiple various Linux architectures.</p>

    <p>
      Furthermore in <a target="_blank" href="http://tools.jboss.org/">JBoss
        Tools</a> we are working on better integrating Docker with
      Eclipse server adapters, to ease deployment of your web
      applications to a Docker container. You can see how server
      deployment works with the current Docker tooling by leveraging <a
        target="_blank"
        href="http://tools.jboss.org/blog/2015-03-02-getting-started-with-docker-and-wildfly.html">docker
        volumes</a> and <a target="_blank"
        href="http://tools.jboss.org/blog/2015-03-03-docker-and-wildfly-2.html">remote
        deployment</a> support.
    </p>

    <h2>Feedback</h2>
    <p>I want to give a big thank you to Jeff Johnston, Roland Grunberg
      and Xavier Coulon for making the Docker Tooling happen so far. Now
      that we have the project in Linux Tools and part of Mars we are
      looking forward to bug reports and contributions from the Eclipse
      community.</p>

    <p>
      If you have suggestions or find bugs, please open these in the <a
        href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Linux%20Tools">Linux
        Tools project under Docker</a>.
    </p>

    <p>Have fun!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/june/images/maxandersen.jpeg"
        alt="Max Rydahl Andersen" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Max Rydahl Andersen<br /> <a target="_blank"
          href="http://www.redhat.com/">Red Hat</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/maxandersen">Twitter</a></li>
        <li><a target="_blank" href="http://xam.dk/blog/">Blog</a></li>
        <!--<li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

