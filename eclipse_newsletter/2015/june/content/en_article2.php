<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>The Mars release of Eclipse includes Oomph in all the packages
      and the release's download page prominently features the new
      Eclipse Installer, powered by Oomph. With the Eclipse Installer
      you can create your installations more easily and more reliably.
      Not only that, you can quickly create several installations,
      without downloading many huge packages, and without filling up
      your disk with many duplicate files.</p>

    <h2>Why is the Installer Better?</h2>
    <p>
      If you've downloaded and "installed" Eclipse packages in the past,
      you might be familiar with the various ways that Eclipse fails to
      run. Why? Because it just can't find a Java Virtual Machine (JVM)
      or can't find an appropriate JVM. So what do you need to do?
      Install the right one or make Eclipse find the right one. But how?
      <a target="_blank" href="http://lmgtfy.com/?q=eclipse+won%27t+start">Let
        me Google that for you</a>. You'll find the best answer at the
      top of the list of more than a million matches. Yes, that's right,
      a million of matches. In a twisted sense, it's reassuring that
      you're not alone; misery loves company. In any case, you'll be
      told that fixing this is easy, but you have to wonder, if it's so
      easy, why do you have to do it at all?
    </p>

    <h2>Please Just Find the JVM</h2>
    <p>
      To do a better job than Eclipse's native launcher, we need a
      better launcher. For Windows, the Eclipse Installer provides just
      that. It's distributed as a self-extracting executable. When you
      run it, it inspects your system to find all available JVMs and
      indexes them. It tests each one to determine its bitness and
      version. The actual Eclipse Installer itself is an Eclipse
      application that needs Java 1.7. If you have 1.7 or higher already
      installed on your system, of the appropriate bitness, the Eclipse
      Installer will launch and you're off to the races. If not, you'll
      be <a target="_blank"
        href="https://waynebeaton.wordpress.com/2015/05/25/screenshot-of-the-week-missing-jre/">directed</a>
      what to install and where to find it. In either case, you'll be up
      and running quickly, without consulting Google.
    </p>

    <h2>Installing a Product</h2>
    <p>The Eclipse Installer supports two modes, simple and advanced. In
      the simple mode, you'll be presented with the list of products to
      install, just like the list of packages on the download page. You
      pick the one you want, then you'll be asked where you want to
      install it. A suitable default location is suggested, but you can
      change that of course. Click the Install button, and when that's
      done, launch the installation. Because none of the packages need a
      Java version higher than what's needed by the installer itself,
      the right JVM will be available and the installation will
      definitely launch. For Windows you can even create menu and
      desktop shortcuts automatically.


    <p>In the Eclipse Installer's advanced mode, the process is much the
      same, but you can also choose Eclipse projects whose source
      artifacts you want provisioned in the workspace. As such, you can
      automatically set up a development environment just like the one
      used by the project's developers. In addition, you can author your
      own project setups to automate the process of provisioning your
      own specific development environment.</p>

    <h2>Technical Details</h2>
    <p>The Eclipse Installer, by default, uses p2's little-known bundle
      pool technology. All the artifacts you've downloaded to create
      your installation are pooled for reuse. If you use the installer
      again to install another product, these artifacts are reused,
      i.e., they are not downloaded again, and they are shared across
      installations. With Oomph's targlet technology, fully integrated
      with the Plug-in Development Environment, this bundle pool is
      reused for provisioning target platforms.</p>

    <p>We hope that Oomph helps to improve your overall Eclipse
      experience with Mars, including Oomph's support for sharing
      preferences uniformly across all your workspaces. We're always
      open to suggestions for improvements and of course welcome
      contributions. Using the advanced mode of the Eclipse Installer
      you can provision a full Oomph development environment configured
      so you can commit contributions to Gerrit.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2014/may/images/Eike_Stepper2.jpg"
        alt="Eike Stepper" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Eike Stepper<br />
        <a target="_blank" href="http://www.esc-net.de/">ES-Computersysteme</a>
      </p>
      <p><a target="_blank" href="http://thegordian.blogspot.ca/">Blog</a>
        <br><a target="_blank" href="https://twitter.com/eikestepper">Twitter</a></p>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2014/november/images/Edmerks.JPG"
        alt="Ed Merks" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Ed Merks<br />
        <a target="_blank" href="http://www.itemis.com/">itemis</a>
      </p>
      <p><a target="_blank" href="http://ed-merks.blogspot.de/">Blog</p>
        </div>
      </div>
    </div>
  </div>
</div>

