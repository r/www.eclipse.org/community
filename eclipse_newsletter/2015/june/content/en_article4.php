<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>The Eclipse team is happy to announce early access support for
      JavaSE 1.9 and JDK 9. With this feature patch, developers will be
      able to start using the JavaSE-1.9 execution environment and Java
      9 in their projects.</p>

    <h2>Getting the Update</h2>

    <p>The feature patch can be installed from the Eclipse Marketplace
      with the following steps:</p>
    <ul>
      <li>Make sure the Marketplace client is installed (this is the
        case if you use an EPP)</li>
      <li>Select "Help > Eclipse Marketplace..."</li>
      <li>Enter "Java 9 support" in the search box and click on "Go"</li>
      <li><b>Install Eclipse Java 9 Support (BETA) for Mars</b></li>
    </ul>

    <p>The update can also be installed from the following p2
      repository:</p>

    <p>
      <a
        href="http://download.eclipse.org/eclipse/updates/4.5-P-builds/">http://download.eclipse.org/eclipse/updates/4.5-P-builds/</a>
    </p>


    <h2>Using the Batch Compiler</h2>

    <p>
      In order to use the batch compiler only, you can follow the steps
      described at the link <a
        href="http://help.eclipse.org/topic/org.eclipse.jdt.doc.user/tasks/task-using_batch_compiler.htm">Using
        the batch compiler</a>. Make sure to use newer version of the
      'org.eclipse.jdt.core' JAR.
    </p>

    <p>With this update, Java projects and plug-in projects can have
      JavaSE-1.9 as their execution environment and can be built with
      JDK 9. All the JDT features, including editor and search, work
      with JavaSE-1.9 now.</p>

    <p>
      <b>Note</b>: The Eclipse IDE and the batch compiler must be run
      with JRE 9 to be able to use JavaSE-1.9 and JDK 9.
    </p>

    <p>
      For more information on our early Java 9 work, see the Eclipse
      wiki page on that topic - <a href="https://wiki.eclipse.org/Java9">https://wiki.eclipse.org/Java9</a>.
    </p>


    <h2>More about JDK 9 - The Modular JDK (jigsaw)</h2>

    <p>In JDK 9, the entire JDK source base, including the run-time, is
      being modularized. The feature that is most interesting to us is a
      totally restructured JDK run-time images. The new run-time images
      do away with the legacy JAR format replacing it with a new
      proprietary format. The Eclipse Java 9 support (BETA) updates JDT
      so that it can make use of the jrt-fs, a file system provider
      bundled within the Java run-time, to load the Java run-time
      classes from the jimages.</p>

    <p>More information about Modular JDK and the Modular Run-Time
      Images are available here:</p>

    <ul>
      <li><a target="_blank"
        href="http://openjdk.java.net/projects/jigsaw/">http://openjdk.java.net/projects/jigsaw/</a></li>
      <li><a target="_blank" href="http://openjdk.java.net/jeps/220">http://openjdk.java.net/jeps/220</a></li>
    </ul>

    <p>
      <b>Disclaimer</b>: This is an implementation of an early-draft
      specification developed under the Java Community Process (JCP) and
      is made available for testing and evaluation purposes only. The
      code is not compatible with any specification of the JCP.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/june/images/jay.png"
        alt="jay arthanareeswaran" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jay Arthana-<br>reeswaran<br />
        <a target="_blank" href="http://www.ibm.com/">IBM</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="https://plus.google.com/114523397779183846903/">Google +</a></li>
        <!--<li><a target="_blank" href=""></a></li>
          <li><a target="_blank" href="">Twitter</a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

