<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Andrey Loskutov";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2015/june">June 2015</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2015/june/images/andreyloskutov.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I work as R&D software engineer/Eclipse expert at <a target="_blank" href="https://www.advantest.com/DE/index.htm">Advantest Europe</a> in B&ouml;blingen/Germany. My main responsibility is the 
					Eclipse platform, Eclipse test framework and core UI components we develop for our <a target="_blank" href="https://www.advantest.com/DE/products/ictestsystems/WEBDEV004754">V93000 product</a>. 
					Advantest V93000 SoC is the world's leading semiconductor test solution and its UI is based on Eclipse. I also evaluate, build, and configure the Eclipse IDE and various plugins used by our 
					R&D engineers. My work day is a mix of Eclipse/JVM crash or deadlock investigations, bug triaging, code reviews, meetings, Java debugging, shell scripting, platform patching, tea 
					drinking, and lot of R&D engineers support and coaching.</p>

				<p>After my day job, in my spare time I try to do some open source development - I'm the author of few <a target="_blank" href="http://andrei.gmxhome.de/eclipse.html">Eclipse plugins</a>, 
					maintainer of <a target="_blank" href="http://findbugs.sourceforge.net/">FindBugs</a> Eclipse plugin, committer for the following Eclipse projects: <a href="https://www.eclipse.org/egit/">EGit</a> and 
					<a href="https://projects.eclipse.org/projects/eclipse.platform.ui/">Platform UI</a>.</p>
					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I've been using Eclipse since its first public release (2.0, in 2002), and before that I've used its predecessor (Visual Age for Java) I think since 2000. Very soon after we've got this new 
					shiny Eclipse toy I wrote my first Eclipse plugin - BranchView for <a target="_blank" href="http://www.perforce.com/">Perforce</a>, a graphical version tree of p4 commits. This 
					(now discontinued) plugin was a warm up, soon followed by <a target="_blank" href="http://andrei.gmxhome.de/jdepend4eclipse/">JDepend4Eclipse</a>,
					 <a target="_blank" href="http://andrei.gmxhome.de/anyedit/">AnyEdit Tools</a>, <a target="_blank" href="http://andrei.gmxhome.de/filesync/">FileSync</a>, 
					<a target="_blank" href="http://andrei.gmxhome.de/bytecode/">Bytecode Outline</a> and <a target="_blank" href="http://andrei.gmxhome.de/datahierarchy/">Data Hierarchy</a> plugins. 
					I wrote all those plugins to simplify our, mine and my collegue's, daily work on a very large Java code bases at <a target="_blank" href="http://www.tui-infotec.com/">TUI Infotec</a> 
					and later at <a target="_blank" href="https://www.advantest.com/DE/index.htm">Advantest Europe</a>. All my plugins are <a target="_blank" href="https://github.com/iloveeclipse">open source</a> 
					and available on <a href="http://marketplace.eclipse.org/search/site/%22Andrey%20Loskutov%22">Eclipse Marketplace</a>.</p>

				<p>Since 2002, I followed each Eclipse release (and sometimes even milestones) to catch up on platform changes, updating my (sometimes broken) plugins, reporting bugs, and sometimes 
					contributing patches. Last year, I started contributing more to Eclipse projects and less to my own. I had to patch JGit/EGit because we moved to Git from Clearcase and hit 
					serious performance issues in Eclipse with our 7 GB git repository. At same time, I had to patch Platform UI because my beloved Eclipse 3.8.2 was not working on modern Linux 
					desktops anymore (thanks to ever changing GTK behavior) and Eclipse 4.4 still didn't match my quality expectations. After my patches to JGit and EGit I became EGit committer.
					Later, after my patches to Eclipse 4.5, I became Platform UI committer. I hope I can continue to contribute to these projects in the future.</p>

				<p>One very important thing I've learned during this ~15 years with Eclipse: everyone can and should try to contribute back to Eclipse (or to open source projects in general). 
					IMHO Eclipse today can only survive with contributions from our users, so I encourage every one to test Eclipse milestones, write bug reports and contribute patches - 
					it is much easier today than it was a few years ago, and it is much more important now as well. Read my recent <a target="_blank" href="http://www.jroller.com/andyl/entry/from_outsider_to_insider">blog entry</a>
					 to understand why!</p>		
									
			<h3>Name five plugins you use and recommend:</h3>
					<p>I must confess, I eat my own dog food and use my own plugins every day, so don't be surprised to find some of them in the list below. :)</p>
					<ul>
						<li><b><a href="http://marketplace.eclipse.org/content/anyedit-tools">AnyEdit Tools</a></b>: a "must have" swiss knife for every Eclipse programmer! Makes lot of boring programming tasks easy and 
					convenient. Many of the ideas and features found in this plugin were contributed by AnyEdit users.</li>
						<li><b><a href="http://marketplace.eclipse.org/content/findbugs-eclipse-plugin">FindBugs</a></b>: <b>*the*</b> static code analyzer for Java, integrated into Eclipse. No one should write Java code 
					without it! Please note that FindBugs can be also used on command line and so we at Advantest run the FindBugs analysis for all of our Java code in our continuous integration build.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/eclemma-java-code-coverage">Eclemma</a></b>: a very useful and handy Java code coverage plugin, based on <a target="_blank" href="http://www.eclemma.org/jacoco">JaCoCo</a>. 
					Similar to FindBugs, JaCoCo can be integrated into the continuous integration build (and of course all our automated Eclipse tests at Advantest are executed with JaCoCo).</li>
						<li><b><a href="https://marketplace.eclipse.org/content/eclipse-zip-editor">ZipEditor</a></b>: very useful little plugin for patching Eclipse platform jars  or editing/browsing archives 
					of many types directly inside IDE.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/bytecode-outline">Bytecode Outline</a></b>: shows your what the compiler does with your Java code. "Who cares" would you say? 
					This plugin is surely not for daily use, but sometimes it is an eye opener on some very subtle Java language details. Of course it is a "must have" tool for every one trying to engineer 
					Java bytecode with <a target="_blank" href="http://asm.ow2.org/">ASM</a>.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>Playing with my son, reading news & books (lately unfortunately not so often) and hacking on Eclipse (this <b>*is*</b> my hobby). Recently, I finally got my "fishing license" 
					(in Germany you can't just go fishing!!!) and now I can really enjoy fishing with my son.</p><br/>
					<iframe width="640" height="360" src="https://www.youtube.com/embed/FZcop9EzGb0" frameborder="0" allowfullscreen></iframe>
					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="http://andrei.gmxhome.de/privat.html">Homepage</li>
					<li><a target="_blank" href="https://www.google.com/+AndreyLoskutov/">Google+</li>
					<li><a target="_blank" href="http://www.jroller.com/andyl/">Blog</li>
					<li><a target="_blank" href="https://github.com/iloveeclipse/">GitHub</li> 
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>