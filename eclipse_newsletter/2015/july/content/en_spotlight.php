<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
<img align=right src="/community/eclipse_newsletter/2015/july/images/jaybillings.png"/>


			<h2>What do you do?</h2>
			<p> I am a research scientist at Oak Ridge National Laboratory and I am a computational physicist in the Computer Science
			and Mathematics Division. I work on high-performance computing simulators for energy science and I lead the development of the Eclipse Integrated
			Computational Environment. I also squeeze in as much interaction as possible with the <a href="https://science.eclipse.org/">Eclipse Science Working Group</a>.</p>


			<h2>How long have you been using Eclipse?</h2>
			<p>The first time I used Eclipse was in 2006 and I used it lightly thereafter until late 2008 when I started using it regularly. I've always been
			fascinated by it and I really buy into the philosophy behind it. I have a serious nerd crush on the Mars release because it is *so* well done.</p>


			<h2>Name five plugins you use and recommend:</h2>
					<p>This is hard to do with all of the projects that are part of the simultaneous release these days. Let's see:</p>
					<ul>
						<li><b><a href="https://marketplace.eclipse.org/content/eclipse-moonrise-ui-theme">Moonrise</a></b>:  A dark theme for Eclipse. I like it better than the Eclipse Dark Theme because I think it has a better color balance and
						interaction with the color schemes on older versions of Linux, like RHEL6.</li>
						<li><b><a href="https://eclipse.org/ease/">The Eclipse Advanced Scripting Environment (EASE)</a></b>:  I just recently learned about EASE, which is technically an Eclipse project. It
						gives you a scripting environment in either Javascript (Rhino/Nashorn) or Jython that gives you direct access to the platform and other services.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/pydev-python-ide-eclipse">PyDev</a></b>:  An absolute must for anyone doing Python development and it includes some things for python development that are more common
						in editors like SublimeText, such as a graphical, vertical outline next to your code.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/texlipse">TeXlipse</a></b>: I write a lot of scientific documents in LaTeX and Texlipse is the best LaTeX editor I have found because of its strong
						integration with both LaTeX and the rest of the workbench.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/sonarqube">SonarQube</a></b>: We use SonarQube to track various aspects of our code quality and it has some really great support in its Eclipse
						plugin for tracking issues in our SonarQube database.</li>
					</ul>

			<h2>What's your favorite thing to do when you're not working?</h2>
			<p>Get out and about! :-) When I am not working I always try to get out on the town and separate myself from work, whether it be by hitting the gym, kayaking or attending community events.</p>


			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://twitter.com/jayjaybillings">Twitter</a></li>
					<li><a target="_blank" href="https://plus.google.com/117329041765539129591/posts">Google +</a></li>
					<li><a target="_blank" href="https://www.linkedin.com/pub/jay-jay-billings/17/a03/b51">Linkedin</a></li>
				</ul>