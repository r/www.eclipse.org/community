<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	    <p>The Hybrid Mobile Tooling (Thym) project aims to provide Eclipse based tools for building hybrid mobile applications. The project focuses on the <a target="_blank" href="http://cordova.apache.org/">Apache Cordova</a> project,
	    which is the de facto framework for building hybrid mobile applications, and provides tools for it. However, it is possible to extend the Thym to utilize alternate frameworks.</p>

		<h2 id="features">Features</h2>
		<p>Thym supports building Cordova applications for iOS, Android and Windows Phone. It uses locally available SDKs to build for the targeted platforms. It can build native binaries using the available SDKs. It is also possible
		to export projects that can be built or developed further using the tool chain of the target platforms. For instance, Thym export wizard can export an iOS project that can be directly opened on XCode and run.</p>
		<p>Thym also integrates with the locally available simulators. It is possible to launch a Cordova project on iOS, Android or Windows Phone simulators. Launching projects directly on a real device is supported for Android,
		provided that the Android device is connected to the developer&#39;s system.</p>
		<p>Starting with Cordova version 3.0, the core of the Cordova framework does not deliver device functionality, but delegates the responsibility to Cordova plug-ins. Hence, it is essential for any tooling for Cordova to
		support the management of Cordova plug-ins. Thym allows the full management of Cordova plug-ins on a project and it is possible to add and remove plug-ins via Eclipse menus and actions. Eclipse hybrid mobile projects
		can also restore the list of plug-ins from the config.xml file on a Cordova project.</p>
		<p>In addition to management of plug-ins, Thym also includes a Cordova plug-in discovery wizard that helps on finding the correct set of plug-ins from the extensive list of plug-ins on the Cordova plug-in registry.
		The wizard works in a similar way to the Eclipse Marketplace client.</p>

		<img src="/community/eclipse_newsletter/2015/july/images/PluginDiscovery.png" alt="Cordova Plug-in Disvovery Wizard" class="img-responsive"/><br />

		<p>Working with Cordova framework does require at one point during development cycle to modify the config.xml file of the project. The config.xml file is the central file for managing several aspects of your mobile
		application including name, version, preferences, plug-ins. Thym includes a form based editor for config.xml file. Config.xml editor aids developers with the editing of this file for common tasks.</p>

		<img src="/community/eclipse_newsletter/2015/july/images/configEditor.png" alt="Config.xml Editor" class="img-responsive"/> <br />

		<h2 id="community">Community</h2>
		<p>Thym code base started its life as part of the <a target="_blank" href="http://tools.jboss.org/">JBoss Tools project</a>. The code is contributed to Eclipse foundation little more than a year ago. In addition to JBoss Tools,
		Thym has been adopted by other Eclipse based products such as Zend Studio.</p>
		<p>The development of Thym continues on the <a target="_blank" href="https://github.com/eclipse/thym">Thym&#39;s repository on GitHub</a>. The project uses GitHub pull requests for non-committer contributions. The daily
		communication of the project happens on the project&#39;s <a href="https://dev.eclipse.org/mailman/listinfo/thym-dev">mailing list</a> and <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Thym">bugzilla</a>.</p>
		<p>Thym and Apache Cordova project depends on multiple platforms and multiple tool chains. Therefore both Apache Cordova project and Thym needs to make releases often. Thym is part of Eclipse Mars release train, but the
		project follows a more frequent release schedule and the best way to follow Thym releases is through <a href="https://marketplace.eclipse.org/content/eclipse-thym">Thym entry at Eclipse marketplace</a>.</p>

		<h2 id="future-plans">Future Plans</h2>
		<p>First and foremost, Thym will continue to follow the changes coming into the Apache Cordova project and adopt them. The team is also looking into ways to use Cordova CLI directly to improve the accuracy of builds
		as well as validators for Cordova projects.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/july/images/gorkem150.jpg"
        alt="Gorkem Ercan" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Gorkem Ercan <br />
        <a target="_blank" href="http://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.gorkem-ercan.com/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/gorkemercan">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://github.com/gorkem">GitHub</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

