<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	    <p>It seems fitting that Jubula’s mission to Mars should open up vast new frontiers for the tool and those working with it. One of the main contributions to the Mars release is the new “Jubula Client API” which allows Jubula tests
	    to be written in Java code. In this article, I’d like to take you on a short journey of what this means for the tool and for the teams using it.</p>

	<h2>Introducing “Mr Hyde”</h2>
		<p>Markus Tiede and I did a <a target="_blank" href="https://www.eclipsecon.org/na2015/session/jekyll-and-hyde-jubula">talk at EclipseCon North America</a> to introduce Jubula’s new personality. We deliberately pitched the talk as a comparison between “Dr. Jekyll” and “Mr Hyde”, since the addition of the Client API and
		the implications for writing tests could be seen as a scary departure from our main target group of non-programmers.</p>
		The first item to clarify, then, is that our main target group remains testers who are not programmers. We believe strongly that functional tests from the user perspective require a specific structure and focus that can
		often best be found in people who are not software engineers. Nevertheless, teams come in a variety of shapes, sizes and contexts. There are teams whose testers can indeed program, or who require particularly close
		collaboration between developers and testers. There are almost certainly teams who do not have the luxury of testers, and who are looking for tools to help them test in the best way possible in this situation.</p>
		<p>Particularly in the more technically-minded Eclipse community, the question has often been asked whether Jubula tests could also be written in Java (especially since the tool itself is written in Java).
		“Somewhere in there”, it has been said, “Jubula itself has to make Java calls to remotely control the application”.</p>
		<p>That’s very true, and in the summer of 2014, thanks to external sponsorship from a company using Jubula, we were able to dissect the parts for remote control from the parts for the ITE.</p>

	<h2>Commonalities and differences between the two perspectives</h2>
		<p>The Jubula ITE (Integrated Testing Environment) and the Jubula Client API are entirely single-sourced. The actions available in the Client API are directly generated from the actions available for the ITE. With minimal
		exceptions, the full set of actions for all supported toolkits is available in the API. The only exclusions are actions used to restart applications or to store variables for example, since these are also fully controllable
		within Java itself.</p>

	<h2>Before testing</h2>
		<p>As in the ITE, an AUT (Application under Test) must be started before it can be tested. The Client API offers methods for connecting to a running AUT Agent (which must be started separately), for starting an
		AUT and for connecting to the AUT.</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/Figure1_aut-start.png" alt="Auto Start" class="img-responsive"/><br />

	<h2>Executing CAPs (test steps)</h2>
		<p>Both the actions in the ITE and in the Client API use the same concept of a “CAP” being the absolutely necessary information for GUI automation. “CAP” stands for “Component, Action, Parameter” and can be summarized
		as “on which widget should which action be performed with which data”. For example, you might tell a user to “enter [action] <i>admin</i> [parameter] into the username field [component]”.</p>
		<p>In the ITE, this information is compounded into the Test Steps or unbound modules. Via the Client API, an execute method can be called on the AUT instance which specifies the component, the action and the parameter.</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/Figure2_loop.png" alt="Loop" class="img-responsive"/><br />

	<h2>Performing object mapping</h2>
		<p>The “component” aspect of the ITE and the Client API is exactly the same: the component identifiers required by the Client API are gained by exporting object mapping from the ITE.</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/Figure3_om.png" alt="Auto Start" class="om"/><br />
		<br />
		<p>This step incurs a dependency on the ITE when using the Client API, but does mean that the heuristic analysis for component recognition is also used by tests written with the Client API.</p>

	<h2>Executing the test</h2>
		<p>The Jubula Client API itself does not have any dependency to a specific test runner and can therefore be used with the runner of your choice. Unlike in the ITE, which provides exception handling and test result
		reports, exceptions and result processing must be implemented individually in the Client API. We already have teams using the Client API who have created their own HTML and HP ALM reports for their test projects.</p>

	<h2>Cleaning up</h2>
		<p>When you run an ITE test via the command line, the AUT is automatically disconnected and the AUT Agent is stopped at the end of the test. In the Client API, these steps must also be explicitly invoked.</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/Figure4_tearDown.png" alt="tear down" class="img-responsive"/><br />

	<h2>With great power comes great responsibility</h2>
		<p>Now we’ve taken a quick tour of the Jubula Client API, it’s time for some good old test advice. I am, after all, writing this both from the perspective of Product Owner and as head of our test consulting ;-).
		I’m all for developers taking an interest in testing and assisting with test automation. After all, quality is a whole team responsibility. However, it is worth bearing in mind that the test specification language
		is not the important factor when deciding how to structure tests. Java allows a great deal of freedom and flexibility – some of which may not be suitable for writing system- or acceptance-level functional tests.</p>
		<p>The primary aim of a functional test at this level is to test interactions that users will perform, in the way that they will perform them. Each automated “Test Case” should be an executable example of a “Use Case”
		(i.e. it should be relatable to a specific functional requirement). The “danger” when using a programmatic approach to functional test automation is that the tests may lose this focus and that test levels may be
		incorrectly mixed. Just because you can use JUnit as a test runner for Jubula does not mean that Jubula should be used for unit-level tests, or that Jubula tests should be structured like unit tests. The test
		consulting team at Bredex has been doing a great deal of work and research on patterns for functional test automation, and we’d recommend that functional tests (whether they are written with the Client API or
		the ITE (or any other tool for that matter)):</p>

			<ol>
				<li>follow patterns for separating services from implementation, such as e.g. the Page Object Pattern</li>
				<li>maintain a black-box perspective on the AUT</li>
				<li>are structured as user actions and workflows, e.g.</li>
			</ol>
		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/Figure5_FunctionalTest.png" alt="Functional Test" class="img-responsive"/><br />

		<p>Thus ends the sermon on using your newly available powers wisely!</p>

	<h2>Getting started and further information</h2>
		<p>The Jubula Client API is a part of the Jubula standalone edition and is in the code contributed for Mars. If you’re interested in using the Jubula Client API for your tests, then the documentation for getting started is
		<a href="http://help.eclipse.org/mars/topic/org.eclipse.jubula.client.ua.help/content/html/developerManual/index.html">here</a>. You can also read our blog entries
		<a target="_blank" href="http://www.bredex.de/blog_article_de/the-jubula-client-api-part-2.html">here</a>, and contact us via the forum with any questions.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/july/images/alexs.jpg"
        alt="Alexandra Schladebeck" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Alexandra Schladebeck <br />
        <a target="_blank" href="http://www.bredex.de/">Bredex</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://www.bredex.de/blog_en.html">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/alex_schl">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://plus.google.com/+AlexSchladebeck/posts">Google +</a></li>

        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
