<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	    <p><i>Eclipse Mars, this year’s new platform version, has well and truly arrived! Included in the release you will find Sirius 3.0, the latest version of the solution that allows you to create a custom and dedicated graphical modeling tool.</i></p>

	<h2>What is Sirius?</h2>
		<p><a href="http://www.eclipse.org/sirius/">Sirius</a> is the Eclipse project to easily create a customized modeling workbench. Dedicated to your area of expertise and supporting your design concepts, it allows you to
		graphically design complex systems (IT software, business activities, physics, etc.) while keeping the corresponding data consistent (architecture, component properties, etc.).</p>

		<p>A modeling workbench created with Sirius comprises a set of Eclipse editors (diagrams, tables and trees) that allow you to create, edit and visualize EMF models.</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/img1.png" class="img-responsive pull-left" />
		<img src="/community/eclipse_newsletter/2015/july/images/img2.jpg" align="left" class="img-responsive"/>
		<img src="/community/eclipse_newsletter/2015/july/images/img3.png" class="img-responsive"/> <br />

		<p>Whether you are an IT Architect or Developer, a Tools and Methods Manager, or an Embedded Software or Systems Engineer, you are called upon daily to devise and design innovative solutions in complex environments. With Sirius,
		you can create an efficient graphical tool to describe your design choices and define your architecture.</p>

	<h2>Sirius 3.0: robust, fast, and a great user experience</h2>

		<p>Since Sirius 1.0, released June 2014, many efforts were made on the performance and scalability. With this new version, Sirius takes robustness to another level. This technology is more than capable of handling intensive use thanks to
		testing scenarios that can handle up to 1 million elements! Even with your most limited scenarios, you will experience the difference: a lower memory footprint, faster refresh times, lower latencies with high volume models, etc.</p>

		<p>User experience is extremely important for us and it is no different with this latest version. Some basic behaviour inherited from GMF Runtime were changed to better fit user needs. For example, the new behavior for Resizing a container:</p>

		<br />
		<img src="/community/eclipse_newsletter/2015/july/images/img4.gif" class="img-responsive"/><img src="/community/eclipse_newsletter/2015/july/images/img5.gif" class="img-responsive"/>
		<br />

		<p>New behavior for Positioning things:</p>
			<ul>
				<li>Snap To Shape enabled by default for new diagrams</li>
				<li>Snap To Grid now used when an element is created</li>
				<li>Resize no longer change ports or children's location</li>
				<li>Actions to distribute shapes</li>
				<li>Action to reset the diagram origin</li>
			</ul>
		<h3>Diagram Editing</h3>
		<p>Sirius 3.0 also brings many ergonomic improvements for diagram editing:</p>

		<h4>Anchor on borders</h4>

		<p>Now the edges are linked closely to the images. You only need to use images with a transparent background.</p>


		<img src="/community/eclipse_newsletter/2015/july/images/img6.png" class="img-responsive"/>
		<br /><br />

		<h4>Underlined and strikethrough</h4>

		<p>In the Appearance tab, in addition to <b>Bold</b> and <i>Italic</i>, it is now possible to set the font formatting style to <u>Underline</u> and <s>Strikethrough</s>.</p>


		<img src="/community/eclipse_newsletter/2015/july/images/img7.png" class="img-responsive"/>
		<br />

		<h4>Compartments</h4>

		<p>Containers now support vertical and horizontal compartments layout, thanks to new possible values for Children Presentation attribute.</p>


		<img src="/community/eclipse_newsletter/2015/july/images/img8.png" class="img-responsive"/>
		<br />

		<p>The complete list of changes is available here: <a href="https://www.eclipse.org/sirius/whatsnew3.html">https://www.eclipse.org/sirius/whatsnew3.html</a></p>

	<h2>Concrete examples gallery</h2>

		<p>If you want to discover concrete examples of how Sirius is used, the Gallery page on the Sirius website is definitely here for you. There, you will find many noteworthy examples that are already implemented.</p>

		<p>From the Android applications modeler to a full Systems Engineering workbench, including the configuration of home automation systems, you will discover a wide range of use cases using Sirius in many domains.</p>


		<img src="/community/eclipse_newsletter/2015/july/images/img9.PNG" class="img-responsive"/>
		<br />

		<p>Visit the gallery: <a href="http://www.eclipse.org/sirius/gallery.html">http://www.eclipse.org/sirius/gallery.html</a>

	<h2>SiriusCon coming this December!</h2>

		<p>Launched in 2013, the Sirius project depends on a very active community. After a roadshow in 2014/2015 that took us west to Canada and east to Germany it’s now time for us to organize an international
		event fully dedicated to Sirius.</p>

		<p>SiriusCon is coming to Paris on  December 3, 2015 for a full day conference for both users and developers of this technology. Don’t miss out on the opportunity to learn more about Sirius, its new advanced
		features as well as user experience feedback.</p>

		<p>To stay up to date on the SiriusCon event please subscribe here: <a target="_blank" href="http://www.siriuscon.org/">http://www.siriuscon.org/</a></p>


		<img src="/community/eclipse_newsletter/2015/july/images/img10.png" class="img-responsive"/>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/july/images/freddya.jpeg"
        alt="Freddy Allilaire" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Freddy Allilaire <br />
        <a href="http://www.obeo.fr/en/">Obeo</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" href="https://twitter.com/allilaire">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" href="https://plus.google.com/+FreddyAllilaire">Google +</a></li>
        <li><a class="btn btn-small btn-warning" href="https://www.linkedin.com/in/allilaire">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
