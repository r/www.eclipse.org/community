<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

     <p>The number of Eclipse IoT projects keeps growing (we are approaching 20!) and it is sometimes difficult for people to quickly understand the problems
     each of these projects are trying to solve. While we have been pretty successful using the <i>Greenhouse demo<sup>™</sup></i> over the past few years to explain the
     "big picture", this is a rather limited use case and it does not really allow us to showcase some of our newest projects like <a href="https://eclipse.org/tiaki">Tiaki</a>
     or <a href="https://www.eclipse.org/vorto/">Vorto</a>, that solve more horizontal concerns.</p>

    <p>So a few weeks ago, I started creating a new setup for Eclipse IoT demos. I hope it will be adopted by everyone interested in having an easy way to setup a platform that explains
    what Eclipse IoT is all about. You should read this article to the end if you want to learn how to replicate this setup yourself, and build your very own Eclipse IoT box!</p>

    <p>It is probably worth mentioning at this point that I have now adopted the Raspberry Pi 2 for IoT demos, since having more horsepower means you get to
    run more processes simultaneously. This is exactly what you need when you want to demo several projects at the same time (even though this is probably not what you would
    do in a real-life scenario)! There are only a few tweaks to be made to the "vanilla" Raspberry Pi:</p>

    <h2>Adding a touchscreen</h2>

    <p>In order to allow for easy interaction, I've attached a touchscreen to my Pi. I got a <a href="https://www.adafruit.com/products/2407" target="_blank">7"
    HDMI display</a> for about $90. The set-up is pretty straightforward, as it's essentially just seen by the system as a regular screen and a regular mouse!</p>

    <h2>Running the IoT servers locally</h2>
    <p>When you make demos on the fly, you do not always have Internet access. Therefore, the Raspberry Pi runs a <strong>
    <a href="https://eclipse.org/mosquitto" target="_blank">Mosquitto</a></strong> MQTT broker and a <strong><a href="https://eclipse.org/leshan" target="_blank">Leshan</a></strong>
    LWM2M server to make it possible to play with the technology even when completely off the network.</p>

    <h1>A versatile prototyping platform</h1>

    <p align="center"><a href="/community/eclipse_newsletter/2015/october/images/iot_table.png"><img class="img-responsive"
    src="/community/eclipse_newsletter/2015/october/images/iot_table_small.png" alt="Eclipse IoT Demo Box - Home screen" /></a></p>

    <p>As of today, the demonstrators included in the "demo box" are:
        <ul>
            <li><strong>IoT Gateways</strong>: This helps to discover the capabilities of Kura, and reuses the existing "<a href="http://iot.eclipse.org/java/tutorial"
            target="_blank">Greenhouse demo</a>" (including sensors) to showcase how <a href="https://eclipse.org/kura" target="_blank">Kura</a> can act as an IoT application container.</li>

            <li><strong><a href="https://eclipse.org/smarthome" target="_blank">Smart Home</a></strong>: The Raspberry Pi runs the SmartHome/OpenHAB 2 demo bundle,
            and if you attach a WeMo plug or a Philips Hue to the same network as the Raspberry Pi, you can start using some really cool features like automatic device discovery!</li>

            <li><strong>Device Management</strong>: The box runs the <a href="https://eclipse.org/leshan" target="_blank">Leshan</a> LWM2M server, and a simple
            <a href="https://eclipse.org/wakaama" target="_blank">Wakaama</a> client registers against this server so you can perform device management operations directly with the box.</li>

            <li><strong>Service Discovery</strong>: <a href="https://eclipse.org/tiaki" target="_blank">Tiaki</a> provides an implementation of DNS-SD,
            and the demo box allows you to quickly run its command line examples.</li>

            <li><strong>MQTT and <a href="https://eclipse.org/mosquitto" target="_blank">Mosquitto</a></strong>: For people not familiar with the MQTT "way of life",
            I've packaged a nice live visualization of the MQTT topics hierarchy that allows to understand better what's happening on the Mosquitto broker that is running on the demo box.</li>

        </ul>
    </p>
    <p>It is actually pretty easy and fun for you to setup your own Eclipse IoT Demo Box, and to add your own features/applications should you want to:
        <ul>
            <li>Get yourself a Raspberry Pi 2 and a touchscreen</li>
            <li>Go to <a href="https://github.com/kartben/eclipseiot-demobox" target="_blank">https://github.com/kartben/eclipseiot-demobox</a> and follow the instructions in the README</li>
            <li>... That's it!</li>
        </ul>
    </p>
    <p>Just let me know if you try it out, I am very interested in what you will be doing with Eclipse IoT project!
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/october/images/ben.jpg"
        alt="Benjamin Cabe" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Benjamin Cabé<br />
        <a target="_blank" href="http://www.eclipse.org/">Eclipse Foundation, Inc.</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/kartben">Twitter</a></li>
      <li><a class="btn btn-small btn-warning" target="_blank" href="http://blog.benjamin-cabe.com/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/benjamincabe">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

