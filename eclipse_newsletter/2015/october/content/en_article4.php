<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
	<a target="_blank" href="http://zolertia.io/"><img src="/community/eclipse_newsletter/2015/october/images/zolertia.png" alt="zolertia logo" class="img-responsive" /></a><br><br/>

	<p><i>Zolertia releases the RE-Mote: its new IoT development platform, with long range communications and ultra-low power consumption.</i></p>

	<h2>RE-Mote Main Features</h2>
    <p>The RE-Mote is an IoT (Internet of Things) and wireless sensor networks (WSN) development platform, designed for developers and enthusiasts,
    which provides a complete and easy solution for data acquisition, monitoring and control in any application.</p>

	<p>The RE-Mote features a dual band radio interface, allowing operation in the 2.4 GHz and Sub 1GHz (868MHz or 915MHz) ISM bands, compliant
	with the IEEE 802.15.4-2006, IEEE 802.15.4e & IEEE 802.15.4g standards and compatible with Thread and SIGFOX as well.</p>

	<img src="/community/eclipse_newsletter/2015/october/images/remote_frontback.png" alt="re-mote front back" class="img-responsive"/><br/>

	<p>The core of the RE-Mote is the Zoul: a stand-alone module comprising the Texas Instruments ARM Cortex-M3 CC2538 System-on-Chip,
	bundled with the CC1200 sub-1GHz RF transceiver.  In a nutshell the RE-Mote packs:</p>
	<ul>
		<li>ISM 2.4-GHz IEEE 802.15.4-2006 and IEEE 802.15.4e  & Zigbee compliant radio</li>
		<li>ISM 863-950-MHz ISM/SRD band IEEE 802.15.4g compliant radio</li>
		<li>ARM Cortex-M3 32 MHz clock speed, 512 KB flash and 32 KB RAM (16 KB retention)</li>
		<li>AES-128/256, SHA2 Hardware Encryption Engine</li>
		<li>ECC-128/256, RSA Hardware Acceleration Engine for Secure Key Exchange</li>
		<li>User and reset button</li>
		<li>Consumption down to 300 nA using the shutdown mode</li>
		<li>Programming over BSL without requiring to press any button to enter bootloader mode</li>
		<li>Built-in battery charger (500 mA), facilitating Energy Harvesting and direct connection to Solar Panels and to standards LiPo batteries</li>
		<li>Wide range DC Power input: 3.7-16 V</li>
		<li>Small form-factor (73 x 40 mms)</li>
		<li>MicroSD (over SPI)</li>
		<li>On board RTC (programmable real time clock) and external watchdog timer (WDT)</li>
		<li>Programmable RF switch to connect an external antenna either to the 2.4 GHz or to the Sub 1 GHz RF interface through the RP-SMA connector</li>
	</ul>

	<p>RE-Mote has been developed under the RERUM European Project (Seventh Framework Programme), the project is mainly focus on security for Internet of Things applications.
	RE-Mote is the platform used for all wireless communications during the project.</p>

	<h2>Long Range Communication Test</h2>
	<p>The RE-Mote platform was taken for its first long-range test and the results were promising: a 3.14 Km distance was easily achieved using IEEE 802.15.4g settings
	(50Kbps on the 868MHz band), with an average RSSI of -56dBm (the CC1200 sensitivity is -110dBm).</p>

	<p>The field test was done near Barcelona with two RE-Motes featuring simple omni-directional antennas with almost no gain, no special fixtures or highly-directive antennas,
	just good old duct tape and some amazing views at the Parc de la Serralada de Marina.</p>
	<p>The results are shown in the image below.</p>

    <img src="/community/eclipse_newsletter/2015/october/images/map.png" alt="Serrlada de Marina" class="img-responsive"/><br>

    <h2>Ultra-low Power Consumption Test</h2>

    <p>For this test we wanted to know the needed energy which is required for the setup of a wireless sensor network by comparing the consumption of RE-Mote with a WiFi module.
    After having the RE-Mote and a WiFi module back to back publishing HTTP requests to an IoT cloud platform, the energy consumption of the RE-Mote was: </p>

    <ul>
		<li>At least 15 times lower than the WiFi device (40.5mA vs 2.61mA) in similar conditions (1 minute posting period, devices sleeping as much as possible);</li>
		<li>and 213 times less (0.19mA) using non-IP communication.</li>
	</ul>

	    <img src="/community/eclipse_newsletter/2015/october/images/consumption_test.png" alt="consumption test" class="img-responsive" /><br/>
	<p>So we thought, why not try to supply the RE-Mote with a potato energy and see how it goes? We manage to replicate the results of the non-IP test,
	posting every minute to a receiver and sending the data over serial to an application.</p>
	<p><u>Important note</u>: the potatoes were not suitable for dinner afterwards.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/october/images/marc.jpg"
        alt="Marc Fabregas" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Marc Fàbregas
        <br />
        <a target="_blank" href="http://zolertia.io/">Zolertia</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://es.linkedin.com/pub/marc-f%C3%A0bregas-bachs/0/174/163/es">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

