<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div id="fullcolumn">
  <div id="midcolumn">
    <h1><?php echo $pageTitle; ?></h1> <img align=right src="/community/eclipse_newsletter/2015/october/images/julien.png"/>
	    
			<h2>What do you do?</h2>
			<p>I'm a software developer at Sierra Wireless. I mainly work on our IoT Cloud platform, implementing device to cloud protocols like MQTT, 
			CoAP, and Lightweight M2M, with the associated security layers and credential management systems. To achieve that we are involved in several 
			Eclipse IoT projects: Wakaama, Californium, Leshan, and Tinydtls.</p> 
			
								
			<h2>How long have you been using Eclipse?</h2>
			<p>Previously, I was more involved in the Apache Software Foundation. I really started being involved in Eclipse 4 years ago when I 
			joined Sierra Wireless to work on IoT products. I'm now a committer on Californium and Wakaama, as well as project lead for Leshan.</p>
									
			<h2>What do you enjoy about working in an open source community, like Eclipse?</h2>
			<p>I really like the friendly community we have at Eclipse, everybody behaves nicely and we have very interesting technical discussions. 
			I find the companies and individuals behind the IoT working group are always eager to collaborate across the different projects.</p>

			<h2>What's your favorite thing to do when you're not working?</h2>
				<p>Presently, my main occupation is to take care of my 2 toddlers. I'm also a tinkerer and fan of DIY: from electronics to carpentry.</p>

					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://twitter.com/vrmvrm">Twitter</a></li>
					<li><a target="_blank" href="http://linkedin.com/in/jvermillard">Linkedin</a></li>
				</ul>
		</div>
		</div>
	</div>