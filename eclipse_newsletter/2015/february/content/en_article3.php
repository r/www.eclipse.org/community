<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Some major and exciting technologies have hit maturity over the
      past few years among them we (codefresh) were looking at 2 closely
      - the first is the cloud being the de-facto infrastructure for
      apps and the 2nd is the vast adoption of Docker as ‘the’ container
      for apps and microservices.</p>

    <p>These two disruptions essentially empower us as developers to
      instantly setup & clone complex environments. There is a great
      benefit with having these Dockers based environments not only as
      the production and staging environments, but also as the standard
      core development environments. Such practice significantly reduces
      the frictions both among the individual developers as well as the
      frictions often found when moving code from development to staging
      and then to production. It is not a secret that mature companies
      such as Facebook and Google invested substantial amount of man
      years in building an internal framework to allow their developers
      to gain an instant access to remote development environments, a
      development environments that are as close as possible to the
      staging & production environments. While both companies have built
      these framework even before the Docker ‘era’, Docker is, in fact,
      doing a great job helping teams implement similar concepts in much
      less efforts.</p>

    <p>There are two additional components, though, that are required in
      order to fully utilize these Docker environments as development
      environments - the 1st is auto setting of the code base (the
      relevant git branch) and the other is providing IDE & terminal
      interfaces. For the first, we have implemented an automated git
      flow process, for the latter, we have chosen to embed Orion, an
      open source Web IDE eclipse project. The main reasons behind
      choosing Orion were, first, the extensibility of Orion - It is a
      robust framework that is fully extensible on the client side and
      allows adding plugins hosted on remote domains. It allowed us to
      build plug-ins, such as auto deployment to heroku, built-in
      terminals, muti doc views etc… Second, is Orion’s agnosity on the
      backend, it offers two versions of backend, Java and nodejs.
      Although few of the recent features in Orion version 8 implemented
      in the Java backend only, we have found the nodejs backend more
      compelling for our developers, easy to be set, configured and
      extended (we have managed to rewrite the backend with nodejs
      express module in 3 days!). The third is the community, the
      transparency and the progress we see with every new release of the
      Orion project, introducing new enhancements, fixing defect etc…</p>

    <p>
      A preview of our extended Orion can be found at each one of the
      code samples at: <a href="http://www.codefresh.io/labs">www.codefresh.io/labs</a>
    </p>

    <p>
      <b>Orion embedded in codefresh labs</b>
    </p>

    <a
      href="/community/eclipse_newsletter/2015/february/images/screenshot.png"><img
      src="/community/eclipse_newsletter/2015/february/images/screenshot.png"
      width="700" alt=""></a><br />
    <br />

    <p>By leveraging Docker as a container, GitHub as a hosted git (but
      essentially it can be any git), AWS as infrastructure (same here
      it can be implemented with other cloud IaaS) and Orion as a web
      IDE, we have managed to automate the git flow in a way that you
      can setup an environment in the relevant git context instantly and
      manage your development pipeline with the least friction possible.</p>

    <p>
      <b>Docker based gitflow</b>
    </p>
    <a
      href="/community/eclipse_newsletter/2015/february/images/gitflow.png"><img
      src="/community/eclipse_newsletter/2015/february/images/gitflow.png"
      width="500" alt=""></a><br />
    <br />

    <p>
      Contact us from any further question <a
        href="mailto:contact@codefresh.io">contact@codefresh.io</a>.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/february/images/raziel.png"
        height="90" alt="raziel tabib" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Raziel Tabib<br />
        <a target="_blank" href="http://www.codefresh.io/">codefresh</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="https://www.linkedin.com/company/codefresh">Linkedin</a></li>
        <!--<li><a target="_blank" href="https://twitter.com/rcheetham">Twitter</a></li> -->
        <li><a target="_blank"
          href="https://www.facebook.com/codefresh.io">Facebook</a></li>
        <!--$og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

