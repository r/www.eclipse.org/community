<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>The Orion project is releasing version 8.0 at the end of February
      2015, as part of our regular cadence of major new releases every
      four months. The team has been as busy as ever, with this release
      including over 1000 commits from 36 unique contributors, and
      fixing 280 bugs. The major themes for this release were building
      higher value tools for JavaScript/CSS/HTML development, and in
      making the Orion server highly scalable.</p>
    <p>In language tools, Orion 8.0 has made some major strides in
      JavaScript tooling over what you see in most code editors.
      Developing "smart" tools for JavaScript is very challenging, given
      the very forgiving and untyped nature of the language. This
      release introduces the Eclipse concept of Quick Fixes to Orion,
      which suggest automatic fixes for common coding errors. Quick
      Fixes are now available for about a dozen common JavaScript and
      CSS coding problems, with a steady stream of new ones being added
      over time. For example, unused or unreachable code is flagged with
      a warning, and a quick fix is provided to delete the unused code.
      Quick fixes are also provided for missing/extra semi-colons,
      incorect use of == operator, sparse arrays, and switch case
      fall-through. Activate quick fixes by pressing F2 or hovering on
      an error or warning. The hover will contain a link for each fix
      proposal.</p>
    <p>
      <img src="images/quick-fix.png" alt="Quick fixes" />
    </p>
    <p>Another significant new tooling feature is semantic navigation.
      Using a model of your web application structure, Orion will now
      provide navigation between related HTML, CSS, and JavaScript
      artifacts. For example, within an HTML file, you can jump to
      referenced script and CSS files. In CSS, you can navigate across
      CSS import statements to related CSS. In JavaScript files that use
      the AMD module syntax, you can jump to imported modules referenced
      in "require" statements. Like Quick Fix, these hovers are
      available when either hovering or pressing F2 on a language
      element that supports semantic navigation.</p>
    <p>
      <img src="images/semantic-nav.png" alt="Semantic navigation" />
    </p>

    <p>Also in language tools, the Orion editor now supports multiple
      edit cursors. This allows you to edit multiple sections of the
      file at once. Access this by pressingCtrl++click to make multiple
      selections, and then any typing you do will be entered at all
      cursors. This is useful for doing a "poor man's refactoring" to
      rename a function or variable along with all occurrences.</p>
    <p>
      <img src="images/multi-cursor.png" alt="Multi-Cursor" />
    </p>
    <p>There are many more new goodies in the Orion language tools. A
      number of new JavaScript linting rules have been added, using the
      eslint code linting tool. Syntax highlighting has been added for
      several new languages, including Swift, C-Sharp, JSP, and VB.NET
      Many new rich editor hovers have also been added. For example if
      you hover or press F2 on a color definition in CSS or HTML, a
      preview of that color will be shown. Hovering on a CSS font
      declaration will show a preview of what that font looks like.
      Hovering on a JavaScript function will show jsdoc for that
      function declaration, if available. Finally, the entire suite of
      Orion JavaScript and CSS tooling has been made available for
      JS/CSS code that is embedded in HTML files.</p>
    <p>
      <img src="images/html-tools.png" alt="Tooling in HTML" />
    </p>
    <p>On the server side, the main focus for Orion 8.0 has been on
      clustering support. For a site with heavy traffic, it is useful to
      have multiple Orion instances operating on the same Orion
      workspace content. This also allows you to have a running standby
      instance for fail-over and upgrade without any site downtime. To
      support this, Orion 8.0 has introduced file-based locking to
      manage contention across multiple instances. In addition,
      persistent state has been divided between instance-private and
      shared locations, to prevent sharing of the server's own internal
      state that is not designed for sharing across instances.</p>
    <p>A final major change on the server side was completely rewriting
      our search infrastructure. The new Orion search implementation
      provides global search and replace based on a server side search
      crawler, rather than indexing. This means searches take a little
      longer, but they have perfect accuracy. Server side search now
      also supports regular expressions and case-sensitive search,
      making this kind of search much faster than before. This
      implementation also simplifies Orion server management for
      large-scale Orion installs, because it saves Orion server admins
      from having to deal with search index sharding and Solr
      clustering. This server side global search is currently only
      available on the Java implementation of the Orion server, but
      there is a team in the community working on a Node.js
      implementation for this as well!</p>
    <p>
      Overall, a ton of work has gone into this new release, and the
      Orion team is very excited to have it completed, and to be making
      it avaiable to the community. Please give this new Orion release a
      try, by registering for free at <a target="_blank"
        href="https://orionhub.org">OrionHub</a>. The release will also
      be available by end of February on the <a
        href="http://download.eclipse.org/orion/">Orion download page</a>.
      The team would love to hear any and all feedback, criticism and
      praise! Please get in touch on the <a
        href="http://dev.eclipse.org/mhonarc/lists/orion-dev">orion-dev</a>
      mailing list, or by entering bugs and enhancements in bugzilla.
      Enjoy!
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/february/images/john.jpg"
        width="90" alt="jay billings" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        John Arthorne<br />
        <a target="_blank" href="http://www.ibm.com/ca/en/">IBM</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Blog</a></li>-->
        <li><a target="_blank" href="https://twitter.com/jarthorne">Twitter</a></li>
        <!--<li><a target="_blank" href="https://www.linkedin.com/pub/max-rydahl-andersen">Lindedin</a></li>
        $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

