<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>It’s been four months since Eclipse Che was announced and it’s a
      good time to talk about what’s been happening. Che is an open,
      extensible way to execute distributed developer services (such as
      build, run, debug) packaged as a browser-based IDE.</p>

    <p>The exciting thing about open source is seeing other people’s
      visions turned into reality. Che has been pulled by the community
      in four directions:</p>

    <ol>
      <li><b>Che as a cloud IDE on the desktop:</b> Several
        organizations including WSO2 and Fuego are going to be deploying
        Che as a product on the desktop. They are generating their own
        plug-ins to create a custom IDE experience.</li>
      <li><b>Chromium packaging:</b> WSO2 has created a chromium
        distribution of Che that makes it possible to install Che as a
        desktop IDE with Tomcat executing internal to process the server
        requests.</li>
      <li><b>Che embedded within other products:</b> Software vendors
        are beginning to work with Che as the basis for embedded
        products that require an extensible cloud-based IDE.</li>
      <li><b>New plug-ins:</b> The Che community has been hard at work
        and will be releasing five new plug-ins for everyone to enjoy
        including Gradle and GitHub! We’re really excited about this
        participation and the addition of these great technologies.</li>
    </ol>

    <p>The success of cloud developer environments will hinge upon the
      development of an ecosystem. For those that aren’t yet familiar
      with Che it’s useful to start with the component model - we
      created a layered component model that gave customization
      capabilities within the SaaS public cloud or an ability to create
      entirely new desktop or cloud solutions outside of the public
      cloud. There are 5 components:</p>
    <ul>
      <li>Che SDK</li>
      <li>Plug-Ins</li>
      <li>APIs and CLI</li>
      <li>The Codenvy IDE (open sourced as part of Eclipse Che)
        including 55 plug-ins</li>
      <li>The Codenvy platform</li>
    </ul>

    <h2>Eclipse Che SDK</h2>
    <p>The SDK is a runtime framework for building, packaging and
      deploying applications in the form of extension combinations. Its
      architecture is similar to the Eclipse RCP, but uses GWT libraries
      to generate REST APIs and JavaScript for components that need to
      run within the browser IDE. The SDK is authored in Java and can
      run within most Servlet containers. There are plugins for various
      editors (CodeMirror, Orion, and Collide), AngularJS, Yeoman,
      Maven, NPM, Gulp, Grunt, Bower, git, Subversion, Datasource, SQL
      editing, Ant, SSH, Java, and Debugger.</p>

    <p>The Codenvy IDE is an example of an implementation of Eclipse
      Che. It’s a collection of 55 (and growing!) packaged extensions
      that add functionality to the system. We have provided default
      project types for many popular stacks, and implementations for
      core developer functions across the spectrum from editing through
      to deployment. The IDE can run locally on your desktop as a simple
      application inside of a servlet container. The IDE and every
      plug-in that has been generated by us, or a partner, is
      open-sourced as part of the Eclipse Che project.</p>

    <h3>Plug-Ins</h3>
    <p>Plug-Ins are packaged extensions that can add new capabilities or
      modify existing capabilities of the IDE or the developer
      environment cloud. We created the extension format in Java using
      dependency injection and GWT to minimize the conversion effort of
      plug-ins authored for Eclipse or any other IDE / editor. Plug-ins
      can modify either client-side components (windows, projects,
      source code) or server-side elements, such as runners, builders,
      and code assistants.


    <p>This HelloWorld extension prints a statement to the runner
      console, the same location that System output goes during a
      console application execution.


    <pre class="prettyprint lang-xtend">
@Extension(title=”Hello World”, version =”1.0.0”)
public class HelloWorldExtension {
  @Inject
  public HelloWorldExtension(ConsolePart console) {
    console.printf(“Hello World”);
  }
    }
</pre>
    <h3>APIs and CLI</h3>
    <p>Extensions are compiled into various assets. Extensions can
      generate server-side logic or they can generate cross-browser
      optimized JavaScript, which is loaded directly into the
      Codenvy/Che IDE. Finally, they can generate REST APIs that
      represent developer microservices, enabling remote clients
      (whether the browser-side portion of the extension, the CLI or
      other clients) to interact with the services exposed by this
      extension.</p>

    <p>Each API exposed as part of an extension can have multiple
      implementations based upon deployment environment. For example,
      the extensions that operate the builder and runner plug-ins spawn
      local processes to handle these requests when the plug-in is
      installed on a desktop, and they place job requests onto queues to
      be handled by our elves when operating within the Codenvy
      Platform. We also generate a CLI that interacts with these APIs
      providing command-line access to interacting with environments,
      making environment lifecycle operations possibly part of a broader
      automation process.</p>

    <h3>Codenvy Platform</h3>
    <p>Eclipse Che on its own is a single node system. All functions run
      within the context of the single server. It can potentially host
      1000s of concurrent developers on the same node instance, but it
      does have capacity limitations.</p>

    <p>While not part of Eclipse Che, the Codenvy Platform explodes the
      extensions that run within the Eclipse Che runtime kernel and
      provide scalability, security, and manageability to the system as
      a whole. A principle architectural concern of this platform is
      separation processing, designed to get the maximum density of
      users on the least amount of hardware. We take each operation of
      the developer process and route them to different microservices
      nodes, giving the developer effectively many computers in one. For
      example, a “build” request is routed onto a Builder Queue and
      serviced by a specialty set of Builder nodes dedicated to handling
      all builder functions for the entire cloud. The developer’s
      environment is not blocked or experience any thrashing from the
      editor while this builder job is executing. We perform separation
      process for editors, code assistants, builders, runners,
      datasource management, and external API invocations. It’s like 10
      computers in one. The platform runs on top of any IaaS provider,
      which gives it access to additional physical nodes on-demand to be
      allocated to the various clusters of hardware processing.</p>

    <p>For more information, tutorials, and getting started see the
      following:</p>
    <ul>
      <li><a href="https://eclipse.org/che/">Visit the Che project page</a></li>
      <li><a target="_blank" href="http://docs.codenvy.com/che/">Che
          Documentation</a></li>
      <li><a target="_blank"
        href="http://docs.codenvy.com/che/creating-extensions/#creating-che-apps">Write
          Your First Plug-In</a></li>
      <li><a target="_blank" href="http://docs.codenvy.com/apidocs/">SDK
          Libraries</a></li>
    </ul>

    <br />
    <iframe width="853" height="480"
      src="https://www.youtube.com/embed/fOsL8qFGT5U"
      allowfullscreen></iframe>


    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/prettify.js"
      type="text/javascript"></script>
    <script
      src="http://www.eclipse.org/xtend/google-code-prettify/lang-xtend.js"
      type="text/javascript"></script>
    <script type="text/javascript">
      prettyPrint();
    </script>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/february/images/tyler.jpeg"
        alt="tyler jewell" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Tyler Jewell<br />
        <a target="_blank" href="https://codenvy.com/">Codenvy</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/TylerJewell">Twitter</a>
        </li>
        <!--<li><a target="_blank" href="https://twitter.com/kichwacoders">Twitter Kichwa Coders</a></li>-->
        <li><a target="_blank"
          href="https://www.linkedin.com/in/tylerjewell">Linkedin</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

