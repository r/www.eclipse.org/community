<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Maximilian Koegel";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2015/february">February 2015</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2015/february/images/maximilian.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I lead the <a target="_blank" href="http://eclipsesource.com/munich">EclipseSource office in Munich</a>. Our mission is to make Eclipse Open Source software consumable for our customers - 
					through <a target="_blank" href="http://eclipsesource.com/en/services/">training, developer support, implementation, sponsored open source development and consulting</a>. Therefore we spent a great part of our daily work in Munich 
					on Eclipse Open Source projects, and once a year we spend a <a target="_blank" href="http://eclipsesource.com/blogs/2015/01/29/opensource-week-in-the-alps/">full week only working on open source</a>. 
					Apart from other Eclipse projects, we are involved in (e.g. e4) we have a strong focus on the Modeling Project and its subprojects. This includes 
					<a href="https://www.eclipse.org/modeling/emf/">EMF Core</a>, <a href="https://www.eclipse.org/ecp/emfforms/">EMFForms</a>, <a href="https://www.eclipse.org/ecp/">EMF Client Platform</a>, 
					<a href="https://www.eclipse.org/emfstore/">EMFStore</a>, <a href="https://www.eclipse.org/edapt/">Edapt</a>, and <a href="https://www.eclipse.org/emf/compare/">EMFCompare</a>, 
					where we have a leading role and/or are committers contributing on a regular basis.</p>

				<p>In addition to customer work, we invest at least 20% of each developer's time to open source. We consider this as an investment to the foundation 
					of our business, an important source of innovation, and a constant education and training for us as developers. And last, but not least, it is 
					very rewarding to contribute to the Eclipse Open Source community.</p>

					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I have already been using Eclipse as a student, more than 10 years ago. Towards the end of my studies I started developing the first plugins for Eclipse. During my Ph.D. 
					I developed the software underlying my thesis based on EMF, which was the basis for the <a href="https://www.eclipse.org/emfstore/">EMFStore</a> Eclipse project. 
					So like probably many of us at Eclipse, I grew from an IDE user over a framework user, to a framework developer in the Eclipse ecosystem.</p>
					
			<h3>Name five plugins you use and recommend:</h3>
					<p>This is difficult since there are so many, but if I had to limit myself to 6 plugins ;):</p>
					<ul>
						<li><a href="http://www.eclemma.org/">ECLEmma</a>: It is a Java code coverage tool integrated into JDT and facilitates a fast develop/test cycle of your JUnit Tests.</li>
						<li><a href="http://www.codetrails.com/blog/imagine-you-deliver-broken-software-but-no-one-tells-you">Automated Error Reporting</a>: Not really a plugin you would install 
						as an end-user, but it currently ships with the Mars milestones and allows you to report errors very easily. It even features a back-end where the reported errors are 
						aggregated and can be viewed by the projects committers.</li>
						<li><a href="https://www.eclipse.org/ecp/emfforms/">EMF Forms Tooling</a>: Allows you to declaratively define form-based UIs including validation, data binding, and many 
						other features in the Eclipse IDE with a visual editor and preview. You will never have to code a form manually any more.</li>
						<li><a href="https://wiki.eclipse.org/Eclipse_Oomph_Installer">Oomph</a>: Allows you to define the setup of your IDE including workspace setup and many settings and 
						make it available to other developers at the click of a button.</li>
						<li><a href="https://www.eclipse.org/rcptt/">RCPTT</a>: Recording UI test cases for SWT or RAP applications in addition to JUnit tests is a great way to reduce manual testing.</li>
						<li><a href="https://github.com/trylimits/Eclipse-Postfix-Code-Completion">Postfix Code Completion</a>: Offers many additional postfix code completions to improve your programming workflow.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>I like to ski preferably during an <a target="_blank" href="http://eclipsesource.com/blogs/2015/01/29/opensource-week-in-the-alps/">open-source week in the Alps</a> 
					and record speaker pitches ;):</p>		
					<iframe width="640" height="480" src="https://www.youtube.com/embed/KQliJCX7zNE" frameborder="0" allowfullscreen></iframe><br/>		
				
				<p>Apart from sports, I currently like to experiment with home automation; this also includes the brand new <a href="https://eclipse.org/smarthome/">Eclipse Smart Home</a> project. So when I leave my office now, 
					I will press a button which will turn off the lights and power everywhere, check if any of the windows are open or if the latest EMFForms Build has failed (and provide 
					a sound signal in case), turn off the heating and, upon closing the door, lock it twice.</p>
					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="http://eclipsesource.com/blogs/author/mkoegel/">Blog</li>
					<li><a target="_blank" href="https://plus.google.com/+MaximilianKoegel">Google +</li>
					<li><a target="_blank" href="https://twitter.com/mkoegel">Twitter</li>
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>