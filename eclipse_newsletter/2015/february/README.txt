
CONTENTS OF THIS FILE
---------------------

 * About Mailchimp
 * About Eclipse Newsletter Template
 * Using the Eclipse Newsletter Starterkit

ABOUT MAILCHIMP
---------------------------------

MailChimp makes it easy to design exceptional email campaigns, 
share them on social networks, integrate with web services you 
already use, manage subscribers, and track your results. 
You'll love mixing and matching MailChimp's templates, 
features, and integrations to suit your needs—think of 
it as your own personal publishing platform.

http://mailchimp.com/about/

ABOUT ECLIPSE NEWSLETTER TEMPLATE
---------------------------------

The Eclipse Newsletter is for Eclipse users who would like to 
stay up to date with everything Eclipse related. 

USING THE ECLIPSE NEWSLETTER STARTERKIT
---------------------------------------

Each month you will need to publish a new issue of the newsletter.

1-First copy the starterkit folder located at 
http://eclipse.org/community/eclipse_newsletter/starterkit/

2-Paste this folder in %CURRENT YEAR% folder. Eclipse Newsletter 
folder structure is based off the current date. For example 
the first issue of the newsletter was sent February 2013.

The starterkit needs to be pasted here :
http://eclipse.org/community/eclipse_newsletter/2013/

3- Rename the starter kit folder to the current issue month, 
for this example, the starterkit folder should be renamed to
February.
http://eclipse.org/community/eclipse_newsletter/2013/february/

4- Make the necessary changes to the text in the settings.php file. 
All new images should be located inside the images folder in the 
current issue folder. For example:
http://eclipse.org/community/eclipse_newsletter/2013/february/images/logo.jpg

IMPORTANT: You need to specify the full url for links and images otherwise they won't 
show inside the user's mail client.

5- Create each page for each article, please make sure that you link to the original 
article and additionally use this meta tag: <link rel="canonical" href="http://the article url" />
This should be done for you automaticaly if you set the $original_url variable 
but please make sure it's there!

6- Once you are done editing the newsletter, commit the newsletter,
wait until the code is propagated to the server and run build.php.
For example: http://eclipse.org/community/eclipse_newsletter/2013/february/build.php

9- Copy the source code of build.php (Right Click + View Page Source) and paste it
in the index.html file. Please remember to commit index.html. 
You will also need to paste the output in mailchimp.

9- Send out the newsletter to Eclipse Foundation testing list for testing.
Make sure to test all links and images.

10- If all goes well you are ready to set your Newsletter free into the wilderness!