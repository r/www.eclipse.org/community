<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div id="fullcolumn">
  <div id="midcolumn">
    <h1><?php echo $pageTitle; ?></h1> <img align=right src="/community/eclipse_newsletter/2015/november/images/tracy.png"/>
	    
			<h2>What do you do?</h2>
			<p>I  am founder of <a target=="_blank" href="http://kichwacoders.com/">Kichwa Coders</a>, a software consultancy for Eclipse tools for science and embedded software. 
			I started the company 8 years ago to focus more on solving different clients' technology problems with open source. 
			With my background in electronics, the focus was first on embedded tools, and then more recently the equally-challenging 
			scientific tools.  I like how Eclipse gives you access to lots of different technology, so some days I'm 
			<a target="_blank" href="https://www.youtube.com/watch?v=KcVozMo--M4">integrating 
			Java & Python to solve problems in the scientific space</a>, other days I'm 
			<a target="_blank" href="http://kichwacoders.com/2014/06/20/lean-design-critique-talk-at-eclipsecon-france-2014/">empowering engineers to 
			improve their tool usability</a> and still others I'm experimenting with 
			<a target="_blank" href="https://www.youtube.com/watch?v=vqd_uFFf5Zs">fun prototypes for Internet of Things solutions</a>.</p>
								
			<h2>How long have you been using Eclipse?</h2>
			<p>Since 2003. The company I worked for was choosing between using Eclipse or building their own IDE. Fortunately I was able 
			to help convince them to back Eclipse, and that saved me from having to reinvent dockable windows.  My very first tech conference was 
			EclipseCon 2004, held in of all places, Disneyland! It was great. I'd like to think I went into the conference a hardware engineer 
			and came out a software engineer... well at least I learnt all about APIs.  Over the next ten years I attended many more conferences, 
			and in 2015 I was on the programme committee for EclipseCon France.  Working with a great team, I got to ensure there was good content 
			for the newbies, the hardware engineers, the scientists, well everyone really.  It was terrific, like coming full circle and seeing 
			how the more you put into the community, the more you get out of it.</p>
							
			<h2>Name five plugins you use and recommend:</h2>
			<ul>
			<li><a href="https://eclipse.org/cdt/">CDT</a>: my very first open source project, and at the time I was one of the first participants in CDT from 
			outside North America. I just love it. CDT drives innovation in the embedded world - small hardware start-ups focus on their 
			core technology knowing CDT gives them great tools to compete with the big names. </li>
			<li><a target="_blank" href="http://www.pydev.org/">PyDev</a>: the Python development tools for Eclipse that are so awesome they are the driving
			 force behind commercial tools like PyCharm. PyDev is huge in science.</li>
			<li><a target="_blank" href="http://andrei.gmxhome.de/anyedit/">AnyEdit</a>: Adds to Eclipse some of the key missing features that arguably 
			the IDE should have as built in. The additional items in the Compare to menu, such as to Opened Editor or Clipboard are part of my regular use.</li>
			<li><a target="_blank" href="https://github.com/mbarbero/fr.obeo.releng.targetplatform">Target Platform Definition DSL and Generator</a>: On the one 
			hand this plug-in is a key tool for anyone managing target files. It makes target files (via the custom .tpd file) 
			a text editable file with some nice features (autocompletion, outlines, validation, etc.) . On the other hand, this plug-in exemplifies the rich 
			tool set of the Eclipse ecosystem that you can build such a tool by leveraging things like EMF & XText. This one plug-in perfectly epitomizes 
			what I love about the rich Eclipse ecosystem.</li>
			<li><a href="https://eclipse.org/ease/">EASE</a> :the Eclipse Advanced Scripting Environment is a relatively new kid on the block, 
			but one with big ambitions. It is trying to plug one of the biggest holes in the Eclipse IDE ecosystem: the lack of a standard IDE 
			scripting environment. Users coming from other tools such as Visual Studio have long expected such a feature, so it is a delight that 
			EASE is coming along nicely now. I'm also looking forward to working more with EASE in 2016 to solve Python scripting & macros for Eclipse.</li>
			</ul>
			
			<h2>What's your favorite thing to do when you're not working?</h2>
			<p>When I need to hit things I play badminton (at least a couple of times a week!). I also am part of Toastmasters - another great open 
			community that's great for personal development, not just for public speaking but for leadership as well. But really when I'm not working 
			most of my time is spent with my two young kids. Parenting life and working in tech often don't play well together - it's the biggest 
			integration challenge ever and I'm still trying to figure it out . But it's worth it watching the kids grow & discover their world - 
			they are so creative, so inspiring and so utterly exhausting.</p>	
			
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://twitter.com/tracymiranda">Twitter</a></li>
					<li><a target="_blank" href="https://www.linkedin.com/in/tracymiranda">Linkedin</a></li>
				</ul>
		</div>
		</div>
	</div>