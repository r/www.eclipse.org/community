<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
   <p>Need to provide a better visualization experience to the users of your modeling tools? Download
   <a href="http://www.eclipse.org/sirius/whatsnew3-1.html">Sirius 3.1</a> and build awesome
   diagrams with new graphical options, including compartments and customized border styles.</p>

	<p>With Sirius 3.0, it was already possible to create compartments, but we had introduced it as an "experimental" feature
	since some behaviors were not supported yet. Now, with Sirius 3.1,  compartments are fully operational. They allow you to
	group and stack your model elements within a container. With a better representation, your information is easier to understand!</p>

	<p>Containers are diagram elements that contain sub-elements, displayed in a list or with shapes (squares, circle, images, etc)
	arranged according to user needs. Compartments can be used to group these sub-elements into predefined regions within the container.</p>

	<h2>When to use containers?</h2>
	<p>Let’s consider this diagram that represents persons and their parental relationships.</p>

	<p align="center"><img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/peoplediagram.png"/></p>

	<p>On this diagram, if you focus on Elias, you can directly see his parents (Paul and Isa) and his children (Bryan, Clara, Dave and Fiona).
And, as you understand the semantic of the father and mother relationships, you can easily guess who is his sister (Léa), and who are his
grandchildren (John, Mary, Sophia, Emma, Fred and Alain).</p>

	<p>Rather than representing these “indirect” relationships on this diagram (which would rapidly lead to a real spaghetti nightmare!)
	you can create an alternative object-centered representation where a person is represented by a container and relationships by compartments:
	fixed compartments for parents, siblings and children, and a dynamic compartment for the grandchildren that groups grandchildren by their parents.</p>

	<p align="center"><img class="img-responsive"src="/community/eclipse_newsletter/2015/november/images/elias.png"/></p>

	<p>This new diagram doesn't replace the first diagram with edges. It is just complementary, providing a more synthetical representation.</p>

	<h2>How does it work?</h2>

	<p>To get this new diagram, Sirius 3 has introduced two new options for children representation: "Horizontal Stack" and "Vertical Stack".
	These options define how sub-containers will be organized.</p>

	<p align="center"><img class="img-responsive"src="/community/eclipse_newsletter/2015/november/images/personcontainer.png"/><br/></p>

	<p>To read more about these compartments, I have written a <a href="https://wiki.eclipse.org/Sirius/Tutorials/CompartmentsTutorial">Compartments Tutorial</a>
	that explains, step-by-step, how to create the example that illustrates this post.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/fmadiot.jpg"
        alt="fred madiot" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Frédéric Madiot
      <br />
        <a target="_blank" href="http://www.obeo.fr/en">Obeo</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fmadiot">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://fmadiot.blogspot.ca/">Blog</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
