<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

	<img align="right" src="/community/eclipse_newsletter/2015/november/images/logo_emf_incquery.png" class="img-responsive" alt="emf incquery logo" />
	<p><a href="https://www.eclipse.org/incquery/">EMF-IncQuery</a> is a state-of-the-art incremental graph query framework based on:

	<ol>
		<li>an expressive declarative language that developers can use to capture queries</li>
		<li>a scalable query engine to execute live queries over models</li>
		<li>an extensive automation to seamlessly integrate incremental queries into EMF applications</li>
	</ol>

	<h2>Why EMF-IncQuery?</h2>
	<p>The leading industrial modeling ecosystem, the Eclipse Modeling Framework (<a href="http://www.eclipse.org/modeling/emf/">EMF</a>),
	provides different ways for querying the contents of models. These approaches range from hand-coded model traversal to high-level
	declarative constraint languages such as <a href="http://www.eclipse.org/modeling/mdt/?project=ocl">Eclipse OCL</a>.
	However, industrial experience <a target="_blank" href="https://inf.mit.bme.hu/sites/default/files/publications/models10-submitted.pdf">shows</a>
	strong evidence of scalability problems in complex query evaluation over large EMF models, and manual query optimization is time consuming to
	implement on a case-by-case basis.</p>

	<p>In order to overcome this limitation, the EMF-IncQuery framework proposes to capture queries over EMF models declaratively and
	execute them efficiently without manual coding using incremental graph pattern matching techniques. The benefits of EMF-IncQuery
	with respect to the state-of-the-art of querying EMF models include:</p>
	<ul>
		<li>high-level declarative query language based on graph patterns supported by advanced Xtext based editor,</li>
		<li>a highly efficient query engine capable of
		<a target="_blank" href="https://github.com/FTSRG/publication-pages/wiki/Benchmarking-query-technologies-in-model-driven-scenarios">evaluating queries</a>
		over models with millions of elements within a few milliseconds,</li>
		<li>an advanced integrated development environment to ease the construction and validation of model queries.</li>
	</ul>

	<p>In addition, EMF-IncQuery <a href="http://wiki.eclipse.org/EMFIncQuery/UserDocumentation/API/BaseIndexer">efficiently addresses several shortcomings</a>
	of the EMF API (such as backward navigation or instance enumeration).</p>

	<h2>Writing my first EMF-IncQuery graph patterns</h2>

	<p>For the query language, we reuse the concepts of graph patterns (a concepts similar to datalog/prolog clauses) as a concise and easy way to
	specify complex structural model queries. These graph-based queries can capture interrelated constellations of EMF objects, with the following benefits:</p>
	<ul>
		<li>the language is expressive and provides powerful features such as negation or counting,</li>
		<li>graph patterns are composable and reusable,</li>
		<li>queries can be evaluated with great freedom, i.e. input and output parameters can be selected at run-time,</li>
		<li>some frequently encountered shortcomings of EMF’s interfaces are addressed:</li>
	<ul>
			<li>easy and efficient enumeration of all instances of a class regardless of location,</li>
			<li>simple backwards navigation along all kinds of references (even without eOpposite)</li>
			<li>finding objects based on attribute value.</li>
	</ul>
	</ul>

	<h3>Example</h3>
	<p>To illustrate the pattern language of EMF-IncQuery we present a set of patterns in Figure  1(a) for identifying empty classes in UML
	(from our <a target="_blank" href="http://incquery.net/blog/2015/04/live-querying-over-uml-models-emf-incquery">UML case study</a>):
	classes that do not have operations or properties (not even in their superclasses), suggesting an incomplete model.
	However, if the name of the class is postfixed with the string “Empty”, we consider the class empty by design, so it is not returned.</p>

	<div class="row">
  <div class="col-md-12"><img src="/community/eclipse_newsletter/2015/november/images/1_sourceCode.png" class="img-responsive" alt="source code" /></div>
  <div class="col-md-12"><img src="/community/eclipse_newsletter/2015/november/images/1_exampleInstanceModel.png" class="img-responsive" alt="example instance model" /></div>
	</div>

	<p align="center"><i>Figure 1: (a) example graph pattern code (b) UML class model</i></p>

	<ul>
		<li>The <b>SupEmpty</b> class is not considered empty because of its name, while the classes <i>B</i>, <i>C</i> and <i>D</i> either define or
		inherit the property called <i>refers</i>.</li>
		<li>The pattern <b>superClass</b> in Line 1 consists only of structural constraints: it describes the direct superclass relation by a generalization node
		(local variable gen) that is connected both to the classes referenced as sub and sup.</li>
		<li>The pattern <b>hasOperation</b> in Line 7 consists of the disjunction of two bodies: one represents the fact that the selected class cl holds an
		<i>Operation</i>. The second body uses the transitive closure of the relation defined by the superClass pattern in Line 10 to select the indirect
		<i>superclasses</i> of a selected class, and then declares that the superclass <i>owner</i> holds an Operation.</li>
		<li>Finally, the pattern <b>emptyClass</b> in Line 14 selects classes without operations and properties by evaluating two corresponding neg calls,
		which means that the parent query evaluates to true if those called patterns cannot be matched to the underlying model (for the sake of
		simplicity the <i>hasProperty</i> pattern is omitted as it works exactly the same as the presented <i>hasOperation</i>). The second parameters of the
		pattern calls are single-use variables (starting with the ‘_’ symbol), so these NACs are simple non-existence checks. The check expression
		in the Line 18 reuses the <i>String.endsWith</i> Java method on a local variable.</li>
	</ul>

	<h2>What is under the hood - the development environment</h2>
	<p>The development workflow of the EMF-IncQuery framework focuses on the specification and evaluation of queries and the automatic generation of
	integration code for plugging into existing EMF-based applications. As depicted in Figure 2, the development environment offers three major
	components: (1) the Graph Pattern Editor, (2) the Query Result Explorer and (3) the Pattern Code Generator.</p>

	<img src="/community/eclipse_newsletter/2015/november/images/2_Architecture.png" class="img-responsive" alt="architecture incquery" />
	<p align="center"><i>Figure 2: EMF-IncQuery architecture overview</i></p><br>

	<p><b>Graph pattern editor</b> - The EMF-IncQuery development environment provides an Xtext-based editor for the pattern language with syntax
	highlighting, code completion and well-formedness validation. The editor is tightly integrated with the other components: the code generator
	is integrated into the Eclipse builder framework, and is executed after changes in pattern definitions are saved (unless Eclipse automatic
	builders are turned off), while the Query Explorer updates the displayed query results.</p>

	<p><b>Query result explorer</b> - In order to evaluate complex model queries the EMF-IncQuery provides the Query result explorer. This component
	visualizes live query results of both interpretative and generated pattern matchers in a generic view, and provides a quick feedback
	cycle during transformation development.</p>

	<p><b>Pattern code generator</b> - The environment also helps the integration of queries into a Java application by maintaining a project with
	pattern-specific generated matcher code. The generated matcher is semantically equivalent of the interpretative one, but provides an
	easy-to-integrate type-safe Java API, and some performance optimizations are also executed. Furthermore, the generator may also produce
	code for various integration components, such as the data binding support, validation framework or query-based features.</p>

	<p>To see it in action Figure 3 shows the EMF-IncQuery development environment while developing an EMF-UML2 based validation plugin
	(as in our pattern example). On the left side the used model and <i>plug-in projects</i> are shown. As EMF-IncQuery projects are plug-in
	projects, their management relies on already existing Eclipse features. On the right, the <i>Query Editor</i> is open next to the Papyrus
	UML editor in the middle that contains a sample model for evaluating the queries currently developed. Finally, in the bottom of the
	screen the <i>Query Explorer</i> has already loaded the model and the queries from the editors, and reacts on changes in any of the editors.</p>


	<p align="center"><a href="/community/eclipse_newsletter/2015/november/images/3_ide.png">
	<img src="/community/eclipse_newsletter/2015/november/images/3_ide_small.png" class="img-responsive" alt="incquery ide" /></a></p>
	<p align="center"><i>Figure 3: EMF-IncQuery development environment</i></p>

	<h2>Cool features based on incremental query evaluation</h2>
	<p>What is important to see that incremental query evaluation is an enabler technology to many advanced modeling features that can benefit
	from the blazing fast query reevaluation. The following list provides an overview on some of those:</p>

	<ul>
	<li><a href="http://wiki.eclipse.org/EMFIncQuery/UserDocumentation/Validation">On-the-fly model validation</a>:
	EMF-IncQuery provides facilities to create validation rules based on the pattern language of the framework.
	These rules can be evaluated on various EMF instance models and upon violations of constraints, markers are automatically created in the
	Eclipse Problems View. Used in an Autosar context for
	<a target="_blank" href="https://github.com/FTSRG/publication-pages/wiki/MODELS2010-Incremental-Model-Queries-over-EMF-Models">validating large models</a>
	loaded into the editor.</li>
	<li><a href="http://wiki.eclipse.org/EMFIncQuery/UserDocumentation/Query_Based_Features">EMF query-based features</a>:
	EMF-IncQuery supports the definition of efficient, incrementally maintained, well-behaving (meaning that all
	the derived features act as any ordinary EStructuralFeature and whenever changes throws proper Notifications) derived features in EMF.
	Based on incremental evaluation for calculating the value of derived features and providing automated code generation for integrating into
	existing applications. This approach is extensively used in our
	<a target="_blank" href="https://github.com/FTSRG/massif">Massif project</a> to provide easier navigation on Matlab Simulink models.</li>
	<li><a href="http://wiki.eclipse.org/EMFIncQuery/UserDocumentation/IncQuery_Viewers">Model visualization made easy</a>:
	The goal of the IncQuery Viewers component is to help developing model-driven user interfaces by filling
	and updating model viewer results with the results of model queries. The Viewers component can bind the results of queries to various
	visualization frameworks like JFace, GEF4 Zest and it also incorporates a specific
	<a target="_blank" href="https://www.eclipsecon.org/europe2015/session/incquery-gets-sirius-faster-and-better-diagrams">Sirius integration</a>
	to support interpreted expressions.</li>
	<li><a href="http://www.eclipse.org/viatra/">Incremental model synchronization</a>: The common recurring task in any tool, framework or
	application based on model-driven concepts is to capture and process not only the underlying models, but also their changes as a stream
	of events (operations that affect models). We generalized this approach to provide a common conceptual framework for defining reactive
	model transformations/processors based on an
	<a href="https://wiki.eclipse.org/EMFIncQuery/DeveloperDocumentation/EventDrivenVM">event-driven virtual machine</a>
	(EVM) architecture. This approach provides the basis for our <a href="http://www.eclipse.org/viatra/">Viatra</a> reactive model transformation engine
	(<a target="_blank" href="https://www.eclipsecon.org/france2015/session/decreasing-your-coffee-consumption-incremental-code-regeneration">a short introduction</a>).</li>
	</ul>

	<h2>Summary</h2>
	<p><a href="https://www.eclipse.org/incquery/">EMF-IncQuery</a> is an incremental graph query engine based on a declarative pattern language
	to capture and execute live queries over models. It provides up-to-date results and result change notifications as the models evolve.
	EMF-IncQuery is powered by a highly scalable engine capable of executing complex queries over large models (10M+ elements) in a few milliseconds.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/akos_horvath.jpg"
        alt="akos horvath" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Akos Horvath<br />
        <a target="_blank" href="http://www.incquerylabs.com/">IncQuery Labs</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.researchgate.net/profile/Akos_Horvath5">ResearchGate</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://hu.linkedin.com/in/akoshorvathincqurylabs">Linkedin</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://incquery.net">IncQuery Blog</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/istvan_rath.jpg"
        alt="Istvan Rath" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Istvan Rath<br />
        <a target="_blank" href="http://www.incquerylabs.com/">IncQuery Labs</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/istvanrath">Twitter</a></li>
      <li><a class="btn btn-small btn-warning" target="_blank" href="http://hu.linkedin.com/pub/istvan-rath-phd/4/6b6/98b">Linkedin</a></li>
      <li><a class="btn btn-small btn-warning" target="_blank" href="http://incquery.net">IncQuery Blog</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/daniel_varro.jpg"
        alt="Daniel Varro" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Daniel Varro<br />
        <a target="_blank" href="http://www.incquerylabs.com/">IncQuery Labs</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://scholar.google.hu/citations?user=4Ya6dVoAAAAJ&hl=en&oi=ao">Google Scholar</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/daniel-varro-3854bb49">Linkedin</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://incquery.net">IncQuery Blog</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

