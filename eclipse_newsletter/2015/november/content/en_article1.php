<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Over three years ago, we started the explicit development of EMF Forms as a sub component of the EMF Client Platform.
    The goal was to ease the development of data-centric form-based UIs based on a given EMF data model. Rather than manually
    coding UIs (e.g. in SWT), the approach is to describe them declaratively in a simple model language, which is focussed on the
    specification of forms – the View Model. A view model instance is then interpreted by a flexible and extensible rendering component
    to create a working UI at the end.</p>
	<p>The approach has been a major success and shows significant advantages over manual UI programming or the usage of WYSIWYG editors.
	Besides the lower development effort and the higher quality of the resulting form-based UI, another advantage is the technological
	flexibility of the approach. The view model, which specifies the UI is not depending on a certain UI toolkit (e.g. SWT, GWT or JavaFX).
	Implementing new renderers allows you to switch the UI technology without respecifying the concrete UI itself. With renderers for the
	Remote Application Platform and Vaadin, EMF Forms is already used for web applications.</p>
	<p>EMF Forms has grown to a very active, frequently used project. This success motivates us to continuously drive the technology
	forward, extend its use cases, and, in doing so, attract new users. An obvious new field for applying the concepts of EMF Forms
	is the implementation of data-centric web applications. More and more complex business applications are developed as single-page
	web applications using JavaScript and frameworks such as AngularJS and EmberJS. These clients are then connected to backends
	using Restful Services. This JavaScript based area opens the gate for a large group of new users and use cases. Therefore, it
	is a consequent step to implement an AngularJS based renderer for EMF Forms.</p>
	<p>The former eponym “EMF” is rather unknown in the web area. Additionally, it loses its central role for defining the data
	schema and the UI schema. Therefore, a new name was required: JSON Forms. However, it is not difficult to guess, which technology
	takes over the role of EMF.</p>

	<h2>What happened before?</h2>
	<p>Before we talk about the world of JavaScript, we would like to take a brief look back on the main concepts of <a href="http://www.eclipse.org/ecp/emfforms/">
	EMF Forms</a>. While with <a target="_blank" href="http://github.eclipsesource.com/jsonforms/#/localdemo">JSON</a>
	Forms we switch to a new technology stack, the basic idea and the basic concepts remains the same as in EMF
	Forms. If you are already familiar with EMF Forms, you can probably skip this section.</p>
	<p>Both frameworks are based on the fact that typical UI toolkits are not focussed on the implementation of form-based UIs.
	Therefore, they make things unnecessarily complicated and require too much effort. For a typical input field, displaying a
	String attribute, you need to implement a label as a text field and possibly validate it as well as the binding to the
	underlying data entity. This has to be repeated for all required fields in a form.</p>
	<p>In a declarative language, like the one provided by EMF/JSON Forms, you only need to specify the existence of a control,
	which references an attribute of the data schema. The whole creation of a fully functional UI, including validation and
	binding is then done by a rendering component.</p>
	<p>Analogously, layouts are specified, in which controls can be embedded. As for controls, the declarative approach focuses
	 on concepts, which are typically required in form-based UIs. Instead of complex layouts, such as GridLayout, the declarative
	 language provides simple concepts such as groups or columns. This abstraction makes the specification of UIs much simpler
	 and more efficient. Additionally, the central rendering component, which replaces a great deal of the manually written UI
	 code, improves the adaptability and maintenance. Any desired change to the UI must be applied only once on the renderer.
	 Further information on EMF Forms can be found <a href="https://www.eclipse.org/ecp/emfforms/">here</a>.</p>
	<p>Last but not least, the declarative description of the UI is technology independent. Only the renderer component is bound
	to a specific toolkit. Therefore, it is possible to migrate existing UIs to new technologies. JSON Forms provides a complete
	new technology stack, based on HTML, CSS, JavaScript, JSON, JSON Schema and AngularJS. However, our goal is to keep the
	declarative description compatible, so it is also possible to reuse existing “view models” from EMF Forms on the new stack.</p>

	<h2>A new technology stack</h2>
	<p>As mentioned before, the general concept of EMF Forms has been maintained. The declarative way of describing UIs and the central
	rendering approach provide the same advantage and can be efficiently implemented in an client server oriented browser
	application. To display a form, JSON Forms still needs three artefacts: the definition of the displayed entity (Data Model),
	the declarative description of the UI (View Model) and finally the data entity to be displayed (Data Model Instance). In EMF
	Forms, all those artefacts are modelled in EMF. As the name already implies, we use JSON instead of EMF for JSON Forms. More
	precisely, we use JSON Schema for the data model, and JSON objects for the view model and the data entity. Therefore, in JSON
	Forms, we use the terms “data schema” and “ui schema” instead of “data model” or “View Model”.</p>
	<p>The following example shows a very simple data schema, which defines a data object with only two attributes. The specified
	entity shall be displayed in a form-based UI later on. That means, there are controls to enter both attributes “Name” and “Gender”.
	The schema defines the possible values for “Gender”, and is an enumeration. The “Name” field is specified as mandatory. Both
	constraints are considered by JSON Forms.</p>

<pre>
{
  "type": "object",
  "properties": {
    "name": {
      "type": "string"
    },
    "gender": {
      "type": "string",
      "enum": [ "Male", "Female" ]
    }
  },
  required:["name"]
}
</pre>


	<p>The schema only describes the data to be displayed, it does not specify how the data should be rendered in the form.
	This is specified in a second JSON-based artifact, the “UI Schema” (see listing below). The ui schema references the data
	schema, more precisely the attributes defined in the data schema. The ui schema element “Control” specifies, a certain attribute
	shall be rendered at a specific location in the UI. Therefore, it references the specific attribute from the data schema (see the
	JSON attribute “scope” in the following example). The renderer is responsible for displaying the attribute now,  accordingly, it
	also selects a UI element, e.g. a text field for a string attribute. Therefore, you do not need to specify, that there should be
	a drop down for the “Gender” attribute or which values it should contain. Also, you do not need to specify additional labels, if
	the labels should just show the name of the attribute. If you want to show another label, you can optionally specify it in the ui
	schema. As you can see, JSOn Forms derives a lot of information directly from the data schema and therefore reduces duplicate specification.</p>
	<p>The ui schema also allows to structure controls with container elements. This allows to specify a logical layout. As a simple example is
	a HorizontalLayout, which displays all children elements next to each other. As in EMF Forms, there are more complex layouts in practice,
	for example groups, stacks, or multi-page forms. However, the basic concepts remains the same, the ui schema specifies, in a simple way,
	how the form-based UI is structured, the rendering component takes care of creating a concrete UI. The following example ui schema shows
	two controls structured in a horizontal layout:</p>

<pre>
{
  "type": "HorizontalLayout",
  "elements": [
    {
      "type": "Control",
      "scope": {
        "$ref": "#/properties/name"
      }
    },
    {
      "type": "Control",
      "scope": {
        "$ref": "#/properties/gender"
      }
    }
  ]
}
</pre>

	<p>This simple and concise specification of the data schema and the ui schema is already enough to render a fully functional form-based UI.
	This is done by the JSON Forms rendering component describe in the following section.</p>


	<h2>Rendering</h2>
	<p>The two artifacts, the data schema and the ui schema, are now rendered to a UI. That is done by the rendering component. In JSON Forms,
	it uses HTML for defining the visible elements of the UI and JavaScript for the implementation of any behavior, such as data binding and
	validation. To ease the implementation and also offer state-of-the-art features such as bi-directional data binding, we additionally use
	the framework AngularJS. It has a growing user base since its publication in 2012, which is already a long period in the volatile area
	of web frameworks.</p>
	<p>The rendering component consists of several registered renderers. Every renderer is responsible for rendering a specific element of
	the ui schema. This allows the modular development of new renders. Additionally, specific renderers can be replaced.</p>

	<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/json_forms.png"/>

	<p>A frequently raised question is, why AngularJS alone is not enough to efficiently develop form-based Web UI. AngularJS definitely
	provides a lot of  support to the development of web UIs, however, implementing the desired feature manually is still required.
	To implement the example above, you have to complete several manual steps in AngularJS. First you have to create an HTML template
	defining all UI elements such as the text box, the labels and the drop down element. This can lead to complex HTML documents,
	especially for larger forms. Now, you have to manually set the AngularJS directives on those elements to bind the data and to
	add validations. Finally, to achieve a homogenous look & feel, you need to layout and alling all created elements, typically
	with CSS. That means, even for defining a very simple form, you have to deal with three different languages, JavaScript, HTML
	and CSS. In contrast, when using JSON Forms, you just have to define the data schema and ui schema, both done in a simple JSON
	format. The rendered form just needs to be embedded into an existing web page using a custom directive.</p>

	<p>The declarative approach of JSON Forms especially pays off, in case the underlying data schema is extended or changed.
	In this case, you just need to slightly adapt the ui schema, e.g. by adding a new control. Another advantage is that there
	is one implementation per UI element, enabled by the renderer registry. This allows you to adapt the form-based UI at a central
	place in a homogenous way. As an example, if you want to add a specific behavior to text fields, you just need to adapt the
	renderer for string attributes.</p>

	<p>Therefore, JSON Forms allows to adapt and replace existing renderers. Renderers can be registered for the complete application,
	 e.g. for all text fields, or alternatively, for specific attributes, e.g. only for the string attribute “name”. As an example,
	 if you want to show the Enumeration “Gender” from before as radio buttons instead of a drop down box, you could adapt the renderer
	 for Enumerations. Alternatively, you can even depend on a certain condition, e.g. you can adapt the renderer for all Enumerations
	 with only two possible values.</p>

	 <img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/jsonform2.png"/>

	 <p>After the renderer has created a read-to-use UI, the questions remains open, how this can be used in an existing application?
	 In the following section, we describe how to embed JSON Forms into any web application.</p>

	<h2>Embedded</h2>
	<p>To use the rendered form at the end, it is typically embedded into an existing application. As for EMF Forms, our focus is on a
	not invasive integration. That means, it is possible to integrate the framework as easily into an existing application. Additionally,
	it should integrate well with existing frameworks, such as Bootstrap.</p>
	<p>For embedding EMF Forms into a HTML page, we use a specific directive (as often done in AngularJS). The JSON Forms directive
	specifies the data schema, the data entity as well as the ui schema to be shown (see following code example). The values of the
	attributes “schema”, “ui-schema” and input must be in the scope of the current controller. Therefore, they can be retrieved from
	any kind of source, which allows full flexibility in connecting a backend. In a typical application, the data schema and the ui
	schema would be static content, while the data entity is retrieved from a REST service.</p>

<code>&#60;jsonforms schema=”mySchema” ui-schema=”myUiSchema” input=”myDataObject”/&#62;</code>

	<h2>Tooling</h2>
	<p>In contrast to EMF Forms, JSON Forms uses JSON to represent the ui schema. The default JSON serialization is much easier to
	read than the EMF one. Further, JSON is a de-facto standard in the JavaScript world. A first ui schema can easily be created
	using any kind of text editor. However, one good tooling for creating and modifying View Models (ui schemata in JSON Forms)
	has been an important success factor for EMF Forms. As an example, the EMF Forms tooling allows one to generate a complete
	default view model based on a given data schema. This default model can then be iteratively adapted. This feature reduced
	the initial effort for creating a form-based UI even more.</p>
	<p>Of course we also want to support the user as much as possible to specify ui schemata in JSON Forms. The first good news
	is that you can directly export existing view models from EMF Forms to JSON forms. That allows you to reuse any existing
	view model as well as using the existing and well-known tooling for the creation of new ui schemata. Beside the ui schema,
	we also provide an export from an existing EMF model to a json data schema. Therefore, you can reuse the existing features
	of the EMF Forms and EMF/Ecore tooling.</p>
	<p>To export view models and Ecore models to JSON Forms, EMF provides a new option in the right click menu on both artefacts.
	This has been introduced in the <a target="_blank" href="https://www.eclipse.org/ecp/emfforms/download.html">1.8.x development stream</a>. Further documentation on how the ui schema can be used with JSON
	Forms then can be found on the <a target="_blank" href="http://github.eclipsesource.com/jsonforms/#/localdemo">JSON Forms homepage</a>
	 and the <a target="_blank" href="https://github.com/eclipsesource/jsonforms/wiki/EMF-Forms-Integration-Guide">EMF Forms / JSON Forms integration guide</a>.</p>
	<p>On a medium term, we of course also want to address user groups outside of the Eclipse ecosystem with JSON Forms. Therefore,
	we are working on a ui schema editor as a pure web application. As for EMF Forms, the editor is of course based on JSON Forms
	itself. Therefore the framework is bootstrapping itself.</p>

	<h2>Conclusion</h2>
	<p>With JSON Forms, EMF Forms enters the JavaScript world and the area of typical single page web applications. The well-proven concepts of
	EMF Forms are transferred seamlessly. The implementation is adapted to the new context and the requirements of typical web applications.
	Technically, JSON Forms is based on HTML, CSS, JavaScript, JSON, JSONSchema und AngularJS. JSON Forms can be used independently of Eclipse.
	However, you can reuse the data models and view models from EMF Forms. Therefore it should be easy for existing EMF Forms users to get started
	with JSON Forms. The most important concepts of EMF Forms are already supported in JSON Forms, we are actively working on completing the framework.</p>
	<p>The development of JSON Forms does not mean we will lose our focus on EMF Forms. We plan on continuously driving it forward and extending its
	 vital user base.</p>
	<p>One advantage of the declarative approach is especially relevant in the web area, i.e. for JSON Forms: The independance of a UI toolkit.
	If you manually develop web UIs, you bind the application to a specific JavaScript framework, e.g. AngularJS or Ember.JS. This is a technical
	risk, especially in the area of web framework. Angular has been pretty popular for the past 3 years, which is already a long time for a
	JavaScript framework. However, at the end of 2015, the next major version will be published, which is not fully backwards compatible.
	With JSON Forms, you partially mitigate this risk. The actual specification of your custom forms is one in a declarative and UI technology
	independant JSON format. By adding new renderers, you can reuse this specification with new JavaScript frameworks or new versions of them.</p>
	<p>If you want to try JSON Forms, <a target="_blank" href="http://github.eclipsesource.com/jsonforms/#/localdemo">please visit our website</a>, it provides
	tutorials and a running live example, where you can specify a ui schema online. Further information about EMF Forms and first steps with
	JSON Forms can be also found on the <a href="http://www.eclipse.org/ecp/emfforms/">EMF Forms website</a>. Finally, we will soon start a blog series,
	which provides detailed tutorial how to implement complex forms with JSON Forms based on an example
	application. if you want to learn about the on-going development of JSON Forms, please <a target="_blank" href="https://twitter.com/EclipseSourceM">follow us on twitter</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/jonasandmax.jpg"
        alt="Jonas and Maximilian" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Maximilian Koegel<br />
       Jonas Helming<br />
        <a target="_blank" href="http://eclipsesource.com/en/about/company/eclipsesource-munich/">EclipseSource Munich</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://eclipsesource.com/blogs/">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/EclipseSourceM">Twitter</a></li>
        <li><?php echo $og; ?></li>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

