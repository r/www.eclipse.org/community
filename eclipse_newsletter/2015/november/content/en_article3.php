<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

<a href="https://www.eclipse.org/emf-parsley/"><img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/logosx.jpg"></a><br>

<h2>Introduction</h2>

<p>Eclipse Modeling Framework (EMF) models are easily maintainable, useful, pluggable, amongst other things, but building
user interfaces (UIs) from a model wasn't always straightforward. This is why <a href="https://www.eclipse.org/emf-parsley/">EMF Parsley</a>
 was developed to make creating UIs from EMF models just as easy.</p>

<p>EMF Parsley is very powerful and allows you to build complex custom applications and user interfaces in a matter of minutes.
It's even compatible with any UI technology such as desktop, web development and mobile, thanks to the recent efforts
leveraging the Parsley core over the JSON protocol.</p>

<p>Every single aspect of your project is easily customizable, including label and content providers,
resource management, features handling, etc.. They are also maintained in a consistent way
thanks to EMF Parsley's flexible Domain Specific Language (DSL) and the help of the powerful Google Guice dependency injection framework.
It is also easy to incrementally or partially port an existing EMF application to EMF Parsley since EMF Parsley relies on EMF.Edit for the default behaviors.</p>

Now that we have your attention, find out what's behind the tool and how easy it is to use it!

<h2>Parsley DSL</h2>

<p>The Parsley DSL is built with <a href="http://www.eclipse.org/Xtext/">Xtext</a>
and Xbase, which allows for compact definition of many aspects.
Thanks to Xtext, the DSL provides a rich IDE tooling;
just open the DSL editor and make use of Ctrl-space for auto-completion
features for example. The entire Java Type-system and APIs are available within Xtext DSLs just like in the standard Java
editor.</p>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image001.png"><br><br>

<p>You can write code
directly using the DSL or call methods from a Java class if reuse is required. As you write in the DSL editor a set of
 Java classes are generated and maintained automatically. </p>

 <p>The Eclipse Quickfix feature is also available, as in the standard Java editor.</p>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image003.png"><br><br>


<p>This is where Xbase comes into play and makes the Eclipse IDE tooling integration so seamless and advanced, you can even add
breakpoints in the DSL and debug the DSL itself, rather than the generated code. </p>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image005.png"><br><br>

<p>As you can see below, standard and EMF context menus are easily customizable and Undo/Redo features are managed by EMF Parsley automatically.</p>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image007.png"><br><br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image009.png"><br><br>


<p>The Parsley DSL is statically typed, in
order to promote static checks and to catch possible problems during
compilation, not at run-time. This means that the DSL strongly differs from purely
reflective approaches. The debugging functionality also allows you to have full
control on the code. Again, making possible problems easier to spot compared to a
reflective runtime approach.</p>

<p>The Parsley DSL is only an additional
facility for the usage of EMF Parsley. All EMF Parsley APIs are Java APIs, thus
specifications made with the DSL and code written in Java will seamlessly
coexist and interoperate.</p>

<h2>Usage with a generic service or even non-EMF-models</h2>

<p>The fact that Parsley leverages EMF
doesn't mean that you can only use it if you adopted EMF for modeling. In fact,
although you can have EMF persistence (Teneo and CDO) out-of-the-box, it is extremely easy
to provide your own Resource Manager from external services.</p>

<p>For example you could have web services,
legacy code or something else: just wrap your retrieved data in an EMF model as
simple as needed and you are done.</p>

<p>Finally, thanks to the databinding
mechanism, any change in the underlying model will be reflected on the UI
automatically, so you can focus on the model content instead of dealing with UI
custom code.</p>

<h2>Single sourcing for Desktop and Web</h2>

<p>Switching from RCP Running platform to a
RAP (Remote Application Platform) Target Platform allows you to use the same source
code to run your application on the Web!</p>

<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image011.png"><br><br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image013.png"><br><br>


<h2>Opening to other development scenarios</h2>

<p>Recently the Parsley Core has been
decoupled from the UI parts and made accessible in plain JEE projects,
throughout JSON APIs. Parsley now works with
all of its customizations, even virtually in any UI development environment.</p>
<p>Initial implementations have been made with:</p>
<ul>
	<li>AngularJS</li>
	<li>GWT</li>
	<li>Mobile Android (using Eclipse Andmore project)</li>
	<li>Mobile Hybrid (using Eclipse Thym project)</li>
</ul>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/uis.png"><br><br>

<h2>Getting Started</h2>

<p>EMF Parsley is listed on the Oomph Catalog,
so anyone can setup the development environment to use it or contribute to the project
in a matter of minutes.</p>
<br>
<img class="img-responsive" src="/community/eclipse_newsletter/2015/november/images/image015.png"><br><br>


<h2>Links &amp; Resources</h2>

<h3>EMF Parsley EclipseCon Talks</h3>


<iframe width="460" height="275" src="https://www.youtube.com/embed/NOt0YywP5nQ?list=PLy7t4z5SYNaR0yp9EQ9txQhO-JgCLJAga" frameborder="0" allowfullscreen></iframe>
<p>EclipseCon Europe 2015 - From EMF to UIs: how to use EMF Parsley to get desktop, web and mobile UIs from the model</p>
<br>

<iframe width="460" height="275" src="https://www.youtube.com/embed/-S8mh5p-ChE?list=PLy7t4z5SYNaQGJrjB5CFs3tSaulL6yiNU" frameborder="0" allowfullscreen></iframe>
<p>EclipseCon Europe 2013 - Add some spice to your application! (using EMF Parsley in your UI)</p>
<h3>Other useful resources:</h3>

<ul>
	<li><a href="http://www.eclipse.org/emf-parsley">EMF Parsley Website</a>
	</li>

	<li><a href="https://www.eclipse.org/emf-parsley/documentation.html">Documentation</a>
	</li>

	<li><a href="https://www.eclipse.org/forums/index.php/f/263/">Forum</a>
	</li>

	<li><a href="https://bugs.eclipse.org/bugs/buglist.cgi?product=EMF.Parsley">Bugzilla</a>
	</li>

	<li><a href="https://dev.eclipse.org/sonar/dashboard/index/org.eclipse.emf.parsley:org.eclipse.emf.parsley.parent">Sonar Metrics</a>
	</li>
</ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/lorenzo.jpg"
        alt="Lorenzo Bettini" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Lorenzo Bettini<br />
        <a target="_blank" href="">Dip. Informatica, Univ. Torino, Italy</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/lorenzo_bettini">Twitter</a></li>
      <li><a class="btn btn-small btn-warning" target="_blank" href="www.lorenzobettini.it">Blog</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/lorenzo-bettini-2719a02">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/francesco.png"
        alt="Francesco Guidieri" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
       Francesco Guidieri<br />
        <a target="_blank" href="http://www.rcp-vision.com">RCP Vision</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/fraguid">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/francesco-guidieri-1898221">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/november/images/vincenzo.png"
        alt="Vincenzo Caselli" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">Vincenzo Caselli
       <br />
        <a target="_blank" href="http://www.rcp-vision.com">RCP Vision</a>
      </p>
      <ul class="author-link list-inline">
      <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/vcaselli">Twitter</a></li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://it.linkedin.com/in/vincenzo-caselli-1613386">Linkedin</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>


