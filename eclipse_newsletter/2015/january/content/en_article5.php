<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <a target="_blank" href="https://www.eclipsecon.org/na2015/"><img
      src="/community/eclipse_newsletter/2015/january/images/ec2015_logo_final2.png"
      class="img-responsive" /></a><br>
    <br />
    <p>
      <a target="_blank" href="https://www.eclipsecon.org/na2015/">EclipseCon</a>
      is a great opportunity for developers to learn what is new in the
      Eclipse community and the larger software community. It is a place
      to meet other developers who are working and interested in similar
      technology.
    </p>

    <p>Some highlights this year include:</p>
    <ul>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/session/keynote-java-9-and-beyond-grand-peninsula-defg">Mark
          Reinhold</a>, the Chief Architect for the Java Platform, will
        talk about the next Java 9 release and what to expect in future
        Java releases. Java 9 will represent a significant advancement
        of the Java platform so understanding the details will be
        important for any Java developer.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/session/eclipse-gradle-best-both-worlds">Hans
          Dockter</a>, founder of Gradle and Gradleware, will talk about
        using Gradle for your build system and how he sees Gradle being
        used from Eclipse. Gradleware has proposed a new Eclipse project
        for Gradle integration so expect awesome Eclipse-Gradle
        integration coming soon.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/session/eclipse-platform-news-what-cooking-mars">Lars
          Vogel</a>, an active Eclipse Platform committer, is set to
        showcase all the great new features in the next release of the
        Eclipse platform.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/session/oomph-eclipse-way-you-want-it">Ed
          Merks and Eike Stepper</a>, Oomph project committers, will be
        talking about a new installation and configuration experience
        they are planning to role out with the Mars release. Oomph will
        be a significant improvement over the existing configuration
        experience for Eclipse. It is definitely something to watch.</li>
    </ul>
    <p>EclipseCon will also features tracks on Eclipse Platform/RCP,
      Cloud Development, Modeling, and Languages and Tools. There are
      over 80 technical talks to choose from.</p>

    <p>EclipseCon will also feature 5 single-track Theme Days,
      including:</p>
    <ul>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/scienceday">Science Day</a>
        that will include talks on software components used for
        scientific research, including visualizing and manipulating
        multidimensional dataset, modeling platforms for diseases, and
        simulation software.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/iotday">IoT Day</a>
        highlights the technology being created within the Eclipse IoT
        community, including sessions on IoT security, IoT messaging
        protocols, device management and API development.</li>
      <li><a target="_blank" href="http://xtextday.org/">Xtext Day</a>
        is the place for new users to quickly learn Xtext and for
        existing users to understand advanced use cases.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/cdtday">CDT Day</a> will
        spotlight the open source technology at Eclipse for C/C++
        development used by the embedded, mobile and IoT industries.</li>
      <li><a target="_blank"
        href="https://www.eclipsecon.org/na2015/polarsysday">PolarSys
          Day</a> is an opportunity to learn about the open source tools
        for embedded systems development that are being created by the
        PolarSys Working Group. PolarSys is a collaboration between
        large system engineering and aerospace companies like Airbus,
        Ericsson, and Thales.</li>
    </ul>
    <p>Finally, EclipseCon attendess will also get a chance to
      participate in the co-located FOSS4G NA conference, an entire
      conference dedicated to software developers using geospatial
      technology. This will be a great opportunity to learn about
      mapping technology, including geospatial databases and frameworks
      for building interactive maps.</p>

    <p>EclipseCon is March 9-12 in San Francisco. We hope to see you
      there.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/january/images/Ian.jpg"
        width="75" alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Ian Skerrett<br />
        <a target="_blank" href="http://www.eclipse.org">Eclipse
          Foundation</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://ianskerrett.wordpress.com/">Blog</a></li>
        <li><a target="_blank" href="https://twitter.com/IanSkerrett">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+IanSkerrett/posts">Google +</a></li>
        <!--$og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

