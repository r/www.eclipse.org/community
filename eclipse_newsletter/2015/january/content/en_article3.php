<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <img
      src="/community/eclipse_newsletter/2015/january/images/dawn.png"
      class="img-responsive" alt="" /><br>
    <br />
    <p>
      DAWN, Data Analysis Workbench, (<a target="_blank"
        href="http://www.dawnsci.org/">www.dawnsci.org</a>) is a
      RCP-based application used in Synchrotrons, Neutron Sources and
      Universities for visualizing and slicing data and running analysis
      software. DAWN is primarily developed at the Diamond Light Source
      (<a target="_blank" href="http://www.diamond.ac.uk/Home.html">www.diamond.ac.uk</a>)
      however developers and collaborators from around the world are
      active with the codebase which is open source and available on
      github. Most of the users of DAWN either download the binary or
      have a version installed at their institution.
    </p>

    <p>DAWN's visual tools are contributed from extension points and
      there are a large number of different options available to users
      when visualizing data. From slicing of multidimensional data to
      integration, selection regions to 1D mathematics or custom
      specific scientific tools; DAWN is a powerful tool.</p>

    <p>
      Software to analyse data is available in three forms within DAWN.
      Java-based algorithms configured by a user interface inside DAWN,
      third party codes run on a compute cluster of various kinds and
      finally python scripts developed directly inside DAWN, using Pydev
      (<a target="_blank" href="http://www.pydev.org/">www.pydev.org</a>).
      DAWN provides a rich CPython API for use inside the product, so
      for instance plotting and file readers are accessible for
      scripting.
    </p>

    <p>
      The team tries to make DAWN as generic and reusable as possible.
      DAWN is repackaged and released in three other products with
      others evaluating. There are some videos of DAWN at different
      points of development at <a target="_blank"
        href="http://www.opengda.org/dawnsci/videos/">www.opengda.org/dawnsci/videos</a>.
    </p>

    <h2>DAWNSci and the Eclipse Science Working Group</h2>
    <p>
      Diamond Light Source have been involved with forming the Eclipse
      Science Working Group and are committed to working with the
      community to make tools modular and reusable between disciplines.
      It would be great if serendipitous discoveries could be made by
      applying tools developed for one discipline in another and of
      course the reduction of effort where existing tools can be reused
      rather than redeveloped. Working with the Eclipse Foundation and
      members of the group, we hope to make available the ‘DAWNSci’ (<a
        href="http://projects.eclipse.org/proposals/dawnsci">projects.eclipse.org/proposals/dawnsci</a>)
      eclipse project first quarter of 2015 on github. We have
      abstracted much of the DAWN API into interfaces/services for
      plotting, data, file reading and conversion, slicing and much
      more. This API we will commit to supporting an implementation,
      long term and encourage alternative implementations, which will
      make tools and algorithms reusable.
    </p>

    <h2>DAWN Under Development</h2>
    <p>
      DAWN is released at every shutdown of the Diamond Synchrotron,
      about 4 or 5 times per year. The next release, DAWN 1.8, will be
      in March 2015. In this version we will provide the ‘Processing’
      perspective to make a model based execution engine where users can
      visually build components to complete custom analysis. We would
      like to use EMF in future for the user interface and the execution
      engine is based on Ptolemy 2 (<a
        href="http://ptolemy.eecs.berkeley.edu/">ptolemy.eecs.berkeley.edu</a>)
      developed with help from Isencia Belgium (<a
        href="http://www.isencia.be/">www.isencia.be</a>). In 1.8, DAWN
      will be extended to allow all of the user interface to be scripted
      by CPython. Users will be able to record macros to see the
      commands which the user interface is generating. Normally the
      interface will be driving an OSGi service so all the OSGi services
      will be available to the cpython layer running in Pydev. It is
      also hoped that, using reflection, we will be able to autocomplete
      the cpython calling Java objects. Several new scientific tools
      will be added in 1.8 for specific tasks for example spectroscopy,
      ptychography, image alignment and circular dichroism.
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/january/images/figure-1-1.png"><img
        src="/community/eclipse_newsletter/2015/january/images/figure-1-1.png"
        width="500" alt="" /></a><br> Fig 1 - New Isosurface feature in
      development for DAWN, uses JavaFX
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/january/images/output-1.png"><img
        src="/community/eclipse_newsletter/2015/january/images/output-1.png"
        width="500" alt="" /></a><br> Fig 2 - Slicing of 4D data and showing a
      new macro being recorded
    </p>

    <h2>Getting Started with DAWN</h2>
    <p>
      You can download a binary of DAWN at <a target="_blank"
        href="http://www.dawnsci.org/">www.dawnsci.org</a> or contact
      the mailing list at DAWNDEV@ JISCMAIL.AC.UK. If you would like to
      check out a version of DAWN to reuse in your products, as a
      software developer, then there are guides on the website or simply
      get in touch by email.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/january/images/mattg.jpg"
        width="75" alt="" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Matt Gerring<br />
        <a target="_blank" href="http://www.diamond.ac.uk/Home.html">Diamond
          Light Source</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank"
          href="http://uk.linkedin.com/in/matthewgerring">Linkedin</a></li>
        <!--<li><a target="_blank" href="https://twitter.com/rcheetham">Twitter</a></li>
          <li><a target="_blank" href="https://plus.google.com/+KaiKreuzer">Google +</a></li>
          $og -->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

