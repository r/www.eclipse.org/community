<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
include_once('settings.php');	
# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "User Spotlight - Philip Wenig";
	$pageKeywords	= "eclipse, newsletter, user spotlight";
	$pageAuthor		= "Roxanne Joncas";
		
	#Uncomment and set $original_url if you know the original url of this article.	
	$original_url = "";
	$og = (isset($original_url)) ? '<li><a href="'. $original_url .'" target="_blank">Original Article</a></li>': '';
	
	
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML
	<link rel="canonical" href="" />
	<div id="fullcolumn">
		<div id="midcolumn">
			<div class="breadcrumbs"><a href="/community/eclipse_newsletter/">Eclipse Newsletter</a> > <a href="/community/eclipse_newsletter/2015/january">January 2015</a> > <strong>$pageTitle</strong></div>	
			<h1>$pageTitle</h1>	<img align=right src="/community/eclipse_newsletter/2015/january/images/philip.png"/>
			<html>
			
			<h3>What do you do?</h3>
			
				<p>I'm the co-founder of Lablicate UG, a company specialized in developing software for laboratories with chemical background. Our main product is OpenChrom® (<a target="_blank" href="https://openchrom.net/">https://openchrom.net</a>), an open source software to analyze data from chromatography and mass spectrometry. Most people know chromatography from their school times. It's about using a marker, coffee filter paper and a solvent to produce nice looking blossoms. The purpose is to separate the various dye components of the marker. That's called chromatography. In fact, today's systems are much more sensitive than the latter approach. Moreover, they are as expensive as a luxury car. Despite of that, it's among the most commonly used methods in laboratories today and very often coupled with mass spectrometry – a technique to identify each component. The technology is used in forensics, quality control, food analytics and several other fields. Anyhow, there are three big drawbacks. Each vendor offers its own software, that runs most commonly under Microsoft Windows only and is proprietary. That's why we created OpenChrom, based on Eclipse. I'm responsible for the development of OpenChrom and other tailor-made solutions for our customers.</p> 

				<p>I really love open source software and the idea of a community that shares software and ideas. Hence, it's a great opportunity to run an own company and to be able to invest resources and time in open source software. Recently, we have submitted a proposal called ChemClipse (<a href="http://projects.eclipse.org/proposals/chemclipse">http://projects.eclipse.org/proposals/chemclipse</a>) to bring OpenChrom to the Eclipse Foundation. It will be a part of the Foundations Science Working Group (<a href="http://science.eclipse.org/">http://science.eclipse.org</a>).</p>
					
			<h3>How long have you been using Eclipse?</h3>
				
				<p>I started using Eclipse during my PhD studies in 2008. In the years before, I have developed several tools using Visual Basic 6. But it was time to start something new that is more flexible and sophisticated. Luckily, I decided to use Java and Eclipse RCP. Moreover, I was dissatisfied with using the vendors software for our chromatography/mass spectrometry systems. Hence I started writing an own software, the dawn of OpenChrom. I never regret the choice to use Eclipse as a platform, even though the migration to Eclipse 4 was far from being a sunny day on the beach. Anyhow, I really appreciate the benefits of Eclipse 4.</p>
					
			<h3>Name five plugins you use and recommend:</h3>
					<p>Eclipse as a platform and community offers so many great tools, so that it is really hard to focus on five favorites. I try to make a choice:</p>
					<ul>
						<li><a href="https://www.eclipse.org/egit/">EGit</a>: The Git support with EGit within the IDE is fantastic.</li>
						<li><a href="http://marketplace.eclipse.org/content/tycho-build-tools">Tycho</a>: I tried to build Eclipse RCP applications with pure Maven several years ago – a nightmare. Hence, it's a great leap forward to build RCP applications with Maven/Tycho, even though people say that other tools are better than Maven/Tycho.</li>
						<li><a href="http://download.eclipse.org/e4/downloads/">e4 Tools</a>: I really like the e4 tools, especially the *.e4xmi editor. It's much better than to edit the XML file manually. Moreover, the Eclipse runtime spies are a great support to inspect the platform and to find the needed element ids.</li>
						<li><a href="http://eclipse.org/recommenders">Code Recommenders</a>: The code recommender functionality is really nice, especially when working with new libraries and functionalities. </li>
						<li><a href="http://help.eclipse.org/luna/topic/org.eclipse.pde.doc.user/guide/tools/views/image_browser_view.htm">Plug-in Image Browser </a>: It's a great help to find icons and their reference URI.</li>
					</ul>

			<h3>What's your favorite thing to do when you're not working?</h3>
			
				<p>I really enjoy baking all kinds of bread. There's a speciality in northern Germany called „Franzbrötchen“ - my favorite one. The dictionary translation is „sweet cinnamon flavored pastry“. It is fantastic to see that principally only flower, water, salt and yeast are necessary to produce different kinds of bread. Just make it, no debugging – even though it sometimes fails too!

					</p>				
				
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://openchrom.net">OpenChrom</a></li>
					<li><a target="_blank" href="https://www.linkedin.com/pub/philip-wenig/2a/4a8/877">Linkedin</a></li>
				</ul>
		</div>
		</div>
	</div>

EOHTML;
	if(isset($original_url)){
		$App->AddExtraHtmlHeader('<link rel="canonical" href="' . $original_url . '" />');
	}
	$App->AddExtraHtmlHeader('<link rel="canonical" href="" />');
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />');
	
	# Generate the web page
	$App->generatePage(NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>