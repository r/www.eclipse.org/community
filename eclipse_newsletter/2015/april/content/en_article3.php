<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <h2>Spring Boot and Cloud Foundry</h2>

    <p>
      <a target="_blank" href="http://projects.spring.io/spring-boot/">Spring
        Boot</a> allows for easier development of Spring based
      stand-alone Java or web applications. Spring Boot is ideal for
      Platform as a Service (PaaS), and is a great way to develop
      applications that run on Cloud Foundry.
    </p>

    <p>
      <a target="_blank" href="https://run.pivotal.io/">Pivotal Web
        Services (PWS)</a> powered by Cloud Foundry offers a wide
      variety of services readily available to deployed applications,
      whether Spring Boot, traditional war web applications, or other
      supported types like Node.js. In addition, it provides automatic
      runtime setup for applications through buildpacks, like the <a
        target="_blank"
        href="http://blog.cloudfoundry.org/2013/09/06/introducing-the-cloud-foundry-java-buildpack/">Java
        buildpack</a> for applications that run on a JVM.
    </p>

    <p>
      To deploy a Spring Boot application in Eclipse, users need only
      drag and drop their application project to a PWS target created by
      <a target="_blank"
        href="https://github.com/cloudfoundry/eclipse-integration-cloudfoundry">Cloud
        Foundry Eclipse</a>, a joint tooling collaboration between
      Pivotal Software, Inc. and IBM. The Cloud Foundry runtime setup,
      application deployment and startup, and creation of services used
      by the application like an SQL database, are all taken care of for
      the user by the tooling and Cloud Foundry. Application scaling is
      also easily performed, and project changes can be incrementally
      pushed through the Cloud Foundry Eclipse UI . This allows for
      smooth deployment and management of an application from the same
      Eclipse environment where the application is developed.
    </p>

    <p>
      In this article, we cover how straightforward and simple it is to
      create a Spring Boot application in <a target="_blank"
        href="https://spring.io/tools/sts">Spring Tool Suite (STS)</a>,
      an Eclipse based development environment for Spring applications,
      and deploy it to a PWS target using Cloud Foundry Eclipse.
    </p>

    <h2>Creating a Spring Boot Application</h2>

    <p>
      STS provides a wizard to easily create a Spring Boot project that
      is ready to run locally or deployed to Cloud Foundry. This is done
      simply by opening the Eclipse <b>New Project</b> wizard and
      selecting "Spring Starter Project".
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.1.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.1.png"
        alt="" class="img-responsive" /></a>
    </p>


    <br>
    <br>

    <p>The wizard provides a large number of ways to configure a Spring
      Boot application. In addition, a user can select the type of
      packaging that is need for the application, whether jar if it is
      meant to be a stand-alone Java application, or war if it is to be
      deployed to a container like Tomcat. Either packaging is supported
      in Cloud Foundry.</p>

    <p>
      STS also features a Getting Started guide through the same <b>New
        Project</b> wizard. Selecting "Import Spring Getting Started
      Content" in the wizard creates a Spring project that is deployable
      to Cloud Foundry following these <a target="_blank"
        href="https://spring.io/guides/gs/sts-cloud-foundry-deployment/">guide
        instructions</a>.
    </p>

    <p>Once the project is created and it appears in the Eclipse
      workspace, deploying it to Cloud Foundry requires three simple
      steps:</p>

    <ol>
      <li>Configuring the application for Cloud Foundry deployment. This
        is only required if the application was packaged as jar. It’s
        not needed for war applications. In the Package Explorer, right
        click on the Spring Boot project and select: <br>Configure ->
        Enable as Cloud Foundry App
      </li>
      <li>Creating a Cloud target.</li>
      <li>Drag and dropping the Spring Boot project to the Cloud target.</li>
    </ol>

    <p>
      Cloud Foundry Eclipse comes pre-installed in STS 3.6.3 and higher.
      It can also be <a target="_blank"
        href="http://docs.cloudfoundry.org/buildpacks/java/sts.html#install">installed</a>
      into Eclipse Java EE that uses a Java 7 JRE through the <a
        href="http://marketplace.eclipse.org/content/cloud-foundry-integration-eclipse">Eclipse
        Marketplace</a>.
    </p>


    <h2>Deploying to Cloud Foundry</h2>

    <p>
      To deploy to PWS, <a target="_blank"
        href="https://console.run.pivotal.io/register">account
        registration</a> is required.
    </p>

    <p>
      Cloud Foundry Eclipse integrates into Eclipse Web Tools Platform,
      and creating a Cloud target where an application can be deployed
      to is a simple task of creating a new server instance through the
      WTP <b>New Server</b> wizard.
    </p>

    <p>
      Open the <b>New Server</b> wizard and select "Cloud Foundry" under
      "Pivotal".
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.2.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.2.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <p>Once created, the Cloud Foundry server instance appears in the
      Eclipse Servers view and it maps to the Cloud space where the
      application will be deployed.</p>

    <p>Now the application project simply needs to be drag and dropped
      to the Cloud target in the Eclipse Servers view.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.3.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.3.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <p>During the deployment process, an application deployment wizard
      will prompt the user to enter the deployment details for the
      application. The wizard is pre-populated with default values, so a
      user can just click "Finish" to deploy immediately or "Next" to
      further configure the deployment.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.4.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.4.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <p>Deployment properties that a user can modify include the
      application URL, memory, and environment variables, like for
      example defining JAVA_OPTS.</p>

    <p>Navigating through the same wizard will also open the Services
      selection page, where a user can create and bind service instances
      to the application.</p>

    <p>The list of services varies based on the Cloud target, and is
      populated with up-to-date content from the user’s space.</p>

    <a
      href="/community/eclipse_newsletter/2015/april/images/article3.5.png"><img
      src="/community/eclipse_newsletter/2015/april/images/article3.5.png"
      alt="" class="img-responsive" /></a><br>
    <br>

    <p>
      Among the types of services that a user can create in PWS are SQL
      databases and messaging services like RabbitMQ. Once created, some
      of these services need to be <a target="_blank"
        href="http://docs.cloudfoundry.org/devguide/services/adding-a-service.html#use">bound</a>
      to an application in order to be used. This binding is also done
      in the same Services selection page.
    </p>

    <p>Once a user clicks "Finish", the deployment process completes,
      and an Eclipse console is automatically opened showing the
      progress of the deployment, including staging of the application
      by the buildpack.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.6.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.6.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <h2>Managing the Application</h2>

    <p>When the application is successfully deployed, it will appear in
      the Servers view under the Cloud target, and the deployed
      application will be linked to the workspace project. Changes to
      the workspace project can be published via WTP “Publish” in the
      Servers view, or through the Cloud Foundry editor, which is opened
      by double clicking on the application.</p>

    <p>The editor contains two tabs:</p>

    <p>1. "Overview", which shows information regarding the Cloud
      target, like the space name and user credentials.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.7.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.7.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <p>2. "Applications and Services", where a user can scale the
      application, manage services, view a running application’s stats
      like CPU and memory usage, and incrementally push changes in the
      application project via "Update and Restart".</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article3.8.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article3.8.png"
        alt="" class="img-responsive" /></a>
    </p>
    <br>

    <h2>Enhancing the Development Experience</h2>

    <p>
      This article showed how a Spring Boot application can easily be
      created and deployed to Cloud Foundry in an Eclipse development
      environment. Further enhancements to the development experience
      include <a target="_blank"
        href="https://github.com/cloudfoundry/eclipse-integration-cloudfoundry/blob/master/debugging.md">debugging
        support</a>, which is currently featured in Cloud Foundry
      Eclipse 1.8.1 for certain applications through a "Debug" button in
      the editor, and <a target="_blank"
        href="http://zeroturnaround.com/software/jrebel/quickstart/eclipse/">JRebel</a>
      integration available in version 1.8.2. A comprehensive view of
      Cloud Foundry Eclipse can also be found <a target="_blank"
        href="http://docs.cloudfoundry.org/buildpacks/java/sts.html">here</a>.
    </p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/april/images/nieraj.jpg"
        alt="nireaj" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Nieraj Singh<br>
        <a target="_blank" href="http://pivotal.io/">Pivotal Software,
          Inc.</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Twitter</a></li>
          <li><a target="_blank" href="">Linkedin</a></li>
          <li><a target="_blank" href=""></a></li>
          $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

