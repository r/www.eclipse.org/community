<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>
      <a target="_blank" href="http://cloudfoundry.org/index.html">Cloud
        Foundry</a> is an open source platform as a service (PaaS) that
      is used in cloud computing. One of the keys to success of a given
      platform is having some IDEs that help users to develop
      applications to run on the platform. The <a target="_blank"
        href="http://docs.cloudfoundry.org/buildpacks/java/sts.html">Cloud
        Foundry Eclipse Tools</a> (CF Tools) is a framework that does
      exactly that. It is a set of tools that allows developers to
      easily develop, test, and deploy applications to Cloud Foundry,
      directly within Eclipse. It has been jointly developed by IBM and
      Pivotal Software, Inc. Other vendors are more than welcome to
      contribute to the project.
    </p>
    <p>
      The CF Tools are an open source project built on top of the <a
        target="_blank" href="https://eclipse.org/webtools/">Eclipse Web
        Tools Platform</a> (WTP). It allows adopters to quickly
      implement a server type to interact with a Cloud Foundry server.
      There are a couple of vendor specific implementations already
      available in the Eclipse marketplace, e.g. <a target="_blank"
        href="http://marketplace.eclipse.org/content/ibm-eclipse-tools-bluemix">IBM
        Eclipse Tools for Bluemix</a>, <a target="_blank"
        href="http://marketplace.eclipse.org/content/cloud-foundry-integration-eclipse">Cloud
        Foundry Integration for Eclipse</a> and <a target="_blank"
        href="http://marketplace.eclipse.org/content/hp-helion-development-platform-eclipse-plugin">HP
        Helion Development Platform Eclipse Plugin</a>. The framework
      runs on a wide range of Eclipse versions from Indigo to the latest
      Luna version.
    </p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.1.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.1.png"
        alt="" /></a>
    </p>
    <br />

    <h2>Functions Provided by the CF Tools</h2>
    <p>Here are some highlighted functions provided by CF Tools using
      IBM Eclipse Tools for Bluemix as a case study:</p>
    <h3>Installation</h3>
    <p>Most of the vendor specific implementations are available on the
      Eclipse marketplace. You can do a search on the Eclipse
      marketplace and install the server support of your choice by using
      the menu item Help > Eclipse Marketplace.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.2.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.2.png"
        alt="" /></a>
    </p>
    <br />

    <h3>Server creation</h3>
    <p>
      In order to interact with a Cloud Foundry server, the first thing
      that you need to do is to create a new server. Similar to other
      WTP server types, you can create a server by selecting the menu <b>File
        > New > Other > Server</b> and selecting the server of your
      choice, e.g. <b>IBM > IBM Bluemix</b>. In the server creation
      wizard, the user can specify the connection information for the
      tools to connect to a Cloud Foundry server. The user can then
      choose the organization and space for publishing the user
      applications.
    </p>
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.3.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.3.png"
        alt="" /></a>
    </p>
    <br />

    <h3>Servers view integration</h3>
    <p>After the server has been created, you can find the server on the
      Servers view and the list of applications that are installed on
      the server, including the ones that are installed outside of the
      Tools.</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.4.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.4.png"
        alt="" /></a>
    </p>
    <br />

    <h3>Application deployment and management</h3>
    <p>
      The CF Tools framework allows you to easily deploy applications to
      CF servers (e.g. WAR). To deploy an application, you can right
      click on the server from the Servers view and select the menu <b>Add
        and Remove</b> to add the application to the server. During the
      application deployment, you can specify the related deployment
      information, e.g. service bindings, environment variables and
      application URL. The Tools also provide a <b>Save to manifest file</b>
      option that saves the deployment information in the manifest.yml
      file for the application. The information saved in the manifest
      file will be prefilled in the deployment dialog when deploying the
      application again.
    </p>
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.5.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.5.png"
        alt="" /></a>
    </p>
    <br />

    <p>You can also update the application during your development
      without restaging the application.</p>

    <h3>Server editor</h3>
    <p>The server editor is one of the key features of the CF Tools
      framework. It allows you to view and configure server connection
      information, application and instance information, application
      environment variables, and control applications. It also allows
      you to manage and bind services to the application, etc.</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.6.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.6.png"
       alt="" /></a>
    </p>
    <br />

    <p>It also allows you to query and create new services on the fly.</p>
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.7.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.7.png"
        alt="" /></a>
    </p>
    <br />

    <h3>Remote System View</h3>
    <p>The Tools integrate with the Eclipse Remote System view to show
      the files on the deployed instance in the cloud. You can click on
      the Remote System View link in the server configuration editor to
      view the application artifacts deployed on the instance running in
      the cloud.</p>

    <h3>Application Debug</h3>
    <p>Application development will be difficult without the ability to
      debug an application. The framework also provide functions to
      allow you to debug java applications running in the Cloud.</p>

    <h3>Extensions and APIs</h3>
    <p>In terms of extensibility, the CF Tools framework also provides
      extensions and APIs for adopters to provide vendor specific
      implementations.</p>

    <h3>Branding extension</h3>
    <p>The branding extension point
      org.cloudfoundry.ide.eclipse.server.core.branding allows adopters
      to customize products to provide any needed fit and finish, e.g.
      default cloud foundry URL, account sign up URL, wizard icons and
      labels, etc.</p>


    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article2.8.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article2.8.png"
        alt="" /></a>
    </p>
    <br />

    <h3>Application Extension</h3>
    <p>Application extension point
      org.cloudfoundry.ide.eclipse.server.core.application allows
      adopters to define different supported application types that are
      unique to the adopter’s cloud foundry server implementation.
      Combined with the publishing implementation in the WTP server
      extension
      org.eclipse.wst.server.core.model.ServerBehaviourDelegate, the
      adopter can publish different types of applications seamlessly
      using the same user interfaces. For example, in the IBM Eclipse
      Tools for Bluemix, the Tools has implemented the application
      publish support for the Liberty packaged server and the JavaScript
      applications.</p>

    <h3>Application Wizard Extension</h3>
    <p>Adopters can also customize the deployment dialog by implementing
      the extension point
      org.cloudfoundry.ide.eclipse.server.ui.applicationWizard if they
      need extra information specified when deploying their new
      application type.</p>

    <h2>Summary</h2>
    <p>In this article, we have covered the highlights of the functions
      and extensions provided by the Cloud Foundry Eclipse Tools. As an
      application developer, you can enjoy the ease of use of the Tools
      to help you to develop your applications in Eclipse and run them
      on a Cloud Foundry server. As a vendor specific provider, when
      combined with the rich set of extensions from WTP, adopters can
      provide even more functions as needed, such as incremental publish
      on Java applications in Bluemix. The sky is the limit! Download
      one of the extensions and try it yourself.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/april/images/yuen.jpg"
        width="90" alt="yuen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Elson Yuen<br />
        <a target="_blank" href="http://www.ibm.com/ca/en/">IBM Canada
          Ltd.</a>
      </p>
      <ul class="author-link">
        <!--<li><a target="_blank" href="">Blog</a></li>
            <li><a target="_blank" href=""></a></li>
            <li><a target="_blank" href=""></a></li>
            $og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

