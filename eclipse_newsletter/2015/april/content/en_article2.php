<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Support for Cloud Foundry was originally introduced into Orion a
      couple of releases ago. In the latest Orion version (release 8.0)
      you might have noticed a few major changes to the feature.</p>
    <p>Cloud Foundry integration was already fairly comprehensive. Orion
      provides ways to deploy applications as Cloud Foundry instances,
      to control their lifecycle, and to see logs. The focus in Orion
      release 8.0 was with the user experience related to Cloud Foundry
      - offering a fresher and hopefully simpler approach to working
      with the same rich features.</p>
    <p>
      To begin working with Cloud Foundry, it is important to add the
      cloud URLs to the Settings page to configure a connection
      relationship. There are several options, <a target="_blank"
        href="http://run.pivotal.io/">Pivotal CF</a>, <a target="_blank"
        href="http://www8.hp.com/us/en/cloud/helion-overview.html?jumpid=va_uezi8g75dv">HP
        Helion</a> or <a target="_blank"
        href="https://console.ng.bluemix.net/">IBM Bluemix</a> (for a
      longer list see the <a target="_blank"
        href="http://en.wikipedia.org/wiki/Cloud_Foundry#Platform">Cloud
        Foundry wiki</a>). For this article, I will use Bluemix for
      illustrative purposes. You will need to register first to use it.
    </p>

    <a
      href="/community/eclipse_newsletter/2015/april/images/article1.1.png"><img
      src="/community/eclipse_newsletter/2015/april/images/article1.1.png"
      alt="" class="img-responsive" /></a><br />
    <br />

    <p>
      <b>Note</b>: To quickly test Cloud Foundry support, you can create
      a project in <a target="_blank"
        href="https://orionhub.org/mixloginstatic/landing.html?redirect=https%3A%2F%2Forionhub.org%2F&key=FORMOAuthUser">OrionHub</a>
      using a sample NodeJS app from GitHub <a target="_blank"
        href="https://github.com/szbra/nodehelloworld">https://github.com/szbra/NodeHelloWorld.git</a>
    </p>
    <p>
      A speedy way to try it out is by pushing the 'Deploy to Bluemix'
      button <img
        src="/community/eclipse_newsletter/2015/april/images/bluemixbutton.png"
        alt="" /> on the GitHub project page. It will conveniently
      deploy the app in the cloud, and setup a development environment
      for you. After which, you can click 'Edit Code' and you will be
      taken to the Orion-based WebIDE in Bluemix with the code already
      imported into your workspace. This is the power of the cloud!
    </p>


    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/bluemixsuccess.png"><img
        src="/community/eclipse_newsletter/2015/april/images/bluemixsuccess.png"
        alt="" /></a><br />
    </p>

    <p>The Run Bar is the most noticeable visible change, when you’re
      working with a project. It replaces the Deployment Info box from
      previous releases.</p>

    <a
      href="/community/eclipse_newsletter/2015/april/images/article1.2.png"><img
      src="/community/eclipse_newsletter/2015/april/images/article1.2.png"
      alt="" /></a><br />
    <br />

    <p>The Run Bar shows the state of apps deployed from the selected
      project. It offers lifecycle commands, links to logs and the
      running app using recognizable icons.</p>
    <p>Launch configurations can be created from the Run Bar, and the
      name, url, and target for the application can all be set from
      there as well.</p>

    <a
      href="/community/eclipse_newsletter/2015/april/images/article1.3.png"><img
      src="/community/eclipse_newsletter/2015/april/images/article1.3.png"
      alt="" /></a><br />
    <br />
    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article1.4.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article1.4.png"
        alt="" /></a>
    </p>
    <br />
    <br />

    <p>Each launch configuration can be now edited and deleted anytime.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article1.5.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article1.5.png"
        alt="" /></a>
    </p>
    <br />
    <br />

    <p>Another useful new addition is the manifest editor, with syntax
      highlighting and content assist. An application manifest can be
      easily edited from it, or a new one can be created from scratch by
      clicking Ctrl + Space.</p>

    <a
      href="/community/eclipse_newsletter/2015/april/images/article1.6.png"><img
      src="/community/eclipse_newsletter/2015/april/images/article1.6.png"
      alt="" /></a><br />
    <br />

    <p>More advanced users can try Cloud Foundry commands directly from
      the Orion shell.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/article1.7.png"><img
        src="/community/eclipse_newsletter/2015/april/images/article1.7.png"
        alt="" /></a>
    </p>
    <br />
    <br />

    <p>
      Cloud Foundry app development is even quicker and easier when you
      use Bluemix as your target platform. Try the tutorial at <a
        target="_blank"
        href="https://hub.jazz.net/tutorials/jazzeditor/">https://hub.jazz.net/tutorials/jazzeditor/</a>
      to see how to create a NodeJS app and configure a whole
      development environment including an Orion-based WebIDE in
      seconds.
    </p>
    <p>An especially useful feature is Auto-Deploy. You can configure
      builds for all projects created in Bluemix. When any change is
      pushed to the repository linked with the project, a build is
      triggered and the updated application is automatically deployed to
      Cloud Foundry.</p>
    <p>Bluemix also offers a fast, cutting edge approach for developing
      on the cloud with its Orion-based WebIDE, called Live Edit.</p>

    <p>
      <a
        href="/community/eclipse_newsletter/2015/april/images/editlive.png"><img
        src="/community/eclipse_newsletter/2015/april/images/editlive.png"
         alt="" /></a>
    </p>
    <br />

    <p>When live edit is enabled, all edits saved in the WebIDE
      workspace are immediately pushed to the running application
      instance. Your app is updated in seconds.</p>
    <p>
      Please explore and try the great new Cloud Foundry support on <a
        target="_blank"
        href="https://orionhub.org/mixloginstatic/landing.html?redirect=https%3A%2F%2Forionhub.org%2Fedit%2Fedit.html&key=FORMOAuthUser">OrionHub</a>,
      or the enhanced version that’s part of <a target="_blank"
        href="https://console.ng.bluemix.net/">Bluemix</a>! We would
      love to hear your feedback: criticism, ideas, observations or
      praise! (Especially praise ;)
    </p>

    <p>
      Contact us on the <a
        href="http://dev.eclipse.org/mhonarc/lists/orion-dev/">orion-dev</a>
      mailing list, by entering bugs and enhancements in bugzilla, or on
      Twitter <a target="_blank" href="https://twitter.com/orionhub">@orionhub</a>.
    </p>
    <p>Enjoy!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="author-picture"
        src="/community/eclipse_newsletter/2015/april/images/szymon.jpg"
        alt="szymon brandys" height="90" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Szymon Brandys<br />
        <a target="_blank" href="http://www.ibm.com/ca/en/">IBM</a>
      </p>
      <ul class="author-link">
        <li><a target="_blank" href="https://twitter.com/szymonbrandys">Twitter</a></li>
        <li><a target="_blank"
          href="https://plus.google.com/+SzymonBrandys/posts">Google +</a></li>
        <li><a target="_blank"
          href="https://www.linkedin.com/in/szymonbrandys">LinkedIn</a></li>
        <!--$og-->
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

