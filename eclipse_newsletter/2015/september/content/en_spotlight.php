<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div id="fullcolumn">
  <div id="midcolumn">
    <h1><?php echo $pageTitle; ?></h1> <img align=right src="/community/eclipse_newsletter/2015/september/images/achiml.png"/>
	    
			<h2>What do you do?</h2>
			<p>I am a Managing Partner at BREDEX GmbH in Braunschweig, Germany. I have been working here for almost 30 years and I have no plans of changing that before retirement. I am the co-lead of the Eclipse Jubula project where I am 
			mostly responsible for architecture decisions and setting the direction for the next releases, so if you do not like where Jubula is heading please speak to me. In addition, I manage the IT department and consult colleagues on 
			the adaption of Eclipse technologies. Since we use a lot of these technologies in our customer projects this is close to a full-time job itself.</p> 
			
								
			<h2>How long have you been using Eclipse?</h2>
			<p>My first contact was in 2004 when we started with the development of the predecessor of Jubula, based on RCP and integrated into the IDE. This was around the time when Eclipse switched to Equinox and allowed a relatively easy 
			integration of add-ons.</p>
									
			<h2>Name five plugins you use and recommend:</h2>
					<p>Of course, I have to mention Jubula which is a great plug-in. But because we're not all testers I will focus on the more general plug-ins. I like (in no particular order):</p>
					<ul>
						<li><b><a href="https://marketplace.eclipse.org/content/tasktop-pro">Tasktop Dev</a></b>: taking Mylyn to the next level and helping me to keep an overview of the different tasks I have to do.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/checkstyle-plug">Checkstyle</a></b>: we fought long and hard in the company to agree on a common coding style and we can actually check and enforce it with this plug-in.</li>
						<li><b><a href="https://wiki.eclipse.org/Eclipse_Installer">Oomph</a></b>: to setup Eclipse versions and manage project workspaces. There will be a lot of helpful talks and a tutorial at EclipseCon Europe.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/findbugs-eclipse-plugin">FindBugs</a></b>: even the best need some help. And better to be embarrassed by a tool than by a user.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/eclipse-code-recommenders">Eclipse Code Recommenders</a></b>: Maybe there is a lot of boiler plate code necessary for Java, but we have a tool to type this for us - and a lot of really useful other things.</li>
					</ul>

			<h2>What's your favorite thing to do when you're not working?</h2>
				<p>My wife and I like canoeing, and weather and time permitting, I am a stargazer. Last year I started a “back to my roots” (which is electronic development) hobby and now have a room in the basement 
				which is a mess of Raspberry Pi’s, Arduino’s,  a MacBook, wires, a soldering iron and piles of electronic components. And depending on your definition of working: I just did my fifth tour on the 
				EclipseCon Europe Program Committee.</p>

					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://twitter.com/achimloerke">Twitter</a></li>
					<li><a target="_blank" href="http://testing.bredex.de/">Website</a></li>
				</ul>
		</div>
		</div>
	</div>