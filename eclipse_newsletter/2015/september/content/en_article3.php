<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    <p>Want to build compelling and meaningful data visualizations that can be integrated into rich-client or web applications? Go with BIRT. With
    components for both designing and deploying data visualizations, BIRT provides core features such as design layout, data access and scripting.</p>

	<p>I have compiled this overview to provide a practical introduction to both the design and deployment aspects of BIRT. I start by describing the overall
	BIRT architecture and the various packages available for download from eclipse.org. Then I introduce the report designer tool, called BIRT Designer;
	I also describe some basic options for customizing BIRT designs, walk you through creation of a simple design and explore some scripting possibilities.</p>

	<p>Next, I’ll explain some of the options BIRT gives you to deploy your visualizations. You can run BIRT reports directly from BIRT Designer, but if
	 you need to reach large groups of users you’ll want to explore some of the other deployment options I describe, such as BIRT Runtime or BIRT Web Viewer.
	 I’ll conclude the article by listing some resources you’ll need as you explore BIRT.</p>

	<h2>Report Design and Report Engine Components</h2>

	<p>Open Source BIRT has two main components: a design engine that is integrated into the report designer based on Eclipse and the report engine which
	you can add to your application. The BIRT Web Viewer is also available as an example web application deployment that uses the report engine to render
	reports. These components will be discussed in more detail throughout the article.</p>

	<img alt="birt open source" src="/community/eclipse_newsletter/2015/september/images/birt_opensource.jpg"><br><br>

	<h2>Where to download and which version to get?</h2>

	<p>Open source BIRT can be downloaded from <a href="http://download.eclipse.org/birt/downloads">http://download.eclipse.org/birt/downloads</a>.
	There are several different packages containing BIRT, so you can choose one that is right for your needs.</p><br>


	    <table class="table">
  <tr>
    <td><b>Package</b></td>
    <td><b>Uses</b></td>
  </tr>
  <tr>
    <td>BIRT All-In-One Download</td>
    <td>This package includes everything you need to start creating BIRT designs, including the full Eclipse SDK.</td>
  </tr>
    <tr>
    <td>BIRT Framework</td>
    <td>This download allows you to add the BIRT plug-in to your existing Eclipse environment. (Make sure you check the dependencies and
    update those too.)</td>
  </tr>
  <tr>
    <td>Eclipse Update Manager</td>
    <td>This package enables you to integrate BIRT into your existing Eclipse environment (http://wiki.eclipse.org/BIRT_Update_Site_URL).
    Be sure to also select the Data Tools Project when using this approach.</td>
  </tr>
    <tr>
    <td>RCP Report Designer</td>
    <td>This package lets you deploy BIRT using Eclipse Rich Client Platform (RCP) technology. It provides a simplified report designer
    without the additional perspectives of the standard Eclipse platform.</td>
  </tr>
  <tr>
    <td>BIRT Runtime Engine</td>
    <td>Deployment components of the BIRT project including a command line example, API examples and example web viewer. This package is
    available as a POJO runtime or OSGi runtime.</td>
  </tr>
  <tr>
    <td>BIRT Web Tools Integration</td>
    <td>Contains the plug-ins required to use the BIRT Web Project Wizard and the BIRT Viewer JSP tag library.</td>
  </tr>
</table>

    <h2>Tour the BIRT Designer</h2>
	<p>BIRT Designer is a visual report development tool with task-specific editors, builders and wizards that enable you to create data
	visualizations that can be integrated into web applications. The BIRT report designer supports:</p>
	<ul>
		<li>A component-based model for reuse of designs and elements</li>
		<li>Ease of use features in a drag-and-drop, WYSIWYG format</li>
		<li>Support for a wide range of reports, layouts and formatting</li>
		<li>Programmatic control, so report content can be built and controlled through scripting</li>
		<li>Data access across multiple data sources</li>
	</ul>


    <img alt="birt designer" src="/community/eclipse_newsletter/2015/september/images/birt_designer.png"><br><br>
    <p><i>The BIRT Designer window. Red numbers refer to descriptions below.</i></p>


    <h3>BIRT Navigator (1)</h3>
	<p>This is where you start when you open BIRT for the first time. In this area, you’ll see all of the projects in your workspace. When you want
	to create a new Report Design Project or a new BIRT file within a project, this is the place to go. Below you’ll see a table of the different
	file types you can create in BIRT.</p>

	<table class="table">
	<tr>
	<td><b>Type of file (and suffix)</b></td>
	<td><b>Description</b></td>
	</tr>

	<tr>
	<td>Design File (*.rptdesign)</td>
	<td>An XML file that contains the data connection information, design layout and instructions. Created when making a design in the BIRT Designer.</td>
	</tr>

	<tr>
	<td>Template File (*.rpttemplate)</td>
	<td>Ensures all reports you create start with some common elements, such as a company header or predefined styles. The starting point for a BIRT report.</td>
	</tr>

	<tr>
	<td>Library File (*.rptlibrary)</td>
	<td>Stores commonly used report elements, such as a company logo, so they are managed in one place for all reports.</td>
	</tr>

	<tr>
	<td>Report Document (*.rptdocument)</td>
	<td>The executed design, including layout instructions and data. Can be transformed into final report output, such as HTML, PDF and XLSX.</td>
	</tr>
	</table>

	<h3>Data Explorer (2)</h3>

	<p>Once you have created your project and design file in the Navigator, go here next. This is where you create and view all of the Data Sources,
	Data Sets, Data Cubes and Report and Page Variables.</p>

	<h4>Data Sources</h4>
	<p>The BIRT project has been designed to natively access and support many data sources, as shown in the table below. Supported data sources can
	be extended using the Eclipse ODA framework to support any data to which you have access. In addition to the list below, BIRT ships with a connection
	to a sample database (called Classic Models). BIRT also includes a Joint Data Set which allows you to join data across data sources, no matter the type.</p>

	<table class="table">
	<colgroup>
       <col span="1" style="width: 25%;">
       <col span="1" style="width: 75%;">
    </colgroup>
	<tr>
	<td><b>Data Source</b></td>
	<td><b>Connection Information</b></td>
	</tr>

	<tr>
	<td>Flat File</td>
	<td>Supports tab, comma, semicolon and pipe delimited data.</td>
	</tr>

	<tr>
	<td>JDBC</td>
	<td>Supports connections to relational databases.</td>
	</tr>

	<tr>
	<td>Scripted</td>
	<td>Allows you to implement custom logic, communicate with Java objects, or get access to data within your application. It can be used to access
	virtually any data source that is structured or that contains an API.</td>
	</tr>

	<tr>
	<td>Web Services</td>
	<td>Supports connections to a web service. A wizard helps you point at a service through a WSDL and select the data.</td>
	</tr>

	<tr>
	<td>XM</td>
	<td>Supports data from XML.</td>
	</tr>

	<tr>
	<td>Hive/Hadoop</td>
	<td>Allows access to Hadoop data through Hive using Hive Query Language (HQL). If you use Hive 2, please ensure that you copy the
	client library JAR files to the appropriate folders for both BIRT Designer and the BIRT Runtime deployment application.</td>
	</tr>

	<tr>
	<td>POJO</td>
	<td>Allows for easy connection to POJOs.</td>
	</tr>

	<tr>
	<td>MongoDB</td>
	<td>Provides the ability to access data contained in MongoDB.</td>
	</tr>

	<tr>
	<td>Cassandra Scripted<br>
	<ul>
	<li>Apache Cassandra</li>
	<li>Datastax Cassandra Community</li>
	<li>Datastax Cassandra Enterprise</li>
	</ul></td>
	<td>BIRT connects to and queries a Cassandra data source using the Hector API. You must copy the Hector client library JAR files to the
	appropriate folders for both BIRT Designer and the BIRT Runtime deployment application.</td>
	</tr>

	<tr>
	<td>Excel</td>
	<td>Initially contributed by the community, this connector allows you to pull in data from Excel Workbooks.</td>
	</tr>

	<tr>
	<td>Additional Data Sources</td>
	<td>BIRT has been extended by the open source community allowing additional data connections to LDAP, Report Documents, LinkedIn,
	Facebook, GitHub, SalesForce and Spring Beans.</td>
	</tr>
	</table>

	<h3>Resource Explorer (3)</h3>
	<p>Next to the Data Explorer, you’ll see the Resource Explorer. This is where you’ll find all of your resources that can be used in your design,
	such as libraries, images and js files.</p>

	<h3>Layout (4)</h3>

	<p>The main portion of the designer is the Layout view. This is where you create your WYSIWYG design. Below this window, you’ll also see tabs that
	 will allow you to see your Master Page, the Script on a selected element, the design’s XML Source and a Preview of your design.</p>

	<h3>Palette (5)</h3>
	<p>Now that you’ve created your project and your design and connected to your data, you’re ready to start building the design layout. To insert
	an element into your layout from the Palette, simply drag and drop it. The table below lists available items, including a breakdown of the available
	chart types.</p>

	<table class="table">
	<tr>
	<td><b>Tool</b></td>
	<td><b>What it does</b></td>
	</tr>

	<tr>
	<td>Label</td>
	<td>Use to include static (or localized) text within a report. Typically used for report titles, column headers or any other report text.</td>
	</tr>

	<tr>
	<td>Text</td>
	<td>Use to include richly formatted text to your report, including HTML formatting integrated with dynamic data.</td>
	</tr>

	<tr>
	<td>Dynamic Text</td>
	<td>Use to integrate static text with dynamic or conditional data.</td>
	</tr>

	<tr>
	<td>Data</td>
	<td>Use to include data from a connection in your report.</td>
	</tr>

	<tr>
	<td>Image</td>
	<td>Use to include images from various sources and locations.</td>
	</tr>

	<tr>
	<td>Grid</td>
	<td>Use to define the layout of a report. A grid can be nested within other grids to support complex layouts.</td>
	</tr>

	<tr>
	<td>List</td>
	<td>Use to display data elements from your data sources that repeat and create a new report row for each data set row. Lists can contain
	multiple levels of grouping.</td>
	</tr>

	<tr>
	<td>Table</td>
	<td>Use to display repeating data elements within your report. Tables support multiple columns and multiple levels of grouping.</td>
	</tr>

	<tr>
	<td>Chart</td>
	<td>Use to add rich, interactive charting to your BIRT report.</td>
	</tr>

	<tr>
	<td>Cross Tab</td>
	<td>Use to display grouped and dynamic data, organized by both the row and column level.</td>
	</tr>

	<tr>
	<td>Aggregation</td>
	<td>Use to build totals for tables and groups. Includes over 30 built-in functions including COUNT, SUM, MAX, MIN, AVE, RUNNINGSUM, COUNTDISTINCT and RANK.</td>
	</tr>
	</table>

    <h4>Chart Types</h4>
	<p>The chart types you’ll find out-of-the-box with BIRT are shown below. Several of the charts are offered in 2D, 2D with depth and 3D formats.
	Most charts also have more than one sub-type, such as a bar chart with options of side-by-side, stacked and percent stacked, or a stock chart with
	options of candlestick and bar-stick. If you don’t find what you’re looking for, you can create a custom chart plug-in.</p>

	<img alt="birt bar stick" src="/community/eclipse_newsletter/2015/september/images/bar_stick.png"><br><br>

	<h3>Property Editor (7)</h3>
	<p>Just below the layout window, you’ll find the property editor. This area allows you to customize your design or individual elements with just a
	few clicks. Options include filtering, grouping, sorting, mapping and highlighting.</p>

	<h3>Outline (8)</h3>
	<p>The outline shows your design in a tree structure. You also use the outline view to create and edit master pages, add styles, import CSS formatting,
	embed images and locate scripts within your design.</p>

	<h2>Tutorial: Creating a Simple Listing Design</h2>
	<p>Now that you’ve taken a quick tour of BIRT Designer, let’s build a very simple design. This tutorial assumes you’ve already installed BIRT
	and are in the Report Design perspective.</p>
	<ol>
	<li>Right click in the Navigator and create a new Project by selecting Report Project under Business Intelligence and Reporting Tools.
	Enter a name for your project and hit Finish.</li>
	<li>Right click on your new project in the Navigator and create a new report. Be sure your project is selected, give the design a name and hit Next.
	Here you’ll have several template options for getting started. As stated above in the BIRT Files list, you can create your own templates to add to this
	list. Choose the blank template and hit Finish.</li>
	<li>Go to the Data Explorer, right-click on Data Sources and create a new data source. Choose the Classic Models database, give your Data Source a
	name and hit Finish. Next, right-click on Data Sets and create a new data set. Make sure your data source is selected, give your data set a name
	and hit Next. Enter the following query, then hit Finish:<br>
	     select * from payments<br>
		Click Preview Results, then click OK.<br></li>
	<li>Drag a Label from the Palette into the layout window and enter My First Report. Drag your data set from the Data Explorer into your layout,
	below the label. Select all of the columns in the pop-up and click OK.</li>
	<li>Select the table in the layout by hovering over the lower-left corner of the table and selecting the “Table” tab. You’ll notice the Property
	Editor changes to reflect the fact that you’re working on a table. Go to the Groups tab in the property editor and select Add. Name your group,
	then select CUSTOMERNUMBER from the Group On drop-down menu. Hit OK and delete the CUSTOMERNUMBER data element from the detail row (not the group
	header). Go to Run -> View Report -> In Web Viewer.</li>
	</ol>

	<p>In five short steps, you’ve created a very simple report with a table listing your data. Take some time to explore other options in the property
	editor, e.g., add a chart, or add a CSS style. You’ll see how quickly you can take this simple design to the next level.</p>

	<h2>Advanced Design Customization</h2>

	<p>Now that we’ve gone through the basics and you’ve created your first design, it’s time to learn how to customize and personalize your design
	to fit the needs of your application. Customization, in this case, refers to things like localization, internationalization, styles and scripting.</p>

	<h3>Localization</h3>
	<p>BIRT supports internationalization of report data, including support for bidirectional text. BIRT also supports the localization of static
	report elements within a report, allowing you to replace report labels, table headers and chart titles with localized text. BIRT allows the use
	of multiple resource files with name/value pairs and a *.properties file extension. For example, a file called MyLocalizedText_de.properties can
	include a line that says “welcomeMessage=Willkommen”.</p>

	<p>To use the properties files within a BIRT report:</p>

	<table class="table">
	<tr>
	<td><b>Localization required</b></td>
	<td><b>Where to set it</b></td>
	</tr>

	<tr>
	<td>Assign resource files to entire report</td>
	<td>Report &#x27a4; Properties &#x27a4; Resources &#x27a4; Resource Files</td>
	</tr>

	<tr>
	<td>Assign individual keys to a label</td>
	<td>Label &#x27a4; Properties &#x27a4; Localization &#x27a4; Text key</td>
	</tr>
	</table>

	<h3>Styles</h3>
	<p>Reports created with BIRT Designer can be richly formatted to adapt the look and feel of your existing web application or company color scheme.</p>

	<table class="table">
	<tr>
	<td><b>Type of style</b></td>
	<td><b>How the style is used</b></td>
	</tr>

	<tr>
	<td>Built-In Styles</td>
	<td>Built-in styles can be shared in a report library for managing style across multiple reports.</td>
	</tr>

	<tr>
	<td>CSS</td>
	<td>BIRT can import CSS files at design time or reference existing CSS files at run time.</td>
	</tr>
	</table>

	<p>I have collected some examples of CSS styles <a target="_blank"
	href="http://developer.actuate.com/community/forum/index.php?/files/file/1126-eclipse-newsletter-birt-code-samples/">here</a>.</p>

	<h3>Customization with Expressions, Scripting and Events</h3>

	<p>You can modify many aspects of reports in BIRT Designer using drag-and-drop capabilities and by setting properties, but the designer
	also supports more advanced customizations through expressions, scripting and events.</p>

	<p>The expression builder in BIRT allows you to do conditional report processing instead of hard coding values. For example, the expression
	below will include the ADDRESSLINE2 field in an address if it’s not null.</p>

<pre>
if (dataSetRow[“ADDRESSLINE2”] == nulll) {
dataSetRow[“ADDRESSLINE1”] + “, “ + dataSetRow[“CITY”] + “, “ + dataSetRow[“STATE”] + “ “ + dataSetRow[“POSTALCODE”];
} else {
dataSetRow[“ADDRESSLINE1”] + “ “ + dataSetRow[“ADDRESSLINE2”] + “, “ + dataSetRow[“CITY”] + “, “ + dataSetRow[“STATE”] + “ “ +
dataSetRow[“POSTALCODE”];
}
</pre>

	<p>Scripting of BIRT content can be done in either JavaScript or Java. You can add scripting to report object, data source and data element
	event types. Each of these event types has several events that you can overwrite. I have collected a variety of sample scripts
	<a target="_blank" href="http://developer.actuate.com/community/forum/index.php?/files/file/1126-eclipse-newsletter-birt-code-samples/">here</a>.</p>


	<p>You can also use head.js() in the report’s clientScripts method to load external JavaScript libraries for use on the client side.
	For an example on this, see this <a target="_blank"
	href="http://developer.actuate.com/community/forum/index.php?/blog/14/entry-496-using-external-javascript-libraries-with-birt-jquery-jvectormaps/">blog post</a>.</p>

	 <h2>BIRT Deployment Options</h2>
	<p>Now that you’ve created your designs, you’re ready to deploy them. There are several different ways to generate the output. You can run these
	designs directly from the BIRT Designer, or generate them from your Java application using the BIRT APIs.</p>

	<h3>BIRT Runtime Engine</h3>
	<p>BIRT supplies several APIs and an example Java EE application for generating and viewing reports. The major APIs are the Design Engine API (DEAPI),
	Report Engine API (REAPI) and the Chart Engine API (CEAPI). In addition to the APIs, BIRT supports scripting using either Java or JavaScript within
	report designs.</p>

	<table class="table">
	<tr>
	<td><b>API name</b></td>
	<td><b>How to use it</b></td>
	</tr>

	<tr>
	<td>Design Engine API (DEAPI)</td>
	<td>Use the DEAPI to create a custom report designer tool, or to explore or modify BIRT report designs. The BIRT Designer uses this API.
	You can call this API within a BIRT script to modify the currently running report design.</td>
	</tr>

	<tr>
	<td>Report Engine API (REAPI)</td>
	<td>Use the REAPI to run BIRT reports directly from Java code or to create a custom web application front end for BIRT.</td>
	</tr>

	<tr>
	<td>Chart Engine API (CEAPI)</td>
	<td>Use the CEAPI with other Java or Web applications to render charts directly (i.e., not inside of the BIRT Runtime). CEAPI can
	also be used within the BIRT Runtime.</td>
	</tr>
	</table>

	<p>To see API examples, visit <a target="_blank" href="http://developer.actuate.com/community/devshare">http://developer.actuate.com/community/devshare</a>
	and perform a search for “API.”</p>

	<h4>BIRT Report Engine Tasks</h4>
	<p>There are several tasks supplied by the REAPI that can be used to generate report output. A few key tasks are listed below.</p>

	<table class="table">
	<tr>
	<td><b>Class</b></td>
	<td><b>How to use it</b></td>
	</tr>

	<tr>
	<td>IRunAndRenderTask</td>
	<td>Use this task to run a report and create output directly to a supported format. This task does not create a report document.</td>
	</tr>

	<tr>
	<td>IRunTask</td>
	<td>Use this task to run a report and generate a report document that can be saved to disk.</td>
	</tr>

	<tr>
	<td>IRenderTask</td>
	<td>Use this task to render a report document created in the IRunTask to a specific format, such as HTML or PDF.</td>
	</tr>

	<tr>
	<td>IGetParameterDefinitionTask</td>
	<td>Use this task to create your own parameter GUI and obtain information about parameters, including default values.</td>
	</tr>

	<tr>
	<td>IDataExtractionTask</td>
	<td>Use this task to extract data from a report document. The BIRT Web Viewer uses this class to extract report data into CSV format.</td>
	</tr>
	</table>

	<h4>Example of BIRT Runtime Engine</h4>
	<p>With some simple code you can start up the BIRT Runtime Engine and Platform, create a task to accept the parameter value, generate
	and render the design to PDF, then destroy the engine and shut down the platform when finished. I have collected sample code to do that
	<a target="_blank" href="http://developer.actuate.com/community/forum/index.php?/files/file/1126-eclipse-newsletter-birt-code-samples/">here</a>.
	In an actual application, you’d want to separate the BIRT Runtime Engine and Platform code and only shut them down when you terminate your application.</p>

	<h3>Web Viewer</h3>
	<p>The BIRT Web Viewer is an example application that illustrates generating and rendering BIRT design outputs in a web application. This viewer
	demonstrates report pagination, an integrated table of contents, report export to several formats and printing to local and server side printers.</p>

    <img alt="birt web viewer" src="/community/eclipse_newsletter/2015/september/images/web viewer.png"><br><br>

    <h4>BIRT Web Viewer</h4>

	<p>The BIRT Web Viewer can be used in a variety of ways:</p>

	<table class="table">
	<tr>
	<td><b>Use for the viewer</b></td>
	<td><b>How it’s used</b></td>
	</tr>

	<tr>
	<td>Standalone (Example Viewer)</td>
	<td>Use as a pre-built web application, for running and viewing static reports, that doesn’t require security.</td>
	</tr>

	<tr>
	<td>Modify Viewer source</td>
	<td>Use as a starter web application, customizing it for your environment.</td>
	</tr>

	<tr>
	<td>RCP application</td>
	<td>Use as a plug-in for your existing RCP application.</td>
	</tr>

	<tr>
	<td>Integrated with existing web application</td>
	<td>Integrate using URLs or BIRT JSP tag library.</td>
	</tr>

	</table>

    <p>The BIRT Web Viewer consists of two main Servlets: the ViewerServlet and the BirtEngineServlet. These Servlets handle three mappings:
    (/frameset, /run and /preview).</p>

    <table class="table">
    <tr>
    <td><b>Servlet mapping</b></td>
    <td><b>What it does, how it’s used</b></td>
    </tr>

    <tr>
    <td>/frameset</td>
    <td>Renders the report in the full AJAX viewer, complete with toolbar, navigation bar and table of contents. This mapping also generates an
    intermediate report document from the report design file to support the AJAX-based features. For example,
    http://localhost:8080/viewer/frameset?__report=myreport.rptdesign&parm1=value.</td>
    </tr>

    <tr>
    <td>/run</td>
    <td>Runs and renders the report but does not create a report document. This mapping does not supply HTML pagination, table or contents,
    or toolbar features, but it does use the AJAX framework to collect parameters, support report cancelling and retrieve the report output in HTML
    format. For example, http://localhost:8080/viewer/run?__report=myreport.rptdesign&parm1=value.</td>
    </tr>

    <tr>
    <td>/preview</td>
    <td>Runs and renders the report but does not generate a report document, although an existing report document can be used (in which case
    just the render operation occurs). The output from the run and render operation is sent directly to the browser. For example,
    http://localhost:8080/viewer/preview?__report=myreport.rptdesign&parm1=value.</td>
    </tr>
    </table>


    <h3>Other BIRT Web Viewer Settings</h3>
	<p>Many URL parameters can be used with the Servlet mappings.  The BIRT Web Viewer itself has several key settings that can be configured
	by modifying the web.xml file located in the WebViewerExample/WEB-INF folder.  The BIRT Web Viewer also includes a set of JSP tags, available
	from the BIRT Web Tools Integration download, that make it easy to integrate BIRT reports into browser pages. All of these options are described
	in the BIRT Web Viewer documentation here:
	<a href="https://eclipse.org/birt/documentation/integrating/viewer-usage.php">https://eclipse.org/birt/documentation/integrating/viewer-usage.php</a>.</p>

	<h2>BIRT Output Formats</h2>
	<p>In addition to delivering paginated report content to a web browser, BIRT also supports several other output formats. These formats
	listed below are supported by both the Report Engine API as well as the BIRT Web Viewer.</p>

	<table class="table">
	<tr>
	<td><b>Output type</b></td>
	<td><b>What it means</b></td>
	</tr>

	<tr>
	<td>Paginated HTML output</td>
	<td>An example web viewer is included with BIRT, allowing for on-demand paginated web output.</td>
	</tr>

	<tr>
	<td>Microsoft Office</td>
	<td>Word, Excel and PowerPoint.</td>
	</tr>

	<tr>
	<td>HTML</td>
	<td>Suitable for creating HTML pages of report data that can be deployed to any server.</td>
	</tr>

	<tr>
	<td>PDF</td>
	<td>Adobe PDF output suitable for emailing or printing.</td>
	</tr>

	<tr>
	<td>Postscript</td>
	<td>Output that can be directed to a Postscript-compatible printer.</td>
	</tr>

	<tr>
	<td>Open Document</td>
	<td>Text, spreadsheet and presentation.</td>
	</tr>
	</table>

	<h2>BIRT Extension Points</h2>
	<p>The APIs in BIRT define extension points that let the developer add custom functionality to the BIRT framework. These extensions can
	be in the form of custom data sources, report items, chart types, output formats and functions. Once implemented, these custom extensions
	will show along with the built-in types. For example, you can create a custom report item, like a rotated text label, that will show up in
	the BIRT Palette along with the existing items. Below are some of the most used extension points.</p>

	<table class="table">
	<tr>
	<td><b>Extension point</b></td>
	<td><b>How it’s supported</b></td>
	</tr>

	<tr>
	<td>Data Sources</td>
	<td>BIRT supports the Eclipse Open Data Access (ODA) architecture, which means it can be extended to support custom data sources.</td>
	</tr>

	<tr>
	<td>Functions</td>
	<td>BIRT allows you to create custom functions that extend the functions available in BIRT Expressions.</td>
	</tr>

	<tr>
	<td>Report Items</td>
	<td>Report Items can be extended, allowing you to create your own custom report items.</td>
	</tr>

	<tr>
	<td>Chart Types</td>
	<td>Additional chart types can be added to BIRT as plug-ins.</td>
	</tr>

	<tr>
	<td>Output Emitters</td>
	<td>BIRT can be extended to include your own custom output type. For example, a simple CSV emitter can be added to BIRT.</td>
	</tr>
	</table>

	<h2>BIRT Resources</h2>

	<table class="table">
	<tr>
	<td><b>Resource name</b></td>
	<td><b>Where it’s found</b></td>
	</tr>

	<tr>
	<td>Eclipse BIRT Project Site</td>
	<td><a href="http://www.eclipse.org/birt">http://www.eclipse.org/birt</a></td>
	</tr>

	<tr>
	<td>Actuate’s BIRT Developer Center</td>
	<td><a target="_blank" href="http://developer.actuate.com/">http://developer.actuate.com/</a></td>
	</tr>

	<tr>
	<td>OpenText Actuate Information Hub (commercial deployment offering)</td>
	<td><a target="_blank" href="http://www.actuate.com/resources/product-downloads/">http://www.actuate.com/resources/product-downloads/</a></td>
	</tr>

	<tr>
	<td>Analytics Designer (commercial report designer)</td>
	<td><a target="_blank" href="http://www.actuate.com/resources/product-downloads/">http://www.actuate.com/resources/product-downloads/</a></td>
	</tr>

	<tr>
	<td>Submitting/Searching BIRT Bugs</td>
	<td><a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=BIRT">https://bugs.eclipse.org/bugs/enter_bug.cgi?product=BIRT</a></td>
	</tr>

	<tr>
	<td>BIRT Design Guides</td>
	<td><a target="_blank" href="http://developer.actuate.com/design-center/creating-birt-designs/">http://developer.actuate.com/design-center/creating-birt-designs/</a></td>
	</tr>

	<tr>
	<td>BIRT Deployment Guides</td>
	<td><a target="_blank" href="http://developer.actuate.com/deployment-center/deployment-guides/">http://developer.actuate.com/deployment-center/deployment-guides/</a></td>
	</tr>

	<tr>
	<td>Google+</td>
	<td><a target="_blank" href="https://plus.google.com/+actuate/about">https://plus.google.com/+actuate/about</a></td>
	</tr>

	<tr>
	<td>Twitter</td>
	<td><a target="_blank" href="https://twitter.com/OT_Analytics">https://twitter.com/OT_Analytics</a>
	</td>

	<tr>
	<td>Facebook</td>
	<td><a target="_blank" href="https://www.facebook.com/BIRT">https://www.facebook.com/BIRT</a></td>
	</tr>

	<tr>
	<td>Design Center - Video Gallery</td>
	<td><a target="_blank" href="http://developer.actuate.com/design-center/video-gallery/">http://developer.actuate.com/design-center/video-gallery/</a></td>
	</tr>

	<tr>
	<td>Deployment Center - Video Gallery</td>
	<td><a target="_blank" href="http://developer.actuate.com/deployment-center/video-gallery/">http://developer.actuate.com/deployment-center/video-gallery/</a></td>
	</tr>

	<tr>
	<td>Book: BIRT – A Field Guide</td>
	<td><a target="_blank" href="http://www.amazon.com/BIRT-Field-Edition-Eclipse-Series/dp/0321733584">http://www.amazon.com/BIRT-Field-Edition-Eclipse-Series/dp/0321733584</a></td>
	</tr>

	<tr>
	<td>Blogs</td>
	<td><a target="_blank" href="http://developer.actuate.com/community/forum/index.php?/blogs/">http://developer.actuate.com/community/forum/index.php?/blogs/</a>
	<a target="_blank" href="http://blogs.actuate.com">http://blogs.actuate.com</a></td>
	</tr>
	</table>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/september/images/jesse.png"
        alt="Jesse Freeman" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Jesse Freeman<br />
        <a target="_blank" href="http://www.opentext.com/">OpenText</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://blogs.actuate.com/author/jesse-freeman/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/JFreemanActu">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

