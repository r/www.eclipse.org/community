<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    <p>The era of smart devices is here - from our smartphones, smart cars and intelligent fridges, to home surveillance and industrial sensors. Each device
    collects and churns out data 24/7, against a backdrop of more “traditional” digital systems, and online channels that have become the foundation for today’s
    social and business environments.</p>

	<p>So much data, and so much potential. If only we could make sense of it all. This is where analytics (and data visualization) comes into the picture.
	The bridge between raw data and actionable insights that drive better decisions, analytics has become a key tool for developers and digital strategists alike.</p>

	<p>As a new generation of data consumers goes live, the need for flexible and open, mobile-ready development tools becomes increasingly critical. This next wave of
	online experiences and devices drives more demand for simpler, more task-oriented and personalized apps. What is needed is a reporting server to help design,
	deploy, and embed these visualizations. Fortunately, this is just what BIRT and the latest <a target="_blank" href="http://www.opentext.com/what-we-do/products/analytics">
	analytics solutions from OpenText</a> based on this technology provide today.</p>

	<h2>Smart Machines and Small Data</h2>

	<p>Looking forward, the “smart machine” era will be a major disruption and could be one of the most disruptive in the history of technology. Smart machines
	can deal with high levels of complexity and uncertainty and come up with their own theorems from the information they consume. Sure this is an “AI” type
	scenario, but like earlier generations of AI and machine learning—where I started my career—it is sometimes surprising how quickly these approaches leap
	from the lab into real life applications, especially when there is clear value for everyday tasks and users.</p>

	<p>Yet the potential for information overload is also high, especially as these new machines come on line and automate more complex information flows.
	So the need for less clutter becomes paramount. In data visualization this begins with good information design as prescribed by <a target="_blank"
	href="http://www.edwardtufte.com/tufte/">Edward Tufte</a> and other data visualization pioneers. Their frameworks and examples help us find simple approaches
	to deliver and visualize data and its insights and bring the user experience back to the foreground.</p>

	<p>It’s less about systems at this point, but rather about what answer or insight we can present, and in what format. And knowing we have the right
	API to embed our dashboard or animated infographic, or alert in our customer’s application or device of choice. This is the “last mile” after the
	sensors and sources have been wired to data models; where value is created for end-users and impressions are made and shared.</p>

	<p>For these reasons, the <a target="_blank" href="http://www.smalldatagroup.com">Small Data</a> movement which I’ve helped to define and evangelize along with groups such
	as the <a target="_blank" href="http://smalldata.io/">small data lab at Cornell</a> and the <a target="_blank" href="http://okfn.org/">Open Knowledge
	Foundation</a> in the UK has established itself over the past few years as both a counterpoint to “big systems thinking” and a new way to think about
	data ownership and privacy, and (in my scope) a new design philosophy. Driven by the goal of creating simpler, smarter, more responsive and more social apps,
	small data has become the darling of a growing number of designers and digital types, and very much fits with the open source model as well.</p>

	<h2>Tools for Today’s and Tomorrow’s Apps</h2>

	<p>The pursuit of tools and approaches that turn information into actionable intelligence at any level remains our focus. Embedding (visual) intelligence
	in both traditional and emerging apps/devices continues to require design tools that easily access big and small data, along with data management, granular
	security, and an ability to apply third-party visualizations. It must incorporate unstructured content, and create data-driven apps that will work in any
	environment. Oh, and they will also likely need to connect to any number of new devices as new data sources, and also support potentially millions of users,
	each with little or no training.</p>

	<p>More than ever, embedded BI and analytics platforms built on BIRT provide a route forward for developing these types of high-scale and complex yet
	personalized visualizations – and even creating the next big mobile, IoT or wearable app.</p>

	<p>The author is vice president of product marketing & innovation at embedded analytics leader OpenText Analytics (formerly Actuate), and is a former
	data scientist, gamer and C programmer from way back when the Macintosh Quadra was the hottest thing coming out of Cupertino.</p>


<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/september/images/allen.png"
        alt="Allen Bonde" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Allen Bonde <br />
        <a target="_blank" href="http://www.opentext.com/">OpenText</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/in/allenbonde">Linkedin</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/abonde">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>
