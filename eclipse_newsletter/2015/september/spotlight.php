<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
  require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
  $App = new App ();
  $Nav = new Nav ();
  $Menu = new Menu ();
  include ($App->getProjectCommon ());

  // Begin: page-specific settings. Change these.
  $pageTitle = "User Spotlight - Achim L&ouml;rke ";
  $pageKeywords = "eclipse, newsletter, achim, lorke, bredex";
  $pageAuthor = "Christopher Guindon";

  // Uncomment and set $original_url if you know the original url of this article.
  //$original_url = "http://eclipse.org/community/eclipse_newsletter/";
  //$og = (isset ( $original_url )) ? '<li><a href="' . $original_url . '" target="_blank">Original Article</a></li>' : '';

  // Place your html content in a file called content/en_article1.php
  ob_start ();
  include ("content/en_" . $App->getScriptName ());
  $html = ob_get_clean ();

  if (isset ( $original_url )) {
    $App->AddExtraHtmlHeader ( '<link rel="canonical" href="' . $original_url . '" />' );
  }
  $App->AddExtraHtmlHeader ( '<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles.css" media="screen" />' );

  // Generate the web page
  $App->generatePage ( NULL, $Menu, NULL, $pageAuthor, $pageKeywords, $pageTitle, $html, $Breadcrumb );
?>