<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div id="fullcolumn">
  <div id="midcolumn">
    <h1><?php echo $pageTitle; ?></h1>
    	<div class="row">
		  		  <div class="col-sm-7"><a href="https://marketplace.eclipse.org/"><img class="img-responsive" alt="marketplace logo" src="/community/eclipse_newsletter/2015/august/images/mtkplacelogo.png"/></a></div>
		  		  <div class="col-sm-17"><p>The <a href="https://marketplace.eclipse.org/">Eclipse Marketplace</a> makes it easy to discover new solutions that will help you in your everyday work. 
		  		  These tools and plugins, all created by the Eclipse Community, can be installed directly to Eclipse with the Eclipse Marketplace Client.</p> 
		  		  <p>In total, these solutions have been downloaded 15,423,526 times and installed directly from Eclipse! There are so many solutions to discover, so we 
		  		  decided to help by highlighting some of the more popular ones.</p> </div>
		</div>
    	

			    
		<h2>Top 10 Most Popular Solutions</h2>
		<p>Here are the most popular solutions on Eclipse Marketplace, based solely on the number of downloads in the past 30 days.</p>
			<ol>
			<li><a href="https://marketplace.eclipse.org/content/subversive-svn-team-provider/metrics">Subversive - SVN Team Provider</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/eclipse-color-theme/metrics">Eclipse Color Theme</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/subclipse/metrics">Subclipse</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/jboss-tools/metrics">JBoss Tools</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/spring-tool-suite-sts-eclipse/metrics">Spring Tool Suite (STS) for Eclipse</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/pydev-python-ide-eclipse/metrics">PyDev - Python IDE for Eclipse</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/optimizer-eclipse/metrics">Optimizer for Eclipse</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/eclipse-moonrise-ui-theme/metrics">Eclipse Moonrise UI Theme</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/testng-eclipse/metrics">TestNG for Eclipse</a></li>  
			<li><a href="https://marketplace.eclipse.org/content/android-development-tools-eclipse/metrics">Android Development Tools for Eclipse</a></li>  
		</ol>	
		<p><small>Note: This list was created on August 26, 2015.</small></p>	
			
			
			
		<h2>Top 10 Up and Coming Solutions</h2>
		<p>These solutions are definitely some you want to have in your toolbox! They have climbed the ranks more quickly than the other solutions over the past year.</p>
			<ol>
			<li><a href="https://marketplace.eclipse.org/content/scalastyle">Scalastyle</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/uml-designer">UML Designer</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/early-security-vulnerability-detector-esvd">Early Security Vulnerability Detertor - ESVD</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/reactive-blocks">Reactive Blocks</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/elastic-cobol">Elastic COBOL</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/oracle-coherence-tools">Oracle Coherence Tools</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/obeo-designer-community">Obeo Designer</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/atlassian-clover-eclipse">Atlassian Clover for Eclipse</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/fontsize">Fontsize</a></li> 
			<li><a href="https://marketplace.eclipse.org/content/maintainj">Maintain J</a></li> 
    		</ol>
    		<p><small>Note: This list was created based on data from July 2014 - July 2015.</small></p>
			  </div>

  <!-- remove the entire <div> tag to omit the right column!  -->
  <div id="rightcolumn">
    <div class="sideitem text-center">
      <h6>About the Author</h6>
      <p class="author-picture"><img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/roxanne.jpg"
        alt="Roxanne Joncas" /></p>
      <p class="author-name">
        Roxanne Joncas <br />
        <a target="_blank" href="http://eclipse.org/">Eclipse Foundation</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://www.linkedin.com/profile/view?id=70967844">Linkedin</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/roxannejoncas">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
    </div>
    <!--<div class="sideitem">
        <h6>Related Links</h6>
        <ul>
          <li><a href="/community/eclipse_newsletter/">The Eclipse Newsletter</a></li>
        </ul>
      </div> -->
  </div>
</div>
