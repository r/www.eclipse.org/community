<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>
    	<p><a target="_blank" href="http://www.pydev.org/">PyDev</a> is an open source plugin which turns Eclipse into a resourceful Python IDE. It's a mature plugin and has been in active development for more than 12 years already!</p>

		<p>It has features that are to be expected from most Eclipse language plugins, such as:</p>
			<ul>
				<li>Code completion</li>
				<li>Code analysis</li>
				<li>Go to definition</li>
				<li>Find References</li>
				<li>Refactoring</li>
				<li>Debugger</li>
				<li>Tokens browser</li>
				<li>Unittest integration</li>
			</ul>
		<p>And a number of advanced features, which aren't so common, such as:</p>

			<ul>
				<li>Interactive console</li>
				<li>Type hinting</li>
				<li>Remote debugger</li>
				<li>Find Referrers in Debugger</li>
				<li>Code coverage</li>
				<li>Django integration</li>
			</ul>

		<p>And more... (see the <a target="_blank" href="http://www.pydev.org/">PyDev homepage</a> for more details).</p>

		<p>It's also one of the most used and downloaded plugins in the Eclipse Marketplace and is kept going through <a target="_blank" href="https://sw-brainwy.rhcloud.com/supporters/PyDev/">community contributions</a>,
		<a target="_blank" href="https://sw-brainwy.rhcloud.com/support/pydev-2014/">crowdfunding campaigns</a> and licenses from it's commercial counterpart
		(<a target="_blank" href="http://www.liclipse.com/">LiClipse</a>, which is a PyDev standalone, plus other niceties).</p>

		<p>Now, this article will focus in a single point on PyDev/Eclipse, which is navigating through a codebase, which is one of the main advantages of using an IDE. Below is an explanation on the best ways
		to get to the code you're interested in when coding in PyDev/Eclipse.</p>

	<h2>1. Any file in your workspace</h2>

		<p>The simplest way is searching for any resource for any project you have available (this is actually provided by Eclipse itself). So, if you know the file name (or at least part of it),
		use <b>Ctrl+Shift+R</b> and filter using the Open Resource dialog:</p>

			<img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/0_find_resource.png"
        alt="Find Resource" /><br/>

	<h2>2. Any Python Token</h2>

		<p>Another way of getting to what you want is using the PyDev Globals Browser (using <b>Ctrl+Shift+T</b>).</p>

		<p>This is probably one of the main ways to navigate the code as a single place can be used to filter for package names, classes, methods and attributes inside your own projects or any other package in Python itself.
		It also allows for advanced filtering, so you can search for tokens only inside a package (i.e.: dj.tz filters for any 'tz' token inside django in the example below).</p>
		<img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/1_globals.png"
        alt="Global Browser" /><br/>

    <h2>3. Quick Outline (current editor)</h2>
    	<p><b>Ctrl+O</b> will show a Quick Outline which shows the structure of your current file. And pressing <b>Ctrl+O</b> one more time in this dialog will also show the structure of superclasses in the hierarchy (you can see that in
    	 the example below __setitem__ appears twice, once for the method in this class and another one for the superclass).</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/3_quick.png"
        alt="Quick Outline" /><br/>

    <h2>4. Selection History</h2>

    	<p>Go back and forth in your selection: <b>Alt+Left</b> goes to the place you were before and <b>Alt+Right</b> to the place you just came from... this allows you to easily navigate through the places you've recently visited.</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/4_history.png"
        alt="History" /><br/>

    <h2>5. Open files</h2>
    	<p>To filter through the open files you can use <b>Ctrl+E</b>: a dropdown will appear and from there you can filter through its name. You can even close existing editors from this dropdown using <b>Del</b>.</p>

        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/5_current.png"
        alt="Current" /><br/>

    <h2>6. Go to previous next token (class or method)</h2>

    	<p><b>Ctrl+Shift+Up</b> and <b>Ctrl+Shift+Down</b> allows you to quickly navigate from your current position to the previous or next method (selecting the full method/class name).</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/6_prev_next.png"
        alt="Prev Next Method" /><br/>

    <h2>7. Navigate through occurrences and errors in the file</h2>
    	<p><b>Ctrl+Dot</b> allows navigation through occurrences and errors found in the file. In the case below, we navigate through the occurrences of 'func'.</p>

        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/7_occurrences.png"
        alt="Occurences and Errors" /><br/>

    <h2>8. Go to some view/menu/action/preference</h2>

    	<p>This is an Eclipse standard mechanism: using <b>Ctrl+3</b> allows you to navigate to any part of the IDE.</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/8_place.png"
        alt="Preferences" /><br/>

	<h2>9. References</h2>

		<p><b>Ctrl+Shift+G</b> will create a search showing all the references to the token under the cursor (and the search view where results are shown can be navigated with Ctrl+Dot).</p>
		<img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/9_references.png"
        alt="References" /><br/>

    <h2>10. Go to definition</h2>

    	<p>Just press <b>F3</b> or <b>Ctrl+Click</b> an available token and go directly to the selected place.</p>

    <h2>11. Hierarchy View</h2>

    	<p>Using <b>F4</b> shows a hierarchy view. There you can see the structure of your classes.</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/11_hierarchy.png"
        alt="Hierachy View" /><br/>

    <h2>12. Show In</h2>

    	<p><b>Alt+Shift+W</b> allows you to see the current file in a given place (such as the PyDev Package Explorer or the System Explorer from your OS) or your current class/method in the Outline View.</p>
        <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/12_show_in.png"
        alt="Show In" /><br/>

    <h2>Other Shortcuts</h2>

    	<p>The shorcuts below are available in standard Eclipse, so you should also definitely get to know them :)</p>

    		<ul>
				<li><b>Ctrl+L</b> allows you to navigate to a given line in your current editor.</li>
				<li><b>Ctrl+Q</b> goes to the place where the last edition was made.</li>
				<li><b>Ctrl+F6</b> navigates through the opened editors. In LiClipse <b>Ctrl+Tab</b> is also bound to it by default-- and I suggest you also add this binding if you aren't using LiClipse :)</li>
				<li><b>Ctrl+F7</b> navigates through opened views (i.e.: Package Explorer, Outline, etc.)</li>
				<li><b>Ctrl+F8</b> navigates through opened perspectives (i.e.: PyDev perspective, Debug perspective, etc).</li>
				<li><b>Ctrl+F10</b> opens the menu for the current view (so you can select filters in the Package Explorer, etc.)</li>
				<li><b>F12</b> focuses the editor (so, you can go from any view to the editor)</li>
				<li><b>Ctrl+H</b> Opens the search dialog so you can do text searches</li>
				<li><b>Ctrl+Shift+L</b> twice goes to the keybindings preferences</li>
			</ul>
		<p>Now you can enjoy going really fast to any place you wish inside PyDev!</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/fabio.png"
        alt="Fabio Zadrozny" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Fabio Zadrozny <br />
        <a target="_blank" href="https://sw-brainwy.rhcloud.com/">Brainwy Software</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://pydev.blogspot.com/">Blog</a>
        </li>
        <!--  <li><a class="btn btn-small btn-warning" target="_blank" href="">Twitter</a></li>-->
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

