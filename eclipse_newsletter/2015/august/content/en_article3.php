<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>
<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    	<p>JBoss Tools is a set of plugins with the tagline "Eclipse Plugins for
		JBoss Technology".  This tagline is true, but JBoss Tools actually
		contains a lot of features that are not limited to just JBoss specific
		technologies. In this article I'll highlight a few of those features
		that is useful for (almost) all.</p>

	<h2>Quick Connect a Remote Java debugger</h2>

		<p>Eclipse has great support for running Java applications and application servers
		directly from the IDE, but sometimes you are in a situation where
		you cannot do that. You might have to launch the process from a command line directly
		or from your favorite build tool, and from then manually setup a remote launch in Eclipse.</p>

		<p>We found that a bit tedious, thus we added support for Connecting a
		debugger directly from our _JMX Navigator_ view which has the
		functionallity of listing any running Java Hotspot based JVM.</p>

		<p>Imagine you are running Tomcat from the command line with debug enabled:</p>

		<code>sh catalina.sh jpda start</code><br/><br/>

		<p>Now instead of remembering which port to connect to you simply
		right click on the relevant process in the JMX Navigator view and click
		"Connect to Debugger".</p>

			<img alt="connect debugger" class="img-responsive"
			src="/community/eclipse_newsletter/2015/august/images/connect_debugger.png"><br/>

		<p>Now JBoss Tools will setup a remote connection to the debugger,
		added all projects to the source lookup (which you can edit if you want).</p>

		<p>That is it. Nothing else needed for quick debugging of externally launched applications.</p>

		<p>You can read more about this feature <a target="_blank" href="http://tools.jboss.org/blog/2015-03-17-debugging-an-externally-launched-wildfly.html">here</a>.</p>

	<h2>Live Reload</h2>

		When developing web applications it is tedious to have to click Reload
		in the browser when you do updates.

		<p>To help that in JBoss Tools, when an HTML/CSS/JavaScript or image file
		is changed in the workspace, or a resource is redeployed on your
		server, the embedded LiveReload server running in JBoss Tools commands
		your browsers to reload its content. No need to go back and forth
		between the IDE and the browser to see the changes on the pages.</p>

			<img alt="live reloader server" class="img-responsive"
			src="/community/eclipse_newsletter/2015/august/images/livereload-open-with-webbrowser-via-livereload-server.png"><br/>

		<p>This is done by supporting the LiveReload protocol, meaning this will
		work with any browser that has LiveReload plugin installed or if
		you allow JBoss Tools it will inject the necessary Javascript into
		your HTML page to have it work automatically.</p>

			<img alt="live reloader browser" class="img-responsive"
			src="/community/eclipse_newsletter/2015/august/images/livereload-open-in-web-browser-via-browser-messagedialog.png"><br/>

		<p>This functionallity is not only available on resources in the filesystem,
		but also when serving content via a server controlled by WTP; meaning you
		can have this enabled on even remotely running servers and you can even
		use the "Open in External Device" to have a QR code to easily browse with
		LiveReload enabled on your mobile phone or tablet.</p>

			<img alt="live reloader qr code" class="img-responsive"
			src="/community/eclipse_newsletter/2015/august/images/livereload-open-in-web-browser-via-qrcode-dialog.png"><br/>

	<h2>Deploy Only Server</h2>

		Finally there is our Deploy Only server.

		<p>The Deploy only server is a server adapter which has support for all the incremental
		deploy features our JBoss server adapter has. Meaning it can keep a directory
		(both locally and remotely) in sync with what you have defined as a deployment in Eclipse.</p>

		<p>This means you can use JBoss Tools to deploy your PHP, Node, Ruby-on-rails, etc. application
		to a remotely running instance and have it done incrementally directly from within the Eclipse IDE.</p>

		<p>Combine that with Live Reload above and you get a nice quick continous deployment feedback
		without requiring or setting up additional tools.</p>

		<p>You get to this server by using File &#x27a4; New &#x27a4; Server and choose the "Deploy Only Server".</p>

		<img alt="deploy only" class="img-responsive"
			src="/community/eclipse_newsletter/2015/august/images/deploy_only.png"><br/>

		<p>Then you specify the directory to deploy to and you end up with server
		in the server view.</p>

		<p>With this server you can now deploy WTP modules or any content made
		Deployable via "Mark as Deployable" on any resource in Eclipse workspace.</p>

	<h2>...and more</h2>

		<p>JBoss Tools also adds support for technologies like <a target="_blank" href="http://tools.jboss.org/features/cdi.html">CDI</a>,
		<a target="_blank" href="http://tools.jboss.org/features/webservices.html">webservices - JAX-RS or JAX-WS</a> and
		<a target="_blank" href="http://tools.jboss.org/blog/batch-diagram-editor.html">Batch</a> which are not (yet) present in Eclipse WTP.
		There is <a target="_blank" href="http://tools.jboss.org/features/browsersim.html">BrowserSim</a> and CordovaSim for mobile testing to extend what is in Eclipse Thym.
		We add features utilizing Maven that goes beyond what is in m2e or m2e-wtp.</p>

		<p>...and then of course there are the JBoss and Red Hat specific feature support like Hibernate, OpenShift, WildFly, EAP, Fuse, etc.</p>

		<p>You can see more at <a target="_blank" href="http://tools.jboss.org/features/index.html">http://tools.jboss.org/features/index.html</a></p>

	<h2>The End</h2>

		<p>Hope you enjoyed the brief look into some of the generic features provided by JBoss Tools and want to give it a try.</p>

		<p>You can find more info at these locations:</p>

			<ul>
			 <li>Marketplace install: <a href="https://marketplace.eclipse.org/content/jboss-tools">https://marketplace.eclipse.org/content/jboss-tools</a></li>
			 <li><a href="http://tools.jboss.org">http://tools.jboss.org</a></li>
			 <li>Features: <a href="http://tools.jboss.org/features">http://tools.jboss.org/features</a></li>
			 <li>Blog: <a href="http://tools.jboss.org/blog">http://tools.jboss.org/blog</a></li>
			 <li>How To's: <a href="http://tools.jboss.org/documentation/howto">http://tools.jboss.org/documentation/howto</a></li>
			 <li>git repo: <a href="https://github.com/jbosstools">https://github.com/jbosstools</a></li>
			 <li>Issue tracker: <a href="https://issues.jboss.org/jira/browse/JBIDE">https://issues.jboss.org/jira/browse/JBIDE</a></li>
			 <li>Twitter: <a href="https://twitter.com/jbosstools">https://twitter.com/jbosstools</a></li>
		    </ul>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/june/images/maxandersen.jpeg"
        alt="Max Andersen" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Max Rydahl Andersen <br />
        <a target="_blank" href="http://www.redhat.com/en">Red Hat</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="http://xam.dk/blog/">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/maxandersen">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

