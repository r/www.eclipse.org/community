<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>

    	<p>Many years ago the Spring framework revolutionized the way Java developers implemented enterprise apps. Nowadays Spring is leading a new revolution in the way developers implement cloud native applications. They are not big monolithic
    	applications that are forklifted to run in the cloud, simply because there is the old-fashioned app server being hosted on an Amazon machine.  This next generation of cloud native applications is built with a native runtime platform,
    	native application framework, and native infrastructure automation.</p>

		<p>At the heart of this revolution is a cloud native application framework - the Spring Framework - and key projects like Spring Boot and Spring Cloud. Spring Boot makes it easy to build extremely small, modular and self-contained
		Spring microservices. These are the small, independent apps that are the building blocks of cloud native applications.  Those building blocks need support and must work together in a resilient way, scale up to high demand situations,
		work in various and across datacenter, and with cloud native runtime platform services. This is where Spring Cloud provides a solid foundation, where a huge part of it is based on the Netflix open-source products, like Eureka, Ribbon,
		Hystrix, and Turbine. Spring Boot together with Spring Cloud fulfills the promise to provide developers with (1) microservice-infrastructure-as-a-service, (2) purpose built client libraries for those runtime services, and (3) a
		consistent, repeatable infrastructure automation to make it all work - a solid foundation for your cloud native application development.</p>

		<p>Both revolutions were accompanied by great developer tooling. The tooling made it easy for developers to get started with those technologies as well as to work with large code bases in an efficient way. The Spring IDE was the
		central piece for many years. The Spring IDE told Eclipse to understand Spring apps, helping developers with thousands of features to make working with Spring apps a pleasure - just like JDT makes it a pleasure to work with plain
		Java code. This tradition continued with the Spring Tool Suite, which combines the Spring IDE tooling with additional extensions for working with Pivotal tc Server and foremost Pivotal Cloud Foundry, and Pivotal Web Services.</p>

		<p>Today the Spring Tool Suite doesn’t only understand your Spring applications and provide you with content-assist where possible, additional navigations, visualizing your request-mappings, helping you in creating new Spring
		projects from scratch, guide you through hundreds of examples how to use Spring, visualizing your Spring Integration and Batch flows, to name just a few highlights. The Spring Tool Suite continues to innovate. The latest
		versions help you to start your journey towards cloud native applications designing 12 factor applications and microservices with Spring Boot and Spring Cloud. It starts to understand your cloud native applications and
		provide new ways to work with potentially many small microservice projects, still providing a consistent view of the various applications and services. And it doesn’t stop on your machine, it reaches out the cloud and
		lets you deploy, run, test, and debug your microservices on Cloud Foundry - the premier cloud platform from Pivotal to run your cloud native applications.</p>

		<p>Go, grab the Spring Tool Suite distribution download from <a target="_blank" href="http://spring.io/tools">http://spring.io/tools</a> - or install the <a href="https://marketplace.eclipse.org/content/spring-tool-suite-sts-eclipse">
		STS components</a> from the Eclipse marketplace. And you are ready to join the cloud native revolution in application development. Creating your first Spring-Boot-based microservice and deploy it to Pivotal Web Services is a matter of minutes.</p>

		<p>For more information, check out the <a target="_blank" href="http://spring.io/guides">Getting started Guides</a>.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/martin2.png"
        alt="Martin Lippert" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Martin Lippert <br />
        <a target="_blank" href="http://pivotal.io/">Pivotal</a>
      </p>
      <ul class="author-link list-inline">
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://spring.io/team/mlippert">Blog</a>
        </li>
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/martinlippert">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

