<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<h1 class="article-title"><?php echo $pageTitle; ?></h1>


    <h2>Introduction</h2>
    	<p><i><a target="_blank" href="https://en.wikipedia.org/wiki/Test-driven_development">Test Driven Development (TDD)</a></i> is a practice followed by most modern software projects nowadays. With this technique you
    	always create automated regression tests along with your code. If you follow the <i>Test First</i> approach you even create your test cases before your actual code to help specifying clean APIs and come-up with
    	minimal concise implementations.</p>

		<p>Test suites are typically based on unit test frameworks like <a target="_blank" href="http://junit.org/">JUnit</a> or <a target="_blank" href="http://testng.org/doc/index.html">TestNG</a> and should be executed
		frequently during development - at least once before every checking-in. Fortunately, the Eclipse IDE supports this with integrated test runners.</p>

		<p>While the existing tests verify the correctness of the code under test there is no guarantee that the entire code base is actually tested. Especially legacy code bases often come with no or limited test coverage.
		This is where so-called <i><a target="_blank" href="https://en.wikipedia.org/wiki/Code_coverage">code coverage</a></i> tools come into picture:  Such tools determine the portions of the code that has actually been executed by a test suite.
		Conversely code coverage tools help you to identify the untested parts of your code.</p>

		<p>While your test suites verify the correctness of your code, code coverage measures the completeness of your test suites.</p>

			<br/><img alt="correctness completness" src="/community/eclipse_newsletter/2015/august/images/1-correctnesscompleteness.png" class="img-responsive"><br/>

	<h2>The Eclipse Plug-In</h2>
		<p>The <a target="_blank" href="http://www.eclemma.org/">EclEmma code coverage plug-in</a> was created in 2006 and open-sourced under the Eclipse Public License (EPL). It was quickly adopted by the Eclipse Java developer community
		and got nominated for the Eclipse Community Award in 2007 (Finalist) and in 2008 (Winner). EclEmma retrieves code coverage metrics as conveniently as you execute your test suites directly in the IDE . The plugin can be easily installed
		from its update site at http://update.eclemma.org/ on any Eclipse installation of version 3.5 or above. It is also available from the <a href="http://marketplace.eclipse.org/content/eclemma-java-code-coverage">Eclipse Marketplace</a>.</p>

		<p>After you have installed the plugin, you can run your tests using the new <i>Coverage As</i> launch configuration. This will display the code coverage in a convenient tree view as well as directly in your code.</p>

			<br/><img alt="correctness completness" src="/community/eclipse_newsletter/2015/august/images/2-launchtoolbar.gif" class="img-responsive"><br/>


		<p>For any Java application executed in coverage mode, EclEmma collects coverage data and automatically calculates coverage statistics in the <a target="_blank" href="http://www.eclemma.org/userdoc/coverageview.html"><i>Coverage</i> view</a>
		as soon as the application terminates. This view shows different <a target="_blank" href="http://www.eclemma.org/jacoco/trunk/doc/counters.html">coverage metrics</a> (instructions, branches, cyclomatic complexity, lines, methods, classes)
		and allows drilling-down from project to method level.</p>

			<br/><img alt="correctness completness" src="/community/eclipse_newsletter/2015/august/images/3-coverageview.png" class="img-responsive"><br/>

		<p>In addition, EclEmma highlights the execution status directly in the Java source editors: Green lines were fully executed, yellow lines were executed partially only and red lines were not hit at all.
		Little diamonds symbols in the editor's ruler, on the left to the source code, show the execution status of branches in your code (e.g.. for if and switch statements):</p>

			<br/><img alt="correctness completness" src="/community/eclipse_newsletter/2015/august/images/4-annotations.png" class="img-responsive"><br/>

    <h2>More Interesting Features</h2>
    	<p>Aside from the most common use-cases, EclEmma has more features that are not so widely known. Depending on your test setup EclEmma can collect code coverage data from a running Java process without terminating it. For this the
    	<a target="_blank" href="http://www.eclemma.org/userdoc/coverageview.html"><i>Coverage</i> view</a> provides a <i>Dump Execution Data</i> toolbar button. You can even retrieve execution data from a remote process that has
    	a <a target="_blank" href="http://www.eclemma.org/jacoco/trunk/doc/agent.html">JaCoCo agent</a> configured (see next section about the technology behind EclEmma) via the
    	<a target="_blank" href="http://www.eclemma.org/userdoc/importexport.html">Coverage Session Import</a>.</p>

		<p>If you run multiple tests on the same code base in different launches (e.g. unit tests and integration tests) another toolbar button in the coverage view called <i>Merge Sessions</i> allows combining these executions in a single
		coverage analysis.</p>

		<p>Once you collected some coverage data within you Eclipse IDE you might want to export the data in different formats. For this the <a target="_blank" href="http://www.eclemma.org/userdoc/importexport.html"><i>Coverage Export Wizard</i></a>
		creates, for example, browsable HTML reports with highlighted source code.</p>

		<p>To find out more, the EclEmma <a target="_blank" href="http://www.eclemma.org/userdoc/index.html">user guide</a> (which is also included with the plug-in's help pages) is a good starting point to get an overview over all features of this useful
		Eclipse plug-in.</p>


    <h2>Technology behind EclEmma</h2>

		<p>The EclEmma plug-in originally was designed as a wrapper for the EMMA code coverage library to make EMMA available within the Eclipse IDE. Since <a target="_blank" href="http://emma.sourceforge.net/">EMMA</a> maintenance has stopped
		many years ago and missed functional enhancements, such as branch coverage or support for the latest JDKs, a completely new code coverage backend called <a target="_blank" href="http://www.eclemma.org/jacoco/"><i>JaCoCo</i></a> was
		launched under the EclEmma project umbrella. Since the release of version 2.0 in 2012, EclEmma is -	despite its name - backed by the JaCoCo code coverage library.</p>

		<p>The JaCoCo library nicely complements the EclEmma Eclipse plug-in as there are extensive <a target="_blank" href="http://www.eclemma.org/jacoco/trunk/doc/integrations.html">integrations</a> with build tools like Ant, Maven, Gradle or
		SBT and also Continuous Inspection (CI) platforms like <a target="_blank" href="http://www.sonarqube.org/">SonarQube</a>.</p>

	<h2>Conclusion</h2>

		<p>As Uncle Bob Martin <a target="_blank" href="http://butunclebob.com/ArticleS.UncleBob.UntestedCodeDarkMatter">coined</a> it: "Untested code is the dark matter of the software -  apparently because it makes up 90% of the software universe".
		So let's start fighting dark matter by visualizing it with code coverage tools in your IDE and CI builds.</p>

<div class="bottomitem">
  <h3>About the Authors</h3>

  <div class="row">
    <div class="col-sm-12">
      <div class="row">
        <div class="col-sm-8">
          <img class="img-responsive"
        src="/community/eclipse_newsletter/2015/august/images/marc_hoffmann.jpg"
        alt="Marc R Hoffmann" />
        </div>
        <div class="col-sm-16">
          <p class="author-name">
        Marc R. Hoffmann <br />
        <a target="_blank" href="http://www.mtrail.ch/">mtrail GmbH</a>
      </p>
      <ul class="author-link list-inline">
        <!--  <li><a class="btn btn-small btn-warning" target="_blank" href="http://eclipsesource.com/blogs/author/jhelming/">Blog</a>
        </li>-->
        <li><a class="btn btn-small btn-warning" target="_blank" href="https://twitter.com/marcandsweep">Twitter</a></li>
        <?php //echo $og; ?>
      </ul>
        </div>
      </div>
    </div>
  </div>
</div>

