<?php
/*******************************************************************************
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/
?>

<div id="fullcolumn">
  <div id="midcolumn">
    <h1><?php echo $pageTitle; ?></h1> <img align=right src="/community/eclipse_newsletter/2015/august/images/frederic.png"/>
	    
			<h2>What do you do?</h2>
				<p>I am a Software Engineer at Yatta in Frankfurt am Main, Germany. Before I started at Yatta, I was working as a consultant for a number of eCommerce projects. As project lead, I am responsible for Yatta Profiles, 
				an app allowing you to save, share, install and update complete Eclipse and Workspace setups. I am also a committer for Eclipse Oomph.</p>
				<p>At Yatta, we work with Eclipse ourselves and we started developing Profiles because we wanted to make developers’ lives easier. Since Oomph was already great, we didn’t 
				have to reinvent the wheel entirely: Profiles is based on Oomph, but unique, because you can share and update your Eclipse Workspace setups with just a single click. 
				This is a huge benefit, especially for development teams working with the same Eclipse Workspace setup.</p>
				
			<h2>How long have you been using Eclipse?</h2>
				
				<p>I started using Eclipse in one of my first university classes, back in 2004. In the meantime I have tried Netbeans, IntelliJ and also Visual Studio (if necessary), but eventually I came to love 
				Eclipse and I’ve been using it ever since.</p>
									
			<h2>Name five plugins you use and recommend:</h2>
					<p>My first recommendation is of course Yatta Profiles. It has become absolutely essential to my own work and will soon be available for you to use. I can provide a working Eclipse setup to new 
						team members by just sending them a link. If there is an update for our Eclipse setup (for example a completely new plugin), I can roll out the update at the flick of a switch.</p>
					<ul>
						<li><b><a href="https://marketplace.eclipse.org/content/qwickie">qwickie</a></b>: I am a big fan of the Apache Wicket framework. Qwickie provides Apache Wicket integration in Eclipse, so it has to be on the list.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/maven-java-ee-integration-eclipse-wtp-lunamars">M2e</a></b>: This is actually more of a pet peeve of mine, but M2e provides good Maven integration – and Maven support 
						is absolutely essential for a modern IDE.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/tycho-build-tools">Tycho</a></b>: For developers who use Maven like me, Tycho is a vital plugin – I can’t think of any better alternative.</li>
						<li><b><a href="https://marketplace.eclipse.org/content/findbugs-eclipse-plugin">Findbugs</a></b>: Although I don’t use Findbugs every day, it always offers good clues to track down source code weaknesses.</li>
					</ul>

			<h2>What's your favorite thing to do when you're not working?</h2>
			<p>If I am not working, I train to make a 10 km run in under 42 minutes again. I also like taking short trips to European cities. It’s quite possible to meet me for breakfast in Barcelona on a Sunday morning. 
			As I usually don’t bring my runners, it will probably take me some time to beat 42 minutes.</p>
					
			<h2>User Details</h2>
				<ul>
					<li><a target="_blank" href="https://www.twitter.com/fEbelshaeuser">Twitter</a></li>
					<li><a target="_blank" href="https://www.xing.com/profile/Frederic_Ebelshaeuser">Xing</a></li>
					<li><a target="_blank" href="https://www.linkedin.com/pub/frederic-ebelshaeuser/97/a2b/619">LinkedIn</a></li>

				</ul>
		</div>
		</div>
	</div>