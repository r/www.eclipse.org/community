<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

?>

<div class="newsletter-header"><div class="newsletter-header-bg newsletter-subpages"></div>

    <?php if (isset($displayNewsletterSponsor) && $displayNewsletterSponsor): ?>

    <div class="sponsored-by-neon">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-16 text-center">
            <small class="margin-top-10">Newsletter sponsored by</small>
            <a target="_blank" href="<?php print $sponsorLink; ?>"><img class="neon-logo" src="<?php print $sponsorImage; ?>" alt="<?php print $sponsorName; ?>"></a>
          </div>
        </div>
      </div>
    </div>

    <?php endif; ?>

    <?php if (isset($displayNewsletterSearch) && $displayNewsletterSearch): ?>

    <div class="newsletter-search">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-sm-offset-16">
            <h3>Search the newsletter archives</h3>
            <div class="reset-box-sizing content">
            <style>
              .gsc-control-cse {
                background-color:inherit;
                border: inherit;
              }
            </style>
            <script>
              (function() {
                var cx = '011805775785170369411:9igyhchnmqc';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
              })();
            </script>
            <gcse:searchbox-only gname="newsletter" resultsUrl="https://www.eclipse.org/home/search.php" queryParameterName="n" as_sitesearch="www.eclipse.org/community/eclipse_newsletter/"></gcse:searchbox-only>
            </div>
          </div>
        </div>
      </div>

    </div>

    <?php endif; ?>

</div>