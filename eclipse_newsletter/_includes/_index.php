<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *    Christopher Guindon (Eclipse Foundation)
 *******************************************************************************/
//if name of the file requested is the same as the current file, the script will exit directly.
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){exit();}
  require_once($App->getProjectCommon());

  // Email template
  // Set body width for website
  $email_body_width = "100%";
  $col_1_img = "100%";
  $col_2_table = "48%";
  $col_2_img = "100%";
  $path = $App->getWWWPrefix();

  // Custom theme variables
  $variables = array();
  $variables['main_container_classes'] = '';
  $App->setThemeVariables($variables);

  $Theme->setPageAuthor($pageAuthor);
  $Theme->setPageKeywords($pageKeywords);
  $Theme->setPageTitle($pageTitle);

  // Bug 2191 - Page description - Newsletter and Newsletter Articles
  if (isset($pageDescription) && !empty($pageDescription)) {
    $Theme->setMetatags('og:description',  array(
      'property' => 'og:description',
      'content' => $pageDescription,
    ));
    $Theme->setMetatags('itemprop:description',  array(
      'itemprop' => 'description',
      'content' => $pageDescription,
    ));
    $Theme->setMetatags('twitter:description',  array(
      'name' => 'twitter:description',
      'content' => $pageDescription,
    ));
  }

  ob_start();
  include("_index-css.php");
  include("_mailchip.php");
  include("_subpage_header.php");
  print '<div class="container padding-top-20"><div class="row"><div class="col-sm-16">';
  include ("content/en_" . $script_name);
  print '</div>';
  include("_subpage_sidebar.php");
  print '</div></div>';

  $html = ob_get_clean();

  $Theme->setHtml($html);
  $Theme->setBreadcrumb($Breadcrumb);
  $Theme->setExtraHeaders('<link href="/downloads/assets/public/stylesheets/thankyou.min.css" media="screen" rel="stylesheet" type="text/css"/>');
  $Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
  $Theme->setDisplayFooterPrefix(FALSE);
  $Theme->generatePage();