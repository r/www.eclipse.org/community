<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation) - Initial implementation
 *******************************************************************************/

?>


<div class="col-sm-8">
  <div class="eclipse-newsletter-sidebar-subscribe">
    <h3>Eclipse Newsletter</h3>
    <p class="eclipse-newsletter-sidebar-big-text">A fresh new issue delivered monthly</p>
    <form action="https://eclipsecon.us6.list-manage.com/subscribe/post" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="margin-bottom-10 validate form-inline" target="_blank" novalidate>
      <div class="form-group">
        <input type="hidden" name="u" value="eaf9e1f06f194eadc66788a85">
        <input type="hidden" name="id" value="46e57eacf1">
        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-warning">
      </div>
    </form>
    <p>All Eclipse Newsletters available <a href="/community/eclipse_newsletter">here</a></p>
  </div>
</div>
