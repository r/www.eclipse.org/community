<?php
/*******************************************************************************
 * Copyright (c) 2016 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Christopher Guindon (Eclipse Foundation) - Initial implementation
 ********************************************************************************/
require_once($App->getProjectCommon());

if (isset ( $original_url )) {
  $App->AddExtraHtmlHeader ( '<link rel="canonical" href="' . $original_url . '" />' );
}

$Theme = $App->getThemeClass();
$Theme->setThemeVariables(array('sharethis' => TRUE));

ob_start();
include("_index-css.php");
include("_mailchip.php");
include("_subpage_header.php");
print '<div class="container padding-top-20"><div class="row"><div class="col-sm-16"><div class="article-body">';
include ("content/en_" . $script_name);
print '</div></div>';
include("_subpage_sidebar.php");
print '</div></div>';
$html = ob_get_clean ();

$App->AddExtraHtmlHeader ( '<link rel="stylesheet" type="text/css" href="/community/eclipse_newsletter/assets/articles-v2.css" media="screen" />' );

// Custom theme variables
$variables = array();
$variables['main_container_classes'] = '';
$App->setThemeVariables($variables);

$Theme->setHTML($html);
$Theme->setPageTitle($pageTitle);
$Theme->getPageKeywords($pageKeywords);
// Bug 2191 - Page description - Newsletter and Newsletter Articles
if (isset($pageDescription) && !empty($pageDescription)) {
  $Theme->setMetatags('og:description',  array(
    'property' => 'og:description',
    'content' => $pageDescription,
  ));
  $Theme->setMetatags('itemprop:description',  array(
    'itemprop' => 'description',
    'content' => $pageDescription,
  ));
  $Theme->setMetatags('twitter:description',  array(
    'name' => 'twitter:description',
    'content' => $pageDescription,
  ));
  $Theme->setMetatags('description',  array(
    'name' => 'twitter:description',
    'content' => $pageDescription,
  ));
}

$Theme->setPageAuthor($pageAuthor);
$Theme->setBreadcrumb($Breadcrumb);
$Theme->removeAttributes('breadcrumbs', 'breadcrumbs-default-margin');
$Theme->setDisplayFooterPrefix(FALSE);
$Theme->generatePage();